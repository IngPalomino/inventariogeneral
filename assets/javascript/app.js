$(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip();
});

function serializeObj(obj) {
	var result = [];

	for (var property in obj) {
		if (property != 'id' && property != 'created_at' && typeof obj[property] != 'function') {
			if (Array.isArray(obj[property])) {
				result.push(encodeURIComponent(property) + '=' + angular.toJson(obj[property]));
			} else if (typeof obj[property] == 'object') {
				try { result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]['id'])); }
				catch(err) { result.push(encodeURIComponent(property) + '=' + null); }
			} else if (property == 'tipo_almacen') {
				if (obj[property] == 1) {
					result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
					result.push('almacen=' + encodeURIComponent(obj['almacen']));
				} else if (obj[property] == 2) {
					result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
					result.push('almacen=' + encodeURIComponent(obj['a_sitio']));
					delete obj['a_sitio'];
				}
			} else {
				result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
			}
		}
	}

	return result.join('&');
}