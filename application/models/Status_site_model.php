<?php
class Status_site_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function get_sites($limit = null,$offset = null)
	{
		$this->db->order_by("codigo","ASC");
		if(isset($limit) && isset($offset))
		{
			$this->db->limit($limit,$offset);
		}
		elseif(isset($limit))
		{
			$this->db->limit($limit);
		}
		$sitios = $this->db->get("tb_status_site");
		$sitios_array = $sitios->result_array();

		$sitios_array = $this->Populate_model->populate("tb_status_site",$sitios_array,true);

		return $sitios_array;
	}

	public function look_for_sites($cadenas)
	{
		$this->db->order_by("codigo","ASC");

		$ind = 0;
		foreach($cadenas as $cadena)
		{
			if($ind == 0)
			{
				$this->db->like("nombre_completo",$cadena);
			}
			else
			{
				$this->db->or_like("nombre_completo",$cadena);
			}
			$ind++;
		}

		$sitios = $this->db->get("tb_status_site");
		$sitios_array = $sitios->result_array();

		$sitios_array = $this->Populate_model->populate("tb_status_site",$sitios_array,true);

		return $sitios_array;
	}
	
	public function look_for_sites_suministro($cadenas)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->join("tb_suministros su","su.sitio = ss.id","left");

		$ind = 0;
		foreach($cadenas as $cadena)
		{
			if($ind == 0)
			{
				$this->db->like("su.numero_suministro",$cadena);
			}
			else
			{
				$this->db->or_like("su.numero_suministro",$cadena);
			}
			$ind++;
		}

		$sitios = $this->db->get("tb_status_site ss");
		$sitios_array = $sitios->result_array();

		$sitios_array = $this->Populate_model->populate("tb_status_site",$sitios_array,true);

		return $sitios_array;
	}

	public function edit($id)
	{
		$sitio = $this->db->get_where("tb_status_site",array('id' => $id),1);
		$sitio_array = $sitio->result_array();

		$sitio_array = $this->Populate_model->populate("tb_status_site",$sitio_array,true,true);

		return $sitio_array;
	}

	public function listar($id)
	{
		$sitio = $this->db->get_where("tb_status_site",array('id' => $id),1);
		$sitio_array = $sitio->result_array();

		$sitio_array = $this->Populate_model->populate("tb_status_site",$sitio_array,true);

		/*foreach($sitio_array as &$sitio)
		{
			if(count($sitio["contratos"]) > 0)
			{
				foreach($sitio["contratos"] as &$contratos)
				{
					if(count($contratos["contactos_contrato"]) > 0)
					{
						foreach($contratos["contactos_contrato"] as &$contacto)
						{
							$contacto["telefono"] = json_decode($contacto["telefono"]);
							$contacto["correo"] = json_decode($contacto["correo"]);
						}
					}
				}
			}
		}*/

		return $sitio_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_status_site');

		return $resp;
	}

	public function insert($datos)
	{
		$salida = array(
				"resp" => false,
				"id" => null
			);
		$resp = $this->db->insert('tb_status_site',$datos);

		if($resp)
		{
			$salida["resp"] = $resp;
			$salida["id"] = $this->db->insert_id();
		}

		return $salida;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_status_site');
	}

	/*********************************************************************************************/

	public function resumen_sitios($limit = null,$offset = null)
	{
		$resumen = array();

		// Resumen 1
		$soloPOP = $this->db
			->select("'Sólo POP' tipo, COUNT(id) cantidad")
			->get_where("tb_status_site", array("in_building" => 0, "dorsal" => 0, "agregador" => 0, "coubicado" => 0, "estado" => 2));
		$soloInBuilding = $this->db
			->select("'Sólo In-building' tipo, COUNT(id) cantidad")
			->get_where("tb_status_site", array("in_building" => 1, "dorsal" => 0, "agregador" => 0, "coubicado" => 0, "estado" => 2));
		$soloDorsal = $this->db
			->select("'Sólo Dorsal' tipo, COUNT(id) cantidad")
			->get_where("tb_status_site", array("in_building" => 0, "dorsal" => 1, "agregador" => 0, "coubicado" => 0, "estado" => 2));
		$soloAgregador = $this->db
			->select("'Sólo Agregador' tipo, COUNT(id) cantidad")
			->get_where("tb_status_site", array("in_building" => 0, "dorsal" => 0, "agregador" => 1, "coubicado" => 0, "estado" => 2));
		$soloCoubicado = $this->db
			->select("'Sólo Coubicado' tipo, COUNT(id) cantidad")
			->get_where("tb_status_site", array("in_building" => 0, "dorsal" => 0, "agregador" => 0, "coubicado" => 1, "estado" => 2));

		$resumen01 = array();
		array_push($resumen01, $soloPOP->row_array());
		array_push($resumen01, $soloInBuilding->row_array());
		array_push($resumen01, $soloDorsal->row_array());
		array_push($resumen01, $soloAgregador->row_array());
		array_push($resumen01, $soloCoubicado->row_array());
		$resumen["resumen01"] = $resumen01;

		// Resumen 2
		$resumen02 = $this->db
						->select("p.proyecto, COUNT(ss.id) cantidad")
						->join("tb_proyecto p","p.id = ss.proyecto","left")
						->group_by("proyecto")
						->get_where("tb_status_site ss", array("estado" => 2));
		$resumen["resumen02"] = $resumen02->result_array();

		return $resumen;
	}

	public function tecnologias()
	{
		$tecnos = $this->db->get("tb_tecnologias");

		return $tecnos->result_array();
	}

	public function get_sitios_dif($id)
	{
		$this->db->select("id, nombre_completo, latitud, longitud, estado, agregador");
		$sitios = $this->db->get_where("tb_status_site", array("id !=" => $id));
		return $sitios->result_array();
	}

	public function get_sitio_nombre($id)
	{
		$this->db->select("id,nombre_completo");
		$sitio = $this->db->get_where("tb_status_site", array("id" => $id));
		return $sitio->row_array();
	}

	public function get_factor_uso_sitio($idsitio)
	{
		$this->db->select("id,nombre_completo,factor_uso_torre");
		$sitio = $this->db->get_where("tb_status_site", array("id" => $idsitio));
		return $sitio->row_array();
	}

	public function get_sitios_total()
	{
		$this->db->select("id, nombre_completo");
		$sitios = $this->db->get("tb_status_site");
		return $sitios->result_array();
	}

	public function esNombreCompletoValido($nombre)
	{
		$sitio = $this->db->select("id")
						->get_where("tb_status_site", array("nombre_completo" => $nombre), 1);

		if($sitio->num_rows() > 0)
		{
			return $sitio->row_array();
		}
		else
		{
			return false;
		}
	}



	public function get_codigo_on_air($codigo)
	{
		$this->db->select("id,codigo,nombre_completo");
		$sitio = $this->db->get_where("tb_status_site", array("codigo" => $codigo,"estado"=>2));
		return $sitio->result_array();
	}









}