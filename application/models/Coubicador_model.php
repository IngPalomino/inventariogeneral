<?php
class Coubicador_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_Coubicador()
	{
		$this->db->order_by("id","ASC");
		$coubicadores = $this->db->get("tb_coubicador");
		return $coubicadores->result_array();
	}

	public function edit($id)
	{
		$coubicador = $this->db->get_where("tb_coubicador",array('id' => $id),1);
		return $coubicador->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_coubicador');
	}

	public function insert($datos)
	{
		$this->db->insert('tb_coubicador',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_coubicador');
	}
}