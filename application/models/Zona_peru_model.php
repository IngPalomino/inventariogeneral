<?php
class Zona_peru_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_zonas()
	{
		$this->db->order_by("departamento","ASC");
		$this->db->order_by("provincia","ASC");
		$this->db->order_by("distrito","ASC");
		$zonas = $this->db->get("tb_zona_peru");
		return $zonas->result_array();
	}

	public function get_zona($datos)
	{
		$zona = $this->db->get_where("tb_zona_peru", array('departamento'=>$datos['departamento'], 'provincia'=>$datos['provincia'], 'distrito'=>$datos['distrito']));
		return $zona->result_array();
	}

	public function edit($id)
	{
		$zona = $this->db->get_where("tb_zona_peru",array('id' => $id),1);
		return $zona->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_zona_peru');
	}

	public function insert($datos)
	{
		$this->db->insert('tb_zona_peru',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_zona_peru');
	}

	public function get_departamentos()
	{
		$this->db->group_by("departamento");
		$this->db->order_by("departamento","ASC");
		$this->db->select("departamento");
		$departamentos = $this->db->get("tb_zona_peru");
		return $departamentos->result_array();
	}

	public function get_provincias($departamento)
	{
		$this->db->group_by("provincia");
		$this->db->order_by("provincia","ASC");
		$this->db->where("departamento",$departamento);
		$this->db->select("provincia");
		$provincias = $this->db->get("tb_zona_peru");
		return $provincias->result_array();
	}

	public function get_distritos($departamento,$provincia)
	{
		$this->db->group_by("distrito");
		$this->db->order_by("distrito","ASC");
		$this->db->where(array("departamento"=>$departamento,"provincia"=>$provincia));
		$this->db->select("id,distrito");
		$provincias = $this->db->get("tb_zona_peru");
		return $provincias->result_array();
	}
}