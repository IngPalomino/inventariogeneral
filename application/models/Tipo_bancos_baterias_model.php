<?php
class Tipo_bancos_baterias_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$listado = array(
				"marcas" => array(),
				"modelos" => array()
			);
		
		$this->db->select("marca");
		$this->db->group_by("marca");
		$marcas = $this->db->get("tb_tipo_bancos_baterias");
		
		$listado["marcas"] = $marcas->result_array();
		
		$this->db->reset_query();
		
		$this->db->order_by("modelo","ASC");
		$modelos = $this->db->get("tb_tipo_bancos_baterias");
		
		$listado["modelos"] = $modelos->result_array();
		
		return $listado;
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_tipo_bancos_baterias",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$antena = $this->db->get_where("tb_tipo_bancos_baterias",array('id' => $id),1);
		return $antena->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_bancos_baterias');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_tipo_bancos_baterias',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_bancos_baterias');
	}
}