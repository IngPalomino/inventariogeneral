<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_tanques_combustible_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}
	
	public function get_tanques_combustible($filtros = array())
	{
		if(isset($filtros["almacen"]))
		{
			$this->db->where(array("tipo_almacen" => 1, "almacen" => $filtros["almacen"]));
		}
		elseif(isset($filtros["sitio"]))
		{
			$this->db->where(array("tipo_almacen" => 2, "almacen" => $filtros["sitio"]));
		}
		
		$this->db->order_by("id", "ASC");
		$this->db->where(array('eliminado' => 0));
		$grupos = $this->db->get("tb_bodega_tanques_combustible");
		$grupos_array = $grupos->result_array();
		$grupos_array = $this->Populate_model->populate("tb_bodega_tanques_combustible",$grupos_array,true);
		
		foreach($grupos_array as &$grupo)
		{
			$this->db->select("id, nombre_completo");
			$nombre_completo = $this->db->get_where("tb_status_site",array('id' => $grupo["proveniente_sitio"]));
			$grupo["proveniente_sitio"] = $nombre_completo->row_array();

			switch($grupo["tipo_almacen"])
			{
				case 1:
					$almacen = $this->db->get_where("tb_almacenes",array("id" => $grupo["almacen"]));
					$grupo["almacen"] = $almacen->row_array();
					break;
				case 2:
					$this->db->select("id, nombre_completo");
					$sitio = $this->db->get_where("tb_status_site",array("id" => $grupo["almacen"]));
					$almacen = $sitio->row_array();
					$grupo["almacen"] = array(
							"id" => $almacen["id"],
							"almacen" => $almacen["nombre_completo"]
						);
					break;
			}
		}
		
		return $grupos_array;
	}

	public function edit($id)
	{
		$grupo = $this->db->get_where("tb_bodega_tanques_combustible",array('id' => $id),1);
		$grupo_array = $grupo->result_array();
		$grupo_array = $this->Populate_model->populate("tb_bodega_tanques_combustible",$grupo_array,true,true);
		return $grupo_array;
	}

	public function update($id, $datos)
	{
		$this->db->set($datos);
		$this->db->where('id', $id);
		return $this->db->update('tb_bodega_tanques_combustible');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_bodega_tanques_combustible', $datos);
	}
}