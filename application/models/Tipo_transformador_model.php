<?php
class Tipo_transformador_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$this->db->order_by("marca","ASC");
		$types = $this->db->get("tb_tipo_transformador");
		return $types->result_array();	
    }


    public function get_modelo($id)
	{
		$this->db->order_by("modelo","ASC");
		$this->db->where('tipo_transformador',$id);
		$types = $this->db->get("tb_tipo_transformador_modelo");
		return $types->result_array();	
    }


	public function get_type($data)
	{
		$type = $this->db->get_where("tb_tipo_transformador",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$antena = $this->db->get_where("tb_tipo_transformador",array('id' => $id),1);
		return $antena->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_transformador');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_tipo_transformador',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_transformador');
	}
}