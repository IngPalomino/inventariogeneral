<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mso_salas_tecnicas_model extends CI_Model {

	private $table;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = 'tb_mso_sala_tecnica';
	}

	public function get_sala_tecnicas()
	{

	$lista = $this->db->order_by('id', 'asc')	                    
						->get($this->table)
						->result_array();						
        
		return $lista;	
	}
	   


    public function get_sala_tecnicas_especifica()
	{
	   //return $this->db->select('id, nombre_completo')
	   return $this->db->select('CONCAT(id, "-mso") as id, nombre_completo')
						->get($this->table)
						->result_array();
	}

    public function get_sala_tecnica_id($id)
	{		
	$lista = $this->db->order_by('id', 'asc')
	                    ->where('id', $id)
						->get($this->table)
						->result_array();						
        
		return $lista;	
	}



}