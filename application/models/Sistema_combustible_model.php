<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema_combustible_model extends CI_Model {

// 	private $table;

// 	public function __construct()
// 	{
// 		$this->load->model('Populate_model');
// 		$this->table = 'tb_sistema_combustible';
// 	}

// 	public function get_sistema_combustible()
// 	{
// 		return $this->db->order_by('id', 'ASC')
// 						->get($this->table)
// 						->result_array();
// 	}

// 	public function get_sistema_combustible_sitio($id)
// 	{
// 		$antenas_microondas = $this->db->order_by('id', 'ASC')
// 										->select('i.sitio,i.link_name,tipoante.diametro, a.*')
// 										->join('tb_idus i', 'i.id = a.idu', 'left')
//                           ->join('tb_tipo_antena_mw tipoante', 'a.tipo_antena_mw = tipoante.id', 'left')
// 						  ->get_where($this->table.' a', array('i.sitio' => $id, 'a.eliminado' => 0))
										
// 										->result_array();
// 		$antenas_microondas = $this->Populate_model->populate($this->table, $antenas_microondas, TRUE);
		
// 		foreach ($antenas_microondas as &$antena)
// 		{
// 			$antena['enlace'] = $this->db->select('id, idu_1, idu_2')
// 											->get_where('tb_enlaces', array('id' => $antena['enlace']), 1)
// 											->row_array();
			
// 			$antena['enlace']['idu_1'] = $this->db->select('id, sitio')
// 													->get_where('tb_idus', array('id' => $antena['enlace']['idu_1'], 'eliminado' => 0), 1)
// 													->row_array();
			
// 			$antena['enlace']['idu_1']['sitio'] = $this->db->select('id, nombre_completo')
// 															->get_where('tb_status_site', array('id' => $antena['enlace']['idu_1']['sitio']), 1)
// 															->row_array();
			
// 			$antena['enlace']['idu_2'] = $this->db->select('id, sitio')
// 													->get_where('tb_idus', array('id' => $antena['enlace']['idu_2'], 'eliminado' => 0), 1)
// 													->row_array();
			
// 			$antena['enlace']['idu_2']['sitio'] = $this->db->select('id, nombre_completo')
// 															->get_where('tb_status_site', array('id' => $antena['enlace']['idu_2']['sitio']), 1)
// 															->row_array();
			
// 			$idu2Sitio = $antena['enlace']['idu_2']['sitio'];
			
// 			if ($antena['sitio'] == $idu2Sitio['id'])
// 			{
// 				$temp = $antena['enlace']['idu_1'];
// 				$antena['enlace']['idu_1'] = $antena['enlace']['idu_2'];
// 				$antena['enlace']['idu_2'] = $temp;
// 			}
// 		}
		
// 		return $antenas_microondas;
// 	}

// 	public function edit($id)
// 	{
// 		$antena_microondas = $this->db->get_where($this->table, array('id' => $id), 1)
// 										->result_array();
// 		return $this->Populate_model->populate($this->table, $antena_microondas, TRUE, TRUE);
// 	}

// 	public function update($id, $datos)
// 	{
// 		return $this->db->set($datos)
// 						->where('id', $id)
// 						->update($this->table);
// 	}

// 	public function update_where($parametros, $datos)
// 	{
// 		$this->db->set($datos);

// 		foreach ($parametros as $campo => $valor)
// 		{
// 			$this->db->where($campo, $valor);
// 		}

// 		return $this->db->update($this->table);
// 	}

// 	public function insert($datos)
// 	{
		
// 		return array(
// 				'resp' => $this->db->insert($this->table, $datos),
// 				'last_id' => $this->db->insert_id()
// 			);
		
// 	}

// 	public function delete($id)
// 	{
// 		return $this->db->where('id', $id)
// 						->delete($this->table);
// 	}

// 	public function get_sitio_antena($id)
// 	{
// 		$enlace_array = $this->db->select('ss.id, ss.nombre_completo, i2.sitio')
// 									->join('tb_enlaces e', 'e.id = ame.enlace')
// 									->join('tb_idus i', 'i.id = e.idu_1 OR i.id = e.idu_2', 'left')
// 									->join('tb_status_site ss', 'ss.id = i.sitio', 'left')
// 									->join('tb_idus i2', 'i2.id = ame.idu', 'left')
// 									->get_where($this->table.' ame', array('ame.id' => $id), 2)
// 									->result_array();

// 		foreach ($enlace_array as $key => $enlace)
// 		{
// 			if ($enlace['id'] != $enlace['sitio'])
// 			{
// 				$enlace_array[1] = $enlace;
// 			}
// 			else
// 			{
// 				$enlace_array[0] = $enlace;
// 			}
// 		}

// 		return $enlace_array;
// 	}

// 	public function delete_where($parametros)
// 	{
// 		foreach ($parametros as $campo => $valor)
// 		{
// 			$this->db->where($campo, $valor);
// 		}

// 		return $this->db->delete($this->table);
// 	}

// // version 2.3
// 	public function get_antenas_microondas_sitio_pendiente($id)
// 	{
// 	//funciona que trae los rojos y naranjas 		
// //0 antiguo aprobado
// //1 pendiente
// //2 reservado
// //3 instalado
// $where="i.sitio='$id'and a.eliminado='0'and a.rechazado='0' and  (a.estado_aprobacion='1' or a.estado_aprobacion='2')";
// $antenas_microondas = $this->db->order_by('id', 'ASC')
// 			->select('i.sitio,a.estado_aprobacion,i.link_name,tipoante.diametro, a.*')
// 						  ->join('tb_idus i', 'i.id = a.idu', 'left')
//                           ->join('tb_tipo_antena_mw tipoante', 'a.tipo_antena_mw = tipoante.id', 'left') 						 
//                           ->where($where, NULL, FALSE)
//                      ->get($this->table.' a')
// 						  ->result_array();
// 		$antenas_microondas = $this->Populate_model->populate($this->table, $antenas_microondas, TRUE);		
// 		foreach ($antenas_microondas as &$antena)
// 		{
// 			$antena['enlace'] = $this->db->select('id, idu_1, idu_2')
// 											->get_where('tb_enlaces', array('id' => $antena['enlace']), 1)
// 											->row_array();
			
// 			$antena['enlace']['idu_1'] = $this->db->select('id, sitio')
// 													->get_where('tb_idus', array('id' => $antena['enlace']['idu_1'], 'eliminado' => 0), 1)
// 													->row_array();
			
// 			$antena['enlace']['idu_1']['sitio'] = $this->db->select('id, nombre_completo')
// 															->get_where('tb_status_site', array('id' => $antena['enlace']['idu_1']['sitio']), 1)
// 															->row_array();
			
// 			$antena['enlace']['idu_2'] = $this->db->select('id, sitio')
// 													->get_where('tb_idus', array('id' => $antena['enlace']['idu_2'], 'eliminado' => 0), 1)
// 													->row_array();
			
// 			$antena['enlace']['idu_2']['sitio'] = $this->db->select('id, nombre_completo')
// 															->get_where('tb_status_site', array('id' => $antena['enlace']['idu_2']['sitio']), 1)
// 															->row_array();
			
// 			$idu2Sitio = $antena['enlace']['idu_2']['sitio'];
			
// 			if ($antena['sitio'] == $idu2Sitio['id'])
// 			{
// 				$temp = $antena['enlace']['idu_1'];
// 				$antena['enlace']['idu_1'] = $antena['enlace']['idu_2'];
// 				$antena['enlace']['idu_2'] = $temp;
// 			}
// 		}
		
// 		return $antenas_microondas;
// 	}

//     public function get_antenas_microondas_sitio_md($id)
// 	{
// 	//eslaoriginalcon la modificacionde estado, para listar



// //0 antiguo aprobado
// //1 pendiente
// //2 reservado
// //3 instalado
// $where="i.sitio='$id'and a.eliminado='0'and a.rechazado= '0' and(a.estado_aprobacion='0' or a.estado_aprobacion='3')";
// $antenas_microondas = $this->db->order_by('id', 'ASC')
// 			->select('i.sitio,a.estado_aprobacion,i.link_name,tipoante.diametro, a.*')
// 						  ->join('tb_idus i', 'i.id = a.idu', 'left')
//                           ->join('tb_tipo_antena_mw tipoante', 'a.tipo_antena_mw = tipoante.id', 'left') 						 
//                           ->where($where, NULL, FALSE)
//                      ->get($this->table.' a')
// 						  ->result_array();
// 		$antenas_microondas = $this->Populate_model->populate($this->table, $antenas_microondas, TRUE);		
// 		foreach ($antenas_microondas as &$antena)
// 		{
// 			$antena['enlace'] = $this->db->select('id, idu_1, idu_2')
// 											->get_where('tb_enlaces', array('id' => $antena['enlace']), 1)
// 											->row_array();
			
// 			$antena['enlace']['idu_1'] = $this->db->select('id, sitio')
// 													->get_where('tb_idus', array('id' => $antena['enlace']['idu_1'], 'eliminado' => 0), 1)
// 													->row_array();
			
// 			$antena['enlace']['idu_1']['sitio'] = $this->db->select('id, nombre_completo')
// 															->get_where('tb_status_site', array('id' => $antena['enlace']['idu_1']['sitio']), 1)
// 															->row_array();
			
// 			$antena['enlace']['idu_2'] = $this->db->select('id, sitio')
// 													->get_where('tb_idus', array('id' => $antena['enlace']['idu_2'], 'eliminado' => 0), 1)
// 													->row_array();
			
// 			$antena['enlace']['idu_2']['sitio'] = $this->db->select('id, nombre_completo')
// 															->get_where('tb_status_site', array('id' => $antena['enlace']['idu_2']['sitio']), 1)
// 															->row_array();
			
// 			$idu2Sitio = $antena['enlace']['idu_2']['sitio'];
			
// 			if ($antena['sitio'] == $idu2Sitio['id'])
// 			{
// 				$temp = $antena['enlace']['idu_1'];
// 				$antena['enlace']['idu_1'] = $antena['enlace']['idu_2'];
// 				$antena['enlace']['idu_2'] = $temp;
// 			}
// 		}
		
// 		return $antenas_microondas;
// 	}
    



}