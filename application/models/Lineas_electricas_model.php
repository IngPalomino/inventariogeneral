<?php
class Lineas_electricas_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function get_lineas_electricas()
	{
		$this->db->order_by("id","ASC");
		$lineas = $this->db->get("tb_lineas_electricas");
		return $lineas->result_array();
	}

	public function get_lineas_electricas_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$lineas_electricas = $this->db->get_where("tb_lineas_electricas",array('sitio' => $id, 'eliminado' => 0));

		$lineas_electricas_array = $lineas_electricas->result_array();

		$lineas_electricas_array = $this->Populate_model->populate("tb_lineas_electricas",$lineas_electricas_array,true);

		return $lineas_electricas_array;
	}

	public function edit($id)
	{
		$linea_electrica = $this->db->get_where("tb_lineas_electricas",array('id' => $id),1);
		$linea_electrica_array = $linea_electrica->result_array();

		$linea_electrica_array = $this->Populate_model->populate("tb_lineas_electricas",$linea_electrica_array,true,true);

		return $linea_electrica_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_lineas_electricas');
		return $resp;
	}

	public function insert($datos)
	{
		$resultado = $this->db->insert('tb_lineas_electricas',$datos);
		
		$resp = array(
			"resp" => $resultado,
			"last_id" => $this->db->insert_id()
		);
		
		return $resp;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_lineas_electricas');
	}

	public function get_sitio_linea($id)
	{
		$this->db->select("ss.id, ss.nombre_completo");
		$this->db->join("tb_status_site ss","ss.id = aa.sitio","left");
		$sitio = $this->db->get_where("tb_lineas_electricas aa", array("aa.id" => $id), 1);
		return $sitio->row_array();
	}
}