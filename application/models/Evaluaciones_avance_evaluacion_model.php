<?php
class Evaluaciones_avance_evaluacion_model extends CI_Model {
	public function __construct()
	{
		$this->load->database("evaluaciones");
	}

	public function listar()
	{
		$this->db->order_by("id","ASC");
		$query = $this->db->get("tb_avance_evaluacion");
		$resultado_array = $query->result_array();

		/*$resultado_array = $this->populate("tb_status_site",$resultado_array,true);*/

		return $resultado_array;
	}

	public function editar($id)
	{
		$query = $this->db->get_where("tb_avance_evaluacion",array('id' => $id),1);
		$resultado_array = $query->result_array();

		/*$resultado_array = $this->populate("tb_status_site",$resultado_array,true);*/

		return $resultado_array;
	}

	public function actualizar($id,$info)
	{

	}

	public function insertar($info)
	{
		$resp = $this->db->insert('tb_avance_evaluacion',$info);

		$last_id = $this->db->insert_id();

		$salida = array(
				"resp"		=>	$resp,
				"last_id"	=>	$last_id
			);

		return $salida;
	}

	public function nueva_evaluacion($id_eva,$contrata,$periodo)
	{
		$this->db->select("id tecnico_contrata, $id_eva evaluacion, $periodo evaluacion_periodo");
		$this->db->where(array("contrata" => $contrata));
		$resp = $this->db->get("tb_tecnicos_contrata");
		$resp = $resp->result_array();
		/*$resp[0]['evaluacion_periodo'] = $periodo;*/

		$respuesta = $this->db->insert_batch("tb_avance_evaluacion",$resp);

		return $respuesta;
	}

	public function listar_pendientes($id)
	{
		$resp = $this->db->get_where("tb_avance_evaluacion",array('evaluacion_periodo' => $id, 'ejecutado' => 0));
		$resp = $resp->result_array();

		foreach ($resp as &$evaluado)
		{
			$this->db->select("tc.*,c.contrata nombre_contrata, tt.tipo_tecnico nombre_tipo_tecnico");
			$this->db->join("tb_contrata c","c.id = tc.contrata");
			$this->db->join("tb_tipo_tecnico tt","tt.id = tc.tipo_tecnico");
			$this->db->where(array('tc.id' => $evaluado['tecnico_contrata']));
			$tecnico_contrata = $this->db->get("tb_tecnicos_contrata tc");

			$tecnico_contrata = $tecnico_contrata->row_array();

			$evaluado['tecnico_contrata'] = $tecnico_contrata;
		}

		/* Vigilar esta función */
		usort($resp,function($a,$b){
			/*strcasecmp($a["tecnico_contrata"]["nombre"],$b["tecnico_contrata"]["nombre"]) + strcasecmp($a["tecnico_contrata"]["nombre_contrata"],$b["tecnico_contrata"]["nombre_contrata"]);*/
			return strcasecmp($a["tecnico_contrata"]["nombre"],$b["tecnico_contrata"]["nombre"]);
		});

		return $resp;
	}

	public function listar_evaluados($id)
	{
		$resp = $this->db->get_where("tb_avance_evaluacion",array('evaluacion_periodo' => $id));
		$resp = $resp->result_array();

		foreach ($resp as &$evaluado)
		{
			$this->db->select("tc.*,c.contrata nombre_contrata, tt.tipo_tecnico nombre_tipo_tecnico");
			$this->db->join("tb_contrata c","c.id = tc.contrata");
			$this->db->join("tb_tipo_tecnico tt","tt.id = tc.tipo_tecnico");
			$this->db->where(array('tc.id' => $evaluado['tecnico_contrata']));
			$tecnico_contrata = $this->db->get("tb_tecnicos_contrata tc");

			$tecnico_contrata = $tecnico_contrata->row_array();

			$evaluado['tecnico_contrata'] = $tecnico_contrata;
		}

		// Vigilar esta función
		usort($resp,function($a,$b){
			return strcasecmp($a["tecnico_contrata"]["nombre"],$b["tecnico_contrata"]["nombre"]);
		});

		return $resp;
	}

	public function info_evaluado($evaPeriodo,$tecnico)
	{
		$this->db->limit(1);
		$resp = $this->db->get_where("tb_avance_evaluacion",array('evaluacion_periodo' => $evaPeriodo, 'tecnico_contrata' => $tecnico));
		$resp = $resp->row_array();

		$this->db->select("tc.*,c.contrata nombre_contrata");
		$this->db->join("tb_contrata c","c.id = tc.contrata");
		$tecnico_contrata = $this->db->get_where("tb_tecnicos_contrata tc",array('tc.id' => $tecnico));
		$evaluacion = $this->db->get_where("tb_evaluacion",array('id' => $resp['evaluacion']));

		$resp["tecnico_contrata"] = $tecnico_contrata->row_array();
		$resp["evaluacion"] = $evaluacion->row_array();

		$contenido = $this->db->get_where("tb_criterio",array("evaluacion" => $resp['evaluacion']['id']));
		$contenido = $contenido->result_array();

		foreach ($contenido as &$criterio)
		{
			$preguntas = $this->db->get_where("tb_sub_criterio",array('criterio' => $criterio['id']));

			$criterio['preguntas'] = $preguntas->result_array();
		}

		$resp['contenido'] = $contenido;

		return $resp;
	}

	public function registrar_evaluacion($id,$data)
	{
		$data["ejecutado"] = 1;
		$data["fecha_evaluacion"] = date("Y-m-d H:i:s");

		$this->db->where("id",$id);
		$resp = $this->db->update("tb_avance_evaluacion",$data);

		return $resp;
	}

	public function contar_evaluaciones_pendientes($evaluacion,$periodo)
	{
		$listado = $this->db->get_where("tb_avance_evaluacion",array("evaluacion" => $evaluacion, "evaluacion_periodo" => $periodo, "ejecutado" => 0));

		$listado = $listado->result_array();

		return $listado;
	}

	public function finalizar_evaluacion($id)
	{
		$this->db->where("id",$id);
		$resp = $this->db->update("tb_evaluaciones_periodo",array("finalizado" => 1));
	}
}
?>