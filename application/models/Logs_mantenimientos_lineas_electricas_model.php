<?php
class Logs_mantenimientos_lineas_electricas_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}

	public function get_logs()
	{
		$logs = $this->db->get("tb_logs_mantenimientos_lineas_electricas");
		return $logs->result_array();
	}

	public function ingreso_de_datos($user = FALSE,$datos)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		$datos["tipo"]	= "INSERT";
		$datos["id"]	= $this->db->insert_id();
		$datos["query"]	= $this->db->last_query();

		$data = array(
				"usuario"	=> $user,
				"mantenimiento"	=> $datos["id"],
				"evento"	=> json_encode($datos)
			);

		$this->db->insert("tb_logs_mantenimientos_lineas_electricas",$data);
	}

	public function actualizacion_de_datos($user = FALSE,$id,$datos,$comentario = FALSE)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		if($comentario === FALSE)
		{
			$comentario = "";
		}

		$datos["tipo"] = "UPDATE";
		$datos["query"]	= $this->db->last_query();

		$data = array(
				"usuario"	=> $user,
				"mantenimiento"	=> $id,
				"evento"	=> json_encode($datos),
				"comentario"=> $comentario
			);

		$this->db->insert("tb_logs_mantenimientos_lineas_electricas",$data);
		return json_encode($data);
	}
}
?>