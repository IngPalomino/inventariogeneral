<?php
class Tipo_antena_in_building_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$this->db->order_by("modelo","ASC");
		$types = $this->db->get("tb_tipo_antena_in_building");
		return $types->result_array();
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_tipo_antena_in_building",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$antena = $this->db->get_where("tb_tipo_antena_in_building",array('id' => $id),1);
		return $antena->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_antena_in_building');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_tipo_antena_in_building',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_antena_in_building');
	}
	
	public function get_tipo_antena()
	{
		$this->db->order_by("tipo", "ASC");
		$this->db->select("tipo");
		$this->db->group_by("tipo");
		$tipo = $this->db->get("tb_tipo_antena_in_building");
		return $tipo->result_array();
	}
}