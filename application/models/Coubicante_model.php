<?php
class Coubicante_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_Coubicante()
	{
		$this->db->order_by("id","ASC");
		$coubicantes = $this->db->get("tb_coubicante");
		return $coubicantes->result_array();
	}

	public function edit($id)
	{
		$coubicante = $this->db->get_where("tb_coubicante",array('id' => $id),1);
		return $coubicante->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_coubicante');
	}

	public function insert($datos)
	{
		$this->db->insert('tb_coubicante',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_coubicante');
	}
}