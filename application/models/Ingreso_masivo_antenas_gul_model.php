<?php
class Ingreso_masivo_antenas_gul_model extends CI_Model{

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function esNombreCompletoValido($nombre)
	{
		$sitio = $this->db->select("id")
						->get_where("tb_status_site", array("nombre_completo" => $nombre), 1);

		if($sitio->num_rows() > 0)
		{
			return $sitio->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esNombreCodigoValidos($codigo, $nombre)
	{
		$sitio = $this->db->select("id")
						->get_where("tb_status_site", array("codigo" => $codigo, "nombre_completo" => $nombre), 1);

		if($sitio->num_rows() > 0)
		{
			return $sitio->row_array();
		}
		else
		{
			return false;
		}
	}

	public function existeAntenaEnSitio($sitio, $sector)
	{
		$query = $this->db->get_where("tb_antenas_gul", array("sitio" => $sitio, "sector" => $sector, "eliminado" => 0), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function existeAntenaTelnetEnSitio($sitio)
	{
		$telnet = $this->get_antenas_telnet();
		$this->db->group_start();

		foreach($telnet as $antena)
		{
			if(count($telnet) == 1)
			{
				$this->db->where("tipo_antena_gul", $antena["id"]);
			}
			elseif(count($telnet) > 1)
			{
				$this->db->or_where("tipo_antena_gul", $antena["id"]);
			}
			
		}

		$this->db->group_end();
		$this->db->where(array("sitio" => $sitio, "eliminado" => 0));
		$query = $this->db->get("tb_antenas_gul", 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esMarcaValida($marca)
	{
		$query = $this->db->get_where("tb_tipo_antena_gul", array("marca" => $marca), 1);

		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function esModeloValido($modelo)
	{
		$query = $this->db->get_where("tb_tipo_antena_gul", array("modelo" => $modelo), 1);

		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function esMarcaModeloValidos($marca, $modelo)
	{
		$query = $this->db->get_where("tb_tipo_antena_gul", array("marca" => $marca, "modelo" => $modelo), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function registrar_ingreso_masivo($usuario)
	{
		$datos["usuario"] = $usuario;
		$datos["tabla"] = 1;

		if($this->db->insert("tb_ingreso_masivo", $datos))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}

	public function ingresar_temporal($datos)
	{
		return $this->db->insert("tb_temp_antenas_gul", $datos);
	}

	public function update($id, $datos)
	{
		$query = $this->db->set($datos)
							->where("id", $id)
							->update("tb_temp_antenas_gul");
		return $query;
	}

	public function get_solicitudes($usuario)
	{
		$this->db->order_by("id", "DESC");
		$query = $this->db->get_where("tb_ingreso_masivo", array("usuario" => $usuario, "tabla" => 1));
		$soliticudes = $query->result_array();
		$soliticudes = $this->Populate_model->populate("tb_ingreso_masivo", $soliticudes, true);
		return $soliticudes;
	}

	function get_antenas_telnet()
	{
		$antenas = $this->db->get_where("tb_tipo_antena_gul", array("marca" => "TELNET"));
		return $antenas->result_array();
	}
}