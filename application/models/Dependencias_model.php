<?php
class Dependencias_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}

	public function get_dependencies()
	{
		$dependencias = $this->db->get("tb_dependencias");
		return $dependencias->result_array();
	}

	public function insert($info)
	{
		$datos = array();
		$datos['tabla_primaria'] = $info['tabla_primaria'];
		$datos['tabla_secundaria'] = $info['tabla_secundaria'];
		$datos['objetos_primario'] = array(
										"columna" => $info['objetos_primario']
									);
		$datos['objetos_primario'] = json_encode($datos['objetos_primario']);
		$datos['objetos_secundario'] = array(
										"columna" => $info['objetos_secundario']
									);
		$datos['objetos_secundario'] = json_encode($datos['objetos_secundario']);

		if($info['operador_primario'] == "*")
		{
			if($info['operador_secundario'] == "*")
			{
				$datos['caracteristicas'] = array(
												"operador" => "*",
												"entonces" => array(
																"operador" => "*",
																"max" => $info['maximo']
																)
											);
			}
		}
		$datos['caracteristicas'] = json_encode($datos['caracteristicas']);

		$this->db->insert('tb_dependencias',$datos);
	}
}