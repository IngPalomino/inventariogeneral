<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_masivo_rectificadores_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function esNombreCompletoValido($nombre)
	{
		$sitio = $this->db->select("id")
						->get_where("tb_status_site", array("nombre_completo" => $nombre), 1);

		if($sitio->num_rows() === 1)
		{
			return $sitio->row_array();
		}
		else
		{
			return FALSE;
		}
	}


	public function esNombreCodigoValidos($codigo, $nombre)
	{
		$sitio = $this->db->select("id")
						->get_where("tb_status_site", array("codigo" => $codigo, "nombre_completo" => $nombre), 1);

		if($sitio->num_rows() === 1)
		{
			return $sitio->row_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function marca_valida($marca)
	{
		$query = $this->db->get_where("tb_tipo_controlador_rectif", array("marca" => $marca), 1);

		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function controlador_valido($controlador)
	{
		$query = $this->db->get_where("tb_tipo_controlador_rectif", array("modelo" => $controlador), 1);

		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function tipo_controlador_rectif_valido($marca, $controlador)
	{
		$query = $this->db->get_where("tb_tipo_controlador_rectif", array("marca" => $marca, "modelo" => $controlador), 1);

		if ($query->num_rows() === 1)
		{
			return $query->row_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function modelo_valido($modelo)
	{
		$query = $this->db->get_where("tb_tipo_modulos_rectif", array("modelo" => $modelo), 1);

		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function tipo_modulo_rectif_valido($marca, $modelo, $controlador)
	{
		$query = $this->db->get_where("tb_tipo_modulos_rectif", array("marca" => $marca, "modelo" => $modelo, "tipo_controlador_rectif" => $controlador), 1);

		if ($query->num_rows() === 1)
		{
			return $query->row_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function validate_date($date, $format = "Y-m-d H:i:s")
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

	public function energizacion_valida($energizacion)
	{
		$query = $this->db->get_where("tb_energizacion_controlador_rectif", array("energizacion_controlador_rectif" => $energizacion), 1);

		if ($query->num_rows() === 1)
		{
			return $query->row_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function ubicacion_valida($ubicacion)
	{
		$query = $this->db->get_where("tb_ubicacion_controlador_rectif", array("ubicacion_controlador_rectif" => $ubicacion), 1);

		if ($query->num_rows() === 1)
		{
			return $query->row_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function existe_rectificador($energizacion, $sitio)
	{
		$query = $this->db->get_where("tb_controlador_rectif", array("sitio" => $sitio, "energizacion_controlador_rectif" => $energizacion, "eliminado" => 0), 1);

		if ($query->num_rows() === 1)
		{
			return $query->row_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function get_tipo_rectificador()
	{
		return $this->db->order_by("marca ASC, modelo ASC, controlador ASC")
						->select("cr.marca, mr.modelo, cr.modelo controlador")
						->join("tb_tipo_controlador_rectif cr", "cr.id = mr.tipo_controlador_rectif", "left")
						->get("tb_tipo_modulos_rectif mr")
						->result_array();
	}

	public function registrar_ingreso_masivo($usuario)
	{
		$datos["usuario"] = $usuario;
		$datos["tabla"] = 7;

		if ($this->db->insert("tb_ingreso_masivo", $datos))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}

	public function ingresar_temporal($datos)
	{
		$respuesta["resp"] = $this->db->insert("tb_temp_controlador_rectif", $datos);
		$respuesta["last_id"] = $this->db->insert_id();
		return $respuesta;
	}

	public function ingresar_temporal_modulos($datos)
	{
		return $this->db->insert("tb_temp_modulos_rectif", $datos);
	}

	public function update($id, $datos)
	{
		return $this->db->set($datos)
						->where("id", $id)
						->update("tb_temp_controlador_rectif");
	}

	public function get_solicitudes($usuario)
	{
		$query = $this->db->order_by("id", "DESC")
							->get_where("tb_ingreso_masivo", array("usuario" => $usuario, "tabla" => 7))
							->result_array();

		return $this->Populate_model->populate("tb_ingreso_masivo", $query, TRUE);
	}
}