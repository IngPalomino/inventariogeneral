<?php
class Proveedor_mantenimiento_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_Proyecto()
	{
		$this->db->order_by("id","ASC");
		$tipo_soluciones = $this->db->get("tb_proveedor_mantenimiento");
		return $tipo_soluciones->result_array();
	}

	public function edit($id)
	{
		$tipo_solucion = $this->db->get_where("tb_proveedor_mantenimiento",array('id' => $id),1);
		return $tipo_solucion->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_proveedor_mantenimiento');
	}

	public function insert($datos)
	{
		$this->db->insert('tb_proveedor_mantenimiento',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_proveedor_mantenimiento');
	}
}