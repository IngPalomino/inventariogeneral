<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs_bodega_elementos_sistema_combustible_model extends CI_Model {

	private $table;
	private $identity;

	public function __construct()
	{
		$this->table = 'tb_logs_bodega_elementos_sistema_combustible';
		$this->identity = 'elemento_sistema_combustible';

	}

	public function get_logs()
	{
		return $this->db->get($this->table)
						->result_array();
	}

	public function ingreso_de_datos($datos, $user = FALSE)
	{
		$user || $user = $this->session->userdata('id_usuario');

		$datos['tipo'] = 'INSERT';
		$datos['id'] = $this->db->insert_id();
		$datos['query'] = $this->db->last_query();

		$data = array(
				'usuario' => $user,
				$this->identity => $datos['id'],
				'evento' => json_encode($datos)
			);

		return $this->db->insert($this->table, $data);
	}

	public function actualizacion_de_datos($id, $datos, $comentario = NULL, $user = FALSE)
	{
		$user || $user = $this->session->userdata('id_usuario');

		$datos['tipo'] = 'UPDATE';
		$datos['query'] = $this->db->last_query();

		$data = array(
				'usuario' => $user,
				$this->identity => $id,
				'evento' => json_encode($datos),
				'comentario' => $comentario
			);

		return $this->db->insert($this->table, $data);
	}
}