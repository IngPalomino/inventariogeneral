<?php
class Logs_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_logs()
	{
		$logs = $this->db->get("tb_logs");
		return $logs->result_array();
	}

	public function inicio_sesion($user = FALSE)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		$data = array(
				"usuario" => $user,
				"evento" => '{"campo":"sesion", "proceso":"inicio", "datos":"", "accion":"", "resumen":"inicio de sesión"}'
			);

		$this->db->insert("tb_logs",$data);
	}

	public function cerrar_sesion($user = FALSE)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		$data = array(
				"usuario" => $user,
				"evento" => '{"campo":"sesion", "proceso":"fin", "datos":"", "accion":"", "resumen":"fin de sesión"}'
			);

		$this->db->insert("tb_logs",$data);
	}

	public function ingreso_de_datos($user = FALSE,$datos,$tabla)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		$datos["tipo"]	= "INSERT";
		$datos["id"]	= $this->db->insert_id();
		$datos["query"]	= $this->db->last_query();

		$data = array(
				"usuario" => $user,
				"evento" => json_encode($datos)
			);

		$this->db->insert("tb_logs",$data);
	}

	public function actualizacion_de_datos($user = FALSE,$datos)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		$datos["tipo"] = "UPDATE";
		$datos["query"]	= $this->db->last_query();

		$data = array(
				"usuario" => $user,
				"evento" => json_encode($datos)
			);

		$this->db->insert("tb_logs",$data);
		return json_encode($data);
	}
}