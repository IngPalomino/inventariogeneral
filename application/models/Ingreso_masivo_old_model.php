<?php
	class Ingreso_masivo_model extends CI_Model{
		public function __construct()
		{
			$this->load->database();		
		}

		public function confirmar_sitio($sitio)
		{
			$this->db->select("id");
			$this->db->where('nombre_completo = "'.$sitio.'"');
			$lista = $this->db->get("tb_status_site");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_departamento($departamento)
		{
			$this->db->select("id");
			$this->db->where('LOWER(departamento) = LOWER("'.$departamento.'")');
			$lista = $this->db->get("tb_zona_peru");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_provincia($provincia)
		{
			$this->db->select("id");
			$this->db->where('LOWER(provincia) = LOWER("'.$provincia.'")');
			$lista = $this->db->get("tb_zona_peru");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_distrito($distrito)
		{
			$this->db->select("id");
			$this->db->where('LOWER(distrito) = LOWER("'.$distrito.'")');
			$lista = $this->db->get("tb_zona_peru");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_zona_peru($departamento,$provincia,$distrito)
		{
			$this->db->select("id");
			$this->db->where('LOWER(departamento) = LOWER("'.$departamento.'") AND LOWER(provincia) = LOWER("'.$provincia.'") AND LOWER(distrito) = LOWER("'.$distrito.'")');
			$lista = $this->db->get("tb_zona_peru");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_tipo_torre($torre)
		{
			$this->db->select("id");
			$this->db->where("LOWER(tipo_torre) = LOWER('$torre')");
			$lista = $this->db->get("tb_tipo_torre");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_tipo_estacion($estacion)
		{
			$estacion = strtoupper($estacion);
			if($estacion == "GREENFIELD" || $estacion == "ROOFTOP")
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_tipo_mimetizado($camuflaje)
		{
			$this->db->select("id");
			$this->db->where("LOWER(camuflaje) = LOWER('$camuflaje')");
			$lista = $this->db->get("tb_camuflaje");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_coubicador($coubicador)
		{
			$this->db->select("id");
			$this->db->where("LOWER(coubicador) = LOWER('$coubicador')");
			$lista = $this->db->get("tb_coubicador");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_coubicante($coubicante)
		{
			$this->db->select("id");
			$this->db->where("LOWER(coubicante) = LOWER('$coubicante')");
			$lista = $this->db->get("tb_coubicante");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_proyecto($proyecto)
		{
			$this->db->select("id");
			$this->db->where("LOWER(proyecto) = LOWER('$proyecto')");
			$lista = $this->db->get("tb_proyecto");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_contrata_constructora($contrata_constructora)
		{
			$this->db->select("id");
			$this->db->where("LOWER(contrata_constructora) = LOWER('$contrata_constructora')");
			$lista = $this->db->get("tb_contrata_constructora");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_tipo_solucion($tipo_solucion)
		{
			$this->db->select("id");
			$this->db->where("LOWER(tipo_solucion) = LOWER('$tipo_solucion')");
			$lista = $this->db->get("tb_tipo_solucion");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function confirmar_proveedor_mantenimiento($proveedor_mantenimiento)
		{
			$this->db->select("id");
			$this->db->where("LOWER(proveedor_mantenimiento) = LOWER('$proveedor_mantenimiento')");
			$lista = $this->db->get("tb_proveedor_mantenimiento");
			$resultado = $lista->result_array();

			if(count($resultado) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/****************************************************************************************************/

		public function get_sitio($sitio)
		{
			$this->db->select("id");
			$this->db->where('nombre_completo = "'.$sitio.'"');
			$lista = $this->db->get("tb_status_site");
			$resultado = $lista->result_array();

			return $resultado[0];
		}

		public function get_zona_peru($departamento,$provincia,$distrito)
		{
			$this->db->select("id");
			$this->db->where('LOWER(departamento) = LOWER("'.$departamento.'") AND LOWER(provincia) = LOWER("'.$provincia.'") AND LOWER(distrito) = LOWER("'.$distrito.'")');
			$lista = $this->db->get("tb_zona_peru");
			$resultado = $lista->result_array();

			return $resultado[0];
		}

		public function get_tipo_torre($tipo_torre)
		{
			$this->db->select("id");
			$this->db->where("tipo_torre = '$tipo_torre'");
			$lista = $this->db->get("tb_tipo_torre");
			$resultado = $lista->result_array();

			return $resultado[0];
		}

		public function get_camuflaje($camuflaje)
		{
			$this->db->select("id");
			$this->db->where("camuflaje = '$camuflaje'");
			$lista = $this->db->get("tb_camuflaje");
			$resultado = $lista->result_array();

			return $resultado[0];
		}

		public function get_ubicacion_equipos($ubicacion_equipos)
		{
			$this->db->select("id");
			$this->db->where("ubicacion_equipos = '$ubicacion_equipos'");
			$lista = $this->db->get("tb_ubicacion_equipos");
			$resultado = $lista->result_array();

			return $resultado[0];
		}

		public function get_coubicador($coubicador)
		{
			$this->db->select("id");
			$this->db->where("coubicador = '$coubicador'");
			$lista = $this->db->get("tb_coubicador");
			$resultado = $lista->result_array();

			return $resultado[0];
		}

		public function get_coubicante($coubicante)
		{
			$this->db->select("id");
			$this->db->where("coubicante = '$coubicante'");
			$lista = $this->db->get("tb_coubicante");
			$resultado = $lista->result_array();

			return $resultado[0];
		}

		public function get_proyecto($proyecto)
		{
			$this->db->select("id");
			$this->db->where("proyecto = '$proyecto'");
			$lista = $this->db->get("tb_proyecto");
			$resultado = $lista->result_array();

			return $resultado[0];
		}

		public function get_contrata_constructora($contrata_constructora)
		{
			$this->db->select("id");
			$this->db->where("contrata_constructora = '$contrata_constructora'");
			$lista = $this->db->get("tb_contrata_constructora");
			$resultado = $lista->result_array();

			return $resultado[0];
		}

		public function get_tipo_solucion($tipo_solucion)
		{
			$this->db->select("id");
			$this->db->where("tipo_solucion = '$tipo_solucion'");
			$lista = $this->db->get("tb_tipo_solucion");
			$resultado = $lista->result_array();

			return $resultado[0];
		}

		public function get_proveedor_mantenimiento($proveedor_mantenimiento)
		{
			$this->db->select("id");
			$this->db->where("proveedor_mantenimiento = '$proveedor_mantenimiento'");
			$lista = $this->db->get("tb_proveedor_mantenimiento");
			$resultado = $lista->result_array();

			return $resultado[0];
		}

		public function get_id_contrato($sitio,$contrato)
		{
			$this->db->select("id");
			$this->db->where(array("id_contrato" => $contrato, "sitio" => $sitio));
			$lista = $this->db->get("tb_contratos");
			$resultado = $lista->result_array();

			if( count($resultado) == 0 )
			{
				return FALSE;
			}
			else
			{
				return $resultado[0];
			}
		}

		public function get_id_contacto($contrato,$contacto)
		{
			$this->db->select("id");
			$this->db->where(array("contrato" => $contrato, "contacto" => $contacto));
			$this->db->limit(1);
			$lista = $this->db->get("tb_contactos_contrato");
			$resultado = $lista->result_array();

			if( count($resultado) == 0 )
			{
				return FALSE;
			}
			else
			{
				return $resultado[0];
			}
		}

		/*******************************************************************************************************************/

		public function registrar_ingreso_masivo($usuario,$tabla)
		{
			$datos = array(
					"usuario"	=> $usuario,
					"tabla"		=> $tabla,
					"pendiente"	=> 1
				);

			if( $this->db->insert("tb_ingreso_masivo",$datos) )
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}
		}

		public function ingresar_temporal($tabla,$datos)
		{
			$resp = $this->db->insert($tabla, $datos);

			return $resp;
		}

		public function listar()
		{
			$this->db->order_by('pendiente DESC, created_at ASC');
			$lista = $this->db->get("tb_ingreso_masivo");
			$resultado = $lista->result_array();

			$resultado = $this->populate("tb_ingreso_masivo",$resultado,true);

			foreach($resultado as &$validacion)
			{
				$tabla = "tb_temp_" . substr($validacion["tabla"],3);

				$this->db->select("COUNT(id) pendiente");
				$this->db->where(array("ingreso_masivo" => $validacion["id"], "estado_validacion" => 0));
				$evaluacion = $this->db->get($tabla);
				$res = $evaluacion->row_array();
				$validacion["pendiente"] = $res["pendiente"];

				$this->db->select("COUNT(id) validado");
				$this->db->where(array("ingreso_masivo" => $validacion["id"], "estado_validacion" => 1));
				$evaluacion = $this->db->get($tabla);
				$res = $evaluacion->row_array();
				$validacion["validado"] = $res["validado"];

				$this->db->select("COUNT(id) cancelado");
				$this->db->where(array("ingreso_masivo" => $validacion["id"], "estado_validacion" => 2));
				$evaluacion = $this->db->get($tabla);
				$res = $evaluacion->row_array();
				$validacion["cancelado"] = $res["cancelado"];
			}

			return $resultado;
		}

		public function listar_pendientes($elemento)
		{
			$this->db->where(array("tabla" => $elemento, "pendiente" => 1));
			$this->db->order_by('created_at', 'ASC');
			$lista = $this->db->get("tb_ingreso_masivo");
			$resultado = $lista->result_array();

			$resultado = $this->populate("tb_ingreso_masivo",$resultado,true);

			return $resultado;
		}

		public function listar_detalles_ingreso_masivo($id,$elemento)
		{
			$tablaOriginal = $elemento;
			$elemento = substr($elemento, 3);
			$tablaTemp = "tb_temp_" . $elemento;

			$this->db->where(array("ingreso_masivo" => $id));
			$this->db->order_by('estado_validacion ASC');
			$lista = $this->db->get($tablaTemp);
			$resultado = $lista->result_array();

			$resultado = $this->populate($tablaTemp,$resultado,true); /*"tb_temp_status_site"*/

			foreach($resultado as &$fila)
			{
				$campoRelacion = $elemento;

				$this->db->where(array("id" => $fila[$campoRelacion]));
				$listaRelacion = $this->db->get($tablaOriginal);
				$fila[$campoRelacion] = $listaRelacion->row_array();

				$fila[$campoRelacion] = $this->populate($tablaOriginal,$fila[$campoRelacion],false); /*"tb_status_site"*/

				$ind = 0;
				foreach($fila as $campo => $valor)
				{
					if($valor == NULL)
					{
						unset($fila[$campo]);
						unset($fila[$campoRelacion][$campo]);
						$ind--;
					}
				}
			}

			$resultado = array_values($resultado);

			/*$resultado = $this->populate("tb_temp_status_site",$resultado,true);*/

			return $resultado;
		}

		public function validar($datos)
		{
			$tablaAfectante = "tb_temp_".$datos["elemento"];

			$this->db->select($datos["campo_ingreso_masivo"]);
			$this->db->where(array("id" => $datos["id_ingreso_masivo"]));
			$resultado = $this->db->get($tablaAfectante);
			$resultado = $resultado->row_array();

			return $resultado;
		}

		public function validar_estado($datos)
		{
			$tablaAfectante = "tb_temp_".$datos["elemento"];

			$this->db->set(array( "estado_validacion" => 1,"usuario_revision" => $datos["usuario"],"fecha_revision" => date("Y-m-d H:i:s") ));
			$this->db->where('id',$datos["id_ingreso_masivo"]);
			$resp = $this->db->update($tablaAfectante);

			return $resp;
		}

		public function cancelar($datos)
		{
			$tablaAfectante = "tb_temp_".$datos["elemento"];

			$this->db->set(array( "estado_validacion" => 2,"usuario_revision" => $datos["usuario"],"fecha_revision" => date("Y-m-d H:i:s"),"comentario_cancelacion" => $datos["comentario_cancelacion"] ));
			$this->db->where('id',$datos["id_ingreso_masivo"]);
			$resp = $this->db->update($tablaAfectante);

			return $resp;
		}

		/*********************************************************************************************/

		private function populate($table, $info_array, $is_array)
		{
			if((gettype($table) != "string") && (gettype($info_array) != "array") && (gettype($is_array) != "boolean"))
			{
				return false;
			}

			$relaciones = $this->db->get_where("tb_relaciones",array("tabla_primaria" => $table));
			$relaciones = $relaciones->result_array();

			if($is_array)
			{	
				foreach($info_array as &$info)
				{
					foreach($relaciones as $relacion)
					{
						if(($relacion["relacion"] == "1:1") || ($relacion["relacion"] == "N:N"))
						{
							$datos = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $info[$relacion["columna_primaria"]]),1);
							$datos = $datos->row_array();
							$datos = $this->populate($relacion["tabla_secundaria"],$datos,false);
							$info[$relacion["columna_primaria"]] = $datos;
						}
					}
				}
			}
			else
			{
				foreach($relaciones as $relacion)
				{
					if(($relacion["relacion"] == "1:1") || ($relacion["relacion"] == "N:N"))
					{
						$datos = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $info_array[$relacion["columna_primaria"]]),1);
						$datos = $datos->row_array();
						$datos = $this->populate($relacion["tabla_secundaria"],$datos,false);
						$info_array[$relacion["columna_primaria"]] = $datos;
					}
				}
			}

			return $info_array;
		}
	}
?>