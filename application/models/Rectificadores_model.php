<?php
class Rectificadores_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function get_controlador_rectif()
	{
		$this->db->order_by("id","ASC");
		$controladores = $this->db->get("tb_controlador_rectif");
		return $controladores->result_array();
	}

	public function get_controlador_rectif_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$controladores_rectif = $this->db->get_where("tb_controlador_rectif",array('sitio' => $id, 'eliminado' => 0,'id_sala_tecnica'=> 0));

		$controladores_rectif_array = $controladores_rectif->result_array();

		$controladores_rectif_array = $this->Populate_model->populate("tb_controlador_rectif",$controladores_rectif_array,true);

		return $controladores_rectif_array;
	}



	public function get_controlador_rectif_sitio_mso($id)
	{
		$this->db->order_by("id","ASC");
		$controladores_rectif = $this->db->get_where("tb_controlador_rectif",array('sitio' => $id,
		 'eliminado' => 0,'id_sala_tecnica >'=> 0));
		$controladores_rectif_array = $controladores_rectif->result_array();

		$controladores_rectif_array = $this->Populate_model->populate("tb_controlador_rectif",$controladores_rectif_array,true);

		return $controladores_rectif_array;
	}









	
	public function get_controlador_rectif_eliminado_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$controladores_rectif = $this->db->get_where("tb_controlador_rectif",array('sitio' => $id, 'eliminado' => 1));

		$controladores_rectif_array = $controladores_rectif->result_array();

		$controladores_rectif_array = $this->Populate_model->populate("tb_controlador_rectif",$controladores_rectif_array,true);

		return $controladores_rectif_array;
	}

	public function edit($id)
	{
		$controlador_rectif = $this->db->get_where("tb_controlador_rectif",array('id' => $id),1);
		$controlador_rectif_array = $controlador_rectif->result_array();

		$controlador_rectif_array = $this->Populate_model->populate("tb_controlador_rectif",$controlador_rectif_array,true,true);

		return $controlador_rectif_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_controlador_rectif');
		return $resp;
	}

	public function update_ss_tecnologias($id,$eliminado)
	{
		$rectif = $this->db->get_where("tb_controlador_rectif",array("id" => $id));
		$rectif_array = $rectif->row_array();

		$this->db->set(array("eliminado" => $eliminado));
		$this->db->where(array("sitio" => $rectif_array["sitio"], "tecnologia" => $rectif_array["ubicacion_controlador_rectif"]));
		$resp = $this->db->update('tb_ss_tecnologias');
		return $resp;
	}
	

	public function insert($datos)
	{
		$resultado = $this->db->insert('tb_controlador_rectif',$datos);
		
		$resp = array(
			"resp" => $resultado,
			"last_id" => $this->db->insert_id()
		);
		
		return $resp;
	}

	public function insert_ss_tecnologias($datos)
	{
		$datitos = array(
				"sitio" => $datos["sitio"],
				"tecnologia" => $datos["energizacion_controlador_rectif"]
			);

		$resultado = $this->db->insert('tb_ss_tecnologias',$datitos);

		if($resultado)
		{
			$this->db->insert('db_portal.tb_controlador_rectif_tiempo_activacion',array("controlador_rectif" => $datos["id"]));
		}
		
		return $resp;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$resp = $this->db->delete('tb_controlador_rectif');

		if($resp)
		{
			$this->db->delete('tb_ss_tecnologias',array("id" => $id));
			$this->db->delete('db_portal.tb_tiempo_activacion',array("ss_tecnologia" => $id));
		}

		return $resp;
	}

	public function get_sitio_rectificador($id)
	{
		$this->db->select("ss.id, ss.nombre_completo");
		$this->db->join("tb_status_site ss","ss.id = cr.sitio","left");
		$sitio = $this->db->get_where("tb_controlador_rectif cr", array("cr.id" => $id));
		return $sitio->row_array();
	}
}