<?php
class Tipo_modulos_rectif_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$this->db->order_by("modelo","ASC");
		$types = $this->db->get("tb_tipo_modulos_rectif");
		return $types->result_array();
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_tipo_modulos_rectif",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$antena = $this->db->get_where("tb_tipo_modulos_rectif",array('id' => $id),1);
		return $antena->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_modulos_rectif');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_tipo_modulos_rectif',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_modulos_rectif');
	}
}