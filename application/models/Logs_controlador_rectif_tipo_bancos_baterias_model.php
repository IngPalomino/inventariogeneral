<?php
class Logs_controlador_rectif_tipo_bancos_baterias_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}

	public function get_logs()
	{
		$logs = $this->db->get("tb_logs_controlador_rectif_tipo_bancos_baterias");
		return $logs->result_array();
	}

	public function ingreso_de_datos($user = FALSE,$datos)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		$datos["tipo"]	= "INSERT";
		$datos["id"]	= $this->db->insert_id();
		$datos["query"]	= $this->db->last_query();

		$data = array(
				"usuario"	=> $user,
				"banco_bateria"	=> $datos["id"],
				"evento"	=> json_encode($datos)
			);

		$this->db->insert("tb_logs_controlador_rectif_tipo_bancos_baterias",$data);
	}

	public function actualizacion_de_datos($user = FALSE,$id,$datos,$comentario = FALSE)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		if($comentario === FALSE)
		{
			$comentario = "";
		}

		$datos["tipo"] = "UPDATE";
		$datos["query"]	= $this->db->last_query();

		$data = array(
				"usuario"	=> $user,
				"banco_bateria"	=> $id,
				"evento"	=> json_encode($datos),
				"comentario"=> $comentario
			);

		$this->db->insert("tb_logs_controlador_rectif_tipo_bancos_baterias",$data);
		return json_encode($data);
	}
}
?>