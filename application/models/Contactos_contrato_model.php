<?php
class Contactos_contrato_model extends CI_Model {

	public function __construct()
	{
		require_once("Populate.php");
		$this->load->database();
		$this->populate = new Populate();
	}

	public function get_contratos()
	{
		$this->db->order_by("contacto","ASC");
		$contactos = $this->db->get("tb_contactos_contrato");
		$contactos_array = $contactos->result_array();

		/*$contactos_array = $this->populate->populate("tb_contactos_contrato",$contactos_array,true);*/

		return $contactos_array;
	}

	public function look_for_contratos($busqueda)
	{
		$this->db->order_by("contacto","ASC");

		$this->db->where($busqueda);
		$contactos = $this->db->get("tb_contactos_contrato");
		$contactos_array = $contactos->result_array();

		/*$contactos_array = $this->populate->populate("tb_contactos_contrato",$contactos_array,true);*/

		return $contactos_array;
	}

	public function edit($id)
	{
		$contacto = $this->db->get_where("tb_contactos_contrato",array('id' => $id),1);
		$contacto_array = $contacto->result_array();

		/*$contacto_array = $this->populate->populate("tb_contactos_contrato",$contacto_array,true,true);*/

		return $contacto_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_contactos_contrato');

		return $resp;
	}

	public function insert($datos)
	{
		$resp = $this->db->insert('tb_contactos_contrato',$datos);

		return $resp;
	}

	/*public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_contactos_contrato');
	}*/

	public function delete_where($parametros)
	{
		foreach($parametros as $campo => $valor)
		{
			$this->db->where($campo,$valor);
		}

		return $this->db->delete("tb_contactos_contrato");
	}
}