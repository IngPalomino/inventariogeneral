<?php
class Tipo_idu_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$listado = array(
				"marcas" => array(),
				"modelos" => array()
			);
		
		$this->db->select("marca");
		$this->db->group_by("marca");
		$marcas = $this->db->get("tb_tipo_idu");
		
		$listado["marcas"] = $marcas->result_array();
		
		$this->db->reset_query();
		$this->db->order_by("modelo");
		$modelos = $this->db->get("tb_tipo_idu");
		
		$listado["modelos"] = $modelos->result_array();
		
		return $listado;
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_tipo_idu",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$idu = $this->db->get_where("tb_tipo_idu",array('id' => $id),1);
		return $idu->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_idu');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_tipo_idu',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_idu');
	}
}