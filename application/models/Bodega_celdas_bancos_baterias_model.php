<?php
class Bodega_celdas_bancos_baterias_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function edit($id)
	{
		$celda = $this->db->get_where("tb_bodega_celdas_bancos_baterias",array('id' => $id),1);
		$celda_array = $celda->result_array();
		$celda_array = $this->Populate_model->populate("tb_bodega_celdas_bancos_baterias", $celda_array, true, true);
		return $celda_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_bodega_celdas_bancos_baterias');
		return $resp;
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_bodega_celdas_bancos_baterias', $datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		return $this->db->delete('tb_bodega_celdas_bancos_baterias');
	}
}