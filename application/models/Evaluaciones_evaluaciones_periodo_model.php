<?php
class Evaluaciones_evaluaciones_periodo_model extends CI_Model {
	public function __construct()
	{
		$this->load->database("evaluaciones");
	}

	public function listar()
	{
		$this->db->order_by("id","ASC");
		$query = $this->db->get("tb_evaluaciones_periodo");
		$resultado_array = $query->result_array();

		/*$resultado_array = $this->populate("tb_status_site",$resultado_array,true);*/

		return $resultado_array;
	}

	public function editar($id)
	{
		$query = $this->db->get_where("tb_evaluaciones_periodo",array('id' => $id),1);
		$resultado_array = $query->result_array();

		/*$resultado_array = $this->populate("tb_status_site",$resultado_array,true);*/

		return $resultado_array;
	}

	public function actualizar($id,$info)
	{

	}

	public function insertar($info)
	{
		$resp = $this->db->insert('tb_evaluaciones_periodo',$info);

		$last_id = $this->db->insert_id();

		$salida = array(
				"resp"		=>	$resp,
				"last_id"	=>	$last_id
			);

		return $salida;
	}

	public function nueva_evaluacion($id_eva)
	{
		$resp = $this->db->insert('tb_evaluaciones_periodo',array("evaluacion" => $id_eva));

		$last_id = $this->db->insert_id();

		$respuesta = array(
				"resp"		=>	$resp,
				"last_id"	=>	$last_id
			);

		return $respuesta;
	}

	public function listar_pendientes()
	{
		$this->db->where(array('finalizado' => 0));
		$resp = $this->db->get('tb_evaluaciones_periodo');
		$resp = $resp->result_array();

		foreach ($resp as &$evaluacion)
		{
			$eva = $this->db->get_where('tb_evaluacion',array('id' => $evaluacion['evaluacion']));
			$eva = $eva->row_array();

			$evaluados = $this->db->get_where('tb_avance_evaluacion',array('evaluacion' => $evaluacion['evaluacion']));
			$evaluados = $evaluados->result_array();

			$evaluacion['evaluacion'] = $eva;
			$evaluacion['evaluados'] = $evaluados;
		}

		return $resp;
	}

	public function listar_anios($evaluacion)
	{
		$this->db->select("YEAR(created_at) anio");
		$this->db->where(array('evaluacion' => $evaluacion));
		$this->db->group_by("anio");
		$this->db->order_by("anio","ASC");
		$query = $this->db->get("tb_evaluaciones_periodo");
		$resultado_array = $query->result_array();

		/*$resultado_array = $this->populate("tb_status_site",$resultado_array,true);*/

		return $resultado_array;
	}

	public function listar_periodos($evaluacion,$anio)
	{
		$this->db->select("id,num_periodo");
		$this->db->where(array('evaluacion' => $evaluacion, "YEAR(created_at)" => $anio));
		$this->db->order_by("num_periodo","ASC");
		$query = $this->db->get("tb_evaluaciones_periodo");
		$resultado_array = $query->result_array();

		/*$resultado_array = $this->populate("tb_status_site",$resultado_array,true);*/

		return $resultado_array;
	}

	public function info_avance_desempeno($id)
	{
		$this->db->select("ep.id, ep.evaluacion, ep.num_periodo, ep.created_at, e.nombre, e.periodo, e.contrata");
		$this->db->join("tb_evaluacion e","e.id = ep.evaluacion");
		$query = $this->db->get("tb_evaluaciones_periodo ep");
		$resultado_array = $query->result_array();

		// Listado de contratas
		$contratas = json_decode($resultado_array[0]["contrata"]);
		$indContrata = array();

		foreach ($contratas as $contrata)
		{
			$query2 = $this->db->get_where("tb_contrata",array("id" => $contrata));

			array_push($indContrata, $query2->row_array());
		}

		$resultado_array[0]["contrata"] = $indContrata;

		// Fin Listado de contratas

		// Listado de técnico
		$resultado_array[0]["avance_evaluacion"] = array();

		$this->db->select("ae.id, ae.tecnico_contrata, ae.evaluacion, ae.evaluacion_periodo, ae.resultado, ae.ejecutado, ae.evaluador, u.nombres evaluador_nombres, u.apellido_paterno evaluador_apellido_paterno, u.apellido_materno evaluador_apellido_materno, ae.fecha_evaluacion, tc.nombre, tc.apellidos, tc.contrata, tc.zona_texto, tt.tipo_tecnico");
		$this->db->join("tb_tecnicos_contrata tc","tc.id = ae.tecnico_contrata");
		$this->db->join("tb_tipo_tecnico tt","tt.id = tc.tipo_tecnico");
		$this->db->join("db_oym_inventario_general.tb_usuarios u","u.id_usuario = ae.evaluador","left");
		$this->db->where(array("ae.evaluacion_periodo" => $resultado_array[0]["id"]));
		$this->db->order_by("nombre","ASC");
		$this->db->order_by("apellidos","ASC");
		$query3 = $this->db->get("tb_avance_evaluacion ae");
		$avance_array = $query3->result_array();

		$resultado_array[0]["avance_evaluacion"] = $avance_array;
		// Fin de Listado de técnicos

		// Listado de evaluadores
		$resultado_array[0]["evaluadores"] = array();

		$this->db->select("ae.evaluador, u.nombres, u.apellido_paterno, u.apellido_materno");
		$this->db->join("db_oym_inventario_general.tb_usuarios u","u.id_usuario = ae.evaluador","left");
		$this->db->where("ae.evaluador !=", 0);
		$this->db->group_by("nombres","ASC");
		$this->db->order_by("nombres","ASC");
		$this->db->order_by("apellido_paterno","ASC");
		$this->db->order_by("apellido_materno","ASC");
		$query4 = $this->db->get("tb_avance_evaluacion ae");
		$evaluadores = $query4->result_array();

		$resultado_array[0]["evaluadores"] = $evaluadores;
		// Fin de Listado de evaluadores

		return $resultado_array;
	}
}
?>