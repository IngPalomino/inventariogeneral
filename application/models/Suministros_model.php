<?php
class Suministros_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function get_suministros()
	{
		$this->db->order_by("id","ASC");
		$suministros = $this->db->get("tb_suministros");
		return $suministros->result_array();
	}

	public function get_suministros_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$suministros = $this->db->get_where("tb_suministros",array('sitio' => $id, 'eliminado' => 0));

		$suministros_array = $suministros->result_array();

		$suministros_array = $this->Populate_model->populate("tb_suministros",$suministros_array,true);

		return $suministros_array;
	}

	public function edit($id)
	{
		$suministro = $this->db->get_where("tb_suministros",array('id' => $id),1);
		$suministro_array = $suministro->result_array();

		$suministro_array = $this->Populate_model->populate("tb_suministros",$suministro_array,true,true);

		return $suministro_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_suministros');
		return $resp;
	}

	public function insert($datos)
	{
		$resultado = $this->db->insert('tb_suministros',$datos);

		$resp = array(
			"resp" => $resultado,
			"last_id" => $this->db->insert_id()
		);
		
		return $resp;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_suministros');
	}

	public function get_sitio_suministro($id)
	{
		$this->db->select("ss.id, ss.nombre_completo");
		$this->db->join("tb_status_site ss","ss.id = aa.sitio","left");
		$sitio = $this->db->get_where("tb_suministros aa", array("aa.id" => $id), 1);
		return $sitio->row_array();
	}
}