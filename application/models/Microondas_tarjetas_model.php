<?php
class Microondas_tarjetas_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_tarjetas()
	{
		$this->db->order_by("id","ASC");
		$tarjetas = $this->db->get("tb_mw_tarjetas");
		return $tarjetas->result_array();
	}

	public function get_tarjetas_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$this->db->select("i.sitio,t.*");
		$this->db->join("tb_idus i","i.id = t.idu","left");
		$tarjetas = $this->db->get_where("tb_mw_tarjetas t",array('i.sitio' => $id, 'i.eliminado' => 0));
		$tarjetas_array = $tarjetas->result_array();
		
		foreach($tarjetas_array as &$tarjeta)
		{
			$this->db->select("id,ne_id");
			$ne_id = $this->db->get_where("tb_idus",array("id" => $tarjeta["idu"]));
			$tarjeta["idu"] = $ne_id->row_array();
		}
		
		return $tarjetas_array;
	}

	public function get_tarjetas_idu($id, $like, $slot)
	{
		$this->db->order_by("id","ASC");
		$this->db->like("board_type", $like);
		$tarjetas = $this->db->get_where("tb_mw_tarjetas",array('idu' => $id, 'slot_id' => $slot), 1);
		$tarjetas_array = $tarjetas->result_array();
	
		return $tarjetas_array;
	}

	public function get_puertos_idu($where, $port_number)
	{
		$where = urldecode($where);
		$this->db->select("i.*, ps.*");
		$this->db->join("tb_puertos_servicios ps", "ps.ne_name = i.link_name", "left");
		$query = $this->db->get_where("tb_idus i", array('ps.port_name IS NOT' => NULL, "i.link_name" => $where, "ps.port_type" => "Ethernet", "ps.port_number" => $port_number, "i.eliminado" => 0));
		return $query->result_array();
	}
	
	public function get_idu_id($id)
	{
		$idu = $this->db->get_where("tb_idus",array("ne_id" => $id));
		return $idu->row_array();
	}
	
	public function get_parts()
	{
		$rowsNumber = $this->db->count_all_results('tb_mw_tarjetas');
		
		return $rowsNumber;
	}

	public function edit($id)
	{
		$tarjeta = $this->db->get_where("tb_mw_tarjetas",array('id' => $id),1);
		$tarjeta_array = $tarjeta->result_array();
		return $tarjeta_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_mw_tarjetas');
		return $resp;
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_mw_tarjetas',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_mw_tarjetas');
	}
}