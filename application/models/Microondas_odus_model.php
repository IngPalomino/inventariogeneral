<?php
class Microondas_odus_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_odus()
	{
		$this->db->order_by("id","ASC");
		$odus = $this->db->get("tb_mw_odus");
		return $odus->result_array();
	}

	public function get_odus_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$odus = $this->db->get_where("tb_mw_odus",array('sitio' => $id));
		$odus_array = $odus->result_array();
		return $odus_array;
	}

	public function edit($id)
	{
		$odu = $this->db->get_where("tb_mw_odus",array('id' => $id),1);
		$odu_array = $odu->result_array();
		return $odu_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_mw_odus');
		return $resp;
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_mw_odus',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_mw_odus');
	}
}