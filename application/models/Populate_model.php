<?php
class Populate_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}
	
	public function populate($table, $info_array, $is_array, $is_edit = FALSE)
	{
		if((gettype($table) != "string") && (gettype($info_array) != "array") && (gettype($is_array) != "boolean"))
		{
			return FALSE;
		}

		$relaciones = $this->db->get_where("tb_relaciones",array("tabla_primaria" => $table));
		$relaciones = $relaciones->result_array();

		if($is_array)
		{	
			foreach($info_array as &$info)
			{
				foreach($relaciones as $relacion)
				{
					if($relacion["relacion"] == "1:1")
					{
						$datos = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $info[$relacion["columna_primaria"]]),1);
						$datos = $datos->row_array();
						$datos = $this->populate($relacion["tabla_secundaria"], $datos, FALSE);
						$info[$relacion["columna_primaria"]] = $datos;
					}
					elseif($relacion["relacion"] == "1:N")
					{
						$pos = strpos($relacion["tabla_secundaria"], "logs");
						
						if($pos !== FALSE && $is_edit === FALSE)
						{
							$this->db->select("id,usuario,evento,created_at");
							$this->db->order_by("id", "DESC");
							$datos = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $info[$relacion["columna_primaria"]]),1);
							$datos = $datos->row_array();
							
							$this->db->select("id,usuario,evento,created_at");
							$this->db->order_by("id", "ASC");
							$datoss = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $info[$relacion["columna_primaria"]]),1);
							$datoss = $datoss->row_array();
							
							$datos_array = array();
							!$datoss? : array_push($datos_array,$datoss);
							!$datos? : array_push($datos_array,$datos);
							
							$exp = explode("tb_",$relacion["tabla_secundaria"]);
							$info[$exp[1]] = $datos_array;
							
							foreach($info[$exp[1]] as &$log)
							{
								if($log["evento"])
								{
									$log["evento"] = json_decode($log["evento"],true);
									unset($log["evento"]["query"]);
									unset($log["evento"]["datos viejos"]);
									unset($log["evento"]["datos nuevos"]);
								}
								
								if($log["usuario"])
								{
									$this->db->select("id_usuario,nombres,apellido_paterno");
									$usuario = $this->db->get_where("tb_usuarios",array('id_usuario' => $log["usuario"]));
									$log["usuario"] = $usuario->row_array();
								}
							}
						}
						elseif($pos !== false && $is_edit !== false) 
						{
							//Omite logs cuando se usa el método edit en un modelo.
						}
						else
						{
							if(in_array("eliminado", $this->db->list_fields($relacion["tabla_secundaria"])))
							{
								$this->db->where("eliminado", 0);
							}

							$datos = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $info[$relacion["columna_primaria"]]));
							$datos = $datos->result_array();
							$datos = $this->populate($relacion["tabla_secundaria"],$datos,true);
							$exp = explode("tb_",$relacion["tabla_secundaria"]);
							$info[$exp[1]] = $datos;
						}
					}
					elseif($relacion["relacion"] == "N:N")
					{
						$tabla_relacion = $relacion["tabla_primaria"] . "_" . substr($relacion["tabla_secundaria"],3);
						$campo_primaria = substr($relacion["tabla_primaria"],3);
						$campo_secundaria = substr($relacion["tabla_secundaria"],3);

						$id_primaria = $info[$relacion["columna_primaria"]];

						$datos = $this->db->get_where($tabla_relacion,array($campo_primaria => $id_primaria));
						$datos = $datos->result_array();

						$this->db->reset_query();
						
						$datos = $this->populate($tabla_relacion,$datos,true);
						
						$info[substr($tabla_relacion,3)] = $datos;
					}
				}
			}
		}
		else
		{
			foreach($relaciones as $relacion)
			{
				if($relacion["relacion"] == "1:1")
				{
					$datos = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $info_array[$relacion["columna_primaria"]]),1);
					$datos = $datos->row_array();
					$datos = $this->populate($relacion["tabla_secundaria"],$datos,false);
					$info_array[$relacion["columna_primaria"]] = $datos;
				}
				elseif($relacion["relacion"] == "1:N")
				{
					$pos = strpos($relacion["tabla_secundaria"], "logs");
					
					if($pos !== false && $is_edit === false)
					{
						$this->db->select("id,usuario,evento,created_at");
						$this->db->order_by("id", "DESC");
						$datos = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $info[$relacion["columna_primaria"]]),1);
						$datos = $datos->row_array();
						
						$this->db->select("id,usuario,evento,created_at");
						$this->db->order_by("id", "ASC");
						$datoss = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $info[$relacion["columna_primaria"]]),1);
						$datoss = $datoss->row_array();
						
						$datos_array = array();
						!$datoss? : array_push($datos_array,$datoss);
						!$datos? : array_push($datos_array,$datos);
						
						$exp = explode("tb_",$relacion["tabla_secundaria"]);
						$info[$exp[1]] = $datos_array;
						
						foreach($info[$exp[1]] as &$log)
						{
							if($log["evento"])
							{
								$log["evento"] = json_decode($log["evento"],true);
								unset($log["evento"]["query"]);
								unset($log["evento"]["datos viejos"]);
								unset($log["evento"]["datos nuevos"]);
							}
							
							if($log["usuario"])
							{
								$this->db->select("id_usuario,nombres,apellido_paterno");
								$usuario = $this->db->get_where("tb_usuarios",array('id_usuario' => $log["usuario"]));
								$log["usuario"] = $usuario->row_array();
							}
						}
					}
					elseif($pos !== false && $is_edit !== false) 
					{
						//Omite logs cuando se usa el método edit en un modelo.
					}
					else
					{
						if(in_array("eliminado", $this->db->list_fields($relacion["tabla_secundaria"])))
						{
							$this->db->where("eliminado", 0);
						}

						$datos = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $info[$relacion["columna_primaria"]]));
						$datos = $datos->result_array();
						$datos = $this->populate($relacion["tabla_secundaria"],$datos,true);
						$exp = explode("tb_",$relacion["tabla_secundaria"]);
						$info[$exp[1]] = $datos;
					}
				}
				elseif($relacion["relacion"] == "N:N")
				{
					$tabla_relacion = $relacion["tabla_primaria"] . "_" . substr($relacion["tabla_secundaria"],3);
					$campo_primaria = substr($relacion["tabla_primaria"],3);
					$campo_secundaria = substr($relacion["tabla_secundaria"],3);

					$id_primaria = $info[$relacion["columna_primaria"]];

					$datos = $this->db->get_where($tabla_relacion,array($campo_primaria => $id_primaria));
					$datos = $datos->result_array();

					$this->db->reset_query();
					
					$datos = $this->populate($tabla_relacion,$datos,true);
					
					$info[substr($tabla_relacion,3)] = $datos;
				}
			}
		}

		return $info_array;
	}
}