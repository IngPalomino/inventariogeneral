<?php
class E_gabinetes_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_gabinetes()
	{
		$this->db->order_by("id","ASC");
		$gabinetes = $this->db->get("tb_e_gabinetes");
		return $gabinetes->result_array();
	}

	public function get_gabinetes_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$gabinetes = $this->db->get_where("tb_e_gabinetes",array('sitio' => $id));
		$gabinetes_array = $gabinetes->result_array();
		return $gabinetes_array;
	}

	public function edit($id)
	{
		$gabinete = $this->db->get_where("tb_e_gabinetes",array('id' => $id),1);
		$gabinete_array = $gabinete->result_array();
		return $gabinete_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_e_gabinetes');
		return $resp;
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_e_gabinetes',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_e_gabinetes');
	}
}