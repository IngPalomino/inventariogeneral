<?php
class Sesion_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function confirmar_inicio($usuario = FALSE, $contrasena = FALSE)
	{
		if( ($usuario === FALSE) || ($contrasena === FALSE) )
		{
			return FALSE;
		}

		$patron1 = "/([\w.]+)/";
		$patron2 = "/([\w.]+)@osctelecoms.com([A-Za-z.]{0,4})/";
		$patron3 = "/([\w.]+)@entel.pe/";
		$coincidencias = array();

		$esCorreo = strpos($usuario, "@");

		if( ($esCorreo != false) && ($esCorreo > 0) )
		{
			if(preg_match_all($patron2, $usuario, $coincidencias) || preg_match_all($patron3, $usuario, $coincidencias))
			{
				$busqueda = $coincidencias[1][0]."@";
			}
			else
			{
				return false;
			}
		}
		else
		{
			if(preg_match_all($patron1, $usuario, $coincidencias))
			{
				$busqueda = $coincidencias[1][0]."@";
			}
			else
			{
				return false;
			}
		}

		$usuario = $this->db->select("correo,contrasena")
					->like("correo",$busqueda,"after")
					->get("tb_usuarios");
		$usuario = $usuario->row_array();

		if(password_verify($contrasena, $usuario["contrasena"]))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}