<?php
class Enlaces_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function get_enlaces()
	{
		$this->db->order_by("id","ASC");
		$enlaces = $this->db->get("tb_enlaces");
		return $enlaces->result_array();
	}

	public function get_enlaces_sitio($id)
	{
		$this->db->order_by("i.ne_id","ASC");
		$this->db->select("i.sitio,e.*");
		$this->db->join("tb_idus i","e.idu_1 = i.id OR e.idu_2 = i.id","left");
		$enlaces = $this->db->get_where("tb_enlaces e",array('i.sitio' => $id, 'e.eliminado' => 0));
		$enlaces_array = $enlaces->result_array();
		$enlaces_array = $this->Populate_model->populate("tb_enlaces",$enlaces_array,true);
		
		foreach($enlaces_array as &$enlace)
		{
			$this->db->select("id,sitio,ne_id, idu_name");
			$idu_1 = $this->db->get_where("tb_idus",array('id' => $enlace["idu_1"], 'eliminado' => 0));
			$enlace["idu_1"] = $idu_1->row_array();
			
			$this->db->reset_query();
			$this->db->select("id,codigo,nombre_completo");
			$nombreSitio1 = $this->db->get_where("tb_status_site",array('id' => $enlace["idu_1"]["sitio"]));
			$enlace["idu_1"]["sitio"] = $nombreSitio1->row_array();
			
			$this->db->reset_query();
			$this->db->select("id,sitio,ne_id, idu_name");
			$idu_2 = $this->db->get_where("tb_idus",array('id' => $enlace["idu_2"], 'eliminado' => 0));
			$enlace["idu_2"] = $idu_2->row_array();
			
			$this->db->reset_query();
			$this->db->select("id,codigo,nombre_completo");
			$nombreSitio2 = $this->db->get_where("tb_status_site",array('id' => $enlace["idu_2"]["sitio"]));
			$enlace["idu_2"]["sitio"] = $nombreSitio2->row_array();
			
			$idu2Sitio = $enlace["idu_2"]["sitio"];
			
			if($enlace["sitio"] == $idu2Sitio["id"])
			{
				$temp = $enlace["idu_1"];
				$enlace["idu_1"] = $enlace["idu_2"];
				$enlace["idu_2"] = $temp;
			}
		}
		
		return $enlaces_array;
	}
	
	public function edit($id)
	{
		$enlaces = $this->db->get_where("tb_enlaces e",array('e.id' => $id));
		$enlaces_array = $enlaces->result_array();
		$enlaces_array = $this->Populate_model->populate("tb_enlaces",$enlaces_array,true,true);
		return $enlaces_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_enlaces');
		return $resp;
	}

	public function update_or_where($parametros,$datos)
	{
		$this->db->set($datos);
		foreach($parametros as $campo => $valor)
		{
			if(count($parametros) == 1)
			{
				$this->db->where($campo,$valor);
			}
			elseif(count($parametros) > 1)
			{
				$this->db->or_where($campo,$valor);
			}
		}
		$resp = $this->db->update('tb_enlaces');
		return $resp;
	}

	public function insert($datos)
	{
		$resultado = $this->db->insert('tb_enlaces',$datos);
		$resp = array(
			"resp" => $resultado,
			"last_id" => $this->db->insert_id()
		);
		return $resp;
	}
	
	public function get_sitios($id)
	{
		$this->db->select("id,nombre_completo");
		$this->db->order_by("codigo");
		$sitios = $this->db->get_where("tb_status_site",array('id !=' => $id));
		return $sitios->result_array();
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_enlaces');
	}

	public function get_enlaces_sitio_coor($id)
	{
		$this->db->order_by("e.id","ASC");
		$this->db->select("i.sitio,e.*");
		$this->db->join("tb_idus i","e.idu_1 = i.id OR e.idu_2 = i.id","left");
		$enlaces = $this->db->get_where("tb_enlaces e",array('i.sitio' => $id, 'e.eliminado' => 0));
		$enlaces_array = $enlaces->result_array();
		
		foreach($enlaces_array as &$enlace)
		{
			$this->db->select("id,sitio,ne_id");
			$idu_1 = $this->db->get_where("tb_idus",array('id' => $enlace["idu_1"], 'eliminado' => 0));
			$enlace["idu_1"] = $idu_1->row_array();
			
			$this->db->reset_query();
			$this->db->select("id,codigo,nombre_completo,latitud,longitud");
			$nombreSitio1 = $this->db->get_where("tb_status_site",array('id' => $enlace["idu_1"]["sitio"]));
			$enlace["idu_1"]["sitio"] = $nombreSitio1->row_array();
			
			$this->db->reset_query();
			$this->db->select("id,sitio,ne_id");
			$idu_2 = $this->db->get_where("tb_idus",array('id' => $enlace["idu_2"], 'eliminado' => 0));
			$enlace["idu_2"] = $idu_2->row_array();
			
			$this->db->reset_query();
			$this->db->select("id,codigo,nombre_completo,latitud,longitud");
			$nombreSitio2 = $this->db->get_where("tb_status_site",array('id' => $enlace["idu_2"]["sitio"]));
			$enlace["idu_2"]["sitio"] = $nombreSitio2->row_array();
			
			$idu2Sitio = $enlace["idu_2"]["sitio"];
			
			if($enlace["sitio"] == $idu2Sitio["id"])
			{
				$temp = $enlace["idu_1"];
				$enlace["idu_1"] = $enlace["idu_2"];
				$enlace["idu_2"] = $temp;
			}
		}
		
		return $enlaces_array;
	}

	public function get_sitios_enlace($id)
	{
		$this->db->select("ss.id, ss.nombre_completo");
		$this->db->join("tb_idus i","i.id = e.idu_1 OR i.id = e.idu_2","left");
		$this->db->join("tb_status_site ss","ss.id = i.sitio","left");
		$sitios = $this->db->get_where("tb_enlaces e", array("e.id" => $id), 2);
		return $sitios->result_array();
	}


	
}