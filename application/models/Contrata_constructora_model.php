<?php
class Contrata_constructora_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_Proyecto()
	{
		$this->db->order_by("id","ASC");
		$contratas_constructoras = $this->db->get("tb_contrata_constructora");
		return $contratas_constructoras->result_array();
	}

	public function edit($id)
	{
		$contrata_constructora = $this->db->get_where("tb_contrata_constructora",array('id' => $id),1);
		return $contrata_constructora->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_contrata_constructora');
	}

	public function insert($datos)
	{
		$this->db->insert('tb_contrata_constructora',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_contrata_constructora');
	}
}