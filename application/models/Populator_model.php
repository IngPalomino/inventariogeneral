<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Populator_model extends CI_Model {

	protected $table = NULL;
	protected $response = NULL;
	protected $answer = NULL;
	public $_where = array();
	public $_select = array();
	public $_join = array();
	public $_like = array();
	public $_limit = NULL;
	public $_offset = NULL;
	public $_order_by = NULL;
	public $_order = NULL;

	public function __construct()
	{
		$this->load->database();
	}

	public function where($where, $value = NULL)
	{
		if ( ! is_array($where))
		{
			$where = array($where => $value);
		}

		array_push($this->_where, $where);

		return $this;
	}

	public function select($select)
	{
		$this->_select[] = $select;

		return $this;
	}

	public function join($table, $on, $type = 'left')
	{
		array_push($this->_join, array($table, $on, $type));

		return $this;
	}

	public function like($like, $value = NULL, $position = 'both')
	{
		array_push($this->_like, array(
			'like' => $like,
			'value' => $value,
			'position' => $position
		));

		return $this;
	}

	public function limit($limit)
	{
		$this->_limit = $limit;

		return $this;
	}

	public function offset($offset)
	{
		$this->_offset = $offset;

		return $this;
	}

	public function order_by($by, $order = 'DESC')
	{
		$this->_order_by = $by;
		$this->_order = $order;

		return $this;
	}

	public function row($return = FALSE)
	{
		$this->answer = $this->response->row();

		return $return ? $this->answer : $this;
	}

	public function row_array($return = FALSE)
	{
		$this->answer = $this->response->row_array();

		return $return ? $this->answer : $this;
	}

	public function result($return = FALSE)
	{
		$this->answer = $this->response->result();

		return $return ? $this->answer : $this;
	}

	public function result_array($return = FALSE)
	{
		$this->answer = $this->response->result_array();

		return $return ? $this->answer : $this;
	}

	public function num_rows()
	{
		$result = $this->response->num_rows();

		return $result;
	}

	public function get($table)
	{
		$this->table = $table;

		if (isset($this->_select) && ! empty($this->_select))
		{
			foreach ($this->_select as $select)
			{
				$this->db->select($select);
			}

			$this->_select = array();
		}

		if (isset($this->_join) && ! empty($this->_join))
		{
			foreach ($this->_join as $join)
			{
				$this->db->join(join(', ', $join));
			}

			$this->_join = array();
		}

		if (isset($this->_where) && ! empty($this->_where))
		{
			foreach ($this->_where as $where)
			{
				$this->db->where($where);
			}

			$this->_where = array();
		}

		if (isset($this->_like) && ! empty($this->_like))
		{
			foreach ($this->_like as $like)
			{
				$this->db->or_like($like['like'], $like['value'], $like['position']);
			}

			$this->_like = array();
		}

		if (isset($this->_limit) && isset($this->_offset))
		{
			$this->db->limit($this->_limit, $this->_offset);

			$this->_limit = NULL;
			$this->_offset = NULL;
		}
		else if (isset($this->_limit))
		{
			$this->db->limit($this->_limit);

			$this->_limit = NULL;
		}

		if (isset($this->_order_by) && isset($this->_order))
		{
			$this->db->order_by($this->_order_by, $this->_order);

			$this->_order = NULL;
			$this->_order_by = NULL;
		}

		$this->response = $this->db->get($this->table);

		return $this;
	}

	public function populate($tabla = '', $info_array = array(), $omit_logs = FALSE)
	{
		$tabla = empty($tabla) ? $this->table : $tabla;
		$info_array = empty($info_array) ? $this->answer : $info_array;

		if (gettype($tabla) != 'string' && gettype($info_array) != 'array')
		{
			return FALSE;
		}

		$relaciones = $this->db->get_where('tb_relaciones', array('tabla_primaria' => $tabla))
								->result_array();

		if (is_int(array_keys($info_array)[0]))
		{
			foreach ($info_array as &$info)
			{
				foreach ($relaciones as $relacion)
				{
					if ($relacion['relacion'] == '1:1')
					{
						$datos = $this->db->get_where($relacion['tabla_secundaria'], array($relacion['columna_secundaria'] => $info[$relacion['columna_primaria']]), 1)
											->row_array();
						$info[$relacion['columna_primaria']] = ($datos ? $this->populate($relacion['tabla_secundaria'], $datos) : NULL);
					}
					elseif ($relacion['relacion'] == '1:N')
					{
						$pos = strpos($relacion['tabla_secundaria'], '_logs_');

						if ($pos !== FALSE && $omit_logs !== FALSE)
						{
							//Omite logs cuando es Populate es usado en edit en un modelo.
						}
						elseif ($pos !== FALSE && $omit_logs === FALSE)
						{
							$datos_array = array();

							$datos = $this->db->select('id, usuario, evento, created_at')
												->order_by('id', 'DESC')
												->get_where($relacion['tabla_secundaria'], array($relacion['columna_secundaria'] => $info[$relacion['columna_primaria']]), 1)
												->row_array();
							array_push($datos_array, $datos);

							$datos = $this->db->select('id, usuario, evento, created_at')
												->order_by('id', 'ASC')
												->get_where($relacion['tabla_secundaria'], array($relacion['columna_secundaria'] => $info[$relacion['columna_primaria']]), 1)
												->row_array();
							array_push($datos_array, $datos);

							foreach ($datos_array as &$log)
							{
								if (isset($log['evento']))
								{
									$log['evento'] = json_decode($log['evento'], TRUE);
									unset($log['evento']['query']);
									unset($log['evento']['datos viejos']);
									unset($log['evento']['datos nuevos']);
								}

								if (isset($log['usuario']))
								{
									$log['usuario'] = $this->db->select('id_usuario, nombres, apellido_paterno')
																->get_where('tb_usuarios', array('id_usuario' => $log['usuario']))
																->row_array();
								}
							}

							$exp = explode('tb_', $relacion['tabla_secundaria'])[1];
							$info[$exp] = $datos_array;
						}
						else
						{
							if (in_array('eliminado', $this->db->list_fields($relacion['tabla_secundaria'])))
							{
								$this->db->where('eliminado', 0);
							}

							$datos = $this->db->get_where($relacion['tabla_secundaria'], array($relacion['columna_secundaria'] => $info[$relacion['columna_primaria']]))
												->result_array();
							$exp = explode('tb_', $relacion['tabla_secundaria'])[1];

							$info[$exp] = ($datos ? $this->populate($relacion['tabla_secundaria'], $datos) : array());
						}
					}
					elseif ($relacion['relacion'] == 'N:N')
					{
						$tabla_relacion = $relacion['tabla_primaria'] . '_' . substr($relacion['tabla_secundaria'], 3);
						$campo_primaria = substr($relacion['tabla_primaria'], 3, -1);
						$campo_secundaria = substr($relacion['tabla_secundaria'], 3, -1);

						$datos = $this->db->select($relacion['tabla_secundaria'].'.*')
											->join($relacion['tabla_secundaria'], $relacion['tabla_secundaria'].'.'.$relacion['columna_secundaria'].' = '.$tabla_relacion.'.'.$campo_secundaria, 'left')
											->get_where($tabla_relacion, array($tabla_relacion.'.'.$campo_primaria => $info[$relacion['columna_primaria']]))
											->result_array();

						$info[substr($tabla_relacion, 3)] = ($datos ? $this->populate($tabla_relacion, $datos) : array());
					}
				}
			}
		}
		else
		{
			foreach ($relaciones as $relacion)
			{
				if ($relacion['relacion'] == '1:1')
				{
					$datos = $this->db->get_where($relacion['tabla_secundaria'], array($relacion['columna_secundaria'] => $info_array[$relacion['columna_primaria']]), 1)
										->row_array();
					$info_array[$relacion['columna_primaria']] = ($datos ? $this->populate($relacion['tabla_secundaria'], $datos) : NULL);
				}
				elseif ($relacion['relacion'] == '1:N')
				{
					$pos = strpos($relacion['tabla_secundaria'], '_logs_');

					if ($pos !== FALSE && $omit_logs !== FALSE)
					{
						//Omite logs cuando es Populate es usado en edit en un modelo.
					}
					elseif ($pos !== FALSE && $omit_logs === FALSE)
					{
						$datos_array = array();

						$datos = $this->db->select('id, usuario, evento, created_at')
											->order_by('id', 'DESC')
											->get_where($relacion['tabla_secundaria'], array($relacion['columna_secundaria'] => $info_array[$relacion['columna_primaria']]), 1)
											->row_array();
						array_push($datos_array, $datos);

						$datos = $this->db->select('id, usuario, evento, created_at')
											->order_by('id', 'ASC')
											->get_where($relacion['tabla_secundaria'], array($relacion['columna_secundaria'] => $info_array[$relacion['columna_primaria']]), 1)
											->row_array();
						array_push($datos_array, $datos);

						foreach ($datos_array as &$log)
						{
							if (isset($log['evento']))
							{
								$log['evento'] = json_decode($log['evento'], TRUE);
								unset($log['evento']['query']);
								unset($log['evento']['datos viejos']);
								unset($log['evento']['datos nuevos']);
							}

							if (isset($log['usuario']))
							{
								$log['usuario'] = $this->db->select('id_usuario, nombres, apellido_paterno')
															->get_where('tb_usuarios', array('id_usuario' => $log['usuario']))
															->row_array();
							}
						}

						$exp = explode('tb_', $relacion['tabla_secundaria'])[1];
						$info_array[$exp] = $datos_array;
					}
					else
					{
						if (in_array('eliminado', $this->db->list_fields($relacion['tabla_secundaria'])))
						{
							$this->db->where('eliminado', 0);
						}

						$datos = $this->db->get_where($relacion['tabla_secundaria'], array($relacion['columna_secundaria'] => $info_array[$relacion['columna_primaria']]))
											->result_array();
						$exp = explode('tb_', $relacion['tabla_secundaria'])[1];

						$info_array[$exp] = ($datos ? $this->populate($relacion['tabla_secundaria'], $datos) : array());
					}
				}
				elseif ($relacion['relacion'] == 'N:N')
				{
					$tabla_relacion = $relacion['tabla_primaria'] . '_' . substr($relacion['tabla_secundaria'], 3);
					$campo_primaria = substr($relacion['tabla_primaria'], 3, -1);
					$campo_secundaria = substr($relacion['tabla_secundaria'], 3, -1);

					$datos = $this->db->select($relacion['tabla_secundaria'].'.*')
										->join($relacion['tabla_secundaria'], $relacion['tabla_secundaria'].'.'.$relacion['columna_secundaria'].' = '.$tabla_relacion.'.'.$campo_secundaria, 'left')
										->get_where($tabla_relacion, array($tabla_relacion.'.'.$campo_primaria => $info_array[$relacion['columna_primaria']]))
										->result_array();

					$info_array[substr($tabla_relacion, 3)] = ($datos ? $this->populate($tabla_relacion, $datos) : array());
				}
			}
		}

		return $info_array;
	}
}