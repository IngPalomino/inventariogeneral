<?php
class Tipo_torre_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_tipo_torre()
	{
		$this->db->order_by("tipo_torre","ASC");
		$tipos = $this->db->get("tb_tipo_torre");
		return $tipos->result_array();
	}

	public function edit($id)
	{
		$tipo = $this->db->get_where("tb_tipo_torre",array('id' => $id),1);
		return $tipo->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_torre');
	}

	public function insert($datos)
	{
		$this->db->insert('tb_tipo_torre',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_torre');
	}
}