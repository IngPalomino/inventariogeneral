<?php
class Fases_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

		public function get_fases()
	{
		$this->db->order_by("id","ASC");
		$types = $this->db->get("tb_fases_subestacion");
		return $types->result_array();
	}

	
}