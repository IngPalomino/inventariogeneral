<?php
class Tarjetas_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_tarjetas()
	{
		$this->db->order_by("id","ASC");
		$tarjetas = $this->db->get("tb_e_tarjetas");
		return $tarjetas->result_array();
	}

	public function get_tarjetas_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$tarjetas = $this->db->get_where("tb_e_tarjetas",array('sitio' => $id));
		$tarjetas_array = $tarjetas->result_array();
		/*$tarjetas_array = $this->populate->populate("tb_e_tarjetas",$tarjetas_array,true);*/
		return $tarjetas_array;
	}

	public function edit($id)
	{
		$tarjeta = $this->db->get_where("tb_e_tarjetas",array('id' => $id),1);
		$tarjeta_array = $tarjeta->result_array();

		/*$tarjeta_array = $this->populate->populate("tb_e_tarjetas",$tarjeta_array,true);*/

		return $tarjeta_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_e_tarjetas');
		return $resp;
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_e_tarjetas',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_e_tarjetas');
	}
}