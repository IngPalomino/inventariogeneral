<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_masivo_model extends CI_Model {

	private $table;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = 'tb_ingreso_masivo';
	}

	public function get_solicitudes()
	{
		$solicitudes = $this->db->order_by('id', 'DESC')
								->get_where($this->table, array('eliminado' => 0))
								->result_array();
		$solicitudes = $this->Populate_model->populate($this->table, $solicitudes, TRUE);

		foreach($solicitudes as &$solicitud)
		{
			$solicitud['usuario'] = $this->db->select('id_usuario, nombres, apellido_paterno')
												->get_where('tb_usuarios', array('id_usuario' => $solicitud['usuario']), 1)
												->row_array();

			$solicitud['pendiente'] = $this->db->select('COUNT(id) pendiente')
												->get_where($solicitud['tabla']['tabla_temporal'], array('ingreso_masivo' => $solicitud['id'], 'estado' => 1, 'eliminado' => 0))
												->row()
												->pendiente;

			$solicitud['validado'] = $this->db->select('COUNT(id) validado')
												->get_where($solicitud['tabla']['tabla_temporal'], array('ingreso_masivo' => $solicitud['id'], 'estado' => 2, 'eliminado' => 0))
												->row()
												->validado;

			$solicitud['rechazado'] = $this->db->select('COUNT(id) rechazado')
												->get_where($solicitud['tabla']['tabla_temporal'], array('ingreso_masivo' => $solicitud['id'], 'estado' => 3, 'eliminado' => 0))
												->row()
												->rechazado;
		}

		return $solicitudes;
	}

	public function get_elemento($elemento)
	{
		$elemento = $this->db->get_where($this->table, array('id' => $elemento), 1)->row_array();
		$elemento = $this->Populate_model->populate($this->table, $elemento, FALSE);

		$elemento['usuario'] = $this->db->select('id_usuario, nombres, apellido_paterno')
										->get_where('tb_usuarios', array('id_usuario' => $elemento['usuario']), 1)
										->row_array();

		$elemento['pendiente'] = $this->db->select('COUNT(id) pendiente')
											->get_where($elemento['tabla']['tabla_temporal'], array('ingreso_masivo' => $elemento['id'], 'estado' => 1, 'eliminado' => 0))
											->row()
											->pendiente;

		$elemento['validado'] = $this->db->select('COUNT(id) validado')
											->get_where($elemento['tabla']['tabla_temporal'], array('ingreso_masivo' => $elemento['id'], 'estado' => 2, 'eliminado' => 0))
											->row()
											->validado;

		$elemento['rechazado'] = $this->db->select('COUNT(id) rechazado')
											->get_where($elemento['tabla']['tabla_temporal'], array('ingreso_masivo' => $elemento['id'], 'estado' => 3, 'eliminado' => 0))
											->row()
											->rechazado;

		return $elemento;
	}

	public function get_filas_pendientes($elemento)
	{
		$solicitud = $this->get_elemento($elemento);

		if ($solicitud["tabla"]["tabla_produccion"] == "tb_controlador_rectif_bancos_baterias")
		{
			$this->db->select("cr.sitio, ecr.energizacion_controlador_rectif, tt.*");
			$this->db->join("tb_controlador_rectif cr", "cr.id = tt.controlador_rectif", "left");
			$this->db->join("tb_energizacion_controlador_rectif ecr", "ecr.id = cr.energizacion_controlador_rectif", "left");
		}

		$filas = $this->db->get_where($solicitud["tabla"]["tabla_temporal"]." tt", array("tt.ingreso_masivo" => $solicitud["id"], "tt.estado" => 1, "tt.eliminado" => 0))
							->result_array();
		$filas = $this->Populate_model->populate($solicitud["tabla"]["tabla_temporal"], $filas, TRUE);

		foreach($filas as &$fila)
		{
			if (isset($fila["temp_contactos_contrato"]))
			{
				foreach ($fila["temp_contactos_contrato"] as &$contacto)
				{
					$contacto["telefono"] = json_decode($contacto["telefono"]);
					$contacto["correo"] = json_decode($contacto["correo"]);
				}
			}

			if(isset($fila["info_telnet"]))
			{
				$fila["info_telnet"] = json_decode($fila["info_telnet"]);
			}

			if(isset($fila["sitio"]))
			{
				$query = $this->db->select("id, nombre_completo")
									->get_where("tb_status_site", array("id" => $fila["sitio"]), 1);
				$fila["sitio"] = $query->row_array();
			}

			if(isset($fila["idu_1"]))
			{
				$fila["odus_ne"] = array();
				$fila["antenas_ne"] = array();
				$idu = $this->db->select("id, sitio, ne_id")->get_where("tb_idus", array("id" => $fila["idu_1"]), 1);
				$fila["idu_1"] = $idu->row_array();

				$sitio = $this->db->select("id, codigo, nombre_completo")->get_where("tb_status_site", array("id" => $fila["idu_1"]["sitio"]), 1);
				$fila["idu_1"]["sitio"] = $sitio->row_array();
			}

			if(isset($fila["idu_2"]))
			{
				$fila["odus_fe"] = array();
				$fila["antenas_fe"] = array();
				$idu = $this->db->select("id, sitio, ne_id")->get_where("tb_idus", array("id" => $fila["idu_2"]), 1);
				$fila["idu_2"] = $idu->row_array();

				$sitio = $this->db->select("id, codigo, nombre_completo")->get_where("tb_status_site", array("id" => $fila["idu_2"]["sitio"]), 1);
				$fila["idu_2"]["sitio"] = $sitio->row_array();

				foreach($fila["temp_odus_enlaces"] as $odu)
				{
					if($odu["idu"] == $fila["idu_1"]["id"])
					{
						array_push($fila["odus_ne"], $odu);
					}
					elseif($odu["idu"] == $fila["idu_2"]["id"])
					{
						array_push($fila["odus_fe"], $odu);
					}
				}
				unset($fila["temp_odus_enlaces"]);

				foreach($fila["temp_antenas_mw_enlaces"] as $antena)
				{
					if($antena["idu"] == $fila["idu_1"]["id"])
					{
						array_push($fila["antenas_ne"], $antena);
					}
					elseif($antena["idu"] == $fila["idu_2"]["id"])
					{
						array_push($fila["antenas_fe"], $antena);
					}
				}
				unset($fila["temp_antenas_mw_enlaces"]);
			}
		}

		return $filas;
	}

	public function get_filas_validadas($elemento)
	{
		$solicitud = $this->get_elemento($elemento);

		if ($solicitud["tabla"]["tabla_produccion"] == "tb_controlador_rectif_bancos_baterias")
		{
			$this->db->select("cr.sitio, ecr.energizacion_controlador_rectif, tt.*");
			$this->db->join("tb_controlador_rectif cr", "cr.id = tt.controlador_rectif", "left");
			$this->db->join("tb_energizacion_controlador_rectif ecr", "ecr.id = cr.energizacion_controlador_rectif", "left");
		}

		$query = $this->db->get_where($solicitud["tabla"]["tabla_temporal"]." tt", array("tt.ingreso_masivo" => $solicitud["id"], "tt.estado !=" => 1, "tt.eliminado" => 0));
		$filas = $query->result_array();
		$filas = $this->Populate_model->populate($solicitud["tabla"]["tabla_temporal"], $filas, true);

		foreach($filas as &$fila)
		{
			if (isset($fila["temp_contactos_contrato"]))
			{
				foreach ($fila["temp_contactos_contrato"] as &$contacto)
				{
					$contacto["telefono"] = json_decode($contacto["telefono"]);
					$contacto["correo"] = json_decode($contacto["correo"]);
				}
			}
			
			if(isset($fila["info_telnet"]))
			{
				$fila["info_telnet"] = json_decode($fila["info_telnet"]);
			}

			if(isset($fila["sitio"]))
			{
				$query = $this->db->select("id, nombre_completo")
									->get_where("tb_status_site", array("id" => $fila["sitio"]), 1);
				$fila["sitio"] = $query->row_array();
			}

			if(isset($fila["idu_1"]))
			{
				$fila["odus_ne"] = array();
				$fila["antenas_ne"] = array();
				$idu = $this->db->select("id, sitio, ne_id")->get_where("tb_idus", array("id" => $fila["idu_1"]), 1);
				$fila["idu_1"] = $idu->row_array();

				$sitio = $this->db->select("id, codigo, nombre_completo")->get_where("tb_status_site", array("id" => $fila["idu_1"]["sitio"]), 1);
				$fila["idu_1"]["sitio"] = $sitio->row_array();
			}

			if(isset($fila["idu_2"]))
			{
				$fila["odus_fe"] = array();
				$fila["antenas_fe"] = array();
				$idu = $this->db->select("id, sitio, ne_id")->get_where("tb_idus", array("id" => $fila["idu_2"]), 1);
				$fila["idu_2"] = $idu->row_array();

				$sitio = $this->db->select("id, codigo, nombre_completo")->get_where("tb_status_site", array("id" => $fila["idu_2"]["sitio"]), 1);
				$fila["idu_2"]["sitio"] = $sitio->row_array();

				foreach($fila["temp_odus_enlaces"] as $odu)
				{
					if($odu["idu"] == $fila["idu_1"]["id"])
					{
						array_push($fila["odus_ne"], $odu);
					}
					elseif($odu["idu"] == $fila["idu_2"]["id"])
					{
						array_push($fila["odus_fe"], $odu);
					}
				}
				unset($fila["temp_odus_enlaces"]);

				foreach($fila["temp_antenas_mw_enlaces"] as $antena)
				{
					if($antena["idu"] == $fila["idu_1"]["id"])
					{
						array_push($fila["antenas_ne"], $antena);
					}
					elseif($antena["idu"] == $fila["idu_2"]["id"])
					{
						array_push($fila["antenas_fe"], $antena);
					}
				}
				unset($fila["temp_antenas_mw_enlaces"]);
			}
		}

		return $filas;
	}

	public function update($id, $datos)
	{
		return $this->db->update($this->table, $datos, array('id' => $id));
	}
}