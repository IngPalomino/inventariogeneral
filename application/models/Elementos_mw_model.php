<?php
class Elementos_mw_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$this->db->order_by("elemento_mw","ASC");
		$types = $this->db->get("tb_elementos_mw");
		return $types->result_array();
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_elementos_mw",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$antena = $this->db->get_where("tb_elementos_mw",array('id' => $id),1);
		return $antena->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_elementos_mw');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_elementos_mw',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_elementos_mw');
	}
}