<?php
class Basedatos_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}

	public function show_tables()
	{
		$query = $this->db->query("SHOW TABLES");
		return $query->result_array();
	}

	public function show_columns($tabla)
	{
		$query = $this->db->query("SHOW COLUMNS FROM $tabla");
		return $query->result_array();
	}
}