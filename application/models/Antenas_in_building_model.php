<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antenas_in_building_model extends CI_Model
{
	private $table;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = 'tb_antenas_in_building';
	}

	public function get_antenas_in_building()
	{
		return $this->db->order_by('id', 'ASC')
						->get($this->table)
						->result_array();
	}

	public function get_antenas_in_building_sitio($id)
	{
		$antenas = $this->db->order_by('id', 'ASC')
							->get_where($this->table, array('sitio' => $id, 'eliminado' => 0))
							->result_array();

		return $this->Populate_model->populate($this->table, $antenas, TRUE);
	}

	public function edit($id)
	{
		$antena = $this->db->get_where($this->table, array('id' => $id), 1)
							->result_array();

		return $this->Populate_model->populate($this->table, $antena, TRUE, TRUE);
	}

	public function update($id, $datos)
	{
		return $this->db->set($datos)
						->where('id', $id)
						->update($this->table);
	}

	public function insert($datos)
	{
		return $this->db->insert($this->table, $datos);
	}

	public function delete($id)
	{
		return $this->db->where('id', $id)
						->delete($this->table);
	}
}