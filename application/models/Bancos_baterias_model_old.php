<?php
class Bancos_baterias_model extends CI_Model {

	public function __construct()
	{
		require_once("Populate.php");
		$this->load->database();
		$this->populate = new Populate();
	}

	public function get_bancos_baterias()
	{
		$this->db->order_by("id","ASC");
		$modulos = $this->db->get("tb_controlador_rectif_bancos_baterias");
		return $modulos->result_array();
	}

	public function get_bancos_baterias_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$bancos_baterias = $this->db->get_where("tb_controlador_rectif_bancos_baterias",array('sitio' => $id, 'eliminado' => 0));

		$bancos_baterias_array = $bancos_baterias->result_array();

		$bancos_baterias_array = $this->populate->populate("tb_controlador_rectif_bancos_baterias",$bancos_baterias_array,true);

		return $bancos_baterias_array;
	}
	
	public function get_bancos_baterias_sitio_controlador($id)
	{
		$bancos_baterias_final_array = array(
									"rectificadores" => array()
								);
								
		$this->db->order_by("id","ASC");
		$this->db->group_by("controlador_rectif");
		$this->db->select("controlador_rectif");
		$this->db->where(array('sitio' => $id, 'eliminado' => 0));
		$bancos_baterias = $this->db->get("tb_controlador_rectif_bancos_baterias");
		
		$bancos_baterias_final_array["rectificadores"] = $bancos_baterias->result_array();
		
		foreach($bancos_baterias_final_array["rectificadores"] as &$rectif)
		{
			$rectif["bancos"] = array();
			
			$bancos = $this->db->get_where("tb_controlador_rectif_bancos_baterias",
				array('sitio' => $id, 'eliminado' => 0, 'controlador_rectif' => $rectif["controlador_rectif"]));
				
			$bancos = $bancos->result_array();
			$bancos = $this->populate->populate("tb_controlador_rectif_bancos_baterias",$bancos,true);
				
			$rectif["bancos"] = $bancos;
		}
		
		return $bancos_baterias_final_array;
	}

	public function edit($id)
	{
		$bancos_baterias = $this->db->get_where("tb_controlador_rectif_bancos_baterias",array('id' => $id),1);
		$bancos_baterias_array = $bancos_baterias->result_array();

		$bancos_baterias_array = $this->populate->populate("tb_controlador_rectif_bancos_baterias",$bancos_baterias_array,true);

		return $bancos_baterias_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_controlador_rectif_bancos_baterias');
		return $resp;
	}
	
	public function update_where($parametros,$datos)
	{
		$this->db->set($datos);
		foreach($parametros as $campo => $valor)
		{
			$this->db->where($campo,$valor);
		}
		$resp = $this->db->update('tb_controlador_rectif_bancos_baterias');
		/*return $resp;*/
		return $this->db->last_query();
	}

	public function insert($datos)
	{
		$resultado = $this->db->insert('tb_controlador_rectif_bancos_baterias',$datos);
		
		$resp = array(
			"resp" => $resultado,
			"last_id" => $this->db->insert_id()
		);
		
		return $resp;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_controlador_rectif_bancos_baterias');
	}
	
	public function get_tipo_bancos_baterias($id)
	{
		$modelo = $this->db->get_where("tb_tipo_bancos_baterias",array("id" => $id));
		return $modelo->result_array();
	}
	
	public function get_sections()
	{
		
	}
}