<?php
class Autorizacion_grupos_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function comprobar_autorizacion($idUsuario, $gruposAutorizados)
	{
		$gruposUsuario = $this->get_grupos_usuario($idUsuario);

		$respuesta = false;

		foreach ($gruposAutorizados as $grupo)
		{
			if(in_array(array("grupo" => $grupo), $gruposUsuario))
			{
				$respuesta = true;
			}
		}

		return $respuesta;
	}

	public function comprobar_autorizacion_modulo($idUsuario, $modulo, $sitio = null)
	{
		$gruposPermitidosDefault = $this->get_admin_modulos();
		$gruposUsuario = $this->get_grupos_usuario($idUsuario);
		$gruposPermitidos = $this->get_grupos_modulos($modulo);

		foreach($gruposPermitidosDefault as &$grupo)
		{
			array_push($gruposPermitidos, $grupo);
		}

		if(isset($sitio) && $sitio)
		{
			$crm = $this->get_crm_grupo($sitio);
			$eliminar = (($crm["grupo"] == 8)? 11 : 8);

			if(($key = array_search($eliminar, array_column($gruposPermitidos, "grupos"))) !== false)
			{
				array_splice($gruposPermitidos, $key, 1);
			}
		}

		$respuesta = false;

		foreach($gruposUsuario as $grupo)
		{
			if(array_search($grupo["grupo"], array_column($gruposPermitidos, "grupos")) !== false)
			{
				$respuesta = true;
			}
		}

		return $respuesta;
	}

	public function isAdmin($usuario)
	{
		return $this->comprobar_autorizacion($usuario, array(1, 2));
	}

	public function isSuperAdmin($usuario)
	{
		return $this->comprobar_autorizacion($usuario, array(1));
	}

	function get_grupos_usuario($idUsuario)
	{
		if( $idUsuario === FALSE )
		{
			return FALSE;
		}

		$grupos = $this->db->select("grupo")
					->get_where("tb_usuarios_grupos", array("usuario" => $idUsuario));
		$grupos_array = $grupos->result_array();

		return $grupos_array;
	}

	function get_grupos_modulos($modulo)
	{
		$modulos = $this->db->select("grupos")
							->get_where("tb_grupos_modulos", array("modulo" => $modulo));

		return $modulos->result_array();
	}

	function get_admin_modulos()
	{
		$grupos_admin = $this->db->select("grupos")
								->get_where("tb_grupos_modulos", array("modulo" => 1));

		return $grupos_admin->result_array();
	}

	function get_crm_grupo($sitio)
	{
		$this->db->select("c.grupo");
		$this->db->join("tb_crm c", "c.id = sc.crm", "left");
		$crm = $this->db->get_where("tb_sitio_crm sc", array("sc.sitio" => $sitio), 1);
		return $crm->row_array();
	}
}