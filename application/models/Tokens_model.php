<?php
class Tokens_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function delete($id)
	{
		return $this->db->delete("tb_tokens",array("id" => $id));
	}

	public function delete_where($parametros)
	{
		foreach($parametros as $campo => $valor)
		{
			$this->db->where($campo,$valor);
		}

		return $this->db->delete('tb_tokens');
	}
}