<?php
class Evaluaciones_contrata_model extends CI_Model {
	public function __construct()
	{
		$this->load->database("evaluaciones");
	}

	public function listar()
	{
		$this->db->order_by("contrata","ASC");
		$query = $this->db->get("tb_contrata");
		$resultado_array = $query->result_array();

		/*$resultado_array = $this->populate("tb_status_site",$resultado_array,true);*/

		return $resultado_array;
	}

	public function editar($id)
	{
		$query = $this->db->get_where("tb_contrata",array('id' => $id),1);
		$resultado_array = $query->result_array();

		/*$resultado_array = $this->populate("tb_status_site",$resultado_array,true);*/

		return $resultado_array;
	}
}
?>