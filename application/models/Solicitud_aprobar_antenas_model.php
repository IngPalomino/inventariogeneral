<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitud_aprobar_antenas_model extends CI_Model {

	private $table;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = 'tb_antenas_gul_pendiente';
	}

	public function get_solicitudes()
	{
		// $solicitudes = $this->db->select('u.nombres,ante.created_at,ante.estado')
		//                          $this->db->from('tb_antenas_gul_pendiente as ante')
		//                         ->order_by('id', 'DESC')		                         
		// 						//->get_where($this->table, array('eliminado' => 0,'estado <' => 2))
		// 						->join('tb_usuarios u', 'ante.idusuario = u.id_usuario', 'left')
		// 						->get_where($this->table, array('eliminado' => 0))
		// 						->result_array();

	$solicitudes = $this->db->select('a.id,a.estado,a.created_at,a.sitio,
		st.nombre_completo,u.nombres,u.apellido_paterno')
						->join('tb_usuarios u', 'u.id_usuario = a.idusuario', 'left')
						->join('tb_status_site st', 'st.id = a.sitio', 'left')
						->order_by('id', 'DESC')	
						->get_where($this->table.' a',array('eliminado' => 0))
						->result_array();

        foreach ($solicitudes as &$solicitud) {
           $solicitud['estadonumero'] = $solicitud['estado'];           
           $solicitud['estado'] = ($solicitud['estado'] > 0) ? "Reservado" : "Pendiente" ;
           $solicitud['modulo'] = "Antenas";
        }
   //	$solicitudes = $this->Populate_model->populate($this->table, $solicitudes, TRUE);
		return $solicitudes;
	}

	// public function get_solicitudes_listar_filas_mw(){
	// 	$solicitudes = $this->db->select('a.id,a.estado_aprobacion,a.created_at,a.sitio,u.nombres,u.apellido_paterno')
	// 					->join('tb_usuarios u', 'u.id_usuario = a.idusuario', 'left')
	// 					->get_where('tb_antenas_mw_enlaces a',array('eliminado' => 0))
	// 					->order_by('a.id', 'ASC')
	// 					->result_array();
 //        foreach ($solicitudes as &$solicitud) {
 //           $solicitud['estadonumero'] = $solicitud['estado'];           
 //           $solicitud['estado_aprobacion'] = ($solicitud['estado'] > 0) ? "Reservado" : "Pendiente" ;
 //           $solicitud['modulo'] = "Antenas";
 //        }
 //        return $solicitudes;
	// }

    public function get_elemento($id)
	{

	//--- antiguo
	// $elemento = $this->db->order_by('id', 'DESC')
	//     ->join('tb_usuarios u', 'tb_antenas_gul_pendiente.idusuario = u.id_usuario', 'left')
	//     ->get_where($this->table, array('id' => $id), 1)
	//     ->row_array();
	// $elemento = $this->Populate_model->populate($this->table, $elemento, FALSE);
	// return $elemento;
    //--- antiguo
    $elemento = $this->db->select('a.id,a.estado,a.created_at,st.nombre_completo ,a.sitio,u.nombres,u.apellido_paterno')
						->join('tb_usuarios u', 'u.id_usuario = a.idusuario', 'left')
						->join('tb_status_site st','a.sitio = st.id','left')
						->order_by('id', 'DESC')	
						->get_where($this->table.' a',array('a.id' => $id))
						->row_array();
	return $elemento;
	
	}



    public function get_elemento_mw($id)
	{
		
   $this->table = 'tb_antenas_mw_enlaces';	
	$where="a.id = '$id' and  a.eliminado='0' and (a.estado_aprobacion='1' or a.estado_aprobacion='2' or a.estado_aprobacion='3')";
	$antenas_microondas = $this->db->order_by('id', 'ASC')
				->select('a.id,u.nombres,u.apellido_paterno,i.sitio,i.link_name,tipoante.diametro, a.*')
							  ->join('tb_idus i', 'i.id = a.idu', 'left')
	                          ->join('tb_tipo_antena_mw tipoante', 'a.tipo_antena_mw = tipoante.id', 'left')
	                          ->join('tb_usuarios u', 'u.id_usuario = a.id_usuario', 'left')
	                          ->where($where, NULL, FALSE)	                          
	                     ->get($this->table.' a')
							  ->result_array();
			$antenas_microondas = $this->Populate_model->populate($this->table, $antenas_microondas, TRUE);		
			foreach ($antenas_microondas as &$antena)
			{
				$antena['enlace'] = $this->db->select('id, idu_1, idu_2')
												->get_where('tb_enlaces', array('id' => $antena['enlace']), 1)
												->row_array();
				
				$antena['enlace']['idu_1'] = $this->db->select('id, sitio')
														->get_where('tb_idus', array('id' => $antena['enlace']['idu_1'], 'eliminado' => 0), 1)
														->row_array();
				
				$antena['enlace']['idu_1']['sitio'] = $this->db->select('id, nombre_completo')
																->get_where('tb_status_site', array('id' => $antena['enlace']['idu_1']['sitio']), 1)
																->row_array();
				
				$antena['enlace']['idu_2'] = $this->db->select('id, sitio')
														->get_where('tb_idus', array('id' => $antena['enlace']['idu_2'], 'eliminado' => 0), 1)
														->row_array();
				
				$antena['enlace']['idu_2']['sitio'] = $this->db->select('id, nombre_completo')
																->get_where('tb_status_site', array('id' => $antena['enlace']['idu_2']['sitio']), 1)
																->row_array();
				
				$idu2Sitio = $antena['enlace']['idu_2']['sitio'];
				
				if ($antena['sitio'] == $idu2Sitio['id'])
				{
					$temp = $antena['enlace']['idu_1'];
					$antena['enlace']['idu_1'] = $antena['enlace']['idu_2'];
					$antena['enlace']['idu_2'] = $temp;
				}
			}

	return $antenas_microondas;
	}



	public function get_filas_pendientes($elemento)
	{$id = $elemento;	
		//estado 0  es pendiente, 1 es reservado		
	    $filas = $this->db->order_by('id', 'DESC')		                         
								->get_where($this->table, 
								 array('id' => $id,'estado' => 0,'eliminado' => 0))
								->result_array();
		$filas = $this->Populate_model->populate($this->table, $filas, TRUE);
		return $filas;
	}

 //  public function get_filas_pendientes_mw($elemento)
	// {$id = $elemento;	
	// 	//estado 1  es pendiente, 2 es reservado ,3 instalado , 0 antiguo registro		
	//     $filas = $this->db->order_by('id', 'DESC')		                         
	// 							->get_where($this->table, 
	// 							 array('id' => $id,'estado' => 1,'eliminado' => 0))
	// 							->result_array();
	// 	$filas = $this->Populate_model->populate($this->table, $filas, TRUE);
	// 	return $filas;
	// }



  public function get_filas_reservado($elemento)
	{    

		$id = $elemento;
		//estado 0  es pendiente, 1 es reservado		
	    $filas = $this->db->order_by('id', 'DESC')		                         
								->get_where($this->table, 
									array('id' => $id,
									'estado' => 1,'eliminado' => 0))
								->result_array();
		$filas = $this->Populate_model->populate($this->table, $filas, TRUE);     
		return $filas;
	}



  public function get_filas_instalado($elemento)
	{    

		$id = $elemento;
		//estado 0  es pendiente, 1 es reservado, 2 es insta		
	    $filas = $this->db->order_by('id', 'DESC')		                         
								->get_where($this->table,
									array('id' => $id, 'estado' => 2,'eliminado' => 0))
								->result_array();
		$filas = $this->Populate_model->populate($this->table, $filas, TRUE);   
		return $filas;
	}   
  
  public function get_filas_instalado_aprobado($elemento)
	{    

	$id = $elemento;
		//estado 0  es pendiente, 1 es reservado, 2 es insta		
   $filas = $this->db->order_by('id', 'DESC')		                         
								->get_where($this->table, 
									array('id' => $id,
									'estado' => 2,'eliminado' => 0,'rechazado' => 0))
								->result_array();
	$filas = $this->Populate_model->populate($this->table, $filas, TRUE);   
		return $filas;
	}   

  public function update_filas_pendiente_reservado($ids,$datos)
  {
   
	foreach ($ids as $key => $id) {
		 $this->db->update($this->table, $datos, array('id' => $id));
	}

  }
//rechazando pendiente
  public function update_filas_pendiente_reservado_rechazo($ids,$datos)
  {
   
	foreach ($ids as $key => $id) {
		 $this->db->update($this->table, $datos, array('id' => $id));
	}

  }
//rechazando pendiente
  public function update_filas_reservado_instalado($ids,$datos)
  {   
	foreach ($ids as $key => $id) {
		 $this->db->update($this->table, $datos, array('id' => $id));
	}

  }
  public function update_filas_reservado_instalado_rechazo($ids,$datos)
  {   
	foreach ($ids as $key => $id) {
		 $this->db->update($this->table, $datos, array('id' => $id));

	}

  }

    public function get_antenas_gul()
	{
		return $this->db->order_by($this->pk, 'ASC')
						->get($this->table)
						->result_array();
	}

	public function get_antenas_gul_sitio($id)
	{
		$antenas_gul_array = $this->db->order_by('sector', 'ASC')
										->get_where($this->table, array('sitio' => $id, 'eliminado' => 0))
										->result_array();
		$antenas_gul_array = $this->Populate_model->populate($this->table, $antenas_gul_array, TRUE);
		
		foreach($antenas_gul_array as &$antena)
		{
			$antena['info_telnet'] = json_decode($antena['info_telnet']);
		}
		
		return $antenas_gul_array;
	}

	// public function edit($id)
	// {
	// 	$antena_gul = $this->db->get_where($this->table, array('id' => $id), 1)
	// 							->result_array();

	// 	return $this->Populate_model->populate($this->table, $antena_gul, TRUE, TRUE);
	// }

	// public function update($id, $datos)
	// {
	// 	return $this->db->update($this->table, $datos, array('id' => $id));
	// }

	// public function insert($datos)
	// {
	// 	return array(
	// 			'resp' => $this->db->insert($this->table, $datos),
	// 			'last_id' => $this->db->insert_id()
	// 		);
	// }

	// public function delete($id)
	// {
	// 	return $this->db->where('id', $id)
	// 					->delete($this->table);
	// }

	public function get_sitio_antena($id)
	{
		return $this->db->select('ss.id, ss.nombre_completo')
						->join('tb_status_site ss', 'ss.id = aa.sitio', 'left')
						->get_where($this->table.' aa', array('aa.id' => $id), 1)
						->row_array();
	}

	public function get_solicitudes_mw_listar_all()
	{

	//0 antiguo aprobado
	//1 pendiente
	//2 reservado
	//3 instalado		
	$this->table = 'tb_antenas_mw_enlaces';	
	$where="a.eliminado='0'and (a.estado_aprobacion='1' or a.estado_aprobacion='2' or a.estado_aprobacion='3')";
	$antenas_microondas = $this->db->select('a.id,u.nombres,u.apellido_paterno,i.sitio,i.link_name,tipoante.diametro, a.*')
							  ->join('tb_idus i', 'i.id = a.idu', 'left')
	                          ->join('tb_tipo_antena_mw tipoante', 'a.tipo_antena_mw = tipoante.id', 'left')
	                          ->join('tb_usuarios u', 'u.id_usuario = a.id_usuario', 'left')
	                          ->order_by('a.id', 'DESC')
	                          ->where($where, NULL, FALSE)	                          
	                     ->get($this->table.' a')
							  ->result_array();
			$antenas_microondas = $this->Populate_model->populate($this->table, $antenas_microondas, TRUE);		
			foreach ($antenas_microondas as &$antena)
			{
				$antena['enlace'] = $this->db->select('id, idu_1, idu_2')
												->get_where('tb_enlaces', array('id' => $antena['enlace']), 1)
												->row_array();
				
				$antena['enlace']['idu_1'] = $this->db->select('id, sitio')
														->get_where('tb_idus', array('id' => $antena['enlace']['idu_1'], 'eliminado' => 0), 1)
														->row_array();
				
				$antena['enlace']['idu_1']['sitio'] = $this->db->select('id, nombre_completo')
																->get_where('tb_status_site', array('id' => $antena['enlace']['idu_1']['sitio']), 1)
																->row_array();
				
				$antena['enlace']['idu_2'] = $this->db->select('id, sitio')
														->get_where('tb_idus', array('id' => $antena['enlace']['idu_2'], 'eliminado' => 0), 1)
														->row_array();
				
				$antena['enlace']['idu_2']['sitio'] = $this->db->select('id, nombre_completo')
																->get_where('tb_status_site', array('id' => $antena['enlace']['idu_2']['sitio']), 1)
																->row_array();
				$idu2Sitio = $antena['enlace']['idu_2']['sitio'];				
				if ($antena['sitio'] == $idu2Sitio['id'])
				{
					$temp = $antena['enlace']['idu_1'];
					$antena['enlace']['idu_1'] = $antena['enlace']['idu_2'];
					$antena['enlace']['idu_2'] = $temp;
				}
			}			
			return $antenas_microondas;
	}

//-----microondas pendientes por id
    public function get_filas_pendientes_mw($id){
	//0 antiguo aprobado
	//1 pendiente
	//2 reservado
	//3 instalado		
	$this->table = 'tb_antenas_mw_enlaces';	
	$where="a.id = '$id'and a.eliminado='0'and a.estado_aprobacion='1'";
	$antenas_microondas = $this->db->select('a.id,u.nombres,u.apellido_paterno,i.sitio,i.link_name,tipoante.diametro, a.*')
							  ->join('tb_idus i', 'i.id = a.idu', 'left')
	                          ->join('tb_tipo_antena_mw tipoante', 'a.tipo_antena_mw = tipoante.id', 'left')
	                          ->join('tb_usuarios u', 'u.id_usuario = a.id_usuario', 'left')
	                          ->order_by('a.id', 'DESC')
	                          ->where($where, NULL, FALSE)	                          
	                     ->get($this->table.' a')
							  ->result_array();
			$antenas_microondas = $this->Populate_model->populate($this->table, $antenas_microondas, TRUE);		
			foreach ($antenas_microondas as &$antena)
			{
				$antena['enlace'] = $this->db->select('id, idu_1, idu_2')
												->get_where('tb_enlaces', array('id' => $antena['enlace']), 1)
												->row_array();
				
				$antena['enlace']['idu_1'] = $this->db->select('id, sitio')
														->get_where('tb_idus', array('id' => $antena['enlace']['idu_1'], 'eliminado' => 0), 1)
														->row_array();
				
				$antena['enlace']['idu_1']['sitio'] = $this->db->select('id, nombre_completo')
																->get_where('tb_status_site', array('id' => $antena['enlace']['idu_1']['sitio']), 1)
																->row_array();
				
				$antena['enlace']['idu_2'] = $this->db->select('id, sitio')
														->get_where('tb_idus', array('id' => $antena['enlace']['idu_2'], 'eliminado' => 0), 1)
														->row_array();
				
				$antena['enlace']['idu_2']['sitio'] = $this->db->select('id, nombre_completo')
																->get_where('tb_status_site', array('id' => $antena['enlace']['idu_2']['sitio']), 1)
																->row_array();
				$idu2Sitio = $antena['enlace']['idu_2']['sitio'];				
				if ($antena['sitio'] == $idu2Sitio['id'])
				{
					$temp = $antena['enlace']['idu_1'];
					$antena['enlace']['idu_1'] = $antena['enlace']['idu_2'];
					$antena['enlace']['idu_2'] = $temp;
				}
			}			
			return $antenas_microondas;

    }


    public function get_filas_reservado_mw($id){
	//0 antiguo aprobado
	//1 pendiente
	//2 reservado
	//3 instalado		
	$this->table = 'tb_antenas_mw_enlaces';	
	$where="a.id = '$id'and a.eliminado='0'and a.estado_aprobacion='2'";
	$antenas_microondas = $this->db->select('a.id,u.nombres,u.apellido_paterno,i.sitio,i.link_name,tipoante.diametro, a.*')
							  ->join('tb_idus i', 'i.id = a.idu', 'left')
	                          ->join('tb_tipo_antena_mw tipoante', 'a.tipo_antena_mw = tipoante.id', 'left')
	                          ->join('tb_usuarios u', 'u.id_usuario = a.id_usuario', 'left')
	                          ->order_by('a.id', 'DESC')
	                          ->where($where, NULL, FALSE)	                          
	                     ->get($this->table.' a')
							  ->result_array();
			$antenas_microondas = $this->Populate_model->populate($this->table, $antenas_microondas, TRUE);		
			foreach ($antenas_microondas as &$antena)
			{
				$antena['enlace'] = $this->db->select('id, idu_1, idu_2')
												->get_where('tb_enlaces', array('id' => $antena['enlace']), 1)
												->row_array();
				
				$antena['enlace']['idu_1'] = $this->db->select('id, sitio')
														->get_where('tb_idus', array('id' => $antena['enlace']['idu_1'], 'eliminado' => 0), 1)
														->row_array();
				
				$antena['enlace']['idu_1']['sitio'] = $this->db->select('id, nombre_completo')
																->get_where('tb_status_site', array('id' => $antena['enlace']['idu_1']['sitio']), 1)
																->row_array();
				
				$antena['enlace']['idu_2'] = $this->db->select('id, sitio')
														->get_where('tb_idus', array('id' => $antena['enlace']['idu_2'], 'eliminado' => 0), 1)
														->row_array();
				
				$antena['enlace']['idu_2']['sitio'] = $this->db->select('id, nombre_completo')
																->get_where('tb_status_site', array('id' => $antena['enlace']['idu_2']['sitio']), 1)
																->row_array();
				$idu2Sitio = $antena['enlace']['idu_2']['sitio'];				
				if ($antena['sitio'] == $idu2Sitio['id'])
				{
					$temp = $antena['enlace']['idu_1'];
					$antena['enlace']['idu_1'] = $antena['enlace']['idu_2'];
					$antena['enlace']['idu_2'] = $temp;
				}
			}			
			return $antenas_microondas;

    }
    
    public function get_filas_instalado_mw($id){
	//0 antiguo aprobado
	//1 pendiente
	//2 reservado
	//3 instalado		
	$this->table = 'tb_antenas_mw_enlaces';	
	$where="a.id = '$id'and a.eliminado='0'and a.estado_aprobacion='3'";
	$antenas_microondas = $this->db->select('a.id,u.nombres,u.apellido_paterno,i.sitio,i.link_name,tipoante.diametro, a.*')
							  ->join('tb_idus i', 'i.id = a.idu', 'left')
	                          ->join('tb_tipo_antena_mw tipoante', 'a.tipo_antena_mw = tipoante.id', 'left')
	                          ->join('tb_usuarios u', 'u.id_usuario = a.id_usuario', 'left')
	                          ->order_by('a.id', 'DESC')
	                          ->where($where, NULL, FALSE)	                          
	                     ->get($this->table.' a')
							  ->result_array();
			$antenas_microondas = $this->Populate_model->populate($this->table, $antenas_microondas, TRUE);		
			foreach ($antenas_microondas as &$antena)
			{
				$antena['enlace'] = $this->db->select('id, idu_1, idu_2')
												->get_where('tb_enlaces', array('id' => $antena['enlace']), 1)
												->row_array();
				
				$antena['enlace']['idu_1'] = $this->db->select('id, sitio')
														->get_where('tb_idus', array('id' => $antena['enlace']['idu_1'], 'eliminado' => 0), 1)
														->row_array();
				
				$antena['enlace']['idu_1']['sitio'] = $this->db->select('id, nombre_completo')
																->get_where('tb_status_site', array('id' => $antena['enlace']['idu_1']['sitio']), 1)
																->row_array();
				
				$antena['enlace']['idu_2'] = $this->db->select('id, sitio')
														->get_where('tb_idus', array('id' => $antena['enlace']['idu_2'], 'eliminado' => 0), 1)
														->row_array();
				
				$antena['enlace']['idu_2']['sitio'] = $this->db->select('id, nombre_completo')
																->get_where('tb_status_site', array('id' => $antena['enlace']['idu_2']['sitio']), 1)
																->row_array();
				$idu2Sitio = $antena['enlace']['idu_2']['sitio'];				
				if ($antena['sitio'] == $idu2Sitio['id'])
				{
					$temp = $antena['enlace']['idu_1'];
					$antena['enlace']['idu_1'] = $antena['enlace']['idu_2'];
					$antena['enlace']['idu_2'] = $temp;
				}
			}			
			return $antenas_microondas;

    } 

//--------------- Steps Microondas
  public function update_filas_pendiente_reservado_mw($ids,$datos)
  {    
  	$this->table = 'tb_antenas_mw_enlaces';
	foreach ($ids as $key => $id) {
	$this->db->update($this->table, $datos, array('id' => $id));
	//echo $this->db->last_query();
	}
  }
//rechazando pendiente
  public function update_filas_pendiente_reservado_rechazo_mw($ids,$datos)
  {
   $this->table = 'tb_antenas_mw_enlaces';
	foreach ($ids as $key => $id) {
		 $this->db->update($this->table, $datos, array('id' => $id));
		 echo $this->db->last_query();
	}


  }
//rechazando pendiente
  public function update_filas_reservado_instalado_mw($ids,$datos)
  {  
    $this->table = 'tb_antenas_mw_enlaces';
	foreach ($ids as $key => $id) {
		 $this->db->update($this->table, $datos, array('id' => $id));

	}

  }
  public function update_filas_reservado_instalado_rechazo_mw($ids,$datos)
  {   
  	$this->table = 'tb_antenas_mw_enlaces';
	foreach ($ids as $key => $id) {
		 $this->db->update($this->table, $datos, array('id' => $id));
	}
  }

// Steps Microondas

	
}