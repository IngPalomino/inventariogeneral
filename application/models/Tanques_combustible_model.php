<?php
class Tanques_combustible_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function get_tanques_combustible()
	{
		$this->db->order_by("id","ASC");
		$antenas = $this->db->get("tb_tanques_combustible");
		return $antenas->result_array();
	}

	public function get_tanques_combustible_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$tanques_combustible = $this->db->get_where("tb_tanques_combustible",array('sitio' => $id, 'eliminado' => 0));

		$tanques_combustible_array = $tanques_combustible->result_array();

		$tanques_combustible_array = $this->Populate_model->populate("tb_tanques_combustible",$tanques_combustible_array,true);

		return $tanques_combustible_array;
	}

	public function edit($id)
	{
		$tanque_combustible = $this->db->get_where("tb_tanques_combustible",array('id' => $id),1);
		$tanque_combustible_array = $tanque_combustible->result_array();

		$tanque_combustible_array = $this->Populate_model->populate("tb_tanques_combustible",$tanque_combustible_array,true,true);

		return $tanque_combustible_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_tanques_combustible');
		return $resp;
	}

	public function insert($datos)
	{
		$resultado = $this->db->insert('tb_tanques_combustible',$datos);

		$resp = array(
			"resp" => $resultado,
			"last_id" => $this->db->insert_id()
		);
		
		return $resp;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tanques_combustible');
	}

	public function get_sitio_tanque($id)
	{
		$this->db->select("ss.id, ss.nombre_completo");
		$this->db->join("tb_status_site ss","ss.id = aa.sitio","left");
		$sitio = $this->db->get_where("tb_tanques_combustible aa", array("aa.id" => $id), 1);
		return $sitio->row_array();
	}
}