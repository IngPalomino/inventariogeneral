<?php
class Logs_antenas_mw_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}

	public function get_logs()
	{
		$logs = $this->db->get("tb_logs_antenas_mw_enlaces");
		return $logs->result_array();
	}

    public function get_logs_antena_mw($id_antena_mw)
	{
	    $logs = $this->db->get_where("tb_logs_antenas_mw_enlaces", array('antena_mw' => $id_antena_mw));
		return $logs->result_array();
	}


	public function ingreso_de_datos($user = FALSE,$datos)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		$datos["tipo"]	= "INSERT";
		$datos["id"]	= $this->db->insert_id();
		$datos["query"]	= $this->db->last_query();

		$data = array(
				"usuario"	=> $user,
				"antena_mw"	=> $datos["id"],
				"evento"	=> json_encode($datos)
			);

		$this->db->insert("tb_logs_antenas_mw_enlaces",$data);
	}

	public function actualizacion_de_datos($user = FALSE,$id,$datos,$comentario = FALSE)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		if($comentario === FALSE)
		{
			$comentario = "";
		}

		$datos["tipo"] = "UPDATE";
		$datos["query"]	= $this->db->last_query();

		$data = array(
				"usuario"	=> $user,
				"antena_mw"	=> $id,
				"evento"	=> json_encode($datos),
				"comentario"=> $comentario
			);

		$this->db->insert("tb_logs_antenas_mw_enlaces",$data);
		return json_encode($data);
	}

	public function eliminacion_de_datos($user = FALSE,$id,$datos,$comentario = FALSE)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		if($comentario === FALSE)
		{
			$comentario = "";
		}

		$datos["tipo"] = "DELETE";
		$datos["query"]	= $this->db->last_query();

		$data = array(
				"usuario"	=> $user,
				"antena_mw"	=> $id,
				"evento"	=> json_encode($datos),
				"comentario"=> $comentario
			);

		$this->db->insert("tb_logs_antenas_mw_enlaces",$data);
		return json_encode($data);
	}
}