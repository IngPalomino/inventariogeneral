<?php
class Bodega_bancos_baterias_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}
	
	public function get_bancos_baterias($filtros = array())
	{
		if(isset($filtros["almacen"]))
		{
			$this->db->where(array("tipo_almacen" => 1, "almacen" => $filtros["almacen"]));
		}
		elseif(isset($filtros["sitio"]))
		{
			$this->db->where(array("tipo_almacen" => 2, "almacen" => $filtros["sitio"]));
		}
		
		$this->db->order_by("id", "ASC");
		$bancos = $this->db->get_where("tb_bodega_bancos_baterias", array('eliminado' => 0));
		$bancos_array = $bancos->result_array();
		$bancos_array = $this->Populate_model->populate("tb_bodega_bancos_baterias", $bancos_array, true);
		
		foreach($bancos_array as &$banco)
		{
		 if($banco["id_sala_tecnica"]>0){
            //si tiene id de mso 
			 	$this->db->select("id, nombre_completo");
			$nombre_completo = $this->db->get_where("tb_mso_sala_tecnica",array('sitio' => $banco["proveniente_sitio"]));			
		$banco["proveniente_sitio"] = $nombre_completo->row_array();	

           }else{
           	$this->db->select("id, nombre_completo");
			$nombre_completo = $this->db->get_where("tb_status_site", array('id' => $banco["proveniente_sitio"]));
			$banco["proveniente_sitio"] = $nombre_completo->row_array();	
           }


     






			switch($banco["tipo_almacen"])
			{
				case 1:
					$almacen = $this->db->get_where("tb_almacenes", array("id" => $banco["almacen"]));
					$banco["almacen"] = $almacen->row_array();
					break;
				case 2:
					$this->db->select("id, nombre_completo");
					$sitio = $this->db->get_where("tb_status_site", array("id" => $banco["almacen"]));
					$almacen = $sitio->row_array();
					$banco["almacen"] = array(
							"id" => $almacen["id"],
							"almacen" => $almacen["nombre_completo"]
						);
					break;
			}
		}
		
		return $bancos_array;
	}
	
	public function get_bancos_baterias_almacen($id)
	{
		$this->db->order_by("id", "ASC");
		$bancos = $this->db->get_where("tb_bodega_bancos_baterias", array('eliminado' => 0, 'tipo_almacen' => 1, 'almacen' => $id));
		$bancos_array = $bancos->result_array();
		$bancos_array = $this->Populate_model->populate("tb_bodega_bancos_baterias", $bancos_array, true);

		foreach($bancos_array as &$banco)
		{
			$this->db->select("id, nombre_completo");
			$nombre_completo = $this->db->get_where("tb_status_site",array('id' => $banco["proveniente_sitio"]));
			$banco["proveniente_sitio"] = $nombre_completo->row_array();
		}
		
		return $bancos_array;
	}
	
	public function get_bancos_baterias_sitio($id)
	{
		$this->db->order_by("id", "ASC");
		$bancos = $this->db->get_where("tb_bodega_bancos_baterias", array('eliminado' => 0, 'tipo_almacen' => 2, 'almacen' => $id));
		$bancos_array = $bancos->result_array();
		$bancos_array = $this->Populate_model->populate("tb_bodega_bancos_baterias", $bancos_array, true);

		foreach($bancos_array as &$banco)
		{
			$this->db->select("id, nombre_completo");
			$nombre_completo = $this->db->get_where("tb_status_site",array('id' => $banco["proveniente_sitio"]));
			$banco["proveniente_sitio"] = $nombre_completo->row_array();
		}
		
		return $bancos_array;
	}

	public function edit($id)
	{
		$banco = $this->db->get_where("tb_bodega_bancos_baterias", array('id' => $id), 1);
		$banco_array = $banco->result_array();
		$banco_array = $this->Populate_model->populate("tb_bodega_bancos_baterias", $banco_array, true, true);
		return $banco_array;
	}

	public function update($id, $datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		return $this->db->update('tb_bodega_bancos_baterias');
	}

	public function insert($datos)
	{
		$respuesta["resp"] = $this->db->insert('tb_bodega_bancos_baterias', $datos);
		$respuesta["last_id"] = $this->db->insert_id();
		return $respuesta;
	}
}