<?php
class Celdas_bancos_baterias_model extends CI_Model {

	public function __construct()
	{
		require_once("Populate.php");
		$this->load->database();
		$this->populate = new Populate();
	}

	public function get_celdas()
	{
		$this->db->order_by("id","ASC");
		$celdas = $this->db->get("tb_celdas_controlador_rectif_bancos_baterias");
		return $celdas->result_array();
	}

	public function get_celdas_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$celdas = $this->db->get_where("tb_celdas_controlador_rectif_bancos_baterias",array('controlador_rectif_bancos_baterias' => $id, 
         'eliminado' => 0));

		$celdas_array = $celdas->result_array();

		$celdas_array = $this->populate->populate("tb_celdas_controlador_rectif_bancos_baterias",$celdas_array,true);

		return $celdas_array;
	}

	public function edit($id)
	{
		$celda = $this->db->get_where("tb_celdas_controlador_rectif_bancos_baterias",array('id' => $id),1);
		$celda_array = $celda->result_array();

		$celda_array = $this->populate->populate("tb_celdas_controlador_rectif_bancos_baterias",$celda_array,true,true);

		return $celda_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_celdas_controlador_rectif_bancos_baterias');
		return $resp;
	}

	public function update_where($parametros, $datos)
	{
		$this->db->set($datos);

		foreach($parametros as $campo => $valor)
		{
			$this->db->where($campo, $valor);
		}

		return $this->db->update('tb_celdas_controlador_rectif_bancos_baterias');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_celdas_controlador_rectif_bancos_baterias',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_celdas_controlador_rectif_bancos_baterias');
	}

	public function delete_where($parametros)
	{
		foreach($parametros as $campo => $valor)
		{
			$this->db->where($campo, $valor);
		}

		return $this->db->delete("tb_celdas_controlador_rectif_bancos_baterias");
	}
}