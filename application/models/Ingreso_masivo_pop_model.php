<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_masivo_pop_model extends CI_Model {

	public function __construct()
	{
		$this->load->model('Populator_model', 'Populate_model');
	}

	public function esNombreCompletoValido($nombre)
	{
		$sitio = $this->db->select('id')
							->get_where('tb_status_site', array('nombre_completo' => $nombre), 1);

		return ($sitio->num_rows() === 1) ? $sitio->row_array() : FALSE;
	}

	public function esNombreCodigoValidos($codigo, $nombre)
	{
		$sitio = $this->db->select('id')
							->get_where('tb_status_site', array('codigo' => $codigo, 'nombre_completo' => $nombre), 1);

		return ($sitio->num_rows() === 1) ? $sitio->row_array() : FALSE;
	}

	public function departamento_valido($departamento)
	{
		$query = $this->db->select('departamento')
							->get_where('tb_zona_peru', array('departamento' => $departamento), 1);

		return $query->num_rows() === 1;
	}

	public function provincia_valida($provincia)
	{
		$query = $this->db->select('provincia')
							->get_where('tb_zona_peru', array('provincia' => $provincia), 1);

		return $query->num_rows() === 1;
	}

	public function distrito_valido($distrito)
	{
		$query = $this->db->select('distrito')
							->get_where('tb_zona_peru', array('distrito' => $distrito), 1);

		return $query->num_rows() === 1;
	}

	public function zona_peru_valida($departamento, $provincia, $distrito)
	{
		$query = $this->db->limit(1)
							->order_by('id', 'ASC')
							->where(array('departamento' => $departamento, 'provincia' => $provincia, 'distrito' => $distrito))
							->get('tb_zona_peru');

		return ($query->num_rows() === 1) ? $query->row() : FALSE;
	}

	public function esTipoTorreValido($tipo)
	{
		$query = $this->db->get_where('tb_tipo_torre', array('tipo_torre' => $tipo), 1);

		return ($query->num_rows() === 1) ? $query->row_array() : FALSE;
	}

	public function esTipoMimetizadoValido($tipo)
	{
		$query = $this->db->get_where('tb_camuflaje', array('camuflaje' => $tipo), 1);

		return ($query->num_rows() === 1) ? $query->row_array() : FALSE;
	}

	public function esTipoProyectoValido($tipo)
	{
		$query = $this->db->get_where('tb_tipo_proyecto', array('tipo_proyecto' => $tipo), 1);

		return ($query->num_rows() === 1) ? $query->row_array() : FALSE;
	}

	public function esCoubicadorValido($coubicador)
	{
		$query = $this->db->get_where('tb_coubicador', array('coubicador' => $coubicador), 1);

		return ($query->num_rows() === 1) ? $query->row_array() : FALSE;
	}

	public function esCoubicanteValido($coubicante)
	{
		$query = $this->db->get_where('tb_coubicante', array('coubicante' => $coubicante), 1);

		return ($query->num_rows() === 1) ? $query->row_array() : FALSE;
	}

	public function existeContratoEnSitio($contrato, $sitio)
	{
		$query = $this->db->limit(1)
							->order_by('id', 'ASC')
							->where(array('id_contrato' => $contrato, 'sitio' => $sitio, 'eliminado' => 0))
							->get('tb_contratos');

		return ($query->num_rows() === 1) ? $query->row_array() : FALSE;
	}

	public function registrar_ingreso_masivo($tabla, $usuario = FALSE)
	{
		$datos['usuario'] = ( ! $usuario ? $this->session->userdata('id_usuario') : $usuario);
		$datos['tabla'] = $tabla;

		return $this->db->insert('tb_ingreso_masivo', $datos) ? $this->db->insert_id() : FALSE;
	}

	public function ingresar_temporal($datos)
	{
		return $this->db->insert('tb_temp_status_site', $datos);
	}

	public function ingresar_temporal_contratos($datos)
	{
		return array(
			'resp' => $this->db->insert('tb_temp_contratos', $datos),
			'last_id' => $this->db->insert_id()
		);
	}

	public function ingresar_temporal_contactos($datos)
	{
		return $this->db->insert('tb_temp_contactos_contrato', $datos);
	}

	public function update($id, $datos)
	{
		return $this->db->update('tb_temp_status_site', $datos, array('id' => $id));
	}

	public function update_contratos($id, $datos)
	{
		return $this->db->update('tb_temp_contratos', $datos, array('id' => $id));
	}

	public function get_solicitudes($usuario = FALSE)
	{
		return $this->Populate_model->order_by('id', 'DESC')
									->where(array('usuario' => $usuario, 'tabla' => 5))
									->get('tb_ingreso_masivo')
									->result_array()
									->populate();
	}
}