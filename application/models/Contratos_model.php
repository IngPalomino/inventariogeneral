<?php
class Contratos_model extends CI_Model {

	public function __construct()
	{
		require_once("Populate.php");
		$this->load->database();
		$this->populate = new Populate();
	}

	public function get_contratos()
	{
		$this->db->order_by("id_contrato","ASC");
		$contratos = $this->db->get("tb_contratos");
		$contratos_array = $contratos->result_array();

		$contratos_array = $this->populate->populate("tb_contratos",$contratos_array,true);

		return $contratos_array;
	}

	public function look_for_contratos($busqueda)
	{
		$this->db->order_by("id_contrato","ASC");

		$this->db->where($busqueda);
		$contratos = $this->db->get("tb_contratos");
		$contratos_array = $contratos->result_array();

		$contratos_array = $this->populate->populate("tb_contratos",$contratos_array,true);

		return $contratos_array;
	}

	public function edit($id)
	{
		$contrato = $this->db->get_where("tb_contratos",array('id' => $id),1);
		$contrato_array = $contrato->result_array();

		$contrato_array = $this->populate->populate("tb_contratos",$contrato_array,true,true);

		return $contrato_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_contratos');

		return $resp;
	}

	public function insert($datos)
	{
		$resp["resp"] = $this->db->insert('tb_contratos',$datos);
		$resp["last_id"] = $this->db->insert_id();

		return $resp;
	}

	/*public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_contratos');
	}*/
}