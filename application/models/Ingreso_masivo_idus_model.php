<?php
class Ingreso_masivo_idus_model extends CI_Model{

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function esNombreCompletoValido($nombre)
	{
		$sitio = $this->db->select("id")
						->get_where("tb_status_site", array("nombre_completo" => $nombre), 1);

		if($sitio->num_rows() > 0)
		{
			return $sitio->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esNombreCodigoValidos($codigo, $nombre)
	{
		$sitio = $this->db->select("id")
						->get_where("tb_status_site", array("codigo" => $codigo, "nombre_completo" => $nombre), 1);

		if($sitio->num_rows() > 0)
		{
			return $sitio->row_array();
		}
		else
		{
			return false;
		}
	}

	public function existeIDUEnSitio($sitio, $neid)
	{
		$query = $this->db->get_where("tb_idus", array("sitio" => $sitio, "ne_id" => $neid, "eliminado" => 0), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esIDUDuplicada($neid)
	{
		$query = $this->db->get_where("tb_idus", array("ne_id" => $neid, "eliminado" => 0), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esMarcaValida($marca)
	{
		$query = $this->db->get_where("tb_tipo_idu", array("marca" => $marca));

		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function esModeloValido($modelo)
	{
		$query = $this->db->get_where("tb_tipo_idu", array("modelo" => $modelo));

		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function esMarcaModeloValidos($marca, $modelo)
	{
		$query = $this->db->get_where("tb_tipo_idu", array("marca" => $marca, "modelo" => $modelo), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function registrar_ingreso_masivo($usuario)
	{
		$datos["usuario"] = $usuario;
		$datos["tabla"] = 3;

		if($this->db->insert("tb_ingreso_masivo", $datos))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}

	public function ingresar_temporal($datos)
	{
		return $this->db->insert("tb_temp_idus", $datos);
	}

	public function update($id, $datos)
	{
		$query = $this->db->set($datos)
							->where("id", $id)
							->update("tb_temp_idus");
		return $query;
	}

	public function get_solicitudes($usuario)
	{
		$this->db->order_by("id", "DESC");
		$query = $this->db->get_where("tb_ingreso_masivo", array("usuario" => $usuario, "tabla" => 3));
		$soliticudes = $query->result_array();
		$soliticudes = $this->Populate_model->populate("tb_ingreso_masivo", $soliticudes, true);
		return $soliticudes;
	}
}