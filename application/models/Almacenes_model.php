<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Almacenes_model extends CI_Model {

	public function __construct()
	{
		$this->load->model('Populate_model');
	}
	
	public function get_types()
	{
		return $this->db->order_by('almacen', 'ASC')
						->get('tb_almacenes')
						->result_array();
	}
	
	public function get_sites()
	{
		return $this->db->select('nombre as id, nombre_completo')
						->order_by('codigo')
						->get('tb_status_site')
						->result_array();
	}
}