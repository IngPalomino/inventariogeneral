<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitante_model extends CI_Model {

	private $table;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = 'tb_antenas_gul_pendiente';
	}

	public function get_solicitudes_mw_listar_all_usuario($id_usuario)
	{

	//0 antiguo aprobado
	//1 pendiente
	//2 reservado
	//3 instalado		
	$this->table = 'tb_antenas_mw_enlaces';	
	$where="a.eliminado='0'and (a.estado_aprobacion='1' or a.estado_aprobacion='2' or a.estado_aprobacion='3') and u.id_usuario = '$id_usuario'";
	$antenas_microondas = $this->db->select('a.id,u.nombres,u.apellido_paterno,i.sitio,i.link_name,tipoante.diametro, a.*')
							  ->join('tb_idus i', 'i.id = a.idu', 'left')
	                          ->join('tb_tipo_antena_mw tipoante', 'a.tipo_antena_mw = tipoante.id', 'left')
	                          ->join('tb_usuarios u', 'u.id_usuario = a.id_usuario', 'left')
	                          ->order_by('a.id', 'DESC')
	                          ->where($where, NULL, FALSE)	                          
	                     ->get($this->table.' a')
							  ->result_array();
			$antenas_microondas = $this->Populate_model->populate($this->table, $antenas_microondas, TRUE);		
			foreach ($antenas_microondas as &$antena)
			{
				$antena['enlace'] = $this->db->select('id, idu_1, idu_2')
												->get_where('tb_enlaces', array('id' => $antena['enlace']), 1)
												->row_array();
				
				$antena['enlace']['idu_1'] = $this->db->select('id, sitio')
														->get_where('tb_idus', array('id' => $antena['enlace']['idu_1'], 'eliminado' => 0), 1)
														->row_array();
				
				$antena['enlace']['idu_1']['sitio'] = $this->db->select('id, nombre_completo')
																->get_where('tb_status_site', array('id' => $antena['enlace']['idu_1']['sitio']), 1)
																->row_array();
				
				$antena['enlace']['idu_2'] = $this->db->select('id, sitio')
														->get_where('tb_idus', array('id' => $antena['enlace']['idu_2'], 'eliminado' => 0), 1)
														->row_array();
				
				$antena['enlace']['idu_2']['sitio'] = $this->db->select('id, nombre_completo')
																->get_where('tb_status_site', array('id' => $antena['enlace']['idu_2']['sitio']), 1)
																->row_array();
				$idu2Sitio = $antena['enlace']['idu_2']['sitio'];				
				if ($antena['sitio'] == $idu2Sitio['id'])
				{
					$temp = $antena['enlace']['idu_1'];
					$antena['enlace']['idu_1'] = $antena['enlace']['idu_2'];
					$antena['enlace']['idu_2'] = $temp;
				}
			}			
			return $antenas_microondas;
	}

// Steps Microondas

public function get_solicitudes_usuario($id_usuario)
	{
	$solicitudes = $this->db->select('a.id,a.estado,a.created_at,a.sitio,
		st.nombre_completo,u.nombres,u.apellido_paterno')
						->join('tb_usuarios u', 'u.id_usuario = a.idusuario', 'left')
						->join('tb_status_site st', 'st.id = a.sitio', 'left')
						->order_by('id', 'DESC')	
						->get_where($this->table.' a',array('eliminado' => 0,
						'u.id_usuario'=>$id_usuario))
						->result_array();

        foreach ($solicitudes as &$solicitud) {
           $solicitud['estadonumero'] = $solicitud['estado'];           
           $solicitud['estado'] = ($solicitud['estado'] > 0) ? "Reservado" : "Pendiente" ;
           $solicitud['modulo'] = "Antenas";
        }  
		return $solicitudes;
	}
	
}