<?php
class Iden_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_iden()
	{
		$this->db->order_by("id","ASC");
		$iden = $this->db->get("tb_iden");
		return $iden->result_array();
	}

	public function get_iden_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$iden = $this->db->get_where("tb_iden",array('sitio' => $id));
		$iden_array = $iden->result_array();

		foreach($iden_array as &$info)
		{
			$this->db->reset_query();
			$this->db->select("cantidad");
			$resultado = $this->db->get_where("tb_antenas_iden",array("eliminado" => 0, "sitio" => $id));
			$resultado_array = $resultado->result_array();

			$cantidad_total = 0;
			
			foreach($resultado_array as $key => $value)
			{
				$cantidad_total += (Float)$value["cantidad"];
			}

			$info["antennas"] = $cantidad_total;
		}

		return $iden_array;
	}

	public function edit($id)
	{
		$iden = $this->db->get_where("tb_iden",array('id' => $id),1);
		$iden_array = $iden->result_array();
		return $iden_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_iden');
		return $resp;
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_iden',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_iden');
	}
}