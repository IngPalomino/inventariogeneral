<?php
class Subestacion_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function get_subestacion()
	{
		$this->db->order_by("id","ASC");
		$subestacion = $this->db->get("tb_subestacion");
		return $subestacion->result_array();
	}

	public function get_subestacion_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$subestacion = $this->db->get_where("tb_subestacion",array('sitio' => $id, 'eliminado' => 0));

		$subestacion_array = $subestacion->result_array();
		$subestacion_array = $this->Populate_model->populate("tb_subestacion",$subestacion_array,true);
		return $subestacion_array;
	}

	public function edit($id)
	{
		$subestacion = $this->db->get_where("tb_subestacion",array('id' => $id),1);
		$subestacion_array = $subestacion->result_array();

		$subestacion_array = $this->Populate_model->populate("tb_subestacion",$subestacion_array,true,true);

		return $subestacion_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_subestacion');
		return $resp;
	}
	
	public function get_sitios_subestacion()
	{
		$this->db->order_by("codigo","ASC");
		$this->db->select("ss.nombre_completo, ss.id");
		$this->db->join("tb_status_site ss","ss.id = gg.sitio","left");
		$sitios = $this->db->get("tb_subestacion gg");
		return $sitios->result_array();
	}

	public function insert($datos)
	{
		$resultado = $this->db->insert('tb_subestacion',$datos);

		$resp = array(
			"resp" => $resultado,
			"last_id" => $this->db->insert_id()
		);
		
		return $resp;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_subestacion');
	}

	public function get_sitio_subestacion($id)
	{
		$this->db->select("ss.id, ss.nombre_completo");
		$this->db->join("tb_status_site ss","ss.id = aa.sitio","left");
		$sitio = $this->db->get_where("tb_subestacion aa", array("aa.id" => $id), 1);
		return $sitio->row_array();
	}
}