<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antenas_gul_model extends CI_Model {

	private $table;
	private $pk;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = 'tb_antenas_gul';
		$this->pk = 'id';
	}

	public function get_antenas_gul()
	{
		return $this->db->order_by($this->pk, 'ASC')
						->get($this->table)
						->result_array();
	}

	public function get_antenas_gul_sitio($id)
	{
		$antenas_gul_array = $this->db->order_by('sector', 'ASC')
									  ->get_where($this->table, array('sitio' => $id, 'eliminado' => 0))
										->result_array();
		$antenas_gul_array = $this->Populate_model->populate($this->table, $antenas_gul_array, TRUE);
		
		foreach($antenas_gul_array as &$antena)
		{
			$antena['info_telnet'] = json_decode($antena['info_telnet']);
		}
		
		return $antenas_gul_array;
	}


	public function edit($id)
	{
		$antena_gul = $this->db->get_where($this->table, array('id' => $id), 1)
								->result_array();

		return $this->Populate_model->populate($this->table, $antena_gul, TRUE, TRUE);
	}

	public function update($id, $datos)
	{
		return $this->db->update($this->table, $datos, array('id' => $id));
	}

	public function insert($datos)
	{
		return array(
				'resp' => $this->db->insert($this->table, $datos),
				'last_id' => $this->db->insert_id()
			);
	}

	public function delete($id)
	{
	    return $this->db->where('id', $id)
						->delete($this->table);
	}

	public function get_sitio_antena($id)
	{
		return $this->db->select('ss.id, ss.nombre_completo')
						->join('tb_status_site ss', 'ss.id = aa.sitio', 'left')
						->get_where($this->table.' aa', array('aa.id' => $id), 1)
						->row_array();
	}
    //3.0
   

}