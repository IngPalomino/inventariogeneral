<?php
class Tipo_aires_acondicionados_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$listado = array(
				"marcas" => array(),
				"modelos" => array(),
				"capacidades" => array(),
				"capacidades2" => array(),
				"tipos_equipo" => array()
			);
			
		$this->db->select("marca, capacidad");
		$this->db->group_by("marca");
		$marcas = $this->db->get("tb_tipo_aires_acondicionados");
		
		$listado["marcas"] = $marcas->result_array();
		
		$this->db->reset_query();
		$this->db->order_by("modelo");
		$modelos = $this->db->get("tb_tipo_aires_acondicionados");
		
		$listado["modelos"] = $modelos->result_array();
		
		$this->db->reset_query();
		$this->db->select("capacidad, marca, tipo_equipo");
		$this->db->where("tipo_equipo", "Mochila");
		$this->db->group_by("capacidad, marca, tipo_equipo");
		
		$capacidades = $this->db->get("tb_tipo_aires_acondicionados");
		
		$listado["capacidades"] = $capacidades->result_array();
		
		$this->db->reset_query();
		$this->db->select("id, capacidad, marca, modelo, tipo_equipo");
		$this->db->where("tipo_equipo !=", "Mochila");
		$this->db->group_by("capacidad, marca, tipo_equipo");
		
		$capacidades_otros = $this->db->get("tb_tipo_aires_acondicionados");
		
		$listado["capacidades2"] = $capacidades_otros->result_array();
		
		$this->db->reset_query();
		$this->db->select("marca, tipo_equipo");
		$this->db->group_by("tipo_equipo, marca");
		$tipos_equipo = $this->db->get("tb_tipo_aires_acondicionados");
		
		$listado["tipos_equipo"] = $tipos_equipo->result_array();
		
		return $listado;
	}
	
	public function get_tipos_equipo()
	{
		$this->db->select("tipo_equipo");
		$this->db->group_by("tipo_equipo");
		$tipos_equipo = $this->db->get("tb_tipo_aires_acondicionados");
		return $tipos_equipo->result_array();
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_tipo_aires_acondicionados",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$antena = $this->db->get_where("tb_tipo_aires_acondicionados",array('id' => $id),1);
		return $antena->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_aires_acondicionados');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_tipo_aires_acondicionados',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_aires_acondicionados');
	}
}