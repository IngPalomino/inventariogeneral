<?php
class Logs_bodega_antenas_iden_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}

	public function get_logs()
	{
		$logs = $this->db->get("tb_logs_bodega_antenas_iden");
		return $logs->result_array();
	}

	public function ingreso_de_datos($user = FALSE,$datos)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		$datos["tipo"]	= "INSERT";
		$datos["id"]	= $this->db->insert_id();
		$datos["query"]	= $this->db->last_query();

		$data = array(
				"usuario"	=> $user,
				"antena_iden"	=> $datos["id"],
				"evento"	=> json_encode($datos)
			);

		$this->db->insert("tb_logs_bodega_antenas_iden",$data);
	}

	public function actualizacion_de_datos($user = FALSE,$id,$datos,$comentario = FALSE)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		if($comentario === FALSE)
		{
			$comentario = "";
		}

		$datos["tipo"] = "UPDATE";
		$datos["query"]	= $this->db->last_query();

		$data = array(
				"usuario"	=> $user,
				"antena_iden"	=> $id,
				"evento"	=> json_encode($datos),
				"comentario"=> $comentario
			);

		$this->db->insert("tb_logs_bodega_antenas_iden",$data);
		return json_encode($data);
	}
}