<?php
class Proyecto_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_Proyecto()
	{
		$this->db->order_by("id","ASC");
		$proyectos = $this->db->get("tb_proyecto");
		return $proyectos->result_array();
	}

	public function edit($id)
	{
		$proyecto = $this->db->get_where("tb_proyecto",array('id' => $id),1);
		return $proyecto->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_proyecto');
	}

	public function insert($datos)
	{
		$this->db->insert('tb_proyecto',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_proyecto');
	}
}