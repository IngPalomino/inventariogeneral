<?php
class Ubicacion_controlador_rectif_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$this->db->order_by("ubicacion_controlador_rectif","ASC");
		$types = $this->db->get("tb_ubicacion_controlador_rectif");
		return $types->result_array();
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_ubicacion_controlador_rectif",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$ubicacion = $this->db->get_where("tb_ubicacion_controlador_rectif",array('id' => $id),1);
		return $ubicacion->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_ubicacion_controlador_rectif');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_ubicacion_controlador_rectif',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_ubicacion_controlador_rectif');
	}
}