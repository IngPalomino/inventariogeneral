<?php
class Tipo_odu_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_types()
	{
		$this->db->order_by("tipo","ASC");
		$types = $this->db->get("tb_tipo_odu");
		return $types->result_array();
	}

	/*public function get_types()
	{
		$listado = array(
				"marcas" => array(),
				"modelos" => array(),
				"tipos" => array(),
				"bandas" => array(),
				"frecuencias" => array()
			);
		
		$this->db->select("marca");
		$this->db->group_by("marca");
		$marcas = $this->db->get("tb_tipo_odu");
		
		$listado["marcas"] = $marcas->result_array();
		
		$this->db->reset_query();
		$this->db->select("marca,modelo");
		$this->db->group_by("modelo");
		$modelos = $this->db->get("tb_tipo_odu");
		
		$listado["modelos"] = $modelos->result_array();
		
		$this->db->reset_query();
		$this->db->select("marca,modelo,tipo");
		$this->db->group_by("tipo,modelo");
		$tipos = $this->db->get("tb_tipo_odu");
		
		$listado["tipos"] = $tipos->result_array();
		
		$this->db->reset_query();
		$this->db->select("marca,modelo,tipo,banda");
		$this->db->group_by("banda,tipo,modelo");
		$bandas = $this->db->get("tb_tipo_odu");
		
		$listado["bandas"] = $bandas->result_array();
		
		$this->db->reset_query();
		$this->db->order_by("frec_min","ASC");
		$frecuencias = $this->db->get("tb_tipo_odu");
		
		$listado["frecuencias"] = $frecuencias->result_array();
		
		return $listado;
	}*/

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_tipo_odu",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$odu = $this->db->get_where("tb_tipo_odu",array('id' => $id),1);
		return $odu->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_odu');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_tipo_odu',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_odu');
	}
}