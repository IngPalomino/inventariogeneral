<?php
class Ubicacion_equipos_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_ubicaciones()
	{
		$this->db->order_by("id","ASC");
		$ubicaciones = $this->db->get("tb_ubicacion_equipos");
		return $ubicaciones->result_array();
	}

	public function edit($id)
	{
		$ubicacion = $this->db->get_where("tb_ubicacion_equipos",array('id' => $id),1);
		return $ubicacion->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_ubicacion_equipos');
	}

	public function insert($datos)
	{
		$this->db->insert('tb_ubicacion_equipos',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_ubicacion_equipos');
	}
}