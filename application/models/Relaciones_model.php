<?php
class Relaciones_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_joins()
	{
		$this->db->order_by("id","ASC");
		$relaciones = $this->db->get("tb_relaciones");

		return $relaciones->result_array();
	}
	
	public function get_joins_id($id)
	{
		$this->db->order_by("id","ASC");
		$relacion = $this->db->get_where("tb_relaciones",array('id' => $id));

		$relacion_array = $relacion->result_array();

		return $relacion_array;
	}

	public function insert($info)
	{
		$this->db->insert('tb_relaciones',$info);
	}
	
	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_relaciones');
	}
	
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_relaciones');
	}
}