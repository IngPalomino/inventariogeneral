<?php
class Lineas_electricas_conexion_linea_electrica_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$this->db->order_by("conexion_linea_electrica","ASC");
		$types = $this->db->get("tb_lineas_electricas_conexion_linea_electrica");
		return $types->result_array();
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_lineas_electricas_conexion_linea_electrica",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$antena = $this->db->get_where("tb_lineas_electricas_conexion_linea_electrica",array('id' => $id),1);
		return $antena->result_array();
	}

	public function cleanUp($linea)
	{
		$this->db->where('lineas_electricas',$linea);
		return $this->db->delete('tb_lineas_electricas_conexion_linea_electrica');
	}
	
	public function update($id,$datos)
	{
		/*$this->db->where('id',$id);
		$this->db->delete('tb_lineas_electricas_conexion_linea_electrica');
		return $this->db->insert('tb_lineas_electricas_conexion_linea_electrica',$datos);*/
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_lineas_electricas_conexion_linea_electrica');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_lineas_electricas_conexion_linea_electrica',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_lineas_electricas_conexion_linea_electrica');
	}
}