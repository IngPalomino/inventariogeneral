<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_sistema_combustible_model extends CI_Model {

	private $table;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = 'tb_bodega_sistema_combustible';
	}

	public function insert($datos)
	{
		return $this->db->insert($this->table, $datos);
	}

	public function get_sistema_combustible($filtros = array())
	{
		if(isset($filtros['almacen']))
		{
			$this->db->where(array('tipo_almacen' => 1, 'almacen' => $filtros['almacen']));
		}
		elseif(isset($filtros['sitio']))
		{
			$this->db->where(array('tipo_almacen' => 2, 'almacen' => $filtros['sitio']));
		}

		$elementos_array = $this->db->order_by('id', 'ASC')
									->where(array('eliminado' => 0))
									->get($this->table)
									->result_array();
		$elementos_array = $this->Populate_model->populate($this->table, $elementos_array, TRUE);
		
		foreach($elementos_array as &$elemento)
		{
			$elemento["proveniente_sitio"] = $this->db->select('id, nombre_completo')
														->get_where('tb_status_site', array('id' => $elemento['proveniente_sitio']), 1)
														->row_array();

			$tipo_elemento_obj = $this->db->get_where('tb_elementos_sistema_combustible', array('id' => $elemento['elemento_sistema_combustible']['id']), 1)
											->row_array();

			$elemento['tipo_elemento'] = $this->db->get_where($tipo_elemento_obj['tipo_elemento'], array('id' => $elemento['tipo_elemento']), 1)
														->row_array();

			switch($elemento['tipo_almacen'])
			{
				case 1:
					$elemento['almacen'] = $this->db->get_where('tb_almacenes', array('id' => $elemento['almacen']))
													->row_array();
					break;
				case 2:
					$almacen = $this->db->select('id, nombre_completo')
										->get_where('tb_status_site', array('id' => $elemento['almacen']))
										->row_array();
					$elemento['almacen'] = array(
							'id' => $almacen['id'],
							'almacen' => $almacen['nombre_completo']
						);
					break;
			}
		}

		return $elementos_array;
	}

	public function edit($id)
	{
		$antena = $this->db->get_where($this->table, array('id' => $id), 1)
							->result_array();
		return $this->Populate_model->populate($this->table, $antena, TRUE, TRUE);
		return $antena;
	}

	public function update($id, $datos)
	{
		return $this->db->update($this->table, $datos, array('id' => $id));
	}

	public function delete($id)
	{
		return $this->db->delete($this->table, array('id' => $id));
	}
}