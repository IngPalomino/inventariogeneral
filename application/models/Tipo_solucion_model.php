<?php
class Tipo_solucion_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_Proyecto()
	{
		$this->db->order_by("id","ASC");
		$tipo_soluciones = $this->db->get("tb_tipo_solucion");
		return $tipo_soluciones->result_array();
	}

	public function edit($id)
	{
		$tipo_solucion = $this->db->get_where("tb_tipo_solucion",array('id' => $id),1);
		return $tipo_solucion->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_solucion');
	}

	public function insert($datos)
	{
		$this->db->insert('tb_tipo_solucion',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_solucion');
	}
}