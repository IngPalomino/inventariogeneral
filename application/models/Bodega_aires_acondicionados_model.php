<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_aires_acondicionados_model extends CI_Model
{
	private $table;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = 'tb_bodega_aires_acondicionados';
	}
	
	public function get_aires_acondicionados($filtros = array())
	{
		if(isset($filtros['almacen']))
		{
			$this->db->where(array('tipo_almacen' => 1, 'almacen' => $filtros['almacen']));
		}
		elseif(isset($filtros['sitio']))
		{
			$this->db->where(array('tipo_almacen' => 2, 'almacen' => $filtros['sitio']));
		}
		
		$aires = $this->db->order_by('id', 'ASC')
							->where(array('eliminado' => 0))
							->get($this->table)
							->result_array();
		$aires_array = $this->Populate_model->populate($this->table, $aires, TRUE);
		
		foreach($aires_array as &$aire)
		{
			$aire['proveniente_sitio'] = $this->db->select('id, nombre_completo')
													->get_where('tb_status_site', array('id' => $aire['proveniente_sitio']), 1)
													->row_array();

			switch($aire['tipo_almacen'])
			{
				case 1:
					$aire['almacen'] = $this->db->get_where('tb_almacenes', array('id' => $aire['almacen']), 1)
												->row_array();
					break;
				case 2:
					$almacen = $this->db->select('id, nombre_completo')
									->get_where('tb_status_site', array('id' => $aire['almacen']), 1)
									->row_array();
					$aire['almacen'] = array(
							'id' => $almacen['id'],
							'almacen' => $almacen['nombre_completo']
						);
					break;
			}
		}
		
		return $aires_array;
	}

	public function edit($id)
	{
		$grupo = $this->db->get_where($this->table, array('id' => $id), 1)
							->result_array();
		return $this->Populate_model->populate($this->table, $grupo, TRUE, TRUE);
	}

	public function update($id, $datos)
	{
		return $this->db->set($datos)
						->where('id', $id)
						->update($this->table);
	}

	public function insert($datos)
	{
		return $this->db->insert($this->table, $datos);
	}
}