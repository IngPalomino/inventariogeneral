<?php
class Modulos_rectif_model extends CI_Model {

	public function __construct()
	{
		require_once("Populate.php");
		$this->load->database();
		$this->populate = new Populate();
	}

	public function get_modulos_rectif()
	{
		$this->db->order_by("id","ASC");
		$modulos = $this->db->get("tb_modulos_rectif");
		return $modulos->result_array();
	}

	public function get_modulos_rectif_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$modulos_rectif = $this->db->get_where("tb_modulos_rectif",array('controlador_rectif' => $id, 'eliminado' => 0));

		$modulos_rectif_array = $modulos_rectif->result_array();

		$modulos_rectif_array = $this->populate->populate("tb_modulos_rectif",$modulos_rectif_array,true);

		return $modulos_rectif_array;
	}

	public function edit($id)
	{
		$modulo_rectif = $this->db->get_where("tb_modulos_rectif",array('id' => $id),1);
		$modulo_rectif_array = $modulo_rectif->result_array();

		$modulo_rectif_array = $this->populate->populate("tb_modulos_rectif",$modulo_rectif_array,true,true);

		return $modulo_rectif_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_modulos_rectif');
		return $resp;
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_modulos_rectif',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_modulos_rectif');
	}

	public function delete_where($parametros)
	{
		foreach($parametros as $campo => $valor)
		{
			$this->db->where($campo,$valor);
		}

		return $this->db->delete("tb_modulos_rectif");
	}
}