<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_masivo_bancos_baterias_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function nombre_completo_valido($nombre)
	{
		$query = $this->db->select('id')
						->get_where('tb_status_site', array('nombre_completo' => $nombre), 1);

		return ($query->num_rows() === 1) ? $query->row_array() : FALSE;
	}

	public function nombre_codigo_validos($codigo, $nombre)
	{
		$query = $this->db->select('id')
						->get_where('tb_status_site', array('codigo' => $codigo, 'nombre_completo' => $nombre), 1);

		return ($query->num_rows() === 1) ? $query->row_array() : FALSE;
	}

	public function marca_valida($marca)
	{
		$query = $this->db->get_where('tb_tipo_bancos_baterias', array('marca' => $marca), 1);

		return ($query->num_rows() > 0) ? TRUE : FALSE;
	}

	public function modelo_valido($modelo)
	{
		$query = $this->db->get_where('tb_tipo_bancos_baterias', array('modelo' => $modelo), 1);

		return ($query->num_rows() > 0) ? TRUE : FALSE;
	}

	public function tipo_banco_baterias_valido($marca, $modelo)
	{
		$query = $this->db->get_where('tb_tipo_bancos_baterias', array('marca' => $marca, 'modelo' => $modelo), 1);

		return ($query->num_rows() === 1) ? $query->row_array() : FALSE;
	}

	public function validate_date($date, $format = 'Y-m-d H:i:s')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

	public function energizacion_valida($energizacion)
	{
		$query = $this->db->get_where('tb_energizacion_controlador_rectif', array('energizacion_controlador_rectif' => $energizacion), 1);

		return ($query->num_rows() === 1) ? $query->row_array() : FALSE;
	}

	public function rectificador_valido($sitio, $energizacion)
	{
		$query = $this->db->get_where('tb_controlador_rectif', array('sitio' => $sitio, 'energizacion_controlador_rectif' => $energizacion, 'eliminado' => 0), 1);

		return ($query->num_rows() === 1) ? $query->row_array() : FALSE;
	}

	public function existe_banco($id, $sitio)
	{
		$query = $this->db->select('cr.id, cr.sitio')
							->join('tb_controlador_rectif cr', 'cr.id = bb.controlador_rectif', 'left')
							->get_where('tb_controlador_rectif_bancos_baterias bb', array('bb.id' => $id, 'cr.sitio' => $sitio, 'cr.eliminado' => 0, 'bb.eliminado' => 0), 1);

		return ($query->num_rows() === 1) ? $query->row_array() : FALSE;
	}

	public function existe_numero_serie($ns)
	{
		$query = $this->db->get_where('tb_celdas_controlador_rectif_bancos_baterias', array('numero_serie' => $ns, 'eliminado' => 0));

		return ($query->num_rows() > 0) ? TRUE : FALSE;
	}

	public function get_bancos()
	{
		return $this->db->order_by('codigo ASC, id ASC')
						->select('ss.codigo, ss.nombre_completo, bb.id, tbb.marca, tbb.modelo, ecr.energizacion_controlador_rectif')
						->join('tb_controlador_rectif cr', 'cr.id = bb.controlador_rectif', 'left')
						->join('tb_status_site ss', 'ss.id = cr.sitio', 'left')
						->join('tb_tipo_bancos_baterias tbb', 'tbb.id = bb.tipo_bancos_baterias', 'left')
						->join('tb_energizacion_controlador_rectif ecr', 'ecr.id = cr.energizacion_controlador_rectif', 'left')
						->where(array('cr.eliminado' => 0, 'bb.eliminado' => 0))
						->get('tb_controlador_rectif_bancos_baterias bb')
						->result_array();
	}

	public function registrar_ingreso_masivo($usuario)
	{
		$datos['usuario'] = $usuario;
		$datos['tabla'] = 8;

		if ($this->db->insert('tb_ingreso_masivo', $datos))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}

	public function ingresar_temporal($datos)
	{
		return array(
				'resp' => $this->db->insert('tb_temp_bancos_baterias', $datos),
				'last_id' => $this->db->insert_id()
			);
	}

	public function ingresar_temporal_celdas($datos)
	{
		return $this->db->insert('tb_temp_celdas_bancos_baterias', $datos);
	}

	public function update($id, $datos)
	{
		return $this->db->set($datos)
						->where('id', $id)
						->update('tb_temp_bancos_baterias');
	}

	public function get_solicitudes($usuario)
	{
		$query = $this->db->order_by('id', 'DESC')
							->get_where('tb_ingreso_masivo', array('usuario' => $usuario, 'tabla' => 8))
							->result_array();

		return $this->Populate_model->populate('tb_ingreso_masivo', $query, TRUE);
	}
}