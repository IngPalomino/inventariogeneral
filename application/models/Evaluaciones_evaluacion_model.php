<?php
class Evaluaciones_evaluacion_model extends CI_Model {
	public function __construct()
	{
		$this->load->database("evaluaciones");
	}

	public function listar()
	{
		$this->db->order_by("id","ASC");
		$query = $this->db->get("tb_evaluacion");
		$resultado_array = $query->result_array();

		/*$resultado_array = $this->populate("tb_status_site",$resultado_array,true);*/

		return $resultado_array;
	}

	public function editar($id)
	{
		$query = $this->db->get_where("tb_evaluacion",array('id' => $id),1);
		$resultado_array = $query->result_array();

		/*$resultado_array = $this->populate("tb_status_site",$resultado_array,true);*/

		return $resultado_array;
	}

	public function actualizar($id,$info)
	{
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_evaluacion',$info);

		return $resp;
	}

	public function insertar($info)
	{
		$resp = $this->db->insert('tb_evaluacion',$info);

		$last_id = $this->db->insert_id();

		$salida = array(
				"resp"		=>	$resp,
				"last_id"	=>	$last_id
			);

		return $salida;
	}

	public function listar_activas()
	{
		$this->db->where(array("eliminado" => 0));
		$this->db->order_by("id","ASC");
		$query = $this->db->get("tb_evaluacion");
		$resultado_array = $query->result_array();

		return $resultado_array;
	}
}
?>