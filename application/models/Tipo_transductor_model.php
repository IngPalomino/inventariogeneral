<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_transductor_model extends CI_Model {

	private $table;

	public function __construct()
	{
		$this->load->database();
		$this->table = 'tb_tipo_transductor';
	}

	public function insert($datos)
	{
		return $this->db->insert($this->table, $datos);
	}

	public function get_types()
	{
		$listado['marcas'] = $this->db->select('marca')
										->group_by('marca')
										->get($this->table)
										->result_array();

		$listado['modelos'] = $this->db->order_by('modelo', 'ASC')
										->get($this->table)
										->result_array();

		return $listado;
	}

	public function update($id, $datos)
	{
		return $this->db->update($this->table, $datos, array('id' => $id));
	}

	public function delete($id)
	{
		return $this->db->delete($this->table, array('id' => $id));
	}
}