<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antenas_iden_model extends CI_Model {

	private $table;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = 'tb_antenas_iden';
	}

	public function get_antenas_iden()
	{
		return $this->db->order_by('id', 'ASC')
						->get($this->table)
						->result_array();
	}

	public function get_antenas_iden_sitio($id)
	{
		$antenas_iden = $this->db->order_by('sector', 'ASC')
									->get_where($this->table, array('sitio' => $id, 'eliminado' => 0))
									->result_array();

		return $this->Populate_model->populate($this->table, $antenas_iden, TRUE);
	}

	public function edit($id)
	{
		$antena_iden = $this->db->get_where($this->table, array('id' => $id), 1)
								->result_array();

		return $this->Populate_model->populate($this->table, $antena_iden, TRUE, TRUE);
	}

	public function update($id, $datos)
	{
		return $this->db->set($datos)
						->where('id', $id)
						->update($this->table);
	}

	public function insert($datos)
	{
		return array(
				'resp' => $this->db->insert($this->table, $datos),
				'last_id' => $this->db->insert_id()
			);
	}

	public function delete($id)
	{
		return $this->db->where('id', $id)
						->delete($this->table);
	}

	public function get_sitio_antena($id)
	{
		return $this->db->select('ss.id, ss.nombre_completo')
						->join('tb_status_site ss', 'ss.id = aa.sitio', 'left')
						->get_where($this->table.' aa', array('aa.id' => $id))
						->row_array();
	}
}