<?php
class Sistema_suministro_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$this->db->order_by("sistema_suministro","ASC");
		$types = $this->db->get("tb_sistema_suministro");
		return $types->result_array();
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_sistema_suministro",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$antena = $this->db->get_where("tb_sistema_suministro",array('id' => $id),1);
		return $antena->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_sistema_suministro');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_sistema_suministro',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_sistema_suministro');
	}
}