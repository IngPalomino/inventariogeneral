<?php
class Ups_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function get_ups()
	{
		$this->db->order_by("id","ASC");
		$ups = $this->db->get("tb_ups");
		return $ups->result_array();
	}

	public function get_ups_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$ups = $this->db->get_where("tb_ups",array('sitio' => $id, 'eliminado' => 0,'id_sala_tecnica'=> 0));
		$ups_array = $ups->result_array();
		$ups_array = $this->Populate_model->populate("tb_ups",$ups_array,true);
		return $ups_array;
	}

	public function get_ups_sitio_mso($id)
	{
		$this->db->order_by("id","ASC");
		$ups = $this->db->get_where("tb_ups",array('sitio' => $id, 'eliminado' => 0,'id_sala_tecnica >'=> 0));
		$ups_array = $ups->result_array();
		$ups_array = $this->Populate_model->populate("tb_ups",$ups_array,true);
		return $ups_array;
	}



	public function edit($id)
	{
		$ups = $this->db->get_where("tb_ups",array('id' => $id),1);
		$ups_array = $ups->result_array();
		$ups_array = $this->Populate_model->populate("tb_ups",$ups_array,true,true);
		return $ups_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_ups');
		return $resp;
	}
	
	public function get_sitios_ups()
	{
		$this->db->order_by("codigo","ASC");
		$this->db->select("ss.nombre_completo, ss.id");
		$this->db->join("tb_status_site ss","ss.id = gg.sitio","left");
		$sitios = $this->db->get("tb_ups gg");
		return $sitios->result_array();
	}
	
	public function insert($datos)
	{
		$resultado = $this->db->insert('tb_ups',$datos);
		$resp = array(
			"resp" => $resultado,
			"last_id" => $this->db->insert_id()
		);		
		return $resp;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_ups');
	}

	public function get_sitio_ups($id)
	{
		$this->db->select("ss.id, ss.nombre_completo");
		$this->db->join("tb_status_site ss","ss.id = aa.sitio","left");
		$sitio = $this->db->get_where("tb_ups aa", array("aa.id" => $id), 1);
		return $sitio->row_array();
	}
}