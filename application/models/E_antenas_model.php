<?php
class E_antenas_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_antenas()
	{
		$this->db->order_by("antenna_device_id","ASC");
		$antenas = $this->db->get("tb_e_antenas");
		return $antenas->result_array();
	}

	public function get_antenas_sitio($id)
	{
		$this->db->order_by("antenna_device_id","ASC");
		$antenas = $this->db->get_where("tb_e_antenas",array('sitio' => $id));
		$antenas_array = $antenas->result_array();
		return $antenas_array;
	}

	public function edit($id)
	{
		$antena = $this->db->get_where("tb_e_antenas",array('id' => $id),1);
		$antena_array = $antena->result_array();
		return $antena_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_e_antenas');
		return $resp;
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_e_antenas',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_e_antenas');
	}
}