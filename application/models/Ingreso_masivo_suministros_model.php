<?php
class Ingreso_masivo_suministros_model extends CI_Model{

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function esNombreCompletoValido($nombre)
	{
		$sitio = $this->db->select("id")
						->get_where("tb_status_site", array("nombre_completo" => $nombre), 1);

		if($sitio->num_rows() > 0)
		{
			return $sitio->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esNombreCodigoValidos($codigo, $nombre)
	{
		$sitio = $this->db->select("id")
						->get_where("tb_status_site", array("codigo" => $codigo, "nombre_completo" => $nombre), 1);

		if($sitio->num_rows() > 0)
		{
			return $sitio->row_array();
		}
		else
		{
			return false;
		}
	}

	public function existeSuministroEnSitio($sitio)
	{
		$query = $this->db->get_where("tb_suministros", array("sitio" => $sitio, "eliminado" => 0), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esTipoAlimentacionValido($tipo)
	{
		$query = $this->db->get_where("tb_tipo_alimentacion", array("tipo_alimentacion" => $tipo), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esOpcionTarifariaValida($opcion)
	{
		$query = $this->db->get_where("tb_opcion_tarifaria", array("opcion_tarifaria" => $opcion), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esConcesionariaValida($concesionaria)
	{
		$query = $this->db->get_where("tb_concesionaria_suministro", array("concesionaria_suministro" => $concesionaria), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esConexionValida($conexion)
	{
		$query = $this->db->get_where("tb_conexion_suministro", array("conexion_suministro" => $conexion), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esSistemaValido($sistema)
	{
		$query = $this->db->get_where("tb_sistema_suministro", array("sistema_suministro" => $sistema), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function registrar_ingreso_masivo($usuario)
	{
		$datos["usuario"] = $usuario;
		$datos["tabla"] = 2;

		if($this->db->insert("tb_ingreso_masivo", $datos))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}

	public function ingresar_temporal($datos)
	{
		return $this->db->insert("tb_temp_suministros", $datos);
	}

	public function update($id, $datos)
	{
		$query = $this->db->set($datos)
							->where("id", $id)
							->update("tb_temp_suministros");
		return $query;
	}

	public function get_solicitudes($usuario)
	{
		$this->db->order_by("id", "DESC");
		$query = $this->db->get_where("tb_ingreso_masivo", array("usuario" => $usuario, "tabla" => 2));
		$soliticudes = $query->result_array();
		$soliticudes = $this->Populate_model->populate("tb_ingreso_masivo", $soliticudes, true);
		return $soliticudes;
	}
}