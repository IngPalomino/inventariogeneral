<?php
class Tipo_alimentacion_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$this->db->order_by("tipo_alimentacion","ASC");
		$listado = $this->db->get("tb_tipo_alimentacion");
		return $listado->result_array();
	}
}