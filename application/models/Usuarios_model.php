<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

	private $table;

	public function __construct()
	{
		$this->load->model('Populator_model', 'Populate_model');
		$this->load->model('Usuarios_grupos_model');
		$this->table = 'tb_usuarios';
	}

	public function insert($data, $groups = array())
	{
		if ($this->identity_check($data['correo']))
		{
			$this->app->set_error('El correo electrónico ya existe.');
			return FALSE;
		}

		$data['contrasena'] = $this->hash_password($data['contrasena']);

		$this->db->insert($this->table, $data);

		$id_usuario = $this->db->insert_id($this->table . '_id_seq');

		if ( ! empty($groups))
		{
			foreach ($groups as $group)
			{
				$this->add_to_group($group, $id_usuario);
			}
		}

		return isset($id_usuario) ? $id_usuario : FALSE;
	}

	public function get_users()
	{
		return $this->Populate_model->select('id_usuario, correo, nombres, apellido_paterno, apellido_materno, telefono, activo, created_at')
									->order_by('apellido_paterno', 'ASC')
									->get($this->table)
									->result_array()
									->populate();
	}

	public function get_user($id)
	{
		return $this->db->select('id_usuario, correo, nombres, apellido_paterno, apellido_materno, telefono, created_at')
						->get_where($this->table, array('id_usuario' => $id), 1)
						->row_array();
	}

	public function update($id, array $data)
	{
		$user = $this->get_user($id);

		$this->db->trans_begin();

		if (array_key_exists('correo', $data) && $this->identity_check($data['correo']) && $user['correo'] !== $data['correo'])
		{
			$this->db->trans_rollback();
			$this->app->set_error('Correo duplicado.');

			return FALSE;
		}

		$data = $this->app->_filter_data($this->table, $data);

		if (array_key_exists('correo', $data) || array_key_exists('contrasena', $data))
		{
			if (array_key_exists('contrasena', $data))
			{
				if ( ! empty($data['contrasena']))
				{
					$data['contrasena'] = $this->hash_password($data['contrasena']);
				}
				else
				{
					unset($data['contrasena']);
				}
			}
		}

		$this->db->update($this->table, $data, array('id_usuario' => $id));

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$this->app->set_error('No se pudo actualizar.');

			return FALSE;
		}

		$this->db->trans_commit();

		return TRUE;
	}

	public function delete($id)
	{
		$this->db->where('id_usuario',$id);
		$this->db->delete('tb_usuarios');
	}

	public function get_responsable($id)
	{
		$this->db->select("id_usuario,correo,nombres,apellido_paterno,apellido_materno,telefono,created_at");
		$usuario = $this->db->get_where("tb_usuarios",array('id_usuario' => $id),1);
		return $usuario->row_array();
	}

	public function edit($id)
	{
		$listado = $this->db->get_where("tb_usuarios",array("id_usuario" => $id));

		return $listado->result_array();
	}

	public function confirm_user($user = FALSE)
	{
		if($user === FALSE)
		{
			return FALSE;
		}

		$usuario = $this->db->get_where("tb_usuarios",array('correo' => $user),1);
		return $usuario->result_array();
	}

	public function rebootPassword($id)
	{
		$ppww = "entel@oym";
		$algo = CRYPT_BLOWFISH;
		$options = ['cost'=>10];

		$datos['contrasena'] = password_hash($ppww,$algo,$options);

		$this->db->set($datos);
		$this->db->where('id_usuario',$id);
		$this->db->update('tb_usuarios');
	}

	public function newPassword($id,$pass)
	{
		$algo = CRYPT_BLOWFISH;
		$options = ['cost'=>10];

		$datos['contrasena'] = password_hash($pass,$algo,$options);

		$this->db->set($datos);
		$this->db->where('id_usuario',$id);
		$resp = $this->db->update('tb_usuarios');

		return $resp;
	}

	public function select_by_user($usuario = FALSE)
	{
		if( $usuario === FALSE )
		{
			return FALSE;
		}

		$patron1 = "/([\w.]+)/";
		$patron2 = "/([\w.]+)@osctelecoms.com([A-Za-z.]{0,4})/";
		$patron3 = "/([\w.]+)@entel.pe/";
		$coincidencias = array();

		$esCorreo = strpos($usuario, "@");

		if( ($esCorreo != false) && ($esCorreo > 0) )
		{
			if(preg_match_all($patron2, $usuario, $coincidencias) || preg_match_all($patron3, $usuario, $coincidencias))
			{
				$busqueda = $coincidencias[1][0]."@";
			}
			else
			{
				return false;
			}
		}
		else
		{
			if(preg_match_all($patron1, $usuario, $coincidencias))
			{
				$busqueda = $coincidencias[1][0]."@";
			}
			else
			{
				return false;
			}
		}

		$usuario = $this->db->select("id_usuario, nombres, apellido_paterno, apellido_materno, correo, evaluador, responsable, fecha_inicio, fecha_fin")
					->like("correo",$busqueda,"after")
					->get("tb_usuarios");
		$usuario = $usuario->result_array();

		if( count($usuario) > 0 )
		{
			return $usuario[0];
		}
		else
		{
			return false;
		}
	}

	public function getUserInfo($id)
    {
        $q = $this->db->get_where('tb_usuarios', array('id_usuario' => $id), 1);  
        if($this->db->affected_rows() > 0)
        {
            $row = $q->row_array();
            return $row;
        }
        else
        {
            return false;
        }
    }

	public function isDuplicate($email)
    {
    	$q = $this->db->get_where('tb_usuarios', array('correo' => $email), 1);
        return (($q->num_rows() > 0) ? TRUE : FALSE);
    }

    public function insertToken($usuario)
    {   
        $token = substr(sha1(rand()), 0, 30);
        $date = date('Y-m-d');
        
        $string = array(
                'token' => $token,
                'usuario' => $usuario
            );
        $query = $this->db->insert_string('tb_tokens',$string);
        $this->db->query($query);
        return $token . $usuario;
    }

    public function getUserInfoByEmail($correo)
    {
        $q = $this->db->get_where('tb_usuarios', array('correo' => $correo), 1);  
        if($this->db->affected_rows() > 0)
        {
            $row = $q->row_array();
            return $row;
        }
        else
        {
            return false;
        }
    }

    public function isTokenValid($token)
    {
       $tkn = substr($token,0,30);
       $uid = substr($token,30);
       
        $q = $this->db->get_where('tb_tokens', array('token' => $tkn, 'usuario' => $uid), 1);
        $row = $q->row_array();

        if( isset($row["id"]) )
        {
            $created = $row["created_at"];
            $createdTS = strtotime($created);
            $today = date('Y-m-d H:i:s');
            $todayTS = strtotime($today);
            $createdTS = $createdTS + 900;
            
            if($createdTS < $todayTS)
            {
                return false;
            }
            
            $user_info = $this->getUserInfo($row["usuario"]);
            return $user_info;
        }
        else
        {
            return false;
        }
    }

    public function get_grupos_usuario($idUsuario)
	{
		if( $idUsuario === FALSE )
		{
			return FALSE;
		}

		$grupos = $this->db->select("grupo")
					->get_where("tb_usuarios_grupos", array("usuario" => $idUsuario));
		$grupos_array = $grupos->result_array();

		return $grupos_array;
	}

	public function getUserEmailByModulo($modulo)
	{
		$this->load->model('Usuarios_grupos_model');
		$usuarios = $this->Usuarios_grupos_model->get_usuarios_grupos($modulo);
		$correos = array();

		foreach ($usuarios as &$usuario)
		{
			$info = $this->db->get_where("tb_usuarios", array("id_usuario" => $usuario["usuarios"]));
			$info_usuario = $info->row_array();
			array_push($correos, $info_usuario["correo"]);
		}

		return $correos;
	}

	public function hash_password($password)
	{
		if (empty($password))
		{
			return FALSE;
		}

		$algo = CRYPT_BLOWFISH;
		$options = ['cost' => 10];

		return password_hash($password, $algo, $options);
	}

	public function identity_check($identity = '')
	{
		if (empty($identity))
		{
			return FALSE;
		}

		return $this->db->where('correo', $identity)
						->count_all_results($this->table) > 0;
	}

	public function add_to_group($groups, $user)
	{
		if ( ! is_array($groups))
		{
			$groups = array($groups);
		}

		$return = 0;

		foreach ($groups as $group)
		{
			if ($this->Usuarios_grupos_model->insert(array('grupo' => (float) $group, 'usuario' => (float) $user)))
			{
				$return += 1;
			}
		}

		return $return;
	}

	public function remove_from_group($user = FALSE, $groups = FALSE)
	{
		if (empty($user))
		{
			return FALSE;
		}

		if ( ! empty($groups))
		{
			if ( ! is_array($groups))
			{
				$groups = array($groups);
			}

			foreach ($groups as $group)
			{
				$this->Usuarios_grupos_model->delete(array('grupo' => (float) $group, 'usuario' => (float) $user));
			}

			$return = TRUE;
		}
		else
		{
			$return = $this->Usuarios_grupos_model->delete(array('usuario' => (float) $user));
		}

		return $return;
	}
}