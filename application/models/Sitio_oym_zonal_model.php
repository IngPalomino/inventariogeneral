<?php
class Sitio_oym_zonal_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function insert($datos)
	{
		$salida = array(
				"resp" => false,
				"id" => null
			);
		$resp = $this->db->insert('tb_sitio_oym_zonal',$datos);

		if($resp)
		{
			$salida["resp"] = $resp;
			$salida["id"] = $this->db->insert_id();
		}

		return $salida;
	}
}