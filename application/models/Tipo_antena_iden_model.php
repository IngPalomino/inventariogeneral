<?php
class Tipo_antena_iden_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$listado = array(
				"marcas" => array(),
				"modelos" => array()
			);
		
		$this->db->select("marca");
		$this->db->group_by("marca");
		$marcas = $this->db->get("tb_tipo_antena_iden");
		
		$listado["marcas"] = $marcas->result_array();
		
		$this->db->reset_query();
		
		$this->db->order_by("modelo","ASC");
		$modelos = $this->db->get("tb_tipo_antena_iden");
		
		$listado["modelos"] = $modelos->result_array();
		
		return $listado;
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_tipo_antena_iden",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$antena = $this->db->get_where("tb_tipo_antena_iden",array('id' => $id),1);
		return $antena->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_antena_iden');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_tipo_antena_iden',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_antena_iden');
	}

	public function get_marcas()
	{
		$this->db->select("marca");
		$this->db->group_by("marca");
		$marcas = $this->db->get("tb_tipo_antena_iden");

		return $marcas->result_array();
	}

	public function get_modelos($marca)
	{
		$this->db->select("modelo");
		$this->db->group_by("modelo");
		$this->db->where(array("marca" => $marca));
		$modelos = $this->db->get("tb_tipo_antena_iden");

		return $modelos->result_array();
	}
}