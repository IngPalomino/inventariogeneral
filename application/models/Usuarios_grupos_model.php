<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_grupos_model extends CI_Model {

	private $table;

	public function __construct()
	{
		$this->load->database();
		$this->table = 'tb_usuarios_grupos';
	}

	// Métodos predeterminados
	public function insert($datos)
	{
		return $this->db->insert($this->table, $datos);
	}

	public function select()
	{

	}

	public function update($datos, $where, $value = NULL)
	{
		if ( ! is_array($where))
		{
			$where = array($where => $value);
		}

		foreach ($where as $key => $val)
		{
			$this->db->where($key, $val);
		}

		return $this->db->set($datos)
						->update($this->table);
	}

	public function delete($where, $value = NULL)
	{
		if ( ! is_array($where))
		{
			$where = array($where => $value);
		}

		foreach ($where as $key => $val)
		{
			$this->db->where($key, $val);
		}

		return $this->db->delete($this->table);
	}

	// Métodos especiales
	public function select_grupos($usuario)
	{
		$grupos = $this->db->select("grupo")
							->get_where("tb_usuarios_grupos", array("usuarios" => $usuario));

		$grupos_array = $grupos->result_array();

		return $grupos_array;
	}

	public function borrar_grupos_usuario($usuario)
	{
		$respuesta = $this->db->where("usuarios",$usuario)
								->delete("tb_usuarios_grupos");

		return $respuesta;
	}

	public function ingresar_grupos_usuario($usuario,$grupo)
	{
		$respuesta = $this->db->insert("tb_usuarios_grupos", array("usuarios" => $usuario, "grupo" => $grupo));

		return $respuesta;
	}

	public function get_usuarios_grupos($modulo)
	{
		/*$this->load->model('Grupos_modulos_model');
		$grupos = $this->Grupos_modulos_model->get_grupos_modulos($modulo);*/
		$this->load->model('Grupos_model');
		$grupos = $this->Grupos_model->get_grupos_email($modulo);

		$this->db->select("usuarios");
		foreach($grupos as &$grupo)
		{
			if(!$grupo["correo"])
			{
				if(count($grupos) == 1)
				{
					$this->db->where("grupo", $grupo["id"]);
				}
				elseif(count($grupos) > 1)
				{
					$this->db->or_where("grupo", $grupo["id"]);
				}
			}
		}
		$resp = $this->db->get('tb_usuarios_grupos');

		return $resp->result_array();
	}
}