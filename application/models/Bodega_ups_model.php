<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_ups_model extends CI_Model {

	private $table;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = 'tb_bodega_ups';
	}

	public function insert($datos)
	{
		return $this->db->insert($this->table, $datos);
	}

	public function get_ups($filtros = array())
	{
		if(isset($filtros['almacen']))
		{
			$this->db->where(array('tipo_almacen' => 1, 'almacen' => $filtros['almacen']));
		}
		elseif(isset($filtros['sitio']))
		{
			$this->db->where(array('tipo_almacen' => 2, 'almacen' => $filtros['sitio']));
		}

		$ups_array = $this->db->order_by('id', 'ASC')
									->where(array('eliminado' => 0))
									->get($this->table)
									->result_array();
		$ups_array = $this->Populate_model->populate($this->table, $ups_array, TRUE);
		
		foreach($ups_array as &$ups)
		{
			$ups["proveniente_sitio"] = $this->db->select('id, nombre_completo')
				->get_where('tb_status_site', array('id' => $ups['proveniente_sitio']), 1)
				->row_array();

			$tipo_elemento_obj = $this->db->get_where('tb_ups', array('id' => $ups['ups']['id']), 1)
											->row_array();

			$ups['tipo_ups'] = $this->db->get_where($tipo_elemento_obj['tipo_ups'], array('id' => $ups['tipo_ups']), 1)
														->row_array();

			switch($ups['tipo_almacen'])
			{
				case 1:
					$ups['almacen'] = $this->db->get_where('tb_almacenes', array('id' => $ups['almacen']))
													->row_array();
					break;
				case 2:
					$ups = $this->db->select('id, nombre_completo')
										->get_where('tb_status_site', array('id' => $ups['almacen']))
										->row_array();
					$ups['almacen'] = array(
							'id' => $almacen['id'],
							'almacen' => $almacen['nombre_completo']
						);
					break;
			}
		}

		return $ups_array;
	}

	public function edit($id)
	{
		$antena = $this->db->get_where($this->table, array('id' => $id), 1)
							->result_array();
		return $this->Populate_model->populate($this->table, $antena, TRUE, TRUE);
	}

	public function update($id, $datos)
	{
		return $this->db->update($this->table, $datos, array('id' => $id));
	}

	public function delete($id)
	{
		return $this->db->delete($this->table, array('id' => $id));
	}
}