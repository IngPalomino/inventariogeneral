<?php
class Notificaciones_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Grupos_model');
	}

	public function get_correos_grupos($modulo, $sitio)
	{
		$grupos = $this->db->select("grupos")
							->get_where("tb_grupos_modulos", array("modulo" => $modulo));
		$grupos_array = $grupos->result_array();

		$correos = $this->Grupos_model->listar_correo($grupos_array);
		$crm = $this->get_crm_grupo($sitio);
		$eliminar = (($crm["grupo"] == 8)? 11 : 8);
		$administradores = $this->get_correos_administradores();

		$correos_final = array();

		if(($key = array_search($eliminar, array_column($correos, "id"))) !== false)
		{
			array_splice($correos, $key, 1);
		}

		foreach ($correos as &$correo)
		{
			array_push($correos_final, $correo["correo"]);
		}

		if(array_search(5, array_column($correos, "id")) !== false)
		{
			array_push($correos_final, "osc.servicioaccesodered@osctelecoms.com");
		}

		if((array_search(8, array_column($correos, "id")) !== false) || (array_search(11, array_column($correos, "id")) !== false))
		{
			$ambos_crm = $this->get_ambos_crm();

			foreach($ambos_crm as &$crm)
			{
				array_push($correos_final, $crm["correo"]);
			}
		}

		foreach($correos as &$correo)
		{
			if(strpos($correo["correo"], "@entel.pe") !== false)
			{
				$otros_correos = $this->get_otros_correos($correo["id"], "@entel.pe");

				foreach($otros_correos as &$otro_correo)
				{
					array_push($correos_final, $otro_correo["correo"]);
				}
			}
			else
			{
				$correos_entel = $this->get_correos_entel($correo["id"], "@entel.pe");

				foreach($correos_entel as &$correo_entel)
				{
					array_push($correos_final, $correo_entel["correo"]);
				}
			}
		}

		foreach($administradores as &$administrador)
		{
			array_push($correos_final, $administrador["correo"]);
		}

		$correos_final = array_unique($correos_final);
		$correos_final = array_values($correos_final);

		return $correos_final;
	}

	public function get_crm_grupo($sitio)
	{
		$this->db->select("c.grupo");
		$this->db->join("tb_crm c", "c.id = sc.crm", "left");
		$crm = $this->db->get_where("tb_sitio_crm sc", array("sc.sitio" => $sitio), 1);
		return $crm->row_array();
	}

	public function get_otros_correos($grupo, $not_like)
	{
		$this->db->select("u.id_usuario, u.correo");
		$this->db->join("tb_usuarios_grupos ug", "ug.usuario = u.id_usuario", "left");
		$this->db->not_like("u.correo", $not_like, "before");
		$usuarios = $this->db->get_where("tb_usuarios u", array("ug.grupo" => $grupo));
		return $usuarios->result_array();
	}

	public function get_correos_entel($grupo, $like)
	{
		$this->db->select("u.id_usuario, u.correo");
		$this->db->join("tb_usuarios_grupos ug", "ug.usuario = u.id_usuario", "left");
		$this->db->like("u.correo", $like, "before");
		$usuarios = $this->db->get_where("tb_usuarios u", array("ug.grupo" => $grupo));
		return $usuarios->result_array();
	}

	public function get_correos_administradores()
	{
		$this->db->select("u.id_usuario, u.correo");
		$this->db->join("tb_usuarios_grupos ug", "ug.usuario = u.id_usuario", "left");
		/*$this->db->join("tb_grupos g", "g.id = ug.grupo", "left");*/
		$this->db->where("ug.grupo", 1);
		$this->db->or_where("ug.grupo", 2);
		$this->db->or_where("ug.grupo", 9);
		$administradores = $this->db->get("tb_usuarios u");
		return $administradores->result_array();
	}

	public function get_ambos_crm()
	{
		$query = $this->db->query("SELECT u.id_usuario, u.correo
									FROM tb_usuarios u
									WHERE
										EXISTS
										(
											SELECT * FROM tb_usuarios_grupos ug
											WHERE u.id_usuario = ug.usuario AND ug.grupo = 8
										)
										AND EXISTS
										(
											SELECT * FROM tb_usuarios_grupos ug
											WHERE u.id_usuario = ug.usuario AND ug.grupo = 11
										);
			");

		return $query->result_array();
	}
}