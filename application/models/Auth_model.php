<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {
	
	protected $errors;
	private $tables;

	public function __construct()
	{
		$this->load->database();
		$this->load->helper(array('cookie', 'date'));
		$this->errors = array();
		$this->tables = array(
			'usuarios' => 'tb_usuarios',
			'grupos' => 'tb_grupos',
			'usuarios_grupos' => 'tb_usuarios_grupos'
		);
	}

	public function get_user_groups($id = FALSE)
	{
		$id || $id = $this->session->userdata('id_usuario');

		return $this->db->select('g.id, g.nombre, g.descripcion')
						->join('tb_grupos g', 'g.id = ug.grupo', 'left')
						->get_where('tb_usuarios_grupos ug', array('ug.usuario' => $id))
						->result_array();
	}

	public function get_module_groups($id)
	{
		return $this->db->select("g.id, g.nombre, g.descripcion")
						->join("tb_grupos g", "g.id = gm.grupos", "left")
						->where("gm.modulo", $id)
						->or_where("gm.modulo", 1)
						->get("tb_grupos_modulos gm")
						->result_array();
	}

	public function get_crm_group($sitio)
	{
		return $this->db->select("c.grupo")
						->join("tb_crm c", "c.id = sc.crm", "left")
						->get_where("tb_sitio_crm sc", array("sc.sitio" => $sitio), 1)
						->row_array();
	}

	public function update_last_login($id)
	{
		$this->db->update("tb_usuarios", array("last_login" => time()), array("id" => $id));
		return $this->db->affected_rows() == 1;
	}

	public function set_error($error)
	{
		$this->errors[] = $error;
		return $error;
	}

	public function errors()
	{
		$return = "";

		foreach ($this->errors as $error)
		{
			$return .= $error . " ";
		}

		return $return;
	}
}