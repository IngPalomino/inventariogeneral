<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bancos_baterias_model extends CI_Model
{
	private $table;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = "tb_controlador_rectif_bancos_baterias";
	}

	public function get_bancos_baterias()
	{
		return $this->db->order_by("id", "ASC")		              
						->get($this->table)
						->result_array();
	}

	public function get_bancos_baterias_sitio($id)
	{
		$bancos_array = $this->db->order_by("bb.id", "ASC")
									->group_by("bb.controlador_rectif")
									->select("bb.controlador_rectif, tcr.marca, tcr.modelo, ecr.energizacion_controlador_rectif,
                                        bb.id_sala_tecnica")									
									->join("tb_controlador_rectif cr", "cr.id = bb.controlador_rectif", "left")
									
									->join("tb_tipo_controlador_rectif tcr", "tcr.id = cr.tipo_controlador_rectif", "left")
									
									->join("tb_energizacion_controlador_rectif ecr", "ecr.id = cr.energizacion_controlador_rectif", "left")


							->get_where($this->table." bb", array('cr.sitio' => $id, 'cr.eliminado' => 0,
							'bb.eliminado'=>0,'bb.id_sala_tecnica ' => 0))
									->result_array();

		foreach($bancos_array as &$rectificador)
		{
			$bancos = $this->db->order_by("bb.id","ASC")
								->select("cr.sitio,bb.*")
								->join("tb_controlador_rectif cr","bb.controlador_rectif = cr.id","left")
								->get_where($this->table." bb", array('cr.sitio' => $id, 'bb.eliminado' => 0,'bb.id_sala_tecnica ' => 0,'controlador_rectif' => $rectificador["controlador_rectif"]))
								->result_array();
			
			$rectificador["bancos"] = $this->Populate_model->populate($this->table, $bancos, TRUE);
		}
		return $bancos_array;
	}


	public function get_bancos_baterias_sitio_mso($id)
	{
		$bancos_array = $this->db->order_by("bb.id", "ASC")
									->group_by("bb.controlador_rectif")
									->select("bb.controlador_rectif, tcr.marca, tcr.modelo, ecr.energizacion_controlador_rectif,
                                        bb.id_sala_tecnica")									
									->join("tb_controlador_rectif cr", "cr.id = bb.controlador_rectif", "left")
									
									->join("tb_tipo_controlador_rectif tcr", "tcr.id = cr.tipo_controlador_rectif", "left")
									
									->join("tb_energizacion_controlador_rectif ecr", "ecr.id = cr.energizacion_controlador_rectif", "left")


							->get_where($this->table." bb", array('cr.sitio' => $id, 'cr.eliminado' => 0,
							'bb.eliminado'=>0,'bb.id_sala_tecnica > ' => 0))
									->result_array();

		foreach($bancos_array as &$rectificador)
		{
			$bancos = $this->db->order_by("bb.id","ASC")
								->select("cr.sitio,bb.*")
								->join("tb_controlador_rectif cr","bb.controlador_rectif = cr.id","left")
								->get_where($this->table." bb", array('cr.sitio' => $id, 'bb.eliminado' => 0,'bb.id_sala_tecnica > ' => 0,'controlador_rectif' => $rectificador["controlador_rectif"]))
								->result_array();
			
			$rectificador["bancos"] = $this->Populate_model->populate($this->table, $bancos, TRUE);
		}
		return $bancos_array;
	}

	
	public function get_bancos_baterias_sitio_resumen($id)
	{
		$bancos_baterias = $this->db->order_by("id", "ASC")
									->select("cr.sitio, ecr.energizacion_controlador_rectif, bb.*")
									->join("tb_controlador_rectif cr", "cr.id = bb.controlador_rectif", "left")
									->join("tb_energizacion_controlador_rectif ecr", "ecr.id = cr.energizacion_controlador_rectif", "left")
									->get_where($this->table." bb", array('cr.sitio' => $id, 'cr.eliminado' => 0, 'bb.eliminado' => 0))
									->result_array();
		return $this->Populate_model->populate($this->table, $bancos_baterias, TRUE);
	}

	public function edit($id)
	{
		$banco = $this->db->get_where($this->table, array('id' => $id), 1)
							->result_array();

		return $this->Populate_model->populate($this->table, $banco, TRUE, TRUE);
	}

	public function update($id, $datos)
	{
		return $this->db->set($datos)
						->where('id', $id)
						->update($this->table);
	}

	public function insert($datos)
	{
		return array(
				"resp" => $this->db->insert($this->table, $datos),
				"last_id" => $this->db->insert_id()
			);
	}

	public function delete($id)
	{
		return $this->db->where('id', $id)
						->delete($this->table);
	}
	
	public function get_tipo_bancos_baterias($id)
	{
		return $this->db->get_where("tb_tipo_bancos_baterias", array("id" => $id))
						->result_array();
	}

	public function update_where($parametros, $datos)
	{
		$this->db->set($datos);

		foreach($parametros as $campo => $valor)
		{
			$this->db->where($campo, $valor);
		}

		$this->db->update($this->table);
		
		return $this->db->last_query();
	}

	public function get_sitio_banco($id)
	{
		return $this->db->select("ss.id, ss.nombre_completo")
						->join("tb_controlador_rectif cr", "cr.id = crbb.controlador_rectif", "left")
						->join("tb_status_site ss", "ss.id = cr.sitio", "left")
						->get_where($this->table." crbb", array("crbb.id" => $id), 1)
						->row_array();
	}
}