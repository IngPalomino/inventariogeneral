<?php
class Wimax_detailed_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_wimax_detailed()
	{
		$this->db->order_by("id","ASC");
		$wimax = $this->db->get("tb_wimax_detailed");
		return $wimax->result_array();
	}

	public function get_wimax_detailed_sitio($id)
	{
		$this->db->order_by("bts_name,entity_type","ASC");
		$wimax = $this->db->get_where("tb_wimax_detailed",array('sitio' => $id));
		$wimax_array = $wimax->result_array();
		/*$wimax_array = $this->populate->populate("tb_wimax_detailed",$wimax_array,true);*/
		return $wimax_array;
	}

	public function edit($id)
	{
		$wimax = $this->db->get_where("tb_wimax_detailed",array('id' => $id),1);
		$wimax_array = $tarjeta->result_array();

		/*$wimax_array = $this->populate->populate("tb_wimax_detailed",$wimax_array,true);*/

		return $wimax_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_wimax_detailed');
		return $resp;
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_wimax_detailed',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_wimax_detailed');
	}
}