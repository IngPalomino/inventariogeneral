<?php
class Tipo_controlador_ge_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$listado = array(
				"marcas" => array(),
				"modelos" => array()
			);
		
		$this->db->select("marca");
		$this->db->group_by("marca");
		$marcas = $this->db->get("tb_tipo_controlador_ge");
		
		$listado["marcas"] = $marcas->result_array();
		
		$this->db->reset_query();
		
		$this->db->order_by("modelo","ASC");
		$modelos = $this->db->get("tb_tipo_controlador_ge");
		
		$listado["modelos"] = $modelos->result_array();
		
		return $listado;
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_tipo_controlador_ge",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$controlador = $this->db->get_where("tb_tipo_controlador_ge",array('id' => $id),1);
		return $controlador->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_controlador_ge');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_tipo_controlador_ge',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_controlador_ge');
	}
}