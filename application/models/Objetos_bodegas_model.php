<?php
class Objetos_bodegas_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types($filtros = array())
	{
		if (isset($filtros["objeto"]))
		{
			$this->db->where("objeto !=", $filtros["objeto"]);
		}

		return $this->db->order_by("objeto", "ASC")
						->get("tb_objetos_bodegas")
						->result_array();
	}

	public function get_type($objeto)
	{
		return $this->db->get_where("tb_objetos_bodegas", array("objeto" => $objeto), 1)
						->row_array();
	}

	public function get_objeto($tipo, $id)
	{
		$this->load->model('Bodega_'.$tipo.'_model', 'Objeto_model');
		return $this->Objeto_model->select($id);
	}
}