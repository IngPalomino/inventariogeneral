<?php
class Wimax_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_wimax()
	{
		$this->db->order_by("id","ASC");
		$wimax = $this->db->get("tb_wimax");
		return $wimax->result_array();
	}

	public function get_wimax_sitio($id)
	{
		$this->db->order_by("wimax_code,entity_type","ASC");
		$wimax = $this->db->get_where("tb_wimax",array('sitio' => $id));
		$wimax_array = $wimax->result_array();
		return $wimax_array;
	}

	public function edit($id)
	{
		$wimax = $this->db->get_where("tb_wimax",array('id' => $id),1);
		$wimax_array = $tarjeta->result_array();
		return $wimax_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_wimax');
		return $resp;
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_wimax',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_wimax');
	}
}