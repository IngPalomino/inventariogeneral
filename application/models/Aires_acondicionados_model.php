<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aires_acondicionados_model extends CI_Model {

	private $table;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = 'tb_aires_acondicionados';
	}

	public function get_aires_acondicionados()
	{
		return $this->db->order_by('id', 'ASC')
						->get($this->table)
						->result_array();
	}

	public function get_aires_acondicionados_sitio($id)
	{
		$aires = $this->db->order_by('id', 'ASC')
							->get_where($this->table, array('sitio' => $id, 'eliminado' => 0,'id_sala_tecnica'=> 0))
							->result_array();

		return $this->Populate_model->populate($this->table, $aires, TRUE);
	}
	
	public function get_aires_acondicionados_sitio_mso($id)
	{
		$aires = $this->db->order_by('id', 'ASC')
							->get_where($this->table, array('sitio' => $id, 'eliminado' => 0,'id_sala_tecnica >'=> 0))
							->result_array();

		return $this->Populate_model->populate($this->table, $aires, TRUE);
	}


	public function edit($id)
	{
		$aire = $this->db->get_where($this->table, array('id' => $id), 1)
							->result_array();

		return $this->Populate_model->populate($this->table, $aire, TRUE, TRUE);
	}

	public function update($id, $datos)
	{
		return $this->db->update($this->table, $datos, array('id' => $id));
	}

	public function insert($datos)
	{
		return array(
				'resp' => $this->db->insert($this->table, $datos),
				'last_id' => $this->db->insert_id()
			);
	}

	public function delete($id)
	{
		return $this->db->delete($this->table, array('id' => $id));
	}

	public function get_sitio_aire($id)
	{
		return $this->db->select('ss.id, ss.nombre_completo')
						->join('tb_status_site ss', 'ss.id = aa.sitio', 'left')
						->get_where($this->table.' aa', array('aa.id' => $id), 1)
						->row_array();
	}
}