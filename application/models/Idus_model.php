<?php
class Idus_model extends CI_Model {

	public function __construct()
	{
		require_once("Populate.php");
		$this->load->database();
		$this->populate = new Populate();
	}

	public function get_idus()
	{
		$this->db->order_by("id","ASC");
		$idus = $this->db->get("tb_idus");
		return $idus->result_array();
	}

	public function get_idus_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$idus = $this->db->get_where("tb_idus",array('sitio' => $id, 'eliminado' => 0));

		$idus_array = $idus->result_array();

		$idus_array = $this->populate->populate("tb_idus",$idus_array,true);

		return $idus_array;
	}

	public function get_enlaces_idu($id)
	{
		$this->db->where("idu_1",$id);
		$this->db->or_where("idu_2",$id);
		$enlaces = $this->db->get("tb_enlaces");
		return $enlaces->result_array();
	}

	public function edit($id)
	{
		$idu = $this->db->get_where("tb_idus",array('id' => $id),1);
		$idu_array = $idu->result_array();

		$idu_array = $this->populate->populate("tb_idus",$idu_array,true,true);

		return $idu_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_idus');
		return $resp;
	}

	public function insert($datos)
	{
		$resultado = $this->db->insert('tb_idus',$datos);

		$resp = array(
				"resp" => $resultado,
				"last_id" => $this->db->insert_id()
			);

		return $resp;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_idus');
	}
	
	public function check_duplicate_neid($neid)
	{
		$neids = $this->db->get_where("tb_idus",array('ne_id' => $neid, 'eliminado' => 0));
		return $neids->num_rows();
	}

	public function get_sitio_idu($id)
	{
		$this->db->select("ss.id, ss.nombre_completo");
		$this->db->join("tb_status_site ss","ss.id = aa.sitio","left");
		$sitio = $this->db->get_where("tb_idus aa", array("aa.id" => $id));
		return $sitio->row_array();
	}



    public function get_sitio_con_neid($ne_id)
	{
		$this->db->select("aa.id,aa.ne_id,ss.nombre_completo");
		$this->db->join("tb_status_site ss","ss.id = aa.sitio","left");
		$neids = $this->db->get_where("tb_idus aa",array('aa.ne_id' => $ne_id, 'aa.eliminado' => 0));
		return $neids->result_array();
	}

	
}