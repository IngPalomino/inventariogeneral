<?php
class Reportes_model extends CI_Model {

	public function __construct()
	{
		require_once("Populate.php");
		$this->load->database();
		$this->populate = new Populate();
	}

	public function get_pop($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->select("ss.*");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["proyecto"]) )
		{
			$this->db->join("tb_proyecto p","p.id = ss.proyecto","left");
			$this->db->where("p.id",$data["proyecto"]);
		}
		if(isset($data["sitio"]))
		{
			$this->db->where("ss.id",$data["sitio"]);
		}

		$sitios = $this->db->get("tb_status_site ss");
		$sitios_array = $sitios->result_array();

		$sitios_array = $this->populate->populate("tb_status_site",$sitios_array,true);

		foreach ($sitios_array as &$sitio)
		{
			$this->db->reset_query();
			$tecnos = $this->db->get_where("tb_sitio_tecnologia", array("sitio" => $sitio["id"]));
			$tecnos = $tecnos->result_array();

			$wimax25 = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);
			$wimax35 = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);
			$iDEN = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);
			$Agregador = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);
			$Repetidor = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);
			$GUL = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);
			$LTE_700 = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);

			foreach ($tecnos as $t)
			{
				if( $t["tecnologia"] == 1 )
				{
					$wimax25["codigo"] = $t["codigo"];
					$wimax25["fecha_servicio"] = $t["fecha_servicio"];
					$wimax25["servicios"] = $t["servicios"];
				}
				
				if( $t["tecnologia"] == 2 )
				{
					$wimax35["codigo"] = $t["codigo"];
					$wimax35["fecha_servicio"] = $t["fecha_servicio"];
					$wimax35["servicios"] = $t["servicios"];
				}
				
				if( $t["tecnologia"] == 3 )
				{
					$iDEN["codigo"] = $t["codigo"];
					$iDEN["fecha_servicio"] = $t["fecha_servicio"];
					$iDEN["servicios"] = $t["servicios"];
				}
				
				if( $t["tecnologia"] == 4 )
				{
					$Agregador["codigo"] = $t["codigo"];
					$Agregador["fecha_servicio"] = $t["fecha_servicio"];
					$Agregador["servicios"] = $t["servicios"];
				}
				
				if( $t["tecnologia"] == 5 )
				{
					$Repetidor["codigo"] = $t["codigo"];
					$Repetidor["fecha_servicio"] = $t["fecha_servicio"];
					$Repetidor["servicios"] = $t["servicios"];
				}
				
				if( $t["tecnologia"] == 6 )
				{
					$GUL["codigo"] = $t["codigo"];
					$GUL["fecha_servicio"] = $t["fecha_servicio"];
					$GUL["servicios"] = $t["servicios"];
				}
				
				if( $t["tecnologia"] == 7 )
				{
					$LTE_700["codigo"] = $t["codigo"];
					$LTE_700["fecha_servicio"] = $t["fecha_servicio"];
					$LTE_700["servicios"] = $t["servicios"];
				}
				
			}

			$sitio["wimax25"] = $wimax25;
			$sitio["wimax35"] = $wimax35;
			$sitio["iDEN"] = $iDEN;
			$sitio["Agregador"] = $Agregador;
			$sitio["Repetidor"] = $Repetidor;
			$sitio["GUL"] = $GUL;
			$sitio["LTE_700"] = $LTE_700;
		}

		foreach ($sitios_array as &$sitio)
		{
			$this->db->reset_query();
			$crm = $this->db->select("c.crm, CONCAT(uo.nombre, ' ', uo.apellido) supervisor")
							->join("tb_crm c","c.id = sc.crm","left")
							->join("db_users.tb_users_oym uo","uo.id = sc.supervisor","left")
							->get_where("tb_sitio_crm sc", array("sc.sitio" => $sitio["id"]));
			$crm_array = $crm->row_array();

			$sitio["crm"] = $crm_array;

			$this->db->reset_query();
			$oym_zonales = $this->db->select("GROUP_CONCAT(CONCAT(uo.nombre, ' ', uo.apellido)) oym_zonales")
									->join("db_users.tb_users_oym uo","uo.id = soz.oym_zonal","left")
									->get_where("tb_sitio_oym_zonal soz", array("sitio" => $sitio["id"]));
			$oym_zonales_array = $oym_zonales->row_array();
			$sitio["oym_zonales"] = $oym_zonales_array;
		}

		foreach($sitios_array as &$sitio)
		{
			if(($key = array_search(1, array_column($sitio["contratos"], "eliminado"))) !== false)
			{
				array_splice($sitio["contratos"], $key, 1);
			}
		}

		return $sitios_array;
	}

	public function get_antenas_gul($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("sector","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, ag.*");

		$this->db->join("tb_status_site ss","ss.id = ag.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["tipo_antena_gul"]) )
		{
			$this->db->where("ag.tipo_antena_gul",$data["tipo_antena_gul"]);
		}

		$this->db->where(array("ag.eliminado" => 0, "ss.id IS NOT" => NULL));
		$antenas_gul = $this->db->get("tb_antenas_gul ag");
		$antenas_gul_array = $antenas_gul->result_array();

		$antenas_gul_array = $this->populate->populate("tb_antenas_gul",$antenas_gul_array,true);

		return $antenas_gul_array;
	}

	public function get_antenas_iden($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("sector","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, ai.*");

		$this->db->join("tb_status_site ss","ss.id = ai.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["tipo_antena_gul"]) )
		{
			$this->db->where("ai.tipo_antena_gul",$data["tipo_antena_gul"]);
		}

		$this->db->where(array("ai.eliminado" => 0, "ss.id IS NOT" => NULL));
		$antenas_iden = $this->db->get("tb_antenas_iden ai");
		$antenas_iden_array = $antenas_iden->result_array();

		$antenas_iden_array = $this->populate->populate("tb_antenas_iden",$antenas_iden_array,true);

		return $antenas_iden_array;
	}
	
	public function get_antenas_in_building($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("tipo_antena_in_building","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, ag.*");

		$this->db->join("tb_status_site ss","ss.id = ag.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["tipo_antena_in_building"]) )
		{
			$this->db->join("tb_tipo_antena_in_building cs","cs.id = ag.tipo_antena_in_building","left");
			$this->db->where("cs.tipo",$data["tipo_antena_in_building"]);
		}

		$this->db->where(array("ag.eliminado" => 0));
		$antenas_in_building = $this->db->get("tb_antenas_in_building ag");
		$antenas_in_building_array = $antenas_in_building->result_array();

		$antenas_in_building_array = $this->populate->populate("tb_antenas_in_building",$antenas_in_building_array,true);

		return $antenas_in_building_array;
	}
	
	public function get_idus($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("tipo_idu","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, ag.*");

		$this->db->join("tb_status_site ss","ss.id = ag.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["tipo_idu"]) )
		{
			$this->db->join("tb_tipo_idu cs","cs.id = ag.tipo_idu","left");
			$this->db->where("cs.id",$data["tipo_idu"]);
		}

		$this->db->where(array("ag.eliminado" => 0, "ss.codigo IS NOT" => NULL));
		$idus = $this->db->get("tb_idus ag");
		$idus_array = $idus->result_array();

		$idus_array = $this->populate->populate("tb_idus",$idus_array,true);

		return $idus_array;
	}
	
	public function get_enlaces($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("idu_1","ASC");
		$this->db->select("ss.codigo, e.*");

		$this->db->join("tb_idus i","i.id = e.idu_1","left");
		$this->db->join("tb_status_site ss","ss.id = i.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["tipo_idu"]) )
		{
			$this->db->join("tb_tipo_idu cs","cs.id = e.tipo_idu","left");
			$this->db->where("cs.id",$data["tipo_idu"]);
		}

		$this->db->where(array("i.eliminado" => 0, "e.eliminado" => 0, "ss.codigo IS NOT" => NULL));
		$enlaces = $this->db->get("tb_enlaces e");
		$enlaces_array = $enlaces->result_array();

		$enlaces_array = $this->populate->populate("tb_enlaces",$enlaces_array,true);
		
		foreach($enlaces_array as &$enlace)
		{
			$this->db->select("id,sitio,ne_id");
			$idu_1 = $this->db->get_where("tb_idus",array('id' => $enlace["idu_1"], 'eliminado' => 0));
			$enlace["idu_1"] = $idu_1->row_array();
			
			$this->db->reset_query();
			$this->db->select("id,codigo,nombre_completo");
			$nombreSitio1 = $this->db->get_where("tb_status_site",array('id' => $enlace["idu_1"]["sitio"]));
			$enlace["idu_1"]["sitio"] = $nombreSitio1->row_array();
			
			$this->db->reset_query();
			$this->db->select("id,sitio,ne_id");
			$idu_2 = $this->db->get_where("tb_idus",array('id' => $enlace["idu_2"], 'eliminado' => 0));
			$enlace["idu_2"] = $idu_2->row_array();
			
			$this->db->reset_query();
			$this->db->select("id,codigo,nombre_completo");
			$nombreSitio2 = $this->db->get_where("tb_status_site",array('id' => $enlace["idu_2"]["sitio"]));
			$enlace["idu_2"]["sitio"] = $nombreSitio2->row_array();
			
			$enlace["odus_ne"] = array();
			$enlace["odus_fe"] = array();
			$enlace["antenas_mw_ne"] = array();
			$enlace["antenas_mw_fe"] = array();
			
			if(count($enlace["odus_enlaces"]) > 0)
			{
				foreach($enlace["odus_enlaces"] as &$odu)
				{
					if(($odu["idu"] == $enlace["idu_1"]["id"]) && ($odu["eliminado"] == 0))
					{
						array_push($enlace["odus_ne"],$odu);
					}
					elseif(($odu["idu"] == $enlace["idu_2"]["id"]) && ($odu["eliminado"] == 0))
					{
						array_push($enlace["odus_fe"],$odu);
					}
				}
			}
			unset($enlace["odus_enlaces"]);
			
			if(count($enlace["antenas_mw_enlaces"]) > 0)
			{
				foreach($enlace["antenas_mw_enlaces"] as &$antena)
				{
					if(($antena["idu"] == $enlace["idu_1"]["id"]) && ($antena["eliminado"] == 0))
					{
						array_push($enlace["antenas_mw_ne"],$antena);
					}
					elseif(($antena["idu"] == $enlace["idu_2"]["id"]) && ($antena["eliminado"] == 0))
					{
						array_push($enlace["antenas_mw_fe"],$antena);
					}
				}
			}
			($enlace["antenas_mw_enlaces"]);
		}

		return $enlaces_array;
	}

	public function get_suministros($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("concesionaria_suministro","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, s.*");

		$this->db->join("tb_status_site ss","ss.id = s.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["concesionaria_suministro"]) )
		{
			$this->db->join("tb_concesionaria_suministro cs","cs.id = s.concesionaria_suministro","left");
			$this->db->where("cs.id",$data["concesionaria_suministro"]);
		}

		$this->db->where(array("s.eliminado" => 0, "ss.estado" => 2, "ss.nombre_completo IS NOT" => NULL));
		$suministros = $this->db->get("tb_suministros s");
		$suministros_array = $suministros->result_array();

		$suministros_array = $this->populate->populate("tb_suministros",$suministros_array,true);

		return $suministros_array;
	}

	public function get_tanques_combustible($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("tipo_tanque_combustible","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, tc.*");

		$this->db->join("tb_status_site ss","ss.id = tc.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["tipo_tanque_combustible"]) )
		{
			$this->db->where("tc.tipo_tanque_combustible",$data["tipo_tanque_combustible"]);
		}

		$this->db->where(array("tc.eliminado" => 0, "ss.codigo IS NOT" => NULL));
		$tanques_combustible = $this->db->get("tb_tanques_combustible tc");
		$tanques_combustible_array = $tanques_combustible->result_array();

		$tanques_combustible_array = $this->populate->populate("tb_tanques_combustible",$tanques_combustible_array,true);

		return $tanques_combustible_array;
	}
	
	public function get_lineas_electricas($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, s.*");

		$this->db->join("tb_status_site ss","ss.id = s.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["conexion_linea_electrica"]) )
		{
			$this->db->join("tb_conexion_linea_electrica cs","cs.id = s.conexion_linea_electrica","left");
			$this->db->where("cs.id",$data["conexion_linea_electrica"]);
		}

		$this->db->where(array("s.eliminado" => 0));
		$lineas_electricas = $this->db->get("tb_lineas_electricas s");
		$lineas_electricas_array = $lineas_electricas->result_array();

		$lineas_electricas_array = $this->populate->populate("tb_lineas_electricas",$lineas_electricas_array,true);

		return $lineas_electricas_array;
	}
	
	public function get_rectificadores($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("tipo_controlador_rectif","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, s.*");
		$this->db->join("tb_status_site ss","ss.id = s.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["tipo_controlador_rectif"]) )
		{
			$this->db->join("tb_tipo_controlador_rectif cs","cs.id = s.tipo_controlador_rectif","left");
			$this->db->where("cs.marca",$data["tipo_controlador_rectif"]);
		}
		
		$this->db->where(array("s.eliminado" => 0, "ss.estado" => 2));
		$rectificadores = $this->db->get("tb_controlador_rectif s");
		$rectificadores_array = $rectificadores->result_array();

		$rectificadores_array = $this->populate->populate("tb_controlador_rectif",$rectificadores_array,true);

		return $rectificadores_array;
	}
	
	public function get_aires_acondicionados($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("tipo_aires_acondicionados","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, s.*");

		$this->db->join("tb_status_site ss","ss.id = s.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["tipo_aires_acondicionados"]) )
		{
			$this->db->join("tb_tipo_aires_acondicionados cs","cs.id = s.tipo_aires_acondicionados","left");
			$this->db->where("cs.tipo_equipo",$data["tipo_aires_acondicionados"]);
		}

		$this->db->where(array("s.eliminado" => 0));
		$aires_acondicionados = $this->db->get("tb_aires_acondicionados s");
		$aires_acondicionados_array = $aires_acondicionados->result_array();

		$aires_acondicionados_array = $this->populate->populate("tb_aires_acondicionados",$aires_acondicionados_array,true);

		return $aires_acondicionados_array;
	}
	
	public function get_bancos_baterias($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("tipo_bancos_baterias","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, ucr.energizacion_controlador_rectif, s.*");

		$this->db->join("tb_controlador_rectif cr","cr.id = s.controlador_rectif","left");
		$this->db->join("tb_status_site ss","ss.id = cr.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");
		$this->db->join("tb_energizacion_controlador_rectif ucr","ucr.id = cr.energizacion_controlador_rectif","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["tipo_bancos_baterias"]) )
		{
			$this->db->join("tb_tipo_bancos_baterias cs","cs.id = s.tipo_bancos_baterias","left");
			$this->db->where("cs.marca",$data["tipo_bancos_baterias"]);
		}

		$this->db->where(array("s.eliminado" => 0, "cr.eliminado" => 0));
		$bancos_baterias = $this->db->get("tb_controlador_rectif_bancos_baterias s");
		$bancos_baterias_array = $bancos_baterias->result_array();

		$bancos_baterias_array = $this->populate->populate("tb_controlador_rectif_bancos_baterias",$bancos_baterias_array,true);

		return $bancos_baterias_array;
	}
	
	public function get_grupos_electrogenos($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("tipo_grupo_electrogeno","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, s.*");

		$this->db->join("tb_status_site ss","ss.id = s.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		if( isset($data["tipo_grupo_electrogeno"]) )
		{
			$this->db->join("tb_tipo_grupo_electrogeno cs","cs.id = s.tipo_grupo_electrogeno","left");
			$this->db->where("cs.marca",$data["tipo_grupo_electrogeno"]);
		}

		$this->db->where(array("s.eliminado" => 0, "ss.estado" => 2));
		$grupos_electrogenos = $this->db->get("tb_grupos_electrogenos s");
		$grupos_electrogenos_array = $grupos_electrogenos->result_array();

		$grupos_electrogenos_array = $this->populate->populate("tb_grupos_electrogenos",$grupos_electrogenos_array,true);

		return $grupos_electrogenos_array;
	}
	
	public function get_e_antenas($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("antenna_device_id","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, ea.*");

		$this->db->join("tb_status_site ss","ss.id = ea.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		/*if( isset($data["tipo_grupo_electrogeno"]) )
		{
			$this->db->join("tb_tipo_grupo_electrogeno cs","cs.id = s.tipo_grupo_electrogeno","left");
			$this->db->where("cs.marca",$data["tipo_grupo_electrogeno"]);
		}*/

		$e_antenas = $this->db->get("tb_e_antenas ea");
		$e_antenas_array = $e_antenas->result_array();

		return $e_antenas_array;
	}
	
	public function get_tarjetas_nodo_b($data = null,$part)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->order_by("id","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, ea.*");

		$this->db->join("tb_status_site ss","ss.id = ea.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		else
		{
			$limit = 20000;
			$offset = $part*20000;
			$this->db->limit($limit,$offset);
		}

		$this->db->where(array("ss.codigo IS NOT" => NULL));
		$tarjetas_nodo_b = $this->db->get("tb_e_tarjetas ea");
		$tarjetas_nodo_b_array = $tarjetas_nodo_b->result_array();

		return $tarjetas_nodo_b_array;
	}
	
	public function get_e_gabinetes($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, eg.*");

		$this->db->join("tb_status_site ss","ss.id = eg.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		/*if( isset($data["tipo_grupo_electrogeno"]) )
		{
			$this->db->join("tb_tipo_grupo_electrogeno cs","cs.id = s.tipo_grupo_electrogeno","left");
			$this->db->where("cs.marca",$data["tipo_grupo_electrogeno"]);
		}*/

		$e_gabinetes = $this->db->get("tb_e_gabinetes eg");
		$e_gabinetes_array = $e_gabinetes->result_array();

		return $e_gabinetes_array;
	}
	
	public function get_mw_tarjetas($data = null,$part)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, t.*");

		$this->db->join("tb_idus i","i.id = t.idu","left");
		$this->db->join("tb_status_site ss","ss.id = i.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		else
		{
			$limit = 20000;
			$offset = $part*20000;
			$this->db->limit($limit,$offset);
		}
		
		$this->db->where(array("i.eliminado" => 0, "ss.id IS NOT" => NULL));
		$mw_tarjetas = $this->db->get("tb_mw_tarjetas t");
		$mw_tarjetas_array = $mw_tarjetas->result_array();
		
		foreach($mw_tarjetas_array as &$tarjeta)
		{
			$this->db->select("id,ne_id");
			$ne_id = $this->db->get_where("tb_idus",array("id" => $tarjeta["idu"]));
			$tarjeta["idu"] = $ne_id->row_array();
		}

		return $mw_tarjetas_array ;
	}
	
	public function get_mw_odus($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, o.*");

		$this->db->join("tb_status_site ss","ss.id = o.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}
		/*if( isset($data["tipo_grupo_electrogeno"]) )
		{
			$this->db->join("tb_tipo_grupo_electrogeno cs","cs.id = s.tipo_grupo_electrogeno","left");
			$this->db->where("cs.marca",$data["tipo_grupo_electrogeno"]);
		}*/

		$mw_odus = $this->db->get("tb_mw_odus o");
		$mw_odus_array = $mw_odus->result_array();

		return $mw_odus_array;
	}

	public function get_wimax($data = null)
	{
		$this->db->order_by("ss.codigo,w.wimax_code,w.entity_type");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, w.*");
		$this->db->join("tb_status_site ss","ss.id = w.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}

		if( isset($data["tipo_wimax"]) )
		{
			$this->db->like("w.bts_name",$data["tipo_wimax"]);
		}

		$wimax = $this->db->get("tb_wimax w");
		$listado["wimax"] = $wimax->result_array();

		$this->db->reset_query();
		$this->db->order_by("ss.codigo,wd.bts_name,wd.entity_type");
		$this->db->select("ss.codigo, ss.nombre_completo, wd.*");
		$this->db->join("tb_status_site ss","ss.id = wd.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}

		if( isset($data["tipo_wimax"]) )
		{
			$this->db->like("wd.bts_name",$data["tipo_wimax"]);
		}

		$wimax_detailed = $this->db->get("tb_wimax_detailed wd");
		$listado["wimax_detailed"] = $wimax_detailed->result_array();

		return $listado;
	}

	public function get_iden($data = null)
	{
		$this->db->order_by("codigo","ASC");
		$this->db->select("ss.codigo, ss.nombre_completo, ss.prioridad, zp.departamento, zp.provincia, zp.distrito, i.*");
		$this->db->join("tb_status_site ss","ss.id = i.sitio","left");
		$this->db->join("tb_zona_peru zp","zp.id = ss.zona_peru","left");

		if( isset($data["departamento"]) || isset($data["provincia"]) || isset($data["distrito"]) )
		{
			if( isset($data["departamento"]) )
			{
				$this->db->where("zp.departamento",$data["departamento"]);
			}
			if( isset($data["provincia"]) )
			{
				$this->db->where("zp.provincia",$data["provincia"]);
			}
			if( isset($data["distrito"]) )
			{
				$this->db->where("zp.distrito",$data["distrito"]);
			}
		}

		$iden = $this->db->get("tb_iden i");
		$iden_array = $iden->result_array();

		foreach($iden_array as &$info)
		{
			$this->db->reset_query();
			$this->db->select("cantidad");
			$resultado = $this->db->get_where("tb_antenas_iden",array("eliminado" => 0, "sitio" => $info["sitio"]));
			$resultado_array = $resultado->result_array();

			$cantidad_total = 0;
			
			foreach($resultado_array as $key => $value)
			{
				$cantidad_total += (Float)$value["cantidad"];
			}

			$info["antennas"] = $cantidad_total;
		}

		return $iden_array;
	}
	
	public function get_grupos_electrogenos_almacenados($data = null)
	{
		$this->db->order_by("id","ASC");
		$this->db->select("ss.nombre_completo proveniente, ge.*");
		$this->db->join("tb_status_site ss", "ss.id = ge.proveniente_sitio", "left");
		
		$grupos_electrogenos_almacenados = $this->db->get_where("tb_bodega_grupos_electrogenos ge",array('ge.eliminado' => 0));
		$grupos_electrogenos_almacenados_array = $grupos_electrogenos_almacenados->result_array();
		$grupos_electrogenos_almacenados_array = $this->populate->populate("tb_bodega_grupos_electrogenos", $grupos_electrogenos_almacenados_array, true);
		
		foreach($grupos_electrogenos_almacenados_array as &$grupo)
		{
			switch($grupo["tipo_almacen"])
			{
				case 1:
					$almacen = $this->db->get_where("tb_almacenes",array("id" => $grupo["almacen"]));
					$grupo["almacen"] = $almacen->row_array();
					break;
				case 2:
					$this->db->select("id, nombre_completo");
					$sitio = $this->db->get_where("tb_status_site",array("id" => $grupo["almacen"]));
					$almacen = $sitio->row_array();
					$grupo["almacen"] = array(
							"id" => $almacen["id"],
							"almacen" => $almacen["nombre_completo"]
						);
					break;
			}
		}
		
		return $grupos_electrogenos_almacenados_array;
	}

	public function get_sitio_individual($id)
	{
		$this->db->where(array("id" => $id, "estado" => 2));
		$sitios = $this->db->get("tb_status_site");
		$sitios_array = $sitios->result_array();

		$sitios_array = $this->populate->populate("tb_status_site",$sitios_array,true);

		foreach ($sitios_array as &$sitio)
		{
			$this->db->reset_query();
			$tecnos = $this->db->get_where("tb_sitio_tecnologia", array("sitio" => $sitio["id"]));
			$tecnos = $tecnos->result_array();

			$wimax25 = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);
			$wimax35 = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);
			$iDEN = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);
			$Agregador = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);
			$Repetidor = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);
			$GUL = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);
			$LTE_700 = array(
						"codigo" => "",
						"fecha_servicio" => NULL,
						"servicios" => 0
					);

			foreach ($tecnos as $t)
			{
				if( $t["tecnologia"] == 1 )
				{
					$wimax25["codigo"] = $t["codigo"];
					$wimax25["fecha_servicio"] = $t["fecha_servicio"];
					$wimax25["servicios"] = $t["servicios"];
				}
				
				if( $t["tecnologia"] == 2 )
				{
					$wimax35["codigo"] = $t["codigo"];
					$wimax35["fecha_servicio"] = $t["fecha_servicio"];
					$wimax35["servicios"] = $t["servicios"];
				}
				
				if( $t["tecnologia"] == 3 )
				{
					$iDEN["codigo"] = $t["codigo"];
					$iDEN["fecha_servicio"] = $t["fecha_servicio"];
					$iDEN["servicios"] = $t["servicios"];
				}
				
				if( $t["tecnologia"] == 4 )
				{
					$Agregador["codigo"] = $t["codigo"];
					$Agregador["fecha_servicio"] = $t["fecha_servicio"];
					$Agregador["servicios"] = $t["servicios"];
				}
				
				if( $t["tecnologia"] == 5 )
				{
					$Repetidor["codigo"] = $t["codigo"];
					$Repetidor["fecha_servicio"] = $t["fecha_servicio"];
					$Repetidor["servicios"] = $t["servicios"];
				}
				
				if( $t["tecnologia"] == 6 )
				{
					$GUL["codigo"] = $t["codigo"];
					$GUL["fecha_servicio"] = $t["fecha_servicio"];
					$GUL["servicios"] = $t["servicios"];
				}
				
				if( $t["tecnologia"] == 7 )
				{
					$LTE_700["codigo"] = $t["codigo"];
					$LTE_700["fecha_servicio"] = $t["fecha_servicio"];
					$LTE_700["servicios"] = $t["servicios"];
				}
				
			}

			$sitio["wimax25"] = $wimax25;
			$sitio["wimax35"] = $wimax35;
			$sitio["iDEN"] = $iDEN;
			$sitio["Agregador"] = $Agregador;
			$sitio["Repetidor"] = $Repetidor;
			$sitio["GUL"] = $GUL;
			$sitio["LTE_700"] = $LTE_700;
		}

		foreach ($sitios_array as &$sitio)
		{
			$this->db->reset_query();
			$crm = $this->db->select("c.crm, CONCAT(uo.nombre, ' ', uo.apellido) supervisor")
							->join("tb_crm c","c.id = sc.crm","left")
							->join("db_users.tb_users_oym uo","uo.id = sc.supervisor","left")
							->get_where("tb_sitio_crm sc", array("sc.sitio" => $sitio["id"]));
			$crm_array = $crm->row_array();

			$sitio["crm"] = $crm_array;

			$this->db->reset_query();
			$oym_zonales = $this->db->select("GROUP_CONCAT(CONCAT(uo.nombre, ' ', uo.apellido)) oym_zonales")
									->join("db_users.tb_users_oym uo","uo.id = soz.oym_zonal","left")
									->get_where("tb_sitio_oym_zonal soz", array("sitio" => $sitio["id"]));
			$oym_zonales_array = $oym_zonales->row_array();
			$sitio["oym_zonales"] = $oym_zonales_array;
		}

		return $sitios_array;
	}
}