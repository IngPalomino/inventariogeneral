<?php
class Bodega_grupos_electrogenos_model extends CI_Model {

	public function __construct()
	{
		require_once("Populate.php");
		$this->load->database();
		$this->populate = new Populate();
	}
	
	public function get_grupos_electrogenos($filtros = array())
	{
		if(isset($filtros["almacen"]))
		{
			$this->db->where(array("tipo_almacen" => 1, "almacen" => $filtros["almacen"]));
		}
		elseif(isset($filtros["sitio"]))
		{
			$this->db->where(array("tipo_almacen" => 2, "almacen" => $filtros["sitio"]));
		}
		
		$this->db->order_by("id", "ASC");
		$this->db->where(array('eliminado' => 0));
		$grupos = $this->db->get("tb_bodega_grupos_electrogenos");
		$grupos_array = $grupos->result_array();
		$grupos_array = $this->populate->populate("tb_bodega_grupos_electrogenos",$grupos_array,true);
		
		foreach($grupos_array as &$grupo)
		{
			if($grupo["id_sala_tecnica"]>0){
              //si tiene id de mso 
			 	$this->db->select("id, nombre_completo");
			$nombre_completo = $this->db->get_where("tb_mso_sala_tecnica",array('sitio' => $grupo["proveniente_sitio"]));			
			$grupo["proveniente_sitio"] = $nombre_completo->row_array();

			 }else{
			 	//no viene de mso 
			$this->db->select("id, nombre_completo");
			$nombre_completo = $this->db->get_where("tb_status_site",array('id' => $grupo["proveniente_sitio"]));			
			$grupo["proveniente_sitio"] = $nombre_completo->row_array();

			 }
			

			switch($grupo["tipo_almacen"])
			{
				case 1:
					$almacen = $this->db->get_where("tb_almacenes",array("id" => $grupo["almacen"]));
					$grupo["almacen"] = $almacen->row_array();
					break;
				case 2:
					$this->db->select("id, nombre_completo");
					$sitio = $this->db->get_where("tb_status_site",array("id" => $grupo["almacen"]));
					$almacen = $sitio->row_array();
					$grupo["almacen"] = array(
							"id" => $almacen["id"],
							"almacen" => $almacen["nombre_completo"]
						);
					break;
			}
		}
		
		return $grupos_array;
	}
	
	public function get_grupos_electrogenos_almacen($id)
	{
		$this->db->order_by("id", "ASC");
		$grupos = $this->db->get_where("tb_bodega_grupos_electrogenos",array('eliminado' => 0, 'tipo_almacen' => 1, 'almacen' => $id));
		$grupos_array = $grupos->result_array();
		$grupos_array = $this->populate->populate("tb_bodega_grupos_electrogenos",$grupos_array,true);

		foreach($grupos_array as &$grupo)
		{
			$this->db->select("id, nombre_completo");
			$nombre_completo = $this->db->get_where("tb_status_site",array('id' => $grupo["proveniente_sitio"]));
			$grupo["proveniente_sitio"] = $nombre_completo->row_array();
		}
		
		$grupos_electrogenos["grupos"] = $grupos_array;

		$this->db->reset_query();
		$this->db->order_by("id", "ASC");
		$this->db->where(array('eliminado' => 0, 'elemento !=' => 0));
		$elementos = $this->db->get("tb_bodega_grupos_electrogenos");
		$elementos_array = $elementos->result_array();
		$elementos_array = $this->populate->populate("tb_bodega_grupos_electrogenos",$elementos_array,true);

		$grupos_electrogenos["elementos"] = $elementos_array;
		
		return $grupos_array;
	}
	
	public function get_grupos_electrogenos_sitio($id)
	{
		$this->db->order_by("id", "ASC");
		$grupos = $this->db->get_where("tb_bodega_grupos_electrogenos",array('eliminado' => 0, 'tipo_almacen' => 2, 'almacen' => $id));
		$grupos_array = $grupos->result_array();
		$grupos_array = $this->populate->populate("tb_bodega_grupos_electrogenos",$grupos_array,true);

		foreach($grupos_array as &$grupo)
		{
			$this->db->select("id, nombre_completo");
			$nombre_completo = $this->db->get_where("tb_status_site",array('id' => $grupo["proveniente_sitio"]));
			$grupo["proveniente_sitio"] = $nombre_completo->row_array();
		}
		
		$grupos_electrogenos["grupos"] = $grupos_array;

		$this->db->reset_query();
		$this->db->order_by("id", "ASC");
		$this->db->where(array('eliminado' => 0, 'elemento !=' => 0));
		$elementos = $this->db->get("tb_bodega_grupos_electrogenos");
		$elementos_array = $elementos->result_array();
		$elementos_array = $this->populate->populate("tb_bodega_grupos_electrogenos",$elementos_array,true);

		$grupos_electrogenos["elementos"] = $elementos_array;
		
		return $grupos_electrogenos;
	}

	public function edit($id)
	{
		$grupo = $this->db->get_where("tb_bodega_grupos_electrogenos",array('id' => $id),1);
		$grupo_array = $grupo->result_array();
		$grupo_array = $this->populate->populate("tb_bodega_grupos_electrogenos",$grupo_array,true,true);
		return $grupo_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		return $this->db->update('tb_bodega_grupos_electrogenos');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_bodega_grupos_electrogenos',$datos);
	}
}