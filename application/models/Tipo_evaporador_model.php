<?php
class Tipo_evaporador_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$this->db->order_by("modelo","ASC");
		$types = $this->db->get("tb_tipo_evaporador");
		return $types->result_array();
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_tipo_evaporador",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$antena = $this->db->get_where("tb_tipo_evaporador",array('id' => $id),1);
		return $antena->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_evaporador');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_tipo_evaporador',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_evaporador');
	}
}