<?php
class Database_maintenance_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
	}

	public function get_tokens()
	{
		$tokens = $this->db->get("tb_tokens");
		return $tokens->result_array();
	}

	public function delete_tokens($id)
	{
		return $this->db->delete("tb_tokens",array("id" => $id));
	}

	public function delete_tokens_where($parametros)
	{
		foreach($parametros as $campo => $valor)
		{
			$this->db->where($campo,$valor);
		}

		return $this->db->delete('tb_tokens');
	}
}