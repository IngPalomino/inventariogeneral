<?php
class Grupos_modulos_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function select()
	{
		
	}

	public function insert($datos)
	{
		return $this->db->insert("tb_grupos_modulos",$datos);
	}

	public function update($id,$put)
	{
		
	}

	public function delete($id)
	{
		
	}

	public function edit($id)
	{
		
	}

	public function select_modulos($grupo)
	{
		$modulos = $this->db->select("modulo")
							->get_where("tb_grupos_modulos", array("grupos" => $modulo));

		$modulos_array = $modulos->result_array();

		return $modulos_array;
	}

	public function borrar_grupos_modulos($grupo)
	{
		$respuesta = $this->db->where("grupos",$grupo)
								->delete("tb_grupos_modulos");

		return $respuesta;
	}

	public function get_grupos_modulos($modulo)
	{
		$grupos = $this->db->select("grupos")
							->get_where("tb_grupos_modulos", array("modulo" => $modulo));

		return $grupos->result_array();
	}
}