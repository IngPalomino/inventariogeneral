<?php
class Sitios_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_sites()
	{
		$this->db->order_by("codigo","ASC");
		$sitios = $this->db->get("tb_sitios");
		$sitios_array = $sitios->result_array();

		/*foreach($sitios_array as &$sitio)
		{
			$relacion = $this->db->get_where("tb_zona_peru",array('id' => $sitio['zona_peru']),1);
			$sitio['zona_peru'] = $relacion->row_array();
		}

		return $sitios_array;*/

		$relaciones = $this->db->get_Where("tb_relaciones",array("tabla_primaria" => "tb_sitios"));
		$relaciones = $relaciones->result_array();

		foreach($sitios_array as &$sitio)
		{
			foreach($relaciones as $relacion)
			{
				if($relacion["relacion"] == "1:1")
				{
					$datos = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $sitio[$relacion["columna_primaria"]]),1);
					$sitio[$relacion["columna_primaria"]] = $datos->row_array();
				}
			}
		}

		return $sitios_array;
	}

	public function look_for_sites($cadenas)
	{
		$this->db->order_by("codigo","ASC");

		$ind = 0;
		foreach($cadenas as $cadena)
		{
			if($ind == 0)
			{
				$this->db->like("nombre_completo",$cadena);
			}
			else
			{
				$this->db->or_like("nombre_completo",$cadena);
			}
			$ind++;
		}
		$sitios = $this->db->get("tb_sitios");
		$sitios_array = $sitios->result_array();

		$relaciones = $this->db->get_Where("tb_relaciones",array("tabla_primaria" => "tb_sitios"));
		$relaciones = $relaciones->result_array();

		foreach($sitios_array as &$sitio)
		{
			foreach($relaciones as $relacion)
			{
				if($relacion["relacion"] == "1:1")
				{
					$datos = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $sitio[$relacion["columna_primaria"]]),1);
					$sitio[$relacion["columna_primaria"]] = $datos->row_array();
				}
			}
		}

		return $sitios_array;
	}

	public function edit($id)
	{
		$sitio = $this->db->get_where("tb_sitios",array('id' => $id),1);
		$sitio_array = $sitio->result_array();

		$relaciones = $this->db->get_Where("tb_relaciones",array("tabla_primaria" => "tb_sitios"));
		$relaciones = $relaciones->result_array();

		foreach($sitio_array as &$sitio)
		{
			foreach($relaciones as $relacion)
			{
				if($relacion["relacion"] == "1:1")
				{
					$datos = $this->db->get_where($relacion["tabla_secundaria"],array($relacion["columna_secundaria"] => $sitio[$relacion["columna_primaria"]]),1);
					$sitio[$relacion["columna_primaria"]] = $datos->row_array();
				}
			}
		}

		return $sitio_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_sitios');
	}

	public function insert($datos)
	{
		$this->db->insert('tb_sitios',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_sitios');
	}
}