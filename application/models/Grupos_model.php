<?php
class Grupos_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function select()
	{
		$grupos = $this->db->where(array('eliminado' => 0))->get("tb_grupos");

		$grupos_array = $grupos->result_array();

		foreach ($grupos_array as &$grupo)
		{
			$modulos = $this->db->select("modulo")
							->get_where("tb_grupos_modulos", array("grupos" => $grupo["id"]));
			$grupo["grupos_modulos"] = $modulos->result_array();
		}

		return $grupos_array;
	}

	public function insert($datos)
	{

        $resultado = $this->db->insert('tb_grupos',$datos);
		$resp = array(
			"resp" => $resultado,
			"last_id" => $this->db->insert_id()
		);		
		return $resp;
	}

	public function update($id,$put)
	{
		return $this->db->where("id",$id)->update("tb_grupos",$put);
	}

	public function delete($id)
	{
		$salida = array(
				"respuesta" => false
			);

		$respuesta = $this->db->where("id",$id)
								->update("tb_grupos",array("eliminado" => 1));

		if($respuesta)
		{
			$salida["respuesta"] = true;
		}

		return $salida;
	}

	public function edit($id)
	{
		$listado = $this->db->get_where("tb_grupos",array("id" => $id));
		return $listado->result_array();
	}

	public function listar_correo($grupos)
	{
		$this->db->select("id, correo");

		foreach ($grupos as &$grupo)
		{
			if(count($grupos) == 1)
			{
				$this->db->where("id", $grupo["grupos"]);
			}
			elseif(count($grupos) > 1)
			{
				$this->db->or_where("id", $grupo["grupos"]);
			}
		}
		
		$correos = $this->db->get('tb_grupos');
		return $correos->result_array();
	}
}