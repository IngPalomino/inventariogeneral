<?php
class Tipo_mantenimiento_linea_electrica_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_types()
	{
		$this->db->order_by("tipo_mantenimiento","ASC");
		$types = $this->db->get("tb_tipo_mantenimiento_linea_electrica");
		return $types->result_array();
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_tipo_mantenimiento_linea_electrica",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$mantenimiento = $this->db->get_where("tb_tipo_mantenimiento_linea_electrica",array('id' => $id),1);
		return $mantenimiento->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_mantenimiento_linea_electrica');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_tipo_mantenimiento_linea_electrica',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_mantenimiento_linea_electrica');
	}
}