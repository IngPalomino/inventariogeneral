<?php
class Mso_Grupos_electrogenos_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function get_grupos_electrogenos()
	{
		$this->db->order_by("id","ASC");
		$grupos = $this->db->get("tb_mso_grupos_electrogenos");
		return $grupos->result_array();
	}

	public function get_grupos_electrogenos_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$grupos = $this->db->get_where("tb_mso_grupos_electrogenos",array('sitio' => $id, 'eliminado' => 0));

		$grupos_array = $grupos->result_array();

		$grupos_array = $this->Populate_model->populate("tb_mso_grupos_electrogenos",$grupos_array,true);

		return $grupos_array;
	}

	public function edit($id)
	{
		$grupo = $this->db->get_where("tb_mso_grupos_electrogenos",array('id' => $id),1);
		$grupo_array = $grupo->result_array();

		$grupo_array = $this->Populate_model->populate("tb_mso_grupos_electrogenos",$grupo_array,true,true);

		return $grupo_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_mso_grupos_electrogenos');
		return $resp;
	}
	
	public function get_mso_grupos_electrogenos()
	{
		$this->db->order_by("codigo","ASC");
		$this->db->select("ss.nombre_completo, ss.id");
		$this->db->join("tb_status_site ss","ss.id = gg.sitio","left");
		$sitios = $this->db->get("tb_mso_grupos_electrogenos gg");
		return $sitios->result_array();
	}

	public function insert($datos)
	{
		$resultado = $this->db->insert('tb_mso_grupos_electrogenos',$datos);

		$resp = array(
			"resp" => $resultado,
			"last_id" => $this->db->insert_id()
		);
		
		return $resp;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_mso_grupos_electrogenos');
	}

	public function get_sitio_grupo($id)
	{
		$this->db->select("ss.id, ss.nombre_completo");
		$this->db->join("tb_status_site ss","ss.id = aa.sitio","left");
		$sitio = $this->db->get_where("tb_grupos_electrogenos aa", array("aa.id" => $id), 1);
		return $sitio->row_array();
	}
}