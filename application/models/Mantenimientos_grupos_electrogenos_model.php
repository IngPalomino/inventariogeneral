<?php
class Mantenimientos_grupos_electrogenos_model extends CI_Model {

	public function __construct()
	{
		require_once("Populate.php");
		$this->load->database();
		$this->populate = new Populate();
	}

	public function get_mantenimientos()
	{
		$this->db->order_by("id","ASC");
		$modulos = $this->db->get("tb_mantenimientos_grupos_electrogenos");
		return $modulos->result_array();
	}

	public function get_mantenimientos_sitio($id)
	{
		$this->db->order_by("id","DESC");
		$mantenimientos = $this->db->get_where("tb_mantenimientos_grupos_electrogenos",array('grupo_electrogeno' => $id));

		$mantenimientos_array = $mantenimientos->result_array();

		$mantenimientos_array = $this->populate->populate("tb_mantenimientos_grupos_electrogenos",$mantenimientos_array,true);

		return $mantenimientos_array;
	}

	public function edit($id)
	{
		$mantenimiento = $this->db->get_where("tb_mantenimientos_grupos_electrogenos",array('id' => $id),1);
		$mantenimiento_array = $mantenimiento->result_array();

		$mantenimiento_array = $this->populate->populate("tb_mantenimientos_grupos_electrogenos",$mantenimiento_array,true,true);

		return $mantenimiento_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_mantenimientos_grupos_electrogenos');
		return $resp;
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_mantenimientos_grupos_electrogenos',$datos);
	}

	/*public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_mantenimientos_grupos_electrogenos');
	}*/
}