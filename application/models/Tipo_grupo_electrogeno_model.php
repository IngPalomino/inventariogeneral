<?php
class Tipo_grupo_electrogeno_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_types()
	{
		$listado = array(
				"marcas" => array(),
				"modelos" => array()
			);
		
		$this->db->select("marca");
		$this->db->group_by("marca");
		$marcas = $this->db->get("tb_tipo_grupo_electrogeno");
		
		$listado["marcas"] = $marcas->result_array();
		
		$this->db->reset_query();
		
		$this->db->order_by("modelo","ASC");
		$modelos = $this->db->get("tb_tipo_grupo_electrogeno");
		
		$listado["modelos"] = $modelos->result_array();
		
		return $listado;
	}

	public function get_type($data)
	{
		$type = $this->db->get_where("tb_tipo_grupo_electrogeno",$data,1);
		return $type->result_array();
	}

	public function edit($id)
	{
		$grupo = $this->db->get_where("tb_tipo_grupo_electrogeno",array('id' => $id),1);
		return $grupo->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_grupo_electrogeno');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_tipo_grupo_electrogeno',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_grupo_electrogeno');
	}
}