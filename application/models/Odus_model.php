<?php
class Odus_model extends CI_Model {

	public function __construct()
	{
		require_once("Populate.php");
		$this->load->database();
		$this->populate = new Populate();
	}

	public function get_odus()
	{
		$this->db->order_by("id","ASC");
		$odus = $this->db->get("tb_odus_enlaces");
		return $odus->result_array();
	}

	public function get_odus_sitio($id)
	{
		$this->db->order_by("id","ASC");
		$odus = $this->db->get_where("tb_odus_enlaces",array('sitio' => $id, 'eliminado' => 0));

		$odus_array = $odus->result_array();

		$odus_array = $this->populate->populate("tb_odus_enlaces",$odus_array,true);

		return $odus_array;
	}

	public function edit($id)
	{
		$odu = $this->db->get_where("tb_odus_enlaces",array('id' => $id),1);
		$odu_array = $odu->result_array();

		$odu_array = $this->populate->populate("tb_odus_enlaces",$odu_array,true,true);

		return $odu_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$resp = $this->db->update('tb_odus_enlaces');
		return $resp;
	}

	public function update_where($parametros,$datos)
	{
		$this->db->set($datos);
		foreach($parametros as $campo => $valor)
		{
			$this->db->where($campo,$valor);
		}
		$resp = $this->db->update('tb_odus_enlaces');
		/*return $resp;*/
		return $resp;
	}

	public function insert($datos)
	{
		$resultado = $this->db->insert('tb_odus_enlaces',$datos);

		$resp = array(
				"resp" => $resultado,
				"last_id" => $this->db->insert_id()
			);

		return $resp;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_odus_enlaces');
	}

	public function get_enlace_odu($id)
	{
		$this->db->select("ss.id, ss.nombre_completo, i2.sitio");
		$this->db->join("tb_enlaces e", "e.id = ame.enlace");
		$this->db->join("tb_idus i", "i.id = e.idu_1 OR i.id = e.idu_2", "left");
		$this->db->join("tb_status_site ss", "ss.id = i.sitio", "left");
		$this->db->join("tb_idus i2", "i2.id = ame.idu", "left");
		$enlace = $this->db->get_where("tb_odus_enlaces ame", array("ame.id" => $id), 2);
		$enlace_array = $enlace->result_array();

		foreach($enlace_array as $key => $enlace)
		{
			if($enlace["id"] != $enlace["sitio"])
			{
				$enlace_array[1] = $enlace;
			}
			else
			{
				$enlace_array[0] = $enlace;
			}
		}

		return $enlace_array;
	}

	public function get_odus_enlace_idu($enlace, $idu)
	{
		$query = $this->db->get_where("tb_odus_enlaces", array("enlace" => $enlace, "idu" => $idu, "eliminado" => 0));
		return $query->result_array();
	}

	public function update_odus_enlace($id, $datos)
	{
		if(isset($datos["numero_serie"]))
		{
			unset($datos["numero_serie"]);
		}

		$odu = $this->edit($id);

		$odus = $this->get_odus_enlace_idu($odu[0]["enlace"], $odu[0]["idu"]);

		foreach($odus as $update)
		{
			$this->update($update["id"], $datos);
		}
	}

	public function delete_where($parametros)
	{
		foreach($parametros as $campo => $valor)
		{
			$this->db->where($campo,$valor);
		}

		return $this->db->delete("tb_odus_enlaces");
	}
}