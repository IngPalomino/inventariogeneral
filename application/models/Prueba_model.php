<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prueba_model extends CI_Model {

	public function __construct()
	{
		$this->load->model('Populator_model', 'Populate_model');
	}

	public function test()
	{
		return $this->Populate_model->get('tb_usuarios')
									->result_array()
									->populate();
	}
}