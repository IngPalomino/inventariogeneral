<?php
class Camuflaje_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_Camuflaje()
	{
		$this->db->order_by("id","ASC");
		$camuflajes = $this->db->get("tb_camuflaje");
		return $camuflajes->result_array();
	}

	public function edit($id)
	{
		$camuflaje = $this->db->get_where("tb_camuflaje",array('id' => $id),1);
		return $camuflaje->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_camuflaje');
	}

	public function insert($datos)
	{
		$this->db->insert('tb_camuflaje',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_camuflaje');
	}
}