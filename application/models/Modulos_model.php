<?php
class Modulos_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function select()
	{
		$listado = $this->db->get("tb_modulos");
		return $listado->result_array();
	}

	public function insert()
	{

	}

	public function update($id,$put)
	{
		return $this->db->where("id",$id)->update("tb_modulos",$put);
	}

	public function delete($id)
	{
		$salida = array(
				"respuesta" => false
			);

		$respuesta = $this->db->where("id",$id)
								->update("tb_modulos",array("eliminado" => 1));

		if($respuesta)
		{
			$salida["respuesta"] = true;
		}

		return $salida;
	}

	public function edit($id)
	{
		$listado = $this->db->get_where("tb_modulos",array("id" => $id));
		return $listado->result_array();
	}
}