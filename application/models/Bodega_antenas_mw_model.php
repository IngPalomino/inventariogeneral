<?php
class Bodega_antenas_mw_model extends CI_Model {

	public function __construct()
	{
		require_once("Populate.php");
		$this->load->database();
		$this->populate = new Populate();
	}
	
	public function get_antenas_mw()
	{
		$this->db->order_by("id", "ASC");
		$this->db->where(array('eliminado' => 0));
		$antenas = $this->db->get("tb_bodega_antenas_mw");
		$antenas_array = $antenas->result_array();
		$antenas_array = $this->populate->populate("tb_bodega_antenas_mw",$antenas_array,true);
		
		foreach($antenas_array as &$antena)
		{
			$this->db->select("id, nombre_completo");
			$nombre_completo = $this->db->get_where("tb_status_site",array('id' => $antena["proveniente_sitio"]));
			$antena["proveniente_sitio"] = $nombre_completo->row_array();

			switch($antena["tipo_almacen"])
			{
				case 1:
					$almacen = $this->db->get_where("tb_almacenes",array("id" => $antena["almacen"]));
					$antena["almacen"] = $almacen->row_array();
					break;
				case 2:
					$this->db->select("id, nombre_completo");
					$sitio = $this->db->get_where("tb_status_site",array("id" => $antena["almacen"]));
					$almacen = $sitio->row_array();
					$antena["almacen"] = array(
							"id" => $almacen["id"],
							"almacen" => $almacen["nombre_completo"]
						);
					break;
			}
		}

		return $antenas_array;
	}
	
	public function get_antenas_mw_almacen($id)
	{
		$this->db->order_by("id", "ASC");
		$antenas = $this->db->get_where("tb_bodega_antenas_mw",array('eliminado' => 0, 'tipo_almacen' => 1, 'almacen' => $id));
		$antenas_array = $antenas->result_array();
		$antenas_array = $this->populate->populate("tb_bodega_antenas_mw",$antenas_array,true);

		foreach ($antenas_array as &$antena)
		{
			$this->db->select("id, nombre_completo");
			$nombre_completo = $this->db->get_where("tb_status_site",array('id' => $antena["proveniente_sitio"]));
			$antena["proveniente_sitio"] = $nombre_completo->row_array();
		}

		return $antenas_array;
	}
	
	public function get_antenas_mw_sitio($id)
	{
		$this->db->order_by("id", "ASC");
		$antenas = $this->db->get_where("tb_bodega_antenas_mw",array('eliminado' => 0, 'tipo_almacen' => 2, 'almacen' => $id));
		$antenas_array = $antenas->result_array();
		$antenas_array = $this->populate->populate("tb_bodega_antenas_mw",$antenas_array,true);

		foreach ($antenas_array as &$antena)
		{
			$this->db->select("id, nombre_completo");
			$nombre_completo = $this->db->get_where("tb_status_site",array('id' => $antena["proveniente_sitio"]));
			$antena["proveniente_sitio"] = $nombre_completo->row_array();
		}

		return $antenas_array;
	}

	public function edit($id)
	{
		$antena = $this->db->get_where("tb_bodega_antenas_mw",array('id' => $id),1);
		$antena_array = $antena->result_array();
		$antena_array = $this->populate->populate("tb_bodega_antenas_mw",$antena_array,true,true);
		return $antena_array;
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		return $this->db->update('tb_bodega_antenas_mw');
	}

	public function insert($datos)
	{
		return $this->db->insert('tb_bodega_antenas_mw',$datos);
	}
}