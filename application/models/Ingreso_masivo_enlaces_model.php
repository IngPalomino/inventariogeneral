<?php
class Ingreso_masivo_enlaces_model extends CI_Model{

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Populate_model');
	}

	public function esNombreCompletoValido($nombre)
	{
		$sitio = $this->db->select("id")
						->get_where("tb_status_site", array("nombre_completo" => $nombre), 1);

		if($sitio->num_rows() > 0)
		{
			return $sitio->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esNombreCodigoValidos($codigo, $nombre)
	{
		$sitio = $this->db->select("id")
						->get_where("tb_status_site", array("codigo" => $codigo, "nombre_completo" => $nombre), 1);

		if($sitio->num_rows() > 0)
		{
			return $sitio->row_array();
		}
		else
		{
			return false;
		}
	}

	public function existeIDUEnSitio($sitio, $neid)
	{
		$query = $this->db->get_where("tb_idus", array("sitio" => $sitio, "ne_id" => $neid, "eliminado" => 0), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function existeEnlace($idu_1, $idu_2)
	{
		$this->db->group_start();
			$this->db->group_start();
				$this->db->where(array("idu_1" => $idu_1, "idu_2" => $idu_2));
			$this->db->group_end();
			$this->db->or_group_start();
				$this->db->where(array("idu_1" => $idu_2, "idu_2" => $idu_1));
			$this->db->group_end();
		$this->db->group_end();
		$this->db->where("eliminado", 0);
		$query = $this->db->get("tb_enlaces", 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}

		/*return $this->db->last_query();*/
	}

	public function esTipoOduValido($tipo)
	{
		$query = $this->db->get_where("tb_tipo_odu", array("tipo" => $tipo), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function esMarcaAntenaValida($marca)
	{
		$query = $this->db->get_where("tb_tipo_antena_mw", array("marca" => $marca));

		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function esModeloAntenaValido($modelo)
	{
		$query = $this->db->get_where("tb_tipo_antena_mw", array("modelo" => $modelo));

		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function esTipoAntenaValido($marca, $modelo)
	{
		$query = $this->db->get_where("tb_tipo_antena_mw", array("marca" => $marca, "modelo" => $modelo), 1);

		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function existenODUs($enlace, $idu)
	{
		$query = $this->db->get_where("tb_odus_enlaces", array("enlace" => $enlace, "idu" => $idu, "eliminado" => 0));

		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function existenAntenas($enlace, $idu)
	{
		$query = $this->db->get_where("tb_antenas_mw_enlaces", array("enlace" => $enlace, "idu" => $idu, "eliminado" => 0));

		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function registrar_ingreso_masivo($usuario)
	{
		$datos["usuario"] = $usuario;
		$datos["tabla"] = 4;

		if($this->db->insert("tb_ingreso_masivo", $datos))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}

	public function ingresar_temporal($datos)
	{
		$respuesta["resp"] = $this->db->insert("tb_temp_enlaces", $datos);
		$respuesta["last_id"] = $this->db->insert_id();

		return $respuesta;
	}

	public function ingresar_temporal_odus($datos)
	{
		return $this->db->insert("tb_temp_odus_enlaces", $datos);
	}

	public function ingresar_temporal_antenas($datos)
	{
		return $this->db->insert("tb_temp_antenas_mw_enlaces", $datos);
	}

	public function update($id, $datos)
	{
		$query = $this->db->set($datos)
							->where("id", $id)
							->update("tb_temp_enlaces");
		return $query;
	}

	public function get_solicitudes($usuario)
	{
		$this->db->order_by("id", "DESC");
		$query = $this->db->get_where("tb_ingreso_masivo", array("usuario" => $usuario, "tabla" => 4));
		$soliticudes = $query->result_array();
		$soliticudes = $this->Populate_model->populate("tb_ingreso_masivo", $soliticudes, true);
		return $soliticudes;
	}
}