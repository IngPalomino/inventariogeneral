<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mso_categoria_model extends CI_Model {

	private $table;

	public function __construct()
	{
		$this->load->model('Populate_model');
		$this->table = 'tb_mso_categoria';
	}

	public function get_all()
	{

	$lista = $this->db->order_by('id', 'asc')	                    
						->get($this->table)
						->result_array();						
        
		return $lista;	
	}
	
    public function get_id($id)
	{
		
	$lista = $this->db->order_by('id', 'asc')
	                    ->where('id', $id)
						->get($this->table)
						->result_array();						
        
		return $lista;	
	}


    


}