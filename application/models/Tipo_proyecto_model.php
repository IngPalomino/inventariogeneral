<?php
class Tipo_proyecto_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_proyecto()
	{
		$this->db->order_by("tipo_proyecto","ASC");
		$query = $this->db->get("tb_tipo_proyecto");
		return $query->result_array();
	}

	public function edit($id)
	{
		$query = $this->db->get_where("tb_tipo_proyecto",array('id' => $id),1);
		return $query->result_array();
	}

	public function update($id,$datos)
	{
		$this->db->set($datos);
		$this->db->where('id',$id);
		$this->db->update('tb_tipo_proyecto');
	}

	public function insert($datos)
	{
		$this->db->insert('tb_tipo_proyecto',$datos);
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tb_tipo_proyecto');
	}
}