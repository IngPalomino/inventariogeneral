<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('array_to_ul'))
{
	function array_to_ul($info_array)
	{
		$return = "<ul>";

		foreach ($info_array as $key => $value)
		{
			if(!is_array($value))
			{
				if($key != "id" && $key != "sitio" && $key != "eliminado" && $key != "created_at")
				{
					$return .= "<li>".$key.": ".$value."</li>";
				}
			}
			else
			{
				$return .= "<li>".(is_int($key)? $key+1 : $key).":".array_to_ul($value)."</li>";
			}
		}

		$return .= "</ul>";
		return $return;
	}
}

if( ! function_exists('isAdmin'))
{
	function isAdmin($usuario)
	{
		$CI =& get_instance();
		$CI->load->model('Autorizacion_grupos_model');
		return $CI->Autorizacion_grupos_model->isAdmin($usuario);
	}
}

if( ! function_exists('isSuperAdmin'))
{
	function isSuperAdmin($usuario)
	{
		$CI =& get_instance();
		$CI->load->model('Autorizacion_grupos_model');
		return $CI->Autorizacion_grupos_model->isSuperAdmin($usuario);
	}
}

if( ! function_exists('isPermitted'))
{
	function isPermitted($usuario, $modulo, $sitio = null)
	{
		$CI =& get_instance();
		$CI->load->model('Autorizacion_grupos_model');
		return $CI->Autorizacion_grupos_model->comprobar_autorizacion_modulo($usuario, $modulo, $sitio);
	}
}