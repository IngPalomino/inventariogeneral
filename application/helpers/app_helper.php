<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_navbar'))
{
	function get_navbar()
	{
		get_instance()->load->view('layouts/navbar');
	}
}

if ( ! function_exists('get_navbar_site'))
{
	function get_navbar_site($site_view)
	{
		get_instance()->load->view('layouts/navbar_'.$site_view);
	}
}

if ( ! function_exists('get_content'))
{
	function get_content($view)
	{
		get_instance()->load->view($view);
	}
}

if ( ! function_exists('load_view'))
{
	function load_view($view = '', $view_data = array(), $returnhtml = FALSE)
	{
		get_instance()->load->view($view, $view_data, $returnhtml);
	}
}