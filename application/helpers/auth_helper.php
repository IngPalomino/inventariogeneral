<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('logged_in'))
{
	function logged_in()
	{
		return get_instance()->auth->logged_in();
	}
}

if ( ! function_exists('is_admin'))
{
	function is_admin($id = FALSE)
	{
		return get_instance()->auth->is_admin($id);
	}
}

if ( ! function_exists('is_super_admin'))
{
	function is_super_admin($id = FALSE)
	{
		return get_instance()->auth->is_super_admin($id);
	}
}

if ( ! function_exists('allowed_module'))
{
	function allowed_module($modulo, $sitio = NULL, $id = FALSE)
	{
		return get_instance()->auth->allowed_module($modulo, $sitio, $id) ? 'false' : 'true';
	}
}