<?php

class Aires_acondicionados_test extends TestCase {

	private $controller;

	public function __construct()
	{
		parent::__construct();
		$this->controller = 'aires_acondicionados';
	}

	public function test_listar_sitio()
	{
		$output = $this->request('GET', $this->controller . '/listar_sitio/1');
		$expected = '[]';

		$this->assertContains($expected, $output);
		$this->assertResponseCode(200);
	}

	public function test_editar()
	{
		$output = $this->request('GET', $this->controller . '/editar/1');
		$expected = 'id';

		$this->assertContains($expected, $output);
		$this->assertResponseCode(200);
	}

	public function test_actualizar()
	{
		$_SESSION = array(
			'id_usuario' => 11,
			'usuario' => 'alex.nima@osctelecoms.com',
			'nombres' => 'Alex',
			'apellidos' => 'Nima'
		);

		$this->request->setHeader('Content-Type', 'application/x-www-form-urlencoded');
		$output = $this->request('PUT', $this->controller . '/actualizar/1', 'num_serie_condensador=Pendiente&num_serie_evaporador=Pendiente');

		$this->assertResponseCode(200);
	}
}