<br>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<h4>Microondas</h4>
			<!--<div>
				<button class="btn btn-default btn-xs" onclick="$('#nuevaAntenaMicroondas').bPopup();"><span class="fa fa-plus"></span> Agregar antena</button>
			</div>-->
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<!--<th>Acciones</th>-->
						<th>Enlace</th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Número de serie</th>
						<th>Altura</th>
					</tr>
				</thead>
				<tbody class="ng-cloak">
	                <tr ng-repeat="antenape in ctrl.antenasMicroondasPendiente"  ng-class="{'red': antenape.estado_aprobacion == '2' , 'naranja': antenape.estado_aprobacion == '1'}" >
						<td>{{antenape.enlace.idu_1.sitio.nombre_completo}} <==> {{antenape.enlace.idu_2.sitio.nombre_completo}}</td>
						<td>{{antenape.tipo_antena_mw.marca}}</td>
						<td>{{antenape.tipo_antena_mw.modelo}}</td>
						<td>{{antenape.numero_serie}}</td>
						<td>{{antenape.altura}} m</td>
					</tr>					
					<tr ng-repeat="antena in ctrl.antenasMicroondas">
						<!--<td><button class="btn btn-xs btn-info" ng-click="ctrl.almacenandoAntenaMicroondas(antena)">Almacenar</button></td>-->
						<td>{{antena.enlace.idu_1.sitio.nombre_completo}} <==> {{antena.enlace.idu_2.sitio.nombre_completo}}</td>
						<td>{{antena.tipo_antena_mw.marca}}</td>
						<td>{{antena.tipo_antena_mw.modelo}}</td>
						<td>{{antena.numero_serie}}</td>
						<td>{{antena.altura}} m</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--<div class="col-xs-4">
			<div>
				<h4>Gráfico</h4>
			</div>
		</div>-->
	</div>
	<div class="row">
		<div class="col-xs-12">
			<h4>GUL</h4>
			<div>
	<button class="btn btn-default btn-xs" onclick="$('#nuevaAntenaGUL').bPopup();VerificarFactorUso();GrupoFunciones();" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><span class="fa fa-plus"></span> Agregar antena</button>
				<button class="btn btn-info btn-xs" ng-click="ctrl.importandoAntenaGUL()" ng-disabled="<?php echo allowed_module($modulo, $sitio); ?>"><i class="fa fa-plus-square"></i> Importar desde bodega</button>
			</div>
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th><i class="fa fa-cogs"></i></th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Sector</th>
						<th>Altura</th>
						<th>Azimuth</th>
						<th>Tilt Mecánico</th>
						<th>Tilt Eléctrico</th>
						<th>Número de Serie</th>
						<th>Historial</th>						
					</tr>
				</thead>
<tbody class="ng-cloak">
<!-- solicitud reserva de espacio  -->
          <tr ng-repeat="antenape in ctrl.antenasGULPendiente" ng-class="{'red': antenape.estado == '1' , 'naranja': antenape.estado == '0'}" >	
			<td>
				<!-- <div class="dropdown">
					<button class="btn btn-info btn-xs dropdown-toggle" type="button" id="dropdownAntenasGULPendiente" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" ng-disabled="<?php //echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">
						Acciones
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownAntenasGULPendiente">
						<li><a href ng-click="ctrl.editar_antenaGULPendiente(antenape)"><i class="fa fa-edit"></i> Editar</a></li>
						<li><a href ng-click="ctrl.almacenandoAntenaGULPendiente(antenape)"><i class="fa fa-check"></i> Almacenar</a></li>
						<li role="separator" class="divider"></li>
						<li><a href ng-click="ctrl.eliminar_antenaGULPendiente(antenape)"><i class="fa fa-close"></i> Eliminar</a></li>
					</ul>
				</div> -->
			</td>
   

			<td >{{antenape.tipo_antena_gul.marca}}</td>
			<td>{{antenape.tipo_antena_gul.modelo}}</td>
			<td>
				<span ng-if="antenape.tipo_antena_gul.marca != 'TELNET'">{{antenape.sector}}</span>
				<span ng-repeat="sector in antenape.info_telnet" ng-if="antenape.tipo_antena_gul.marca == 'TELNET'">
					<span ng-if="$index > 0 && sector.sector">/</span>
					<span>{{sector.sector}}</span>
				</span>
			</td>
			<td>{{antenape.altura}} m</td>
			<td>
				<span ng-if="antenape.tipo_antena_gul.marca != 'TELNET'">{{antenape.azimuth}}°</span>
				<span ng-repeat="sector in antenape.info_telnet" ng-if="antenape.tipo_antena_gul.marca == 'TELNET'">
					<span ng-if="$index > 0">/</span>
					<span>{{sector.azimuth}}°</span>
				</span>
			</td>
			<td>
				<span ng-if="antenape.tipo_antena_gul.marca != 'TELNET'">{{antenape.tilt_mecanico}}°</span>
				<span ng-repeat="sector in antenape.info_telnet" ng-if="antenape.tipo_antena_gul.marca == 'TELNET'">
					<span ng-if="$index > 0">/</span>
					<span>{{sector.tilt_mecanico}}°</span>
				</span>
			</td>
			<td>
				<span ng-if="antenape.tipo_antena_gul.marca != 'TELNET'">{{antenape.tilt_electrico}}°</span>
				<span ng-repeat="sector in antenape.info_telnet" ng-if="antenape.tipo_antena_gul.marca == 'TELNET'">
					<span ng-if="$index > 0">/</span>
					<span>{{sector.tilt_electrico}}°</span>
				</span>
			</td>
			<td>{{antenape.numero_serie}}</td>
			<td style="font-size:8pt;">
				<span>Creado: {{antenape.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{antenape.nombres}} {{antenape.apellido_paterno}}  </span>
				<!-- 
				<span ng-if="antenape.logs_antenas_gul_pendiente[0].evento.tipo == 'INSERT'">Creado: {{antenape.logs_antenas_gul_pendiente[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{antenape.logs_antenas_gul_pendiente[0].idusuario.nombres}} {{antenape.logs_antenas_gul_pendiente[0].idusuario.apellido_paterno}}</span>
				<br>
				<span ng-if="antenape.logs_antenas_gul_pendiente[1].evento.tipo == 'UPDATE'">Actualizado: {{antenape.logs_antenas_gul_pendiente[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{antenape.logs_antenas_gul_pendiente[1].idusuario.nombres}} {{antenape.logs_antenas_gul_pendiente[1].idusuario.apellido_paterno}}</span> -->
			</td>
		</tr>
        <!-- solicitud reserva de espacio -->
					<tr ng-repeat="antena in ctrl.antenasGUL">
						<td>
							<div class="dropdown">
								<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdownAntenasGUL" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">
									Acciones
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownAntenasGUL">
									<li><a href ng-click="ctrl.editar_antenaGUL(antena)"><i class="fa fa-edit"></i> Editar</a></li>
									<li><a href ng-click="ctrl.almacenandoAntenaGUL(antena)"><i class="fa fa-check"></i> Almacenar</a></li>
									<li role="separator" class="divider"></li>
									<li><a href ng-click="ctrl.eliminar_antenaGUL(antena)"><i class="fa fa-close"></i> Eliminar</a></li>
								</ul>
							</div>
						</td>
						<td>{{antena.tipo_antena_gul.marca}}</td>
						<td>{{antena.tipo_antena_gul.modelo}}</td>
						<td>
							<span ng-if="antena.tipo_antena_gul.marca != 'TELNET'">{{antena.sector}}</span>
							
                           
							
						   
		
			<span ng-if="antena.tipo_antena_gul.marca == 'TELNET'">	
                <span ng-if="antena.info_telnet.length > 0">
						<span ng-repeat="sector in antena.info_telnet" ng-if="antena.tipo_antena_gul.marca == 'TELNET'">
									<span ng-if="$index > 0 && sector.sector">/</span>
									<span>{{sector.sector}}</span>
						</span>
                </span>
                <span ng-if="antena.info_telnet.length == 0">
					{{antena.sector}}
                </span>
            </span>
                           

						</td>
						<td>{{antena.altura}} m</td>
						<td>





							<span ng-if="antena.tipo_antena_gul.marca != 'TELNET'">{{antena.azimuth}}°</span>


						<span ng-if="antena.tipo_antena_gul.marca == 'TELNET'">

                         <span ng-if="antena.info_telnet.length > 0">
                         	<span ng-if="antena.tipo_antena_gul.marca != 'TELNET'">{{antena.azimuth}}°</span>
							<span ng-repeat="sector in antena.info_telnet" ng-if="antena.tipo_antena_gul.marca == 'TELNET'">
								<span ng-if="$index > 0">/</span>
								<span>{{sector.azimuth}}°</span>
							</span>
                         </span>
                          <span ng-if="antena.info_telnet.length == 0">
                          	<span>{{antena.azimuth}}°</span>
                         </span>

						</span>


						</td>
						<td>
							<span ng-if="antena.tipo_antena_gul.marca != 'TELNET'">{{antena.tilt_mecanico}}°</span>			




                        <span ng-if="antena.tipo_antena_gul.marca == 'TELNET'">

                         <span ng-if="antena.info_telnet.length > 0">
                         	<span ng-repeat="sector in antena.info_telnet" ng-if="antena.tipo_antena_gul.marca == 'TELNET'">
								<span ng-if="$index > 0">/</span>
								<span>{{sector.tilt_mecanico}}°</span>
							</span>
                         </span>
                          <span ng-if="antena.info_telnet.length == 0">
                          	<span>{{antena.tilt_mecanico}}°</span>
                         </span>

						</span>


						</td>
						<td>
						<span ng-if="antena.tipo_antena_gul.marca != 'TELNET'">{{antena.tilt_electrico}}°</span>



							



                      <span ng-if="antena.tipo_antena_gul.marca == 'TELNET'">

                         <span ng-if="antena.info_telnet.length > 0">
                         	<span ng-repeat="sector in antena.info_telnet" ng-if="antena.tipo_antena_gul.marca == 'TELNET'">
								<span ng-if="$index > 0">/</span>
								<span>{{sector.tilt_electrico}}°</span>
							</span>
                         </span>
                          <span ng-if="antena.info_telnet.length == 0">
                          	<span>{{antena.tilt_electrico}}°</span>
                         </span>

						</span>







						</td>
						<td>{{antena.numero_serie}}</td>

					<td style="font-size:8pt;">
							


							<span ng-if="!antena.logs_antenas_gul.length || antena.logs_antenas_gul[0].evento.tipo != 'INSERT'">Creado: {{antena.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>

							<span ng-if="antena.logs_antenas_gul[0].evento.tipo == 'INSERT'">Creado: {{antena.logs_antenas_gul[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{antena.logs_antenas_gul[0].usuario.nombres}} {{antena.logs_antenas_gul[0].usuario.apellido_paterno}}</span>

							<br>


							<span ng-if="antena.logs_antenas_gul[1].evento.tipo == 'UPDATE'">Actualizado: {{antena.logs_antenas_gul[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{antena.logs_antenas_gul[1].usuario.nombres}} {{antena.logs_antenas_gul[1].usuario.apellido_paterno}}</span>



						</td>
					</tr>
					<tr>
						<td colspan="9"></td>
						<td class="info">#</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--<div class="col-xs-4">
			<div>
				<h4>Gráfico</h4>
			</div>
		</div>-->
	</div>
	<div class="row">
		<div class="col-xs-12">
			<h4>iDEN</h4>
			<div>
				<button class="btn btn-default btn-xs" onclick="$('#nuevaAntenaIDEN').bPopup();VerificarFactorUso();" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><span class="fa fa-plus"></span> Agregar antena</button>
			</div>
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th><i class="fa fa-cogs"></i></th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Sector</th>
						<th>Altura</th>
						<th>Azimuth</th>
						<th>Tilt Eléctrico</th>
						<th>Tilt Mecánico</th>
						<th>Números de serie</th>
						<th>Cantidad</th>
					</tr>
				</thead>
				<tbody class="ng-cloak">
					<tr ng-repeat="antena in ctrl.antenasIDEN">
						<td>
							<div class="dropdown">
								<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdownAntenasIDEN" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">
									Acciones
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownAntenasIDEN">
									<li><a href ng-click="ctrl.editar_antenaIDEN(antena.id)"><i class="fa fa-edit"></i> Editar</a></li>
									<li><a href ng-click="ctrl.almacenandoAntenaIDEN(antena)"><i class="fa fa-check"></i> Almacenar</a></li>
									<li role="separator" class="divider"></li>
									<li><a href ng-click="ctrl.eliminar_antenaIDEN(antena.id)"><i class="fa fa-close"></i> Eliminar</a></li>
								</ul>
							</div>
							<!--<button class="btn btn-warning btn-xs" ng-click="ctrl.editar_antenaIDEN(antena.id)">Editar</button>
							<button class="btn btn-xs btn-info" ng-click="ctrl.almacenandoAntenaIDEN(antena)">Almacenar</button>
							<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminar_antenaIDEN(antena.id)">Eliminar</button>-->
						</td>
						<td>{{antena.tipo_antena_iden.marca}}</td>
						<td>{{antena.tipo_antena_iden.modelo}}</td>
						<td>{{antena.sector}}</td>
						<td>{{antena.altura}} m</td>
						<td>{{antena.azimuth}}°</td>
						<td>{{antena.tilt_electrico}}</td>
						<td>{{antena.tilt_mecanico}}</td>
						<td>{{antena.numero_serie}}</td>
						<td>{{antena.cantidad}}</td>
					</tr>
					<tr>
						<td colspan="9"></td>
						<td class="info">#</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--<div class="col-xs-4">
			<h4>Gráfico</h4>
		</div>-->
	</div>
	<!--<div class="row">
		<div class="col-xs-12">
			<h4>Wimax</h4>
			<div>
				<button class="btn btn-default btn-xs"><span class="fa fa-plus"></span> Agregar antena</button>
			</div>
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Versión</th>
						<th>Altura</th>
						<th>Cantidad</th>
					</tr>
				</thead>
				<tbody class="ng-cloak">
					<tr>
						<td colspan="4"></td>
						<td class="info">#</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>-->
	<div class="row ng-cloak" ng-if="ctrl.info_sitio.in_building == 1">
		<div class="col-xs-12">
			<h4>In Building</h4>
			<div>
				<button class="btn btn-default btn-xs" ng-click="ctrl.nueva_antena_in_building()"><span class="fa fa-plus"></span> Agregar antena</button>
			</div>
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Acciones</th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Cantidad</th>
						<th>Tipo</th>
						<th>Configuración</th>
						<th>Banda</th>
						<th>Peso</th>
						<th>Dimensiones</th>
					</tr>
				</thead>
				<tbody class="ng-cloak">
					<tr ng-repeat="antena in ctrl.antenas_in_building">
						<td>
							<button class="btn btn-warning btn-xs" ng-click="ctrl.editar_antenaInBuilding(antena.id)">Editar</button>
							<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminandoAntenaInBuilding(antena)">Eliminar</button>
						</td>
						<td>{{antena.tipo_antena_in_building.marca}}</td>
						<td>{{antena.tipo_antena_in_building.modelo}}</td>
						<td>{{antena.cantidad}}</td>
						<td>{{antena.tipo_antena_in_building.tipo}}</td>
						<td>{{antena.tipo_antena_in_building.configuracion}}</td>
						<td>{{antena.tipo_antena_in_building.banda}} GHz</td>
						<td>{{antena.tipo_antena_in_building.peso}} Kg</td>
						<td>{{antena.tipo_antena_in_building.dimensiones}} mm</td>
					</tr>
					<tr>
						<td colspan="8"></td>
						<td class="info">#</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--<div class="col-xs-4">
			<h4>Gráfico</h4>
		</div>-->
	</div>
</div>

<!-- ANTENAS MICROONDAS -->

<div id="nuevaAntenaMicroondas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nueva antena microondas</h3>

	<form class="verifica_factor_uso">
		
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Número de serie</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.nuevaAntenaMicroondas.numero_serie">
					</td>
				</tr>
				<tr>
					<td class="info">Código de inventario</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.nuevaAntenaMicroondas.numero_inventario">
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaMicroondas.marca" ng-change="ctrl.listarModelosMicroondas(ctrl.nuevaAntenaMicroondas.marca)">
							<option ng-repeat="marca in ctrl.marcasMicroondas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaMicroondas.modelo" ng-change="ctrl.listarVersionesMicroondas(ctrl.nuevaAntenaMicroondas.marca,ctrl.nuevaAntenaMicroondas.modelo)">
							<option ng-repeat="modelo in ctrl.modelosMicroondas" value="{{modelo.modelo}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Versión</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaMicroondas.version">
							<option ng-repeat="version in ctrl.versionesMicroondas" value="{{version.version}}">{{version.version}}</option>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarMicroondas()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionMicroondas()">Cancelar</button>
	</form>
</div>

<div id="editarAntenaMicroondas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Editar antena microondas</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Número de serie</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.editarAntenaMicroondas.numero_serie">
					</td>
				</tr>
				<tr>
					<td class="info">Código de inventario</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.editarAntenaMicroondas.numero_inventario">
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaMicroondas.tipo_antena_microondas.marca" ng-change="ctrl.listarModelosMicroondas(ctrl.editarAntenaMicroondas.marca)">
							<option ng-repeat="marca in ctrl.marcasMicroondas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaMicroondas.tipo_antena_microondas.modelo" ng-change="ctrl.listarVersionesMicroondas(ctrl.editarAntenaMicroondas.marca,ctrl.editarAntenaMicroondas.modelo)">
							<option ng-repeat="modelo in ctrl.modelosMicroondas" value="{{modelo.modelo}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Versión</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaMicroondas.tipo_antena_microondas.version">
							<option ng-repeat="version in ctrl.versionesMicroondas" value="{{version.version}}">{{version.version}}</option>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.actualizarMicroondas(ctrl.editarAntenaMicroondas.id)">Guardar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionMicroondas()">Cancelar</button>
	</form>
</div>

<div id="eliminarAntenaMicroondas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar antena microondas</h3>

	<p>
		¿Está seguro de querer eliminar la antena microondas con características
	</p>
	<p>
		Marca: <b>{{ctrl.eliminarAntenaMicroondas.tipo_antena_microondas.marca}}</b>, Modelo: <b>{{ctrl.eliminarAntenaMicroondas.tipo_antena_microondas.modelo}}</b>, Versión: <b>{{ctrl.eliminarAntenaMicroondas.tipo_antena_microondas.version}}</b>
	</p>
	<p>
		y serie: <b>{{ctrl.eliminarAntenaMicroondas.serie}}</b>?
	</p>
	
	<button class="btn btn-success" ng-click="ctrl.eliminarMicroondas()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionMicroondas()">Cancelar</button>
</div>

<div id="almacenarAntenaMicroondas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Almacenar antena microondas</h3>

	<p>Elegir destino:</p>

	<div style="max-width:400px">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a data-toggle="tab" href="#almacen" ng-click="ctrl.microondasOpcionAlmacen()">Almacén</a></li>
			<li><a data-toggle="tab" href="#sitio" ng-click="ctrl.microondasOpcionSitio()">Sitio</a></li>
		</ul>
		<div class="tab-content">
			<div id="almacen" class="tab-pane fade in active">
				<select class="form-control" ng-model="ctrl.almacenar_AntenaMicroondas.almacen">
					<option value="">Elegir almacén</option>
					<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
				</select>
			</div>
			<div id="sitio" class="tab-pane fade">
				<label style="width:100%">
					<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenar_AntenaMicroondas.a_sitio"></selectize>
				</label>
			</div>
		</div>
		<select class="form-control" ng-model="ctrl.almacenar_AntenaMicroondas.estado">
			<option value="">Elegir estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select>
	</div><br>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea almacenar el elemento..." ng-model="ctrl.almacenar_AntenaMicroondas.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.almacenarAntenaMicroondas(ctrl.almacenar_AntenaMicroondas)" ng-disabled="!ctrl.almacenar_AntenaMicroondas.comentario || (ctrl.almacenar_AntenaMicroondas.tipo_almacen == 1 && !ctrl.almacenar_AntenaMicroondas.almacen) || (ctrl.almacenar_AntenaMicroondas.tipo_almacen == 2 && !ctrl.almacenar_AntenaMicroondas.a_sitio)">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamientoMicroondas()">Cancelar</button>
</div>

<!-- ANTENAS GUL -->


<div id="nuevaAntenaGUL" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nueva antena GUL</h3>

	<form id="FormNuevaAntenaGUL" class="verifica_factor_uso">



   <input type="hidden" name="factor_uso_torre_unico" id="factor_uso_torre_unico" value="{{ctrl.info_torre.factor_uso_torre}}" >

   

		<div id="alerta_de_torre" class="alert alert-danger text-center alerta_de_torre">
			<i class="fa fa-exclamation-triangle fa-2x"></i>Torre bloqueada para nuevas instalaciones!
		</div>
		<div style="max-height:400px;overflow-y:scroll;">
			<table class="table table-condensed">
				<tbody>
					<tr>
						<td class="info">Número de serie</td>
						<td class="default">
							<input onkeyup="VerificarCamposLlenos()"  type="text" class="gul_propertie form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.numero_serie" required>
						</td>
					</tr>
					<tr>
						<td class="info">Marca</td>
						<td class="default">
							<select  onchange="VerificarCamposLlenos()"     class="gul_propertie form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.tipo_antena_gul.marca" ng-change="ctrl.nuevaAntenaGUL.tipo_antena_gul.id = ''; ctrl.cambioMarca()"  required>
								<option value="" >Elegir marca</option>
								<option ng-repeat="marca in ctrl.tipo_antena_gul.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="info">Modelo</td>
						<td class="default">
							<select  onchange="VerificarCamposLlenos()"    class="gul_propertie form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.tipo_antena_gul.id"required>
								<option value="" >Elegir modelo</option>
								<option ng-if="ctrl.nuevaAntenaGUL.tipo_antena_gul.marca" ng-repeat="modelo in ctrl.tipo_antena_gul.modelos | filter:{marca:ctrl.nuevaAntenaGUL.tipo_antena_gul.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="info">Altura</td>
						<td class="default">
							<div class="input-group">
								<input  onkeyup="VerificarCamposLlenos()" type="number" step="any" min="0" class="gul_propertie form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.altura" required>
								<span class="input-group-addon">m</span>
							</div>
						</td>
					</tr>
					<tr ng-if-start="ctrl.nuevaAntenaGUL.tipo_antena_gul.marca != 'TELNET'">
						<td class="info">Sector</td>
						<td class="default">
							<select onchange="VerificarCamposLlenos()"   class="gul_propertie form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.sector" required>
								<option value="" >Seleccione un sector</option>
								<?php for($i=1; $i < 11; $i++):?>
									<option value="<?php echo $i?>"><?php echo $i?></option>
								<?php endfor?>
							</select>
						</td>
					</tr>
					<tr>
						<td class="info">Azimuth</td>
						<td class="default">
							<div class="input-group">
								<input      onkeyup="VerificarCamposLlenos()"  type="number" step="any" min="0" class="gul_propertie form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.azimuth"  required>
								<span class="input-group-addon">°</span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="info">Tilt Eléctrico</td>
						<td class="default">
							<div class="input-group">
								<input      onkeyup="VerificarCamposLlenos()"    type="number" step="any" class="gul_propertie form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.tilt_electrico" required>
								<span class="input-group-addon">°</span>
							</div>
						</td>
					</tr>
					<tr ng-if-end>
						<td class="info">Tilt Mecánico</td>
						<td class="default">
							<div class="input-group">								
								<input onkeyup="VerificarCamposLlenos()"    type="number" step="any" class="gul_propertie form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.tilt_mecanico" required>
								<span class="input-group-addon">°</span>
							</div>
						</td>
					</tr>
					<tr class="success" ng-repeat-start="sector in ctrl.nuevaAntenaGUL.info_telnet" ng-if-start="ctrl.nuevaAntenaGUL.tipo_antena_gul.marca == 'TELNET'">
						<td>Sector</td>
						<td>
							<select onchange="VerificarCamposLlenos()"  class="gul_propertie form-control input-sm" ng-model="sector.sector"  required>
								<option value="" >Seleccione un sector</option>
								<?php for($i=1; $i < 11; $i++):?>
									<option value="<?php echo $i?>"><?php echo $i?></option>
								<?php endfor?>
							</select>
						</td>
					</tr>


					<tr>
						<td class="info">Azimuth</td>
						<td class="default">
							<div class="input-group">
								<input  onkeyup="VerificarCamposLlenos()"  type="number" step="any" min="0" class="gul_propertie form-control input-sm" ng-model="sector.azimuth" required>
								<span class="input-group-addon">°</span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="info">Tilt Eléctrico</td>
						<td class="default">
							<div class="input-group">
								<input  onkeyup="VerificarCamposLlenos()"  type="number" step="any" class="gul_propertie form-control input-sm" ng-model="sector.tilt_electrico" required>
								<span class="input-group-addon">°</span>
							</div>
						</td>
					</tr>
					<tr ng-repeat-end ng-if-end>
						<td class="info" >Tilt Mecánico</td>
						<td class="default">
							<div class="input-group">
								<input  onkeyup="VerificarCamposLlenos()"  type="number" step="any" class="gul_propertie form-control input-sm" ng-model="sector.tilt_mecanico" required>
								<span class="input-group-addon">°</span>
							</div>

						</td>
					</tr>
				</tbody>
			</table>
		</div><br>
            
		<button class="btn btn-success" ng-click="ctrl.agregarGULPendiente(ctrl.nuevaAntenaGUL)"
		 >Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionGUL()">Cancelar</button>

	</form>
</div>

<div id="editarAntenaGUL" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Editar antena GUL</h3>

	<form>
		<div style="max-height:400px;overflow-y:scroll;">
			<table class="table table-condensed">
				<tbody>
					<tr>
						<td class="info">Número de serie</td>
						<td class="default">
							<input type="text" class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.numero_serie">
						</td>
					</tr>
					<tr>
						<td class="info">Marca</td>
						<td class="default">
							<select class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.tipo_antena_gul.marca" ng-change="ctrl.editarAntenaGUL.tipo_antena_gul.id = ''; ctrl.cambioMarcaE();">
								<option value="">Seleccione una marca</option>
								<option ng-repeat="marca in ctrl.tipo_antena_gul.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="info">Modelo</td>
						<td class="default">
							<select class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.tipo_antena_gul.id">
								<option value="">Seleccione un modelo</option>
								<option ng-if="ctrl.editarAntenaGUL.tipo_antena_gul.marca" ng-repeat="modelo in ctrl.tipo_antena_gul.modelos | filter:{marca:ctrl.editarAntenaGUL.tipo_antena_gul.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="info">Altura</td>
						<td class="default">
							<div class="input-group">
								<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.altura">
								<span class="input-group-addon">m</span>
							</div>
						</td>
					</tr>
					<tr ng-if-start="ctrl.editarAntenaGUL.tipo_antena_gul.marca != 'TELNET'">
						<td class="info">Sector</td>
						<td class="default">
							<select class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.sector">
								<option value="">Seleccione un sector</option>
								<?php for($i=1; $i < 11; $i++):?>
									<option value="<?php echo $i?>"><?php echo $i?></option>
								<?php endfor?>
							</select>
						</td>
					</tr>
					<tr>
						<td class="info">Azimuth</td>
						<td class="default">
							<div class="input-group">
								<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.azimuth">
								<span class="input-group-addon">°</span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="info">Tilt Eléctrico</td>
						<td class="default">
							<div class="input-group">
								<input type="number" step="any" class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.tilt_electrico">
								<span class="input-group-addon">°</span>
							</div>
						</td>
					</tr>
					<tr ng-if-end>
						<td class="info">Tilt Mecánico</td>
						<td class="default">
							<div class="input-group">
								<input type="number" step="any" class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.tilt_mecanico">
								<span class="input-group-addon">°</span>
							</div>
						</td>
					</tr>
					<tr class="success" ng-repeat-start="sector in ctrl.editarAntenaGUL.info_telnet" ng-if-start="ctrl.editarAntenaGUL.tipo_antena_gul.marca == 'TELNET'">
						<td>Sector</td>
						<td>
							<select class="form-control input-sm" ng-model="sector.sector">
								<option value="">Seleccione un sector</option>
								<?php for($i=1; $i < 11; $i++):?>
									<option value="<?php echo $i?>"><?php echo $i?></option>
								<?php endfor?>
							</select>
						</td>
					</tr>
					<tr>
						<td class="info">Azimuth</td>
						<td class="default">
							<div class="input-group">
								<input type="number" step="any" min="0" class="form-control input-sm" ng-model="sector.azimuth">
								<span class="input-group-addon">°</span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="info">Tilt Eléctrico</td>
						<td class="default">
							<div class="input-group">
								<input type="number" step="any" class="form-control input-sm" ng-model="sector.tilt_electrico">
								<span class="input-group-addon">°</span>
							</div>
						</td>
					</tr>
					<tr ng-repeat-end ng-if-end>
						<td class="info">Tilt Mecánico</td>
						<td class="default">
							<div class="input-group">
								<input type="number" step="any" class="form-control input-sm" ng-model="sector.tilt_mecanico">
								<span class="input-group-addon">°</span>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div><br>
		<button class="btn btn-success" ng-click="ctrl.actualizarGUL(ctrl.editarAntenaGUL)" ng-disabled="!ctrl.editarAntenaGUL.tipo_antena_gul.id">Guardar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionGUL()">Cancelar</button>
	</form>
</div>

<div id="eliminarAntenaGUL" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar antena GUL</h3>

	<p>
		¿Está seguro de querer eliminar la antena GUL con características
	</p>
	<p>
		Marca: <b>{{ctrl.eliminarAntenaGUL.tipo_antena_gul.marca}}</b>, Modelo: <b>{{ctrl.eliminarAntenaGUL.tipo_antena_gul.modelo}}</b>, Versión: <b>{{ctrl.eliminarAntenaGUL.tipo_antena_gul.version}}</b>
	</p>
	<p>
		y serie: <b>{{ctrl.eliminarAntenaGUL.numero_serie}}</b>?
	</p>

	<textarea class="form-control" ng-model="ctrl.eliminarAntenaGUL.comentario" style="width:100%;resize: none;" rows="3" placeholder="Por favor indique por qué está eliminando este elemento..."></textarea>
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarGUL()" ng-disabled="!ctrl.eliminarAntenaGUL.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionGUL()">Cancelar</button>
</div>

<div id="almacenarAntenaGUL" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Almacenar antena GUL</h3>

	<p>Elegir destino:</p>

	<div style="max-width:400px">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a data-toggle="tab" href="#almacenGUL" ng-click="ctrl.GULOpcionAlmacen()">Almacén</a></li>
			<li><a data-toggle="tab" href="#sitioGUL" ng-click="ctrl.GULOpcionSitio()">Sitio</a></li>
		</ul>
		<div class="tab-content">
			<div id="almacenGUL" class="tab-pane fade in active">
				<select class="form-control" ng-model="ctrl.almacenar_AntenaGUL.almacen">
					<option value="">Elegir almacén</option>
					<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
				</select>
			</div>
			<div id="sitioGUL" class="tab-pane fade">
				<label style="width:100%">
					<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenar_AntenaGUL.a_sitio"></selectize>
				</label>
			</div>
		</div>
		<select class="form-control" ng-model="ctrl.almacenar_AntenaGUL.estado">
			<option value="">Seleccionar estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select>
	</div><br>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea almacenar el elemento..." ng-model="ctrl.almacenar_AntenaGUL.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.almacenarAntenaGUL(ctrl.almacenar_AntenaGUL)" ng-disabled="!ctrl.almacenar_AntenaGUL.comentario || (ctrl.almacenar_AntenaGUL.tipo_almacen == 1 && !ctrl.almacenar_AntenaGUL.almacen) || (ctrl.almacenar_AntenaGUL.tipo_almacen == 2 && !ctrl.almacenar_AntenaGUL.a_sitio)">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamientoGUL()">Cancelar</button>
</div>

<!-- ANTENAS IDEN -->

<div id="nuevaAntenaIDEN" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nueva antena iDEN</h3>

	<form class="verifica_factor_uso">
		<div id="alerta_de_torre" class="alert alert-danger text-center alerta_de_torre">
			<i class="fa fa-exclamation-triangle fa-2x"></i>Torre bloqueada para nuevas instalaciones!
		</div>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Número de serie</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.numero_serie">
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.marca" ng-change="ctrl.listarModelosIDEN(ctrl.nuevaAntenaIDEN.marca)">
							<option value="">Seleccione una marca</option>
							<option ng-repeat="marca in ctrl.marcasIDEN" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.modelo" ng-change="ctrl.listarVersionesMicroondas(ctrl.nuevaAntenaIDEN.marca,ctrl.nuevaAntenaIDEN.modelo)">
							<option value="">Seleccione un modelo</option>
							<option ng-repeat="modelo in ctrl.modelosIDEN" value="{{modelo.modelo}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Sector</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.sector">
							<option value="">Seleccione un sector</option>
							<?php for($i=1; $i < 11; $i++):?>
								<option value="<?php echo $i?>"><?php echo $i?></option>
							<?php endfor?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Altura</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.altura">
					</td>
				</tr>
				<tr>
					<td class="info">Azimuth</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.azimuth">
					</td>
				</tr>
				<tr>
					<td class="info">Tilt Eléctrico</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.tilt_electrico">
					</td>
				</tr>
				<tr>
					<td class="info">Tilt Mecánico</td>
					<td class="default">
						<input type="number" step="any" class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.tilt_mecanico">
					</td>
				</tr>
				<tr>
					<td class="info">Cantidad</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.cantidad">
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarIDEN()" ng-disabled="!ctrl.nuevaAntenaIDEN.numero_serie || !ctrl.nuevaAntenaIDEN.sector || !ctrl.nuevaAntenaIDEN.altura || !ctrl.nuevaAntenaIDEN.azimuth || !ctrl.nuevaAntenaIDEN.tilt_electrico || !ctrl.nuevaAntenaIDEN.cantidad">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionIDEN()">Cancelar</button>
	</form>
</div>

<div id="editarAntenaIDEN" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Editar antena iDEN</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Número de serie</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.numero_serie">
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.tipo_antena_iden.marca" ng-change="ctrl.listarModelosIDEN(ctrl.editarAntenaIDEN.tipo_antena_iden.marca)">
							<option value="">Seleccione una marca</option>
							<option ng-repeat="marca in ctrl.marcasIDEN" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.tipo_antena_iden.modelo" ng-change="ctrl.listarVersionesMicroondas(ctrl.editarAntenaIDEN.tipo_antena_iden.marca,ctrl.editarAntenaIDEN.tipo_antena_iden.modelo)">
							<option value="">Seleccione un modelo</option>
							<option ng-repeat="modelo in ctrl.modelosIDEN" value="{{modelo.modelo}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Sector</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.sector">
							<option value="">Seleccione un sector</option>
							<?php for($i=1; $i < 11; $i++):?>
								<option value="<?php echo $i?>"><?php echo $i?></option>
							<?php endfor?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Altura</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.altura">
					</td>
				</tr>
				<tr>
					<td class="info">Azimuth</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.azimuth">
					</td>
				</tr>
				<tr>
					<td class="info">Tilt Eléctrico</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.tilt_electrico">
					</td>
				</tr>
				<tr>
					<td class="info">Tilt Mecánico</td>
					<td class="default">
						<input type="number" step="any" class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.tilt_mecanico">
					</td>
				</tr>
				<tr>
					<td class="info">Cantidad</td>
					<td class="default">
						<input type="number" step="any" min="1" class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.cantidad">
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.actualizarIDEN(ctrl.editarAntenaIDEN.id)">Guardar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionIDEN()">Cancelar</button>
	</form>
</div>

<div id="eliminarAntenaIDEN" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar antena iDEN</h3>

	<p>
		¿Está seguro de querer eliminar la antena microondas con características
	</p>
	<p>
		Marca: <b>{{ctrl.eliminarAntenaIDEN.tipo_antena_iden.marca}}</b>, Modelo: <b>{{ctrl.eliminarAntenaIDEN.tipo_antena_iden.modelo}}</b>, Versión: <b>{{ctrl.eliminarAntenaIDEN.tipo_antena_iden.version}}</b>
	</p>
	<p>
		y serie: <b>{{ctrl.eliminarAntenaIDEN.numero_serie}}</b>?
	</p>
	
	<textarea class="form-control" ng-model="ctrl.eliminarAntenaIDEN.comentario" style="width:100%;resize: none;" rows="3" placeholder="Por favor indique por qué está eliminando este elemento..."></textarea>
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarIDEN()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionIDEN()">Cancelar</button>
</div>

<div id="almacenarAntenaIDEN" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Almacenar antena iDEN</h3>

	<p>Elegir destino:</p>

	<div style="max-width:400px">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a data-toggle="tab" href="#almacenIDEN" ng-click="ctrl.IDENOpcionAlmacen()">Almacén</a></li>
			<li><a data-toggle="tab" href="#sitioIDEN" ng-click="ctrl.IDENOpcionSitio()">Sitio</a></li>
		</ul>
		<div class="tab-content">
			<div id="almacenIDEN" class="tab-pane fade in active">
				<select class="form-control" ng-model="ctrl.almacenar_AntenaIDEN.almacen">
					<option value="">Elegir almacén</option>
					<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
				</select>
			</div>
			<div id="sitioIDEN" class="tab-pane fade">
				<label style="width:100%">
					<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenar_AntenaIDEN.a_sitio"></selectize>
				</label>
			</div>
		</div>
		<select class="form-control" ng-model="ctrl.almacenar_AntenaIDEN.estado">
			<option value="">Seleccionar estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select>
	</div><br>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea almacenar el elemento..." ng-model="ctrl.almacenar_AntenaIDEN.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.almacenarAntenaIDEN(ctrl.almacenar_AntenaIDEN)" ng-disabled="!ctrl.almacenar_AntenaIDEN.comentario || (ctrl.almacenar_AntenaIDEN.tipo_almacen == 1 && !ctrl.almacenar_AntenaIDEN.almacen) || (ctrl.almacenar_AntenaIDEN.tipo_almacen == 2 && !ctrl.almacenar_AntenaIDEN.a_sitio)">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamientoIDEN()">Cancelar</button>
</div>

<!-- ANTENAS IN BUILDING -->

<div id="nuevaAntenaInBuilding" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nueva antena In Building</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaInBuilding.tipo_antena_in_building.id">
							<option value="">Elegir modelo</option>
							<option ng-repeat="modelo in ctrl.modelos_in_building" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Cantidad</td>
					<td class="default">
						<input class="form-control input-sm" type="number" step="any" min="0" ng-model="ctrl.nuevaAntenaInBuilding.cantidad"/>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarInBuilding(ctrl.nuevaAntenaInBuilding)" ng-disabled="!ctrl.nuevaAntenaInBuilding.tipo_antena_in_building.id || !ctrl.nuevaAntenaInBuilding.cantidad">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionInBuilding()">Cancelar</button>
	</form>
</div>

<div id="editarAntenaInBuilding" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Editar antena In Building</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaInBuilding.tipo_antena_in_building.id">
							<option value="">Elegir modelo</option>
							<option ng-repeat="modelo in ctrl.modelos_in_building" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Cantidad</td>
					<td class="default">
						<input class="form-control input-sm" type="number" step="any" min="0" ng-model="ctrl.editarAntenaInBuilding.cantidad" />
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.actualizarInBuilding(ctrl.editarAntenaInBuilding.id)" ng-disabled="!ctrl.editarAntenaInBuilding.tipo_antena_in_building.id || !ctrl.editarAntenaInBuilding.cantidad">Guardar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionInBuilding()">Cancelar</button>
	</form>
</div>

<div id="eliminarAntenaInBuilding" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar antena In Building</h3>

	<p>¿Está seguro de querer eliminar la antena In Building?</p>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_AntenaInBuilding.comentario"></textarea>
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarAntenaInBuilding()" ng-disabled="!ctrl.eliminar_AntenaInBuilding.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionInBuilding()">Cancelar</button>
</div>

<div class="modal fade" id="importarAntenaGUL" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Importar Antena GUL</h4>
			</div>
			<div class="modal-body modal-format">
				<div style="height:180px;overflow-y:auto;">
					<table class="table table-condensed table-hover table-responsive">
						<thead>
							<tr>
								<th>#</th>
								<th>Almacén</th>
								<th>Estado</th>
								<th>Marca</th>
								<th>Modelo</th>
								<th>Número de Serie</th>
							</tr>
							<tr>
								<th></th>
								<th><input class="form-control input-sm" type="text" name=""></th>
								<th><input class="form-control input-sm" type="text" name=""></th>
								<th><input class="form-control input-sm" type="text" name=""></th>
								<th><input class="form-control input-sm" type="text" name=""></th>
								<th><input class="form-control input-sm" type="text" name=""></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="antena in ctrl.antenas_gul_almacenadas[0].info">
								<td><input type="radio" name="optradio" ng-model="antena.check"></td>
								<td>{{antena.almacen.almacen}}</td>
								<td>{{antena.estado.estado}}</td>
								<td>{{antena.tipo_antena_gul.marca}}</td>
								<td>{{antena.tipo_antena_gul.modelo}}</td>
								<td>{{antena.numero_serie}}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3"><label>Sector</label></div>
					<div class="col-md-3">
						<select class="form-control" ng-model="ctrl.importarAntenaGUL.sector">
							<option value="">Elegir sector</option>
							<?php for ($i = 1; $i <= 10; $i++): ?>
								<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php endfor;?>
						</select>
					</div>
					<div class="col-md-3"><label>Altura</label></div>
					<div class="col-md-3">
						<input class="form-control" type="number" ng-model="ctrl.importarAntenaGUL.altura">
					</div>
				</div>
				<div class="row">
					<div class="col-md-3"><label>Azimuth</label></div>
					<div class="col-md-3">
						<input class="form-control" type="number" ng-model="ctrl.importarAntenaGUL.azimuth">
					</div>
					<div class="col-md-3"><label>Tilt Mecánico</label></div>
					<div class="col-md-3">
						<input class="form-control" type="number" ng-model="ctrl.importarAntenaGUL.tilt_mecanico">
					</div>
				</div>
				<div class="row">
					<div class="col-md-3"><label>Tilt Eléctrico</label></div>
					<div class="col-md-3">
						<input class="form-control" type="number" ng-model="ctrl.importarAntenaGUL.tilt_electrico">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button class="btn btn-primary" ng-click="ctrl.finImportarAntengaGUL(ctrl.importarAntenaGUL, ctrl.antenas_gul_almacenadas[0].info)">Aceptar</button>
			</div>
		</div>
	</div>
</div>