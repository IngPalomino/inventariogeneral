<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="tanque in ctrl.tanques_combustible">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Tanque de combustible {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr>
								<td class="info">Tipo de tanque</td>
								<td class="default">
									<span ng-show="!tanque.editar" ng-if="tanque.tipo_tanque_combustible == 1">Tanque Interno</span>
									<span ng-show="!tanque.editar" ng-if="tanque.tipo_tanque_combustible == 2">Tanque Externo</span>
									<select ng-hide="!tanque.editar" class="form-control" ng-model="tanque.tipo_tanque_combustible">
										<option value="">Elegir tipo</option>
										<option value="1">Tanque Interno</option>
										<option value="2">Tanque Externo</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="info">Fecha de instalación</td>
								<td class="default">
									<span ng-show="!tanque.editar">{{tanque.fecha_instalacion | date:"dd/MM/yyyy"}}</span>
									<input ng-hide="!tanque.editar" class="form-control" type="date" ng-model="tanque.fecha_instalacion"/>
								</td>
							</tr>
							<tr>
								<td class="info">Capacidad (Gl)</td>
								<td class="default">
									<span ng-show="!tanque.editar">{{tanque.capacidad}}</span>
									<input ng-hide="!tanque.editar" class="form-control" type="number" step="any" min="0" ng-model="tanque.capacidad"/>
								</td>
							</tr>
							<tr>
								<td class="info">Consumo (Gl/h)</td>
								<td class="default">
									<span ng-show="!tanque.editar">{{tanque.consumo}}</span>
									<input ng-hide="!tanque.editar" class="form-control" type="number" step="any" min="0" ng-model="tanque.consumo"/>
								</td>
							</tr>
							<tr>
								<td class="info">Autonomía (h)</td>
								<td class="default">
									<span>{{tanque.autonomia() | number:1}}</span>
								</td>
							</tr>
							<tr>
								<td style="font-size:8.5pt;list-style:none;text-align:right;" colspan="2">
									<span ng-if="!tanque.logs_tanques_combustible.length || tanque.logs_tanques_combustible[0].evento.tipo != 'INSERT'">Creado: {{tanque.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>
									<span ng-if="tanque.logs_tanques_combustible[0].evento.tipo == 'INSERT'">Creado: {{tanque.logs_tanques_combustible[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{tanque.logs_tanques_combustible[0].usuario.nombres}} {{tanque.logs_tanques_combustible[0].usuario.apellido_paterno}}</span>
									<br>
									<span ng-if="tanque.logs_tanques_combustible[1].evento.tipo == 'UPDATE'">Actualizado: {{tanque.logs_tanques_combustible[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{tanque.logs_tanques_combustible[1].usuario.nombres}} {{tanque.logs_tanques_combustible[1].usuario.apellido_paterno}}</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editarTanque(tanque)" ng-show="!tanque.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
					<button class="btn btn-sm btn-success" ng-click="ctrl.grabarTanque(tanque)" ng-hide="!tanque.editar">Grabar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarTanque(tanque)" ng-hide="!tanque.editar">Cancelar</button>
					<button class="btn btn-sm btn-info" ng-click="ctrl.almacenandoTanque(tanque)" ng-show="!tanque.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Almacenar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoTanque(tanque)" ng-show="!tanque.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.tanques_combustible.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:15px;" ng-click="ctrl.nuevo_tanque()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir Tanque de combustible</button>
		</div>
	</div>
</div>

<div id="nuevoTanqueCombustible" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nuevo tanque de combustible</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Tipo de tanque</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevoTanqueCombustible.tipo_tanque_combustible">
							<option value="">Elegir tipo</option>
							<option value="1">Tanque Interno</option>
							<option value="2">Tanque Externo</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Capacidad</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevoTanqueCombustible.capacidad">
							<div class="input-group-addon">gl</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Consumo</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevoTanqueCombustible.consumo">
							<div class="input-group-addon">gl/hr</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Fecha de instalación</td>
					<td class="default">
						<input type="date" class="form-control input-sm" ng-model="ctrl.nuevoTanqueCombustible.fecha_instalacion" />
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarTanqueCombustible(ctrl.nuevoTanqueCombustible)" ng-disabled="!ctrl.nuevoTanqueCombustible.tipo_tanque_combustible || !ctrl.nuevoTanqueCombustible.capacidad || !ctrl.nuevoTanqueCombustible.consumo || !ctrl.nuevoTanqueCombustible.fecha_instalacion">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionTanque()">Cancelar</button>
	</form>
</div>

<div id="eliminarTanqueCombustible" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar tanque de combustible</h3>

	<p>¿Está seguro de eliminar el tanque de combustible?</p>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminarTanqueCombustible.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.eliminarTanque()" ng-disabled="!ctrl.eliminarTanqueCombustible.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>

<div id="almacenarTanque" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Almacenar Tanque de Combustible</h3>

	<p>Elegir destino:</p>

	<div style="max-width:400px">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a data-toggle="tab" href="#almacen" ng-click="ctrl.opcionAlmacen()">Almacén</a></li>
			<li><a data-toggle="tab" href="#sitio" ng-click="ctrl.opcionSitio()">Sitio</a></li>
		</ul>
		<div class="tab-content">
			<div id="almacen" class="tab-pane fade in active">
				<select class="form-control" ng-model="ctrl.almacenar_Tanque.almacen">
					<option value="">Elegir almacén</option>
					<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
				</select>
			</div>
			<div id="sitio" class="tab-pane fade">
				<label style="width:100%">
					<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenar_Tanque.a_sitio"></selectize>
				</label>
			</div>
		</div>
		<select class="form-control" ng-model="ctrl.almacenar_Tanque.estado">
			<option value="">Seleccionar estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select>
	</div><br>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea almacenar el elemento..." ng-model="ctrl.almacenar_Tanque.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.almacenarTanque(ctrl.almacenar_Tanque)" ng-disabled="!ctrl.almacenar_Tanque.estado || !ctrl.almacenar_Tanque.comentario || (ctrl.almacenar_Tanque.tipo_almacen == 1 && !ctrl.almacenar_Tanque.almacen) || (ctrl.almacenar_Tanque.tipo_almacen == 2 && !ctrl.almacenar_Tanque.a_sitio)">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamiento()">Cancelar</button>
</div>