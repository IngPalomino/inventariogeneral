<!--<p>
	<?php  print_r($session);?>
</p>-->

<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			
			ref.error.error = false;
			ref.error.logIn = "";
			ref.error.crear = "";
			ref.error.actualizar = "";
            // ref.estax = false;
			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.resumen = <?php echo json_encode($resumen);?>; console.log(ref.resumen);
			ref.resumen.resumen01.total = function(){
				var total = 0;

				this.forEach(function(tipo){
					total += Number(tipo.cantidad);
				});

				return total;
			};
			ref.resumen.resumen02.total = function(){
				var total = 0;

				this.forEach(function(tipo){
					total += Number(tipo.cantidad);
				});

				return total;
			};

			var limit = 50,
				offset = 0;
			ref.sitioNuevo = {
								codigo:"",
								estado:"1",								
								nombre:"",
								zona_peru:null,
								latitud:0,
								longitud:0,
								altitud:0,
								direccion:"",
								in_building:false,
								coubicado:false,
								agregador:false,
								dorsal:false,
								greenfield_rooftop:false,
								altura_edificacion:0,
								ubicacion_equipos:0,
								tipo_torre:"",
								altura_torre:0,
								anio_construccion:(new Date()).getFullYear(),
								camuflaje:"",
								prioridad:"4",
								fecha_inicio_contrato:new Date(),
								fecha_fin_contrato:new Date(),
								coubicador:"",								
								nombre_sitio_coubicador:"N/A",
								codigo_torrera:"N/A",
								operador_coubicante:"1",
								nombre_sitio_coubicante:"N/A",
								fecha_estado_on_air:new Date(),
								proyecto:"",
								contrata_constructora:"",
								tipo_solucion:"",
								proveedor_mantenimiento:"",
								consideracion_acceso:"",
								acceso_libre_24h:false,
								nivel_riesgo:"1",
								consideraciones:"",
								oym_zonal:[
									{oym_zonal:null},
									{oym_zonal:null}
								],
								crm:"",
								supervisor:"",
								tecnologias:[]
								};
								/*factor_uso_torre:0,*/

			ref.agregar_tecnologia = function(){
				
                
				ref.sitioNuevo.tecnologias.push({
					tecnologia: null,
					codigo:"",
										
					fecha_servicio: new Date(),
					servicios: 0
				});
			}

			ref.quitar_tecnologia = function(index){
				ref.sitioNuevo.tecnologias.splice(index,1);
			}

			/*ref.mostrar_tecnologia = function(tecno,index){
				var ngif = true;

				ref.sitioNuevo.tecnologias.forEach(function(t,ind,obj){
					if( (t.tecnologia == tecno) && (index != ind) )
					{
						ngif = false;
					}
				});

				return ngif;
			}*/

			ref.disabled = function(){
				var dis = false;
				 var op =  $("[name=estado]").val();
			     

				if(ref.sitioNuevo.codigo == "")
				{
					dis = true;
				}
				if(ref.sitioNuevo.nombre == "")
				{
					dis = true;
				}
				if(ref.sitioNuevo.zona_peru == null)
				{
					dis = true;
				}
                if(ref.sitioNuevo.zona_peru == "")
				 {
				 	dis = true;
				 }
                if(ref.sitioNuevo.coubicador == "")
				 {
				 	dis = true;
				 }
				 if(ref.sitioNuevo.proyecto == "")
				 {
				 	dis = true;
				 }


				// if(ref.sitioNuevo.estado_selecciona == 1)
				//  {
				//  	$("[name=fecha_estado_on_air]").attr('disabled', 'disabled');
				//  	dis = true;

				//  }

				 
				// if(ref.sitioNuevo.supervisor == "")
				// {
				// 	dis = true;
				// }

//||ctrl.sitioNuevo.zona_peru==''||ctrl.sitioNuevo.coubicador==''||ctrl.sitioNuevo.proyecto==''||










				ref.sitioNuevo.tecnologias.forEach(function(t,indx,objx){
					ref.sitioNuevo.tecnologias.forEach(function(tt,indy,objy){
						if( (t.tecnologia == tt.tecnologia) && (indx != indy) )
						{
							dis = true;
						}
					});
				});

				return dis;
			};

			ref.sitioEliminar = {};

			ref.departamentos = [];
			ref.provincias = [];
			ref.distritos = [];

			ref.ubicaciones = [];
			ref.tipos_torre = [];
			ref.camuflajes = [];
			ref.coubicadores = [];
			ref.operadores_coubicante = [];
			ref.proyectos = [];
			ref.constructoras = [];
			ref.soluciones = [];
			ref.proveedores_mantenimiento = [];

			ref.info = {
				index: 1,
				mostrarInfo: function(index){
					this.index = index;
				}
			};

			selectores = function(){
				$http({
					url:"http://10.30.17.6/portal/index.php/tickets/selectores_correctivos",
					method:"GET"
				}).success(function(data){
					if(data.zonales)
					{
						ref.zonales = data.zonales;
					}
				}).error(function(err){

				});
			}

			tecnologias = function(){
				$http({
					url:"<?php echo base_url("index.php/status_site/tecnologias");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tecnologias = data;
					}
				}).error(function(err){

				});
			}

			listar_zonas = function(){
				$http({
					url:"<?php echo base_url("index.php/zonas/listar/departamentos");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.departamentos = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			ref.listar_provincias = function(departamento){
				ref.provincias = [];
				ref.distritos = [];
				ref.provincia = "";
				ref.sitioNuevo.zona_peru = null;
				$http({
					url:"<?php echo base_url("index.php/zonas/listar/provincias");?>"+"/"+encodeURIComponent(departamento),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.provincias = data
						ref.distritos = [];
					}
				}).error(function(err){

				});
			}

			ref.listar_distritos = function(departamento,provincia){
				ref.distritos = [];
				ref.sitioNuevo.zona_peru = null;
				$http({
					url:"<?php echo base_url("index.php/zonas/listar/distritos");?>"+"/"+encodeURIComponent(departamento)+"/"+encodeURIComponent(provincia),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.distritos = data
					}
				}).error(function(err){

				});
			}

			listar_ubicaciones = function(){
				$http({
					url:"<?php echo base_url("index.php/ubicacion_equipos/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.ubicaciones = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_tipo_torre = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_torre/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipos_torre = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_camuflajes = function(){
				$http({
					url:"<?php echo base_url("index.php/camuflaje/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.camuflajes = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_coubicadores = function(){
				$http({
					url:"<?php echo base_url("index.php/coubicador/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.coubicadores = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_coubicantes = function(){
				$http({
					url:"<?php echo base_url("index.php/coubicante/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.operadores_coubicante = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_proyectos = function(){
				$http({
					url:"<?php echo base_url("index.php/proyecto/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.proyectos = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_contructoras = function(){
				$http({
					url:"<?php echo base_url("index.php/contrata_constructora/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.constructoras = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_soluciones = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_solucion/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.soluciones = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_proveedores_mantenimiento = function(){
				$http({
					url:"<?php echo base_url("index.php/proveedor_mantenimiento/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.proveedores_mantenimiento = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			ref.crear = function(){
				
				var  estado = $("#estado").val();
				ref.sitioNuevo.estado = estado;
				ref.sitioNuevo.fecha_estado_on_air = ref.sitioNuevo.fecha_estado_on_air.toISOString();
				ref.sitioNuevo.fecha_inicio_contrato = ref.sitioNuevo.fecha_inicio_contrato.toISOString();
				ref.sitioNuevo.fecha_fin_contrato = ref.sitioNuevo.fecha_fin_contrato.toISOString();

				ref.sitioNuevo.tecnologias.forEach(function(t,ind,obj){
					obj[ind].fecha_servicio = ((t.fecha_servicio != null)? t.fecha_servicio.toISOString() : null );
				});

				var data = "data="+JSON.stringify(ref.sitioNuevo);

				ref.sitioNuevo.fecha_estado_on_air = new Date(Date.parse(ref.sitioNuevo.fecha_estado_on_air));

				ref.sitioNuevo.fecha_inicio_contrato = new Date(Date.parse(ref.sitioNuevo.fecha_inicio_contrato));
				ref.sitioNuevo.fecha_fin_contrato = new Date(Date.parse(ref.sitioNuevo.fecha_fin_contrato));

				ref.sitioNuevo.tecnologias.forEach(function(t,ind,obj){
					obj[ind].fecha_servicio = ((t.fecha_servicio != null)? new Date(Date.parse(t.fecha_servicio)) : null );
				});

                $(".procesand").css("display","block");
                $(".bloquea_one").attr('disabled', 'disabled');
				$http({
					url:"<?php echo base_url("index.php/status_site/sitio");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
                   $(".procesand").css("display","none");  
					
					if(resp.resp)
					{
						window.location.assign("<?php echo base_url("ver/sitios/torre");?>/"+resp.id);                           
                      
					}
					else
					{


					}
				})
				.error(function(err){

				});
			}

			ref.cancelarCreacion = function(){
				ref.sitioNuevo = {};
				$("#crearSitio").bPopup().close();

			}

			ref.eliminando = function(sitio){
				ref.sitioEliminar = sitio;
				$("#eliminarSitio").bPopup();
			}

			ref.eliminar = function(){
				$http({
					url:"<?php echo base_url("index.php/sitios/sitio");?>"+"/"+ref.sitioEliminar.id,
					method:"DELETE"
				}).success(function(data){
					listar();
					ref.sitioEliminar = {};
					$("#eliminarSitio").bPopup().close();
				}).error(function(err){

				});
			}

			ref.cancelarEliminacion = function(){
				ref.sitioEliminar = {};
				$("#eliminarSitio").bPopup().close();
			}

			ref.listener_coubicador = function(id){
				if(id == 1)
				{
					ref.sitioNuevo.nombre_sitio_coubicador = "N/A";
					ref.sitioNuevo.codigo_torrera = "N/A";
				}
			}

			ref.listener_coubicante = function(id){
				if(id == 1)
				{
					ref.sitioNuevo.nombre_sitio_coubicante = "N/A";
				}
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){

				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};
			
			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};

        //     ref.estadox = function(){             	 
        //      	 var op =  $("[name=estado]").val();
			     // if(op == 1){
			     // 	return true;
			     // }else{
			     // 	return false;
			     // }			   	 
        //     }



            ref.verifica_codigo = function(){
			    var codigo = $("[name=codigo]").val();
			    var data = "codigo="+codigo;
                $http({
					url:"<?php echo base_url("index.php/status_site/busca_codigo_on_air");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
                 console.log(resp);
	                 if(resp.length>0){
	                 	$(".codigo-site-new").css("border","solid 2px red").focus();
                        $(".redtext").css("display","block");
	                 }else{
                       $(".codigo-site-new").css("border","1px solid #ccc");
                       $(".redtext").css("display","none");
	                 }
                  
				})
				.error(function(err){
				});
			};        

          
      

            habilitafecha = function(){	

			   var op =  $("[name=estado]").val();
			   if(op == 1){			   	 
			   //	$("#xfecha").css("display","none");
			   	//$("[name=fecha_estado_on_air]").reset();
			   	//$("[name=fecha_estado_on_air]").css("display","block");
			   	$("[name=fecha_estado_on_air]").attr('disabled', 'disabled');		 
			   }else{
			   //	alert(op);
			    // $("#xfecha").css("display","none");
			   // ref.sitioNuevo.fecha_estado_on_air = ref.sitioNuevo.fecha_estado_on_air.toISOString();
                 //$("[name=fecha_estado_on_air]").css("display","block");
                 $("[name=fecha_estado_on_air]").removeAttr("disabled");
			   }

			  };
			

			selectores();
			tecnologias();
			listar_zonas();
			listar_ubicaciones();
			listar_tipo_torre();
			listar_camuflajes();
			listar_coubicadores();
			listar_coubicantes();
			listar_proyectos();
			listar_contructoras();
			listar_soluciones();
			listar_proveedores_mantenimiento();
		}]);
   
       $('.nav.navbar-nav.tabscreado li').click(function(){
      	 $(".nav.navbar-nav.tabscreado li").css("background-color","transparent");
         $(this).css("background-color","#fff");
       });  

   
   // tabs();
</script>




</body>
</html>