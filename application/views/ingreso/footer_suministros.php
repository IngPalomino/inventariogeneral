<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargando = false;

			ref.suministros = [];
			ref.info_sitio = {};

			ref.nuevoSuministro = {};
			ref.eliminar_Suministro = {};

			ref.concesionarias_suministro = [];
			ref.conexiones_suministro = [];
			ref.sistemas_suministro = [];
			ref.opcion_tarifaria = [];

			listarSuministros = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("index.php/suministros/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						data.forEach(function(suministro,ind,objeto){
							objeto[ind]['potencia_contratada'] = Number(suministro.potencia_contratada);
							objeto[ind]['nivel_tension'] = Number(suministro.nivel_tension);
							
							try{ objeto[ind].created_at = Date.parse(objeto[ind].created_at); } catch(err){ objeto[ind].created_at = null; }
							
							objeto[ind].logs_suministros.forEach(function(modificacion,indx,objetox){
								try{ objetox[indx].created_at = Date.parse(objetox[indx].created_at); } catch(err){ objetox[indx].created_at = null; }
							});
						});
						ref.suministros = data;
						ref.cargando = false;
					}
					else
					{
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			listarConcesionariasSuministro = function(){
				$http({
					url:"<?php echo base_url("index.php/concesionaria_suministro/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.concesionarias_suministro = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarConexionesSuministro = function(){
				$http({
					url:"<?php echo base_url("index.php/conexion_suministro/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.conexiones_suministro = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarSistemasSuministro = function(){
				$http({
					url:"<?php echo base_url("index.php/sistema_suministro/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sistemas_suministro = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarTipoAlimentacion = function(){
				$http({
					url:"<?php echo base_url("tipo_alimentacion/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipo_alimentacion = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarOpcionTarifaria = function(){
				$http({
					url:"<?php echo base_url("opcion_tarifaria/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.opcion_tarifaria = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			infoSitio = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("index.php/status_site/editar/");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.info_sitio = data[0];
						ref.cargando = false;
					}
					else
					{
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			ref.nuevo_suministro = function(){
				$("#nuevoSuministro").bPopup();
			}

			ref.cancelarCreacionSuministro = function(){
				$("#nuevoSuministro").bPopup().close();
				ref.nuevoSuministro = {};
			}

			ref.agregarSuministro = function(suministro){
				var data = "",
					campos = Object.keys(suministro),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado")
					{
						if(contador > 0)
						{
							data += "&";
						}

						if(typeof suministro[campo] != "object")
						{
							data += campo+"="+suministro[campo];
						}
						else
						{
							data += campo+"="+suministro[campo]['id'];
						}

						contador++;
					}
				});
				/*console.log(data);*/

				$http({
					url:"<?php echo base_url("index.php/suministros/suministro/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevoSuministro").bPopup().close();
					listarSuministros();
				}).error(function(err){

				});
			}

			ref.eliminandoSuministro = function(info){
				ref.eliminar_Suministro = info;
				$("#eliminarSuministro").bPopup();
			}

			ref.cancelarEliminacion = function(){
				$("#eliminarSuministro").bPopup().close();
				ref.eliminar_Suministro = {};
			}

			ref.eliminarSuministro = function(){
				var data = "comentario="+ref.eliminar_Suministro.comentario;

				$http({
					url:"<?php echo base_url("index.php/suministros/suministro");?>"+"/"+ref.eliminar_Suministro.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.suministros = [];
						listarSuministros();
						ref.eliminarSuministro = {};
						$('#eliminarSuministro').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.editarSuministro = function(suministro){
				suministro.editar = true;
			}

			ref.grabarSuministro = function(suministro){
				borrarPropiedades(suministro,"logs_suministros");
				
				var data = "",
					campos = Object.keys(suministro),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado")
					{
						if(contador > 0)
						{
							data += "&";
						}

						if(typeof suministro[campo] != "object")
						{
							data += campo+"="+suministro[campo];
						}
						else
						{
							data += campo+"="+suministro[campo]['id'];
						}

						contador++;
					}
				});

				$http({
					url:"<?php echo base_url("index.php/suministros/suministro");?>/"+suministro.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					suministro.editar = false;
					listarSuministros();
				}).error(function(err){

				});
			}

			ref.cancelarSuministro = function(suministro){
				suministro.editar = false;
				listarSuministros();
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}

			listarSuministros();
			listarConcesionariasSuministro();
			listarConexionesSuministro();
			listarSistemasSuministro();
			listarTipoAlimentacion();
			listarOpcionTarifaria();
			infoSitio();
		}]);
</script>
</body>
</html>