<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Tarjetas
				</div>
				<div class="panel-body" style="max-width:1200px;max-height:300px;overflow:scroll;">
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargandoTarjetas"></span>
					</div>
					<table class="table table-striped table-condensed" style="width:2200px;" ng-if="!ctrl.cargandoTarjetas && ctrl.tarjetas.length">
						<thead>
							<tr>
								<th><input type="checkbox" ng-click="ctrl.selectAllTarjetas()" ng-model="ctrl.isAllSelectedTarjetas"/> #</th>
								<th>Board Type</th>
								<th>NE Type</th>
								<th>Slot ID</th>
								<th>Hardware Version</th>
								<th>NE ID</th>
								<th>Board Bar Code</th>
								<th>Board BOM Item</th>
								<th>Description</th>
								<th>Manufacture Date</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="tarjeta in ctrl.tarjetas">
								<td><input type="checkbox" ng-model="tarjeta.check" ng-change="ctrl.tarjetaSeleccionada()"/></td>
								<td>{{tarjeta.board_type}}</td>
								<td>{{tarjeta.ne_type}}</td>
								<td>{{tarjeta.slot_id}}</td>
								<td>{{tarjeta.hardware_version}}</td>
								<td>{{tarjeta.idu.ne_id}}</td>
								<td>{{tarjeta.board_bar_code}}</td>
								<td>{{tarjeta.board_bom_item}}</td>
								<td style="white-space:nowrap;">{{tarjeta.description}}</td>
								<td>
									<span ng-if="tarjeta.manufacture_date != '0000-00-00'">{{tarjeta.manufacture_date | date:"dd/MM/yyyy"}}</span>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="col-xs-12 ng-cloak">
						<div class="well text-center" ng-if="!ctrl.cargandoTarjetas && !ctrl.tarjetas.length">No hay información</div>
					</div>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-success" ng-click="ctrl.nuevas_tarjetas()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Añadir Tarjetas</button>
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editandoTarjetas(ctrl.tarjetas)" ng-disabled="!ctrl.disabledTarjetas()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar selección</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoTarjetas(ctrl.tarjetas)" ng-disabled="!ctrl.disabledTarjetas()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar selección</button>
				</div>
			</div>
		</div>
		
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					ODUs
				</div>
				<div class="panel-body" style="max-height:300px;overflow-y:scroll;">
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargandoODUs"></span>
					</div>
					<table class="table table-striped table-condensed" ng-if="!ctrl.cargandoODUs && ctrl.odus.length">
						<thead>
							<tr>
								<th><input type="checkbox" ng-click="ctrl.selectAllODUs()" ng-model="ctrl.isAllSelectedODUs"/> #</th>
								<th>NE Type</th>
								<th>Frequency</th>
								<th>Range of Frequency Point</th>
								<th>Produce Time</th>
								<th>Factory Information</th>
								<th>Software Version</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="odu in ctrl.odus">
								<td><input type="checkbox" ng-model="odu.check" ng-change="ctrl.oduSeleccionada()"/></td>
								<td>{{odu.ne_type}}</td>
								<td>{{odu.frequency}} GHz</td>
								<td>{{odu.range_frequency}}</td>
								<td>{{odu.produce_time | date:"dd/MM/yyyy"}}</td>
								<td>{{odu.factory_information}}</td>
								<td>{{odu.software_version}}</td>
							</tr>
						</tbody>
					</table>
					<div class="col-xs-12 ng-cloak">
						<div class="well text-center" ng-if="!ctrl.cargandoODUs && !ctrl.odus.length">No hay información</div>
					</div>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-success" ng-click="ctrl.nuevas_odus()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Añadir ODUs</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoODUs(ctrl.odus)" ng-disabled="!ctrl.disabledODUs()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar selección</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="nuevasTarjetas" class="popup" style="width:1000px;">
	<span class="button b-close" ng-click="ctrl.cancelarCreacionTarjetas()">
		<span>X</span>
	</span>
	
	<h3>Nuevas Tarjetas</h3>
	
	<form>
		<div id="nuevasTarjetasData" style="max-width:900px;height:300px;overflow:scroll;"></div><br>
		<button class="btn btn-success" ng-click="ctrl.agregarTarjetas()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionTarjetas()">Cancelar</button>
	</form>
</div>

<div id="eliminarTarjetas" class="popup">
	<span class="button b-close" ng-click="ctrl.cancelarEliminacionTarjetas()">
		<span>X</span>
	</span>
	
	<h3>Eliminar Tarjetas</h3>
	
	<br>
	<p>¿Está seguro de eliminar las tarjetas seleccionadas?</p>
	
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarTarjetas()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionTarjetas()">Cancelar</button>
</div>

<div id="nuevasODUs" class="popup" style="width:800px;">
	<span class="button b-close" ng-click="ctrl.cancelarCreacionODUs()">
		<span>X</span>
	</span>
	
	<h3>Nuevas ODUs</h3>
	
	<form>
		<div id="nuevasODUsData" style="max-width:800px;height:300px;overflow:scroll;"></div><br>
		<button class="btn btn-success" ng-click="ctrl.agregarODUs()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionODUs()">Cancelar</button>
	</form>
</div>

<div id="eliminarODUs" class="popup">
	<span class="button b-close" ng-click="ctrl.cancelarEliminacionODUs()">
		<span>X</span>
	</span>
	
	<h3>Eliminar ODUs</h3>
	
	<br>
	<p>¿Está seguro de eliminar las ODUs seleccionadas?</p>
	
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarODUs()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionODUs()">Cancelar</button>
</div>