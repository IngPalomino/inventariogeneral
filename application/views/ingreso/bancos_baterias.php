<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="rectif in ctrl.bancos_baterias">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Bancos de Baterías del Rectificador {{rectif.marca}} - {{rectif.modelo}} - Energización: {{rectif.energizacion_controlador_rectif}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<thead>
							<tr>
								<th>ID</th>
								<th>Acciones</th>
								<th>Marca</th>
								<th>Modelo</th>
								<th>Capacidad/Banco</th>
								<th>Fecha de Instalación</th>
								<th>Historial</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat-start="banco in rectif.bancos">
								<td>{{banco.id}}</td>
								<td>
									<button class="btn btn-warning btn-xs" ng-click="ctrl.editarBanco(banco)" ng-show="!banco.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
									<button class="btn btn-success btn-xs" ng-click="ctrl.grabarBanco(banco)" ng-hide="!banco.editar" ng-disabled="banco.disabled()">Grabar</button> 
									<button class="btn btn-danger btn-xs" ng-click="ctrl.cancelarBanco(banco)" ng-hide="!banco.editar">Cancelar</button> 
									<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminandoBanco(banco)" ng-show="!banco.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button> 
									<button class="btn btn-primary btn-xs" ng-click="ctrl.mostrarCeldas(banco)" ng-show="!banco.mostrar">Celdas</button> 
									<button class="btn btn-info btn-xs" ng-click="ctrl.ocultarCeldas(banco)" ng-hide="!banco.mostrar"><i class="fa fa-arrow-up"></i> Celdas</button>
									<button class="btn btn-info btn-xs" ng-click="ctrl.almacenandoBanco(banco)" ng-show="!banco.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Almacenar</button>
								</td>
								<td>
									<span ng-show="!banco.editar">{{banco.tipo_bancos_baterias.marca}}</span>
									<select class="form-control input-sm" ng-model="banco.tipo_bancos_baterias.marca" ng-hide="!banco.editar" ng-change="banco.tipo_bancos_baterias.id = '';">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.tipo_bancos_baterias.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
									</select>
								</td>
								<td>
									<span ng-show="!banco.editar">{{banco.tipo_bancos_baterias.modelo}}</span>
									<select class="form-control input-sm" ng-model="banco.tipo_bancos_baterias.id" ng-hide="!banco.editar">
										<option value="">Elegir modelo</option>
										<option ng-if="banco.tipo_bancos_baterias.marca" ng-repeat="modelo in ctrl.tipo_bancos_baterias.modelos | filter:{marca:banco.tipo_bancos_baterias.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
								<td><span ng-if="banco.tipo_bancos_baterias.id == capacidad.id" ng-repeat="capacidad in ctrl.tipo_bancos_baterias.modelos">{{capacidad.capacidad}}</span> Ah</td>
								<td>
									<span ng-show="!banco.editar">{{banco.fecha_instalacion | date:"dd/MM/yyyy"}}</span>
									<input class="form-control input-sm" type="date" ng-model="banco.fecha_instalacion" ng-hide="!banco.editar"/>
								</td>
								<td style="font-size:8pt">
									<span ng-if="!banco.logs_controlador_rectif_bancos_baterias.length || banco.logs_controlador_rectif_bancos_baterias[0].evento.tipo != 'INSERT'">Creado: {{banco.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>
									<span ng-if="banco.logs_controlador_rectif_bancos_baterias[0].evento.tipo == 'INSERT'">Creado: {{banco.logs_controlador_rectif_bancos_baterias[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{banco.logs_controlador_rectif_bancos_baterias[0].usuario.nombres}} {{banco.logs_controlador_rectif_bancos_baterias[0].usuario.apellido_paterno}}</span>
									<br>
									<span ng-if="banco.logs_controlador_rectif_bancos_baterias[1].evento.tipo == 'UPDATE'">Actualizado: {{banco.logs_controlador_rectif_bancos_baterias[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{banco.logs_controlador_rectif_bancos_baterias[1].usuario.nombres}} {{banco.logs_controlador_rectif_bancos_baterias[1].usuario.apellido_paterno}}</span>
								</td>
							</tr>
							<tr ng-repeat-end ng-hide="!banco.mostrar">
								<td colspan="2"></td>
								<td colspan="5">
									<div class="col-xs-12">
										<div class="panel panel-success">
											<div class="panel-heading text-center">
												Celdas del Banco {{$index + 1}}
											</div>
											<div class="panel-body">
											<table class="table table-condensed table-striped table-primary">
												<thead>
													<tr>
														<th>Capacidad</th>
														<th>Fecha de Instalación</th>
														<th>Número de Serie</th>
														<th>Observación <span bs-popover><span style="color:#326497;" class="fa fa-question-circle" rel="popover" data-trigger="hover" data-container="body" data-toggle="popover" data-content="Agregar observación sólo si una celda presenta una marca o modelo diferente al banco."></span></span></th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="celda in banco.celdas_controlador_rectif_bancos_baterias">
														<td><span ng-if="banco.tipo_bancos_baterias.id == capacidad.id" ng-repeat="capacidad in ctrl.tipo_bancos_baterias.modelos">{{capacidad.capacidad}}</span> Ah</td>
														<td>
															<span ng-show="!banco.editar">{{celda.fecha_instalacion | date:"dd/MM/yyyy"}}</span>
															<input class="form-control input-sm" type="date" ng-model="celda.fecha_instalacion" ng-hide="!banco.editar"/>
														</td>
														<td>
															<span ng-show="!banco.editar">{{celda.numero_serie}}</span>
															<input class="form-control" type="text" ng-model="celda.numero_serie" ng-hide="!banco.editar"/>
														</td>
														<td>
															<span ng-show="!banco.editar">
																<span ng-if="celda.tipo_bancos_baterias == 0">-</span>
																<span ng-if="celda.tipo_bancos_baterias != 0">{{celda.tipo_bancos_baterias.marca}} - {{celda.tipo_bancos_baterias.modelo}}</span>
															</span>
															<button class="btn btn-danger btn-xs" ng-hide="!banco.editar" ng-click="ctrl.observacionCeldas(celda)" ng-show="!celda.observacion" ng-if="!celda.tipo_bancos_baterias.id">Agregar</button>
															<div class="input-group input-group-sm" ng-hide="!banco.editar || (!celda.observacion && !celda.tipo_bancos_baterias.id)">
																<div class="input-group-addon">
																	<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminarObservacionCelda(celda); celda.tipo_bancos_baterias.id = ''; celda.tipo_bancos_baterias.marca = '';">X</button>
																</div>
																<div class="input-group-addon">
																	<select ng-model="celda.tipo_bancos_baterias.marca" ng-change="celda.tipo_bancos_baterias.id = '';">
																		<option value="">Elegir marca</option>
																		<option ng-repeat="marca in ctrl.tipo_bancos_baterias.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
																	</select>
																</div>
																<div class="input-group-addon">
																	<select ng-model="celda.tipo_bancos_baterias.id">
																		<option value="">Elegir modelo</option>
																		<option ng-if="celda.tipo_bancos_baterias.marca" ng-repeat="modelo in ctrl.tipo_bancos_baterias.modelos | filter:{marca:celda.tipo_bancos_baterias.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
																	</select>
																</div>
																
															</div>
														</td>
													</tr>
												</tbody>
											</table>
											</div>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.bancos_baterias.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.rectificadores.length && !ctrl.cargando">
			<div class="well text-center">
				Para añadir Banco de Baterías primero se debe añadir un Rectificador.
			</div>
		</div>
	</div>
	<div class="row" ng-if="ctrl.rectificadores.length">
		<div class="col-xs-12">
			<button class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:15px;" ng-click="ctrl.nuevo_banco()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir Banco(s) de Baterías</button>
		</div>
	</div>
</div>

<div id="nuevoBanco" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Nuevo Banco de Baterías</h3>
	
	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Rectificador</td>
					<td class="default" colspan="3">
						<select class="form-control" ng-model="ctrl.nuevoBanco.controlador_rectif">
							<option value="">Elegir rectificador</option>
							<option ng-repeat="rectificador in ctrl.rectificadores" value="{{rectificador.id}}">Marca: {{rectificador.tipo_controlador_rectif.marca}} - Controlador: {{rectificador.tipo_controlador_rectif.modelo}} - Energización: {{rectificador.energizacion_controlador_rectif.energizacion_controlador_rectif}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoBanco.tipo_bancos_baterias.marca" ng-change="ctrl.nuevoBanco.tipo_bancos_baterias.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_bancos_baterias.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoBanco.tipo_bancos_baterias.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoBanco.tipo_bancos_baterias.marca" ng-repeat="modelo in ctrl.tipo_bancos_baterias.modelos | filter:{marca:ctrl.nuevoBanco.tipo_bancos_baterias.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Capacidad/Banco</td>
					<td class="default">
						<span ng-if="ctrl.nuevoBanco.tipo_bancos_baterias.id == capacidad.id" ng-repeat="capacidad in ctrl.tipo_bancos_baterias.modelos">{{capacidad.capacidad}} Ah</span>
					</td>
					<td class="info">Fecha de Instalación</td>
					<td class="default">
						<input class="form-control" type="text" id="datepicker" size="30" ng-model="ctrl.nuevoBanco.fecha_instalacion"/>
					</td>
				</tr>
				<tr>
					<td class="info">Cantidad de bancos</td>
					<td class="default">
						<form method="post">
							<input class="form-control" type="number" name="cantidad" step="any" min="1" ng-model="ctrl.nuevoBanco.cantidad_bancos"/>
						</form>
					</td>
					<td class="info" colspan="2"></td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarBanco(ctrl.nuevoBanco)" ng-disabled="!ctrl.nuevoBanco.controlador_rectif || !ctrl.nuevoBanco.tipo_bancos_baterias.id || !ctrl.nuevoBanco.cantidad_bancos || !ctrl.nuevoBanco.fecha_instalacion">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionBanco()">Cancelar</button>
	</form>
</div>

<div id="eliminarBanco" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar Banco de Baterías</h3>
	
	<p>¿Está seguro de eliminar el banco de baterías?</p>
	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Banco.comentario"></textarea>
	
	<button class="btn btn-success" ng-click="ctrl.eliminarBanco()" ng-disabled="!ctrl.eliminar_Banco.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>

<div id="almacenarBanco" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Almacenar Banco de Baterías</h3>

	<p>Elegir destino:</p>

	<div style="max-width:400px">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a data-toggle="tab" href="#almacen" ng-click="ctrl.bancoOpcionAlmacen()">Almacén</a></li>
			<li><a data-toggle="tab" href="#sitio" ng-click="ctrl.bancoOpcionSitio()">Sitio</a></li>
		</ul>
		<div class="tab-content">
			<div id="almacen" class="tab-pane fade in active">
				<select class="form-control" ng-model="ctrl.almacenar_Banco.almacen">
					<option value="">Elegir almacén</option>
					<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
				</select>
			</div>
			<div id="sitio" class="tab-pane fade">
				<label style="width:100%">
					<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenar_Banco.a_sitio"></selectize>
				</label>
			</div>
		</div>
		<select class="form-control" ng-model="ctrl.almacenar_Banco.estado">
			<option value="">Seleccionar estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select>
	</div><br>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea almacenar el elemento..." ng-model="ctrl.almacenar_Banco.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.almacenarBanco(ctrl.almacenar_Banco)" ng-disabled="!ctrl.almacenar_Banco.estado || !ctrl.almacenar_Banco.comentario || (ctrl.almacenar_Banco.tipo_almacen == 1 && !ctrl.almacenar_Banco.almacen) || (ctrl.almacenar_Banco.tipo_almacen == 2 && !ctrl.almacenar_Banco.a_sitio)">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamiento()">Cancelar</button>
</div>