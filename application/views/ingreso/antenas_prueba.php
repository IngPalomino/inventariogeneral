<br>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<h4>Microondas</h4>
			<div>
				<button class="btn btn-default btn-xs" onclick="$('#nuevaAntenaMicroondas').bPopup();"><span class="fa fa-plus"></span> Agregar antena</button>
			</div>
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Versión</th>
						<th>Número de serie</th>
						<th>Código de inventario</th>
						<th>Enlace</th>
						<th>Altura</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody class="ng-cloak">
					<tr ng-repeat="antena in ctrl.antenasMicroondas">
						<td>{{antena.tipo_antena_microondas.marca}}</td>
						<td>{{antena.tipo_antena_microondas.modelo}}</td>
						<td>{{antena.tipo_antena_microondas.version}}</td>
						<td>{{antena.numero_serie}}</td>
						<td>{{antena.numero_inventario}}</td>
						<td>{{antena.enlace}}</td>
						<td>{{antena.altura}}</td>
						<td>
							<button class="btn btn-warning btn-xs" ng-click="ctrl.editar_antenaMicroondas(antena.id)">Editar</button>
							<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminandoAntenaMicroondas(antena.id)">Eliminar</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--<div class="col-xs-4">
			<div>
				<h4>Gráfico</h4>
			</div>
		</div>-->
	</div>
	<div class="row">
		<div class="col-xs-12">
			<h4>GUL</h4>
			<div>
				<button class="btn btn-default btn-xs" ng-click="ctrl.popupNuevaAntenaGUL()"><span class="fa fa-plus"></span> Agregar antena</button>
			</div>
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Acciones</th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Sector</th>
						<th>Altura</th>
						<th>Historial</th>
					</tr>
				</thead>
				<tbody class="ng-cloak">
					<tr ng-repeat="antena in ctrl.antenasGUL">
						<td>
							<button class="btn btn-warning btn-xs" ng-click="ctrl.editar_antenaGUL(antena.id)">Editar</button>
							<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminar_antenaGUL(antena.id)">Eliminar</button>
						</td>
						<td>{{antena.tipo_antena_gul.marca}}</td>
						<td>{{antena.tipo_antena_gul.modelo}}</td>
						<td>{{antena.sector}}</td>
						<td>{{antena.altura}} m</td>
						<td>
							<ul style="font-size:8.5pt;list-style:none;">
								<li>Creado: {{antena.created_at}}</li>
								<li>Actualizado: </li>
							</ul>
						</td>
					</tr>
					<tr>
						<td colspan="5"></td>
						<td class="info">#</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--<div class="col-xs-4">
			<div>
				<h4>Gráfico</h4>
			</div>
		</div>-->
	</div>
	<div class="row">
		<div class="col-xs-12">
			<h4>iDEN</h4>
			<div>
				<button class="btn btn-default btn-xs" onclick="$('#nuevaAntenaIDEN').bPopup();"><span class="fa fa-plus"></span> Agregar antena</button>
			</div>
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Acciones</th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Sector</th>
						<th>Altura</th>
						<th>Cantidad</th>
					</tr>
				</thead>
				<tbody class="ng-cloak">
					<tr ng-repeat="antena in ctrl.antenasIDEN">
						<td>
							<button class="btn btn-warning btn-xs" ng-click="ctrl.editar_antenaIDEN(antena.id)">Editar</button>
							<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminar_antenaIDEN(antena.id)">Eliminar</button>
						</td>
						<td>{{antena.tipo_antena_iden.marca}}</td>
						<td>{{antena.tipo_antena_iden.modelo}}</td>
						<td>{{antena.sector}}</td>
						<td>{{antena.altura}} m</td>
						<td></td>
					</tr>
					<tr>
						<td colspan="5"></td>
						<td class="info">#</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--<div class="col-xs-4">
			<h4>Gráfico</h4>
		</div>-->
	</div>
	<div class="row">
		<div class="col-xs-12">
			<h4>Wimax</h4>
			<div>
				<button class="btn btn-default btn-xs"><span class="fa fa-plus"></span> Agregar antena</button>
			</div>
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Versión</th>
						<th>Altura</th>
						<th>Cantidad</th>
					</tr>
				</thead>
				<tbody class="ng-cloak">
					<tr>
						<td colspan="4"></td>
						<td class="info">#</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--<div class="col-xs-4">
			<h4>Gráfico</h4>
		</div>-->
	</div>
</div>

<!-- ANTENAS MICROONDAS -->

<div id="nuevaAntenaMicroondas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nueva antena microondas</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Número de serie</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.nuevaAntenaMicroondas.numero_serie">
					</td>
				</tr>
				<tr>
					<td class="info">Código de inventario</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.nuevaAntenaMicroondas.numero_inventario">
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaMicroondas.marca" ng-change="ctrl.listarModelosMicroondas(ctrl.nuevaAntenaMicroondas.marca)">
							<option ng-repeat="marca in ctrl.marcasMicroondas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaMicroondas.modelo" ng-change="ctrl.listarVersionesMicroondas(ctrl.nuevaAntenaMicroondas.marca,ctrl.nuevaAntenaMicroondas.modelo)">
							<option ng-repeat="modelo in ctrl.modelosMicroondas" value="{{modelo.modelo}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Versión</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaMicroondas.version">
							<option ng-repeat="version in ctrl.versionesMicroondas" value="{{version.version}}">{{version.version}}</option>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarMicroondas()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionMicroondas()">Cancelar</button>
	</form>
</div>

<div id="editarAntenaMicroondas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Editar antena microondas</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Número de serie</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.editarAntenaMicroondas.numero_serie">
					</td>
				</tr>
				<tr>
					<td class="info">Código de inventario</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.editarAntenaMicroondas.numero_inventario">
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaMicroondas.tipo_antena_microondas.marca" ng-change="ctrl.listarModelosMicroondas(ctrl.editarAntenaMicroondas.marca)">
							<option ng-repeat="marca in ctrl.marcasMicroondas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaMicroondas.tipo_antena_microondas.modelo" ng-change="ctrl.listarVersionesMicroondas(ctrl.editarAntenaMicroondas.marca,ctrl.editarAntenaMicroondas.modelo)">
							<option ng-repeat="modelo in ctrl.modelosMicroondas" value="{{modelo.modelo}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Versión</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaMicroondas.tipo_antena_microondas.version">
							<option ng-repeat="version in ctrl.versionesMicroondas" value="{{version.version}}">{{version.version}}</option>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.actualizarMicroondas(ctrl.editarAntenaMicroondas.id)">Guardar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionMicroondas()">Cancelar</button>
	</form>
</div>

<div id="eliminarAntenaMicroondas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar antena microondas</h3>

	<p>
		¿Está seguro de querer eliminar la antena microondas con características
	</p>
	<p>
		Marca: <b>{{ctrl.eliminarAntenaMicroondas.tipo_antena_microondas.marca}}</b>, Modelo: <b>{{ctrl.eliminarAntenaMicroondas.tipo_antena_microondas.modelo}}</b>, Versión: <b>{{ctrl.eliminarAntenaMicroondas.tipo_antena_microondas.version}}</b>
	</p>
	<p>
		y serie: <b>{{ctrl.eliminarAntenaMicroondas.serie}}</b>?
	</p>
	
	<button class="btn btn-success" ng-click="ctrl.eliminarMicroondas()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionMicroondas()">Cancelar</button>
</div>

<!-- ANTENAS GUL -->

<div id="nuevaAntenaGUL" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nueva antena GUL</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.inputs.marcaGUL" ng-change="ctrl.nuevaAntenaGUL.modelo = ''">
							<option value="">Seleccione una marca</option>
							<option ng-repeat="marca in ctrl.marcasModelosGUL.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.tipo_antena_gul" ng-change="ctrl.leerExtrasGUL(ctrl.nuevaAntenaGUL.tipo_antena_gul)">
							<option value="">Seleccione un modelo</option>
							<option ng-repeat="modelo in ctrl.marcasModelosGUL.modelos | filter:ctrl.filtroModelosGUL" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Número de serie</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.numero_serie">
					</td>
				</tr>
				<tr>
					<td class="info">Sector</td>
					<td class="default">
						<div ng-if="!((ctrl.extras.GUL != {}) && (ctrl.extras.GUL.multiplicidad_campos != undefined) && (ctrl.extras.GUL.multiplicidad_campos.sector != undefined))">
							<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.sector">
								<option value="">Seleccione un sector</option>
								<?php for($i=1; $i < 11; $i++):?>
									<option value="<?php echo $i?>"><?php echo $i?></option>
								<?php endfor?>
							</select>
						</div>
						<div ng-if="(ctrl.extras.GUL != {}) && (ctrl.extras.GUL.multiplicidad_campos != undefined) && (ctrl.extras.GUL.multiplicidad_campos.sector != undefined)">
							<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.sector[n]" ng-repeat="n in ctrl.range(0,ctrl.extras.GUL.multiplicidad_campos.sector - 1)">
								<option value="">Seleccione un sector</option>
								<?php for($i=1; $i < 11; $i++):?>
									<option value="<?php echo $i?>"><?php echo $i?></option>
								<?php endfor?>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Altura</td>
					<td class="default">
						<div class="input-group">
							<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.altura">
							<span class="input-group-addon">m</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Azimuth</td>
					<td class="default">
						<div class="input-group">
							<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.azimuth">
							<span class="input-group-addon">°</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Tilt eléctrico</td>
					<td class="default">
						<div class="input-group">
							<input type="number" step="any" class="form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.tilt_electrico">
							<span class="input-group-addon">°</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Tilt mecánico</td>
					<td class="default">
						<div class="input-group">
							<input type="number" step="any" class="form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.tilt_mecanico">
							<span class="input-group-addon">°</span>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarGUL()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionGUL()">Cancelar</button>
	</form>
</div>

<div id="editarAntenaGUL" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Editar antena GUL</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.tipo_antena_gul.marca" ng-change="ctrl.editarAntenaGUL.tipo_antena_gul.id = '';ctrl.inputs.marcaGUL = ctrl.editarAntenaGUL.tipo_antena_gul.marca;">
							<option value="">Seleccione una marca</option>
							<option ng-repeat="marca in ctrl.marcasModelosGUL.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.tipo_antena_gul.id">
							<option value="">Seleccione un modelo</option>
							<option ng-repeat="modelo in ctrl.marcasModelosGUL.modelos | filter:ctrl.filtroModelosGUL" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Sector</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.sector">
							<option value="">Seleccione un sector</option>
							<?php for($i=1; $i < 11; $i++):?>
								<option value="<?php echo $i?>"><?php echo $i?></option>
							<?php endfor?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Número de serie</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.numero_serie">
					</td>
				</tr>
				<tr>
					<td class="info">Altura</td>
					<td class="default">
						<div class="input-group">
							<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.altura">
							<span class="input-group-addon">m</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Azimuth</td>
					<td class="default">
						<div class="input-group">
							<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.azimuth">
							<span class="input-group-addon">°</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Tilt eléctrico</td>
					<td class="default">
						<div class="input-group">
							<input type="number" step="any" class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.tilt_electrico">
							<span class="input-group-addon">°</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Tilt mecánico</td>
					<td class="default">
						<div class="input-group">
							<input type="number" step="any" class="form-control input-sm" ng-model="ctrl.editarAntenaGUL.tilt_mecanico">
							<span class="input-group-addon">°</span>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.actualizarGUL(ctrl.editarAntenaGUL.id)">Guardar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionGUL()">Cancelar</button>
	</form>
</div>

<div id="eliminarAntenaGUL" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar antena GUL</h3>

	<p>
		¿Está seguro de querer eliminar la antena GUL con características
	</p>
	<p>
		Marca: <b>{{ctrl.eliminarAntenaGUL.tipo_antena_gul.marca}}</b>, Modelo: <b>{{ctrl.eliminarAntenaGUL.tipo_antena_gul.modelo}}</b>, Versión: <b>{{ctrl.eliminarAntenaGUL.tipo_antena_gul.version}}</b>
	</p>
	<p>
		y serie: <b>{{ctrl.eliminarAntenaGUL.numero_serie}}</b>?
	</p>

	<textarea class="form-control" ng-model="ctrl.eliminarAntenaGUL.comentario" style="width:100%;resize: none;" rows="3" placeholder="Por favor indique por qué está eliminando este elemento..."></textarea>
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarGUL()" ng-disabled="!ctrl.eliminarAntenaGUL.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionGUL()">Cancelar</button>
</div>

<!-- ANTENAS IDEN -->

<div id="nuevaAntenaIDEN" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nueva antena iDEN</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Número de serie</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.numero_serie">
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.marca" ng-change="ctrl.listarModelosIDEN(ctrl.nuevaAntenaIDEN.marca)">
							<option value="">Seleccione una marca</option>
							<option ng-repeat="marca in ctrl.marcasIDEN" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.modelo" ng-change="ctrl.listarVersionesMicroondas(ctrl.nuevaAntenaIDEN.marca,ctrl.nuevaAntenaIDEN.modelo)">
							<option value="">Seleccione un modelo</option>
							<option ng-repeat="modelo in ctrl.modelosIDEN" value="{{modelo.modelo}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Sector</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.sector">
							<option value="">Seleccione un sector</option>
							<?php for($i=1; $i < 11; $i++):?>
								<option value="<?php echo $i?>"><?php echo $i?></option>
							<?php endfor?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Altura</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.altura">
					</td>
				</tr>
				<tr>
					<td class="info">Azimuth</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.azimuth">
					</td>
				</tr>
				<tr>
					<td class="info">Tilt eléctrico</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.tilt_electrico">
					</td>
				</tr>
				<tr>
					<td class="info">Tilt mecánico</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevaAntenaIDEN.tilt_mecanico">
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarIDEN()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionIDEN()">Cancelar</button>
	</form>
</div>

<div id="editarAntenaIDEN" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Editar antena iDEN</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Número de serie</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.numero_serie">
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.tipo_antena_iden.marca" ng-change="ctrl.listarModelosIDEN(ctrl.editarAntenaIDEN.tipo_antena_iden.marca)">
							<option value="">Seleccione una marca</option>
							<option ng-repeat="marca in ctrl.marcasIDEN" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.tipo_antena_iden.modelo" ng-change="ctrl.listarVersionesMicroondas(ctrl.editarAntenaIDEN.tipo_antena_iden.marca,ctrl.editarAntenaIDEN.tipo_antena_iden.modelo)">
							<option value="">Seleccione un modelo</option>
							<option ng-repeat="modelo in ctrl.modelosIDEN" value="{{modelo.modelo}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Sector</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.sector">
							<option value="">Seleccione un sector</option>
							<?php for($i=1; $i < 11; $i++):?>
								<option value="<?php echo $i?>"><?php echo $i?></option>
							<?php endfor?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Altura</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.altura">
					</td>
				</tr>
				<tr>
					<td class="info">Azimuth</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.azimuth">
					</td>
				</tr>
				<tr>
					<td class="info">Tilt eléctrico</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.tilt_electrico">
					</td>
				</tr>
				<tr>
					<td class="info">Tilt mecánico</td>
					<td class="default">
						<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editarAntenaIDEN.tilt_mecanico">
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.actualizarIDEN(ctrl.editarAntenaIDEN.id)">Guardar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionIDEN()">Cancelar</button>
	</form>
</div>

<div id="eliminarAntenaIDEN" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar antena iDEN</h3>

	<p>
		¿Está seguro de querer eliminar la antena microondas con características
	</p>
	<p>
		Marca: <b>{{ctrl.eliminarAntenaIDEN.tipo_antena_iden.marca}}</b>, Modelo: <b>{{ctrl.eliminarAntenaIDEN.tipo_antena_iden.modelo}}</b>, Versión: <b>{{ctrl.eliminarAntenaIDEN.tipo_antena_iden.version}}</b>
	</p>
	<p>
		y serie: <b>{{ctrl.eliminarAntenaIDEN.numero_serie}}</b>?
	</p>
	
	<textarea class="form-control" ng-model="ctrl.eliminarAntenaIDEN.comentario" style="width:100%;resize: none;" rows="3" placeholder="Por favor indique por qué está eliminando este elemento..."></textarea>
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarIDEN()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionIDEN()">Cancelar</button>
</div>