<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="linea in ctrl.lineas_electricas">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Línea Eléctrica {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr>
								<td class="info">Fecha de puesta en Servicio</td>
								<td class="default">
									<span ng-show="!linea.editar">{{linea.fecha_servicio | date:"dd/MM/yyyy"}}</span>
									<input ng-hide="!linea.editar" class="form-control" type="date" ng-model="linea.fecha_servicio"/>
								</td>
								<td class="info">Fases</td>
								<td class="default">
									<span ng-show="!linea.editar">{{linea.sistema_linea_electrica.sistema_linea_electrica}}</span>
									<select class="form-control" ng-hide="!linea.editar" ng-model="linea.sistema_linea_electrica.id">
										<option value="">Elegir fase</option>
										<option ng-repeat="fases in ctrl.sistemas_linea_electrica" value="{{fases.id}}">{{fases.sistema_linea_electrica}}</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="info">Conexión</td>
								<td class="default">
									<span ng-show="!linea.editar" ng-repeat="conexion in linea.lineas_electricas_conexion_linea_electrica" bs-popover>
										<span ng-if="$index > 0">/</span>
										<span class="fa fa-question-circle" rel="popover" data-container="body" data-toggle="popover" data-placement="top"  data-trigger="hover" data-title="Nivel de Tensión" data-content="{{conexion.conexion_linea_electrica.nivel_tension}} {{conexion.conexion_linea_electrica.extra}}"></span> {{conexion.conexion_linea_electrica.conexion_linea_electrica}}
									</span>
									<label ng-hide="!linea.editar" style="width:100%">
										<selectize config="ctrl.config" options='ctrl.conexiones_linea_electrica' ng-model="linea.conexiones_linea_electrica"></selectize>
									</label>
								</td>
								<td class="info">Tipo</td>
								<td class="default">
									<span ng-repeat="conexion in linea.lineas_electricas_conexion_linea_electrica">
										<span ng-if="$index > 0">/</span>
										<span>{{conexion.conexion_linea_electrica.tipo}}</span>
									</span>
								</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Sección de Línea</td>
								<td class="default" colspan="2">
									<span ng-show="!linea.editar">{{linea.seccion_linea}}</span>
									<input ng-hide="!linea.editar" class="form-control" type="text" step="any" min="0" ng-model="linea.seccion_linea"/>
								</td>
							</tr>
							<tr>
								<td class="info">Distancia</td>
								<td class="default">
									<div class="input-group">
										<span ng-show="!linea.editar">{{linea.distancia}} km</span>
										<input ng-hide="!linea.editar" class="form-control" type="number" step="any" min="0" ng-model="linea.distancia"/>
										<div class="input-group-addon" ng-hide="!linea.editar">km</div>
									</div>
								</td>
								<td class="info">Postes</td>
								<td class="default">
									<span ng-show="!linea.editar">{{linea.postes}}</span>
									<input ng-hide="!linea.editar" class="form-control" type="number" step="any" min="0" ng-model="linea.postes"/>
								</td>
							</tr>
							<tr ng-repeat-start="propietario in linea.propietarios_linea_electrica" ng-if="(propietario.eliminado == 0)">
								<td class="info text-center" colspan="4"><strong>Propietario {{$index + 1}}</strong></td>
							</tr>
							<tr>
								<td class="info"><button class="btn btn-danger btn-xs" ng-hide="!linea.editar" ng-click="ctrl.eliminandoPropietario(propietario)"><span class="fa fa-times"></span></button> Propietario del Terreno</td>
								<td class="default">
									<span ng-show="!linea.editar">{{propietario.propietario}}</span>
									<input ng-hide="!linea.editar" class="form-control" type="text" step="any" ng-model="propietario.propietario"/>
								</td>
								<td class="info">Contrato de servidumbre</td>
								<td class="default">
									<div class="input-group">
										<span ng-show="!linea.editar">S/ {{propietario.contrato_servidumbre}}</span>
										<div class="input-group-addon" ng-hide="!linea.editar">S/</div>
										<input ng-hide="!linea.editar" class="form-control" type="text" step="any" ng-model="propietario.contrato_servidumbre"/>
									</div>
								</td>
							</tr>
							<tr ng-repeat-end>
								<td class="info">Teléfono</td>
								<td class="default">
									<span ng-show="!linea.editar">{{propietario.telefono}}</span>
									<input ng-hide="!linea.editar" class="form-control" type="text" step="any" ng-model="propietario.telefono"/>
								</td>
								<td class="info">Correo Electrónico</td>
								<td class="default">
									<span ng-show="!linea.editar">{{propietario.correo}}</span>
									<input ng-hide="!linea.editar" class="form-control" type="email" ng-model="propietario.correo"/>
								</td>
							</tr>
							<tr ng-hide="!linea.editar">
								<td colspan="6" class="active text-center">
									<button class="btn btn-info" style="width:100%;" ng-click="ctrl.nuevo_propietario(linea)"><span class="fa fa-plus"></span> Agregar propietario de terreno</button>
								</td>
							</tr>
							<tr class="bg-success">
								<td>Marca del Transformador</td>
								<td>
									<span ng-show="!linea.editar">{{linea.tipo_transformador.marca}}</span>
									<select class="form-control" ng-hide="!linea.editar" ng-model="linea.tipo_transformador.id">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.tipo_transformador" value="{{marca.id}}">{{marca.marca}}</option>
									</select>
								</td>
								<td>Número de Serie</td>
								<td>
									<span ng-show="!linea.editar">{{linea.num_serie_transformador}}</span>
									<input ng-hide="!linea.editar" class="form-control" type="text" step="any" min="0" ng-model="linea.num_serie_transformador"/>
								</td>
							</tr>
							<tr class="bg-success">
								<td>Potencia</td>
								<td>
									<div class="input-group">
										<span ng-show="!linea.editar">{{linea.potencia_transformador}} KVA</span>
										<input ng-hide="!linea.editar" class="form-control" type="number" step="any" min="0" ng-model="linea.potencia_transformador"/>
										<div class="input-group-addon" ng-hide="!linea.editar">KVA</div>
									</div>
								</td>
								<td>Conexión</td>
								<td>
									<span ng-show="!linea.editar">{{linea.conexion_transformador}}</span>
									<input ng-hide="!linea.editar" class="form-control" type="text" step="any" ng-model="linea.conexion_transformador"/>
								</td>
							</tr>
							<tr class="bg-success" ng-hide="!linea.tipo_transformador || linea.tipo_transformador.id == 21">
								<td colspan="4" class="text-center">
									<button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#almacenar" data-whatever="Transformador" data-ts="{{linea.obj_transformador}}">Almacenar Transformador</button>
								</td>
							</tr>
							<tr class="bg-warning">
								<td>Marca del Transformix</td>
								<td>
									<span ng-show="!linea.editar">{{linea.tipo_transformix.marca}}</span>
									<select class="form-control" ng-hide="!linea.editar" ng-model="linea.tipo_transformix.id">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.tipo_transformix" value="{{marca.id}}">{{marca.marca}}</option>
									</select>
								</td>
								<td>Número de Serie</td>
								<td>
									<span ng-show="!linea.editar">{{linea.num_serie_transformix}}</span>
									<input ng-hide="!linea.editar" class="form-control" type="text" step="any" min="0" ng-model="linea.num_serie_transformix"/>
								</td>
							</tr>
							<tr class="bg-warning">
								<td>Potencia</td>
								<td>
									<span ng-show="!linea.editar">{{linea.potencia_transformix}} VA</span>
									<div class="input-group">
										<input ng-hide="!linea.editar" class="form-control" type="number" step="any" min="0" ng-model="linea.potencia_transformix"/>
										<div class="input-group-addon" ng-hide="!linea.editar">VA</div>
									</div>
								</td>
								<td>Conexión</td>
								<td>
									<span ng-show="!linea.editar">{{linea.conexion_transformix}}</span>
									<input ng-hide="!linea.editar" class="form-control" type="text" step="any" min="0" ng-model="linea.conexion_transformix"/>
								</td>
							</tr>
							<tr class="bg-warning" ng-hide="!linea.tipo_transformix || linea.tipo_transformix.id == 21">
								<td colspan="4" class="text-center">
									<button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#almacenar" data-whatever="Transformix" data-ts="{{linea.obj_transformix}}">Almacenar Transformix</button>
								</td>
							</tr>
							<tr>
								<td colspan="2"></td>
								<td style="font-size:8.5pt;list-style:none;text-align:right;" colspan="2">
									<span ng-if="!linea.logs_lineas_electricas.length || linea.logs_lineas_electricas[0].evento.tipo != 'INSERT'">Creado: {{linea.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>
									<span ng-if="linea.logs_lineas_electricas[0].evento.tipo == 'INSERT'">Creado: {{linea.logs_lineas_electricas[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{linea.logs_lineas_electricas[0].usuario.nombres}} {{linea.logs_lineas_electricas[0].usuario.apellido_paterno}}</span>
									<br>
									<span ng-if="linea.logs_lineas_electricas[1].evento.tipo == 'UPDATE'">Actualizado: {{linea.logs_lineas_electricas[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{linea.logs_lineas_electricas[1].usuario.nombres}} {{linea.logs_lineas_electricas[1].usuario.apellido_paterno}}</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editarLinea(linea)" ng-show="!linea.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
					<button class="btn btn-sm btn-success" ng-click="ctrl.grabarLinea(linea)" ng-hide="!linea.editar">Grabar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarLinea(linea)" ng-hide="!linea.editar">Cancelar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoLinea(linea)" ng-show="!linea.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.lineas_electricas.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:15px;" ng-click="ctrl.nuevo_linea()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir Línea Eléctrica</button>
		</div>
	</div>
</div>

<div id="nuevoLinea" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nueva Línea Eléctrica</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Fecha de Servicio</td>
					<td class="default">
						<input type="date" class="form-control input-sm" ng-model="ctrl.nuevoLinea.fecha_servicio" />
					</td>
					<td class="info">Fases</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoLinea.sistema_linea_electrica">
							<option value="">Elegir fase</option>
							<option ng-repeat="fases in ctrl.sistemas_linea_electrica" value="{{fases.id}}">{{fases.sistema_linea_electrica}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Conexión</td>
					<td class="default">
						<label style="width:100%;">
							<selectize config="ctrl.config" options='ctrl.conexiones_linea_electrica' ng-model="ctrl.nuevoLinea.conexiones_linea_electrica"></selectize>
						</label>
						<!--<select class="form-control" ng-model="ctrl.nuevoLinea.conexion_linea_electrica">
							<option value="">Elegir tipo</option>
							<option ng-repeat="tipo in ctrl.conexiones_linea_electrica" value="{{tipo.id}}">{{tipo.conexion_linea_electrica}}</option>
						</select>-->
					</td>
					<td class="info">Sección de Línea</td>
					<td class="default">
						<input class="form-control" type="text" step="any" min="0" ng-model="ctrl.nuevoLinea.seccion_linea" />
					</td>
				</tr>
				<tr>
					<td class="info">Distancia</td>
					<td class="default">
						<div class="input-group">
							<input class="form-control" type="number" step="any" min="0" ng-model="ctrl.nuevoLinea.distancia" />
							<div class="input-group-addon">Km</div>
						</div>
					</td>
					<td class="info">Postes</td>
					<td class="default">
						<input class="form-control" type="number" step="any" min="0" ng-model="ctrl.nuevoLinea.postes" />
					</td>
				</tr>
				<tr>
					<td class="success">Marca del Transformador</td>
					<td class="success">
						<select class="form-control" ng-model="ctrl.nuevoLinea.tipo_transformador">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_transformador" value="{{marca.id}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="success">Número de serie</td>
					<td class="success">
						<input class="form-control" type="text" step="any" min="0" ng-model="ctrl.nuevoLinea.num_serie_transformador"/>
					</td>
				</tr>
				<tr>
					<td class="success">Potencia</td>
					<td class="success">
						<div class="input-group">
							<input class="form-control" type="number" step="any" min="0" ng-model="ctrl.nuevoLinea.potencia_transformador"/>
							<div class="input-group-addon">KVA</div>
						</div>
					</td>
					<td class="success">Conexión</td>
					<td class="success">
						<input class="form-control" type="text" step="any" ng-model="ctlr.nuevoLinea.conexion_transformador"/>
					</td>
				</tr>
				<tr>
					<td class="warning">Marca del Transformix</td>
					<td class="warning">
						<select class="form-control" ng-model="ctrl.nuevoLinea.tipo_transformix">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_transformix" value="{{marca.id}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="warning">Número de serie</td>
					<td class="warning">
						<input class="form-control" type="text" step="any" min="0" ng-model="ctrl.nuevoLinea.num_serie_transformix"/>
					</td>
				</tr>
				<tr>
					<td class="warning">Potencia</td>
					<td class="warning">
						<div class="input-group">
							<input class="form-control" type="number" step="any" min="0" ng-model="ctrl.nuevoLinea.potencia_transformix"/>
							<div class="input-group-addon">VA</div>
						</div>
					</td>
					<td class="warning">Conexión</td>
					<td class="warning">
						<input class="form-control" type="text" step="any" ng-model="ctrl.nuevoLinea.conexion_transformix"/>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarLinea(ctrl.nuevoLinea)" ng-disabled="!ctrl.nuevoLinea.sistema_linea_electrica || !ctrl.nuevoLinea.conexiones_linea_electrica.length || !ctrl.nuevoLinea.distancia || !ctrl.nuevoLinea.postes || !ctrl.nuevoLinea.tipo_transformador || !ctrl.nuevoLinea.potencia_transformador || !ctrl.nuevoLinea.tipo_transformix || !ctrl.nuevoLinea.potencia_transformix">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionLinea()">Cancelar</button>
	</form>
</div>

<div id="eliminarLinea" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar Línea Eléctrica</h3>

	<p>¿Está seguro de eliminar la línea eléctrica?</p>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Linea.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.eliminarLinea()" ng-disabled="!ctrl.eliminar_Linea.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>

<div id="nuevoPropietario" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nuevo propietario</h3>

	<form>
		<div class="panel panel-primary">
			<div class="panel-body">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<td class="info text-right">Nombre del propietario</td>
							<td class="active">
								<input class="form-control" ng-model="ctrl.nuevoPropietario.propietario" maxlength="35"/>
							</td>
							<td class="info text-right">Contrato de servidumbre</td>
							<td class="active">
								<div class="input-group">
									<div class="input-group-addon">S/ </div>
									<input type="number" step="any" class="form-control" ng-model="ctrl.nuevoPropietario.contrato_servidumbre" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="info">Teléfono</td>
							<td class="active">
								<input class="form-control" ng-model="ctrl.nuevoPropietario.telefono" type="text"/>
							</td>
							<td class="info">Correo Electrónico</td>
							<td class="active">
								<input class="form-control" ng-model="ctrl.nuevoPropietario.correo" type="email"/>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="panel-footer text-center">
				<button class="btn btn-success btn-sm" ng-click="ctrl.agregarNuevoPropietario(ctrl.nuevoPropietario)" ng-disabled="!ctrl.nuevoPropietario.propietario || !ctrl.nuevoPropietario.propietario">Agregar</button>
				<button class="btn btn-danger btn-sm" ng-click="ctrl.cancelarNuevoPropietario()">Cancelar</button>
			</div>
		</div>
	</form>
</div>

<div id="eliminarPropietario" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar propietario</h3>

	<form>
		<div class="panel panel-primary">
			<div class="panel-body">
				<p>Explique la razón por la que quiere eliminar el propietario "{{ctrl.eliminar_Propietario.propietario}}"</p>
				<textarea class="form-control" style="width:100%;" rows="3" placeholder="Razón..." ng-model="ctrl.eliminar_Propietario.comentario"></textarea>
			</div>
			<div class="panel-footer text-center">
				<button class="btn btn-success btn-sm" ng-disabled="!ctrl.eliminar_Propietario.comentario" ng-click="ctrl.eliminarPropietario(ctrl.eliminar_Propietario)">Eliminar</button>
				<button class="btn btn-danger btn-sm" ng-click="ctrl.cancelarEliminacionPropietario()">Cancelar</button>
			</div>
		</div>
	</form>
</div>

<div class="modal fade" id="almacenar" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs nav-justified">
					<li class="active"><a data-toggle="tab" href="#almacen" ng-click="ctrl.opcionAlmacen()">Almacén</a></li>
					<li><a data-toggle="tab" href="#sitio" ng-click="ctrl.opcionSitio()">Sitio</a></li>
				</ul>
				<div class="tab-content">
					<div id="almacen" class="tab-pane fade in active">
						<select class="form-control" ng-model="ctrl.almacenarObjeto.almacen">
							<option value="">Elegir almacén</option>
							<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
						</select>
					</div>
					<div id="sitio" class="tab-pane fade">
						<label style="width:100%">
							<selectize config="ctrl.configSitios" options='ctrl.sitios' ng-model="ctrl.almacenarObjeto.a_sitio"></selectize>
						</label>
					</div>
				</div>
				<select class="form-control" ng-model="ctrl.almacenarObjeto.estado">
					<option value="">Seleccionar estado del equipo</option>
					<option value="1">Nuevo</option>
					<option value="2">Operativo</option>
					<option value="3">De baja</option>
				</select><br>
				<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea almacenar el elemento..." ng-model="ctrl.almacenarObjeto.comentario"></textarea>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" ng-click="ctrl.almacenar(ctrl.almacenarObjeto)" ng-disabled="ctrl.disabledAlmacenar()">Almacenar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>