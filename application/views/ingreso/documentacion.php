<br>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Documentación
				</div>
				<div class="panel-body">
				
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
					</div>
					<div id="docs">
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<div id="eliminarArchivo" class="popup">
	<span class="button b-close" ng-if="!ctrl.procesando">
		<span>X</span>
	</span>
	
	<h3>Eliminar Archivo</h3>
	<br><br>
	<p ng-if="!ctrl.procesando">¿Está seguro de eliminar el elemento?</p>
	<br><br>
	<button class="btn btn-success" ng-click="ctrl.eliminarArchivo()" ng-if="!ctrl.procesando">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()" ng-if="!ctrl.procesando">Cancelar</button>
	<br><br>
	<div class="text-center" ng-if="ctrl.procesando">
		<span class="fa fa-spinner fa-spin"></span> Eliminando archivo...
	</div>
</div>

<div id="subirArchivo" class="popup">
	<span class="button b-close" ng-if="!ctrl.procesando">
		<span>X</span>
	</span>
	
	<h3>Subir Archivo</h3>
	<br><br>
	<p ng-if="!ctrl.procesando">Seleccione el archivo que desea subir:</p>
	<br>
	<form ng-if="!ctrl.procesando">
		<input type="file" id="file" name="file" /><br>
		<button class="btn btn-success" ng-click="ctrl.subirArchivo()">Subir</button>
	</form>
	
	<div class="text-center" ng-if="ctrl.procesando">
		<span class="fa fa-spinner fa-spin"></span> Subiendo archivo...
	</div>
</div>

<div id="renombrarArchivo" class="popup">
	<span class="button b-close" ng-if="!ctrl.procesando">
		<span>X</span>
	</span>
	
	<h3>Renombrar Archivo</h3>
	<br><br>
	<p ng-if="!ctrl.procesando">Escriba el nuevo nombre del archivo:</p>
	<br>
	<form ng-if="!ctrl.procesando">
		<input type="text" id="nombre" name="nombre" maxlength="30" onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9\._()\s*\-]/, '')" ng-model="ctrl.renombrar"/>
		<button class="btn btn-success" ng-click="ctrl.renombrarArchivo()" ng-disabled="!ctrl.renombrar">Renombrar</button>
	</form>
	
	<div class="text-center" ng-if="ctrl.procesando">
		<span class="fa fa-spinner fa-spin"></span> Renombrando archivo...
	</div>
</div>