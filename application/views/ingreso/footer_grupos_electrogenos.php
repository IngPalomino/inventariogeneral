<script type="text/javascript">
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargando = true;
			ref.procesando = false;
			ref.oldSitio = "";
			
			ref.config = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Buscar sitio',
				maxItems: 1
			};
			
			ref.grupos_electrogenos = [];
			ref.nuevoGrupo = {};
			ref.eliminar_Grupo = {};
			ref.almacenar_Grupo = {};
			ref.tipo_grupo_electrogeno = [];
			ref.tipo_motor_ge = [];
			ref.tipo_generador_ge = [];
			ref.tipo_avr_ge = [];
			ref.tipo_controlador_ge = [];
			ref.tipo_controlador_tta = [];
			ref.sitios = [];
			ref.almacenes = [];
			
			listarGruposElectrogenos = function(){
				$http({
					url:"<?php echo base_url("index.php/grupos_electrogenos/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.grupos_electrogenos = data;
						
						data.forEach(function(dato,ind,objeto){
							try { objeto[ind].created_at = Date.parse(objeto[ind].created_at); }catch(err){ objeto[ind].created_at = null;}
							
							objeto[ind].logs_grupos_electrogenos.forEach(function(modificacion,indx,objetox){
								objetox[indx].created_at = Date.parse(objetox[indx].created_at);
							});
						});
						
						ref.grupos_electrogenos.forEach(function(grupo,indx,objx){
							objx[indx].disabled = function(){
								var dis = false;
								
								if(this.tipo_grupo_electrogeno.id == "" || this.tipo_motor_ge.id == "" || this.tipo_generador_ge.id == "" || this.tipo_avr_ge.id == "" || this.tipo_controlador_ge.id == "" || this.tipo_controlador_tta.id == "")
								{
									dis = true;
								}
								
								return dis;
							}
							
							/*this.filtroGenerador = function(a){
								var r = true;
								
								if(a.marca != objx[indx].tipo_generador_ge.marca || a.modelo != objx[indx].tipo_generador_ge.modelo)
								{
									r = false;
								}
								
								return r;
							}*/
						});
						
						ref.cargando = false;
					}
					else
					{
						ref.cargando = false;
					}
				}).error(function(err){

				});
			}
			
			listarTipoGrupoElectrogeno = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_grupo_electrogeno/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_grupo_electrogeno = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoMotorGe = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_motor_ge/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_motor_ge = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoGeneradorGe = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_generador_ge/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_generador_ge = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoAVRGe = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_avr_ge/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_avr_ge = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoControladorGe = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_controlador_ge/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_controlador_ge = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoControladorTTA = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_controlador_tta/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_controlador_tta = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarSitios = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar_sitios");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sitios = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.almacenes = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			ref.nuevo_grupo = function(){
				$("#nuevoGrupo").bPopup();
			}
			
			ref.cancelarCreacionGrupo = function(){
				$("#nuevoGrupo").bPopup().close();
				ref.nuevoGrupo = {};
			}
			
			ref.agregarGrupo = function(grupo){
				var data = "",
					campos = Object.keys(grupo),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(grupo[campo] != null)
						{
							if(typeof grupo[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+grupo[campo];
							}
							else
							{
								if(campo == "fecha_servicio")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+grupo[campo].toISOString();
								}
								else
								{
									if(grupo[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(grupo[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+grupo[campo].id;
									}
										
								}
							}

							contador++;
						}
					}
				});
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("index.php/grupos_electrogenos/grupo_electrogeno/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevoGrupo").bPopup().close();
					listarGruposElectrogenos();
					ref.nuevoGrupo = {};
				}).error(function(err){

				});
			}
			
			ref.eliminandoGrupo = function(info){
				ref.eliminar_Grupo = info;
				$("#eliminarGrupo").bPopup();
			}
			
			ref.cancelarEliminacion = function(){
				$("#eliminarGrupo").bPopup().close();
				ref.eliminar_Grupo = {};
			}
			
			ref.eliminarGrupo = function(){
				var data = "comentario="+ref.eliminar_Grupo.comentario;

				$http({
					url:"<?php echo base_url("index.php/grupos_electrogenos/grupo_electrogeno");?>"+"/"+ref.eliminar_Grupo.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.grupos_electrogenos = [];
						listarGruposElectrogenos();
						ref.eliminar_Grupo = {};
						$('#eliminarGrupo').bPopup().close();
					}
				}).error(function(err){

				});
			}

			
			ref.editarGrupo = function(grupo){
				grupo.editar = true;
			}
			
			ref.grabarGrupo = function(grupo){
				borrarPropiedades(grupo,"mantenimientos_grupos_electrogenos");
				borrarPropiedades(grupo,"logs_grupos_electrogenos");
				ref.procesando = true;
				
				var data = "",
					campos = Object.keys(grupo),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(grupo[campo] != null)
						{
							if(typeof grupo[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+grupo[campo];
							}
							else
							{
								if(campo == "fecha_servicio")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+grupo[campo].toISOString();
								}
								else
								{
									if(grupo[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(grupo[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+grupo[campo].id;
									}
										
								}
							}

							contador++;
						}
					}
				});
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("index.php/grupos_electrogenos/grupo_electrogeno");?>/"+grupo.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					grupo.editar = false;
					listarGruposElectrogenos();
					ref.procesando = false;
				}).error(function(err){

				});
			}

			ref.cancelarGrupo = function(grupo){
				grupo.editar = false;
				listarGruposElectrogenos();
			}
			
			ref.almacenandoGrupoElectrogeno = function(info){
				ref.almacenar_GrupoElectrogeno = info;
				ref.almacenar_GrupoElectrogeno.tipo_almacen = 1;
				$("#almacenarGrupoElectrogeno").bPopup({
					amsl: 0
				});
			}

			ref.almacenarGrupoElectrogeno = function(grupo){
				var data = "tipo_grupo_electrogeno="+grupo.tipo_grupo_electrogeno.id
						+"&numero_serie_grupo_electrogeno="+grupo.numero_serie_grupo_electrogeno
						+"&tipo_motor_ge="+grupo.tipo_motor_ge.id
						+"&numero_serie_motor="+grupo.numero_serie_motor
						+"&tipo_generador_ge="+grupo.tipo_generador_ge.id
						+"&numero_serie_generador="+grupo.numero_serie_generador
						+"&tipo_avr_ge="+grupo.tipo_avr_ge.id
						+"&numero_serie_avr="+grupo.numero_serie_avr
						+"&tipo_controlador_ge="+grupo.tipo_controlador_ge.id
						+"&numero_serie_controlador_ge="+grupo.numero_serie_controlador_ge
						+"&tipo_controlador_tta="+grupo.tipo_controlador_tta.id
						+"&numero_serie_controlador_tta="+grupo.numero_serie_controlador_tta
						+"&estado="+grupo.estado
						+"&proveniente_sitio="+grupo.sitio
						+"&tipo_almacen="+grupo.tipo_almacen
						+"&almacen="+((grupo.tipo_almacen == 1)? grupo.almacen : grupo.a_sitio)
						+"&comentario="+grupo.comentario;
				/*console.log(data);*/

				$http({
					url:"<?php echo base_url("grupos_electrogenos/grupo_electrogeno/almacenar");?>/"+grupo.id,
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#almacenarGrupoElectrogeno").bPopup().close();
					ref.grupos_electrogenos = [];
					listarGruposElectrogenos();
					ref.almacenar_GrupoElectrogeno = {};
				}).error(function(err){

				});
			}

			ref.cancelarAlmacenamientoGrupoElectrogeno = function(){
				$("#almacenarGrupoElectrogeno").bPopup().close();
				ref.almacenar_GrupoElectrogeno = {};
			}

			ref.grupoElectrogenoOpcionSitio = function(){
				ref.almacenar_GrupoElectrogeno.tipo_almacen = 2;
			}

			ref.grupoElectrogenoOpcionAlmacen = function(){
				ref.almacenar_GrupoElectrogeno.tipo_almacen = 1;
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}
			
			listarGruposElectrogenos();
			listarTipoGrupoElectrogeno();
			listarTipoMotorGe();
			listarTipoGeneradorGe();
			listarTipoAVRGe();
			listarTipoControladorGe();
			listarTipoControladorTTA();
			listarSitios();
			listarAlmacenes();
		}]);
</script>
</body>
</html>