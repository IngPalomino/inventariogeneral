<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-if="!ctrl.cargando">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Resumen
				</div>
				<div class="panel-body">
					<div class="col-md-12" ng-show="<?php echo ($infoSitio[0]['factor_uso_torre'] > 1) ? 'true' : 'false'; ?>">
						<div class="alert alert-danger text-center">
							<i class="fa fa-exclamation-triangle fa-2x"></i>
							<strong style="font-size: 25px;">Torre bloqueada para nuevas instalaciones!</strong>
						</div>
					</div>
					<div class="col-xs-6">
						<h4 ng-repeat-start="infraestructura in ctrl.objetos | filter:{parent:'infraestructura'}">{{infraestructura.objeto}}</h4>
						<table class="table table-condensed table-hover" style="table-layout:fixed" ng-repeat-end>
							<thead>
								<tr ng-if="infraestructura.objeto != 'Enlaces'">
									<th>Marca</th>
									<th>Modelo</th>
								</tr>
								<tr ng-if="infraestructura.objeto == 'Enlaces'">
									<th>Near End</th>
									<th>Far End</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="info in infraestructura.info" ng-if="infraestructura.objeto == 'Antenas Microondas'">
									<td>{{info.tipo_antena_mw.marca}}</td>
									<td>{{info.tipo_antena_mw.modelo}}</td>
								</tr>
								<tr ng-repeat="info in infraestructura.info" ng-if="infraestructura.objeto == 'Antenas GUL'">
									<td>{{info.tipo_antena_gul.marca}}</td>
									<td>{{info.tipo_antena_gul.modelo}}</td>
								</tr>
								<tr ng-repeat="info in infraestructura.info" ng-if="infraestructura.objeto == 'IDUs'">
									<td>{{info.tipo_idu.marca}}</td>
									<td>{{info.tipo_idu.modelo}}</td>
								</tr>
								<tr ng-repeat="info in infraestructura.info" ng-if="infraestructura.objeto == 'Enlaces'">
									<td>{{info.idu_1.sitio.nombre_completo}}</td>
									<td>{{info.idu_2.sitio.nombre_completo}}</td>
								</tr>
								<tr>
									<td></td>
									<td class="info"># Total: {{infraestructura.cantidad}}</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="col-xs-6">
						<h4 ng-repeat-start="energia in ctrl.objetos | filter:{parent:'energia'}">{{energia.objeto}}</h4>
						<table class="table table-condensed table-hover" style="table-layout:fixed" ng-repeat-end>
							<thead>
								<tr ng-if="energia.objeto == 'Plantas Rectificadoras'">
									<th>Marca</th>
									<th>Controlador</th>
								</tr>
								<tr ng-if="energia.objeto == 'Bancos de Baterías' || energia.objeto == 'Grupos Electrógenos'">
									<th>Marca</th>
									<th>Modelo</th>
								</tr>
								<tr ng-if="energia.objeto == 'Suministros'">
									<th>Concesionaria</th>
									<th>Suministro</th>
								</tr>
								<tr ng-if="energia.objeto == 'Líneas Eléctricas'">
									<th>Fecha de puesta en servicio</th>
									<th>Fases</th>
								</tr>
								<tr ng-if="energia.objeto == 'Tanques de Combustible'">
									<th>Tipo de Tanque</th>
									<th>Fecha de Instalación</th>
								</tr>
								<tr ng-if="energia.objeto == 'Aires Acondicionados'">
									<th>Marca</th>
									<th>Tipo de Equipo</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="info in energia.info" ng-if="energia.objeto == 'Grupos Electrógenos'">
									<td>{{info.tipo_grupo_electrogeno.marca}}</td>
									<td>{{info.tipo_grupo_electrogeno.modelo}}</td>
								</tr>
								<tr ng-repeat="info in energia.info" ng-if="energia.objeto == 'Plantas Rectificadoras'">
									<td>{{info.tipo_controlador_rectif.marca}}</td>
									<td>{{info.tipo_controlador_rectif.modelo}}</td>
								</tr>
								<tr ng-repeat="info in energia.info" ng-if="energia.objeto == 'Bancos de Baterías'">
									<td>{{info.tipo_bancos_baterias.marca}}</td>
									<td>{{info.tipo_bancos_baterias.modelo}}</td>
								</tr>
								<tr ng-repeat="info in energia.info" ng-if="energia.objeto == 'Suministros'">
									<td>{{info.concesionaria_suministro.concesionaria_suministro}}</td>
									<td>{{info.numero_suministro}}</td>
								</tr>
								<tr ng-repeat="info in energia.info" ng-if="energia.objeto == 'Líneas Eléctricas'">
									<td>{{info.fecha_servicio | date:"dd/MM/yyyy"}}</td>
									<td>{{info.sistema_linea_electrica.sistema_linea_electrica}}</td>
								</tr>
								<tr ng-repeat="info in energia.info" ng-if="energia.objeto == 'Tanques de Combustible'">
									<td>
										<span ng-if="info.tipo_tanque_combustible == 1">Tanque Interno</span>
										<span ng-if="info.tipo_tanque_combustible == 2">Tanque Externo</span>
									</td>
									<td>{{info.fecha_instalacion | date:"dd/MM/yyyy"}}</td>
								</tr>
								<tr ng-repeat="info in energia.info" ng-if="energia.objeto == 'Aires Acondicionados'">
									<td>{{info.tipo_aires_acondicionados.marca}}</td>
									<td>{{info.tipo_aires_acondicionados.tipo_equipo}}</td>
								</tr>
								<tr>
									<td></td>
									<td class="info"># Total: {{energia.cantidad}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>