<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="rectificador in ctrl.rectificadores">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Planta Rectificadora {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr>
								<td class="info">Marca</td>
								<td class="default" colspan="2">
									<span ng-show="!rectificador.editar">{{rectificador.tipo_controlador_rectif.marca}}</span>
									<select class="form-control" ng-hide="!rectificador.editar" ng-model="rectificador.tipo_controlador_rectif.marca" ng-change="rectificador.listarModelosControlador(rectificador.tipo_controlador_rectif.marca)">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.tipo_controlador_rectif" value="{{marca.marca}}">{{marca.marca}}</option>
									</select>
								</td>
								<td class="info">Controlador</td>
								<td class="default" colspan="2">
									<span ng-show="!rectificador.editar">{{rectificador.tipo_controlador_rectif.modelo}}</span>
									<select class="form-control" ng-hide="!rectificador.editar" ng-model="rectificador.tipo_controlador_rectif.id" ng-change="ctrl.listarTipoModulosRectif(rectificador)">
										<option value="">Elegir controlador</option>
										<option ng-repeat="modelo in rectificador.tipos_controlador_rectif" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
							</tr>
							<tr>
								<!--<td class="info">Número de serie</td>
								<td class="default" colspan="2">
									<span ng-show="!rectificador.editar">{{rectificador.numero_serie}}</span>
									<input class="form-control" ng-hide="!rectificador.editar" type="text" ng-model="rectificador.numero_serie"/>
								</td>-->
								<td class="info">Fecha de puesta en servicio</td>
								<td class="default" colspan="2">
									<span ng-show="!rectificador.editar">{{rectificador.anio_servicio | date:"dd/MM/yyyy"}}</span>
									<input class="form-control" ng-hide="!rectificador.editar" type="date" ng-model="rectificador.anio_servicio"/>
								</td>
								<td class="info">Capacidad Máxima</td>
								<td class="default" colspan="2">
									<span ng-show="!rectificador.editar">{{rectificador.capacidad_maxima}} kW</span>
									<div class="input-group">
										<input class="form-control" ng-hide="!rectificador.editar" type="number" step="any" min="0" ng-model="rectificador.capacidad_maxima"/>
										<div class="input-group-addon" ng-hide="!rectificador.editar">kW</div>
									</div>
								</td>
							</tr>
							<tr>
								<td class="info">Ubicación</td>
								<td class="default" colspan="2">
									<span ng-show="!rectificador.editar">{{rectificador.ubicacion_controlador_rectif.ubicacion_controlador_rectif}}</span>
									<select class="form-control" ng-hide="!rectificador.editar" ng-model="rectificador.ubicacion_controlador_rectif.id">
										<option value="">Elegir ubicación</option>
										<option ng-repeat="ubicacion in ctrl.ubicacion_controlador_rectif" value="{{ubicacion.id}}">{{ubicacion.ubicacion_controlador_rectif}}</option>
									</select>
								</td>
								<td class="info" colspan="3"></td>
							</tr>
						</tbody>
						<tbody>
							<tr class="success text-center">
								<td colspan="6">Módulos Rectificadores</td>
							</tr>
							<tr ng-repeat="modulo in rectificador.modulos_rectif" ng-if="modulo.eliminado == '0'">
								<td class="info"><button class="btn btn-danger btn-xs" ng-hide="!rectificador.editar" ng-click="ctrl.eliminandoModRectificador(modulo)"><i class="fa fa-times"></i></button> Tipo</td>
								<td class="default">
									<span ng-show="!rectificador.editar">{{modulo.tipo_modulos_rectif.modelo}}</span>
									<select class="form-control" ng-hide="!rectificador.editar" ng-model="modulo.tipo_modulos_rectif.id">
										<option value="">Elegir modelo</option>
										<option ng-repeat="modelo in modulo.tipos_modulos_rectif" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
								<td class="info">Capacidad</td>
								<td class="default">
									<span ng-if="capacidad.id == modulo.tipo_modulos_rectif.id" ng-repeat="capacidad in modulo.tipos_modulos_rectif">{{capacidad.capacidad}} kW</span>
								</td>
								<td class="info">Número de serie</td>
								<td class="default">
									<span ng-show="!rectificador.editar">{{modulo.numero_serie}}</span>
									<input class="form-control" ng-hide="!rectificador.editar" type="text" ng-model="modulo.numero_serie"/>
								</td>
							</tr>
							<tr ng-hide="!rectificador.editar">
								<td colspan="6">
									<button class="btn btn-primary" style="width:100%" ng-click="ctrl.nuevoModRectificador(rectificador)">
										<span class="fa fa-plus"></span> Agregar Módulo Rectificador
									</button>
								</td>
							</tr>
							<tr class="warning">
								<td>Modulos Instalados</td>
								<td colspan="2">{{rectificador.modulosInstalados()}}</td>
								<td>Capacidad Instalada</td>
								<td colspan="2">{{rectificador.capacidadInstalada()}} kW</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editarRectificador(rectificador)" ng-show="!rectificador.editar">Editar</button>
					<button class="btn btn-sm btn-success" ng-click="ctrl.grabarRectificador(rectificador)" ng-hide="!rectificador.editar" ng-disabled="rectificador.deshabilitarGrabar()">Grabar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarRectificador(rectificador)" ng-hide="!rectificador.editar">Cancelar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoRectificador(rectificador)" ng-show="!rectificador.editar">Eliminar</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-default" style="width:100%;" ng-click="ctrl.nuevo_rectificador()"><i class="fa fa-plus"></i> Añadir Planta Rectificadora</button>
		</div>
	</div>
</div>

<div id="nuevoRectificador" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Nueva Planta Rectificadora</h3>
	
	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoRectificador.tipo_controlador_rectif.marca" ng-change="ctrl.nuevoRectificador.listarModelosControlador()">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_controlador_rectif" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Controlador</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoRectificador.tipo_controlador_rectif.id" ng-change="ctrl.nuevoRectificador.listarModulosRectif(ctrl.nuevoRectificador.tipo_controlador_rectif.id)">
							<option value="">Elegir Controlador</option>
							<option ng-repeat="modelo in ctrl.nuevoRectificador.tipos_controlador_rectif" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<!--<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="ctrl.nuevoRectificador.numero_serie"/>
					</td>-->
					<td class="info">Fecha de puesta en servicio</td>
					<td class="default">
						<input class="form-control" type="text" id="datepicker" size="30" ng-model="ctrl.nuevoRectificador.anio_servicio"/>
					</td>
					<td class="info">Capacidad Máxima</td>
					<td class="default">
						<div class="input-group">
							<input class="form-control" type="number" step="any" min="0" ng-model="ctrl.nuevoRectificador.capacidad_maxima" />
							<div class="input-group-addon">kW</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Ubicación</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoRectificador.ubicacion_controlador_rectif.id">
							<option value="">Elegir ubicación</option>
							<option ng-repeat="ubicacion in ctrl.ubicacion_controlador_rectif" value="{{ubicacion.id}}">{{ubicacion.ubicacion_controlador_rectif}}</option>
						</select>
					</td>
					<td class="info" colspan="2"></td>
				</tr>
				<tr class="success text-center">
					<td colspan="4">Módulos Rectificadores</td>
				</tr>
				<tr class="ng-cloak" ng-repeat="modRectif in ctrl.nuevoRectificador.modulos_rectif" ng-if="ctrl.nuevoRectificador.tipo_controlador_rectif.marca && ctrl.nuevoRectificador.tipo_controlador_rectif.id">
					<td class="info"><button class="btn btn-danger btn-xs" ng-click="ctrl.eliminarNuevoModRectif($index)"><i class="fa fa-times"></i></button> Tipo</td>
					<td class="default">
						<select class="form-control" ng-model="modRectif.tipo_modulos_rectif.id">
							<option value="">Elegir modelo</option>
							<option ng-repeat="modelo in modRectif.tipos_modulos_rectif" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="modRectif.numero_serie"/>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<button class="btn btn-primary" style="width:100%" ng-click="ctrl.nuevoModRectif(ctrl.nuevoRectificador)" ng-disabled="!ctrl.nuevoRectificador.tipo_controlador_rectif.marca || !ctrl.nuevoRectificador.tipo_controlador_rectif.id">
							<span class="fa fa-plus"></span> Agregar Módulo Rectificador
						</button>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarRectificador(ctrl.nuevoRectificador)" ng-disabled="!ctrl.nuevoRectificador.tipo_controlador_rectif.id || !ctrl.nuevoRectificador.anio_servicio || !ctrl.nuevoRectificador.capacidad_maxima || !ctrl.nuevoRectificador.anio_servicio || !ctrl.nuevoRectificador.modulos_rectif.length || ctrl.nuevoRectificador.deshabilitarGrabar()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionRectificador()">Cancelar</button>
	</form>
</div>

<div id="eliminarRectificador" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar Rectificador</h3>

	<p>¿Está seguro de eliminar el rectificador?</p>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Rectificador.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.eliminarRectificador()" ng-disabled="!ctrl.eliminar_Rectificador.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>

<div id="eliminarModRectificador" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar Módulo Rectificador</h3>

	<form>
		<div class="panel panel-primary">
			<div class="panel-body">
				<p>Explique la razón por la que quiere eliminar el Módulo Rectificador</p>
				<textarea class="form-control" style="width:100%;" rows="3" placeholder="Razón..." ng-model="ctrl.eliminar_ModRectificador.comentario"></textarea>
			</div>
			<div class="panel-footer text-center">
				<button class="btn btn-success btn-sm" ng-disabled="!ctrl.eliminar_ModRectificador.comentario" ng-click="ctrl.eliminarModRectificador(ctrl.eliminar_ModRectificador)">Eliminar</button>
				<button class="btn btn-danger btn-sm" ng-click="ctrl.cancelarEliminacionModRectificador()">Cancelar</button>
			</div>
		</div>
	</form>
</div>