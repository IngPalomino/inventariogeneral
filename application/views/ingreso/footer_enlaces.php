<script type="text/javascript">
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargando = false;
			ref.almacenarODUs = [];
			ref.almacenarAntenas = [];
			ref.almacenar_Elementos = {};
			ref.previaEliminar_Enlace = {};
			
			ref.enlaces = [];
			ref.sitios = [];
			ref.almacenes = [];
			ref.enlace = {};
			ref.idus_fe = [];
			ref.idus_ne = [];

			ref.data_idus_ne = [];

			ref.reenrutar_Enlace = {};
			ref.nuevoEnlace = {
				configuracion_3: "",
				configuracion_4: "",
				odus_enlaces_ne: [],
				odus_enlaces_fe: [],
				antenas_mw_enlaces_ne: [],
				antenas_mw_enlaces_fe: [],
				nuevaOduNE: function(enlace = ref.nuevoEnlace){
					this.idu = enlace.idu_1;
					this.ftx = "";
					this.ptx = "";
					this.numero_serie = "";
					this.tipo_odu = {
						id: "",
						marca: "",
						modelo:"",
						part_number: "",
						sub_banda: "",
						frec_min: 0,
						frec_max: 0,
						banda: "",
						peso: 0,
						tipo: "",
						hi_low: ""
					};
				},
				nuevaOduFE: function(enlace = ref.nuevoEnlace){
					this.idu = enlace.idu_2;
					this.ftx = "";
					this.ptx = "";
					this.numero_serie = "";
					this.tipo_odu = {
						id: "",
						marca: "",
						modelo:"",
						part_number: "",
						sub_banda: "",
						frec_min: 0,
						frec_max: 0,
						banda: "",
						peso: 0,
						tipo: "",
						hi_low: ""
					};
				},
				nuevaAntenaNE: function(enlace = ref.nuevoEnlace){
					this.idu = enlace.idu_1;
					this.numero_serie = "";
					this.altura = "";
					this.azimuth = "";
					this.tipo_antena_mw = {
						id: "",
						marca: "",
						modelo: "",
						diametro: 0,
						banda: 0,
						peso: 0
					};
				 
				},
				nuevaAntenaFE: function(enlace = ref.nuevoEnlace){
					this.idu = enlace.idu_2;
					this.numero_serie = "";
					this.altura = "";
					this.azimuth = "";
					this.tipo_antena_mw = {
						id: "",
						marca: "",
						modelo: "",
						diametro: 0,
						banda: 0,
						peso: 0
					};
				},
				disabled: function(){
					var disabled = false;
					
					if(!this.odus_enlaces_fe.length || !this.odus_enlaces_ne.length/* || !this.antenas_mw_enlaces_fe.length || !this.antenas_mw_enlaces_ne.length*/)
					{
						disabled = true;
					}
					else
					{
						this.odus_enlaces_fe.forEach(function(odu,indy,objetoy){
							if(!objetoy[0].tipo_odu.id || !odu.numero_serie || !objetoy[0].ftx)
							{
								disabled = true;
							}
						});
						
						this.odus_enlaces_ne.forEach(function(odu,indy,objetoy){
							if(!objetoy[0].tipo_odu.id || !odu.numero_serie || !objetoy[0].ftx)
							{
								disabled = true;
							}
						});
						/*this.odus_enlaces_fe.forEach(function(odu,indy,objetoy){
							if(!odu.tipo_odu.id || !odu.numero_serie || !odu.ftx || !odu.ptx)
							{
								disabled = true;
							}
						});
						
						this.odus_enlaces_ne.forEach(function(odu,indy,objetoy){
							if(!odu.tipo_odu.id || !odu.numero_serie || !odu.ftx || !odu.ptx)
							{
								disabled = true;
							}
						});
						
						this.antenas_mw_enlaces_fe.forEach(function(antena,indy,objetoy){
							if(!antena.tipo_antena_mw.id || !antena.altura || !antena.numero_serie)
							{
								disabled = true;
							}
						});
						
						this.antenas_mw_enlaces_ne.forEach(function(antena,indy,objetoy){
							if(!antena.tipo_antena_mw.id || !antena.altura || !antena.numero_serie)
							{
								disabled = true;
							}
						});*/
					}
					
					if((this.modulacion_adaptativa && (!this.modulacion_am_numero || !this.modulacion_am_letras || !this.capacidad_am)) || (this.configuracion_2 != "0" && this.configuracion_2 != "1"))
					{
						disabled = true;
					}
					
					return disabled;
				}
			};
			
			ref.editarEnlace = {};
			ref.eliminar_Enlace = {};
			ref.editarOdu = {};
			ref.eliminar_Odu = {};
			ref.editarAntena = {};
			ref.eliminar_Antena = {};
			ref.tipo_odu = [];
			ref.tipo_antena_mw = [];
			ref.nuevoOdu = {};
			ref.nuevoAntena = {};
			ref.almacenar_AntenaMicroondas = {};
			
			ref.config = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Buscar sitio',
				maxItems: 1
			};
			
			listarEnlaces = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("enlaces/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{	/*console.log(data);*/
						ref.enlaces = data;
						
						data.forEach(function(dato,ind,objeto){
							var configuracion = objeto[ind].configuracion.split("+");
							var configuracion_2 = configuracion[1].split(" ");
							
							if(configuracion[1] == 1 || configuracion[1] == 0)
							{
								var configuracion_3 = "";
								var configuracion_4 = "";
							}
							else
							{
								if(configuracion_2[1] != "XPIC")
								{
									var configuracion_3 = "";
									var configuracion_4 = configuracion_2[1];
								}
								else if(configuracion_2[1] == "XPIC")
								{
									var configuracion_3 = configuracion_2[1];
									var configuracion_4 = "";
								}
								else
								{
									var configuracion_3 = configuracion_2[1];
									var configuracion_4 = configuracion_2[2];
								}
							}
							
							var modulacion_numero = objeto[ind].modulacion.split(/[a-zA-Z]/);
							var modulacion_letras = objeto[ind].modulacion.split(/^[0-9]+/);
							
							objeto[ind].configuracion_editar = {
								part_1: Number(configuracion[0]),
								part_2: Number(configuracion_2[0]),
								part_3: configuracion_3,
								part_4: configuracion_4
							};
							
							objeto[ind].modulacion_editar = {
								numero: modulacion_numero[0],
								letras: modulacion_letras[1]
							};
							
							if(objeto[ind].modulacion_adaptativa != 0)
							{
								objeto[ind].modulacion_adaptativa = true;
							}
							else
							{
								objeto[ind].modulacion_adaptativa = false;
							}
							
							if(objeto[ind].modulacion_am != "N/A")
							{
								var modulacion_am_numero = objeto[ind].modulacion.split(/[a-zA-Z]/);
								var modulacion_am_letras = objeto[ind].modulacion.split(/^[0-9]+/);
								
								objeto[ind].modulacion_am_editar = {
									numero: modulacion_am_numero[0],
									letras: modulacion_am_letras[1]
								};
							}
							else
							{
								objeto[ind].modulacion_am_editar = {
									numero: "",
									letras: ""
								};
							}

							try{ objeto[ind].created_at = Date.parse(objeto[ind].created_at); } catch(err){ objeto[ind].created_at = null; }

							objeto[ind].logs_enlaces.forEach(function(log,indx,objetox){
								try{ objetox[indx].created_at = Date.parse(objetox[indx].created_at); } catch(err){ objetox[indx].created_at = null; }
							});
						});
						
						ref.cargando = false;
					}
					else
					{
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}
			
			listarSitios = function(){
				$http({
					url:"<?php echo base_url("enlaces/listar_sitios");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sitios = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.almacenes = data;
					}
				}).error(function(err){

				});
			}
			
			ref.listarEnlace = function(enlace){
				ref.enlace = enlace;
				var keys = Object.keys(ref.enlace);
				ref.enlace.len = keys.length;
				ref.enlace.index = enlace.index;
			}
			
		  ref.listarIDUsFE = function(sitio){
				$http({
					url:"<?php echo base_url("idus/listar_sitio");?>"+"/"+sitio,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
					   ref.idus_fe = data;
						/*console.log(data);*/
					}
					else
					{
						ref.idus_fe = [];
					}
				}).error(function(err){

				});
			}
			
			listarIDUsNE = function(){
				$http({
					url:"<?php echo base_url("idus/listar_sitio");?>"+"/"+<?php echo $sitio; ?>,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.idus_ne = data;
						/*console.log(data);*/
					}
					else
					{
						ref.idus_ne = [];
					}
				}).error(function(err){

				});
			}
			
			listarTipoOdu = function(){
				$http({
					url:"<?php echo base_url("tipo_odu/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipo_odu = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoAntena = function(){
				$http({
					url:"<?php echo base_url("tipo_antena_mw/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_antena_mw = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			ref.nuevo_enlace = function(){
				$("#nuevoEnlace").bPopup({
					amsl: 0
				});
			}
			
			ref.cancelarCreacionEnlace = function(){
				$("#nuevoEnlace").bPopup().close();
				ref.nuevoEnlace = {
					configuracion_3: "",
					configuracion_4: "",
					odus_enlaces_ne: [],
					odus_enlaces_fe: [],
					antenas_mw_enlaces_ne: [],
					antenas_mw_enlaces_fe: [],
					nuevaOduNE: function(enlace = ref.nuevoEnlace){
						this.idu = enlace.idu_1;
						this.ftx = "";
						this.ptx = "";
						this.numero_serie = "";
						this.tipo_odu = {
							id: "",
							marca: "",
							modelo:"",
							part_number: "",
							sub_banda: "",
							frec_min: 0,
							frec_max: 0,
							banda: "",
							peso: 0,
							tipo: "",
							hi_low: ""
						};
					},
					nuevaOduFE: function(enlace = ref.nuevoEnlace){
						this.idu = enlace.idu_2;
						this.ftx = "";
						this.ptx = "";
						this.numero_serie = "";
						this.tipo_odu = {
							id: "",
							marca: "",
							modelo:"",
							part_number: "",
							sub_banda: "",
							frec_min: 0,
							frec_max: 0,
							banda: "",
							peso: 0,
							tipo: "",
							hi_low: ""
						};
					},
					nuevaAntenaNE: function(enlace = ref.nuevoEnlace){
						this.idu = enlace.idu_1;
						this.numero_serie = "";
						this.altura = "";
						this.azimuth = "";
						this.tipo_antena_mw = {
							id: "",
							marca: "",
							modelo: "",
							diametro: 0,
							banda: 0,
							peso: 0
						};


					},
					nuevaAntenaFE: function(enlace = ref.nuevoEnlace){
						this.idu = enlace.idu_2;
						this.numero_serie = "";
						this.altura = "";
						this.azimuth = "";
						this.tipo_antena_mw = {
							id: "",
							marca: "",
							modelo: "",
							diametro: 0,
							banda: 0,
							peso: 0
						};
					},
					disabled: function(){
						var disabled = false;
						
						if(!this.odus_enlaces_fe.length || !this.odus_enlaces_ne.length)
						{
							disabled = true;
						}
						else
						{
							this.odus_enlaces_fe.forEach(function(odu,indy,objetoy){
								if(!objetoy[0].tipo_odu.id || !odu.numero_serie || !objetoy[0].ftx)
								{
									disabled = true;
								}
							});
							
							this.odus_enlaces_ne.forEach(function(odu,indy,objetoy){
								if(!objetoy[0].tipo_odu.id || !odu.numero_serie || !objetoy[0].ftx)
								{
									disabled = true;
								}
							});
						}
						
						if((this.modulacion_adaptativa && (!this.modulacion_am_numero || !this.modulacion_am_letras || !this.capacidad_am)) || (this.configuracion_2 != "0" && this.configuracion_2 != "1"))
						{
							disabled = true;
						}
						
						return disabled;
					}
				};
			}
			
			ref.agregarEnlace = function(enlace){
				
                var idsitio_ne = $("#idsitio-idu-near-end");
                var idsitio_fe = $("#idsitio-idu-far-end");

				//borrarPropiedades(enlace,"sitio_fe");
				borrarPropiedades(enlace,"nuevaOduNE");
				borrarPropiedades(enlace,"nuevaOduFE");
				borrarPropiedades(enlace,"nuevaAntenaNE");
				borrarPropiedades(enlace,"nuevaAntenaFE");
				borrarPropiedades(enlace.odus_enlaces_ne,"$$hashKey");
				borrarPropiedades(enlace.odus_enlaces_fe,"$$hashKey");
				borrarPropiedades(enlace.antenas_mw_enlaces_ne,"$$hashKey");
				borrarPropiedades(enlace.antenas_mw_enlaces_fe,"$$hashKey");
				
				enlace.odus_enlaces_ne.forEach(function(odu,ind,objeto){
					if(objeto[0])
					{
						objeto[ind].tipo_odu = objeto[0].tipo_odu;
						objeto[ind].ftx = objeto[0].ftx;
						objeto[ind].ptx = objeto[0].ptx;
						
					}
				});
				
				enlace.odus_enlaces_fe.forEach(function(odu,ind,objeto){
					if(objeto[0])
					{
						objeto[ind].tipo_odu = objeto[0].tipo_odu;
						objeto[ind].ftx = objeto[0].ftx;
						objeto[ind].ptx = objeto[0].ptx;
						
					}
				});

// nueva creacion ne
				enlace.antenas_mw_enlaces_ne.forEach(function(odu,ind,objeto){
					if(objeto[0])
					{
					objeto[ind].idsitio_ne = $("#idsitio-idu-near-end").val();						
					}
				});
// nueva creacion ne

// nueva creacion fe
				enlace.antenas_mw_enlaces_fe.forEach(function(odu,ind,objeto){
					if(objeto[0])
					{
					objeto[ind].idsitio_fe = $("#idsitio-idu-far-end").val();						
					}
				});
// nueva creacion fe


				
				var data = "",
					campos = Object.keys(enlace),
					contador = 0;
					
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(enlace[campo] != null)
						{
							if(typeof enlace[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+enlace[campo];
							}
							else
							{
								if(enlace[campo].length != undefined)
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+JSON.stringify(enlace[campo]);
								}
								else
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+enlace[campo].id;
								}
							}

							contador++;
						}
					}
				});				
				//console.log(data);
				$http({
					url:"<?php echo base_url("enlaces/enlace/sitio");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
                  
                      if(resp > 0){
                        alert("El Espacio  No esta disponible Elija otro azimuth");
                      }else{
                        //-----------------------
                        	                   if(!(resp.error))
					    {
						$("#nuevoEnlace").bPopup().close();
						listarEnlaces();
						ref.nuevoEnlace = {
							configuracion_3: "",
							configuracion_4: "",
							odus_enlaces_ne: [],
							odus_enlaces_fe: [],
							antenas_mw_enlaces_ne: [],
							antenas_mw_enlaces_fe: [],						
							nuevaOduNE: function(enlace = ref.nuevoEnlace){
								this.idu = enlace.idu_1;
								this.ftx = "";
								this.ptx = "";
								this.numero_serie = "";
								this.tipo_odu = {
									id: "",
									marca: "",
									modelo:"",
									part_number: "",
									sub_banda: "",
									frec_min: 0,
									frec_max: 0,
									banda: "",
									peso: 0,
									tipo: "",
									hi_low: ""
								};
							},
							nuevaOduFE: function(enlace = ref.nuevoEnlace){
								this.idu = enlace.idu_2;
								this.ftx = "";
								this.ptx = "";
								this.numero_serie = "";
								this.tipo_odu = {
									id: "",
									marca: "",
									modelo:"",
									part_number: "",
									sub_banda: "",
									frec_min: 0,
									frec_max: 0,
									banda: "",
									peso: 0,
									tipo: "",
									hi_low: ""
								};
							},
							nuevaAntenaNE: function(enlace = ref.nuevoEnlace){
								this.idu = enlace.idu_1;
								this.numero_serie = "";
								this.altura = "";
								this.azimuth = "";								
								this.tipo_antena_mw = {
									id: "",
									marca: "",
									modelo: "",
									diametro: 0,
									banda: 0,
									peso: 0
								};
                               
							},
							nuevaAntenaFE: function(enlace = ref.nuevoEnlace){
								this.idu = enlace.idu_2;
								this.numero_serie = "";
								this.altura = "";
								this.azimuth = "";								
								this.tipo_antena_mw = {
									id: "",
									marca: "",
									modelo: "",
									diametro: 0,
									banda: 0,
									peso: 0
								};
							},
							disabled: function(){
								var disabled = false;
								
								if(!this.odus_enlaces_fe.length || !this.odus_enlaces_ne.length)
								{
									disabled = true;
								}
								else
								{
									this.odus_enlaces_fe.forEach(function(odu,indy,objetoy){
										if(!objetoy[0].tipo_odu.id || !odu.numero_serie || !objetoy[0].ftx)
										{
											disabled = true;
										}
									});
									
									this.odus_enlaces_ne.forEach(function(odu,indy,objetoy){
										if(!objetoy[0].tipo_odu.id || !odu.numero_serie || !objetoy[0].ftx)
										{
											disabled = true;
										}
									});
								}
								
								if((this.modulacion_adaptativa && (!this.modulacion_am_numero || !this.modulacion_am_letras || !this.capacidad_am)) || (this.configuracion_2 != "0" && this.configuracion_2 != "1"))
								{
									disabled = true;
								}
								
								return disabled;
							}
						};
                  
					}
					else
					{
						
					}
                  //-----------------------
                   }


				}).error(function(err){
					
				});
			}
			
			ref.nuevaOduNE = function(nuevoEnlace){
				nuevoEnlace.odus_enlaces_ne.push(new ref.nuevoEnlace.nuevaOduNE());
			}
			ref.nuevaOduFE = function(nuevoEnlace){
				nuevoEnlace.odus_enlaces_fe.push(new ref.nuevoEnlace.nuevaOduFE());
			}
			
			ref.eliminarNuevaOduNE = function(index){
				ref.nuevoEnlace.odus_enlaces_ne.splice(index,1);
			}
			
			ref.eliminarNuevaOduFE = function(index){
				ref.nuevoEnlace.odus_enlaces_fe.splice(index,1);
			}
			
			ref.nuevaAntenaNE = function(nuevoEnlace){
				nuevoEnlace.antenas_mw_enlaces_ne.push(new ref.nuevoEnlace.nuevaAntenaNE());
			}
			ref.nuevaAntenaFE = function(nuevoEnlace){
				nuevoEnlace.antenas_mw_enlaces_fe.push(new ref.nuevoEnlace.nuevaAntenaFE());
			}
			
			ref.eliminarNuevaAntenaNE = function(index){
				ref.nuevoEnlace.antenas_mw_enlaces_ne.splice(index,1);
			}
			
			ref.eliminarNuevaAntenaFE = function(index){
				ref.nuevoEnlace.antenas_mw_enlaces_fe.splice(index,1);
			}
			
			ref.editar_enlace = function(enlace){
				ref.editarEnlace = enlace;
				ref.editarEnlace["ancho_banda"] = Number(ref.editarEnlace["ancho_banda"]);
				ref.editarEnlace["capacidad"] = Number(ref.editarEnlace["capacidad"]);
				ref.editarEnlace["capacidad_am"] = Number(ref.editarEnlace["capacidad_am"]);
				ref.editarEnlace["banda"] = Number(ref.editarEnlace["banda"]);
				$("#editarEnlace").bPopup();
			}
			
			ref.grabarEnlace = function(enlace){
				
				if(!ref.editarEnlace.modulacion_adaptativa)
				{
					ref.editarEnlace.modulacion_adaptativa = 0;
					ref.editarEnlace.modulacion_am = "N/A";
					ref.editarEnlace.capacidad_am = 0;
				}
				else
				{
					ref.editarEnlace.modulacion_adaptativa = 1;
					ref.editarEnlace.modulacion_am = ref.editarEnlace.modulacion_am_editar.numero+ref.editarEnlace.modulacion_am_editar.letras;
					ref.editarEnlace.capacidad_am = ref.editarEnlace.capacidad_am;
				}
				
				var data = "configuracion_part_1="+ref.editarEnlace.configuracion_editar.part_1
						+"&configuracion_part_2="+ref.editarEnlace.configuracion_editar.part_2
						+"&configuracion_part_3="+ref.editarEnlace.configuracion_editar.part_3
						+"&configuracion_part_4="+ref.editarEnlace.configuracion_editar.part_4
						+"&polaridad="+ref.editarEnlace.polaridad
						+"&ancho_banda="+ref.editarEnlace.ancho_banda
						+"&modulacion="+ref.editarEnlace.modulacion_editar.numero+ref.editarEnlace.modulacion_editar.letras
						+"&capacidad="+ref.editarEnlace.capacidad
						+"&modulacion_adaptativa="+ref.editarEnlace.modulacion_adaptativa
						+"&modulacion_am="+ref.editarEnlace.modulacion_am
						+"&capacidad_am="+ref.editarEnlace.capacidad_am
						+"&banda="+ref.editarEnlace.banda;						
				//console.log(data);
				$http({
					url:"<?php echo base_url("enlaces/enlace");?>/"+enlace.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(!resp.error)
					{
						enlace.editar = false;
						listarEnlaces();
						ref.listarEnlace(ref.enlace);
						$("#editarEnlace").bPopup().close();
					}
					else
					{
						
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEnlace = function(){
				ref.editarEnlace = {};
				$("#editarEnlace").bPopup().close();
				listarEnlaces();
				ref.listarEnlace(ref.enlace);
			}

			ref.previaEliminando = function(info) {
				ref.previaEliminar_Enlace = info;

				if (info.odus_enlaces.length || info.antenas_mw_enlaces.length) {
					$("#previaEliminar").bPopup({
						onOpen: function(){
							document.getElementById("previaEliminar").className = "popup active";
						},
						onClose: function(){
							document.getElementById("previaEliminar").className = "popup";
						}
					});
				} else {
					ref.eliminandoEnlace(info);
				}
			}
			
			ref.eliminandoEnlace = function(info){
				ref.eliminar_Enlace = info;
				$("#eliminarEnlace").bPopup();

				if($("#previaEliminar").hasClass("popup active")) {
					$("#previaEliminar").bPopup().close();
				}
			}
			
			ref.eliminarEnlace = function(){
				var data = "comentario="+ref.eliminar_Enlace.comentario;

				$http({
					url:"<?php echo base_url("enlaces/enlace");?>"+"/"+ref.eliminar_Enlace.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded','X-Requested-With' : 'XMLHttpRequest'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.enlaces = [];
						ref.enlace = {};
						listarEnlaces();
						ref.eliminar_Enlace = {};
						$("#eliminarEnlace").bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEliminacion = function(){
				$("#eliminarEnlace").bPopup().close();
				ref.eliminar_Enlace = {};
			}
			
			ref.reenrutarEnlace = function(enlace){
				enlace.reenrutar = true;
			}
			
			ref.grabarReEnlace = function(enlace){
				
				var data = "idu_1="+enlace.idu_1.id
						+"&idu_2="+enlace.idu_2.id;
				
				//console.log(data);
				$http({
					url:"<?php echo base_url("enlaces/enlace");?>/"+enlace.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(!resp.error)
					{
						enlace.reenrutar = false;
						listarEnlaces();
						/*ref.enlace = [];*/
					}
					else
					{
						
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarReEnlace = function(enlace){
				enlace.reenrutar = false;
				listarEnlaces();
			}
			
			ref.nuevo_odu = function(){
				$("#nuevoOdu").bPopup();
			}
			
			ref.agregarOdu = function(odu){				
				var data = "tipo_odu="+odu.tipo_odu.id
						+"&numero_serie="+odu.numero_serie
						+"&ftx="+odu.ftx
						+"&ptx="+odu.ptx
						+"&enlace="+odu.enlace
						+"&idu="+odu.idu;				
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("odus/odu/sitio");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(!resp.error)
					{
						ref.nuevoOdu = {};
						listarEnlaces();
						ref.listarEnlace(ref.enlace);
						$("#nuevoOdu").bPopup().close();
					}
					else
					{
						
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarCreacionOdu = function(){
				ref.nuevoOdu = {};
				$("#nuevoOdu").bPopup().close();
			}
			
			ref.editar_odu = function(odu){
				ref.editarOdu = odu;
				ref.editarOdu["ftx"] = Number(ref.editarOdu["ftx"]);
				ref.editarOdu["ptx"] = Number(ref.editarOdu["ptx"]);
				$('#editarOdu').bPopup();
			}
			
			ref.grabarOdu = function(odu){
				
				var data = "tipo_odu="+ref.editarOdu.tipo_odu.id
						+"&numero_serie="+ref.editarOdu.numero_serie
						+"&ftx="+ref.editarOdu.ftx
						+"&ptx="+ref.editarOdu.ptx;
				
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("odus/odu");?>/"+odu.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(!resp.error)
					{
						ref.editarOdu = {};
						listarEnlaces();
						ref.listarEnlace(ref.enlace);
						$("#editarOdu").bPopup().close();
					}
					else
					{
						
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEdicionOdu = function(){
				ref.editarOdu = {};
				$("#editarOdu").bPopup().close();
				listarEnlaces();
				ref.listarEnlace(ref.enlace);
			}
			
			ref.eliminandoOdu = function(info){
				ref.eliminar_Odu = info;
				$("#eliminarOdu").bPopup();
			}
			
			ref.eliminarOdu = function(){
				var data = "comentario="+ref.eliminar_Odu.comentario;

				$http({
					url:"<?php echo base_url("odus/odu");?>"+"/"+ref.eliminar_Odu.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded','X-Requested-With' : 'XMLHttpRequest'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.enlaces = [];
						listarEnlaces();
						ref.listarEnlace(ref.enlace);
						ref.eliminar_Odu = {};
						$('#eliminarOdu').bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEliminacionOdu = function(){
				$("#eliminarOdu").bPopup().close();
				ref.eliminar_Odu = {};
			}
			
			ref.nuevo_antena = function(){
				$("#nuevoAntena").bPopup();
			}

		   ref.agregarAntena = function(antena){
				var idsitio = $("#idsitio").val();
				var data = "tipo_antena_mw="+ref.nuevoAntena.tipo_antena_mw.id
						+"&numero_serie="+ref.nuevoAntena.numero_serie
						+"&altura="+ref.nuevoAntena.altura
						+"&azimuth="+ref.nuevoAntena.azimuth
						+"&enlace="+ref.nuevoAntena.enlace
						+"&idu="+ref.nuevoAntena.idu
				        +"&estado_aprobacion=1"
				        +"&idsitio="+idsitio;
				//console.log(data);
				//estado_aprobacion 0=antenas microondas antiguas instaladas
				//1=antenas microondas pendiente
				//2=antenas microondas reservadas
				//3=antenas microondas instaladas nuevas
				$http({
					url:"<?php echo base_url("antenas_mw/antena_mw/enlace");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
                   if(resp>0){
						alert("El espacio  no esta disponible elija otro azimuth ");
					}else{
							if(!resp.error)
							{
								ref.nuevoAntena = {};
								listarEnlaces();
								ref.listarEnlace(ref.enlace);
								$("#nuevoAntena").bPopup().close();
							}
							else
							{
								
							}
					}
					
				}).error(function(err){

				});
			}
			
			ref.cancelarCreacionAntena = function(){
				ref.nuevoAntena = {};
				$("#nuevoAntena").bPopup().close();
			}
			
			ref.editar_antena = function(antena){
				ref.editarAntena = antena;
				ref.editarAntena["altura"] = Number(ref.editarAntena["altura"]);
				ref.editarAntena["azimuth"] = Number(ref.editarAntena["azimuth"]);
				$("#editarAntena").bPopup();
			}
			
			ref.grabarAntena = function(antena){
				
		var data = "tipo_antena_mw="+ref.editarAntena.tipo_antena_mw.id
						+"&numero_serie="+ref.editarAntena.numero_serie
						+"&altura="+ref.editarAntena.altura
						+"&azimuth="+ref.editarAntena.azimuth
						+"&idsitio=<?php echo $sitio;?>";
				
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("antenas_mw/antena_mw");?>/"+antena.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){

                   if(resp>0){
						alert("El Espacio  No esta disponible Elija otra altura o azimuth ");
					}else{
							if(!resp.error)
							{
								ref.editarAntena = {};
								listarEnlaces();
								ref.listarEnlace(ref.enlace);
								$("#editarAntena").bPopup().close();
							}
							else
							{
								
							}
					}

				}).error(function(err){

				});
			}
			
			ref.cancelarEdicionAntena = function(){
				ref.editarAntena = {};
				$("#editarAntena").bPopup().close();
				listarEnlaces();
				ref.listarEnlace(ref.enlace);
			}

			ref.almacenandoAntenaMicroondas = function(info){
				ref.almacenar_AntenaMicroondas = info;
				ref.almacenar_AntenaMicroondas.tipo_almacen = 1;
				$("#almacenarAntenaMicroondas").bPopup();
			}

			ref.almacenarAntenaMicroondas = function(antena){
				var data = "tipo_antena_mw="+antena.tipo_antena_mw.id
						+"&numero_serie="+antena.numero_serie
						+"&estado="+antena.estado
						+"&proveniente_sitio="+antena.sitio
						+"&tipo_almacen="+antena.tipo_almacen
						+"&almacen="+((antena.tipo_almacen == 1)? antena.almacen : antena.a_sitio)
						+"&comentario="+antena.comentario;
				console.log(data);

				$http({
					url:"<?php echo base_url("antenas_mw/antena_mw/almacenar");?>/"+antena.id,
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#almacenarAntenaMicroondas").bPopup().close();
					ref.antenasMicroondas = [];
					listarAntenas();
					ref.almacenar_AntenaMicroondas = {};
				}).error(function(err){

				});
			}

			ref.cancelarAlmacenamientoMicroondas = function(){
				$("#almacenarAntenaMicroondas").bPopup().close();
				ref.almacenar_AntenaMicroondas = {};
			}

			ref.microondasOpcionSitio = function(){
				ref.almacenar_AntenaMicroondas.tipo_almacen = 2;
			}

			ref.microondasOpcionAlmacen = function(){
				ref.almacenar_AntenaMicroondas.tipo_almacen = 1;
			}
			
			ref.eliminandoAntena = function(info){
				ref.eliminar_Antena = info;
				$("#eliminarAntena").bPopup();
			}
			
			ref.eliminarAntena = function(){
				var data = "comentario="+ref.eliminar_Antena.comentario;

				$http({
					url:"<?php echo base_url("antenas_mw/antena_mw");?>"+"/"+ref.eliminar_Antena.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded','X-Requested-With' : 'XMLHttpRequest'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.enlaces = [];
						listarEnlaces();
						ref.listarEnlace(ref.enlace);
						ref.eliminar_Antena = {};
						$('#eliminarAntena').bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEliminacionAntena = function(){
				$("#eliminarAntena").bPopup().close();
				ref.eliminar_Antena = {};
			}

			ref.almacenandoElementos = function(enlace) {
				ref.almacenar_Elementos = enlace;

				ref.almacenar_Elementos.odus_enlaces.forEach(function(odu) {
					odu.check = false;
				});

				ref.almacenar_Elementos.antenas_mw_enlaces.forEach(function(antena) {
					antena.check = false;
				});

				ref.almacenar_Elementos.tipo_almacen = 1;
				$("#almacenarElementos").bPopup();

				if($("#previaEliminar").hasClass("popup active")) {
					$("#previaEliminar").bPopup().close();
				}
			}

			ref.almacenarElementos = function(elementos) {
				elementos.odus_enlaces.forEach(function(odu) {
					if(odu.check) {
						ref.almacenarODUs.push(odu);
					}
				});

				elementos.antenas_mw_enlaces.forEach(function(antena) {
					if(antena.check) {
						ref.almacenarAntenas.push(antena);
					}
				});

				var data = "odus="+angular.toJson(ref.almacenarODUs)
							+"&antenas_mw="+angular.toJson(ref.almacenarAntenas)
							+"&estado="+elementos.estado
							+"&proveniente_sitio="+elementos.sitio
							+"&tipo_almacen="+elementos.tipo_almacen
							+"&almacen="+((elementos.tipo_almacen == 1)? elementos.almacen : elementos.a_sitio)
							+"&comentario="+elementos.comentario;
							console.log(data);

				$http({
					url: "<?php echo base_url("elementos_mw/almacenar"); ?>",
					method: "POST",
					data: data,
					hearders: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function successCallback(response) {
					listarEnlaces();
					ref.almacenar_Elementos = {};
					ref.almacenarODUs = [];
					ref.almacenarAntenas = [];
					$("#almacenarElementos").bPopup().close();
				}, function errorCallback(response) {

				});
			}

			ref.cancelarAlmacenamiento = function() {
				ref.almacenar_Elementos = {};
				ref.almacenarODUs = [];
				ref.almacenarAntenas = [];
				$("#almacenarElementos").bPopup().close();
			}

			ref.disabledAlmacenamiento = function() {
				var dis = false;

				if(Object.keys(ref.almacenar_Elementos).length) {
					var elementos = ref.almacenar_Elementos.odus_enlaces.concat(ref.almacenar_Elementos.antenas_mw_enlaces);

					elementos.forEach(function(elemento) {
						dis = dis || elemento.check;
					});
				}

				if(!ref.almacenar_Elementos.estado || !ref.almacenar_Elementos.comentario || (ref.almacenar_Elementos.tipo_almacen == 1 && !ref.almacenar_Elementos.almacen) || (ref.almacenar_Elementos.tipo_almacen == 2 && !ref.almacenar_Elementos.a_sitio)) {
					dis = false;
				}

				return dis;
			}

			ref.opcionSitio = function(){
				ref.almacenar_Elementos.tipo_almacen = 2;
			}

			ref.opcionAlmacen = function(){
				ref.almacenar_Elementos.tipo_almacen = 1;
			}
			
			ref.filtroEquiposNE = function(a){
				var r = true;
				
				if(a.idu != ref.enlaces[ref.enlace.index].idu_1.id)
				{
					r = false;
				}
				
				return r;
			}
			
			ref.filtroEquiposFE = function(a){
				var r = true;
				
				if(a.idu != ref.enlaces[ref.enlace.index].idu_2.id)
				{
					r = false;
				}
				
				return r;
			}
			
			ref.filtroFrecuencia = function(a){
				var r = true;
				
				if(ref.nuevoOdu.tipo_odu)
				{
					if(a.marca != ref.nuevoOdu.tipo_odu.marca || a.modelo != ref.nuevoOdu.tipo_odu.modelo || a.tipo != ref.nuevoOdu.tipo_odu.tipo || a.banda != ref.nuevoOdu.tipo_odu.banda)
					{
						r = false;
					}
				}
				
				return r;
			}
			
			ref.filtroFrecuenciaE = function(a){
				var r = true;
				
				if(Object.keys(ref.editarOdu).length !== 0)
				{
					if(a.marca != ref.editarOdu.tipo_odu.marca || a.modelo != ref.editarOdu.tipo_odu.modelo || a.tipo != ref.editarOdu.tipo_odu.tipo || a.banda != ref.editarOdu.tipo_odu.banda)
					{
						r = false;
					}
				}
				
				return r;
			}
			
			ref.filtroNuevaOdu = function(a){
				var r = true;
				
				if(a.id != ref.nuevoOdu.enlace)
				{
					r = false;
				}
				
				return r;
			}
			
			ref.filtroNuevaAntena = function(a){
				var r = true;
				
				if(a.id != ref.nuevoAntena.enlace)
				{
					r = false;
				}
				
				return r;
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			borrarPropiedades = function(objeto, propiedad) {
				var campos = Object.keys(objeto);

				campos.forEach(function(campo, ind, obj) {
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo], propiedad);
					}
				});

				return objeto;
			}			
// version 3.0

            VerificarFactorUso = function(){
                resetForm();
				$(".alert.alert-danger.text-center.alerta_de_torre").css('display','none');
				var  e = $("#factor_uso_torre_unico").val();   	
				deshabilita_segun_factor_uso(e);				
			}

		    deshabilita_segun_factor_uso = function(e){

				if(e > 1){
	             //alert(e + "bloqueado");
	            $(".alert.alert-danger.text-center.alerta_de_torre").css('display','block'); 
	            $('.verifica_factor_uso').find("input,select,button").attr("disabled", "disabled");
	            }else{
	        	// alert(e + "Des bloqueado");
	        	$(".alert.alert-danger.text-center.alerta_de_torre").css('display','none'); 
	        	$('.verifica_factor_uso').find("input,select").removeAttr("disabled");
	              }
           }

          Fnbuscarsitio = function(){ 
           var idustext = $('#elegir-idu option:selected').text();
           var idu = idustext.split('sitio:');           
           $("#idsitio").val(idu[1]); 
           buscarFactorUsoTorreSitio(idu[1]);

          }


           FnbuscarsitioOdu = function(){ 
           var odustext = $('#elegir-Odu option:selected').text();
           var odu = odustext.split('sitio:'); 
           //alert(odu[1]);          
           $("#idsitioodu").val(odu[1]); 
           buscarFactorUsoTorreSitio(odu[1]);

          }



           buscarsitio = function(ididu){
             var idu = $("#"+ididu+" option:selected").val();
                $http({
					url:"<?php echo base_url("idus/obtener_sitio_con_idu");?>"+"/"+idu,
					method:"GET"
				}).success(function(data){								
				$("#idsitio-"+ididu).val(data.id);				
                buscarFactorUsoTorreSitio(data.id);
				}).error(function(err){
				});
			}


           buscarFactorUsoTorreSitio = function(idsitio){           
                $http({
					url:"<?php echo base_url("status_site/get_factor_uso_sitio");?>"+"/"+idsitio,
					method:"GET"
				}).success(function(data){				
				    //console.log(data.factor_uso_torre);
				    deshabilita_segun_factor_uso(data.factor_uso_torre);
				}).error(function(err){
				});
			}
          
          resetForm = function(){           	
           	 $('#FormNuevaAntenaMicroondas')[0].reset();
           	 $('#FormNuevaAntenaMicroondasEnlace')[0].reset();
           	 $('#FormNuevaAntenaMicroondasOdu')[0].reset();           	 
           	 refreshSelectize();           	 
          }

          refreshSelectize = function()
			{
				 
              var selectize = $("#selectize-sitio")[0].selectize;
              selectize.clear();
              // selectize.clearOptions();
		  }

  // version 3.0
			listarEnlaces();
			listarSitios();
			listarAlmacenes();
			listarIDUsNE();
			listarTipoOdu();
			listarTipoAntena();
}]);
</script>
</body>
</html>