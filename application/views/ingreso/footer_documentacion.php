<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.procesando = false;
			ref.cargando = true;
			ref.file = [];
			ref.renombrar = "";

			var sitio = "<?php echo $infoSitio[0]["nombre_completo"] ?>";
			
			var nodeGlobal = {};

			function parsingJSON(arrayObj,property)
			{
				arrayObj.forEach(function(li,indx,objx){
					objx[indx][property] = JSON.parse(objx[indx][property]);

					objx[indx].children = parsingJSON(objx[indx].children,property);
				});

				return arrayObj;
			}

			listarFtp = function(){
				$http({
					url:"<?php echo base_url("index.php/sftp_controller/listar");?>",
					data:"dir="+sitio,
					method:"POST",
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.length)
					{
						$("#docs").jstree('destroy');
						
						data = parsingJSON(data,"li_attr");

						var json = {
							core: {
								data: data
							},
							plugins: ["contextmenu"],
							contextmenu:{
								items: function(node){
									if( node.li_attr.class == "menuDir" )
									{
										var items = {
											uploadItem: {
												icon: "fa fa-upload",
												label: "Subir archivo",
												action: function (){
													$("#subirArchivo").bPopup({
														modalClose: false
													});
													nodeGlobal = node;
												}
											}
										}
									}
									else
									{
										var items = {
											downloadItem: {
												icon: "fa fa-download",
												label: "Descargar",
												action: function (){
													var iframe = document.createElement("iframe")
														form = document.createElement("form");

													iframe.id = "iframeEliminar"
													iframe.name = "iframeEliminar";
													iframe.height = "0";
													iframe.width = "0";
													iframe.style = "border:none";

													form.action = "<?php echo base_url("index.php/sftp_controller/download");?>";
													form.method = "POST";
													form.target = "iframeEliminar";

													var input = document.createElement("input");
													input.name = "dir";
													input.value = node.li_attr.direccionftp;

													document.body.appendChild(iframe);
													form.appendChild(input);

													form.submit();
												}
											},
											renameItem: {
												icon: "fa fa-edit",
												label: "Renombrar",
												action: function(){
													$("#renombrarArchivo").bPopup({
														modalClose: false
													});
													nodeGlobal = node;
												}
											},
											deleteItem: {
												icon: "fa fa-close",
												label: "Eliminar",
												action: function (){
													$("#eliminarArchivo").bPopup({
														modalClose: false
													});
													nodeGlobal = node;
												}
											}
										}
									}

									return items;
								}
							}
						};
						
						$("#docs").jstree(json);
						ref.cargando = false;
						ref.procesando = false;
						/*console.log(data);*/
						$("#eliminarArchivo").bPopup().close();
						$("#subirArchivo").bPopup().close();
						$("#renombrarArchivo").bPopup().close();
					}
					else
					{
						ref.cargando = false;
						ref.procesando = false;
					}
				}).error(function(err){
					ref.cargando = false;
					ref.procesando = false;
				});
			}
			
			ref.eliminarArchivo = function(){
				ref.procesando = true;
				$http({
					url:"<?php echo base_url("index.php/sftp_controller/delete");?>",
					method:"POST",
					data:"dir="+nodeGlobal.li_attr.direccionftp,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					listarFtp();
				}).error(function(err){
					ref.procesando = false;
				});
			}
			
			ref.cancelarEliminacion = function(){
				$("#eliminarArchivo").bPopup().close();
			}
			
			ref.subirArchivo = function(){
				ref.inputFile = document.getElementById("file").files;

				if(ref.inputFile.length > 0)
				{
					ref.procesando = true;
					ref.archivo = ref.inputFile[0];
					var fdata = new FormData();
					fdata.append('file',ref.archivo);
					fdata.append('name',ref.archivo.name);
					fdata.append('dir',nodeGlobal.li_attr.direccionftp);

					$.ajax({
						url:"<?php echo base_url() ?>"+"index.php/sftp_controller/upload",
						method:'POST',
						data:fdata,
						contentType:false,
						processData:false,
						dataType:"json"
					})
					.complete(function(data){
						listarFtp();
						nodeGlobal = {};
					});
				}
			}
			
			ref.cancelarSubida = function() {
				$("#subirArchivo").bPopup().close();
			}
			
			ref.renombrarArchivo = function(){
				ref.procesando = true;
				ref.archivo = document.getElementById("nombre").value;

				var fdata = new FormData();
				fdata.append('value',ref.archivo);
				fdata.append('dir',nodeGlobal.li_attr.direccionftp)
				
				$http({
					url:"<?php echo base_url("index.php/sftp_controller/rename");?>",
					method:"POST",
					data:"dir="+nodeGlobal.li_attr.direccionftp+"&value="+ref.archivo,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					nodeGlobal = {};
					listarFtp();
				}).error(function(err){
					ref.procesando = false;
				});
			}
			
			ref.cancelarRenombre = function(){
				$("#renombrarArchivo").bPopup().close();
			}
			
			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};

			listarFtp();
		}]);
</script>
</body>
</html>  