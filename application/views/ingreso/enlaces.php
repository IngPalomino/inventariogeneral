<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-8">
			<div class="panel panel-primary" ng-if="ctrl.enlaces.length">
				<div class="panel-heading text-center">
					Enlaces del sitio <?php echo $infoSitio[0]["nombre_completo"] ?>
					<input type="hidden" name="factor_uso_torre_unico" id="factor_uso_torre_unico" value="<?php echo $infoSitio[0]["factor_uso_torre"] ?>" > 
				</div>
				<div class="panel-body" style="max-height:150px;overflow-y:scroll;">
					<table class="table table-hover table-condensed">
						<thead>
							<tr>
								<th>IDU</th>
								<th>Sitio Far End</th>
								<th>IDU Far End</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							<tr style="cursor:pointer;" ng-repeat="enlace in ctrl.enlaces" ng-class="{success:enlace.id == ctrl.enlace.id}" ng-click="ctrl.listarEnlace(enlace); enlace.index = $index">
								<td>
									<span ng-show="!enlace.reenrutar">{{enlace.idu_1.ne_id}}</span>
									<span ng-hide="!enlace.reenrutar">
										<input value="{{enlace.idu_1.id}}" class="form-control" type="text" ng-model="enlace.idu_1.ne_id" style="max-width:80px;" readonly/>
									</span>
								</td>
								<td>
									<span ng-show="!enlace.reenrutar">{{enlace.idu_2.sitio.nombre_completo}}</span>
									<span ng-hide="!enlace.reenrutar">
										<label style="width:100%">
											<selectize config="ctrl.config" options='ctrl.sitios' ng-model="enlace.idu_2.sitio.id" ng-change="ctrl.listarIDUs(enlace.idu_2.sitio.id); enlace.idu_2.id = '';"></selectize>
										</label>
									</span>
								</td>
								<td>
									<span ng-show="!enlace.reenrutar">{{enlace.idu_2.ne_id}}</span>
									<span ng-hide="!enlace.reenrutar">
										<select class="form-control" ng-model="enlace.idu_2.id">
											<option value="">Elegir IDU</option>
											<option ng-repeat="idu in ctrl.idus" value="{{idu.id}}">{{idu.ne_id}}</option>
										</select>
									</span>
								</td>
								<td>
									<button class="btn btn-info btn-xs" ng-click="ctrl.almacenandoElementos(enlace)" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?> || !enlace.odus_enlaces.length || !enlace.antenas_mw_enlaces.length"><i class="fa fa-archive"></i> Almacenar</button>
									<button class="btn btn-danger btn-xs" ng-click="ctrl.previaEliminando(enlace)" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
									<button class="btn btn-primary btn-xs" ng-click="ctrl.listarEnlace(enlace); enlace.index = $index"><i class="fa fa-plus"></i> Más info</button>
									<!--<div class="dropdown">
										<button class="btn btn-warning btn-xs dropdown-toggle" type="button" id="dropdownEnlaces" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">
											<i class="fa fa-cogs"></i> Acciones
											<span class="caret"></span>
										</button>
										<ul class="dropdown-menu" aria-labelledby="dropdownEnlaces">
											<li><a href ng-click="ctrl.almacenandoElementos(enlace)"><i class="fa fa-archive"></i> Almacenar Elementos</a></li>
											<li role="separator" class="divider"></li>
											<li><a href ng-click="ctrl.eliminandoEnlace(enlace)"><i class="fa fa-close"></i> Eliminar</a></li>
										</ul>
										<button class="btn btn-primary btn-xs" ng-click="ctrl.listarEnlace(enlace); enlace.index = $index"><i class="fa fa-plus"></i> Más info</button>
									</div>-->
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="well" ng-if="!ctrl.cargando && !ctrl.enlaces.length">
				No hay enlaces en este sitio.
			</div>
		</div>		
		<div class="col-xs-4">			
			<button class="btn btn-default" onclick=";VerificarFactorUso();"    style="width:100%;margin-bottom:5px;margin-top:15px;" ng-click="ctrl.nuevo_enlace()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir Enlace</button>
			<button class="btn btn-default"  onclick=";VerificarFactorUso();"  style="width:100%;margin-bottom:5px;margin-top:5px;" ng-click="ctrl.nuevo_odu()" ng-if="ctrl.enlaces.length" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir ODU</button>
			<button onclick=";VerificarFactorUso();" class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:5px;" ng-click="ctrl.nuevo_antena()" ng-if="ctrl.enlaces.length" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir Antena</button>
		</div>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-if="!ctrl.cargando && ctrl.enlaces.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Información del Enlace <span ng-if="ctrl.enlace.len">{{ctrl.enlaces[ctrl.enlace.index].idu_1.idu_name}} to {{ctrl.enlaces[ctrl.enlace.index].idu_2.idu_name}}</span>
				</div>
				<div class="panel-body" ng-if="ctrl.enlace.len" style="max-height:400px;overflow-y:scroll;">
					<div class="well" style="font-size:8pt;">
						<span ng-if="!ctrl.enlaces[ctrl.enlace.index].logs_enlaces.length || ctrl.enlaces[ctrl.enlace.index].logs_enlaces[0].evento.tipo != 'INSERT'">Creado: {{ctrl.enlaces[ctrl.enlace.index].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>
						<span ng-if="ctrl.enlaces[ctrl.enlace.index].logs_enlaces[0].evento.tipo == 'INSERT'">Creado: {{ctrl.enlaces[ctrl.enlace.index].logs_enlaces[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{ctrl.enlaces[ctrl.enlace.index].logs_enlaces[0].usuario.nombres}} {{ctrl.enlaces[ctrl.enlace.index].logs_enlaces[0].usuario.apellido_paterno}}</span>
						<br>
						<span ng-if="ctrl.enlaces[ctrl.enlace.index].logs_enlaces[1].evento.tipo == 'UPDATE'">Actualizado: {{ctrl.enlaces[ctrl.enlace.index].logs_enlaces[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{ctrl.enlaces[ctrl.enlace.index].logs_enlaces[1].usuario.nombres}} {{ctrl.enlaces[ctrl.enlace.index].logs_enlaces[1].usuario.apellido_paterno}}</span>
					</div>
					<table class="table table-condensed" style="table-layout:fixed;">
						<thead>
							<tr>
								<th class="success" colspan="2" style="text-align:center;"><strong>Near End</strong></th>
								<th class="danger" colspan="2" style="text-align:center;"><strong>Far End</strong></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="2">
									<table class="table table-condensed" ng-repeat="odu in ctrl.enlaces[ctrl.enlace.index].odus_enlaces | filter:ctrl.filtroEquiposNE | filter:{eliminado:0}">
										<thead>
											<tr>
												<th colspan="2" style="text-align:center;">ODU {{$index + 1}}</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="info">Tipo</td>
												<td class="default">{{odu.tipo_odu.tipo}}</td>
											</tr>
											<tr>
												<td class="info">N° de serie</td>
												<td class="default">{{odu.numero_serie}}</td>
											</tr>
											<tr>
												<td class="info">Ftx</td>
												<td class="default">{{odu.ftx}} MHz</td>
											</tr>
											<tr>
												<td class="info">Ptx</td>
												<td class="default">{{odu.ptx}} dBm</td>
											</tr>
										</tbody>
										<tfoot>
											<tr class="text-center">
												<td colspan="2">
													<button class="btn btn-warning btn-xs" ng-click="ctrl.editar_odu(odu)" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
													<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminandoOdu(odu)" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
												</td>
											</tr>
										</tfoot>
									</table>
									<table class="table table-condensed" ng-repeat="antena in ctrl.enlaces[ctrl.enlace.index].antenas_mw_enlaces | filter:ctrl.filtroEquiposNE | filter:{eliminado:0}">
										<thead>
											<tr>
												<th colspan="2" style="text-align:center;">Antena {{$index + 1}}</th>
											</tr>
										</thead>
										<tbody>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Marca</td>
												<td class="default">{{antena.tipo_antena_mw.marca}}</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Modelo</td>
												<td class="default">{{antena.tipo_antena_mw.modelo}}</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">N° de serie</td>
												<td class="default">{{antena.numero_serie}}</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Altura</td>
												<td class="default">{{antena.altura}} m</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Azimuth</td>
												<td class="default">{{antena.azimuth}}°</td>
											</tr>
										</tbody>
										<tfoot>
											<tr class="text-center">
												<td colspan="2">
													<button class="btn btn-warning btn-xs" ng-click="ctrl.editar_antena(antena)" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
													<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminandoAntena(antena)" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
												</td>
											</tr>
										</tfoot>
									</table>
								</td>
								<td colspan="2">
									<table class="table table-condensed" ng-repeat="odu in ctrl.enlaces[ctrl.enlace.index].odus_enlaces | filter:ctrl.filtroEquiposFE | filter:{eliminado:0}">
										<thead>
											<tr>
												<th colspan="2" style="text-align:center;">ODU {{$index + 1}}</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="info">Tipo</td>
												<td class="default">{{odu.tipo_odu.tipo}}</td>
											</tr>
											<tr>
												<td class="info">N° de serie</td>
												<td class="default">{{odu.numero_serie}}</td>
											</tr>
											<tr>
												<td class="info">Ftx</td>
												<td class="default">{{odu.ftx}} MHz</td>
											</tr>
											<tr>
												<td class="info">Ptx</td>
												<td class="default">{{odu.ptx}} dBm</td>
											</tr>
										</tbody>
										<tfoot>
											<tr class="text-center">
												<td colspan="2">
													<button class="btn btn-warning btn-xs" ng-click="ctrl.editar_odu(odu)" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
													<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminandoOdu(odu)" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
												</td>
											</tr>
										</tfoot>
									</table>
									<table class="table table-condensed" ng-repeat="antena in ctrl.enlaces[ctrl.enlace.index].antenas_mw_enlaces | filter:ctrl.filtroEquiposFE | filter:{eliminado:0}">
										<thead>
											<tr>
												<th colspan="2" style="text-align:center;">Antena {{$index + 1}}</th>
											</tr>
										</thead>
										<tbody>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Marca</td>
												<td class="default">{{antena.tipo_antena_mw.marca}}</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Modelo</td>
												<td class="default">{{antena.tipo_antena_mw.modelo}}</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">N° de Serie</td>
												<td class="default">{{antena.numero_serie}}</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Altura</td>
												<td class="default">{{antena.altura}} m</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Azimuth</td>
												<td class="default">{{antena.azimuth}}°</td>
											</tr>
										</tbody>
										<tfoot>
											<tr class="text-center">
												<td colspan="2">
													<button class="btn btn-warning btn-xs" ng-click="ctrl.editar_antena(antena)" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
													<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminandoAntena(antena)" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
												</td>
											</tr>
										</tfoot>
									</table>
								</td>
							</tr>
							<tr class="text-center info">
								<td colspan="4"><strong>Configuración del enlace</strong></td>
							</tr>
							<tr>
								<td class="info" colspan="2">Configuración</td>
								<td class="default" colspan="2">{{ctrl.enlaces[ctrl.enlace.index].configuracion}}</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Polaridad</td>
								<td class="default" colspan="2">
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].polaridad == 1">V</span>
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].polaridad == 2">H</span>
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].polaridad == 3">V+H</span>
								</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Ancho de Banda (BW)</td>
								<td class="default" colspan="2">{{ctrl.enlaces[ctrl.enlace.index].ancho_banda}} MHz</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Modulación</td>
								<td class="default" colspan="2">{{ctrl.enlaces[ctrl.enlace.index].modulacion}}</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Capacidad</td>
								<td class="default" colspan="2">{{ctrl.enlaces[ctrl.enlace.index].capacidad}} Mbit/s</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Modulación adaptativa (AM)</td>
								<td class="default" colspan="2">
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].modulacion_adaptativa == 0">NO</span>
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].modulacion_adaptativa == 1">SI</span>
								</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Modulación AM</td>
								<td class="default" colspan="2">{{ctrl.enlaces[ctrl.enlace.index].modulacion_am}}</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Capacidad AM</td>
								<td class="default" colspan="2">
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].capacidad_am != 0">{{ctrl.enlaces[ctrl.enlace.index].capacidad_am}}</span>
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].capacidad_am == 0">N/A</span>
								</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Banda</td>
								<td class="default" colspan="2">{{ctrl.enlaces[ctrl.enlace.index].banda}} GHz</td>
							</tr>
						</tbody>
						<!--<tfoot>
							<tr class="text-center">
								<td colspan="4">
									<button class="btn btn-warning btn-xs" ng-click="ctrl.editar_enlace(ctrl.enlaces[ctrl.enlace.index])">Editar</button>
								</td>
							</tr>
						</tfoot>-->
					</table>
				</div>
				<div class="panel-footer text-center" ng-if="ctrl.enlace.len">
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editar_enlace(ctrl.enlaces[ctrl.enlace.index])" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
					<!--<button class="btn btn-sm btn-success" ng-click="ctrl.grabarEnlace(ctrl.enlace)" ng-hide="!ctrl.enlace.editar">Grabar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarEnlace(ctrl.enlace)" ng-hide="!ctrl.enlace.editar">Cancelar</button>-->
				</div>
			</div>
		</div>
	</div>
</div>

<div id="nuevoEnlace" class="popup">
	<span  class="button b-close">
		<span>X</span>
	</span>

	<h3>Nuevo Enlace</h3>






<form   id="FormNuevaAntenaMicroondasEnlace" class="verifica_factor_uso">




<div id="alerta_de_torre" class="alert alert-danger text-center alerta_de_torre">
			<i class="fa fa-exclamation-triangle fa-2x"></i>Torre bloqueada para nuevas instalaciones!
</div>
		<table class="table table-condensed" style="max-height:450px;overflow-y:scroll;display:block;">
			<tbody>
				<tr>
					<td class="info">Sitio Near End</td>
					<td class="default">
						<input type="text" class="form-control input-sm" value="<?php echo $infoSitio[0]["nombre_completo"]; ?>" readonly>
					</td>
					<td class="info">Sitio Far End</td>
					<td class="default">
						<label style="width:100%;">
							<selectize id="selectize-sitio" config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.nuevoEnlace.sitio_fe" ng-change="ctrl.listarIDUsFE(ctrl.nuevoEnlace.sitio_fe); ctrl.nuevoEnlace.idu_2 = '';"></selectize>
						</label>
					</td>
				</tr>			

				<tr>
					<td class="info">IDU Near End</td>
					<td class="default">
						<select name="idu-near-end" id="idu-near-end" onchange="buscarsitio('idu-near-end')" class="form-control input-sm" ng-model="ctrl.nuevoEnlace.idu_1">
							<option value="">Elegir IDU</option>
							<option ng-repeat="idu in ctrl.idus_ne" value="{{idu.id}}">{{idu.ne_id}}<!--  sitio :{{idu.sitio}} --></option>
						</select>
						<input  type="hidden" name="idsitio-idu-near-end" id="idsitio-idu-near-end" value="">
					</td>
					<td class="info">IDU Far End</td>
					<td class="default">
						<select name="idu-far-end" id="idu-far-end" onchange="buscarsitio('idu-far-end')"   class="form-control input-sm" ng-model="ctrl.nuevoEnlace.idu_2">
							<option value="">Elegir IDU</option>
							<option ng-repeat="idu in ctrl.idus_fe" value="{{idu.id}}">{{idu.ne_id}}<!--  sitio :{{idu.sitio}} --></option>
						</select>
						<input  type="hidden" name="idsitio-idu-far-end" id="idsitio-idu-far-end" value="">
					</td>
				</tr>

				<tr class="info text-center">
					<td colspan="4">Configuración del Enlace</td>
				</tr>
				<tr>
					<td class="info">Configuración</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" min="1" max="10" class="form-control" style="max-width:60px;" ng-model="ctrl.nuevoEnlace.configuracion_1">
							<div class="input-group-addon">+</div>
							<input type="number" min="0" max="1" class="form-control" style="max-width:60px;" ng-model="ctrl.nuevoEnlace.configuracion_2">
							<div class="input-group-addon">
								<select ng-model="ctrl.nuevoEnlace.configuracion_3">
									<option value="">-</option>
									<option value="XPIC">XPIC</option>
								</select>
							</div>
							<div class="input-group-addon">
								<select ng-model="ctrl.nuevoEnlace.configuracion_4">
									<option value="">-</option>
									<option value="HSB" ng-if="!ctrl.nuevoEnlace.configuracion_3">HSB</option>
									<option value="SD">SD</option>
									<option value="FD" ng-if="!ctrl.nuevoEnlace.configuracion_3">FD</option>
								</select>
							</div>
						</div>
					</td>
					<td class="info">Polaridad</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevoEnlace.polaridad">
							<option value="">Elegir polaridad</option>
							<option value="1" ng-if="!ctrl.nuevoEnlace.configuracion_3">V</option>
							<option value="2" ng-if="!ctrl.nuevoEnlace.configuracion_3">H</option>
							<option value="3" ng-if="ctrl.nuevoEnlace.configuracion_3">V+H</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Ancho de banda (Bandwidth)</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" min="0" class="form-control" ng-model="ctrl.nuevoEnlace.ancho_banda">
							<div class="input-group-addon">MHz</div>
						</div>
					</td>
					<td class="info">Modulación</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<select class="form-control" ng-model="ctrl.nuevoEnlace.modulacion_numero">
								<option value="">Elegir</option>
								<option value="16">16</option>
								<option value="32">32</option>
								<option value="64">64</option>
								<option value="128">128</option>
								<option value="256">256</option>
								<option value="512">512</option>
								<option value="1024">1024</option>
							</select>
							<div class="input-group-addon">
								<select ng-model="ctrl.nuevoEnlace.modulacion_letras">
									<option value="">Elegir</option>
									<option value="QAM">QAM</option>
									<option value="QAMStrong">QAMStrong</option>
									<option value="QAMLight">QAMLight</option>
									<option value="QPSK">QPSK</option>
									<option value="QPSKStrong">QPSKStrong</option>
								</select>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Capacidad</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" min="0" class="form-control" ng-model="ctrl.nuevoEnlace.capacidad">
							<div class="input-group-addon">Mbit/s</div>
						</div>
					</td>-
					<td class="info">Modulación Adaptativa (AM)</td>
					<td class="default">
						<input type="checkbox" ng-model="ctrl.nuevoEnlace.modulacion_adaptativa"/>
					</td>
				</tr>
				<tr ng-if="ctrl.nuevoEnlace.modulacion_adaptativa">
					<td class="info">Modulación AM</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<select class="form-control" ng-model="ctrl.nuevoEnlace.modulacion_am_numero">
								<option value="">Elegir</option>
								<option value="16">16</option>
								<option value="32">32</option>
								<option value="64">64</option>
								<option value="128">128</option>
								<option value="256">256</option>
								<option value="512">512</option>
								<option value="1024">1024</option>
							</select>
							<div class="input-group-addon">
								<select ng-model="ctrl.nuevoEnlace.modulacion_am_letras">
									<option value="">Elegir</option>
									<option value="QAM">QAM</option>
									<option value="QPSK">QPSK</option>
								</select>
							</div>
						</div>
					</td>
					<td class="info">Capacidad AM</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" min="0" class="form-control" ng-model="ctrl.nuevoEnlace.capacidad_am">
							<div class="input-group-addon">Mbit/s</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Banda</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" min="0" class="form-control" ng-model="ctrl.nuevoEnlace.banda">
							<div class="input-group-addon">GHz</div>
						</div>
					</td>
					<td class="info"></td>
					<td class="info"></td>
				</tr>
				<tr class="text-center">
					<td class="info" colspan="4">Equipos</td>
				</tr>
				<tr>
					<td class="success text-center" colspan="2"><strong>Near End</strong></td>
					<td class="danger text-center" colspan="2"><strong>Far End</strong></td>
				</tr>
				<tr>
					<td colspan="2" style="width:550px;">
						<table class="table table-condensed">
							<tbody>
								<tr class="ng-cloak text-center" ng-repeat-start="odu in ctrl.nuevoEnlace.odus_enlaces_ne">
									<td colspan="4" class="primary">
										<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminarNuevaOduNE($index)"><i class="fa fa-times"></i></button>
										<strong>ODU {{$index + 1}}</strong>
									</td>
								</tr>
								<tr>
									<td class="info">Tipo</td>
									<td class="default">
										<select class="form-control input-sm" ng-model="ctrl.nuevoEnlace.odus_enlaces_ne[0].tipo_odu.id" ng-disabled="$index > 0">
											<option value="">Elegir tipo</option>
											<option ng-repeat="tipo in ctrl.tipo_odu" value="{{tipo.id}}">{{tipo.tipo}}</option>
										</select>
									</td>
									<td class="info">N° de serie</td>
									<td class="default">
										<input class="form-control input-sm" type="text" ng-model="odu.numero_serie"/>
									</td>
								</tr>
								<tr ng-repeat-end ng-hide="$index > 0">
									<td class="info">Ftx</td>
									<td class="default">
										<div class="input-group input-group-sm">
											<input class="form-control" type="number" min="1" step="any" ng-model="ctrl.nuevoEnlace.odus_enlaces_ne[0].ftx" ng-disabled="$index > 0"/>
											<div class="input-group-addon">MHz</div>
										</div>
									</td>
									<td class="info">Ptx</td>
									<td class="default">
										<div class="input-group input-group-sm">
											<input class="form-control" type="number" step="any" ng-model="ctrl.nuevoEnlace.odus_enlaces_ne[0].ptx" ng-disabled="$index > 0"/>
											<div class="input-group-addon">dBm</div>
										</div>
									</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4">
										<button class="btn btn-primary" style="width:100%;" ng-click="ctrl.nuevaOduNE(ctrl.nuevoEnlace)" ng-disabled="!ctrl.nuevoEnlace.idu_1">
											<i class="fa fa-plus"></i> Agregar ODU
										</button>
									</td>
								</tr>
							</tfoot>
						</table>
						<table class="table table-condensed">
							<tbody>
								<tr class="ng-cloak text-center" ng-repeat-start="antena in ctrl.nuevoEnlace.antenas_mw_enlaces_ne">
									<td colspan="4" class="primary">
										<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminarNuevaAntenaNE($index)"><i class="fa fa-times"></i></button>
										<strong>Antena {{$index + 1}}</strong>
									</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">
										<select class="form-control input-sm" ng-model="antena.tipo_antena_mw.marca" ng-change="antena.tipo_antena_mw.id = '';">
											<option value="">Elegir marca</option>
											<option ng-repeat="marca in ctrl.tipo_antena_mw.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
										</select>
									</td>
									<td class="info">Modelo</td>
									<td class="default">
										<select class="form-control input-sm" ng-model="antena.tipo_antena_mw.id">
											<option value="">Elegir modelo</option>
											<option ng-if="antena.tipo_antena_mw.marca" ng-repeat="modelo in ctrl.tipo_antena_mw.modelos | filter:{marca:antena.tipo_antena_mw.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class="info">Altura</td>
									<td class="default">
										<div class="input-group input-group-sm">
											<input class="form-control" type="number" min="1" step="any" ng-model="antena.altura"/>
											<div class="input-group-addon">m</div>
										</div>
									</td>
									<td class="info">Azimuth</td>
									<td class="default">
										<div class="input-group input-group-sm">
											<input class="form-control" type="number" step="any" ng-model="antena.azimuth"/>
											<div class="input-group-addon">°</div>
										</div>
									</td>
								</tr>
								<tr ng-repeat-end>
									<td class="info">N° de serie</td>
									<td class="default">
										<input class="form-control input-sm" type="text" ng-model="antena.numero_serie"/>
									</td>
									<td class="info" colspan="2"></td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4">
										<button class="btn btn-primary" style="width:100%;" ng-click="ctrl.nuevaAntenaNE(ctrl.nuevoEnlace)" ng-disabled="!ctrl.nuevoEnlace.idu_1">
											<i class="fa fa-plus"></i> Agregar Antena
										</button>
									</td>
								</tr>
							</tfoot>
						</table>
					</td>
					<td colspan="2" style="width:550px;">
						<table class="table table-condensed">
							<tbody>
								<tr class="ng-cloak text-center" ng-repeat-start="odu in ctrl.nuevoEnlace.odus_enlaces_fe">
									<td colspan="4">
										<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminarNuevaOduFE($index)"><i class="fa fa-times"></i></button>
										<strong>ODU {{$index + 1}}</strong>
									</td>
								</tr>
								<!--<tr>
									<td class="info">Marca</td>
									<td class="default">
										<select class="form-control input-sm" ng-model="odu.tipo_odu.marca" ng-change="odu.tipo_odu.modelo = ''; odu.tipo_odu.tipo = ''; odu.tipo_odu.banda = ''; odu.tipo_odu.id = '';">
											<option value="">Elegir marca</option>
											<option ng-repeat="marca in ctrl.tipo_odu.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
										</select>
									</td>
									<td class="info">Modelo</td>
									<td class="default">
										<select class="form-control input-sm" ng-model="odu.tipo_odu.modelo" ng-change="odu.tipo_odu.tipo = ''; odu.tipo_odu.banda = ''; odu.tipo_odu.id = '';">
											<option value="">Elegir modelo</option>
											<option ng-if="odu.tipo_odu.marca" ng-repeat="modelo in ctrl.tipo_odu.modelos | filter:{marca:odu.tipo_odu.marca}" value="{{modelo.modelo}}">{{modelo.modelo}}</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class="info">Tipo</td>
									<td class="default">
										<select class="form-control input-sm" ng-model="odu.tipo_odu.tipo" ng-change="odu.tipo_odu.banda = ''; odu.tipo_odu.id = '';">
											<option value="">Elegir tipo</option>
											<option ng-if="odu.tipo_odu.modelo" ng-repeat="tipo in ctrl.tipo_odu.tipos | filter:{marca:odu.tipo_odu.marca,modelo:odu.tipo_odu.modelo}" value="{{tipo.tipo}}">{{tipo.tipo}}</option>
										</select>
									</td>
									<td class="info">Banda</td>
									<td class="default">
										<div class="input-group input-group-sm">
											<select class="form-control" ng-model="odu.tipo_odu.banda" ng-change="odu.tipo_odu.id = '';">
												<option value="">Elegir banda</option>
												<option ng-if="odu.tipo_odu.tipo" ng-repeat="banda in ctrl.tipo_odu.bandas | filter:{marca:odu.tipo_odu.marca,modelo:odu.tipo_odu.modelo,tipo:odu.tipo_odu.tipo}" value="{{banda.banda}}">{{banda.banda}}</option>
											</select>
											<div class="input-group-addon">GHz</div>
										</div>
									</td>
								</tr>
								<tr>
									<td class="info">Frecuencia</td>
									<td class="default">
										<div class="input-group input-group-sm">
											<select class="form-control" ng-model="odu.tipo_odu.id">
												<option value="">Elegir frecuencia</option>
												<option ng-if="odu.tipo_odu.banda" ng-repeat="frecuencia in ctrl.tipo_odu.frecuencias | filter:{marca:odu.tipo_odu.marca,modelo:odu.tipo_odu.modelo,tipo:odu.tipo_odu.tipo,banda:odu.tipo_odu.banda}" value="{{frecuencia.id}}">{{frecuencia.frec_min}} to {{frecuencia.frec_max}}</option>
											</select>
											<div class="input-group-addon">MHz</div>
										</div>
									</td>
									<td class="info">N° de serie</td>
									<td class="default">
										<input class="form-control input-sm" type="text" ng-model="odu.numero_serie"/>
									</td>
								</tr>-->
								<tr>
									<td class="info">Tipo</td>
									<td class="default">
										<select class="form-control input-sm" ng-model="ctrl.nuevoEnlace.odus_enlaces_fe[0].tipo_odu.id" ng-disabled="$index > 0">
											<option value="">Elegir tipo</option>
											<option ng-repeat="tipo in ctrl.tipo_odu" value="{{tipo.id}}">{{tipo.tipo}}</option>
										</select>
									</td>
									<td class="info">N° de serie</td>
									<td class="default">
										<input class="form-control input-sm" type="text" ng-model="odu.numero_serie"/>
									</td>
								</tr>
								<tr ng-repeat-end ng-hide="$index > 0">
									<td class="info">Ftx</td>
									<td class="default">
										<div class="input-group input-group-sm">
											<input class="form-control" type="number" min="1" step="any" ng-model="ctrl.nuevoEnlace.odus_enlaces_fe[0].ftx" ng-disabled="$index > 0"/>
											<div class="input-group-addon">MHz</div>
										</div>
									</td>
									<td class="info">Ptx</td>
									<td class="default">
										<div class="input-group input-group-sm">
											<input class="form-control" type="number" step="any" ng-model="ctrl.nuevoEnlace.odus_enlaces_fe[0].ptx" ng-disabled="$index > 0"/>
											<div class="input-group-addon">dBm</div>
										</div>
									</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4">
										<button class="btn btn-primary" style="width:100%;" ng-click="ctrl.nuevaOduFE(ctrl.nuevoEnlace)" ng-disabled="!ctrl.nuevoEnlace.idu_2">
											<i class="fa fa-plus"></i> Agregar ODU
										</button>
									</td>
								</tr>
							</tfoot>
						</table>
						<table class="table table-condensed">
							<tbody>
								<tr class="ng-cloak text-center" ng-repeat-start="antena in ctrl.nuevoEnlace.antenas_mw_enlaces_fe">
									<td colspan="4" class="primary">
										<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminarNuevaAntenaFE($index)"><i class="fa fa-times"></i></button>
										<strong>Antena {{$index + 1}}</strong>
									</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">
										<select class="form-control input-sm" ng-model="antena.tipo_antena_mw.marca" ng-change="antena.tipo_antena_mw.id = '';">
											<option value="">Elegir marca</option>
											<option ng-repeat="marca in ctrl.tipo_antena_mw.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
										</select>
									</td>
									<td class="info">Modelo</td>
									<td class="default">
										<select class="form-control input-sm" ng-model="antena.tipo_antena_mw.id">
											<option value="">Elegir modelo</option>
											<option ng-if="antena.tipo_antena_mw.marca" ng-repeat="modelo in ctrl.tipo_antena_mw.modelos | filter:{marca:antena.tipo_antena_mw.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class="info">Altura</td>
									<td class="default">
										<div class="input-group input-group-sm">
											<input class="form-control" type="number" min="1" step="any" ng-model="antena.altura"/>
											<div class="input-group-addon">m</div>
										</div>
									</td>
									<td class="info">Azimuth</td>
									<td class="default">
										<div class="input-group input-group-sm">
											<input class="form-control" type="number" step="any" ng-model="antena.azimuth"/>
											<div class="input-group-addon">°</div>
										</div>
									</td>
								</tr>
								<tr ng-repeat-end>
									<td class="info">N° de serie</td>
									<td class="default">
										<input class="form-control input-sm" type="text" ng-model="antena.numero_serie"/>
									</td>
									<td class="info" colspan="2"></td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4">
										<button class="btn btn-primary" style="width:100%;" ng-click="ctrl.nuevaAntenaFE(ctrl.nuevoEnlace)" ng-disabled="!ctrl.nuevoEnlace.idu_2">
											<i class="fa fa-plus"></i> Agregar Antena
										</button>
									</td>
								</tr>
							</tfoot>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarEnlace(ctrl.nuevoEnlace)" ng-disabled="ctrl.nuevoEnlace.disabled() || !ctrl.nuevoEnlace.configuracion_1 || !ctrl.nuevoEnlace.polaridad || !ctrl.nuevoEnlace.ancho_banda || !ctrl.nuevoEnlace.modulacion_numero || !ctrl.nuevoEnlace.modulacion_letras || !ctrl.nuevoEnlace.capacidad || !ctrl.nuevoEnlace.banda">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionEnlace()">Cancelar</button>
	</form>
</div>

<div id="editarEnlace" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Editar configuración del Enlace</h3>
	
	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Configuración</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" min="1" max="10" class="form-control" style="max-width:60px;" ng-model="ctrl.editarEnlace.configuracion_editar.part_1">
							<div class="input-group-addon">+</div>
							<input type="number" min="0" max="1" class="form-control" style="max-width:60px;" ng-model="ctrl.editarEnlace.configuracion_editar.part_2">
							<div class="input-group-addon">
								<select ng-model="ctrl.editarEnlace.configuracion_editar.part_3" ng-change="ctrl.editarEnlace.configuracion_editar.part_4 = ''; ctrl.editarEnlace.polaridad = '';">
									<option value="">-</option>
									<option value="XPIC">XPIC</option>
								</select>
							</div>
							<div class="input-group-addon">
								<select ng-model="ctrl.editarEnlace.configuracion_editar.part_4">
									<option value="">-</option>
									<option value="HSB" ng-if="!ctrl.editarEnlace.configuracion_editar.part_3">HSB</option>
									<option value="SD">SD</option>
									<option value="FD" ng-if="!ctrl.editarEnlace.configuracion_editar.part_3">FD</option>
								</select>
							</div>
						</div>
					</td>
					<td class="info">Polaridad</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editarEnlace.polaridad">
							<option value="">Elegir polaridad</option>
							<option value="1" ng-if="!ctrl.editarEnlace.configuracion_editar.part_3">V</option>
							<option value="2" ng-if="!ctrl.editarEnlace.configuracion_editar.part_3">H</option>
							<option value="3" ng-if="ctrl.editarEnlace.configuracion_editar.part_3">V+H</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Ancho de Banda (BW)</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input class="form-control" type="number" min="0" step="any" ng-model="ctrl.editarEnlace.ancho_banda"/>
							<div class="input-group-addon">MHz</div>
						</div>
					</td>
					<td class="info">Modulación</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<select class="form-control" ng-model="ctrl.editarEnlace.modulacion_editar.numero">
								<option value="">Elegir</option>
								<option value="16">16</option>
								<option value="32">32</option>
								<option value="64">64</option>
								<option value="128">128</option>
								<option value="256">256</option>
								<option value="512">512</option>
								<option value="1024">1024</option>
							</select>
							<div class="input-group-addon">
								<select ng-model="ctrl.editarEnlace.modulacion_editar.letras">
									<option value="">Elegir</option>
									<option value="QAM">QAM</option>
									<option value="QAMStrong">QAMStrong</option>
									<option value="QAMLight">QAMLight</option>
									<option value="QPSK">QPSK</option>
									<option value="QPSKStrong">QPSKStrong</option>
								</select>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Capacidad</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input class="form-control" type="number" min="1" step="any" ng-model="ctrl.editarEnlace.capacidad"/>
							<div class="input-group-addon">Mbit/s</div>
						</div>
					</td>
					<td class="info">Modulación Adaptativa</td>
					<td class="default">
						<input type="checkbox" ng-model="ctrl.editarEnlace.modulacion_adaptativa"/>
					</td>
				</tr>
				<tr ng-if="ctrl.editarEnlace.modulacion_adaptativa">
					<td class="info">Modulación AM</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<select class="form-control" ng-model="ctrl.editarEnlace.modulacion_am_editar.numero">
								<option value="">Elegir</option>
								<option value="16">16</option>
								<option value="32">32</option>
								<option value="64">64</option>
								<option value="128">128</option>
								<option value="256">256</option>
								<option value="512">512</option>
								<option value="1024">1024</option>
							</select>
							<div class="input-group-addon">
								<select ng-model="ctrl.editarEnlace.modulacion_am_editar.letras">
									<option value="">Elegir</option>
									<option value="QAM">QAM</option>
									<option value="QPSK">QPSK</option>
								</select>
							</div>
						</div>
					</td>
					<td class="info">Capacidad AM</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input class="form-control" type="number" min="0" step="any" ng-model="ctrl.editarEnlace.capacidad_am"/>
							<div class="input-group-addon">Mbit/s</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Banda</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input class="form-control" type="number" min="1" step="any" ng-model="ctrl.editarEnlace.banda"/>
							<div class="input-group-addon">GHz</div>
						</div>
					</td>
					<td class="info" colspan="2"></td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.grabarEnlace(ctrl.editarEnlace)" ng-disabled="!ctrl.editarEnlace.configuracion_editar.part_1 || (ctrl.editarEnlace.configuracion_editar.part_2 != '0' && ctrl.editarEnlace.configuracion_editar.part_2 != '1') || !ctrl.editarEnlace.polaridad || !ctrl.editarEnlace.ancho_banda || !ctrl.editarEnlace.modulacion_editar.numero || !ctrl.editarEnlace.modulacion_editar.letras || !ctrl.editarEnlace.capacidad || !ctrl.editarEnlace.banda || (ctrl.editarEnlace.modulacion_adaptativa && !ctrl.editarEnlace.modulacion_am_numero && !ctrl.editarEnlace.modulacion_am_letras && !ctrl.editarEnlace.capacidad_am)">Guardar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEnlace()">Cancelar</button>
	</form>
</div>

<div id="eliminarEnlace" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar Enlace</h3>
	
	<p>¿Está seguro de eliminar el Enlace?</p>
	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Enlace.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.eliminarEnlace()" ng-disabled="!ctrl.eliminar_Enlace.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>

<div id="nuevoOdu" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Nueva ODU</h3>
	





	<form  id="FormNuevaAntenaMicroondasOdu" class="verifica_factor_uso">





<div id="alerta_de_torre" class="alert alert-danger text-center alerta_de_torre">
			<i class="fa fa-exclamation-triangle fa-2x"></i>Torre bloqueada para nuevas instalaciones!
		</div>
	
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info"><strong>Enlace</strong></td>
					<td class="default">
						<select   class="form-control" ng-model="ctrl.nuevoOdu.enlace" ng-change="ctrl.nuevoOdu.idu = '';">
							<option value="">Elegir enlace</option>
							<option ng-repeat="enlace in ctrl.enlaces" value="{{enlace.id}}">{{enlace.idu_1.sitio.nombre_completo}} <==> {{enlace.idu_2.sitio.nombre_completo}}</option>
						</select>

					</td>
					
					<td class="info"><strong>Sitio - IDU</strong></td>
					<td class="default">
						
						<select id="elegir-Odu"  onchange="FnbuscarsitioOdu()"   class="form-control" ng-model="ctrl.nuevoOdu.idu">
							<option value="">Elegir sitio - IDU</option>
							<option ng-if="ctrl.nuevoOdu.enlace" ng-repeat="idu_1 in ctrl.enlaces | filter:ctrl.filtroNuevaOdu" value="{{idu_1.idu_1.id}}">Near End: {{idu_1.idu_1.sitio.nombre_completo}} - NE ID: {{idu_1.idu_1.ne_id}} sitio:{{idu_1.idu_1.sitio.id}}</option>
							<option ng-if="ctrl.nuevoOdu.enlace" ng-repeat="idu_2 in ctrl.enlaces | filter:ctrl.filtroNuevaOdu" value="{{idu_2.idu_2.id}}">Far End: {{idu_2.idu_2.sitio.nombre_completo}} - NE ID: {{idu_2.idu_2.ne_id}} sitio:{{idu_2.idu_2.sitio.id}}</option>
						</select>
                       <input type="hidden" id="idsitioodu" name="idsitioodu">


					</td>

				</tr>
				

				<tr>
					<td class="info">Tipo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoOdu.tipo_odu.id">
							<option value="">Elegir tipo</option>
							<option ng-repeat="tipo in ctrl.tipo_odu" value="{{tipo.id}}">{{tipo.tipo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="ctrl.nuevoOdu.numero_serie"/>
					</td>
				</tr>
				<tr>
					<td class="info">Ftx</td>
					<td class="default">
						<div class="input-group">
							<input class="form-control" type="number" min="1" step="any" ng-model="ctrl.nuevoOdu.ftx"/>
							<div class="input-group-addon">MHz</div>
						</div>
					</td>
					<td class="info">Ptx</td>
					<td class="default">
						<div class="input-group">
							<input class="form-control" type="number" step="any" ng-model="ctrl.nuevoOdu.ptx"/>
							<div class="input-group-addon">dBm</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarOdu(ctrl.nuevoOdu)" ng-disabled="!ctrl.nuevoOdu.enlace || !ctrl.nuevoOdu.idu || !ctrl.nuevoOdu.tipo_odu.id || !ctrl.nuevoOdu.numero_serie || !ctrl.nuevoOdu.ftx || !ctrl.nuevoOdu.ptx">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionOdu()">Cancelar</button>
	</form>
</div>

<div id="editarOdu" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Editar Odu</h3>
	
	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Tipo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.editarOdu.tipo_odu.id">
							<option value="">Elegir tipo</option>
							<option ng-repeat="tipo in ctrl.tipo_odu" value="{{tipo.id}}">{{tipo.tipo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="ctrl.editarOdu.numero_serie"/>
					</td>
				</tr>
				<tr>
					<td class="info">Ftx</td>
					<td class="default">
						<div class="input-group">
							<input class="form-control" type="number" min="1" step="any" ng-model="ctrl.editarOdu.ftx"/>
							<div class="input-group-addon">MHz</div>
						</div>
					</td>
					<td class="info">Ptx</td>
					<td class="default">
						<div class="input-group">
							<input class="form-control" type="number" min="1" step="any" ng-model="ctrl.editarOdu.ptx"/>
							<div class="input-group-addon">dBm</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.grabarOdu(ctrl.editarOdu)" ng-disabled="!ctrl.editarOdu.tipo_odu.id || !ctrl.editarOdu.numero_serie || !ctrl.editarOdu.ftx || !ctrl.editarOdu.ptx">Guardar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionOdu()">Cancelar</button>
	</form>
</div>

<div id="eliminarOdu" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar ODU</h3>
	
	<p>¿Está seguro de eliminar la ODU?</p>
	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Odu.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.eliminarOdu()" ng-disabled="!ctrl.eliminar_Odu.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionOdu()">Cancelar</button>
</div>

<div id="nuevoAntena" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Nueva Antena Microondas</h3>
	
	<form id="FormNuevaAntenaMicroondas" class="verifica_factor_uso">
         <div id="alerta_de_torre" class="alert alert-danger text-center alerta_de_torre">
			<i class="fa fa-exclamation-triangle fa-2x"></i>Torre bloqueada para nuevas instalaciones!
		</div>
		<table class="table table-condensed">
			<tr>
					<td class="info"><strong>Enlace</strong></td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAntena.enlace" ng-change="ctrl.nuevoAntena.idu = '';">
							<option value="">Elegir enlace</option>
							<option ng-repeat="enlace in ctrl.enlaces" value="{{enlace.id}}">{{enlace.idu_1.sitio.nombre_completo}} <==> {{enlace.idu_2.sitio.nombre_completo}}</option>
						</select>
					</td>
					<td class="info"><strong>Sitio - IDU</strong></td>
					
					<td class="default">						
						
						<select id="elegir-idu" name="elegir-idu" onchange="Fnbuscarsitio()" class="form-control" ng-model="ctrl.nuevoAntena.idu">
							<option value="">Elegir sitio - IDU</option>
							
							<option   ng-if="ctrl.nuevoAntena.enlace" ng-repeat="idu_1 in ctrl.enlaces | filter:ctrl.filtroNuevaAntena" 
							value="{{idu_1.idu_1.id}}">Near End: {{idu_1.idu_1.sitio.nombre_completo}} - NE ID: {{idu_1.idu_1.ne_id}} sitio:{{idu_1.idu_1.sitio.id}}</option>

							<option  ng-if="ctrl.nuevoAntena.enlace" ng-repeat="idu_2 in ctrl.enlaces | filter:ctrl.filtroNuevaAntena"
							 value="{{idu_2.idu_2.id}}">Far End: {{idu_2.idu_2.sitio.nombre_completo}} - NE ID: {{idu_2.idu_2.ne_id}} sitio:{{idu_2.idu_2.sitio.id}}</option>
						</select>

                        <input type="hidden" value="" name="idsitio" id="idsitio">

					</td>

				</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.nuevoAntena.tipo_antena_mw.marca" ng-change="ctrl.nuevoAntena.tipo_antena_mw.id = '';">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_antena_mw.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</td>
				<td class="info">Modelo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.nuevoAntena.tipo_antena_mw.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.nuevoAntena.tipo_antena_mw.marca" ng-repeat="modelo in ctrl.tipo_antena_mw.modelos | filter:{marca:ctrl.nuevoAntena.tipo_antena_mw.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="info">Altura</td>
				<td class="default">
					<div class="input-group">
						<input class="form-control" type="number" min="1" step="any" ng-model="ctrl.nuevoAntena.altura"/>
						<div class="input-group-addon">m</div>
					</div>
				</td>
				<td class="info">Azimuth</td>
				<td class="default">
					<div class="input-group">
						<input class="form-control" type="number" step="any" ng-model="ctrl.nuevoAntena.azimuth"/>
						<div class="input-group-addon">°</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="info">Número de serie</td>
				<td class="default">
					<input class="form-control" type="text" ng-model="ctrl.nuevoAntena.numero_serie"/>
				</td>
				<td class="info" colspan="2"></td>
			</tr>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarAntena(ctrl.nuevoAntena)" ng-disabled="!ctrl.nuevoAntena.enlace || !ctrl.nuevoAntena.idu || !ctrl.nuevoAntena.tipo_antena_mw.id || !ctrl.nuevoAntena.altura || !ctrl.nuevoAntena.azimuth || !ctrl.nuevoAntena.numero_serie">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionAntena()">Cancelar</button>
	</form>
</div>

<div id="editarAntena" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Editar Antena</h3>
	
	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.editarAntena.tipo_antena_mw.marca" ng-change="ctrl.editarAntena.tipo_antena_mw.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_antena_mw.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.editarAntena.tipo_antena_mw.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.editarAntena.tipo_antena_mw.marca" ng-repeat="modelo in ctrl.tipo_antena_mw.modelos | filter:{marca:ctrl.editarAntena.tipo_antena_mw.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Altura</td>
					<td class="default">
						<div class="input-group">
							<input class="form-control" type="number" min="1" step="any" ng-model="ctrl.editarAntena.altura"/>
							<div class="input-group-addon">m</div>
						</div>
					</td>
					<td class="info">Azimuth</td>
					<td class="default">
						<div class="input-group">
							<input class="form-control" type="number" min="1" step="any" ng-model="ctrl.editarAntena.azimuth"/>
							<div class="input-group-addon">°</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="ctrl.editarAntena.numero_serie"/>
					</td>
					<td class="info" colspan="2"></td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.grabarAntena(ctrl.editarAntena)" ng-disabled="!ctrl.editarAntena.tipo_antena_mw.id || !ctrl.editarAntena.altura || !ctrl.editarAntena.azimuth || !ctrl.editarAntena.numero_serie">Guardar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionAntena()">Cancelar</button>
	</form>
</div>

<div id="eliminarAntena" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar Antena Microondas</h3>
	
	<p>¿Está seguro de eliminar la Antena Microondas?</p>
	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Antena.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.eliminarAntena()" ng-disabled="!ctrl.eliminar_Antena.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionAntena()">Cancelar</button>
</div>

<div id="almacenarElementos" class="popup">
	<span class="button b-close">X</span>

	<h3>Almacenar elementos microondas</h3>

	<div style="max-width:400px;max-height:300px;overflow:auto;">
		<div class="panel panel-default">
			<div class="panel-body">
				<h5>Seleccionar ODUs:</h5>
				<div class="checkbox" ng-repeat="odu in ctrl.almacenar_Elementos.odus_enlaces | filter: {eliminado: 0}">
					<label><input type="checkbox" ng-model="odu.check"> Tipo: {{odu.tipo_odu.tipo}} - Número de serie: {{odu.numero_serie}}</label>
				</div><br>
				<h5>Seleccionar Antenas:</h5>
				<div class="checkbox" ng-repeat="antena in ctrl.almacenar_Elementos.antenas_mw_enlaces | filter: {eliminado: 0}">
					<label><input type="checkbox" ng-model="antena.check"> {{antena.tipo_antena_mw.marca}} | {{antena.tipo_antena_mw.modelo}} - Número de serie: {{antena.numero_serie}}</label>
				</div>
			</div>
		</div>

		<p>Elegir destino:</p>

		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a data-toggle="tab" href="#almacen" ng-click="ctrl.opcionAlmacen()">Almacén</a></li>
			<li><a data-toggle="tab" href="#sitio" ng-click="ctrl.opcionSitio()">Sitio</a></li>
		</ul>
		<div class="tab-content">
			<div id="almacen" class="tab-pane fade in active">
				<select class="form-control" ng-model="ctrl.almacenar_Elementos.almacen">
					<option value="">Elegir almacén</option>
					<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
				</select>
			</div>
			<div id="sitio" class="tab-pane fade">
				<label style="width:100%">
					<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenar_Elementos.a_sitio"></selectize>
				</label>
			</div>
		</div>
		<select class="form-control" ng-model="ctrl.almacenar_Elementos.estado">
			<option value="">Elegir estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select><br>
		<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea almacenar el elemento..." ng-model="ctrl.almacenar_Elementos.comentario"></textarea>
	</div>

	<button class="btn btn-success" ng-click="ctrl.almacenarElementos(ctrl.almacenar_Elementos)" ng-disabled="!ctrl.disabledAlmacenamiento()">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamiento()">Cancelar</button>
</div>

<div id="previaEliminar" class="popup">
	<span class="button b-close">X</span>

	<h3>Equipos asociados a este enlace</h3>

	<br><p>¿Desea almacenar los elementos asociados a este Enlace antes de eliminarlo?</p><br>

	<button class="btn btn-success" ng-click="ctrl.almacenandoElementos(ctrl.previaEliminar_Enlace); ctrl.previaEliminar_Enlace = {};">SI</button>
	<button class="btn btn-danger" ng-click="ctrl.eliminandoEnlace(ctrl.previaEliminar_Enlace); ctrl.previaEliminar_Enlace = {};">NO</button>
</div>