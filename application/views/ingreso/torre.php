<style type="text/css">

</style>
<br>
<div class="container ng-cloak">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<nav class="navbar">
				<div class="container-fluid">
					<div class="navbar-header" style="color:rgb(245,121,23);">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-busqueda">
							<span class="sr-only" style="color:rgb(245,121,23);">Toggle navigation</span>
							<span class="fa fa-bars"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="navbar-busqueda">
						<b><ul class="nav navbar-nav">
							<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(1)" href>Nombre y Ubicación</a></li>
							<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(2)" href>Características físicas</a></li>
							<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(3)" href>Operadores y Proyectos</a></li>
							<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(4)" href>Acceso</a></li>
							<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(5)" href>Mantenimiento y Contratos</a></li>
						</ul></b>
					</div>
				</div>
			</nav>
		</div>
		<div class="panel-body ng-cloak" style="height:375px;overflow:scroll">
			<table id="nombre_y_ubicacion" class="table table-condensed" ng-if="ctrl.info.info == 1">
				<tbody>
					<tr>
						<td class="info text-right">Código</td>
						<td class="active">{{ctrl.info_torre.codigo}}</td>
					</tr>
					<tr>
						<td class="info text-right">Nombre</td>
						<td class="active">{{ctrl.info_torre.nombre}}</td>
					</tr>
					<tr>
						<td class="info text-right">Nombre completo</td>
						<td class="active">{{ctrl.info_torre.nombre_completo}}</td>
					</tr>
					<tr>
						<td class="info text-right">Departamento</td>
						<td class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.zona_peru.departamento}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.zona_peru.departamento" ng-change="ctrl.listar_provincias(ctrl.info_torre.zona_peru.departamento)">
								<option value="">Departamento</option>
								<option ng-repeat="departamento in ctrl.departamentos" value="{{departamento.departamento}}">{{departamento.departamento}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="info text-right">Provincia</td>
						<td class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.zona_peru.provincia}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.zona_peru.provincia" ng-change="ctrl.listar_distritos(ctrl.info_torre.zona_peru.departamento,ctrl.info_torre.zona_peru.provincia)">
								<option value="">Provincia</option>
								<option ng-repeat="provincia in ctrl.provincias" value="{{provincia.provincia}}">{{provincia.provincia}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="info text-right">Distrito</td>
						<td class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.zona_peru.distrito}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.zona_peru.distrito">
								<option value="">Distrito</option>
								<option ng-repeat="distrito in ctrl.distritos" value="{{distrito.distrito}}">{{distrito.distrito}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="info text-right">Dirección</td>
						<td class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.direccion}}</span>
							<input class="form-control" placeholder="Introducir dirección" ng-model="ctrl.info_torre.direccion" ng-show="ctrl.editar" />
						</td>
					</tr>
					<tr>
						<td class="info text-right">Coordenadas</td>
						<td class="active">
							<span ng-hide="ctrl.editar">Latitud: {{ctrl.info_torre.latitud}}°, Longitud: {{ctrl.info_torre.longitud}}°</span>
							<span ng-show="ctrl.editar">
								<div class="input-group">
									<div class="input-group-addon">Latitud: </div>
									<input class="form-control" type="number" step="any" ng-model="ctrl.info_torre.latitud" />
									<div class="input-group-addon">°</div>
								</div>
								<div class="input-group">
									<div class="input-group-addon">Longitud: </div>
									<input class="form-control" type="number" step="any" ng-model="ctrl.info_torre.longitud" />
									<div class="input-group-addon">°</div>
								</div>
							</span>
						</td>
					</tr>
				</tbody>
			</table>
			<table id="caracteristicas_fisicas" class="table table-condensed" ng-if="ctrl.info.info == 2">
				<tbody>
					<tr>
						<td colspan="2" class="info text-right">Mimetizado</td>
						<td colspan="2" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.camuflaje.camuflaje}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.camuflaje.id">
								<option value="">Mimetizado</option>
								<option ng-repeat="camuflaje in ctrl.camuflajes" value="{{camuflaje.id}}">{{camuflaje.camuflaje}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Posición de torre</td>
						<td colspan="1" class="active">
							<span ng-if="ctrl.info_torre.greenfield_rooftop == 1" ng-hide="ctrl.editar">Rooftop</span>
							<span ng-if="ctrl.info_torre.greenfield_rooftop == 0" ng-hide="ctrl.editar">Greenfield</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.greenfield_rooftop" ng-change="ctrl.cambioGreenfieldRooftop()">
								<option value="">Mimetizado</option>
								<option value="0">Greenfield</option>
								<option value="1">Rooftop</option>
							</select>
						</td>
						<td colspan="1" class="info text-right">Altura de edificación</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.altura_edificacion}} m</span>
							<div class="input-group" ng-show="ctrl.editar">
								<input class="form-control" type="number" step="any" min="0" ng-model="ctrl.info_torre.altura_edificacion" ng-disabled="ctrl.info_torre.greenfield_rooftop == 0" />
								<div class="input-group-addon">m</div>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Tipo de torre</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.tipo_torre.tipo_torre}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.tipo_torre.id">
								<option value="">Tipo de torre</option>
								<option ng-repeat="tipo_torre in ctrl.tipos_torre" value="{{tipo_torre.id}}">{{tipo_torre.tipo_torre}}</option>
							</select>
						</td>
						<td colspan="1" class="info text-right">Altura de torre</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.altura_torre}} m</span>
							<div class="input-group" ng-show="ctrl.editar">
								<input class="form-control" type="number" step="any" min="0" ng-model="ctrl.info_torre.altura_torre" />
								<div class="input-group-addon">m</div>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="info text-right">Factor de uso de Torre</td>
						<td colspan="2" class="active">
							<h3><span ng-hide="ctrl.editar" ng-class="{'label label-success label-':ctrl.info_torre.factor_uso_torre < 0.8,'label label-warning':ctrl.info_torre.factor_uso_torre >= 0.8 && ctrl.info_torre.factor_uso_torre <= 1,'label label-danger':ctrl.info_torre.factor_uso_torre > 1}">{{ctrl.info_torre.factor_uso_torre}}</span></h3>
							<input ng-show="ctrl.editar" class="form-control" type="number" step="any" min="0" ng-model="ctrl.info_torre.factor_uso_torre" />
						</td>
					</tr>
					<tr>
						<td colspan="2" class="info text-right">Ubicación de equipos</td>
						<td colspan="2" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.ubicacion_equipos.ubicacion_equipos}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.ubicacion_equipos.id">
								<option value="">Ubicación de equipos</option>
								<option ng-repeat="ubicacion_equipos in ctrl.ubicacion_equipos" value="{{ubicacion_equipos.id}}">{{ubicacion_equipos.ubicacion_equipos}}</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
			<table id="operadores_y_proyectos" class="table table-condensed" ng-if="ctrl.info.info == 3">
				<tbody>
					<tr>
						<td colspan="1" class="info text-right">Coubicado en</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.coubicador.coubicador}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.coubicador.id" ng-change="ctrl.cambioCoubicadoEn()">
								<option value="" >Coubicador</option>
								<option ng-repeat="coubicador in ctrl.coubicadores| orderBy:'coubicador'" value="{{coubicador.id}}">{{coubicador.coubicador}}</option>
							</select>
						</td>
						<td colspan="1" class="info text-right">Nombre de sitio por coubicador</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.nombre_sitio_coubicador}}</span>
							<input class="form-control" placeholder="Nombre de sitio por coubicador" ng-show="ctrl.editar" ng-model="ctrl.info_torre.nombre_sitio_coubicador" ng-disabled="ctrl.info_torre.coubicador.id == 1 || !ctrl.info_torre.coubicador.id" />
						</td>
						<td colspan="1" class="info text-right">Código por coubicador</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.codigo_torrera}}</span>
							<input class="form-control" placeholder="Código por coubicador" ng-show="ctrl.editar" ng-model="ctrl.info_torre.codigo_torrera" ng-disabled="ctrl.info_torre.coubicador.id == 1 || !ctrl.info_torre.coubicador.id" />
						</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Operador coubicante</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.operador_coubicante.coubicante}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.operador_coubicante.id" ng-change="ctrl.cambioCoubicante()">
								<option value="">Coubicante</option>
								<option ng-repeat="operador_coubicante in ctrl.operadores_coubicantes" value="{{operador_coubicante.id}}">{{operador_coubicante.coubicante}}</option>
							</select>
						</td>
						<td colspan="2" class="info text-right">Nombre de sitio por coubicante</td>
						<td colspan="2" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.nombre_sitio_coubicante}}</span>
							<input class="form-control" placeholder="Nombre de sitio por coubicador" ng-show="ctrl.editar" ng-model="ctrl.info_torre.nombre_sitio_coubicante" ng-disabled="ctrl.info_torre.operador_coubicante.id == 1 || !ctrl.info_torre.operador_coubicante.id" />
						</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Proyecto</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.proyecto.proyecto}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.proyecto.id">
								<option value="">Proyecto</option>
								<option ng-repeat="proyecto in ctrl.proyectos" value="{{proyecto.id}}">{{proyecto.proyecto}}</option>
							</select>
						</td>
						<td colspan="1" class="info text-right">Año de construcción</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.anio_construccion}}</span>
							<input class="form-control" type="number" step="any" min="0" ng-model="ctrl.info_torre.anio_construccion" ng-show="ctrl.editar" />
						</td>
						<td colspan="1" class="info text-right">Contrata constructora</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.contrata_constructora.contrata_constructora}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.contrata_constructora.id">
								<option value="">Contrata constructora</option>
								<option ng-repeat="contrata_constructora in ctrl.contratas_constructoras" value="{{contrata_constructora.id}}">{{contrata_constructora.contrata_constructora}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Tipo de proyecto</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.tipo_proyecto.tipo_proyecto}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.tipo_proyecto.id">
								<option value="">Elegir tipo de proyecto</option>
								<option ng-repeat="proyecto in ctrl.tipo_proyecto" value="{{proyecto.id}}">{{proyecto.tipo_proyecto}}</option>
							</select>
						</td>
						<td colspan="1" class="info text-right">Sub tipo</td>
						<td colspan="1" class="active"></td>
						<td colspan="1" class="info text-right">Proyecto</td>
						<td colspan="1" class="active"></td>
					</tr>
				</tbody>
			</table>
			<table id="acceso" class="table table-condensed" ng-if="ctrl.info.info == 4">
				<tbody>
					<tr>
						<td colspan="1" class="info text-right">Acceso libre 24h</td>
						<td colspan="1" class="active">
							<span class="label label-success" ng-if="ctrl.info_torre.acceso_libre_24h" ng-hide="ctrl.editar">SÍ</span>
							<span class="label label-danger" ng-if="!ctrl.info_torre.acceso_libre_24h" ng-hide="ctrl.editar">NO</span>
							<label class="switch" ng-show="ctrl.editar">
								<input type="checkbox" ng-model="ctrl.info_torre.acceso_libre_24h">
								<div class="slider round"></div>
							</label>
						</td>
						<td colspan="1" class="info text-right">Nivel de riesgo</td>
						<td colspan="1" class="active">
							<span class="label label-warning" ng-hide="ctrl.editar">{{ctrl.info_torre.nivel_riesgo}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.nivel_riesgo">
								<option value="">Nivel de riesgo</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Consideraciones de acceso</td>
						<td colspan="3" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.consideracion_acceso}}</span>
							<textarea ng-show="ctrl.editar" ng-model="ctrl.info_torre.consideracion_acceso" class="form-control" style="width:100%;resize:none" cols="3"></textarea>
						</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Estado</td>
						<td colspan="1" class="active">
							<span class="label label-success" ng-hide="ctrl.editar" ng-if="ctrl.info_torre.estado == 2">ON AIR</span>
							<span class="label label-danger" ng-hide="ctrl.editar" ng-if="ctrl.info_torre.estado == 1">OFF AIR</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.estado">
								<option value="1">OFF AIR</option>
								<option value="2">ON AIR</option>
							</select>
						</td>
						<td colspan="1" class="info text-right">Fecha On Air</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.fecha_estado_on_air | date:"dd/MM/yyyy"}}</span>
							<input class="form-control" type="date" ng-show="ctrl.editar" ng-model="ctrl.info_torre.fecha_estado_on_air" />
						</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Consideraciones</td>
						<td colspan="3" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.consideraciones}}</span>
							<textarea ng-show="ctrl.editar" ng-model="ctrl.info_torre.consideraciones" class="form-control" style="width:100%;resize:none" cols="3"></textarea>
						</td>
					</tr>
				</tbody>
			</table>
			<table id="mantenimiento_y_contratos" class="table table-condensed" ng-if="ctrl.info.info == 5">
				<tbody>
					<tr>
						<td colspan="3" class="info text-right">Prioridad</td>
						<td colspan="3" class="active">
							<span class="label" ng-class="{'label-success':ctrl.info_torre.prioridad == 4,'label-danger':ctrl.info_torre.prioridad == 0, 'label-warning':(ctrl.info_torre.prioridad > 0 && ctrl.info_torre.prioridad < 4)}" ng-hide="ctrl.editar">P{{ctrl.info_torre.prioridad}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.prioridad">
								<option value="">Prioridad</option>
								<option value="0">P0</option>
								<option value="1">P1</option>
								<option value="2">P2</option>
								<option value="3">P3</option>
								<option value="4">P4</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="info text-right">Características especiales</td>
						<td colspan="4" class="active">
							<span ng-hide="ctrl.editar">
								<span class="label label-default" ng-if="ctrl.info_torre.in_building != '1' && ctrl.info_torre.agregador != '1' && ctrl.info_torre.dorsal != '1'" style="font-size:19px;">NINGUNA</span>
								<span class="label label-info" ng-if="ctrl.info_torre.in_building == '1'" style="font-size:19px;">In-Building</span>
								<span class="label label-danger" ng-if="ctrl.info_torre.agregador == '1'" style="font-size:19px;">Agregador</span>
								<span class="label label-warning" ng-if="ctrl.info_torre.dorsal == '1'" style="font-size:19px;">Dorsal</span>
							</span>
							<span ng-show="ctrl.editar">
								<label class="label label-info" style="font-size:19px;">
									<div class="switch" ng-show="ctrl.editar">
										<input type="checkbox" ng-model="ctrl.info_torre.in_building">
										<div class="slider round"></div>
									</div> In-Building
								</label>
								<label class="label label-danger" style="font-size:19px;">
									<div class="switch" ng-show="ctrl.editar">
										<input type="checkbox" ng-model="ctrl.info_torre.agregador">
										<div class="slider round"></div>
									</div> Agregador
								</label>
								<label class="label label-warning" style="font-size:19px;">
									<div class="switch" ng-show="ctrl.editar">
										<input type="checkbox" ng-model="ctrl.info_torre.dorsal">
										<div class="slider round"></div>
									</div> Dorsal
								</label>
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Tipo de solución</td>
						<td colspan="2" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.tipo_solucion.tipo_solucion}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.tipo_solucion.id">
								<option value="">Tipo de solución</option>
								<option ng-repeat="tipo_solucion in ctrl.tipos_solucion" value="{{tipo_solucion.id}}">{{tipo_solucion.tipo_solucion}}</option>
							</select>
						</td>
						<td colspan="1" class="info text-right">Proveedor de mantenimiento</td>
						<td colspan="2" class="active">
							<span ng-hide="ctrl.editar">{{ctrl.info_torre.proveedor_mantenimiento.proveedor_mantenimiento}}</span>
							<select class="form-control" ng-show="ctrl.editar" ng-model="ctrl.info_torre.proveedor_mantenimiento.id">
								<option value="">Tipo de solución</option>
								<option ng-repeat="proveedor_mantenimiento in ctrl.proveedores_mantenimiento" value="{{proveedor_mantenimiento.id}}">{{proveedor_mantenimiento.proveedor_mantenimiento}}</option>
							</select>
						</td>
					</tr>

					<tr ng-repeat-start="contrato in ctrl.info_torre.contratos" ng-if="contrato.eliminado == 0">
						<td colspan="6" class="success text-center">
							ID contrato: {{contrato.id_contrato}}
							<button class="btn btn-danger btn-sm" ng-show="ctrl.editar" ng-click="ctrl.eliminandoContrato(contrato)"><span class="fa fa-times"></span> Eliminar</button>
						</td>
					</tr>
					<tr ng-if="contrato.eliminado == 0">
						<td colspan="2" class="info text-right">Fecha inicio</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{contrato.fecha_inicio | date:"dd/MM/yyyy"}}</span>
							<input class="form-control" ng-show="ctrl.editar" type="date" ng-model="contrato.fecha_inicio" />
						</td>
						<td colspan="2" class="info text-right">Fecha fin</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar" ng-if="contrato.fecha_fin">{{contrato.fecha_fin | date:"dd/MM/yyyy"}}</span>
							<span ng-hide="ctrl.editar" ng-if="!contrato.fecha_fin">Indeterminado</span>
							<input class="form-control" ng-show="ctrl.editar" type="date" ng-model="contrato.fecha_fin" />
						</td>
					</tr>
					<tr ng-if="contrato.eliminado == 0">
						<td colspan="1" class="info text-right">Arrendador</td>
						<td colspan="2" class="active">
							<span ng-hide="ctrl.editar">{{contrato.arrendador}}</span>
							<input class="form-control" ng-show="ctrl.editar" ng-model="contrato.arrendador" />
						</td>
						<td colspan="1" class="info text-right">Área arrendada</td>
						<td colspan="2" class="active">
							<span ng-hide="ctrl.editar">{{contrato.area}} m&sup2;</span>
							<div class="input-group" ng-show="ctrl.editar">
								<input class="form-control" type="number" min="0" step="any" ng-model="contrato.area" />
								<div class="input-group-addon">m&sup2;</div>
							</div>
						</td>
					</tr>
					<tr ng-if="(contrato.eliminado == 0) && (contacto.eliminado == 0)" ng-repeat="contacto in contrato.contactos_contrato">
						<td colspan="1" class="info text-right">
							<button class="btn btn-danger btn-xs" ng-show="ctrl.editar" ng-click="ctrl.eliminandoContacto(contacto)"><span class="fa fa-times"></span></button> Contacto
						</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{contacto.contacto}}</span>
							<input class="form-control" ng-show="ctrl.editar" ng-model="contacto.contacto" />
						</td>
						<td colspan="1" class="info text-right">Número(s) telefónico(s)</td>
						<td colspan="1" class="active">
							<span class="label label-default" style="margin-right:2.5px;" ng-hide="ctrl.editar" ng-repeat="telefono in contacto.telefono">{{telefono.telefono}}</span>
							<span>
								<input class="form-control" ng-show="ctrl.editar" ng-repeat="telefono in contacto.telefono" ng-model="telefono.telefono" />
							</span>
						</td>
						<td colspan="1" class="info text-right">Correo(s) electrónico(s)</td>
						<td colspan="1" class="active">
							<span class="label label-default" style="margin-right:2.5px;" ng-hide="ctrl.editar" ng-repeat="correo in contacto.correo">{{correo.correo}}</span>
							<span>
								<input class="form-control" ng-show="ctrl.editar" ng-repeat="correo in contacto.correo" ng-model="correo.correo" />
							</span>
						</td>
					</tr>
					<tr ng-show="ctrl.editar" ng-repeat-end ng-if="contrato.eliminado == 0">
						<td colspan="6" class="active text-center">
							<button class="btn btn-info" style="width:100%;" ng-click="ctrl.nuevoContacto(contrato)"><span class="fa fa-plus"></span> Agregar contacto</button>
						</td>
					</tr>

					<tr ng-show="ctrl.editar">
						<td colspan="6" class="active text-center">
							<button class="btn btn-primary" style="width:100%;" ng-click="ctrl.nuevoContrato()"><span class="fa fa-plus"></span> Agregar contrato</button>
						</td>
					</tr>
					<tr ng-show="ctrl.editar">
						<td colspan="6" class="active text-center">
							<button class="btn btn-default" style="width:100%;" ng-click="ctrl.sinNuevoContrato()"><span class="fa fa-plus"></span> Sin contrato</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="panel-footer text-center">
			<button class="btn btn-warning btn-sm" ng-click="ctrl.editarTorre()" ng-hide="ctrl.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
			<button class="btn btn-success btn-sm" ng-click="ctrl.grabar()" ng-show="ctrl.editar">Grabar</button>
			<button class="btn btn-danger btn-sm" ng-click="ctrl.cancelarEdicion()" ng-show="ctrl.editar">Cancelar</button>
			<div class="text-right" style="font-size: 8pt;">
				<span ng-if="!ctrl.info_torre.logs_status_site.length || ctrl.info_torre.logs_status_site[0].evento.tipo != 'INSERT'">Creado: {{ctrl.info_torre.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>
				<span ng-if="ctrl.info_torre.logs_status_site[0].evento.tipo == 'INSERT'">Creado: {{ctrl.info_torre.logs_status_site[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{ctrl.info_torre.logs_status_site[0].usuario.nombres}} {{ctrl.info_torre.logs_status_site[0].usuario.apellido_paterno}}</span>
				<br>
				<span ng-if="ctrl.info_torre.logs_status_site[1].evento.tipo == 'UPDATE'">Actualizado: {{ctrl.info_torre.logs_status_site[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{ctrl.info_torre.logs_status_site[1].usuario.nombres}} {{ctrl.info_torre.logs_status_site[1].usuario.apellido_paterno}}</span>
			</div>
		</div>
	</div>
</div>

<div id="nuevoContrato" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nuevo contrato</h3>

	<form name="nuevo_contrato">
		<div class="panel panel-primary">
			<div class="panel-body">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<td class="info text-right">ID contrato</td>
							<td class="active">
								<input type="text" class="form-control" name="id_contrato" ng-model="ctrl.nuevo_contrato.id_contrato"/>
							</td>
							<td class="info text-right">Fecha inicio</td>
							<td class="active">
								<input class="form-control" type="date" ng-model="ctrl.nuevo_contrato.fecha_inicio" />
							</td>
						</tr>
						<tr>
							<td class="info text-right">¿Tiene fecha de vencimiento?</td>
							<td class="active">
								<input type="checkbox" ng-model="ctrl.nuevo_contrato.tiene_fecha_fin" ng-disabled="ctrl.nuevo_contrato.indeterminado">
							</td>
							<td class="info text-right" ng-if-start="ctrl.nuevo_contrato.tiene_fecha_fin">Fecha fin</td>
							<td class="active" ng-if-end>
								<input class="form-control" type="date" ng-model="ctrl.nuevo_contrato.fecha_fin" />
							</td>
							<td class="info text-right" ng-if-start="!ctrl.nuevo_contrato.tiene_fecha_fin">¿Es indeterminado?</td>
							<td class="active" ng-if-end>
								<input type="checkbox" ng-model="ctrl.nuevo_contrato.indeterminado" ng-disabled="ctrl.nuevo_contrato.tiene_fecha_fin">
							</td>
						</tr>
						<tr>
							<td class="info text-right">Arrendador</td>
							<td class="active">
								<input class="form-control" ng-model="ctrl.nuevo_contrato.arrendador" />
							</td>
							<td class="info text-right">Área arrendada</td>
							<td class="active">
								<div class="input-group">
									<input class="form-control" type="number" min="0" step="any" ng-model="ctrl.nuevo_contrato.area" />
									<div class="input-group-addon">m&sup2;</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="panel-footer text-center">
				<button class="btn btn-success btn-sm" ng-click="ctrl.agregarNuevoContrato(ctrl.nuevo_contrato)" ng-disabled="!ctrl.nuevo_contrato.id_contrato || !ctrl.nuevo_contrato.fecha_inicio || !ctrl.nuevo_contrato.arrendador || !ctrl.nuevo_contrato.area || ((!ctrl.nuevo_contrato.tiene_fecha_fin || !ctrl.nuevo_contrato.fecha_fin) && !ctrl.nuevo_contrato.indeterminado)">Agregar</button>
				<button class="btn btn-danger btn-sm" ng-click="ctrl.cancelarNuevoContrato()">Cancelar</button>
			</div>
		</div>
	</form>
</div>

<div id="sinNuevoContrato" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Sin contrato</h3>

	<form name="nuevo_contrato">
		<div class="panel panel-primary">
			<div class="panel-body">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<td colspan="2" class="info text-right">ID contrato</td>
							<td colspan="2" class="active">
								<input type="text" class="form-control" name="id_contrato" ng-model="ctrl.nuevo_contrato.id_contrato"/>
							</td>
						</tr>
						<tr>
							<td class="info text-right">Arrendador</td>
							<td class="active">
								<input class="form-control" ng-model="ctrl.nuevo_contrato.arrendador" />
							</td>
							<td class="info text-right">Área arrendada</td>
							<td class="active">
								<div class="input-group">
									<input class="form-control" type="number" min="0" step="any" ng-model="ctrl.nuevo_contrato.area" />
									<div class="input-group-addon">m&sup2;</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="panel-footer text-center">
				<button class="btn btn-success btn-sm" ng-click="ctrl.agregarNuevoContrato(ctrl.nuevo_contrato)" ng-disabled="!ctrl.nuevo_contrato.id_contrato || !ctrl.nuevo_contrato.arrendador || !ctrl.nuevo_contrato.area">Agregar</button>
				<button class="btn btn-danger btn-sm" ng-click="ctrl.cancelarSinNuevoContrato()">Cancelar</button>
			</div>
		</div>
	</form>
</div>

<div id="eliminarContrato" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar contrato</h3>

	<form>
		<div class="panel panel-primary">
			<div class="panel-body">
				<p>Explique la razón por la que quiere eliminar el contrato "{{ctrl.eliminar_contrato.id_contrato}}"</p>
				<textarea class="form-control" style="width:100%;" rows="3" placeholder="Razón..." ng-model="ctrl.eliminar_contrato.comentario"></textarea>
			</div>
			<div class="panel-footer text-center">
				<button class="btn btn-success btn-sm" ng-disabled="!ctrl.eliminar_contrato.comentario" ng-click="ctrl.eliminarContrato(ctrl.eliminar_contrato)">Eliminar</button>
				<button class="btn btn-danger btn-sm" ng-click="ctrl.cancelarEliminacionContrato()">Cancelar</button>
			</div>
		</div>
	</form>
</div>

<div id="nuevoContacto" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nuevo contacto</h3>

	<form>
		<div class="panel panel-primary">
			<div class="panel-body">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<td class="info text-right">Nombre de contacto</td>
							<td class="active">
								<input class="form-control" ng-model="ctrl.nuevo_contacto.contacto" />
							</td>
						</tr>
						<tr>
							<td class="info text-right">Teléfono</td>
							<td class="active">
								<input class="form-control" type="tel" ng-repeat="telefono in ctrl.nuevo_contacto.telefono" ng-model="telefono.telefono" />
								<button class="btn btn-info text-center" style="width:100%;margin-top: 5px;" ng-click="ctrl.agregarTelefonoNuevoContacto()"><span class="fa fa-plus"></span></button>
							</td>
						</tr>
						<tr>
							<td class="info text-right">Correo</td>
							<td class="active">
								<input class="form-control" type="email" ng-repeat="correo in ctrl.nuevo_contacto.correo" ng-model="correo.correo" />
								<button class="btn btn-info text-center" style="width:100%;margin-top: 5px;" ng-click="ctrl.agregarCorreoNuevoContacto()"><span class="fa fa-plus"></span></button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="panel-footer text-center">
				<button class="btn btn-success btn-sm" ng-click="ctrl.agregarNuevoContacto(ctrl.nuevo_contacto)" ng-disabled="!ctrl.nuevo_contacto.contacto || !ctrl.nuevo_contacto.telefono || !ctrl.nuevo_contacto.correo">Agregar</button>
				<button class="btn btn-danger btn-sm" ng-click="ctrl.cancelarNuevoContacto()">Cancelar</button>
			</div>
		</div>
	</form>
</div>

<div id="eliminarContacto" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar contacto</h3>

	<form>
		<div class="panel panel-primary">
			<div class="panel-body">
				<p>Explique la razón por la que quiere eliminar el contacto "{{ctrl.eliminar_contacto.contacto}}"</p>
				<textarea class="form-control" style="width:100%;" rows="3" placeholder="Razón..." ng-model="ctrl.eliminar_contacto.comentario"></textarea>
			</div>
			<div class="panel-footer text-center">
				<button class="btn btn-success btn-sm" ng-disabled="!ctrl.eliminar_contacto.comentario" ng-click="ctrl.eliminarContacto(ctrl.eliminar_contacto)">Eliminar</button>
				<button class="btn btn-danger btn-sm" ng-click="ctrl.cancelarEliminacionContacto()">Cancelar</button>
			</div>
		</div>
	</form>
</div>
