<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargandoTarjetas = false;
			ref.cargandoODUs = false;
			ref.isAllSelectedTarjetas = false;
			ref.isAllSelectedODUs = false;
			var hot_tarjetas = "";
			var hot_odus = "";
			
			ref.tarjetas = [];
			ref.odus = [];
			ref.eliminar_Tarjetas = [];
			ref.eliminar_ODUs = [];
			
			listarTarjetas = function(){
				ref.cargandoTarjetas = true;
				$http({
					url:"<?php echo base_url("microondas_tarjetas/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tarjetas = data;
						
						data.forEach(function(dato,ind,objeto){
							objeto[ind].check = false;
						});
						
						ref.cargandoTarjetas = false;
						/*console.log(data);*/
					}
					else
					{
						ref.cargandoTarjetas = false;
					}
				}).error(function(err){
					ref.cargandoTarjetas = false;
				});
			}
			
			listarODUs = function(){
				ref.cargandoODUs = true;
				$http({
					url:"<?php echo base_url("microondas_odus/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.odus = data;
						
						data.forEach(function(dato,ind,objeto){
							objeto[ind].check = false;
						});
						
						ref.cargandoODUs = false;
						/*console.log(data);*/
					}
					else
					{
						ref.cargandoODUs = false;
					}
				}).error(function(err){
					ref.cargandoODUs = false;
				});
			}
			
			ref.disabledTarjetas = function()
			{
				var dis = false;
				
				ref.tarjetas.forEach(function(t){
					dis = dis || t.check;
				});
				
				return dis;
			}
			
			ref.disabledODUs = function()
			{
				var dis = false;
				
				ref.odus.forEach(function(t){
					dis = dis || t.check;
				});
				
				return dis;
			}
			
			ref.nuevas_tarjetas = function(){
				$("#nuevasTarjetas").bPopup({
					amsl: 0
				});
				
				var container = document.getElementById('nuevasTarjetasData');
				hot_tarjetas = new Handsontable(container,
				{
					minRows: 10,
					minCols: 9,
					minSpareRows: 1,
					colHeaders: ["Board Type","NE Type","Slot ID","Hardware Version","NE ID","Board Bar Code","Board BOM Item","Description","Manufacture Date"],
					rowHeaders: true
				});
			}
			
			ref.agregarTarjetas = function(){
				var i = 0;
				var tarjetas_data = [];
				
				while(hot_tarjetas.getData()[i][0] != null)
				{
					var row = hot_tarjetas.getData()[i];
					var linea = {};
					row.forEach(function(dato,ind,objeto){
						linea["board_type"] = objeto[0];
						linea["ne_type"] = objeto[1];
						linea["slot_id"] = objeto[2];
						linea["hardware_version"] = objeto[3];
						linea["idu"] = objeto[4];
						linea["board_bar_code"] = objeto[5];
						linea["board_bom_item"] = objeto[6];
						linea["description"] = objeto[7];
						linea["manufacture_date"] = objeto[8];
					});
					i++;
					tarjetas_data.push(linea);
				}
				
				var data = "data="+encodeURIComponent(JSON.stringify(tarjetas_data));
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("microondas_tarjetas/tarjetas/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevasTarjetas").bPopup().close();
					listarTarjetas();
					hot_tarjetas.destroy();
				}).error(function(err){

				});
			}
			
			ref.cancelarCreacionTarjetas = function(){
				$("#nuevasTarjetas").bPopup().close();
				hot_tarjetas.destroy();
			}
			
			ref.eliminandoTarjetas = function(info){
				info.forEach(function(tarjeta,ind,objeto){
					if(objeto[ind].check)
					{
						ref.eliminar_Tarjetas.push(objeto[ind].id);
					}
				});
				$("#eliminarTarjetas").bPopup();
			}
			
			ref.eliminarTarjetas = function(){
				var data = "data="+JSON.stringify(ref.eliminar_Tarjetas);
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("microondas_tarjetas/tarjetas");?>",
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.tarjetas = [];
						listarTarjetas();
						ref.eliminar_Tarjetas = [];
						ref.isAllSelectedTarjetas = false;
						$("#eliminarTarjetas").bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEliminacionTarjetas = function(){
				ref.eliminar_Tarjetas = [];
				ref.tarjetas.forEach(function(t){
					t.check = false;
				});
				ref.isAllSelectedTarjetas = false;
				$("#eliminarTarjetas").bPopup().close();
			}
			
			ref.nuevas_odus = function(){
				$("#nuevasODUs").bPopup({
					amsl: 0
				});
				
				var container = document.getElementById('nuevasODUsData');
				hot_odus = new Handsontable(container,
				{
					minRows: 10,
					minCols: 6,
					minSpareRows: 1,
					colHeaders: ["NE Type","Frequency","Range of Frequency Point","Produce Time","Factory Information","Software Version"],
					rowHeaders: true
				});
			}
			
			ref.agregarODUs = function(){
				var i = 0;
				var odus_data = [];
				
				while(hot_odus.getData()[i][0] != null)
				{
					var row = hot_odus.getData()[i];
					var linea = {};
					row.forEach(function(dato,ind,objeto){
						linea["ne_type"] = objeto[0];
						linea["frequency"] = objeto[1];
						linea["range_frequency"] = objeto[2];
						linea["produce_time"] = objeto[3];
						linea["factory_information"] = objeto[4];
						linea["software_version"] = objeto[5];
					});
					i++;
					odus_data.push(linea);
				}
				
				var data = "data="+encodeURIComponent(JSON.stringify(odus_data));
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("microondas_odus/odus/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevasODUs").bPopup().close();
					listarODUs();
					hot_odus.destroy();
				}).error(function(err){

				});
			}
			
			ref.cancelarCreacionODUs = function(){
				$("#nuevasODUs").bPopup().close();
				hot_odus.destroy();
			}
			
			ref.eliminandoODUs = function(info){
				info.forEach(function(odu,ind,objeto){
					if(objeto[ind].check)
					{
						ref.eliminar_ODUs.push(objeto[ind].id);
					}
				});
				$("#eliminarODUs").bPopup();
			}
			
			ref.eliminarODUs = function(){
				var data = "data="+JSON.stringify(ref.eliminar_ODUs);
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("microondas_odus/odus");?>",
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.odus = [];
						listarODUs();
						ref.eliminar_ODUs = [];
						ref.isAllSelectedODUs = false;
						$("#eliminarODUs").bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEliminacionODUs = function(){
				ref.eliminar_ODUs = [];
				ref.odus.forEach(function(t){
					t.check = false;
				});
				ref.isAllSelectedODUs = false;
				$("#eliminarODUs").bPopup().close();
			}
			
			ref.selectAllTarjetas = function(){
				ref.tarjetas.forEach(function(tarjeta){
					tarjeta.check = ref.isAllSelectedTarjetas;
				});
			}
			
			ref.tarjetaSeleccionada = function(){
				ref.isAllSelectedTarjetas = ref.tarjetas.every(function(tarjeta){return tarjeta.check;});
			}
			
			ref.selectAllODUs = function(){
				ref.odus.forEach(function(odu){
					odu.check = ref.isAllSelectedODUs;
				});
			}
			
			ref.oduSeleccionada = function(){
				ref.isAllSelectedODUs = ref.odus.every(function(odu){return odu.check;});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			listarTarjetas();
			listarODUs();
		}]);
</script>
</body>
</html>