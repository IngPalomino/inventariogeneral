<script type="text/javascript">
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargando = false;

			ref.sitios = [];
			ref.almacenes = [];
			ref.config = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Buscar sitio',
				maxItems: 1
			};

			ref.tanques_combustible = [];

			ref.nuevoTanqueCombustible = {};
			ref.eliminarTanqueCombustible = {};

			listarTanques = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("index.php/tanques_combustible/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						data.forEach(function(tanque,ind,objeto){
							objeto[ind].capacidad = Number(objeto[ind].capacidad);
							objeto[ind].consumo = Number(objeto[ind].consumo);
							try{ objeto[ind].fecha_instalacion = new Date(objeto[ind].fecha_instalacion + " 00:00:00"); }catch(err){ objeto[ind].fecha_instalacion = null; }
							try{ objeto[ind].created_at = Date.parse(objeto[ind].created_at); }catch(err){ objeto[ind].created_at = null; }
							/*objeto[ind].fecha_instalacion = (objeto[ind].fecha_instalacion != null)? new Date(objeto[ind].fecha_instalacion + " 00:00:00") : null;*/
							objeto[ind].autonomia = function(){
								var autonomia = 0;
								if(this.consumo != 0)
								{
									autonomia = (this.capacidad * .9 / this.consumo);
								}
								return autonomia;
							}
							objeto[ind].fechaToString = function(fecha){
								var string = (fecha != null)? fecha.getDate()+"/"+("00"+(fecha.getMonth()+1)).substr(-2)+"/"+fecha.getFullYear() : "";
								return string;
							}
							
							objeto[ind].logs_tanques_combustible.forEach(function(modificacion,indx,objetox){
								try{ objetox[indx].created_at = Date.parse(objetox[indx].created_at); }catch(err){ objetox[indx].created_at = null; }
							});
						});

						ref.tanques_combustible = data;
						ref.cargando = false;
					}
					else
					{
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			listarSitios = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar_sitios");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sitios = data;
					}
					else
					{
						ref.sitios = [];
					}
				}).error(function(err){

				});
			}
			
			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.almacenes = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.nuevo_tanque = function(){
				$("#nuevoTanqueCombustible").bPopup();
			}

			ref.cancelarCreacionTanque = function(){
				$("#nuevoTanqueCombustible").bPopup().close();
				ref.nuevoTanqueCombustible = {};
			}

			ref.agregarTanqueCombustible = function(info){
				var data = "",
					campos = Object.keys(info);

				campos.forEach(function(campo,ind,objeto){
					if(campo != "fecha_instalacion")
					{
						data += (ind < (objeto.length - 1))? campo+"="+info[campo]+"&" : campo+"="+info[campo] ;
					}
					else
					{
						data += campo+"="+info[campo].toISOString();
					}
				});

				$http({
					url:"<?php echo base_url("index.php/tanques_combustible/tanque/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					listarTanques();
					$("#nuevoTanqueCombustible").bPopup().close();
					ref.nuevoTanqueCombustible = {};
				}).error(function(err){

				});
			}

			ref.eliminandoTanque = function(info){
				ref.eliminarTanqueCombustible = info;
				$("#eliminarTanqueCombustible").bPopup();
			}

			ref.cancelarEliminacion = function(){
				$("#eliminarTanqueCombustible").bPopup().close();
				ref.eliminarTanqueCombustible = {};
			}

			ref.eliminarTanque = function(){
				var data = "comentario="+ref.eliminarTanqueCombustible.comentario;

				$http({
					url:"<?php echo base_url("index.php/tanques_combustible/tanque");?>"+"/"+ref.eliminarTanqueCombustible.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.tanques_combustible = [];
						listarTanques();
						ref.eliminarTanqueCombustible = {};
						$('#eliminarTanqueCombustible').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.editarTanque = function(tanque){
				tanque.editar = true;
			}

			ref.grabarTanque = function(tanque){
				var data = "";
				try{
					data = "tipo_tanque_combustible="+tanque.tipo_tanque_combustible
							+"&capacidad="+tanque.capacidad
							+"&consumo="+tanque.consumo
							+"&fecha_instalacion="+tanque.fecha_instalacion.toISOString();
				}catch(err){
					data = "tipo_tanque_combustible="+tanque.tipo_tanque_combustible
							+"&capacidad="+tanque.capacidad
							+"&consumo="+tanque.consumo
							+"&fecha_instalacion=NULL";
				}

				$http({
					url:"<?php echo base_url("index.php/tanques_combustible/tanque");?>/"+tanque.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					tanque.editar = false;
					listarTanques();
				}).error(function(err){

				});
			}

			ref.cancelarTanque = function(tanque){
				tanque.editar = false;
				listarTanques();
			}

			ref.almacenandoTanque = function(tanque){
				ref.almacenar_Tanque = tanque;
				ref.almacenar_Tanque.tipo_almacen = 1;
				$("#almacenarTanque").bPopup();
			}

			ref.almacenarTanque = function(tanque) {
				var data = "tipo_tanque_combustible="+tanque.tipo_tanque_combustible
							+"&capacidad="+tanque.capacidad
							+"&consumo="+tanque.consumo
							+"&estado="+tanque.estado
							+"&proveniente_sitio="+tanque.proveniente_sitio
							+"&tipo_almacen="+tanque.tipo_almacen
							+"&almacen="+((tanque.tipo_almacen == 1)? tanque.almacen : tanque.a_sitio)
							+"&comentario="+tanque.comentario;

				$http({
					url: "<?php echo base_url("tanques_combustible/tanque_combustible/almacenar") ?>/"+tanque.id,
					method: "POST",
					data: data,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function successCallback(response) {
					ref.tanques_combustible = [];
					listarTanques();
					$("#almacenarTanque").bPopup().close();
				}, function errorCallback(response) {

				});
			}

			ref.cancelarAlmacenamiento = function(){
				ref.almacenar_Tanque = {};
				$("#almacenarTanque").bPopup().close();
			}

			ref.opcionSitio = function(){
				ref.almacenar_Tanque.tipo_almacen = 2;
			}

			ref.opcionAlmacen = function(){
				ref.almacenar_Tanque.tipo_almacen = 1;
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};

			listarTanques();
			listarSitios();
			listarAlmacenes();
		}]);
</script>
</body>
</html>