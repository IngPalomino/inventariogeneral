<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="rectif in ctrl.bancos_baterias">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Bancos de Baterías del Rectificador <span ng-if="rectificador.id == rectif.controlador_rectif" ng-repeat="rectificador in ctrl.rectificadores">{{rectificador.tipo_controlador_rectif.marca}} - {{rectificador.tipo_controlador_rectif.modelo}} | Ubicación: {{rectificador.ubicacion_controlador_rectif.ubicacion_controlador_rectif}}</span>
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody ng-repeat="banco in rectif.bancos">
							<tr class="text-center">
								<td class="success" colspan="5"><button class="btn btn-danger btn-xs" ng-hide="!rectif.editar" ng-click="ctrl.eliminandoCelda(banco)">Eliminar Banco </button>&nbsp;&nbsp;&nbsp;&nbsp; Banco de Baterías {{$index + 1}} &nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-success btn-xs" ng-show="!banco.mostrar" ng-click="ctrl.mostrarCeldas(banco)">Mostrar Celdas</button><button class="btn btn-warning btn-xs" ng-hide="!banco.mostrar" ng-click="ctrl.ocultarCeldas(banco)">Ocultar Celdas</button></td>
							</tr>
							<tr class="info" ng-show="banco.mostrar">
								<th>Marca</th>
								<th>Modelo</th>
								<th>Capacidad</th>
								<th>Fecha de Instalación</th>
								<th>Número de Serie</th>
							</tr>
							<tr class="default" ng-show="banco.mostrar" ng-repeat="celda in banco.celdas_controlador_rectif_bancos_baterias">
								<td>
									<span ng-show="!rectif.editar">{{celda.tipo_bancos_baterias.marca}}</span>
									<select class="form-control" ng-hide="!rectif.editar" ng-model="celda.tipo_bancos_baterias.marca" ng-change="celda.tipo_bancos_baterias.id = '';">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.marcas_tipo_bancos_baterias" value="{{marca.marca}}">{{marca.marca}}</option>
									</select>
								</td>
								<td>
									<span ng-show="!rectif.editar">{{celda.tipo_bancos_baterias.modelo}}</span>
									<select class="form-control" ng-hide="!rectif.editar" ng-model="celda.tipo_bancos_baterias.id">
										<option value="">Elegir modelo</option>
										<option ng-if="celda.tipo_bancos_baterias.marca" ng-repeat="modelo in ctrl.tipo_bancos_baterias | filter:{marca:celda.tipo_bancos_baterias.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
								<td>
									<span ng-if="capacidad.id == celda.tipo_bancos_baterias.id" ng-repeat="capacidad in ctrl.tipo_bancos_baterias">{{capacidad.capacidad}} Ah</span>
								</td>
								<td>
									<span ng-show="!rectif.editar">{{celda.fecha_instalacion | date:"dd/MM/yyyy"}}</span>
									<input class="form-control" type="date" ng-hide="!rectif.editar" ng-model="celda.fecha_instalacion"/>
								</td>
								<td>
									<span ng-show="!rectif.editar">{{celda.numero_serie}}</span>
									<input class="form-control" type="text" ng-hide="!rectif.editar" ng-model="celda.numero_serie"/>
								</td>
							</tr>
							<tr ng-show="banco.mostrar">
								<td colspan="3"></td>
								<td style="font-size:8.5pt;list-style:none;text-align:right;" colspan="2" colspan="2">
									Creado: {{banco.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} <span ng-if="!banco.logs_controlador_rectif_bancos_baterias.length || (banco.logs_controlador_rectif_bancos_baterias.length && (banco.logs_controlador_rectif_bancos_baterias[0].evento.tipo != 'INSERT'))">por Base de Datos</span><span ng-if="banco.logs_controlador_rectif_bancos_baterias.length && (banco.logs_controlador_rectif_bancos_baterias[0].evento.tipo == 'INSERT')">por {{banco.logs_controlador_rectif_bancos_baterias[0].usuario.nombres}} {{banco.logs_controlador_rectif_bancos_baterias[0].usuario.apellido_paterno}}</span><br>
									<span ng-if="banco.logs_controlador_rectif_bancos_baterias.length && (banco.logs_controlador_rectif_bancos_baterias[0].evento.tipo == 'UPDATE')">Actualizado: {{banco.logs_controlador_rectif_bancos_baterias[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{banco.logs_controlador_rectif_bancos_baterias[0].usuario.nombres}} {{banco.logs_controlador_rectif_bancos_baterias[0].usuario.apellido_paterno}}</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="text-center">
					<span class="fa fa-spinner fa-spin fa-2x" ng-if="ctrl.procesando"></span>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editarBanco(rectif)" ng-show="!rectif.editar">Editar</button>
					<button class="btn btn-sm btn-success" ng-click="ctrl.grabarBanco(rectif)" ng-hide="!rectif.editar" ng-disabled="rectif.disabled()">Grabar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarBanco(rectif)" ng-hide="!rectif.editar">Cancelar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoBanco(rectif)" ng-show="!rectif.editar">Eliminar todos los bancos</button>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.bancos_baterias.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.rectificadores.length && !ctrl.cargando">
			<div class="well text-center">
				Para añadir Banco de Baterías primero se debe añadir un Rectificador.
			</div>
		</div>
	</div>
	<div class="row" ng-if="ctrl.rectificadores.length">
		<div class="col-xs-12">
			<button class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:15px;" ng-click="ctrl.nuevo_banco()"><i class="fa fa-plus"></i> Añadir Banco de Baterías</button>
		</div>
	</div>
</div>

<div id="nuevoBanco" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Nuevo Banco de Baterías</h3>
	
	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info" colspan="2">Rectificador</td>
					<td class="default" colspan="2">
						<select class="form-control" ng-model="ctrl.nuevoBanco.controlador_rectif.id">
							<option value="">Elegir rectificador</option>
							<option ng-repeat="rectificador in ctrl.rectificadores" value="{{rectificador.id}}">Marca: {{rectificador.tipo_controlador_rectif.marca}} - Controlador: {{rectificador.tipo_controlador_rectif.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoBanco.tipo_bancos_baterias.marca" ng-change="ctrl.nuevoBanco.tipo_bancos_baterias.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.marcas_tipo_bancos_baterias" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoBanco.tipo_bancos_baterias.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoBanco.tipo_bancos_baterias.marca" ng-repeat="modelo in ctrl.tipo_bancos_baterias | filter:{marca:ctrl.nuevoBanco.tipo_bancos_baterias.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<!--<td class="info">Capacidad/Banco</td>
					<td class="default">
						<div class="input-group">
							<select class="form-control" ng-model="ctrl.nuevoBanco.tipo_bancos_baterias.id">
								<option value="">Elegir capacidad</option>
								<option ng-if="ctrl.nuevoBanco.tipo_bancos_baterias.marca && ctrl.nuevoBanco.tipo_bancos_baterias.modelo" ng-repeat="capacidad in ctrl.tipo_bancos_baterias | filter:{marca:ctrl.nuevoBanco.tipo_bancos_baterias.marca, modelo:ctrl.nuevoBanco.tipo_bancos_baterias.modelo}" value="{{capacidad.id}}">{{capacidad.capacidad}}</option>
							</select>
							<div class="input-group-addon">Ah</div>
						</div>
					</td>-->
					<td class="info">Cantidad de bancos</td>
					<td class="default">
						<form method="post">
							<input class="form-control" type="number" name="cantidad" step="any" min="1" ng-model="ctrl.nuevoBanco.cantidad_bancos"/>
						</form>
					</td>
					<td class="info">Fecha de instalación</td>
					<td class="default">
						<input class="form-control" type="text" id="datepicker" size="30" ng-model="ctrl.nuevoBanco.fecha_instalacion"/>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarBanco(ctrl.nuevoBanco)" ng-disabled="!ctrl.nuevoBanco.controlador_rectif.id || !ctrl.nuevoBanco.tipo_bancos_baterias.id || !ctrl.nuevoBanco.cantidad_bancos">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionBanco()">Cancelar</button>
	</form>
</div>

<div id="eliminarBanco" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar Bancos de Baterías</h3>
	
	<p>¿Está seguro de eliminar todos los bancos de baterías?</p>
	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Banco.comentario"></textarea>
	
	<button class="btn btn-success" ng-click="ctrl.eliminarBanco()" ng-disabled="!ctrl.eliminar_Banco.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>

<div id="eliminarCelda" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar Banco de Baterías</h3>
	
	<p>¿Está seguro de eliminar el banco de baterías?</p>
	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Celda.comentario"></textarea>
	
	<button class="btn btn-success" ng-click="ctrl.eliminarCelda()" ng-disabled="!ctrl.eliminar_Celda.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionCelda()">Cancelar</button>
</div>