<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargandoDetailed = false;
			ref.cargando = false;
			ref.isAllSelectedDetailed = false;
			ref.isAllSelected = false;
			var hot_wimax_detailed = "";
			var hot_wimax = ""
			
			ref.wimax_detailed = [];
			ref.wimax = [];
			ref.eliminar_WimaxDetailed = [];
			ref.eliminar_Wimax = [];
			
			listarWimaxDetailed = function(){
				ref.cargandoDetailed = true;
				$http({
					url:"<?php echo base_url("wimax_detailed/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.wimax_detailed = data;
						/*console.log(data);*/
						data.forEach(function(dato,ind,objeto){
							objeto[ind].check = false;
						});
						ref.cargandoDetailed = false;
					}
					else
					{
						ref.cargandoDetailed = false;
					}
				}).error(function(err){
					ref.cargandoDetailed = false;
				});
			}

			listarWimax = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("wimax/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.wimax = data;
						/*console.log(data);*/
						data.forEach(function(dato,ind,objeto){
							objeto[ind].check = false;
						});
						ref.cargando = false;
					}
					else
					{
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			ref.disabledWimaxDetailed = function()
			{
				var dis = false;
				
				ref.wimax_detailed.forEach(function(t){
					dis = dis || t.check;
				});
				
				return dis;
			}

			ref.disabledWimax = function()
			{
				var dis = false;
				
				ref.wimax.forEach(function(t){
					dis = dis || t.check;
				});
				
				return dis;
			}

			ref.nuevo_wimaxDetailed = function(){
				$("#nuevoWimaxDetailed").bPopup({
					amsl: 0
				});
				
				var container = document.getElementById('nuevoWimaxDetailedData');
				hot_wimax_detailed = new Handsontable(container,
				{
					minRows: 10,
					minCols: 7,
					minSpareRows: 1,
					colHeaders: ["BTS Name","Entity Type","Entity Serial Number","Entity Hardware Version Number","Entity Hardware Revision Number","Entity Operational Software Version Number","Entity Boot Software Version Number"],
					rowHeaders: true
				});
			}
			
			ref.agregarWimaxDetailed = function(){
				var i = 0;
				var wimax_detailed_data = [];
				
				while(hot_wimax_detailed.getData()[i][0] != null)
				{
					var row = hot_wimax_detailed.getData()[i];
					var linea = {};
					row.forEach(function(dato,ind,objeto){
						linea["bts_name"] = objeto[0]
						linea["entity_type"] = objeto[1];
						linea["entity_serial_number"] = objeto[2];
						linea["entity_hardware_version_number"] = objeto[3];
						linea["entity_hardware_revision_number"] = objeto[4];
						linea["entity_operational_software_version_number"] = objeto[5];
						linea["entity_boot_software_version_number"] = objeto[6];
					});
					i++;
					wimax_detailed_data.push(linea);
				}
				
				var data = "data="+encodeURIComponent(JSON.stringify(wimax_detailed_data));
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("wimax_detailed/wimax/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevoWimaxDetailed").bPopup().close();
					listarWimaxDetailed();
					hot_wimax_detailed.destroy();
				}).error(function(err){

				});
			}
			
			ref.cancelarCreacionWimaxDetailed = function(){
				$("#nuevoWimaxDetailed").bPopup().close();
				hot_wimax_detailed.destroy();
			}
			
			ref.eliminandoWimaxDetailed = function(info){
				info.forEach(function(wimax,ind,objeto){
					if(objeto[ind].check)
					{
						ref.eliminar_WimaxDetailed.push(objeto[ind].id);
					}
				});
				$("#eliminarWimaxDetailed").bPopup();
			}
			
			ref.eliminarWimaxDetailed = function(){
				var data = "data="+JSON.stringify(ref.eliminar_WimaxDetailed);
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("wimax_detailed/wimax");?>",
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.wimax_detailed = [];
						listarWimaxDetailed();
						ref.eliminar_WimaxDetailed = [];
						ref.isAllSelectedDetailed = false;
						$("#eliminarWimaxDetailed").bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEliminacionWimaxDetailed = function(){
				ref.eliminar_WimaxDetailed = [];
				ref.wimax_detailed.forEach(function(t){
					t.check = false;
				});
				ref.isAllSelectedDetailed = false;
				$("#eliminarWimaxDetailed").bPopup().close();
			}

			ref.nuevo_wimax = function(){
				$("#nuevoWimax").bPopup({
					amsl: 0
				});
				
				var container = document.getElementById('nuevoWimaxData');
				hot_wimax = new Handsontable(container,
				{
					minRows: 10,
					minCols: 7,
					minSpareRows: 1,
					colHeaders: ["BTS Model","WiMax Code","BTS Name","BTS Number","IP Address","Entity Type","Entity Count"],
					rowHeaders: true
				});
			}
			
			ref.agregarWimax = function(){
				var i = 0;
				var wimax_data = [];
				
				while(hot_wimax.getData()[i][0] != null)
				{
					var row = hot_wimax.getData()[i];
					var linea = {};
					row.forEach(function(dato,ind,objeto){
						linea["bts_model"] = objeto[0];
						linea["wimax_code"] = objeto[1];
						linea["bts_name"] = objeto[2];
						linea["bts_number"] = objeto[3];
						linea["ip_address"] = objeto[4];
						linea["entity_type"] = objeto[5];
						linea["entity_count"] = objeto[6];
					});
					i++;
					wimax_data.push(linea);
				}
				
				var data = "data="+encodeURIComponent(JSON.stringify(wimax_data));
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("wimax/wimax/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevoWimax").bPopup().close();
					listarWimax();
					hot_wimax.destroy();
				}).error(function(err){

				});
			}
			
			ref.cancelarCreacionWimax = function(){
				$("#nuevoWimax").bPopup().close();
				hot_wimax.destroy();
			}
			
			ref.eliminandoWimax = function(info){
				info.forEach(function(wimax,ind,objeto){
					if(objeto[ind].check)
					{
						ref.eliminar_Wimax.push(objeto[ind].id);
					}
				});
				$("#eliminarWimax").bPopup();
			}
			
			ref.eliminarWimax = function(){
				var data = "data="+JSON.stringify(ref.eliminar_Wimax);
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("wimax/wimax");?>",
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.wimax = [];
						listarWimax();
						ref.eliminar_Wimax = [];
						ref.isAllSelected = false;
						$("#eliminarWimax").bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEliminacionWimax = function(){
				ref.eliminar_Wimax = [];
				ref.wimax.forEach(function(t){
					t.check = false;
				});
				ref.isAllSelected = false;
				$("#eliminarWimax").bPopup().close();
			}

			ref.selectAllDetailed = function(){
				ref.wimax_detailed.forEach(function(wimax){
					wimax.check = ref.isAllSelectedDetailed;
				});
			}
			
			ref.seleccionadoDetailed = function(){
				ref.isAllSelectedDetailed = ref.wimax_detailed.every(function(wimax){return wimax.check;});
			}

			ref.selectAll = function(){
				ref.wimax.forEach(function(wimax){
					wimax.check = ref.isAllSelected;
				});
			}
			
			ref.seleccionado = function(){
				ref.isAllSelected = ref.wimax.every(function(wimax){return wimax.check;});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			listarWimaxDetailed();
			listarWimax();
		}]);
</script>
</body>
</html>