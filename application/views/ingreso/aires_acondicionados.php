<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="aire in ctrl.aires_acondicionados" ng-if="ctrl.aires_acondicionados.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Aire Acondicionado {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr>
								<td class="info text-right" colspan="2">Refrigerante</td>
								<td class="default" colspan="1">
									<span ng-show="!aire.editar">{{aire.tipo_refrigerante.tipo_refrigerante}}</span>
									<select class="form-control" ng-hide="!aire.editar" ng-model="aire.tipo_refrigerante.id">
										<option value="">Elegir refrigerante</option>
										<option ng-repeat="refrigerante in ctrl.tipo_refrigerante" value="{{refrigerante.id}}">{{refrigerante.tipo_refrigerante}}</option>
									</select>
								</td>
								<td></td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">
									<span ng-show="!aire.editar">{{aire.tipo_aires_acondicionados.marca}}</span>
									<select class="form-control" ng-hide="!aire.editar" ng-model="aire.tipo_aires_acondicionados.marca" ng-change="aire.tipo_aires_acondicionados.id = ''; aire.tipo_compresor.id = ''; aire.tipo_aires_acondicionados.tipo_equipo = '';">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.tipo_aires_acondicionados.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
									</select>
								</td>
								<td class="info">Tipo de Equipo</td>
								<td class="default">
									<span ng-show="!aire.editar">{{aire.tipo_aires_acondicionados.tipo_equipo}}</span>
									<select class="form-control" ng-hide="!aire.editar" ng-model="aire.tipo_aires_acondicionados.tipo_equipo" ng-change="ctrl.cambioTipo(aire.tipo_aires_acondicionados.tipo_equipo,aire)">
										<option value="">Elegir tipo de equipo</option>
										<option ng-if="aire.tipo_aires_acondicionados.marca" ng-repeat="equipo in ctrl.tipo_aires_acondicionados.tipos_equipo | filter:{marca:aire.tipo_aires_acondicionados.marca}" value="{{equipo.tipo_equipo}}">{{equipo.tipo_equipo}}</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="info">Capacidad</td>
								<td class="default">
									<div class="input-group">
										<span ng-show="!aire.editar">{{aire.tipo_aires_acondicionados.capacidad}} Ton</span>
										<select class="form-control" ng-hide="!aire.editar" ng-model="aire.tipo_aires_acondicionados.capacidad" ng-if="aire.tipo_aires_acondicionados.tipo_equipo == 'Mochila'">
											<option value="">Elegir capacidad</option>
											<option ng-if="aire.tipo_aires_acondicionados.marca && aire.tipo_aires_acondicionados.tipo_equipo" ng-repeat="capacidad in ctrl.tipo_aires_acondicionados.capacidades | filter:{marca:aire.tipo_aires_acondicionados.marca, tipo_equipo:aire.tipo_aires_acondicionados.tipo_equipo}" value="{{capacidad.capacidad}}">{{capacidad.capacidad}}</option>
										</select>
										<div class="input-group-addon" ng-hide="!aire.editar" ng-if="aire.tipo_aires_acondicionados.tipo_equipo == 'Mochila'">Ton</div>
										<select class="form-control" ng-hide="!aire.editar" ng-model="aire.tipo_aires_acondicionados.id" ng-if="aire.tipo_aires_acondicionados.tipo_equipo != 'Mochila'" ng-change="ctrl.elegirCapacidad(aire.tipo_aires_acondicionados.id,aire)">
											<option value="">Elegir capacidad</option>
											<option ng-if="(aire.tipo_aires_acondicionados.marca == capacidad.marca) && (aire.tipo_aires_acondicionados.tipo_equipo == capacidad.tipo_equipo)" ng-repeat="capacidad in ctrl.tipo_aires_acondicionados.capacidades2" value="{{capacidad.id}}">{{capacidad.capacidad}}</option>
										</select>
										<div class="input-group-addon" ng-hide="!aire.editar" ng-if="aire.tipo_aires_acondicionados.tipo_equipo != 'Mochila'">Ton</div>
									</div>
								</td>
								<td class="info">Fecha de Instalación</td>
								<td class="default">
									<span ng-show="!aire.editar">{{aire.fecha_instalacion | date:"dd/MM/yyyy"}}</span>
									<input class="form-control" type="date" ng-hide="!aire.editar" ng-model="aire.fecha_instalacion"/>
								</td>
							</tr>
							<tr ng-if="aire.tipo_aires_acondicionados.tipo_equipo == 'Mochila'">
								<td class="info">Modelo</td>
								<td class="default">
									<span ng-show="!aire.editar">{{aire.tipo_aires_acondicionados.modelo}}</span>
									<select class="form-control" ng-hide="!aire.editar" ng-model="aire.tipo_aires_acondicionados.id" ng-change="ctrl.elegirCapacidad(aire.tipo_aires_acondicionados.id,aire)">
										<option value="">Elegir modelo</option>
										<option ng-if="aire.tipo_aires_acondicionados.marca && aire.tipo_aires_acondicionados.capacidad" ng-repeat="modelo in ctrl.tipo_aires_acondicionados.modelos | filter:{marca:aire.tipo_aires_acondicionados.marca, capacidad:aire.tipo_aires_acondicionados.capacidad}" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
								<td class="info">Número de Serie</td>
								<td class="default">
									<span ng-show="!aire.editar">{{aire.num_serie_aire_acon}}</span>
									<input class="form-control" type="text" ng-hide="!aire.editar" ng-model="aire.num_serie_aire_acon"/>
								</td>
							</tr>
							<tr style="background-color:#d7bde2;" class="text-center">
								<td colspan="4">Compresor</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">
									<span ng-show="!aire.editar">{{aire.tipo_compresor.marca}}</span>
									<select class="form-control" ng-hide="!aire.editar" ng-model="aire.tipo_compresor.marca" ng-change="aire.tipo_compresor.id = '';">
										<option value="">Elegir marca</option>
										<option ng-if="aire.tipo_aires_acondicionados.capacidad" ng-repeat="marca in ctrl.tipo_compresor.marcas | filter:{capacidad:aire.tipo_aires_acondicionados.capacidad}" value="{{marca.marca}}">{{marca.marca}}</option>
									</select>
								</td>
								<td class="info">Modelo</td>
								<td class="default">
									<span ng-show="!aire.editar">{{aire.tipo_compresor.modelo}}</span>
									<select class="form-control" ng-hide="!aire.editar" ng-model="aire.tipo_compresor.id">
										<option value="">Elegir modelo</option>
										<option ng-if="aire.tipo_compresor.marca" ng-repeat="modelo in ctrl.tipo_compresor.modelos | filter:{marca:aire.tipo_compresor.marca, capacidad:aire.tipo_aires_acondicionados.capacidad}" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Tipo</td>
								<td class="default" colspan="2">
									<span ng-if="aire.tipo_compresor.id == tipo.id" ng-repeat="tipo in ctrl.tipo_compresor.modelos">{{tipo.tipo}}</span>
								</td>
							</tr>
							<tr class="success text-center" ng-if-start="(aire.tipo_aires_acondicionados.tipo_equipo) && (aire.tipo_aires_acondicionados.tipo_equipo != 'Mochila')">
								<td colspan="4">Condensador</td>
							</tr>
							<tr>
								<td class="info">Modelo</td>
								<td class="default">
									<span ng-show="!aire.editar">{{aire.tipo_condensador.modelo}}</span>
									<select class="form-control" ng-hide="!aire.editar" ng-model="aire.tipo_condensador.id">
										<option value="">Elegir modelo condensador</option>
										<option ng-repeat="condensador in ctrl.tipo_condensador | filter:{capacidad:aire.tipo_aires_acondicionados.capacidad}" value="{{condensador.id}}">{{condensador.modelo}}</option>
									</select>
								</td>
								<td class="info">Número de Serie</td>
								<td class="default">
									<span ng-show="!aire.editar">{{aire.num_serie_condensador}}</span>
									<input class="form-control" type="text" ng-hide="!aire.editar" ng-model="aire.num_serie_condensador"/>
								</td>
							</tr>
							<tr style="background-color:#fae5d3" class="text-center">
								<td colspan="4">Evaporador</td>
							</tr>
							<tr ng-if-end>
								<td class="info">Modelo</td>
								<td class="default">
									<span ng-show="!aire.editar">{{aire.tipo_evaporador.modelo}}</span>
									<select class="form-control" ng-hide="!aire.editar" ng-model="aire.tipo_evaporador.id">
										<option value="">Elegir modelo</option>
										<option ng-repeat="modelo in ctrl.tipo_evaporador | filter:{capacidad:aire.tipo_aires_acondicionados.capacidad}" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
								<td class="info">Número de Serie</td>
								<td class="default">
									<span ng-show="!aire.editar">{{aire.num_serie_evaporador}}</span>
									<input class="form-control" type="text" ng-hide="!aire.editar" ng-model="aire.num_serie_evaporador"/>
								</td>
							</tr>
							<tr>
								<td colspan="2"></td>
								<td colspan="2" style="font-size:8.5pt;list-style:none;text-align:right;">
									<span ng-if="!aire.logs_aires_acondicionados.length || aire.logs_aires_acondicionados[0].evento.tipo != 'INSERT'">Creado: {{aire.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>
									<span ng-if="aire.logs_aires_acondicionados[0].evento.tipo == 'INSERT'">Creado: {{aire.logs_aires_acondicionados[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{aire.logs_aires_acondicionados[0].usuario.nombres}} {{aire.logs_aires_acondicionados[0].usuario.apellido_paterno}}</span>
									<br>
									<span ng-if="aire.logs_aires_acondicionados[1].evento.tipo == 'UPDATE'">Actualizado: {{aire.logs_aires_acondicionados[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{aire.logs_aires_acondicionados[1].usuario.nombres}} {{aire.logs_aires_acondicionados[1].usuario.apellido_paterno}}</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="text-center">
					<span class="fa fa-spinner fa-spin fa-2x" ng-if="ctrl.procesando"></span>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editarAire(aire)" ng-show="!aire.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
					<button class="btn btn-sm btn-success" ng-click="ctrl.grabarAire(aire)" ng-hide="!aire.editar" ng-disabled="aire.disabled()">Grabar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarAire(aire)" ng-hide="!aire.editar">Cancelar</button>
					<button class="btn btn-sm btn-info" ng-click="ctrl.almacenandoAire(aire)" ng-show="!aire.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Almacenar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoAire(aire)" ng-show="!aire.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.aires_acondicionados.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:15px;" ng-click="ctrl.nuevo_aire()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir Aire Acondicionado</button>
		</div>
	</div>
</div>

<div id="nuevoAire" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Nuevo Aire Acondicionado</h3>
	
	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info" colspan="2">Refrigerante</td>
					<td class="default" colspan="2">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_refrigerante">
							<option value="">Elegir refrigerante</option>
							<option ng-repeat="refrigerante in ctrl.tipo_refrigerante" value="{{refrigerante.id}}">{{refrigerante.tipo_refrigerante}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_aires_acondicionados.marca" ng-change="ctrl.nuevoAire.tipo_aires_acondicionados.id = ''; ctrl.nuevoAire.tipo_compresor.id = ''; ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_aires_acondicionados.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Tipo de Equipo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo" ng-change="ctrl.cambioTipoNuevo(ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo,nuevoAire)">
							<option value="">Elegir tipo de equipo</option>
							<option ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.marca" ng-repeat="equipo in ctrl.tipo_aires_acondicionados.tipos_equipo | filter:{marca:ctrl.nuevoAire.tipo_aires_acondicionados.marca}" value="{{equipo.tipo_equipo}}">{{equipo.tipo_equipo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Capacidad</td>
					<td class="default">
						<div class="input-group">
							<select class="form-control" ng-model="ctrl.nuevoAire.tipo_aires_acondicionados.capacidad" ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo == 'Mochila'">
								<option value="">Elegir capacidad</option>
								<option ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.marca && ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo" ng-repeat="capacidad in ctrl.tipo_aires_acondicionados.capacidades | filter:{marca:ctrl.nuevoAire.tipo_aires_acondicionados.marca, tipo_equipo:ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo}" value="{{capacidad.capacidad}}">{{capacidad.capacidad}}</option>
							</select>
							<div ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo == 'Mochila'" class="input-group-addon">Ton</div>
							<select class="form-control" ng-model="ctrl.nuevoAire.tipo_aires_acondicionados.id" ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo != 'Mochila'" ng-change="ctrl.elegirCapacidadNuevo(ctrl.nuevoAire.tipo_aires_acondicionados.id,ctrl.nuevoAire)">
								<option value="">Elegir capacidad</option>
								<option ng-if="(ctrl.nuevoAire.tipo_aires_acondicionados.marca == capacidad.marca) && (ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo == capacidad.tipo_equipo)" ng-repeat="capacidad in ctrl.tipo_aires_acondicionados.capacidades2 | filter:{marca:ctrl.nuevoAire.tipo_aires_acondicionados.marca, tipo_equipo:ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo}" value="{{capacidad.id}}">{{capacidad.capacidad}}</option>
							</select>
							<div ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo != 'Mochila'" class="input-group-addon">Ton</div>
						</div>
					</td>
					<td class="info">Fecha de Instalación</td>
					<td class="default">
						<input class="form-control" type="text" id="datepicker" size="30" ng-model="ctrl.nuevoAire.fecha_instalacion"/>
					</td>
				</tr>
				<tr ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo == 'Mochila'">
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_aires_acondicionados.id" ng-change="ctrl.elegirCapacidadNuevo(ctrl.nuevoAire.tipo_aires_acondicionados.id,ctrl.nuevoAire)">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.marca && ctrl.nuevoAire.tipo_aires_acondicionados.capacidad" ng-repeat="modelo in ctrl.tipo_aires_acondicionados.modelos | filter:{marca:ctrl.nuevoAire.tipo_aires_acondicionados.marca, capacidad:ctrl.nuevoAire.tipo_aires_acondicionados.capacidad}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de Serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="ctrl.nuevoAire.num_serie_aire_acon"/>
					</td>
				</tr>
				<tr style="background-color:#d7bde2;" class="text-center">
					<td colspan="4">Compresor</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_compresor.marca" ng-change="ctrl.nuevoAire.tipo_compresor.id = '';">
							<option value="">Elegir marca</option>
							<option ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.capacidad" ng-repeat="marca in ctrl.tipo_compresor.marcas | filter:{capacidad:ctrl.nuevoAire.tipo_aires_acondicionados.capacidad}" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_compresor.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoAire.tipo_compresor.marca" ng-repeat="modelo in ctrl.tipo_compresor.modelos | filter:{marca:ctrl.nuevoAire.tipo_compresor.marca, capacidad:ctrl.nuevoAire.tipo_aires_acondicionados.capacidad}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr class="success text-center" ng-if-start="(ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo) && (ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo != 'Mochila')">
					<td colspan="4">Condensador</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_condensador.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.capacidad" ng-repeat="condensador in ctrl.tipo_condensador | filter:{capacidad:ctrl.nuevoAire.tipo_aires_acondicionados.capacidad}" value="{{condensador.id}}">{{condensador.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de Serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="ctrl.nuevoAire.num_serie_condensador"/>
					</td>
				</tr>
				<tr style="background-color:#fae5d3" class="text-center">
					<td colspan="4">Evaporador</td>
				</tr>
				<tr ng-if-end>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_evaporador.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.capacidad" ng-repeat="modelo in ctrl.tipo_evaporador | filter:{capacidad:ctrl.nuevoAire.tipo_aires_acondicionados.capacidad}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de Serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="ctrl.nuevoAire.num_serie_evaporador"/>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarAire(ctrl.nuevoAire)" ng-disabled="!ctrl.nuevoAire.tipo_aires_acondicionados.id || !ctrl.nuevoAire.tipo_compresor.id || !ctrl.nuevoAire.tipo_condensador || !ctrl.nuevoAire.tipo_evaporador">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionAire()">Cancelar</button>
	</form>
</div>

<div id="eliminarAire" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar Aire Acondicionado</h3>
	
	<p>¿Está seguro de eliminar el aire acondicionado?</p>
	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Aire.comentario"></textarea>
	
	<button class="btn btn-success" ng-click="ctrl.eliminarAire()" ng-disabled="!ctrl.eliminar_Aire.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>

<div id="almacenarAire" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Almacenar Aire Acondicionado</h3>

	<p>Elegir destino:</p>

	<div style="max-width:400px">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a data-toggle="tab" href="#almacen" ng-click="ctrl.opcionAlmacen()">Almacén</a></li>
			<li><a data-toggle="tab" href="#sitio" ng-click="ctrl.opcionSitio()">Sitio</a></li>
		</ul>
		<div class="tab-content">
			<div id="almacen" class="tab-pane fade in active">
				<select class="form-control" ng-model="ctrl.almacenar_Aire.almacen">
					<option value="">Elegir almacén</option>
					<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
				</select>
			</div>
			<div id="sitio" class="tab-pane fade">
				<label style="width:100%">
					<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenar_Aire.a_sitio"></selectize>
				</label>
			</div>
		</div>
		<select class="form-control" ng-model="ctrl.almacenar_Aire.estado">
			<option value="">Seleccionar estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select>
	</div><br>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea almacenar el elemento..." ng-model="ctrl.almacenar_Aire.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.almacenarAire(ctrl.almacenar_Aire)" ng-disabled="!ctrl.almacenar_Aire.estado || !ctrl.almacenar_Aire.comentario || (ctrl.almacenar_Aire.tipo_almacen == 1 && !ctrl.almacenar_Aire.almacen) || (ctrl.almacenar_Aire.tipo_almacen == 2 && !ctrl.almacenar_Aire.a_sitio)">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamiento()">Cancelar</button>
</div>