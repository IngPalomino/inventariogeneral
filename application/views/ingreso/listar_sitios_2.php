<style type="text/css">
    .onoffswitch {
        position: relative; width: 90px;
        -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
    }
    .onoffswitch-checkbox {
        display: none;
    }
    .onoffswitch-label {
        display: block; overflow: hidden; cursor: pointer;
        border: 2px solid #999999; border-radius: 20px;
    }
    .onoffswitch-inner {
        display: block; width: 200%; margin-left: -100%;
        transition: margin 0.3s ease-in 0s;
    }
    .onoffswitch-inner:before, .onoffswitch-inner:after {
        display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
        font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
        box-sizing: border-box;
    }
    .onoffswitch-inner:before {
        content: "ON AIR";
        padding-left: 10px;
        background-color: #34A7C1; color: #FFFFFF;
    }
    .onoffswitch-inner:after {
        content: "OFF AIR";
        padding-right: 10px;
        background-color: #EEEEEE; color: #999999;
        text-align: right;
    }
    .onoffswitch-switch {
        display: block; width: 18px; margin: 6px;
        background: #FFFFFF;
        position: absolute; top: 0; bottom: 0;
        right: 56px;
        border: 2px solid #999999; border-radius: 20px;
        transition: all 0.3s ease-in 0s; 
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
        margin-left: 0;
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
        right: 0px; 
    }        
    .redtext{background-color: transparent !important;color: red;font-weight: bold;display: none;}
</style>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Resumen
				</div>
				<div class="panel-body" style="height:400px;overflow:scroll;">
					<div class="row">
						<div class="col-xs-6">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Tipo</th>
										<th>Cantidad</th>
									</tr>
								</thead>
								<tbody>
									<tr class="ng-cloak" ng-repeat="tipo in ctrl.resumen.resumen01">
										<td>{{tipo.tipo}}</td>
										<td>{{tipo.cantidad}}</td>
									</tr>
									<tr>
										<td>Total</td>
										<td>{{ctrl.resumen.resumen01.total()}}</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-xs-6">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Proyecto</th>
										<th>Cantidad</th>
									</tr>
								</thead>
								<tbody>
									<tr class="ng-cloak" ng-repeat="proyecto in ctrl.resumen.resumen02">
										<td>{{proyecto.proyecto || "----"}}</td>
										<td>{{proyecto.cantidad}}</td>
									</tr>
									<tr>
										<td>Total</td>
										<td>{{ctrl.resumen.resumen02.total()}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-default btn-sm" onclick="$('#crearSitio').bPopup();" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo)? "false" : "true"; ?>"><span class="fa fa-plus"></span> Crear Sitio</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="crearSitio" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Crear sitio</h3>

	<form class="panel panel-primary">
		<div class="panel-heading tab-headings">
			<nav class="navbar">
				<div class="container-fluid">
					<div class="navbar-header" style="color:rgb(245,121,23);">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-busqueda">
							<span class="sr-only" style="color:rgb(245,121,23);">Toggle navigation</span>
							<span class="fa fa-bars"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="navbar-busqueda">
						<b><ul  class="nav navbar-nav tabscreado">
								<li style="background-color: #fff;">
									<a onclick="habilitafecha()" style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(1)" href>Nombre y Ubicación</a>
								</li>
								<li>
									<a onclick="habilitafecha()" style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(2)" href>Mantenimiento y características físicas</a>
								</li>
								<li>
									<a onclick="habilitafecha()" style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(3)" href>Operadores y Acceso</a>
								</li>
								<li>
									<a onclick="habilitafecha()" style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(4)" href>Proyectos</a>
								</li>
								<li>
									<a onclick="habilitafecha()" style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(5)" href>Tecnologías</a>
								</li>
						</ul></b>
					</div>
				</div>
			</nav>
		</div>




		<div class="panel-body" id="principal" ng-if="ctrl.info.index == 1" style="height:350px;overflow:auto;">
			<table class="table">
				<tbody>
					<tr>
						<td>Código <span class="obliga">*</span></td>
						<td>							
							<input   ng-blur="ctrl.verifica_codigo()" type="text" name="codigo" ng-model="ctrl.sitioNuevo.codigo" class="form-control input-sm codigo-site-new">
							<br><span class="redtext">El Código ya esta en ON AIR</span>
						</td>
					</tr>
					<tr>
						<td>Nombre <span class="obliga">*</span></td>
						<td>
							<input type="text" name="nombre" ng-model="ctrl.sitioNuevo.nombre" class="form-control input-sm">

						</td>
					</tr>
					<tr>
						<td>
							Nombre completo
						</td>
						<td>
							{{ctrl.sitioNuevo.codigo}}_{{ctrl.sitioNuevo.nombre}}
						</td>
					</tr>
					<tr>
						<td>Prioridad <span class="obliga">*</span></td>
						<td>
							<select name="prioridad" ng-model="ctrl.sitioNuevo.prioridad" class="form-control input-sm">
								<option value="0">P0</option>
								<option value="1">P1</option>
								<option value="2">P2</option>
								<option value="3">P3</option>
								<option value="4">P4</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Dirección</td>
						<td>
							<input type="text" name="direccion" ng-model="ctrl.sitioNuevo.direccion" class="form-control input-sm">
						</td>
					</tr>
					<tr>
						<td>Zona <span class="obliga">*</span></td>
						<td>
							<div class="input-group input-group-sm">
								<select class="form-control" ng-model="ctrl.departamento" ng-change="ctrl.listar_provincias(ctrl.departamento)">
									<option value="">Elegir departamento</option>
									<option ng-repeat="d in ctrl.departamentos">{{d.departamento}}</option>
								</select>
								<span class="input-group-addon"></span>
								<select class="form-control" ng-model="ctrl.provincia" ng-change="ctrl.listar_distritos(ctrl.departamento,ctrl.provincia)">
									<option value="">Elegir provincia</option>
									<option ng-repeat="p in ctrl.provincias">{{p.provincia}}</option>
								</select>
								<span class="input-group-addon"></span>
								<select class="form-control" ng-model="ctrl.sitioNuevo.zona_peru">
									<option value="">Elegir distrito</option>
									<option ng-repeat="d in ctrl.distritos" value="{{d.id}}">{{d.distrito}}</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>Coordenadas y Altitud</td>
						<td>
							<div class="input-group input-group-sm">
								<span class="input-group-addon">Latitud:</span>
								<input type="number" name="latitud" class="form-control" ng-model="ctrl.sitioNuevo.latitud">
								<span class="input-group-addon">°</span>
								<span class="input-group-addon">Longitud:</span>
								<input type="number" name="longitud" class="form-control" ng-model="ctrl.sitioNuevo.longitud">
								<span class="input-group-addon">°</span>
								<input type="number" min="0" max="99999" name="altitud" class="form-control" ng-model="ctrl.sitioNuevo.altitud">
								<span class="input-group-addon">m.s.n.m.</span>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="panel-body" id="mantenimiento" ng-if="ctrl.info.index == 2" style="height:350px;overflow:auto;">
			<table class="table">
				<tbody>
					<tr>
						<td>CRM</td>
						<td>
							<select class="form-control input-sm" ng-model="ctrl.sitioNuevo.crm">
								<option value="">Elegir CRM</option>
								<option value="1">NORTE</option>
								<option value="2">SUR</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Supervisor</td>
						<td>
							<select class="form-control input-sm" ng-model="ctrl.sitioNuevo.supervisor">
								<option value="">Elegir Supervisor</option>
								<option value="19">Percy Rodriguez</option>
								<option value="23">Franklin Medina</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Zonales</td>
						<td>
							<select class="form-control input-sm" ng-repeat="oym in ctrl.sitioNuevo.oym_zonal" ng-model="ctrl.sitioNuevo.oym_zonal[$index].oym_zonal">
								<option value="">Elegir O&M</option>
								<option ng-repeat="z in ctrl.zonales" value="{{z.id}}">{{z.nombre}} {{z.apellido}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Tipo de solución</td>
						<td>
							<select class="form-control input-sm" ng-model="ctrl.sitioNuevo.tipo_solucion">
								<option value="">Elegir tipo de solución</option>
								<option ng-repeat="s in ctrl.soluciones" value="{{s.id}}">{{s.tipo_solucion}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Proveedor de mantenimiento</td>
						<td>
							<select class="form-control input-sm" ng-model="ctrl.sitioNuevo.proveedor_mantenimiento">
								<option value="">Elegir proveedor de mantenimiento</option>
								<option ng-repeat="pm in ctrl.proveedores_mantenimiento" value="{{pm.id}}">{{pm.proveedor_mantenimiento}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Tipo de torre y altura de torre</td>
						<td>
							<div class="input-group input-group-sm">
								<select class="form-control" ng-model="ctrl.sitioNuevo.tipo_torre">
									<option value="">Elegir tipo de torre</option>
									<option ng-repeat="tt in ctrl.tipos_torre" value="{{tt.id}}">{{tt.tipo_torre}}</option>
								</select>
								<span class="input-group-addon"></span>
								<input type="number" min="0" max="99999" class="form-control" ng-model="ctrl.sitioNuevo.altura_torre">
								<span class="input-group-addon">m</span>
							</div>
						</td>
					</tr>
					<tr>
						<td>Rooftop y altura de edificación</td>
						<td>
							<div class="input-group input-group-sm">
								<span class="input-group-addon">
									<input type="checkbox" name="greenfield_rooftop" ng-model="ctrl.sitioNuevo.greenfield_rooftop">
								</span>
								<input type="number" min="0" max="99999" class="form-control" ng-model="ctrl.sitioNuevo.altura_edificacion" ng-disabled="!ctrl.sitioNuevo.greenfield_rooftop">
								<span class="input-group-addon">m</span>
							</div>
						</td>
					</tr>
					<tr>
						<td>Camuflaje</td>
						<td>
							<select class="form-control" ng-model="ctrl.sitioNuevo.camuflaje">
								<option value="">Elegir camuflaje</option>
								<option ng-repeat="c in ctrl.camuflajes" value="{{c.id}}">{{c.camuflaje}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Construcción</td>
						<td>
							<div class="input-group input-group-sm">
								<select class="form-control" ng-model="ctrl.sitioNuevo.contrata_constructora">
									<option value="">Elegir constructora</option>
									<option ng-repeat="c in ctrl.constructoras" value="{{c.id}}">{{c.contrata_constructora}}</option>
								</select>
								<span class="input-group-addon"></span>
								<input type="number" min="1900" name="anio_construccion" class="form-control" ng-model="ctrl.sitioNuevo.anio_construccion">
								<span class="input-group-addon">año</span>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="panel-body" id="acceso" ng-if="ctrl.info.index == 3" style="height:350px;overflow:auto;">
			<table class="table">
				<tbody>
					<tr>
						<td>Coubicador <span class="obliga">*</span></td>
						<td>
							<div class="input-group input-group-sm">								
								<select class="form-control" ng-model="ctrl.sitioNuevo.coubicador" ng-change="ctrl.listener_coubicador(ctrl.sitioNuevo.coubicador)">
									<option value="">Seleccione Coubicador</option>
									<option ng-repeat="c in ctrl.coubicadores" value="{{c.id}}">{{c.coubicador}}</option>
								</select>
								<span class="input-group-addon"></span>
								<input type="text" name="" class="form-control" placeholder="Nombre de sitio por coubicador" ng-model="ctrl.sitioNuevo.nombre_sitio_coubicador" ng-disabled="ctrl.sitioNuevo.coubicador == 1">
								<span class="input-group-addon"></span>
								<input type="text" name="" class="form-control" placeholder="Código de torrera" ng-model="ctrl.sitioNuevo.codigo_torrera" ng-disabled="ctrl.sitioNuevo.coubicador == 1">
							</div>
						</td>
					</tr>
					<tr>
						<td>Coubicante</td>
						<td>
							<div class="input-group input-group-sm">
								<select class="form-control" ng-model="ctrl.sitioNuevo.operador_coubicante" ng-change="ctrl.listener_coubicante(ctrl.sitioNuevo.operador_coubicante)">
									<option ng-repeat="c in ctrl.operadores_coubicante" value="{{c.id}}">{{c.coubicante}}</option>
								</select>
								<span class="input-group-addon"></span>
								<input type="text" name="" class="form-control" placeholder="Nombre de sitio por coubicante" ng-model="ctrl.sitioNuevo.nombre_sitio_coubicante" ng-disabled="ctrl.sitioNuevo.operador_coubicante == 1">
							</div>
						</td>
					</tr>
					<tr>
						<td>Acceso libre 24 H</td>
						<td>
							<input type="checkbox" name="" ng-model="ctrl.sitioNuevo.acceso_libre_24h">
						</td>
					</tr>
					<tr>
						<td>Nivel de riesgo</td>
						<td>
							<select class="form-control" ng-model="ctrl.sitioNuevo.nivel_riesgo">
								<option ng-repeat="nr in [1,2,3,4,5,6,7,8,9]" value="{{nr}}">{{nr}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Consideraciones de acceso</td>
						<td>
							<textarea class="form-control" ng-model="ctrl.sitioNuevo.consideracion_acceso"></textarea>
						</td>
					</tr>
					<tr>
						<td>Consideraciones</td>
						<td>
							<textarea class="form-control" ng-model="ctrl.sitioNuevo.consideraciones"></textarea>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="panel-body" id="especiales" ng-if="ctrl.info.index == 4" style="height:350px;overflow:auto;">
			<table class="table">
				<tbody>
					<tr>
						<td>Proyecto <span class="obliga">*</span></td>
						<td>
							<select class="form-control" ng-model="ctrl.sitioNuevo.proyecto">
								<option value="">Elegir Proyecto</option>
								<option ng-repeat="p in ctrl.proyectos" value="{{p.id}}">{{p.proyecto}}</option>
							</select>
						</td>
					</tr>
			
					<tr>
						<td>Fecha 
						</td>
						<td>							
						<input  name="fecha_estado_on_air"  type="date" name="" class="form-control" ng-model="ctrl.sitioNuevo.fecha_estado_on_air"   disabled="disabled">
						</td>
					</tr>
					<tr>
						<td>
							Estado  	
						</td>
						<td>
						

						

<select ng-model="ctrl.sitioNuevo.estado" onchange="habilitafecha()" name="estado"  id="estado" >
					    	<option value="1">OFF AIR</option>
					    	<option value="2">ON AIR</option>			    	
					    </select>


						</td>
					</tr>









					<tr>
						<td>In-building</td>
						<td>
							<input type="checkbox" name="" ng-model="ctrl.sitioNuevo.in_building">
						</td>
					</tr>
					<tr>
						<td>Agregador</td>
						<td>
							<input type="checkbox" name="" ng-model="ctrl.sitioNuevo.agregador">
						</td>
					</tr>
					<tr>
						<td>Dorsal</td>
						<td>
							<input type="checkbox" name="" ng-model="ctrl.sitioNuevo.dorsal">
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="panel-body" id="contratos" ng-if="ctrl.info.index == 5" style="height:350px;overflow:auto;">
			<table class="table">
				<tbody>
					<tr ng-repeat="t in ctrl.sitioNuevo.tecnologias">
						<td>
							<div class="input-group input-group-sm">
								<div class="input-group-addon">Tecnología:</div>
								<select class="form-control" ng-model="t.tecnologia">
									<option ng-repeat="tecnos in ctrl.tecnologias" value="{{tecnos.id}}">{{tecnos.tecnologia}}</option>
								</select>
								<div class="input-group-addon">Código:</div>
								<input type="text" name="" class="form-control" ng-model="t.codigo">
								<div class="input-group-addon">Fecha de servicio:</div>
								<input type="date" name="" class="form-control" ng-model="t.fecha_servicio">
								<div class="input-group-addon">Servicios:</div>
								<input type="number" name="" class="form-control" min="0" ng-model="t.servicios">
								<div class="input-group-addon btn btn-danger" ng-click="ctrl.quitar_tecnologia($index)">
									<span class="fa fa-times"></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="text-center">
							<button class="btn btn-sm btn-primary" ng-click="ctrl.agregar_tecnologia()">
								<span class="fa fa-plus"></span> Agregar tecnología
							</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-6">
					<button  class="btn btn-success bloquea_one" ng-click="ctrl.crear()" ng-disabled="ctrl.disabled()">Crear sitio</button>
				</div>
				<div class="col-xs-6">
					<button class="btn btn-danger" ng-click="ctrl.cancelarCreacion()">Cancelar</button>
				</div>
			</div>

			<!-- <div class="procesand">
				<center>
					<span>Procesando ...</span>
				</center>
				<img src="<?php //echo base_url('assets/images/procesando_1.gif'); ?>" alt="">
			</div> -->

            <div class="procesand" style="position: absolute;top: 39%;z-index: 9;left: 40%;right: 40%;display:none;">
				<center>
					<span style="color: rgb(245,121,23);font-size: 28px;">Procesando ...</span><br>
					<img src="<?php echo base_url('assets/images/procesando_1.gif'); ?>" alt="">
				</center>				
			</div>









		</div>
	</form>
	<div class="row">
		<div class="col-xs-12">
			{{ctrl.error.crear}}
		</div>
	</div>
</div>