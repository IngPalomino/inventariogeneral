<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Equipments Inventory
				</div>
				<div class="panel-body">
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
					</div>
					<table class="table table-condensed table-bordered wimax" ng-if="!ctrl.cargando && ctrl.wimax.length">
						<thead>
							<tr class="success">
								<th><input type="checkbox" ng-click="ctrl.selectAll()" ng-model="ctrl.isAllSelected"></th>
								<th>BTS Model</th>
								<th>WiMax Code</th>
								<th>BTS Name</th>
								<th>BTS Number</th>
								<th>IP Address</th>
								<th>Entity Type</th>
								<th>Entity Count</th>
							</tr>
						</thead>
						<tbody>
							<tr class="warning" ng-repeat="wimax in ctrl.wimax">
								<td><input type="checkbox" ng-model="wimax.check" ng-change="ctrl.seleccionado()"></td>
								<td>{{wimax.bts_model}}</td>
								<td>{{wimax.wimax_code}}</td>
								<td>{{wimax.bts_name}}</td>
								<td>{{wimax.bts_number}}</td>
								<td>{{wimax.ip_address}}</td>
								<td>{{wimax.entity_type}}</td>
								<td>{{wimax.entity_count}}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-success" ng-click="ctrl.nuevo_wimax()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Añadir</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoWimax(ctrl.wimax)" ng-disabled="!ctrl.disabledWimax()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar selección</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Detailed Equipments Inventory
				</div>
				<div class="panel-body">
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargandoDetailed"></span>
					</div>
					<table class="table table-condensed table-striped table-bordered wimax" ng-if="!ctrl.cargandoDetailed && ctrl.wimax_detailed.length">
						<thead>
							<tr class="success">
								<th><input type="checkbox" ng-click="ctrl.selectAllDetailed()" ng-model="ctrl.isAllSelectedDetailed"></th>
								<th>BTS Name</th>
								<th style="white-space:nowrap;">Entity Type</th>
								<th>Entity Serial Number</th>
								<th>Entity Hardware Version Number</th>
								<th>Entity Hardware Revision Number</th>
								<th>Entity Operational Software Version Number</th>
								<th>Entity Boot Software Version Number</th>
							</tr>
						</thead>
						<tbody>
							<tr class="warning" ng-repeat="wimax in ctrl.wimax_detailed">
								<td><input type="checkbox" ng-model="wimax.check" ng-change="ctrl.seleccionadoDetailed()"></td>
								<td>{{wimax.bts_name}}</td>
								<td style="white-space:nowrap;">{{wimax.entity_type}}</td>
								<td style="white-space:nowrap;">{{wimax.entity_serial_number}}</td>
								<td>{{wimax.entity_hardware_version_number}}</td>
								<td>{{wimax.entity_hardware_revision_number}}</td>
								<td>{{wimax.entity_operational_software_version_number}}</td>
								<td>{{wimax.entity_boot_software_version_number}}</td>
							</tr>
						</tbody>
					</table>
					<div class="col-xs-12 ng-cloak" ng-if="!ctrl.wimax_detailed.length && !ctrl.cargandoDetailed">
						<div class="well text-center">
							No hay información
						</div>
					</div>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-success" ng-click="ctrl.nuevo_wimaxDetailed()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Añadir</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoWimaxDetailed(ctrl.wimax_detailed)" ng-disabled="!ctrl.disabledWimaxDetailed()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar selección</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="nuevoWimaxDetailed" class="popup" style="width:1000px;">
	<span class="button b-close" ng-click="ctrl.cancelarCreacionWimaxDetailed()">
		<span>X</span>
	</span>
	
	<h3>Nuevo Wimax</h3>
	
	<form>
		<div id="nuevoWimaxDetailedData" style="max-width:900px;height:300px;overflow:scroll;"></div><br>
		<button class="btn btn-success" ng-click="ctrl.agregarWimaxDetailed()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionWimaxDetailed()">Cancelar</button>
	</form>
</div>

<div id="eliminarWimaxDetailed" class="popup">
	<span class="button b-close" ng-click="ctrl.cancelarEliminacionWimaxDetailed()">
		<span>X</span>
	</span>
	
	<h3>Eliminar Wimax</h3>
	
	<br>
	<p>¿Está seguro de eliminar los registros seleccionados?</p>
	
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarWimaxDetailed()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionWimaxDetailed()">Cancelar</button>
</div>

<div id="nuevoWimax" class="popup" style="width:800px;">
	<span class="button b-close" ng-click="ctrl.cancelarCreacionWimax()">
		<span>X</span>
	</span>
	
	<h3>Nuevo Wimax</h3>
	
	<form>
		<div id="nuevoWimaxData" style="max-width:700px;height:300px;overflow:scroll;"></div><br>
		<button class="btn btn-success" ng-click="ctrl.agregarWimax()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionWimax()">Cancelar</button>
	</form>
</div>

<div id="eliminarWimax" class="popup">
	<span class="button b-close" ng-click="ctrl.cancelarEliminacionWimax()">
		<span>X</span>
	</span>
	
	<h3>Eliminar Wimax</h3>
	
	<br>
	<p>¿Está seguro de eliminar los registros seleccionados?</p>
	
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarWimax()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionWimax()">Cancelar</button>
</div>