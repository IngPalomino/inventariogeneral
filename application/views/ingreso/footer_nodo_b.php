<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http","$filter",function($http,$filter){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargandoAntenas = false;
			ref.cargandoTarjetas = false;
			ref.cargandoGabinetes = false;
			var hot_tarjetas = {};
			var hot_antenas = {};
			var hot_gabinetes = {};
			var hot_editar_tarjetas = {};
			var hot_editar_antenas = {};
			var hot_editar_gabinetes = {};
			ref.isAllSelectedAntenas = false;
			ref.isAllSelectedTarjetas = false;
			ref.isAllSelectedGabinetes = false;
			
			ref.tarjetas = [];
			ref.antenas = [];
			ref.gabinetes = [];
			ref.editarTarjetas = [];
			ref.editarAntenas = [];
			ref.editarGabinetes = [];
			ref.eliminar_Tarjetas = [];
			ref.eliminar_Antenas = [];
			ref.eliminar_Gabinetes = [];
			
			listarTarjetas = function(){
				ref.cargandoTarjetas = true;
				$http({
					url:"<?php echo base_url("tarjetas/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tarjetas = data;
						ref.tarjetas.forEach(function(t,ind,obj){
							obj[ind].check = false;
						});
						/*console.log(data);*/
						ref.cargandoTarjetas = false;
					}
					else
					{
						ref.cargandoTarjetas = false;
					}
				}).error(function(err){
					ref.cargandoTarjetas = false;
				});
			}
			
			listarAntenas = function(){
				ref.cargandoAntenas = true;
				$http({
					url:"<?php echo base_url("e_antenas/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenas = data;
						ref.antenas.forEach(function(t,ind,obj){
							obj[ind].check = false;
						});
						/*console.log(data);*/
						ref.cargandoAntenas = false;
					}
					else
					{
						ref.cargandoAntenas = false;
					}
				}).error(function(err){
					ref.cargandoAntenas = false;
				});
			}
			
			listarGabinetes = function(){
				ref.cargandoGabinetes = true;
				$http({
					url:"<?php echo base_url("e_gabinetes/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.gabinetes = data;
						ref.gabinetes.forEach(function(t,ind,obj){
							obj[ind].check = false;
						});
						/*console.log(data);*/
						ref.cargandoGabinetes = false;
					}
					else
					{
						ref.cargandoGabinetes = false;
					}
				}).error(function(err){
					ref.cargandoGabinetes = false;
				});
			}
			
			ref.disabledTarjetas = function()
			{
				var dis = false;
				
				ref.tarjetas.forEach(function(t){
					dis = dis || t.check;
				});
				
				return dis;
			}
			
			ref.disabledAntenas = function()
			{
				var dis = false;
				
				ref.antenas.forEach(function(t){
					dis = dis || t.check;
				});
				
				return dis;
			}
			
			ref.disabledGabinetes = function()
			{
				var dis = false;
				
				ref.gabinetes.forEach(function(t){
					dis = dis || t.check;
				});
				
				return dis;
			}

			ref.editandoTarjetas = function(info){
				$("#editarTarjetas").bPopup({
					amsl: 0,
					onClose: ref.editarTarjetas = []
				});

				info.forEach(function(tarjeta,ind,objeto){
					if(objeto[ind].check)
					{
						ref.editarTarjetas.push(objeto[ind]);
					}
				});

				ref.editarTarjetas.forEach(function(dato,ind,objeto){
					objeto[ind].date_manufacture = $filter('date')(objeto[ind].date_manufacture, "dd/MM/yyyy");
				});
				
				var container = document.getElementById('editarTarjetasData');
				hot_editar_tarjetas = new Handsontable(container,
				{
					data: ref.editarTarjetas,
					minRows: ref.editarTarjetas.length,
					minCols: 12,
					maxRows: ref.editarTarjetas.length,
					maxCols: 12,
					colHeaders: ["NEFdn","Board Name","Board Type","Date of Manufacture","BOM Code","Manufacturer Data","Serial Number","Software Version","Unit Position","Vendor Name","Vendor Unit Family Type","Hardware Version"],
					rowHeaders: true,
					columns: [
						{data: 'nefdn'},
						{data: 'board_name'},
						{data: 'board_type'},
						{data: 'date_manufacture'},
						{data: 'bom_code'},
						{data: 'manufacturer_data'},
						{data: 'serial_number'},
						{data: 'software_version'},
						{data: 'unit_position'},
						{data: 'vendor_name'},
						{data: 'vendor_unit_family_type'},
						{data: 'hardware_version'}
					]
				});
			}

			ref.grabarTarjetas = function(){
				var data = "data="+encodeURIComponent(JSON.stringify(ref.editarTarjetas));
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("tarjetas/tarjetas");?>",
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#editarTarjetas").bPopup().close();
					hot_editar_tarjetas.destroy();
					ref.editarTarjetas = [];
					ref.tarjetas.forEach(function(t){
						t.check = false;
					});
					ref.isAllSelectedTarjetas = false;
					listarTarjetas();
				}).error(function(err){

				});
			}

			ref.cancelarEdicionTarjetas = function(){
				$("#editarTarjetas").bPopup().close();
				hot_editar_tarjetas.destroy();
				ref.editarTarjetas = [];
				ref.tarjetas.forEach(function(t){
					t.check = false;
				});
				ref.isAllSelectedTarjetas = false;
				listarTarjetas();
			}
			
			ref.nuevas_tarjetas = function(){
				$("#nuevasTarjetas").bPopup({
					amsl: 0
				});
				
				var container = document.getElementById('nuevasTarjetasData');
				hot_tarjetas = new Handsontable(container,
				{
					minRows: 10,
					minCols: 12,
					minSpareRows: 1,
					colHeaders: ["NEFdn","Board Name","Board Type","Date of Manufacture","BOM Code","Manufacturer Data","Serial Number","Software Version","Unit Position","Vendor Name","Vendor Unit Family Type","Hardware Version"],
					rowHeaders: true
				});
			}
			
			ref.agregarTarjetas = function(){
				var i = 0;
				var tarjetas_data = [];
				
				while(hot_tarjetas.getData()[i][0] != null)
				{
					var row = hot_tarjetas.getData()[i];
					var linea = {};
					row.forEach(function(dato,ind,objeto){
						linea["nefdn"] = objeto[0];
						linea["board_name"] = objeto[1];
						linea["board_type"] = objeto[2];
						linea["date_manufacture"] = objeto[3];
						linea["bom_code"] = objeto[4];
						linea["manufacturer_data"] = objeto[5];
						linea["serial_number"] = objeto[6];
						linea["software_version"] = objeto[7];
						linea["unit_position"] = objeto[8];
						linea["vendor_name"] = objeto[9];
						linea["vendor_unit_family_type"] = objeto[10];
						linea["hardware_version"] = objeto[11];
					});
					i++;
					tarjetas_data.push(linea);
				}
				
				var data = "data="+encodeURIComponent(JSON.stringify(tarjetas_data));
				$http({
					url:"<?php echo base_url("tarjetas/tarjetas/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevasTarjetas").bPopup().close();
					listarTarjetas();
					hot_tarjetas.destroy();
					/*console.log(data);*/
				}).error(function(err){

				});
			}
			
			ref.cancelarCreacionTarjetas = function(){
				$("#nuevasTarjetas").bPopup().close();
				hot_tarjetas.destroy();
			}
			
			ref.eliminandoTarjetas = function(info){
				info.forEach(function(tarjeta,ind,objeto){
					if(objeto[ind].check)
					{
						ref.eliminar_Tarjetas.push(objeto[ind].id);
					}
				});
				$("#eliminarTarjetas").bPopup();
			}
			
			ref.eliminarTarjetas = function(){
				var data = "data="+JSON.stringify(ref.eliminar_Tarjetas);
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("tarjetas/tarjetas");?>",
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.tarjetas = [];
						listarTarjetas();
						ref.eliminar_Tarjetas = [];
						$("#eliminarTarjetas").bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEliminacionTarjetas = function(){
				ref.eliminar_Tarjetas = [];
				ref.tarjetas.forEach(function(t){
					t.check = false;
				});
				ref.isAllSelectedTarjetas = false;
				$("#eliminarTarjetas").bPopup().close();
			}

			ref.editandoAntenas = function(info){
				$("#editarAntenas").bPopup({
					amsl: 0,
					onClose: ref.editarAntenas = []
				});

				info.forEach(function(antena,ind,objeto){
					if(objeto[ind].check)
					{
						ref.editarAntenas.push(objeto[ind]);
					}
				});

				ref.editarAntenas.forEach(function(dato,ind,objeto){
					try{ objeto[ind].date_manufacture_physical = $filter('date')(objeto[ind].date_manufacture_physical, "dd/MM/yyyy"); } catch(err){ objeto[ind].date_manufacture_physical = null; }
					try{ objeto[ind].date_manufacture_m2000 = $filter('date')(objeto[ind].date_manufacture_m2000, "dd/MM/yyyy"); } catch(err){ objeto[ind].date_manufacture_m2000 = null; }
				});
				
				var container = document.getElementById('editarAntenasData');
				hot_editar_antenas = new Handsontable(container,
				{
					data: ref.editarAntenas,
					minRows: ref.editarAntenas.length,
					minCols: 12,
					maxRows: ref.editarAntenas.length,
					maxCols: 12,
					colHeaders: ["NEFdn","Antenna Device ID","Antenna Device Type","Date of Manufacture (Physical)","Date of Manufacture (M2000)","BOM Code","Manufacturer Data","Antenna Model","Serial Number (Antenna Motors)","Unit Position","Vendor Name","Hardware Version"],
					rowHeaders: true,
					columns: [
						{data: 'nefdn'},
						{data: 'antenna_device_id'},
						{data: 'antenna_device_type'},
						{data: 'date_manufacture_physical'},
						{data: 'date_manufacture_m2000'},
						{data: 'bom_code'},
						{data: 'manufacturer_data'},
						{data: 'antenna_model'},
						{data: 'serial_number_motor'},
						{data: 'unit_position'},
						{data: 'vendor_name'},
						{data: 'hardware_version'}
					]
				});
			}

			ref.grabarAntenas = function(){
				var data = "data="+encodeURIComponent(JSON.stringify(ref.editarAntenas));
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("e_antenas/e_antenas");?>",
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#editarAntenas").bPopup().close();
					hot_editar_antenas.destroy();
					ref.editarAntenas = [];
					ref.antenas.forEach(function(t){
						t.check = false;
					});
					ref.isAllSelectedAntenas = false;
					listarAntenas();
				}).error(function(err){

				});
			}

			ref.cancelarEdicionAntenas = function(){
				$("#editarAntenas").bPopup().close();
				hot_editar_antenas.destroy();
				ref.editarAntenas = [];
				ref.antenas.forEach(function(t){
					t.check = false;
				});
				ref.isAllSelectedAntenas = false;
				listarAntenas();
			}
			
			ref.nuevas_antenas = function(){
				$("#nuevasAntenas").bPopup({
					amsl: 0
				});
				
				var container = document.getElementById('nuevasAntenasData');
				hot_antenas = new Handsontable(container,
				{
					minRows: 10,
					minCols: 12,
					minSpareRows: 1,
					colHeaders: ["NEFdn","Antenna Device ID","Antenna Device Type","Date of Manufacture (Physical)","Date of Manufacture (M2000)","BOM Code","Manufacturer Data","Antenna Model","Serial Number (Antenna Motors)","Unit Position","Vendor Name","Hardware Version"],
					rowHeaders: true
				});
			}
			
			ref.agregarAntenas = function(){
				var i = 0;
				var antenas_data = [];
				
				while(hot_antenas.getData()[i][0] != null)
				{
					var row = hot_antenas.getData()[i];
					var linea = {};
					row.forEach(function(dato,ind,objeto){
						linea["nefdn"] = objeto[0];
						linea["antenna_device_id"] = objeto[1];
						linea["antenna_device_type"] = objeto[2];
						linea["date_manufacture_physical"] = objeto[3];
						linea["date_manufacture_m2000"] = objeto[4];
						linea["bom_code"] = objeto[5];
						linea["manufacturer_data"] = objeto[6];
						linea["antenna_model"] = objeto[7];
						linea["serial_number_motor"] = objeto[8];
						linea["unit_position"] = objeto[9];
						linea["vendor_name"] = objeto[10];
						linea["hardware_version"] = objeto[11];
					});
					i++;
					antenas_data.push(linea);
				}
				
				var data = "data="+encodeURIComponent(JSON.stringify(antenas_data));
				$http({
					url:"<?php echo base_url("e_antenas/e_antenas/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevasAntenas").bPopup().close();
					listarAntenas();
					hot_antenas.destroy();
					/*console.log(data);*/
				}).error(function(err){

				});
			}
			
			ref.cancelarCreacionAntenas = function(){
				$("#nuevasAntenas").bPopup().close();
				hot_antenas.destroy();
			}
			
			ref.eliminandoAntenas = function(info){
				info.forEach(function(antena,ind,objeto){
					if(objeto[ind].check)
					{
						ref.eliminar_Antenas.push(objeto[ind].id);
					}
				});
				$("#eliminarAntenas").bPopup();
			}
			
			ref.eliminarAntenas = function(){
				var data = "data="+JSON.stringify(ref.eliminar_Antenas);
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("e_antenas/e_antenas");?>",
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.antenas = [];
						listarAntenas();
						ref.eliminar_Antenas = [];
						$("#eliminarAntenas").bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEliminacionAntenas = function(){
				ref.eliminar_Antenas = [];
				ref.antenas.forEach(function(t){
					t.check = false;
				});
				ref.isAllSelectedAntenas = false;
				$("#eliminarAntenas").bPopup().close();
			}

			ref.editandoGabinetes = function(info){
				$("#editarGabinetes").bPopup({
					amsl: 0,
					onClose: ref.editarGabinetes = []
				});

				info.forEach(function(gabinete,ind,objeto){
					if(objeto[ind].check)
					{
						ref.editarGabinetes.push(objeto[ind]);
					}
				});

				ref.editarGabinetes.forEach(function(dato,ind,objeto){
					try{ objeto[ind].date_manufacture_physical = $filter('date')(objeto[ind].date_manufacture_physical, "dd/MM/yyyy"); } catch(err){ objeto[ind].date_manufacture_physical = null; }
					try{ objeto[ind].date_manufacture_m2000 = $filter('date')(objeto[ind].date_manufacture_m2000, "dd/MM/yyyy"); } catch(err){ objeto[ind].date_manufacture_m2000 = null; }
				});
				
				var container = document.getElementById('editarGabinetesData');
				hot_editar_gabinetes = new Handsontable(container,
				{
					data: ref.editarGabinetes,
					minRows: ref.editarGabinetes.length,
					minCols: 12,
					maxRows: ref.editarGabinetes.length,
					maxCols: 12,
					colHeaders: ["NEFdn","Model (Physical)","Type (Physical)","Date of Manufacture (Physical)","Date of Manufacture (M2000)","BOM Code","Manufacturer Data","Rack Type (Physicial)","Rack Type (M2000)","Serial Number (Physicial)","Serial Number (M2000)","Vendor Name"],
					rowHeaders: true,
					columns: [
						{data: 'nefdn'},
						{data: 'model_physical'},
						{data: 'type_physical'},
						{data: 'date_manufacture_physical'},
						{data: 'date_manufacture_m2000'},
						{data: 'bom_code'},
						{data: 'manufacturer_data'},
						{data: 'rack_type_physical'},
						{data: 'rack_type_m2000'},
						{data: 'serial_number_physical'},
						{data: 'serial_number_m2000'},
						{data: 'vendor_name'}
					]
				});
			}

			ref.grabarGabinetes = function(){
				var data = "data="+encodeURIComponent(JSON.stringify(ref.editarGabinetes));
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("e_gabinetes/e_gabinetes");?>",
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#editarGabinetes").bPopup().close();
					hot_editar_gabinetes.destroy();
					ref.editarGabinetes = [];
					ref.gabinetes.forEach(function(t){
						t.check = false;
					});
					ref.isAllSelectedGabinetes = false;
					listarGabinetes();
				}).error(function(err){

				});
			}

			ref.cancelarEdicionGabinetes = function(){
				$("#editarGabinetes").bPopup().close();
				hot_editar_gabinetes.destroy();
				ref.editarGabinetes = [];
				ref.gabinetes.forEach(function(t){
					t.check = false;
				});
				ref.isAllSelectedGabinetes = false;
				listarGabinetes();
			}
			
			ref.nuevos_gabinetes = function(){
				$("#nuevosGabinetes").bPopup({
					amsl: 0
				});
				
				var container = document.getElementById('nuevosGabinetesData');
				hot_gabinetes = new Handsontable(container,
				{
					minRows: 10,
					minCols: 12,
					minSpareRows: 1,
					colHeaders: ["NEFdn","Model (Physical)","Type (Physical)","Date of Manufacture (Physical)","Date of Manufacture (M2000)","BOM Code","Manufacturer Data","Rack Type (Physicial)","Rack Type (M2000)","Serial Number (Physicial)","Serial Number (M2000)","Vendor Name"],
					rowHeaders: true
				});
			}
			
			ref.agregarGabinetes = function(){
				var i = 0;
				var gabinetes_data = [];
				
				while(hot_gabinetes.getData()[i][0] != null)
				{
					var row = hot_gabinetes.getData()[i];
					var linea = {};
					row.forEach(function(dato,ind,objeto){
						linea["nefdn"] = objeto[0];
						linea["model_physical"] = objeto[1];
						linea["type_physical"] = objeto[2];
						linea["date_manufacture_physical"] = objeto[3];
						linea["date_manufacture_m2000"] = objeto[4];
						linea["bom_code"] = objeto[5];
						linea["manufacturer_data"] = objeto[6];
						linea["rack_type_physical"] = objeto[7];
						linea["rack_type_m2000"] = objeto[8];
						linea["serial_number_physical"] = objeto[9];
						linea["serial_number_m2000"] = objeto[10];
						linea["vendor_name"] = objeto[11];
					});
					i++;
					gabinetes_data.push(linea);
				}
				
				var data = "data="+encodeURIComponent(JSON.stringify(gabinetes_data));
				$http({
					url:"<?php echo base_url("e_gabinetes/e_gabinetes/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevosGabinetes").bPopup().close();
					listarGabinetes();
					hot_gabinetes.destroy();
					/*console.log(data);*/
				}).error(function(err){

				});
			}
			
			ref.cancelarCreacionGabinetes = function(){
				$("#nuevosGabinetes").bPopup().close();
				hot_gabinetes.destroy();
			}
			
			ref.eliminandoGabinetes = function(info){
				info.forEach(function(gabinete,ind,objeto){
					if(objeto[ind].check)
					{
						ref.eliminar_Gabinetes.push(objeto[ind].id);
					}
				});
				$("#eliminarGabinetes").bPopup();
			}
			
			ref.eliminarGabinetes = function(){
				var data = "data="+JSON.stringify(ref.eliminar_Gabinetes);
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("e_gabinetes/e_gabinetes");?>",
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.gabinetes = [];
						listarGabinetes();
						ref.eliminar_Gabinetes = [];
						$("#eliminarGabinetes").bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEliminacionGabinetes = function(){
				ref.eliminar_Gabinetes = [];
				ref.gabinetes.forEach(function(t){
					t.check = false;
				});
				ref.isAllSelectedGabinetes = false;
				$("#eliminarGabinetes").bPopup().close();
			}
			
			ref.selectAllAntenas = function(){
				ref.antenas.forEach(function(antena){
					antena.check = ref.isAllSelectedAntenas;
				});
			}
			
			ref.antenaSeleccionada = function(){
				ref.isAllSelectedAntenas = ref.antenas.every(function(antena){return antena.check;});
			}
			
			ref.selectAllTarjetas = function(){
				ref.tarjetas.forEach(function(tarjeta){
					tarjeta.check = ref.isAllSelectedTarjetas;
				});
			}
			
			ref.tarjetaSeleccionada = function(){
				ref.isAllSelectedTarjetas = ref.tarjetas.every(function(tarjeta){return tarjeta.check;});
			}
			
			ref.selectAllGabinetes = function(){
				ref.gabinetes.forEach(function(gabinete){
					gabinete.check = ref.isAllSelectedGabinetes;
				});
			}
			
			ref.gabineteSeleccionado = function(){
				ref.isAllSelectedGabinetes = ref.gabinetes.every(function(gabinete){return gabinete.check;});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			listarTarjetas();
			listarAntenas();
			listarGabinetes();
		}]);
</script>
</body>
</html>

