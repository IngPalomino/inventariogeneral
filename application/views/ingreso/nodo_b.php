<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Antenas
				</div>
				<div class="panel-body" style="max-width:1200px;max-height:300px;overflow:scroll;">
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargandoAntenas"></span>
					</div>
					<table class="table table-striped table-condensed" style="width:3000px;" ng-if="!ctrl.cargandoAntenas && ctrl.antenas.length">
						<thead>
							<tr>
								<th style="white-space:nowrap;"><input type="checkbox" ng-click="ctrl.selectAllAntenas()" ng-model="ctrl.isAllSelectedAntenas"/> #</th>
								<th>NEFdn</th>
								<th>Antenna Device ID</th>
								<th>Antenna Device Type</th>
								<th>Date of Manufacture</th>
								<th>BOM Code</th>
								<th>Manufacturer Data</th>
								<th>Antenna Model</th>
								<th>Serial Number (Antenna Motors)</th>
								<th>Unit Position</th>
								<th>Vendor Name</th>
								<th>Hardware Version</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="antena in ctrl.antenas">
								<td><input type="checkbox" ng-model="antena.check" ng-change="ctrl.antenaSeleccionada()"/></td>
								<td>{{antena.nefdn}}</td>
								<td>{{antena.antenna_device_id}}</td>
								<td>{{antena.antenna_device_type}}</td>
								<td>
									<span ng-if="antena.date_manufacture_physical">{{antena.date_manufacture_physical | date:"dd/MM/yyyy"}}</span>
									<span ng-if="!antena.date_manufacture_physical">{{antena.date_manufacture_m2000 | date:"dd/MM/yyyy"}}</span>
								</td>
								<td>{{antena.bom_code}}</td>
								<td style="white-space: nowrap">{{antena.manufacturer_data}}</td>
								<td>{{antena.antenna_model}}</td>
								<td>{{antena.serial_number_motor}}</td>
								<td>{{antena.unit_position}}</td>
								<td>{{antena.vendor_name}}</td>
								<td>{{antena.hardware_version}}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-success" ng-click="ctrl.nuevas_antenas()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Añadir Antenas</button>
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editandoAntenas(ctrl.antenas)" ng-disabled="!ctrl.disabledAntenas()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar selección</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoAntenas(ctrl.antenas)" ng-disabled="!ctrl.disabledAntenas()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar selección</button>
				</div>
			</div>
		</div>
		
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Tarjetas
				</div>
				<div class="panel-body" style="max-width:1200px;max-height:300px;overflow:scroll;">
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargandoTarjetas"></span>
					</div>
					<table class="table table-striped table-condensed" style="width:3000px;" ng-if="!ctrl.cargandoTarjetas && ctrl.tarjetas.length">
						<thead>
							<tr>
								<th><input type="checkbox" ng-click="ctrl.selectAllTarjetas()" ng-model="ctrl.isAllSelectedTarjetas"/> #</th>
								<th>NEFdn</th>
								<th>Board Name</th>
								<th>Board Type</th>
								<th>Date of Manufacture</th>
								<th>BOM Code</th>
								<th>Manufacturer Data</th>
								<th>Serial Number</th>
								<th>Software Version</th>
								<th>Unit Position</th>
								<th>Vendor Name</th>
								<th>Vendor Unit Family Type</th>
								<th>Hardware Version</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="tarjeta in ctrl.tarjetas">
								<td><input type="checkbox" ng-model="tarjeta.check" ng-change="ctrl.tarjetaSeleccionada()"/></td>
								<td>{{tarjeta.nefdn}}</td>
								<td>{{tarjeta.board_name}}</td>
								<td>{{tarjeta.board_type}}</td>
								<td>{{tarjeta.date_manufacture | date:"dd/MM/yyyy"}}</td>
								<td>{{tarjeta.bom_code}}</td>
								<td style="white-space: nowrap">{{tarjeta.manufacturer_data}}</td>
								<td>{{tarjeta.serial_number}}</td>
								<td>{{tarjeta.software_version}}</td>
								<td>{{tarjeta.unit_position}}</td>
								<td>{{tarjeta.vendor_name}}</td>
								<td>{{tarjeta.vendor_unit_family_type}}</td>
								<td>{{tarjeta.hardware_version}}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-success" ng-click="ctrl.nuevas_tarjetas()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Añadir Tarjetas</button>
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editandoTarjetas(ctrl.tarjetas)" ng-disabled="!ctrl.disabledTarjetas()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar selección</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoTarjetas(ctrl.tarjetas)" ng-disabled="!ctrl.disabledTarjetas()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar selección</button>
				</div>
			</div>
		</div>
		
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Gabinetes
				</div>
				<div class="panel-body" style="max-width:1200px;max-height:300px;overflow:scroll;">
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargandoGabinetes"></span>
					</div>
					<table class="table table-striped table-condensed" style="width:3000px;" ng-if="!ctrl.cargandoGabinetes && ctrl.gabinetes.length">
						<thead>
							<tr>
								<th><input type="checkbox" ng-click="ctrl.selectAllGabinetes()" ng-model="ctrl.isAllSelectedGabinetes"/> #</th>
								<th>NEFdn</th>
								<th>Model (Physical)</th>
								<th>Type (Physical)</th>
								<th>Date of Manufacture</th>
								<th>BOM Code</th>
								<th>Manufacturer Data</th>
								<th>Rack Type (Physical)</th>
								<th>Rack Type (M2000)</th>
								<th>Serial Number</th>
								<th>Vendor Name</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="gabinete in ctrl.gabinetes">
								<td><input type="checkbox" ng-model="gabinete.check" ng-change="ctrl.gabineteSeleccionado()"/></td>
								<td>{{gabinete.nefdn}}</td>
								<td>{{gabinete.model_physical}}</td>
								<td>{{gabinete.type_physical}}</td>
								<td>
									<span ng-if="gabinete.date_manufacture_physical">{{gabinete.date_manufacture_physical | date:"dd/MM/yyyy"}}</span>
									<span ng-if="!gabinete.date_manufacture_physical">{{gabinete.date_manufacture_m2000 | date:"dd/MM/yyyy"}}</span>
								</td>
								<td>{{gabinete.bom_code}}</td>
								<td style="white-space: nowrap">{{gabinete.manufacturer_data}}</td>
								<td>{{gabinete.rack_type_physical}}</td>
								<td>{{gabinete.rack_type_m2000}}</td>
								<td>
									<span ng-if="gabinete.serial_number_physical">{{gabinete.serial_number_physical}}</span>
									<span ng-if="!gabinete.serial_number_physical">{{gabinete.serial_number_m2000}}</span>
								</td>
								<td>{{gabinete.vendor_name}}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-success" ng-click="ctrl.nuevos_gabinetes()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Añadir Gabinetes</button>
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editandoGabinetes(ctrl.gabinetes)" ng-disabled="!ctrl.disabledGabinetes()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar selección</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoGabinetes(ctrl.gabinetes)" ng-disabled="!ctrl.disabledGabinetes()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar selección</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="editarTarjetas" class="popup" style="width:1000px;">
	<span class="button b-close" ng-click="ctrl.cancelarEdicionTarjetas()">
		<span>X</span>
	</span>
	
	<h3>Editar Tarjetas</h3>
	
	<form>
		<div id="editarTarjetasData" style="max-width:900px;height:300px;overflow:scroll;"></div><br>
		<button class="btn btn-success" ng-click="ctrl.grabarTarjetas()">Grabar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionTarjetas()">Cancelar</button>
	</form>
</div>

<div id="nuevasTarjetas" class="popup" style="width:1000px;">
	<span class="button b-close" ng-click="ctrl.cancelarCreacionTarjetas()">
		<span>X</span>
	</span>
	
	<h3>Nuevas Tarjetas</h3>
	
	<form>
		<div id="nuevasTarjetasData" style="max-width:900px;height:300px;overflow:scroll;"></div><br>
		<button class="btn btn-success" ng-click="ctrl.agregarTarjetas()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionTarjetas()">Cancelar</button>
	</form>
</div>

<div id="eliminarTarjetas" class="popup">
	<span class="button b-close" ng-click="ctrl.cancelarEliminacionTarjetas()">
		<span>X</span>
	</span>
	
	<h3>Eliminar Tarjetas</h3>
	
	<br>
	<p>¿Está seguro de eliminar las tarjetas seleccionadas?</p>
	
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarTarjetas()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionTarjetas()">Cancelar</button>
</div>

<div id="editarAntenas" class="popup" style="width:1000px;">
	<span class="button b-close" ng-click="ctrl.cancelarEdicionAntenas()">
		<span>X</span>
	</span>
	
	<h3>Editar Antenas</h3>
	
	<form>
		<div id="editarAntenasData" style="max-width:900px;height:300px;overflow:scroll;"></div><br>
		<button class="btn btn-success" ng-click="ctrl.grabarAntenas()">Grabar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionAntenas()">Cancelar</button>
	</form>
</div>

<div id="nuevasAntenas" class="popup" style="width:1000px;">
	<span class="button b-close" ng-click="ctrl.cancelarCreacionAntenas()">
		<span>X</span>
	</span>
	
	<h3>Nuevas Antenas</h3>
	
	<form>
		<div id="nuevasAntenasData" style="max-width:900px;height:300px;overflow:scroll;"></div><br>
		<button class="btn btn-success" ng-click="ctrl.agregarAntenas()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionAntenas()">Cancelar</button>
	</form>
</div>

<div id="eliminarAntenas" class="popup">
	<span class="button b-close" ng-click="ctrl.cancelarEliminacionAntenas()">
		<span>X</span>
	</span>
	
	<h3>Eliminar Antenas</h3>
	
	<br>
	<p>¿Está seguro de eliminar las antenas seleccionadas?</p>
	
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarAntenas()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionAntenas()">Cancelar</button>
</div>

<div id="editarGabinetes" class="popup" style="width:1000px;">
	<span class="button b-close" ng-click="ctrl.cancelarEdicionGabinetes()">
		<span>X</span>
	</span>
	
	<h3>Editar Gabinetes</h3>
	
	<form>
		<div id="editarGabinetesData" style="max-width:900px;height:300px;overflow:scroll;"></div><br>
		<button class="btn btn-success" ng-click="ctrl.grabarGabinetes()">Grabar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionGabinetes()">Cancelar</button>
	</form>
</div>

<div id="nuevosGabinetes" class="popup" style="width:1000px;">
	<span class="button b-close" ng-click="ctrl.cancelarCreacionGabinetes()">
		<span>X</span>
	</span>
	
	<h3>Nuevos Gabinetes</h3>
	
	<form>
		<div id="nuevosGabinetesData" style="max-width:900px;height:300px;overflow:scroll;"></div><br>
		<button class="btn btn-success" ng-click="ctrl.agregarGabinetes()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionGabinetes()">Cancelar</button>
	</form>
</div>

<div id="eliminarGabinetes" class="popup">
	<span class="button b-close" ng-click="ctrl.cancelarEliminacionGabinetes()">
		<span>X</span>
	</span>
	
	<h3>Eliminar Gabinetes</h3>
	
	<br>
	<p>¿Está seguro de eliminar los gabinetes seleccionados?</p>
	
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarGabinetes()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionGabinetes()">Cancelar</button>
</div>