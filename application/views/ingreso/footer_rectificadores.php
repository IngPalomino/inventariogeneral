<script type="text/javascript">
	$( function() {
		$('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
	} );
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http","$httpParamSerializerJQLike", function($http, $httpParamSerializerJQLike){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargando = false;

			ref.sitios = [];
			ref.almacenes = [];
			ref.config = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Buscar sitio',
				maxItems: 1
			};

			ref.rectificadores = [];
			ref.rectificadores_eliminados = [];
			ref.almacenar_Rectificador = {};
			ref.almacenar_Modulos = [];
			ref.eliminar_Rectificador = {};
			ref.eliminar_ModRectificador = {};
			ref.eliminar_DefinitivoRectificador = {};
			ref.nuevoRectificador = {
				modulos_rectif: [],
				nuevoModRectif: function(controlador_rectif = "0", numero_serie = ""){
					this.controlador_rectif = controlador_rectif.id;
					this.numero_serie = "";
					this.tipo_modulos_rectif = {
						amperaje: 0,
						capacidad: 0,
						voltaje: 0,
						marca: "",
						modelo: "",
						id: ""
					};
				},
				
				disabled: function(){
					var disable = false;
					
					this.modulos_rectif.forEach(function(modulo,indy,objetoy){
						if(modulo.tipo_modulos_rectif.id == "" || modulo.numero_serie == "")
						{
							disable = true;
						}	
					});
					
					return disable;
				}
			};
			ref.tipo_controlador_rectif = [];
			ref.tipo_modulos_rectif = [];
			ref.energizacion_rectificador = [];
			ref.ubicacion_rectificador = [];
			
			listarRectificadores = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("index.php/rectificadores/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET",
					headers:{'X-Requested-With' : 'XMLHttpRequest'}
				}).success(function(data){
					if(data.length)
					{
						ref.rectificadores = data;
						
						data.forEach(function(dato,ind,objeto){
							try { objeto[ind].anio_servicio = new Date(objeto[ind].anio_servicio + " 00:00:00"); }catch(err){ objeto[ind].anio_servicio = null;}
							try { objeto[ind].created_at = Date.parse(objeto[ind].created_at); }catch(err){ objeto[ind].created_at = null;}
							objeto[ind].capacidad_maxima = Number(objeto[ind].capacidad_maxima);
							
							objeto[ind].logs_controlador_rectif.forEach(function(modificacion,indx,objetox){
								objetox[indx].created_at = Date.parse(objetox[indx].created_at);
							});
						});

						ref.rectificadores.forEach(function(rectificador,indx,objetox){
							objetox[indx].capacidadInstalada = function(){
								var capInstalada = 0;
								
								this.modulos_rectif.forEach(function(modulo,indy,objetoy){
									if(modulo.eliminado != '1')
									{
										capInstalada += Number(modulo.tipo_modulos_rectif.capacidad);
									}
								});
								
								return capInstalada;
							};
							
							objetox[indx].modulosInstalados = function(){
								var modulosInstalados = 0;
								
								this.modulos_rectif.forEach(function(modulo,indy,objetoy){
									if(modulo.eliminado != '1')
									{
										modulosInstalados++;
									}
								});
								
								return modulosInstalados;
							};
							
							objetox[indx].nuevoModRectificador = function(controlador_rectif = "0", numero_serie = ""){
								this.controlador_rectif = controlador_rectif;
								this.eliminado = 0;
								this.numero_serie = "";
								this.tipo_modulos_rectif = {
									amperaje: 0,
									capacidad: 0,
									voltaje: 0,
									marca: "",
									modelo: "",
									id: ""
								};
							};
							
							objetox[indx].disabled = function(){
								var disable = false;
								
								this.modulos_rectif.forEach(function(modulo,indy,objetoy){
									if(modulo.tipo_modulos_rectif.id == "" || modulo.numero_serie == "")
									{
										disable = true;
									}
								});
								
								return disable;
							};
							
							objetox[indx].modulosCambio = function(){
								this.modulos_rectif.forEach(function(modulo,indy,objetoy){
									return modulo.tipo_modulos_rectif.id = "";
								});
							};
						});
						ref.cargando = false;
					}
					else
					{
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}
			
			listarRectificadoresEliminados = function(){
				$http({
					url:"<?php echo base_url("index.php/rectificadores/listar_eliminado_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.rectificadores_eliminados = data;
						
						data.forEach(function(dato,ind,objeto){
							objeto[ind].logs_controlador_rectif.forEach(function(modificacion,indx,objetox){
								try{ objetox[indx].created_at = Date.parse(objetox[indx].created_at); } catch(err){ objetox[indx].created_at = null; }
							});
						});
						
						ref.rectificadores_eliminados.forEach(function(rectificador,indx,objetox){
							objetox[indx].capacidadInstalada = function(){
								var capInstalada = 0;
								
								this.modulos_rectif.forEach(function(modulo,indy,objetoy){
									capInstalada += Number(modulo.tipo_modulos_rectif.capacidad);
								});
								
								return capInstalada;
							};
							
							objetox[indx].modulosInstalados = function(){
								return this.modulos_rectif.length;
							};
						});
					}
					else
					{
						if($("#mostrarEliminados").hasClass("popup active"))
						{
							$("#mostrarEliminados").bPopup().close();
						}
					}
				}).error(function(err){

				});
			}
			
			listarTipoControladorRectif = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_controlador_rectif/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_controlador_rectif = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoModulosRectif = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_modulos_rectif/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipo_modulos_rectif = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarEnergizacionRectificador = function(){
				$http({
					url:"<?php echo base_url("energizacion_controlador_rectif/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.energizacion_rectificador = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarUbicacionRectificador = function(){
				$http({
					url:"<?php echo base_url("ubicacion_controlador_rectif/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.ubicacion_rectificador = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarSitios = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar_sitios");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sitios = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.almacenes = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			ref.nuevo_rectificador = function(){
				$("#nuevoRectificador").bPopup();
			}
			
			ref.cancelarCreacionRectificador = function(){
				$("#nuevoRectificador").bPopup().close();
				ref.nuevoRectificador = {
					modulos_rectif: [],
					nuevoModRectif: function(controlador_rectif = "0", numero_serie = ""){
						this.controlador_rectif = controlador_rectif.id;
						this.numero_serie = "";
						this.tipo_modulos_rectif = {
							amperaje: 0,
							capacidad: 0,
							voltaje: 0,
							marca: "",
							modelo: "",
							id: ""
						};
					}	
				};
			}
			
			ref.agregarRectificador = function(rectificador){
				borrarPropiedades(rectificador.modulos_rectif,"$$hashKey");
				borrarPropiedades(rectificador,"nuevoModRectif");
				/*borrarPropiedades(rectificador,"disabled");*/
				
				var data = "",
					campos = Object.keys(rectificador),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(rectificador[campo] != null)
						{
							if(typeof rectificador[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+rectificador[campo];
							}
							else
							{
								if(campo == "anio_servicio")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+rectificador[campo].toISOString();
								}
								else
								{
									if(rectificador[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(rectificador[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+rectificador[campo].id;
									}
										
								}
							}

							contador++;
						}
					}
				});
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("index.php/rectificadores/rectificador/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevoRectificador").bPopup().close();
					listarRectificadores();
					
					ref.nuevoRectificador = {
						modulos_rectif: [],
						nuevoModRectif: function(controlador_rectif = "0", numero_serie = ""){
							this.controlador_rectif = controlador_rectif.id;
							this.numero_serie = "";
							this.tipo_modulos_rectif = {
								amperaje: 0,
								capacidad: 0,
								voltaje: 0,
								marca: "",
								modelo: "",
								id: ""
							};
						}
					};																	
				}).error(function(err){

				});
			}
			
			ref.eliminandoRectificador = function(info){
				ref.eliminar_Rectificador = info;
				$("#eliminarRectificador").bPopup();
			}
			
			ref.cancelarEliminacion = function(){
				$("#eliminarRectificador").bPopup().close();
				ref.eliminar_Rectificador = {};
			}
			
			ref.eliminarRectificador = function(){
				var data = "comentario="+ref.eliminar_Rectificador.comentario;

				$http({
					url:"<?php echo base_url("index.php/rectificadores/rectificador");?>"+"/"+ref.eliminar_Rectificador.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded','X-Requested-With' : 'XMLHttpRequest'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.rectificadores = [];
						listarRectificadores();
						listarRectificadoresEliminados();
						ref.eliminar_Rectificador = {};
						$('#eliminarRectificador').bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.editarRectificador = function(rectificador){
				rectificador.editar = true;
			}
			
			ref.grabarRectificador = function(rectificador){
				borrarPropiedades(rectificador.modulos_rectif,"$$hashKey");
				borrarPropiedades(rectificador,"capacidadInstalada");
				borrarPropiedades(rectificador,"modulosInstalados");
				borrarPropiedades(rectificador,"nuevoModRectificador");
				borrarPropiedades(rectificador,"modulosCambio");
				
				var data = "",
					campos = Object.keys(rectificador),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(rectificador[campo] != null)
						{
							if(typeof rectificador[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+rectificador[campo];
							}
							else
							{
								if(campo == "anio_servicio")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+rectificador[campo].toISOString();
								}
								else
								{
									if(rectificador[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(rectificador[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+rectificador[campo].id;
									}
										
								}
							}

							contador++;
						}
					}
				});
				
				console.log(data);
				$http({
					url:"<?php echo base_url("rectificadores/rectificador");?>/"+rectificador.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					rectificador.editar = false;
					listarRectificadores();
				}).error(function(err){

				});
			}

			ref.cancelarRectificador = function(rectificador){
				rectificador.editar = false;
				listarRectificadores();
			}
			
			ref.nuevoModRectificador = function(rectificador){
				rectificador.modulos_rectif.push(new rectificador.nuevoModRectificador(rectificador.id));
			}
			
			ref.nuevoModRectif = function(nuevoRectificador){
				nuevoRectificador.modulos_rectif.push(new ref.nuevoRectificador.nuevoModRectif());
			}
			
			ref.eliminarNuevoModRectif = function(index){
				ref.nuevoRectificador.modulos_rectif.splice(index,1);
			}
			
			ref.eliminandoModRectificador = function(modRectificador){
				ref.eliminar_ModRectificador = modRectificador;
				$("#eliminarModRectificador").bPopup();
			}
			
			ref.cancelarEliminacionModRectificador = function(){
				$("#eliminarModRectificador").bPopup().close();
				ref.eliminar_ModRectificador.comentario = "";
				ref.eliminar_ModRectificador = {};
			}
			
			ref.eliminarModRectificador = function(){
				var data = "comentario="+ref.eliminar_ModRectificador.comentario;

				$http({
					url:"<?php echo base_url("index.php/modulos_rectif/rectificador");?>/"+ref.eliminar_ModRectificador.id,
					method:"DELETE",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(!(data.error))
					{
						$("#eliminarModRectificador").bPopup().close();
						ref.eliminar_ModRectificador.comentario = "";
						ref.eliminar_ModRectificador = {};
						ref.editar = false;
						
						listarRectificadores();
						listarRectificadoresEliminados();
					}
					else
					{
						ref.error.error = true;
						ref.error.actualizar = data.error;
					}
				}).error(function(err){
					
				});
			}
			
			ref.mostrarEliminados = function(){
				$("#mostrarEliminados").bPopup({
					onOpen: function(){
						document.getElementById("mostrarEliminados").className = "popup active";
					},
					onClose: function(){
						document.getElementById("mostrarEliminados").className = "popup";
					}
				});
			}
			
			ref.restaurarRectificador = function(rectificador){
				var data = "";
				
				$http({
					url:"<?php echo base_url("index.php/rectificadores/restaurar");?>"+"/"+rectificador.id,
					method:"PUT",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.rectificadores = [];
						ref.rectificadores_eliminados = [];
						listarRectificadores();
						listarRectificadoresEliminados();
						$('#mostrarEliminados').bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.eliminandoDefinitivoRectificador = function(rectificador){
				ref.eliminar_DefinitivoRectificador = rectificador;
				$("#eliminarDefinitivoRectificador").bPopup();
			}
			
			ref.cancelarEliminacionDefinitivoRectificador = function(){
				$("#eliminarDefinitivoRectificador").bPopup().close();
				ref.eliminar_DefinitivoRectificador.comentario = "";
				ref.eliminar_DefinitivoRectificador = {};
			}
			
			ref.eliminarDefinitivoRectificador = function(){
				var data = "",
					rectificador = ref.eliminar_DefinitivoRectificador,
					campos = Object.keys(rectificador),
					contador = 0;

				data += "comentario="+rectificador.comentario;

				$http({
					url:"<?php echo base_url("index.php/rectificadores/eliminar_rectificador");?>/"+rectificador.id,
					method:"DELETE",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					/*console.log(data);*/
					if(!(data.error))
					{
						$("#eliminarDefinitivoRectificador").bPopup().close();
						ref.eliminar_DefinitivoRectificador.comentario = "";
						ref.eliminar_DefinitivoRectificador = {};
						
						ref.rectificadores_eliminados = [];
						/*listarRectificadores();*/
						listarRectificadoresEliminados();
					}
					else
					{
						ref.error.error = true;
						ref.error.actualizar = data.error;
					}
				}).error(function(err){
					
				});
			}

			ref.radio = {
				planta: false,
				modulo: false,
				controlador: false
			}

			ref.almacenandoRectificador = function(rectificador){
				ref.almacenar_Rectificador = rectificador;
				ref.almacenar_Rectificador.tipo_almacen = 1;

				ref.almacenar_Rectificador.modulos_rectif.forEach(function(modulo) {
					modulo.check = false;
				});

				$("#almacenarRectificador").bPopup();
			}

			ref.almacenarRectificador = function(rectificador)
			{
				rectificador.modulos_rectif.forEach(function(modulo){
					if(modulo.check)
					{
						ref.almacenar_Modulos.push(modulo);
					}
				});

				var data = "estado="+rectificador.estado
						+"&proveniente_sitio="+rectificador.sitio
						+"&tipo_almacen="+rectificador.tipo_almacen
						+"&almacen="+((rectificador.tipo_almacen == 1)? rectificador.almacen : rectificador.a_sitio)
						+"&comentario="+rectificador.comentario
						+"&modulos_rectif="+angular.toJson(ref.almacenar_Modulos);

				$http({
					url:"<?php echo base_url("modulos_rectif/almacenar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					ref.almacenar_Rectificador = {};
					ref.almacenar_Modulos = [];
					listarRectificadores();
					$("#almacenarRectificador").bPopup().close();
				}).error(function(err){
					console.log(err);
				});
				console.log(data);
			}

			ref.cancelarAlmacenamiento = function(){
				ref.almacenar_Rectificador = {};
				ref.almacenar_Modulos = [];
				$("#almacenarRectificador").bPopup().close();
			}

			ref.disabledAlmacenamiento = function() {
				var dis = false;

				if(Object.keys(ref.almacenar_Rectificador).length) {
					ref.almacenar_Rectificador.modulos_rectif.forEach(function(modulo) {
						dis = dis || modulo.check;
					});
				}

				if(!ref.almacenar_Rectificador.estado || !ref.almacenar_Rectificador.comentario || (ref.almacenar_Rectificador.tipo_almacen == 1 && !ref.almacenar_Rectificador.almacen) || (ref.almacenar_Rectificador.tipo_almacen == 2 && !ref.almacenar_Rectificador.a_sitio)) {
					dis = false;
				}

				return dis;
			}

			ref.opcionSitio = function(){
				ref.almacenar_Rectificador.tipo_almacen = 2;
			}

			ref.opcionAlmacen = function(){
				ref.almacenar_Rectificador.tipo_almacen = 1;
			}
			
			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}

			/*serialize = function(obj){
				resultado = [];

				for (var property in obj)
				{
					if(Array.isArray(obj[property]))
					{
						resultado.push(property+"="+angular.toJson(obj[property]));
					}
					else
					{
						resultado.push(property+"="+encodeURIComponent(obj[property]));
					}
				}

				return resultado.join("&");
			}*/

			listarRectificadores();
			listarRectificadoresEliminados();
			listarTipoControladorRectif();
			listarTipoModulosRectif();
			listarEnergizacionRectificador();
			listarUbicacionRectificador();
			listarSitios();
			listarAlmacenes();
		}]);
</script>
</body>
</html>