<style>
.loading { border:1px solid #ddd; padding:20px; margin:40px 5px; width:80px;}	
</style>

<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12">
			<div class="panel panel-primary" ng-if="ctrl.idus.length">
				<div class="panel-heading text-center">
					IDUs
				</div>
				<div class="panel-body">
					<div>
						<table class="table table-striped table-condensed">
							<thead>
								<tr>
									<th>Acciones</th>
									<th>NE ID</th>
									<th>Link Name</th>
									<th>Marca</th>
									<th>Modelo</th>
									<th>Número de serie</th>
									<th>Historial</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="idu in ctrl.idus">
									<td>
										<button class="btn btn-warning btn-xs" ng-click="ctrl.editarIDU(idu)" ng-show="!idu.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
										<button class="btn btn-success btn-xs" ng-click="ctrl.grabarIDU(idu)" ng-hide="!idu.editar" ng-disabled="idu.disabled() || !idu.ne_id_l || !idu.ne_id_r">Grabar</button>
										<button class="btn btn-danger btn-xs" ng-click="ctrl.cancelarIDU(idu)" ng-hide="!idu.editar">Cancelar</button>
										<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminandoIDU(idu)" ng-show="!idu.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
									</td>
									<td>
										<span ng-show="!idu.editar">{{idu.ne_id}}</span>
										<div class="input-group input-group-sm" ng-hide="!idu.editar" style="max-width:190px;">
											<input class="form-control" type="number" min="1" step="any" ng-model="idu.ne_id_l"/>
											<span class="input-group-addon">-</span>
											<input class="form-control" type="number" min="1" step="any" ng-model="idu.ne_id_r"/>
										</div>
										<div id="alert-actualizar{{idu.id}}" class="alert alert-danger" ng-hide="!ctrl.error.actualizar"></div>
									</td>
									<td>
										<span ng-show="!idu.editar">{{idu.link_name}}</span>
										<input class="form-control input-sm" type="text" ng-hide="!idu.editar" ng-model="idu.link_name">
									</td>
									<td>
										<span ng-show="!idu.editar">{{idu.tipo_idu.marca}}</span>
										<select class="form-control input-sm" ng-hide="!idu.editar" ng-model="idu.tipo_idu.marca" ng-change="idu.tipo_idu.id = '';">
											<option value="">Elegir marca</option>
											<option ng-repeat="marca in ctrl.tipo_idu.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
										</select>
									</td>
									<td>
										<span ng-show="!idu.editar" bs-popover>{{idu.tipo_idu.modelo}} <span style="color:#326497;" class="fa fa-info-circle" rel="popover" data-trigger="hover" data-html="true" data-container="body" data-toggle="popover" data-title="Datos generales de IDU" data-content="<div style='max-width:320px;min-width:270px;'><i class='fa fa-asterisk'></i> Consumo de Energía: {{idu.tipo_idu.consumo_energia}}<br><i class='fa fa-asterisk'></i> Capacidad de Conmutación: {{idu.tipo_idu.capacidad_conmutacion}} GHz<br><i class='fa fa-asterisk'></i> Peso: {{idu.tipo_idu.peso}} Kg<br><i class='fa fa-asterisk'></i> Dimensiones: {{idu.tipo_idu.dimensiones}}</div>"></span></span>
										<select class="form-control input-sm" ng-hide="!idu.editar" ng-model="idu.tipo_idu.id">
											<option value="">Elegir modelo</option>
											<option ng-if="idu.tipo_idu.marca" ng-repeat="modelo in ctrl.tipo_idu.modelos | filter:{marca:idu.tipo_idu.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
										</select>
									</td>
									<td>
										<span ng-show="!idu.editar">{{idu.numero_serie}}</span>
										<input class="form-control input-sm" type="text" ng-hide="!idu.editar" ng-model="idu.numero_serie"/>
									</td>
									<td style="font-size:8pt;">
										<span ng-if="!idu.logs_idus.length || idu.logs_idus[0].evento.tipo != 'INSERT'">Creado: {{idu.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>
										<span ng-if="idu.logs_idus[0].evento.tipo == 'INSERT'">Creado: {{idu.logs_idus[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{idu.logs_idus[0].usuario.nombres}} {{idu.logs_idus[0].usuario.apellido_paterno}}</span>
										<br>
										<span ng-if="idu.logs_idus[1].evento.tipo == 'UPDATE'">Actualizado: {{idu.logs_idus[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{idu.logs_idus[1].usuario.nombres}} {{idu.logs_idus[1].usuario.apellido_paterno}}</span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.idus.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:15px;" ng-click="ctrl.nuevo_idu()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir IDU</button>
		</div>
	</div>
</div>

<div id="nuevoIDU" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Nueva IDU</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr >
					<td class="info">NE ID</td>
					<td class="default" >
						<div class="input-group codigo-idu-new" style="max-width:280px;">
						
							<input   class="form-control" type="number" onkeyup="this.value = this.value.replace(/[^0-9]/)" min="1" step="any" ng-model="ctrl.nuevoIDU.ne_id_l"/>

							<span class="input-group-addon">-</span>
							<input   class="form-control" type="number" onkeyup="this.value = this.value.replace(/[^0-9]/)" min="1" step="any" ng-model="ctrl.nuevoIDU.ne_id_r"/>
						

						</div>
						<br>
                        <span id="idu-coincidente" class="redtext"></span>
					</td>
				</tr>
				<tr>
					<td class="info">Link Name</td>
					<td class="default">
						<input id="link_name" class="form-control activa_click" type="text" ng-model="ctrl.nuevoIDU.link_name">
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoIDU.tipo_idu.marca">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_idu.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoIDU.tipo_idu.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoIDU.tipo_idu.marca" ng-repeat="modelo in ctrl.tipo_idu.modelos | filter:{marca:ctrl.nuevoIDU.tipo_idu.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="ctrl.nuevoIDU.numero_serie"/>
					</td>
				</tr>
			</tbody>
		</table>
		<div id="alert-crear" class="alert alert-danger" ng-hide="!ctrl.error.crear"></div>
		<loading></loading>
		<button class="btn btn-success" ng-click="ctrl.agregarIDU(ctrl.nuevoIDU)" ng-disabled="!ctrl.nuevoIDU.ne_id_l || !ctrl.nuevoIDU.ne_id_r || !ctrl.nuevoIDU.tipo_idu.id || !ctrl.nuevoIDU.numero_serie">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionIDU()">Cancelar</button>
	</form>
</div>

<div id="eliminarIDU" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar IDU</h3>
	
	<div class="alert alert-danger">AL ELIMINAR LA IDU SE ELIMINARÁN TODOS SUS ENLACES ASOCIADOS</div>
	<p>¿Está seguro de eliminar la IDU?</p>
	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_IDU.comentario"></textarea>
	
	<button class="btn btn-success" ng-click="ctrl.eliminarIDU()" ng-disabled="!ctrl.eliminar_IDU.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>