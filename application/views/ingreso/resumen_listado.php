<br>
<div class="container">
	<div class="row ng-cloak" ng-show="(ctrl.sitios.cuentaTotal() + ctrl.idus.cuentaTotal() + ctrl.objetos.cuentaTotal() + ctrl.sitiosSuministro.cuentaTotal())">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<nav class="navbar">
						<div class="container-fluid">
							<div class="navbar-header" style="color:rgb(245,121,23);">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-busqueda">
									<span class="sr-only" style="color:rgb(245,121,23);">Toggle navigation</span>
									<!--<span class="icon-bar" style="color:rgb(245,121,23);"></span>
									<span class="icon-bar" style="color:rgb(245,121,23);"></span>
									<span class="icon-bar" style="color:rgb(245,121,23);"></span>-->
									<span class="fa fa-bars"></span>
								</button>
							</div>
							<div class="collapse navbar-collapse" id="navbar-busqueda">
								<b><ul class="nav navbar-nav">
									<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.limitesListado.mostrarSitios()" href>Sitios <span class="badge">{{ctrl.sitios.cuentaTotal()}}</span></a></li>
									<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.limitesListado.mostrarIDUs()" href>IDU's <span class="badge">{{ctrl.idus.cuentaTotal()}}</span></a></li>
									<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.limitesListado.mostrarOtrosObjetos()" href>Otros objetos <span class="badge">{{ctrl.objetos.cuentaTotal()}}</span></a></li>
									<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.limitesListado.mostrarSitiosSuministro()" href>Búsqueda por #suministro <span class="badge">{{ctrl.sitiosSuministro.cuentaTotal()}}</span></a></li>
								</ul></b>
							</div>
						</div>
					</nav>
				</div>
				<div class="panel-body" style="height:375px;overflow:scroll">
					<table class="table table-condensed table-striped" ng-show="ctrl.limitesListado.showSitios">
						<thead>
							<tr>
								<th>Código</th>
								<th>Nombre completo</th>
								<th>Departamento</th>
								<th>Provincia</th>
								<th>Distrito</th>
								<th>Acciones</th>
								<th>Otros Objetos</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="sitio in ctrl.sitios.listado" ng-if="($index >= (ctrl.limitesListado.limite * (ctrl.limitesListado.paginaSitios-1))) && ($index < (ctrl.limitesListado.limite * ctrl.limitesListado.paginaSitios))">
								<td>{{sitio.codigo}}</td>
								<td>{{sitio.nombre_completo}} <span ng-if="sitio.estado != 2">(OFF AIR)</span></td>
								<td>{{sitio.zona_peru.departamento}}</td>
								<td>{{sitio.zona_peru.provincia}}</td>
								<td>{{sitio.zona_peru.distrito}}</td>
								<td>
									<a href="<?php echo base_url('ingreso/sitios/resumen');?>/{{sitio.id}}" target="_blank" class="btn btn-xs btn-info">Modificar sitio</a>
								</td>
								<td>
									<a class="btn btn-xs btn-info" ng-click="ctrl.buscar_objetos(sitio.id)" href>Ver otros objetos</a>
								</td>
							</tr>
						</tbody>
					</table>

					<table class="table table-condensed table-striped" ng-show="ctrl.limitesListado.showIDUs">
						<thead>
							<tr>
								<th>NE ID</th>
								<th>Número de serie</th>
								<th>Marca</th>
								<th>Modelo</th>
								<th>Estación base</th>
								<th>Acciones</th>
							</tr>
						</thead>
					</table>

					<table class="table table-condensed table-striped" ng-show="ctrl.limitesListado.showOtrosObjetos">
						<thead>
							<tr>
								<th>Objeto</th>
								<th>Orden</th>
								<th>Acciones</th>
								<!--<th>Acciones</th>-->
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="objeto in ctrl.objetos.listado">
								<td>{{objeto.tipo_objeto}}</td>
								<td>{{($index + 1)}}</td>
								<td>
									<a href="<?php echo base_url('ingreso/sitios/energia');?>/{{objeto.link}}/{{objeto.sitio}}" target="_blank" class="btn btn-xs btn-info">Modificar</a>
									<button class="btn btn-xs btn-info" ng-if="objeto.link == 'grupos_electrogenos'" ng-click="ctrl.verMantGrupoElectrogeno(objeto.id)">Ver Mantenimientos</button>
									<button class="btn btn-xs btn-info" ng-if="objeto.link == 'grupos_electrogenos'" ng-click="ctrl.nuevoMantGrupoElectrogeno(objeto.id)">Registrar Mantenimiento</button>
									<button class="btn btn-xs btn-info" ng-if="objeto.link == 'lineas_electricas'" ng-click="ctrl.verMantLineaElectrica(objeto.id)">Ver Mantenimientos</button>
									<button class="btn btn-xs btn-info" ng-if="objeto.link == 'lineas_electricas'" ng-click="ctrl.nuevoMantLineaElectrica(objeto.id)">Registrar Mantenimiento</button>
								</td>
							</tr>
						</tbody>
					</table>
					
					<table class="table table-condensed table-striped" ng-show="ctrl.limitesListado.showSitiosSuministro">
						<thead>
							<tr>
								<th>Código</th>
								<th>Nombre completo</th>
								<th>Departamento</th>
								<th>Provincia</th>
								<th>Distrito</th>
								<th>Acciones</th>
								<th>Otros Objetos</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="sitio in ctrl.sitiosSuministro.listado" ng-if="($index >= (ctrl.limitesListado.limite * (ctrl.limitesListado.paginaSitiosSuministro-1))) && ($index < (ctrl.limitesListado.limite * ctrl.limitesListado.paginaSitiosSuministro))">
								<td>{{sitio.codigo}}</td>
								<td>{{sitio.nombre_completo}}</td>
								<td>{{sitio.zona_peru.departamento}}</td>
								<td>{{sitio.zona_peru.provincia}}</td>
								<td>{{sitio.zona_peru.distrito}}</td>
								<td>
									<a href="<?php echo base_url('ingreso/sitios/resumen');?>/{{sitio.sitio}}" target="_blank" class="btn btn-xs btn-info">Modificar sitio</a>
								</td>
								<td>
									<a class="btn btn-xs btn-info" ng-click="ctrl.buscar_objetos(sitio.sitio)" href>Ver otros objetos</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer text-center">
					<nav aria-label="Page navigation">
						<!--<ul class="pagination" ng-show="ctrl.limitesListado.showSitios">
							<li>
								<a href aria-label="Previous" ng-click="ctrl.sitios.paginaMenos()">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							<li ng-repeat="pagina in ctrl.sitios.arrayPaginador(ctrl.limitesListado.limite)" ng-click="ctrl.sitios.pagina(pagina)" ng-class="{active:(ctrl.limitesListado.paginaSitios == pagina)}"><a href>{{pagina}}</a></li>
							<li>
								<a href aria-label="Next" ng-click="ctrl.sitios.paginaMas()">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul>-->
						
						<ul class="pagination" ng-show="ctrl.limitesListado.showSitios">
							<li>
								<a href aria-label="Previous" ng-click="ctrl.sitios.paginaMenos()">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							<li ng-repeat="pagina in ctrl.sitios.fPaginador(ctrl.limitesListado.paginaSitios)" ng-click="ctrl.sitios.pagina(pagina)" ng-class="{active:(ctrl.limitesListado.paginaSitios == pagina)}"><a href ng-if="pagina > 0">{{pagina}}</a><span ng-if="pagina <= 0">...</span></li>
							<li>
								<a href aria-label="Next" ng-click="ctrl.sitios.paginaMas()">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul>

						<ul class="pagination" ng-show="ctrl.limitesListado.showIDUs">
							<li>
								<a href aria-label="Previous" ng-click="ctrl.idus.paginaMenos()">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							<li ng-repeat="pagina in ctrl.idus.arrayPaginador(ctrl.limitesListado.limite)" ng-click="ctrl.idus.pagina(pagina)" ng-class="{active:(ctrl.limitesListado.paginaIDUs == pagina)}"><a href>{{pagina}}</a></li>
							<li>
								<a href aria-label="Next" ng-click="ctrl.idus.paginaMas()">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul>

						<ul class="pagination" ng-show="ctrl.limitesListado.showOtrosObjetos">
							<li>
								<a href aria-label="Previous" ng-click="ctrl.objetos.paginaMenos()">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							<li ng-repeat="pagina in ctrl.objetos.arrayPaginador(ctrl.limitesListado.limite)" ng-click="ctrl.objetos.pagina(pagina)" ng-class="{active:(ctrl.limitesListado.paginaOtrosObjetos == pagina)}"><a href>{{pagina}}</a></li>
							<li>
								<a href aria-label="Next" ng-click="ctrl.objetos.paginaMas()">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul>
						
						<ul class="pagination" ng-show="ctrl.limitesListado.showSitiosSuministro">
							<li>
								<a href aria-label="Previous" ng-click="ctrl.sitiosSuministro.paginaMenos()">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							<li ng-repeat="pagina in ctrl.sitiosSuministro.fPaginador(ctrl.limitesListado.paginaSitiosSuministro)" ng-click="ctrl.sitiosSuministro.pagina(pagina)" ng-class="{active:(ctrl.limitesListado.paginaSitiosSuministro == pagina)}"><a href ng-if="pagina > 0">{{pagina}}</a><span ng-if="pagina <= 0">...</span></li>
							<li>
								<a href aria-label="Next" ng-click="ctrl.sitiosSuministro.paginaMas()">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="verMantenimientoGrupoElectrogeno" class="popup" style="width:1200px;">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Mantenimientos de Grupo Electrógeno</h3>
	
	<br>
	<form class="ng-cloak" style="max-height:300px;overflow-y:scroll;" ng-if="ctrl.verMantenimientoGrupoElectrogeno.length">
		<table class="table table-condensed table-striped">
			<thead>
				<tr>
					<th style="width:150px">Acciones</th>
					<th>Tipo</th>
					<th>Fecha</th>
					<th>Horas</th>
					<th>Fecha de Registro</th>
					<th>Fecha de Última Modificación</th>
					<th>Observaciones</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat-start="mantenimiento in ctrl.verMantenimientoGrupoElectrogeno">
					<td>
						<button class="btn btn-xs btn-warning" ng-show="!mantenimiento.editar" ng-click="ctrl.editarMantGE(mantenimiento)" ng-disabled="ctrl.auth.au2">Editar</button>
						<button class="btn btn-xs btn-success" ng-hide="!mantenimiento.editar" ng-click="ctrl.grabarMantGE(mantenimiento)" ng-disabled="!mantenimiento.tipo_mantenimiento_grupo_electrogeno.id || !mantenimiento.fecha || !mantenimiento.horas">Guardar</button>
						<button class="btn btn-xs btn-danger" ng-hide="!mantenimiento.editar" ng-click="ctrl.cancelarMantGE(mantenimiento)">Cancelar</button>
					</td>
					<td>
						<span ng-show="!mantenimiento.editar">{{mantenimiento.tipo_mantenimiento_grupo_electrogeno.tipo_mantenimiento}}</span>
						<select class="form-control input-sm" ng-hide="!mantenimiento.editar" ng-model="mantenimiento.tipo_mantenimiento_grupo_electrogeno.id">
							<option value="">Elegir tipo</option>
							<option ng-repeat="tipo in ctrl.tipos_mantenimiento_ge" value="{{tipo.id}}">{{tipo.tipo_mantenimiento}}</option>
						</select>
					</td>
					<td>
						<span ng-show="!mantenimiento.editar">{{mantenimiento.fecha | date:"dd/MM/yyyy"}}</span>
						<input class="form-control input-sm" type="date" ng-hide="!mantenimiento.editar" ng-model="mantenimiento.fecha"/>
					</td>
					<td>
						<span ng-show="!mantenimiento.editar">{{mantenimiento.horas}}</span>
						<input class="form-control input-sm" type="number" min="0" step="any" ng-hide="!mantenimiento.editar" ng-model="mantenimiento.horas"/>
					</td>
					<td style="font-size:8pt">{{mantenimiento.created_at}} por {{mantenimiento.logs_mantenimientos_grupos_electrogenos[1].usuario.nombres}} {{mantenimiento.logs_mantenimientos_grupos_electrogenos[1].usuario.apellido_paterno}}</td>
					<td style="font-size:8pt">{{mantenimiento.logs_mantenimientos_grupos_electrogenos[mantenimiento.logs_mantenimientos_grupos_electrogenos.length - 1].created_at}} por {{mantenimiento.logs_mantenimientos_grupos_electrogenos[mantenimiento.logs_mantenimientos_grupos_electrogenos.length - 1].usuario.nombres}} {{mantenimiento.logs_mantenimientos_grupos_electrogenos[mantenimiento.logs_mantenimientos_grupos_electrogenos.length - 1].usuario.apellido_paterno}}</td>
					<td>
						<button class="btn btn-xs btn-info" ng-click="ctrl.verObservacion(mantenimiento)" ng-show="!mantenimiento.mostrar">Ver</button>
						<button class="btn btn-xs btn-warning" ng-click="ctrl.ocultarObservacion(mantenimiento)" ng-hide="!mantenimiento.mostrar">Ocultar</button>
					</td>
				</tr>
				<tr ng-repeat-end ng-hide="!mantenimiento.mostrar">
					<td class="info">Observaciones</td>
					<td colspan="6">
						<span ng-show="!mantenimiento.editar">{{mantenimiento.observaciones}}</span>
						<input class="form-control" type="text" ng-hide="!mantenimiento.editar" ng-model="mantenimiento.observaciones"/>
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<div id="nuevoMantenimientoGrupoElectrogeno" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Registrar mantenimiento de Grupo Electrógeno</h3>
	
	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Tipo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoMantenimientoGrupoElectrogeno.tipo_mantenimiento_grupo_electrogeno.id">
							<option value="">Elegir tipo</option>
							<option ng-repeat="tipo in ctrl.tipos_mantenimiento_ge" value="{{tipo.id}}">{{tipo.tipo_mantenimiento}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Fecha</td>
					<td class="default">
						<input class="form-control" type="text" id="datepicker" size="30" ng-model="ctrl.nuevoMantenimientoGrupoElectrogeno.fecha"/>
					</td>
				</tr>
				<tr>
					<td class="info">Horas</td>
					<td class="default">
						<input class="form-control" type="number" min="1" step="any" ng-model="ctrl.nuevoMantenimientoGrupoElectrogeno.horas"/>
					</td>
				</tr>
			</tbody>
		</table>
		
		<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba observaciones adicionales..." ng-model="ctrl.nuevoMantenimientoGrupoElectrogeno.observaciones"></textarea>
		
		<button class="btn btn-success" ng-click="ctrl.agregarMantenimientoGrupoElectrogeno(ctrl.nuevoMantenimientoGrupoElectrogeno)" ng-disabled="!ctrl.nuevoMantenimientoGrupoElectrogeno.tipo_mantenimiento_grupo_electrogeno.id || !ctrl.nuevoMantenimientoGrupoElectrogeno.fecha || !ctrl.nuevoMantenimientoGrupoElectrogeno.horas">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarNuevoMantenimientoGE()">Cancelar</button>
	</form>
</div>

<div id="verMantenimientoLineaElectrica" class="popup" style="width:1200px;">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Mantenimientos de Línea Eléctrica</h3>
	
	<br>
	<form class="ng-cloak" style="max-height:300px;overflow-y:scroll;" ng-if="ctrl.verMantenimientoLineaElectrica.length">
		<table class="table table-condensed table-striped">
			<thead>
				<tr>
					<th style="width:150px">Acciones</th>
					<th>Tipo</th>
					<th>Fecha</th>
					<th>Fecha de Registro</th>
					<th>Fecha de Última Modificación</th>
					<th>Observaciones</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat-start="mantenimiento in ctrl.verMantenimientoLineaElectrica">
					<td>
						<button class="btn btn-xs btn-warning" ng-show="!mantenimiento.editar" ng-click="ctrl.editarMantLE(mantenimiento)" ng-disabled="ctrl.auth.au2">Editar</button>
						<button class="btn btn-xs btn-success" ng-hide="!mantenimiento.editar" ng-click="ctrl.grabarMantLE(mantenimiento)" ng-disabled="!mantenimiento.tipo_mantenimiento_linea_electrica.id || !mantenimiento.fecha || !mantenimiento.horas">Guardar</button>
						<button class="btn btn-xs btn-danger" ng-hide="!mantenimiento.editar" ng-click="ctrl.cancelarMantLE(mantenimiento)">Cancelar</button>
					</td>
					<td>
						<span ng-show="!mantenimiento.editar">{{mantenimiento.tipo_mantenimiento_linea_electrica.tipo_mantenimiento}}</span>
						<select class="form-control input-sm" ng-hide="!mantenimiento.editar" ng-model="mantenimiento.tipo_mantenimiento_linea_electrica.id">
							<option value="">Elegir tipo</option>
							<option ng-repeat="tipo in ctrl.tipo_mantenimiento_le" value="{{tipo.id}}">{{tipo.tipo_mantenimiento}}</option>
						</select>
					</td>
					<td>
						<span ng-show="!mantenimiento.editar">{{mantenimiento.fecha | date:"dd/MM/yyyy"}}</span>
						<input class="form-control input-sm" type="date" ng-hide="!mantenimiento.editar" ng-model="mantenimiento.fecha"/>
					</td>
					<td style="font-size:8pt">{{mantenimiento.created_at}} por {{mantenimiento.logs_mantenimientos_lineas_electricas[1].usuario.nombres}} {{mantenimiento.logs_mantenimientos_lineas_electricas[1].usuario.apellido_paterno}}</td>
					<td style="font-size:8pt">{{mantenimiento.logs_mantenimientos_lineas_electricas[mantenimiento.logs_mantenimientos_lineas_electricas.length - 1].created_at}} por {{mantenimiento.logs_mantenimientos_lineas_electricas[mantenimiento.logs_mantenimientos_lineas_electricas.length - 1].usuario.nombres}} {{mantenimiento.logs_mantenimientos_lineas_electricas[mantenimiento.logs_mantenimientos_lineas_electricas.length - 1].usuario.apellido_paterno}}</td>
					<td>
						<button class="btn btn-xs btn-info" ng-click="ctrl.verObservacion(mantenimiento)" ng-show="!mantenimiento.mostrar">Ver</button>
						<button class="btn btn-xs btn-warning" ng-click="ctrl.ocultarObservacion(mantenimiento)" ng-hide="!mantenimiento.mostrar">Ocultar</button>
					</td>
				</tr>
				<tr ng-repeat-end ng-hide="!mantenimiento.mostrar">
					<td class="info">Observaciones</td>
					<td colspan="6">
						<span ng-show="!mantenimiento.editar">{{mantenimiento.observaciones}}</span>
						<input class="form-control" type="text" ng-hide="!mantenimiento.editar" ng-model="mantenimiento.observaciones"/>
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<div id="nuevoMantenimientoLineaElectrica" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Registrar mantenimiento de Línea Eléctrica</h3>
	
	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Tipo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoMantenimientoLineaElectrica.tipo_mantenimiento_linea_electrica.id">
							<option value="">Elegir tipo</option>
							<option ng-repeat="tipo in ctrl.tipo_mantenimiento_le" value="{{tipo.id}}">{{tipo.tipo_mantenimiento}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Fecha</td>
					<td class="default">
						<input class="form-control" type="text" id="datepickerle" size="30" ng-model="ctrl.nuevoMantenimientoLineaElectrica.fecha"/>
					</td>
				</tr>
			</tbody>
		</table>
		
		<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba observaciones adicionales..." ng-model="ctrl.nuevoMantenimientoLineaElectrica.observaciones"></textarea>
		
		<button class="btn btn-success" ng-click="ctrl.agregarMantenimientoLineaElectrica(ctrl.nuevoMantenimientoLineaElectrica)" ng-disabled="!ctrl.nuevoMantenimientoLineaElectrica.tipo_mantenimiento_linea_electrica.id || !ctrl.nuevoMantenimientoLineaElectrica.fecha">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarNuevoMantenimientoLE()">Cancelar</button>
	</form>
</div>