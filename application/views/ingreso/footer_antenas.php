<script type="text/javascript">
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover();
	});
	function serializeObj(obj, toDelete = []) {
		var result = [];

		if (!Array.isArray(toDelete)) {
			toDelete = toDelete.split(",");
		}

		if (toDelete.length) {
			toDelete.forEach(function(del) {
				delete obj[del];
			});
		}
		
				
		for (var property in obj) {
			if (property != 'id' && property != 'created_at' && property != null && typeof obj[property] != 'function') {
				if (Array.isArray(obj[property])) {
					result.push(encodeURIComponent(property) + '=' + angular.toJson(obj[property]));
				} else if (typeof obj[property] === 'object') {
					try { result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]['id'])); }
					catch(err) { result.push(encodeURIComponent(property) + '=' + null); }
				} else if (property == 'tipo_almacen') {
					if (obj[property] == 1) {
						result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
						result.push('almacen=' + encodeURIComponent(obj['almacen']));
					} else if (obj[property] == 2) {
						result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
						result.push('almacen=' + encodeURIComponent(obj['a_sitio']));
						delete obj['a_sitio'];
					}
				} else {
					result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
				}
			}
		}

		return result.join('&');
	}

	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.sitios = [];
			ref.almacenes = [];

			ref.antenasMicroondas = [];
			ref.antenasMicroondasPendiente = [];
			ref.antenasGUL = [];
			ref.antenasGULPendiente = [];
			ref.antenas_in_building = [];
            //ref.cargando = false;
			ref.marcasMicroondas = [];
			ref.tipo_antena_gul = [];
			ref.tipo_antena_gul_pendiente = [];

			ref.modelosMicroondas = [];
			ref.modelosGUL = [];
			ref.versionesMicroondas = [];
			ref.modelos_in_building = [];

			ref.nuevaAntenaMicroondas = {};
			ref.editarAntenaMicroondas = {};
			ref.eliminarAntenaMicroondas = {};
			ref.almacenar_AntenaMicroondas = {};
			
			ref.nuevaAntenaGUL = {
				info_telnet: [],
				crearExtras: function(){
					this.sector = "";
					this.azimuth = "";
					this.tilt_electrico = "";
					this.tilt_mecanico = "";
				}
			};

			ref.nuevaAntenaGULPendiente = {
			info_telnet: [],
			crearExtras: function(){
				this.sector = "";
				this.azimuth = "";
				this.tilt_electrico = "";
				this.tilt_mecanico = "";
			}
		    };

			ref.editarAntenaGUL = {
				info_telnet: []
			};
			ref.oldData = [];
			ref.almacenar_AntenaGUL = {};
			ref.importarAntenaGUL = {};
			
			ref.nuevaAntenaInBuilding = {};
			ref.editarAntenaInBuilding = {};
			ref.eliminar_AntenaInBuilding = {};
			
			ref.info_sitio = {};

			ref.config = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Buscar sitio',
				maxItems: 1
			};
			
			listarAntenas = function(){

				$http({
					url:"<?php echo base_url("antenas_mw/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenasMicroondas = data;
					}
					else
					{

					}
				}).error(function(err){

				});
//------------- microondas pendiente
				$http({
					url:"<?php echo base_url("antenas_mw/listar_sitio_pendiente");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenasMicroondasPendiente = data;
					}
					else
					{

					}
				}).error(function(err){

				});
//------------- microondas pendiente



				$http({
					url:"<?php echo base_url("index.php/antenas_gul/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenasGUL = data;

						console.log(ref.antenasGUL);

						ref.antenasGUL.forEach(function(antGUL,indx,objx){
							try {objx[indx].created_at = Date.parse(objx[indx].created_at);} catch(err) {objx[indx].created_at = null;}
							//console.log(data);
							objx[indx].logs_antenas_gul.forEach(function(log,ind,obj){
								try {obj[ind].created_at = Date.parse(obj[ind].created_at);} catch(err) {obj[ind].created_at = null;}
							});
						});
						
						//console.log(data);
					}
					else
					{

					}
				}).error(function(err){

				});

				//--- listando antenas gul pendientes 
				$http({
					url:"<?php echo base_url("index.php/antenas_gul_pendiente/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){

					if(data.length)
					{
						ref.antenasGULPendiente = data;
				       
      //                  if(ref.antenasGULPendiente[0].tipo_antena_gul.marca == 'TELNET')
						// {  

      //                    ref.antenasGULPendiente[tilt_electrico_telnet] =  ref.antenasGULPendiente[0].info_telnet[0].tilt_electrico+"/"+ref.antenasGULPendiente[0].info_telnet[1].tilt_electrico+"/"+ref.antenasGULPendiente[0].info_telnet[2].tilt_electrico;
                           
						// }

	ref.antenasGULPendiente.forEach(function(antGULPendiente,indx,objx){
							try {objx[indx].created_at = Date.parse(objx[indx].created_at);} catch(err) {objx[indx].created_at = null;}
							//console.log(data);

							// objx[indx].logs_antenas_gul_pendiente.forEach(function(log,ind,obj){
							// 	try {obj[ind].created_at = Date.parse(obj[ind].created_at);} catch(err) {obj[ind].created_at = null;}
							// 		});


									});

				
					}
					else
					{

					}
				}).error(function(err){

				});
				//--- listando antenas gul pendientes 


				$http({
					url:"<?php echo base_url("index.php/antenas_iden/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenasIDEN = data;
					}
					else
					{

					}
				}).error(function(err){

				});
				
				$http({
					url:"<?php echo base_url("index.php/antenas_in_building/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenas_in_building = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarSitios = function(){
				$http({
					url:"<?php echo base_url("status_site/listar_sitios_total");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sitios = data;
					}
				}).error(function(err){

				});
			}

			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.almacenes = data;
					}
				}).error(function(err){

				});
			}

			/*********************************************************************************************************/
			/*	Antenas Microondas																					 */
			/*********************************************************************************************************/

			listarMarcasMicroondas = function(){
				$http({
					url:"<?php echo base_url("tipo_antena_mw/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.marcasMicroondas = data;
					}
				}).error(function(err){

				});
			}

			ref.listarModelosMicroondas = function(marca){
				$http({
					url:"<?php echo base_url("index.php/tipo_antena_microondas/modelos");?>"+"/"+encodeURI(marca),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.modelosMicroondas = data;
					}
				}).error(function(err){

				});
			}

			/*ref.listarVersionesMicroondas = function(marca,modelo){
				$http({
					url:"<?php //echo base_url("index.php/tipo_antena_microondas/versiones");?>"+"/"+encodeURI(marca)+"/"+encodeURI(modelo),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.versionesMicroondas = data;
					}
				}).error(function(err){

				});
			}*/

			ref.agregarMicroondas = function(){
				var data = "numero_serie="+ref.nuevaAntenaMicroondas.numero_serie
							+"&numero_inventario="+ref.nuevaAntenaMicroondas.numero_inventario
							+"&marca="+ref.nuevaAntenaMicroondas.marca
							+"&modelo="+ref.nuevaAntenaMicroondas.modelo
							+"&version="+ref.nuevaAntenaMicroondas.version;

				$http({
					url:"<?php echo base_url("index.php/antenas_microondas/antena/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.nuevaAntenaMicroondas = {};
						listarAntenas();
						$('#nuevaAntenaMicroondas').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarCreacionMicroondas = function(){
				ref.nuevaAntenaMicroondas = {};
				$('#nuevaAntenaMicroondas').bPopup().close();
			}

			ref.editar_antenaMicroondas = function(id){
				$http({
					url:"<?php echo base_url("index.php/antenas_microondas/antena");?>"+"/"+id,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.editarAntenaMicroondas = data[0];
						ref.listarModelosMicroondas(ref.editarAntenaMicroondas.tipo_antena_microondas.marca);
						ref.listarVersionesMicroondas(ref.editarAntenaMicroondas.tipo_antena_microondas.marca,ref.editarAntenaMicroondas.tipo_antena_microondas.modelo);
						$('#editarAntenaMicroondas').bPopup();
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.actualizarMicroondas = function(id){
				var data = "numero_serie="+ref.editarAntenaMicroondas.numero_serie
							+"&numero_inventario="+ref.editarAntenaMicroondas.numero_inventario
							+"&marca="+ref.editarAntenaMicroondas.tipo_antena_microondas.marca
							+"&modelo="+ref.editarAntenaMicroondas.tipo_antena_microondas.modelo
							+"&version="+ref.editarAntenaMicroondas.tipo_antena_microondas.version;

				$http({
					url:"<?php echo base_url("index.php/antenas_microondas/antena/");?>"+"/"+id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.length)
					{

					}
					else
					{
						$('#editarAntenaMicroondas').bPopup().close();
						ref.editarAntenaMicroondas = {};
						listarAntenas();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEdicionMicroondas = function(){
				ref.editarAntenaMicroondas = {};
				$('#editarAntenaMicroondas').bPopup().close();
			}

			ref.eliminandoAntenaMicroondas = function(id){
				$http({
					url:"<?php echo base_url("index.php/antenas_microondas/antena");?>"+"/"+id,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.eliminarAntenaMicroondas = data[0];
						$('#eliminarAntenaMicroondas').bPopup();
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.eliminarMicroondas = function(){
				$http({
					url:"<?php echo base_url("index.php/antenas_microondas/antena");?>"+"/"+ref.eliminarAntenaMicroondas.id,
					method:"DELETE"
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.antenasMicroondas = [];
						listarAntenas();
						ref.eliminarAntenaMicroondas = {};
						$('#eliminarAntenaMicroondas').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEliminacionMicroondas = function(){
				ref.eliminarAntenaMicroondas = {};
				$('#eliminarAntenaMicroondas').bPopup().close();
			}

			ref.almacenandoAntenaMicroondas = function(info){
				ref.almacenar_AntenaMicroondas = info;
				ref.almacenar_AntenaMicroondas.tipo_almacen = 1;
				$("#almacenarAntenaMicroondas").bPopup();
			}

			ref.almacenarAntenaMicroondas = function(antena){
				var data = "tipo_antena_mw="+antena.tipo_antena_mw.id
						+"&numero_serie="+antena.numero_serie
						+"&estado="+antena.estado
						+"&proveniente_sitio="+antena.sitio
						+"&tipo_almacen="+antena.tipo_almacen
						+"&almacen="+((antena.tipo_almacen == 1)? antena.almacen : antena.a_sitio)
						+"&comentario="+antena.comentario;
				//console.log(data);

				$http({
					url:"<?php echo base_url("antenas_mw/antena_mw/almacenar");?>/"+antena.id,
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#almacenarAntenaMicroondas").bPopup().close();
					ref.antenasMicroondas = [];
					listarAntenas();
					ref.almacenar_AntenaMicroondas = {};
				}).error(function(err){

				});
			}

			ref.cancelarAlmacenamientoMicroondas = function(){
				$("#almacenarAntenaMicroondas").bPopup().close();
				ref.almacenar_AntenaMicroondas = {};
			}

			ref.microondasOpcionSitio = function(){
				ref.almacenar_AntenaMicroondas.tipo_almacen = 2;
			}

			ref.microondasOpcionAlmacen = function(){
				ref.almacenar_AntenaMicroondas.tipo_almacen = 1;
			}

			/*********************************************************************************************************/
			/*	Antenas GUL																							 */
			/*********************************************************************************************************/

			listarTipoAntenasGUL = function(){
				$http({
					url:"<?php echo base_url("tipo_antena_gul/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_antena_gul = data;
					}
				}).error(function(err){

				});
			}			
		
			ref.cambioMarca = function(){
				if(ref.nuevaAntenaGUL.tipo_antena_gul.marca != "TELNET")
				{
					ref.nuevaAntenaGUL.info_telnet = [];
				}
				else
				{
					ref.nuevaAntenaGUL.sector = "";
					ref.nuevaAntenaGUL.azimuth = "";
					ref.nuevaAntenaGUL.tilt_electrico = "";
					ref.nuevaAntenaGUL.tilt_mecanico = "";
					
					for(var i = 0; i < 3; i++)
					{
						ref.nuevaAntenaGUL.info_telnet.push(new ref.nuevaAntenaGUL.crearExtras());
					}
				}
			}

			ref.agregarGUL = function(antena){
				borrarPropiedades(antena,"crearExtras");
				borrarPropiedades(antena.info_telnet,"$$hashKey");
				
				var data = "",
					campos = Object.keys(antena),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(antena[campo] != null)
						{
							if(typeof antena[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+antena[campo];
							}
							else
							{
								if(antena[campo].length != undefined)
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+JSON.stringify(antena[campo]);
								}
								else
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+antena[campo].id;
								}
							}

							contador++;
						}
					}
				});
				//console.log(data);

				$http({
					url:"<?php echo base_url("antenas_gul/antena/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
                    // console.log(data+"test");
					if(data.error)
					{

					}
					else
					{
						ref.nuevaAntenaGUL = {
							info_telnet: [],
							crearExtras: function(){
								this.sector = "";
								this.azimuth = "";
								this.tilt_electrico = "";
								this.tilt_mecanico = "";
							}
						};
						listarAntenas();
						$('#nuevaAntenaGUL').bPopup().close();
					}
				}).error(function(err){

				});
			}


    // Agregando Gul pendientes
            listarTipoAntenasGULPendiente = function(){
				$http({
					url:"<?php echo base_url("tipo_antena_gul/listar");?>",
					method:"GET"
				}).success(function(data){
					
					if(data.marcas.length)
					{
						ref.tipo_antena_gul_pendiente= data;
					}
				}).error(function(err){

				});
			}


            ref.cambioMarcaGulPendiente = function(){
				if(ref.nuevaAntenaGULPendiente.tipo_antena_gul.marca != "TELNET")
				{
					ref.nuevaAntenaGULPendiente.info_telnet = [];
				}
				else
				{
					ref.nuevaAntenaGULPendiente.sector = "";
					ref.nuevaAntenaGULPendiente.azimuth = "";
					ref.nuevaAntenaGULPendiente.tilt_electrico = "";
					ref.nuevaAntenaGULPendiente.tilt_mecanico = "";
					
					for(var i = 0; i < 3; i++)
					{
						ref.nuevaAntenaGULPendiente.info_telnet.push(new ref.nuevaAntenaGULPendiente.crearExtras());
					}
				}
			}
			



ref.agregarGULPendiente = function(antena){           
    //antena2 = ctrl.nuevaAntenaGUL    
	borrarPropiedades(antena,"crearExtras");
	borrarPropiedades(antena.info_telnet,"$$hashKey");    
    //reseteando form
    //$(".verifica_factor_uso").[0].reset();
    //ref.form.$setValidity();
    //form
	var data = "",
	campos = Object.keys(antena),
	contador = 0;

	campos.forEach(function(campo,ind,objeto){
		if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
		{
			if(antena[campo] != null)
			{
				if(typeof antena[campo] != "object")
				{
					if(contador > 0)
					{
						data += "&";
					}
					data += campo+"="+antena[campo];
				}
				else
				{
					if(antena[campo].length != undefined)
					{
						if(contador > 0)
						{
							data += "&";
						}
						data += campo+"="+JSON.stringify(antena[campo]);
					}
					else
					{
						if(contador > 0)
						{
							data += "&";
						}
						data += campo+"="+antena[campo].id;
					}
				}

				contador++;
			}
		}
	});
	//console.log(data);
         //alert(data);
         $http({
					
					url:"<?php echo base_url("antenas_gul_pendiente/antena/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data>0){
						alert("El Espacio  No esta disponible Elija otra altura o azimuth ");
					}else{

						if(data.error)
						{

						}
						else
						{
							ref.nuevaAntenaGULPendiente = {
								info_telnet: [],
								crearExtras: function(){
									this.sector = "";
									this.azimuth = "";
									this.tilt_electrico = "";
									this.tilt_mecanico = "";
								}
							};
							listarAntenas();
							$('#nuevaAntenaGUL').bPopup().close();
						}	
					}
					
				}).error(function(err){

				});
			}
// Agregando Gul Pendiente


			ref.cancelarCreacionGUL = function(){
				ref.nuevaAntenaGUL = {
					info_telnet: [],
					crearExtras: function(){
						this.sector = "";
						this.azimuth = "";
						this.tilt_electrico = "";
						this.tilt_mecanico = "";
					}
				};
				$('#nuevaAntenaGUL').bPopup().close();
			}
			
			ref.cambioMarcaE = function(){
				if(ref.editarAntenaGUL.tipo_antena_gul.marca != "TELNET")
				{
					ref.editarAntenaGUL.info_telnet = [];
				}
				else
				{
					ref.editarAntenaGUL.sector = "";
					ref.editarAntenaGUL.azimuth = "";
					ref.editarAntenaGUL.tilt_electrico = "";
					ref.editarAntenaGUL.tilt_mecanico = "";
					
					if(!ref.oldData.length)
					{
						for(var i = 0; i < 3; i++)
						{
							ref.editarAntenaGUL.info_telnet.push(new ref.editarAntenaGUL.crearExtras());
						}
					} 
					else
					{
						ref.editarAntenaGUL.info_telnet = ref.oldData;
					}
				}
			}

			ref.editar_antenaGUL = function(info){
				ref.editarAntenaGUL = info;
				ref.editarAntenaGUL["altura"] = Number(ref.editarAntenaGUL["altura"]);
				ref.editarAntenaGUL["azimuth"] = Number(ref.editarAntenaGUL["azimuth"]);
				ref.editarAntenaGUL["tilt_electrico"] = Number(ref.editarAntenaGUL["tilt_electrico"]);
				ref.editarAntenaGUL["tilt_mecanico"] = Number(ref.editarAntenaGUL["tilt_mecanico"]);
				
				ref.editarAntenaGUL.crearExtras = function(){
					this.sector = "";
					this.azimuth = "";
					this.tilt_electrico = "";
					this.tilt_mecanico = "";
				}
				
				if(ref.editarAntenaGUL.info_telnet.length)
				{
					ref.editarAntenaGUL.info_telnet.forEach(function(antena,ind,objeto){
						objeto[ind].azimuth = Number(objeto[ind].azimuth);
						objeto[ind].tilt_electrico = Number(objeto[ind].tilt_electrico);
						objeto[ind].tilt_mecanico = Number(objeto[ind].tilt_mecanico);
					});
				}
				
				ref.oldData = ref.editarAntenaGUL.info_telnet;
				
				$('#editarAntenaGUL').bPopup({
					amsl: 0
				});
			}

			ref.actualizarGUL = function(antena){
			    //ref.cargando = true;
				borrarPropiedades(antena,"crearExtras");
				borrarPropiedades(antena.info_telnet,"$$hashKey");
				var data = "",
					campos = Object.keys(antena),
					contador = 0;				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(antena[campo] != null)
						{
							if(typeof antena[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+antena[campo];
							}
							else
							{
								if(antena[campo].length != undefined)
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+JSON.stringify(antena[campo]);
								}
								else
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+antena[campo].id;
								}
							}

							contador++;
						}
					}
				});
				//console.log(data);
				$http({
					url:"<?php echo base_url("antenas_gul/antena");?>"+"/"+antena.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
                  
                   if(data>0){
						alert("El Espacio  No esta disponible Elija otra altura o azimuth ");
					}else{

                       $('#editarAntenaGUL').bPopup().close();
						ref.editarAntenaGUL = {
						info_telnet: []
						};
						ref.oldData = [];
						listarAntenas();

					}

				}).error(function(err){

				});
			}

			ref.cancelarEdicionGUL = function(){
				ref.editarAntenaGUL = {
					info_telnet: []
				};
				ref.oldData = [];
				listarAntenas();
				$('#editarAntenaGUL').bPopup().close();
			}

			ref.eliminar_antenaGUL = function(info){
				ref.eliminarAntenaGUL = info;
				$('#eliminarAntenaGUL').bPopup();
			}

			ref.eliminarGUL = function(){
				var data = "comentario="+ref.eliminarAntenaGUL.comentario;

				$http({
					url:"<?php echo base_url("antenas_gul/antena");?>"+"/"+ref.eliminarAntenaGUL.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.antenasGUL = [];
						listarAntenas();
						ref.eliminarAntenaGUL = {};
						$('#eliminarAntenaGUL').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEliminacionGUL = function(){
				ref.eliminarAntenaGUL = {};
				$('#eliminarAntenaGUL').bPopup().close();
			}

			ref.almacenandoAntenaGUL = function(info){
				ref.almacenar_AntenaGUL = info;
				ref.almacenar_AntenaGUL.tipo_almacen = 1;
				$("#almacenarAntenaGUL").bPopup();
			}

			ref.almacenarAntenaGUL = function(antena){
				var data = "tipo_antena_gul="+antena.tipo_antena_gul.id
						+"&numero_serie="+antena.numero_serie
						+"&estado="+antena.estado
						+"&proveniente_sitio="+antena.sitio
						+"&tipo_almacen="+antena.tipo_almacen
						+"&almacen="+((antena.tipo_almacen == 1)? antena.almacen : antena.a_sitio)
						+"&comentario="+antena.comentario;
			//	console.log(data);

				$http({
					url:"<?php echo base_url("antenas_gul/antena_gul/almacenar");?>/"+antena.id,
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#almacenarAntenaGUL").bPopup().close();
					ref.antenasGUL = [];
					listarAntenas();
					ref.almacenar_AntenaGUL = {};
				}).error(function(err){

				});
			}

			ref.cancelarAlmacenamientoGUL = function(){
				$("#almacenarAntenaGUL").bPopup().close();
				ref.almacenar_AntenaGUL = {};
			}

			ref.GULOpcionSitio = function(){
				ref.almacenar_AntenaGUL.tipo_almacen = 2;
			}

			ref.GULOpcionAlmacen = function(){
				ref.almacenar_AntenaGUL.tipo_almacen = 1;
			}

			listarAntenasGULAlmacenadas = function() {
				$http({
					url: "<?php echo base_url('objetos_bodegas/busqueda'); ?>",
					method: 'POST',
					data: 'objeto=antenas_gul',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function successCallback(resp) {
					if (resp.data.length) {
						ref.antenas_gul_almacenadas = resp.data;

						ref.antenas_gul_almacenadas[0].info.forEach(function(antena) {
							antena.check = false;
						});
					} else {
						ref.antenas_gul_almacenadas = [];
					}
				});
			}

			$('#importarAntenaGUL').on('hidden.bs.modal', function(e) {
				ref.importarAntenaGUL = {};
			});

			ref.importandoAntenaGUL = function() {
				$('#importarAntenaGUL').modal('show');
				$('#importarAntenaGUL').modal('handleUpdate');
			}

			ref.finImportarAntengaGUL = function(info, antena) {
				$http({
					url: "<?php echo base_url("antenas_gul/antena/sitio/{$sitio}"); ?>",
					method: 'POST',
					data: serializeObj(info),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function successCallback(resp) {
					$('#importarAntenaGUL').modal('hide');
				});
			}

			/*********************************************************************************************************/
			/*	Antenas iDEN																						 */
			/*********************************************************************************************************/

			listarMarcasIDEN = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_antena_iden/marcas");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.marcasIDEN = data;
					}
				}).error(function(err){

				});
			}

			ref.listarModelosIDEN = function(marca){
				$http({
					url:"<?php echo base_url("index.php/tipo_antena_iden/modelos");?>"+"/"+encodeURI(marca),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.modelosIDEN = data;
					}
				}).error(function(err){

				});
			}

			ref.agregarIDEN = function(){
				var data = "numero_serie="+ref.nuevaAntenaIDEN.numero_serie
							+"&marca="+ref.nuevaAntenaIDEN.marca
							+"&modelo="+ref.nuevaAntenaIDEN.modelo
							+"&sector="+ref.nuevaAntenaIDEN.sector
							+"&altura="+ref.nuevaAntenaIDEN.altura
							+"&azimuth="+ref.nuevaAntenaIDEN.azimuth
							+"&tilt_electrico="+ref.nuevaAntenaIDEN.tilt_electrico
							+"&tilt_mecanico="+ref.nuevaAntenaIDEN.tilt_mecanico
							+"&cantidad="+ref.nuevaAntenaIDEN.cantidad;

				$http({
					url:"<?php echo base_url("index.php/antenas_iden/antena/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.nuevaAntenaIDEN = {};
						listarAntenas();
						$('#nuevaAntenaIDEN').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarCreacionIDEN = function(){
				ref.nuevaAntenaIDEN = {};
				$('#nuevaAntenaIDEN').bPopup().close();
			}

			ref.editar_antenaIDEN = function(id){
				$http({
					url:"<?php echo base_url("index.php/antenas_iden/antena");?>"+"/"+id,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.editarAntenaIDEN = data[0];
						ref.editarAntenaIDEN["altura"] = Number(ref.editarAntenaIDEN["altura"]);
						ref.editarAntenaIDEN["azimuth"] = Number(ref.editarAntenaIDEN["azimuth"]);
						ref.editarAntenaIDEN["tilt_electrico"] = Number(ref.editarAntenaIDEN["tilt_electrico"]);
						ref.editarAntenaIDEN["tilt_mecanico"] = Number(ref.editarAntenaIDEN["tilt_mecanico"]);
						ref.editarAntenaIDEN["cantidad"] = Number(ref.editarAntenaIDEN["cantidad"]);
						ref.listarModelosIDEN(ref.editarAntenaIDEN.tipo_antena_iden.marca);
						$('#editarAntenaIDEN').bPopup();
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.actualizarIDEN = function(id){
				var campos = Object.keys(ref.editarAntenaIDEN),
					data = "";

				campos.forEach(function(campo,ind,objeto){
					if(campo == "tipo_antena_iden")
					{
						data += "marca="+ref.editarAntenaIDEN.tipo_antena_iden.marca+"&";
						data += "modelo="+ref.editarAntenaIDEN.tipo_antena_iden.modelo+"&";
					}
					else
					{
						if(campo == "created_at")
						{

						}
						else
						{
							data += campo+"="+ref.editarAntenaIDEN[campo];

							if( ind < (objeto.length-2) )
							{
								data += "&";
							}
						}
					}
				});

				$http({
					url:"<?php echo base_url("index.php/antenas_iden/antena");?>"+"/"+id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.length)
					{

					}
					else
					{
						$('#editarAntenaIDEN').bPopup().close();
						ref.editarAntenaIDEN = {};
						listarAntenas();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEdicionIDEN = function(){
				ref.editarAntenaIDEN = {};
				$('#editarAntenaIDEN').bPopup().close();
			}

			ref.eliminar_antenaIDEN = function(id){
				$http({
					url:"<?php echo base_url("index.php/antenas_iden/antena");?>"+"/"+id,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.eliminarAntenaIDEN = data[0];
						$('#eliminarAntenaIDEN').bPopup();
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.eliminarIDEN = function(){
				var data = "comentario="+ref.eliminarAntenaIDEN.comentario;

				$http({
					url:"<?php echo base_url("index.php/antenas_iden/antena");?>"+"/"+ref.eliminarAntenaIDEN.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.antenasIDEN = [];
						listarAntenas();
						ref.eliminarAntenaIDEN = {};
						$('#eliminarAntenaIDEN').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEliminacionIDEN = function(){
				ref.eliminarAntenaIDEN = {};
				$('#eliminarAntenaIDEN').bPopup().close();
			}

			ref.almacenandoAntenaIDEN = function(info){
				ref.almacenar_AntenaIDEN = info;
				ref.almacenar_AntenaIDEN.tipo_almacen = 1;
				$("#almacenarAntenaIDEN").bPopup();
			}

			ref.almacenarAntenaIDEN = function(antena){
				var data = "tipo_antena_iden="+antena.tipo_antena_iden.id
						+"&cantidad="+antena.cantidad
						+"&numero_serie="+antena.numero_serie
						+"&proveniente_sitio="+antena.sitio
						+"&tipo_almacen="+antena.tipo_almacen
						+"&almacen="+((antena.tipo_almacen == 1)? antena.almacen : antena.a_sitio)
						+"&comentario="+antena.comentario;
				//console.log(data);

				$http({
					url:"<?php echo base_url("antenas_iden/antena_iden/almacenar");?>/"+antena.id,
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#almacenarAntenaIDEN").bPopup().close();
					ref.antenasIDEN = [];
					listarAntenas();
					ref.almacenar_AntenaIDEN = {};
				}).error(function(err){

				});
			}

			ref.cancelarAlmacenamientoIDEN = function(){
				$("#almacenarAntenaIDEN").bPopup().close();
				ref.almacenar_AntenaIDEN = {};
			}

			ref.IDENOpcionSitio = function(){
				ref.almacenar_AntenaIDEN.tipo_almacen = 2;
			}

			ref.IDENOpcionAlmacen = function(){
				ref.almacenar_AntenaIDEN.tipo_almacen = 1;
			}
			
			/*********************************************************************************************************/
			/*	Antenas In Building																							 */
			/*********************************************************************************************************/

			listarModelosInBuilding = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_antena_in_building/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.modelos_in_building = data;
					}
				}).error(function(err){

				});
			}
			
			ref.nueva_antena_in_building = function(){
				$("#nuevaAntenaInBuilding").bPopup();
			}
			
			ref.cancelarCreacionInBuilding = function(){
				$("#nuevaAntenaInBuilding").bPopup().close();
				ref.nuevaAntenaInBuilding = {};
			}

			ref.agregarInBuilding = function(antena){
				//console.log(antena);
				var data = "",
					campos = Object.keys(antena),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(antena[campo] != null)
						{
							if(typeof antena[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+antena[campo];
							}
							else
							{
								if(campo == "fecha_servicio")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+antena[campo].toISOString();
								}
								else
								{
									if(antena[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(antena[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+antena[campo].id;
									}
										
								}
							}

							contador++;
						}
					}
				});
				//console.log(data);
				$http({
					url:"<?php echo base_url("index.php/antenas_in_building/antena/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevaAntenaInBuilding").bPopup().close();
					listarAntenas();
				}).error(function(err){

				});
			}

			ref.editar_antenaInBuilding = function(id){
				$http({
					url:"<?php echo base_url("index.php/antenas_in_building/antena");?>"+"/"+id,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.editarAntenaInBuilding = data[0];
						ref.editarAntenaInBuilding["cantidad"] = Number(ref.editarAntenaInBuilding["cantidad"]);
						listarModelosInBuilding(ref.editarAntenaInBuilding.tipo_antena_in_building.id);
						$('#editarAntenaInBuilding').bPopup();
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.actualizarInBuilding = function(id){
				var campos = Object.keys(ref.editarAntenaInBuilding),
					data = "";

				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(campo == "tipo_antena_in_building")
						{
							data += "tipo_antena_in_building="+ref.editarAntenaInBuilding.tipo_antena_in_building.id+"&";
						}
						else
						{
							if(campo == "created_at")
							{

							}
							else
							{
								data += campo+"="+ref.editarAntenaInBuilding[campo];

								if( ind < (objeto.length-2) )
								{
									data += "&";
								}
							}
						}
					}
				});
				//console.log(data);
				$http({
					url:"<?php echo base_url("index.php/antenas_in_building/antena");?>"+"/"+id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.length)
					{

					}
					else
					{
						$('#editarAntenaInBuilding').bPopup().close();
						ref.editarAntenaInBuilding = {};
						listarAntenas();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEdicionInBuilding = function(){
				ref.editarAntenaInBuilding = {};
				$('#editarAntenaInBuilding').bPopup().close();
			}

			ref.eliminandoAntenaInBuilding = function(info){
				ref.eliminar_AntenaInBuilding = info;
				$("#eliminarAntenaInBuilding").bPopup();
			}
			
			ref.cancelarEliminacionInBuilding = function(){
				$("#eliminarAntenaInBuilding").bPopup().close();
				ref.eliminar_AntenaInBuilding = {};
			}
			
			ref.eliminarAntenaInBuilding = function(){
				var data = "comentario="+ref.eliminar_AntenaInBuilding.comentario;

				$http({
					url:"<?php echo base_url("index.php/antenas_in_building/antena");?>"+"/"+ref.eliminar_AntenaInBuilding.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.antenas_in_building = [];
						listarAntenas();
						ref.eliminar_AntenaInBuilding = {};
						$('#eliminarAntenaInBuilding').bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			infoSitio = function(){
				$http({
					url:"<?php echo base_url("index.php/status_site/editar/");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.info_sitio = data[0];
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			/*********************************************************************************************************/
			/*	Sesión																								 */
			/*********************************************************************************************************/

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}

			torre = function(){
				$http({
					url:"<?php echo base_url("index.php/status_site/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						data[0].latitud = Number(data[0].latitud);
						data[0].longitud = Number(data[0].longitud);
						data[0].altura_edificacion = Number(data[0].altura_edificacion);
						data[0].altura_torre = Number(data[0].altura_torre);
						data[0].factor_uso_torre = (data[0].factor_uso_torre? Number(data[0].factor_uso_torre) : null);
						data[0].anio_construccion = Number(data[0].anio_construccion);
						data[0].acceso_libre_24h = Boolean(Number(data[0].acceso_libre_24h));
						data[0].in_building = Boolean(Number(data[0].in_building));
						/*data[0].coubicado = Boolean(Number(data[0].coubicado));*/
						data[0].agregador = Boolean(Number(data[0].agregador));
						data[0].dorsal = Boolean(Number(data[0].dorsal));
						try{ data[0].fecha_estado_on_air = new Date(data[0].fecha_estado_on_air + " 00:00:00"); }catch(err){ data[0].fecha_estado_on_air = null; }

						data[0].contratos.forEach(function(contrato,ind,objeto){
							objeto[ind].area = Number(objeto[ind].area);
							try{ objeto[ind].fecha_inicio = new Date(objeto[ind].fecha_inicio + " 00:00:00"); }catch(err){ objeto[ind].fecha_inicio = null; }
							try{ objeto[ind].fecha_fin = new Date(objeto[ind].fecha_fin + " 00:00:00"); }catch(err){ objeto[ind].fecha_fin = null; }

							contrato.contactos_contrato.forEach(function(contacto,indx,objx){
								objx[indx].telefono = JSON.parse(contacto.telefono);
								objx[indx].correo = JSON.parse(contacto.correo);
							});
						});

						try{ data[0].created_at = Date.parse(data[0].created_at); } catch(err){ data[0].created_at = null; }

						data[0].logs_status_site.forEach(function(log,ind,objeto){
							try{ objeto[ind].created_at = Date.parse(objeto[ind].created_at); } catch(err){ objeto[ind].created_at = null; }
						});

						ref.info_torre = data[0];
						/*console.log(data);*/
					}
					else
					{
						
					}
				}).error(function(err){

				});
			}

            VerificarFactorUso = function(){
				$(".alert.alert-danger.text-center.alerta_de_torre").css('display','none');
				var  e = $("#factor_uso_torre_unico").val();   	
				deshabilita_segun_factor_uso(e);				
			}            
           
			deshabilita_segun_factor_uso = function(e){
				if(e > 1){
             //alert(e + "bloqueado");
            $(".alert.alert-danger.text-center.alerta_de_torre").css('display','block'); 
            $('.verifica_factor_uso').find("input,select,button").attr("disabled", "disabled");
            }else{
        	// alert(e + "Des bloqueado");
        	$(".alert.alert-danger.text-center.alerta_de_torre").css('display','none'); 
        	$('.verifica_factor_uso').find("input,select,button").removeAttr("disabled");
              }
             }

//nuevas funciones

			GrupoFunciones = function(){
            LimpiarForm('FormNuevaAntenaGUL');
            DeshabilitarBotones('FormNuevaAntenaGUL');
			}
            LimpiarForm = function(idform){
              $('#'+idform)[0].reset();
            }
            DeshabilitarBotones = function(idform){
             $('#'+idform).find("button").attr("disabled", "disabled");
            }  

            HabilitarBotones = function(idform){
              $('#'+idform).find("button").removeAttr("disabled"); 
            }


			VerificarCamposLlenos = function(){
			    $('.gul_propertie').each(function() {
			        if ($(this).prop('value') === ''){          
			          DeshabilitarBotones('FormNuevaAntenaGUL');
			        } else {
			             HabilitarBotones('FormNuevaAntenaGUL');
			         
			        }
			    });
        	}

			listarAntenas();
			listarMarcasMicroondas();
			listarTipoAntenasGUL();
			listarTipoAntenasGULPendiente();			
			listarMarcasIDEN();
			listarModelosInBuilding();
			infoSitio();
			listarSitios();
			listarAlmacenes();
			listarAntenasGULAlmacenadas();
            torre();


		}]);
</script>
</body>
</html>