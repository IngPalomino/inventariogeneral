<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-md-3 col-xs-6">
				<a  class="btn boton" role="button" href="<?php echo base_url("ingreso/sitios");?>">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-university fa-5x"></span></td></td></tr>
							<tr><td>Agregar o quitar sitios</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url("ingreso/busqueda");?>" class="btn boton" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-pencil-square-o fa-5x"></span></td></td></tr>
							<tr><td>Modificación de sitio</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url("ingreso_masivo");?>" class="btn boton" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-cloud-upload fa-5x"></span></td></td></tr>
							<tr><td>Ingreso masivo</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url("ingreso/bodegas");?>" class="btn boton" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-archive fa-5x"></span></td></td></tr>
							<tr><td>Bodegas virtuales</td></tr>
						</tbody>
					</table>
				</a>
			</div>
		</div>
	</div>
</section>