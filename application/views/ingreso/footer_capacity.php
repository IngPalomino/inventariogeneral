<script type="text/javascript">
(function() {
	'use strict';

	angular
		.module('app', [])
		.controller('controlador', controlador);

	controlador.$inject = ['$http', 'common'];

	function controlador($http, common) {
		var ref = this;

		ref.antenas_gul = [];
		ref.antenas_gul_pendiente = [];
		ref.antenas_iden = [];
		ref.antenas_mw = [];
		ref.antenas_mw_pendiente = [];
		ref.infoSitio = {};
		ref.resetZoom = resetZoom;
		ref.updateZoom = updateZoom;
		ref.zoomLevel = 1;
		var sitio = document.getElementById('sitio').value;
		var lineStyle = '#00F';
		var backgroundLineStyle = 'rgba(255, 255, 0, 0.4)';
		var backgroundLineWidth = 7;

		listarAntenas();
		getInfoSitio();

		function updateZoom(zoom) {
			ref.zoomLevel += zoom;

			$('#torreImg').css({
				zoom: ref.zoomLevel,
				'-moz-transformx': 'scale(' + ref.zoomLevel + ')'
			});
		}

		function resetZoom() {
			ref.zoomLevel = 1;
			updateZoom(0);
		}

		function listarAntenas() {
			$http({
				url: common.base_url('antenas_mw/listar_sitio/' + sitio),
				method: 'GET'
			}).then(function successCallback(resp) {
				if (resp.data.length) {
					ref.antenas_mw = resp.data;

					ref.antenas_mw_sectores = [
						{sector: 1, antenas: []},
						{sector: 2, antenas: []},
						{sector: 3, antenas: []}
					];

					ref.antenas_mw.forEach(function(antena) {
						antena.bottom = Math.round(((Number(antena.altura) - <?php echo $infoSitio[0]['altura_edificacion']; ?>) / <?php echo $infoSitio[0]['altura_torre']; ?>) * 100);
						 antena.top = (100 - Number(antena.bottom) - 2);
						antena.azimuth = Number(antena.azimuth);
						antena.left = Math.round((antena.azimuth/360) * 100);
						antena.content = '<ul style="padding-left:12px;width:200px;">'
											+ '<li>Marca: ' + antena.tipo_antena_mw.marca + '</li>'
											+ '<li>Modelo: ' + antena.tipo_antena_mw.modelo + '</li>'
											+ '<li>Altura: ' + antena.altura + ' m</li>'
											+ '<li>Azimuth: ' + antena.azimuth + '°</li>'
											+ '<li style="display:none;">Número de Serie: ' + antena.numero_serie + '</li>'
											+ '<li style="word-wrap: break-word;">Enlace: ' + antena.enlace.idu_1.sitio.nombre_completo +'<==>'+antena.enlace.idu_2.sitio.nombre_completo+ '</li>'
											+ '<li>Diámetro de antena: ' + antena.diametro + ' m </li>'
										+ '</ul>';

						if (antena.azimuth <= 120) {
							ref.antenas_mw_sectores[0].antenas.push(antena);
						} else if (antena.azimuth > 120 && antena.azimuth <= 240) {
							ref.antenas_mw_sectores[1].antenas.push(antena);
						} else if (antena.azimuth > 240) {
							ref.antenas_mw_sectores[2].antenas.push(antena);
						}
					});
				} else {
					ref.antenas_mw = [];
				}
			});
			

// microondas pendiente
	$http({
				url: common.base_url('antenas_mw/listar_sitio_pendiente/' + sitio),
				method: 'GET'
			}).then(function successCallback(resp) {
				if (resp.data.length) {
					ref.antenas_mw_pendiente = resp.data;

					ref.antenas_mw_sectores_pendiente = [
						{sector: 1, antenas: []},
						{sector: 2, antenas: []},
						{sector: 3, antenas: []}
					];

					ref.antenas_mw_pendiente.forEach(function(antena) {
						antena.bottom = Math.round(((Number(antena.altura) - <?php echo $infoSitio[0]['altura_edificacion']; ?>) / <?php echo $infoSitio[0]['altura_torre']; ?>) * 100);

                        antena.top = (100 - Number(antena.bottom) - 2);

						antena.azimuth = Number(antena.azimuth);
						antena.left = Math.round((antena.azimuth/360) * 100);
						antena.content = '<ul style="padding-left:12px;width:200px;">'
											+ '<li>Marca: ' + antena.tipo_antena_mw.marca + '</li>'
											+ '<li>Modelo: ' + antena.tipo_antena_mw.modelo + '</li>'
											+ '<li>Altura: ' + antena.altura + ' m</li>'
											+ '<li>Azimuth: ' + antena.azimuth + '°</li>'
											+ '<li style="display:none;">Número de Serie: ' + antena.numero_serie + '</li>'
											+ '<li style="word-wrap: break-word;">Enlace: ' + antena.enlace.idu_1.sitio.nombre_completo +'<==>'+antena.enlace.idu_2.sitio.nombre_completo+ '</li>'
											+ '<li>Diámetro de antena: ' + antena.diametro + ' m </li>'
										+ '</ul>';
                          if(antena.estado_aprobacion >1){
                            antena.foto ='mw_red.png';
                          }else{
                          	antena.foto ='mw_orange.png';
                          }



						if (antena.azimuth <= 120) {
							ref.antenas_mw_sectores_pendiente[0].antenas.push(antena);
						} else if (antena.azimuth > 120 && antena.azimuth <= 240) {
							ref.antenas_mw_sectores_pendiente[1].antenas.push(antena);
						} else if (antena.azimuth > 240) {
							ref.antenas_mw_sectores_pendiente[2].antenas.push(antena);
						}
					});
				} else {
					ref.antenas_mw_pendiente = [];
				}
			});
// microondas pendiente



//-----antenas gul
			$http({
				url: common.base_url('antenas_gul/listar_sitio/' + sitio),
				method: 'GET'
			}).then(function successCallback(resp) {

				//console.log(resp.data);
				if (resp.data.length) {
					ref.antenas_gul = resp.data;

					ref.antenas_gul_sectores = [
						{sector: 1, antenas: []},
						{sector: 2, antenas: []},
						{sector: 3, antenas: []}
					];

					ref.antenas_gul.forEach(function(antena) {


                    //original
						// antena.bottom = Math.round(((Number(antena.altura) - <?php //echo $infoSitio[0]['altura_edificacion']; ?>) / <?php //echo $infoSitio[0]['altura_torre']; ?>) * 100);
                    //new 
					antena.bottom = Math.round(((Number(antena.altura) - <?php echo $infoSitio[0]['altura_edificacion']; ?>) / <?php echo $infoSitio[0]['altura_torre']; ?>) * 100);


					antena.top = (100 - Number(antena.bottom) - 4);
						
				     var new_azimuth;

				            if(antena.tipo_antena_gul.marca == 'TELNET'){

				                 antena.sector = antena.info_telnet[0].sector+'/'+antena.info_telnet[1].sector+'/'+antena.info_telnet[2].sector; 

				                 new_azimuth = antena.info_telnet[0].azimuth+'°/'+antena.info_telnet[1].azimuth+'°/'+antena.info_telnet[2].azimuth+'°';

				                 antena.newtilt_electrico = antena.info_telnet[0].tilt_electrico+'/'+antena.info_telnet[1].tilt_electrico+'/'+antena.info_telnet[2].tilt_electrico+'';
 
				                 antena.tilt_mecanico = antena.info_telnet[0].tilt_mecanico+'/'+antena.info_telnet[1].tilt_mecanico+'/'+antena.info_telnet[2].tilt_mecanico+'';


				             }else{
				                       new_azimuth = Number(antena.azimuth)+'°';
				                      antena.newtilt_electrico = Number(antena.tilt_electrico)+'';
				                      antena.tilt_mecanico = Number(antena.tilt_mecanico)+'';
				            }


						antena.left = Math.round((antena.azimuth/360) * 100);
						antena.content = '<ul style="padding-left:12px;width:200px;">'
											+ '<li>Marca: ' + antena.tipo_antena_gul.marca + '</li>'
											+ '<li>Modelo: ' + antena.tipo_antena_gul.modelo + '</li>'
											+ '<li>Sector: ' + antena.sector + '</li>'
											+ '<li>Altura: ' + antena.altura + ' m</li>'
											+ '<li>Azimuth: ' + new_azimuth + '</li>'
											+ '<li>Tilt Eléctrico: ' + antena.newtilt_electrico + '</li>'
											+ '<li>Tilt Mecánico: ' + antena.tilt_mecanico + '</li>'
											+ '<li style="display:none;">Número de Serie: ' + antena.numero_serie + '</li>'
										+ '</ul>';

                         


						if (antena.azimuth <= 120) {
							ref.antenas_gul_sectores[0].antenas.push(antena);
						} else if (antena.azimuth > 120 && antena.azimuth <= 240) {
							ref.antenas_gul_sectores[1].antenas.push(antena);
						} else if (antena.azimuth > 240) {
							ref.antenas_gul_sectores[2].antenas.push(antena);
						}
					});
				} else {
					ref.antenas_gul = [];
				}
			});
//----antenas gul

//-----antenas gul pendiente
			$http({
				url: common.base_url('antenas_gul_pendiente/listar_sitio/' + sitio),
				method: 'GET'
			}).then(function successCallback(resp) {//console.log(resp.data);
				if (resp.data.length) {
					ref.antenas_gul_pendiente = resp.data;
                   // console.log(ref.antenas_gul_pendiente);


					ref.antenas_gul_pendiente_sectores = [
						{sector: 1, antenas: []},
						{sector: 2, antenas: []},
						{sector: 3, antenas: []}
					];
					ref.antenas_gul_pendiente.forEach(function(antena) {
						antena.bottom = Math.round(((Number(antena.altura) - <?php echo $infoSitio[0]['altura_edificacion']; ?>) / <?php echo $infoSitio[0]['altura_torre']; ?>) * 100);
                   antena.top = (100 - Number(antena.bottom) - 4);
					//antena.azimuth += Number(antena.azimuth);
						//console.log(antena);

     var new_azimuth;
            if(antena.tipo_antena_gul.marca == 'TELNET'){

                 antena.sector = antena.info_telnet[0].sector+'/'+antena.info_telnet[1].sector+'/'+antena.info_telnet[2].sector; 

                  new_azimuth = antena.info_telnet[0].azimuth+'°/'+antena.info_telnet[1].azimuth+'°/'+antena.info_telnet[2].azimuth+'°';

          
				                 antena.newtilt_electrico = antena.info_telnet[0].tilt_electrico+'/'+antena.info_telnet[1].tilt_electrico+'/'+antena.info_telnet[2].tilt_electrico+'';
 
				                 antena.tilt_mecanico = antena.info_telnet[0].tilt_mecanico+'/'+antena.info_telnet[1].tilt_mecanico+'/'+antena.info_telnet[2].tilt_mecanico+'';


				             }else{
				                       new_azimuth = Number(antena.azimuth)+'°';
				                      antena.newtilt_electrico = Number(antena.tilt_electrico)+'';
				                      antena.tilt_mecanico = Number(antena.tilt_mecanico)+'';
				            }

                       //antena.sector = antena[0].sector+"/"+antena[1].sector+"/"+antena[2].sector;
						antena.left = Math.round((antena.azimuth/360) * 100);
						antena.content = '<ul style="padding-left:12px;width:200px;">'
											+ '<li>Marca: ' + antena.tipo_antena_gul.marca + '</li>'
											+ '<li>Modelo: ' + antena.tipo_antena_gul.modelo + '</li>'
											+ '<li>Sector: ' + antena.sector + '</li>'
											+ '<li>Altura: ' + antena.altura + ' m</li>'
											+ '<li>Azimuth:' + new_azimuth + '</li>'
											+ '<li>Tilt Eléctrico: ' + antena.newtilt_electrico + '</li>'
											+ '<li>Tilt Mecánico: ' + antena.tilt_mecanico + '</li>'
											+ '<li style="display:none;">Número de Serie: ' + antena.numero_serie + '</li>'
										+ '</ul>';

						 if(antena.estado >0){
                            antena.foto ='gul_red.png';
                          }else{
                          	antena.foto ='gul_orange.png';
                          }

						if (antena.azimuth <= 120) {
							ref.antenas_gul_pendiente_sectores[0].antenas.push(antena);
						} else if (antena.azimuth > 120 && antena.azimuth <= 240) {
							ref.antenas_gul_pendiente_sectores[1].antenas.push(antena);
						} else if (antena.azimuth > 240) {
							ref.antenas_gul_pendiente_sectores[2].antenas.push(antena);
						}


					});
				} else {
					ref.antenas_gul_pendiente = [];
				}
			});
//----antenas gul pendiente



			$http({
				url: common.base_url('antenas_iden/listar_sitio/' + sitio),
				method: 'GET'
			}).then(function successCallback(resp) {//console.log(resp.data);
				if (resp.data.length) {
					ref.antenas_iden = resp.data;

					ref.antenas_iden_sectores = [
						{sector: 1, antenas: []},
						{sector: 2, antenas: []},
						{sector: 3, antenas: []}
					];

					ref.antenas_iden.forEach(function(antena) {

						antena.bottom = Math.round(((Number(antena.altura) - <?php echo $infoSitio[0]['altura_edificacion']; ?>) / <?php echo $infoSitio[0]['altura_torre']; ?>) * 100);


                       antena.top = (100 - Number(antena.bottom) - 4);


						antena.content = '<ul style="padding-left:12px;width:200px;">'
											+ '<li>Marca: ' + antena.tipo_antena_iden.marca + '</li>'
											+ '<li>Modelo: ' + antena.tipo_antena_iden.modelo + '</li>'
											+ '<li>Sector: ' + antena.sector + '</li>'
											+ '<li>Altura: ' + antena.altura + ' m</li>'
											+ '<li>Azimuth: ' + antena.azimuth + '°</li>'
											+ '<li>Cantidad: ' + antena.cantidad + '</li>'
										+ '</ul>';

						antena.sectores = [];

						for (var i = 0; i < antena.cantidad; i++) {
							antena.sectores.push(i);
						}

						antena.azimuth = Number(antena.azimuth);
						antena.left = Math.round((antena.azimuth/360) * 100);

						if (antena.azimuth <= 120) {
							ref.antenas_iden_sectores[0].antenas.push(antena);
						} else if (antena.azimuth > 120 && antena.azimuth <= 240) {
							ref.antenas_iden_sectores[1].antenas.push(antena);
						} else if (antena.azimuth > 240) {
							ref.antenas_iden_sectores[2].antenas.push(antena);
						}
					});//console.log(ref.antenas_iden_sectores);
				} else {
					ref.antenas_iden = [];
				}
			});
		}

		function getInfoSitio() {
			$http({
				url: common.base_url('status_site/editar/' + sitio),
				method: 'GET'
			}).then(function successCallback(resp) {
				if (resp.data.length) {
					ref.infoSitio = resp.data[0];
				} else {
					ref.infoSitio = {};
				}
			});
		}
	}
})();
</script>
<script type="text/javascript">
	(function() {
		'use strict';

		angular
			.module('app')
			.service('common', common);

		function common() {
			this.base_url = base_url;

			function base_url(segment = '') {
				return '<?php echo base_url(); ?>' + segment;
			}
		}
	})();
</script>
<script type="text/javascript">
	(function() {
		'use strict';

		angular
			.module('app')
			.directive('bsPopover', bsPopover);

		function bsPopover(){
			return function(scope, element, attrs){
				element.find('[rel=popover]').popover({
					placement: 'auto',
					html: 'true',
					trigger: 'hover'
				});
			}
		}
	})();
</script>


<!-- torre -->
<script>
var range = document.getElementById('range');

range.style.height = '500px';
range.style.float = 'left';
range.style.margin = '0 auto 30px';
noUiSlider.create(range, {
	start: [<?php echo $altura_edificacionx; ?>, <?php echo $altura_desde_edificio; ?>], // 4 handles, starting at...
	margin: 300, // Handles must be at least 300 apart
	limit: 600, // ... but no more than 600
	connect: true, // Display a colored bar between the handles
	direction: 'rtl', // Put '0' at the bottom of the slider
	orientation: 'vertical', // Orient the slider vertically
	behaviour: 'tap-drag', // Move handle on tap, bar is draggable
	step: 1,
	tooltips: true,
	format: wNumb({
		decimals: 0
	}),
	range: {
		'min': <?php echo $altura_edificacionx; ?>,
		'max': <?php echo $altura_desde_edificio; ?>
	},
	pips: { // Show a scale with the slider
		mode: 'steps',
		stepped: true,
		density: 4
	}
});
</script>
<!-- torre -->

<!-- torre2 -->
<script>
var range = document.getElementById('range2');

range.style.height = '500px';
range.style.float = 'left';
range.style.margin = '0 auto 30px 71px';
noUiSlider.create(range, {
	start: [<?php echo $altura_edificacionx; ?>, <?php echo $altura_desde_edificio; ?>], // 4 handles, starting at...
	margin: 300, // Handles must be at least 300 apart
	limit: 600, // ... but no more than 600
	connect: true, // Display a colored bar between the handles
	direction: 'rtl', // Put '0' at the bottom of the slider
	orientation: 'vertical', // Orient the slider vertically
	behaviour: 'tap-drag', // Move handle on tap, bar is draggable
	step: 1,
	tooltips: true,
	format: wNumb({
		decimals: 0
	}),
	range: {
		'min': <?php echo $altura_edificacionx; ?>,
		'max': <?php echo $altura_desde_edificio; ?>
	},
	pips: { // Show a scale with the slider
		mode: 'steps',
		stepped: true,
		density: 4
	}
});
</script>
<!-- torre2 -->

</body>
</html>