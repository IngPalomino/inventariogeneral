<script type="text/javascript">
	angular
		.module('app',[])

        	
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";
			ref.error.crear = "";
			ref.error.actualizar = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargando = false;
			
			ref.idus = [];
			ref.nuevoIDU = {};
			ref.eliminar_IDU = {};
			ref.tipo_idu = [];
			ref.editar_IDU = {};
			
			listarIDUS = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("idus/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.idus = data;
						
						data.forEach(function(dato,ind,objeto){
							try{ objeto[ind].created_at = Date.parse(objeto[ind].created_at); } catch(err){ objeto[ind].created_at = null; }
							
							objeto[ind].logs_idus.forEach(function(modificacion,indx,objetox){
								try{ objetox[indx].created_at = Date.parse(objetox[indx].created_at); } catch(err){ objetox[indx].created_at = null; }
							});
							
							var codigo = objeto[ind].ne_id.split("-");
							objeto[ind].ne_id_l = Number(codigo[0]);
							objeto[ind].ne_id_r = Number(codigo[1]);
						});
						
						ref.idus.forEach(function(idu,indx,objetox){
							objetox[indx].disabled = function(){
								var disabled = false;
								
								if(this.ne_id_l == "" || this.ne_id_r == "" || this.tipo_idu.id == "" || this.numero_serie == "")
								{
									disabled = true;
								}
								
								return disabled;
							}
						});
						ref.cargando = false;
					}
					else
					{
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}
			
			listarTipoIDU = function(){
				$http({
					url:"<?php echo base_url("tipo_idu/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_idu = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			ref.nuevo_idu = function(){
			  	
              $("#idu-coincidente").css("display", "none");
			  $('#idu-coincidente').text("");
			  $("#nuevoIDU").bPopup();
			}			
			ref.cancelarCreacionIDU = function(){
				$("#nuevoIDU").bPopup().close();
				ref.nuevoIDU = {};
				ref.error.error = false;
				ref.error.crear = "";
				var alertCrear = document.getElementById("alert-crear");
				alertCrear.innerHTML = "";
			}



			
			ref.agregarIDU = function(idu) {
			        var dato = "ne_id=" + ref.nuevoIDU.ne_id_l + "-" + ref.nuevoIDU.ne_id_r;			            
			        $http({
			            url: "<?php echo base_url("idus/get_sitio_con_neid");?>/",
			            method: "POST",
			            data: dato,
			            headers: {
			                'Content-Type': 'application/x-www-form-urlencoded'
			            }
			        }).success(function(resp) {
			            if (!(resp.error)) {
			                if (resp.length > 0) {
			                    // si hay idu repetido muestra los idus 
			                    ref.resultado = resp;
			                    //console.log(ref.resultado[0].id + ref.resultado[0].link_name);
			                    $("#idu-coincidente").css("display", "block");
			                    $('#idu-coincidente').text("EL NE_ID : " + ref.resultado[0].ne_id + " se repite en el site : " + ref.resultado[0].nombre_completo);
			                } else {
			                    //sino hay idu repetido se agrega 
                                $("#idu-coincidente").css("display", "none");
			                    $('#idu-coincidente').text("");

			                    ref.error.error = false;
			                    ref.error.crear = "";
			                    var alertCrear = document.getElementById("alert-crear");
			                    alertCrear.innerHTML = "";

			                    var data = "ne_id=" + ref.nuevoIDU.ne_id_l + "-" + ref.nuevoIDU.ne_id_r +"&tipo_idu=" + ref.nuevoIDU.tipo_idu.id + "&numero_serie=" + ref.nuevoIDU.numero_serie +
			                        "&link_name=" + ref.nuevoIDU.link_name;

			                    /*console.log(data);*/
			                  
			                    $http({
			                        url: "<?php echo base_url("idus/idu/sitio");?>/"+"<?php echo $sitio;?>",
			                        method: "POST",
			                        data: data,
			                        headers: {
			                            'Content-Type': 'application/x-www-form-urlencoded'
			                        }
			                    }).success(function(resp) {
			                    	
			                        if (!(resp.error)) {
			                            $("#nuevoIDU").bPopup().close();
			                            listarIDUS();
			                            ref.nuevoIDU = {};
			                        } else {
			                            ref.error.error = true;
			                            ref.error.crear = resp.error;
			                            alertCrear.innerHTML = ref.error.crear;
			                        }
			                    }).error(function(err) {

			                    });

			                }



			            }
			        }).error(function(err) {

			        });
				
			}
			
			ref.eliminandoIDU = function(info){
				ref.eliminar_IDU = info;
				$("#eliminarIDU").bPopup();
			}
			
			ref.cancelarEliminacion = function(){
				$("#eliminarIDU").bPopup().close();
				ref.eliminar_IDU = {};
			}
			
			ref.eliminarIDU = function(){
				var data = "comentario="+ref.eliminar_IDU.comentario;

				$http({
					url:"<?php echo base_url("idus/idu");?>"+"/"+ref.eliminar_IDU.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.idus = [];
						listarIDUS();
						ref.eliminar_IDU = {};
						$('#eliminarIDU').bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.editarIDU = function(idu){
				idu.editar = true;
				idu.ne_id = idu.ne_id_l+"-"+idu.ne_id_r;
				ref.editar_IDU = idu;
			}
			
			ref.grabarIDU = function(idu){
				ref.error.error = false;
				ref.error.actualizar = "";
				var alertActualizar = document.getElementById("alert-actualizar"+idu.id);
				alertActualizar.innerHTML = "";
				
				var ne_id = idu.ne_id_l+"-"+idu.ne_id_r;
				
				if(ref.editar_IDU.ne_id != ne_id)
				{	
					var data = "ne_id="+idu.ne_id_l+"-"+idu.ne_id_r
							+"&tipo_idu="+idu.tipo_idu.id
							+"&numero_serie="+idu.numero_serie
							+"&link_name="+idu.link_name;
				}
				else
				{
					var data = "tipo_idu="+idu.tipo_idu.id
						+"&numero_serie="+idu.numero_serie
						+"&link_name="+idu.link_name;
				}
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("idus/idu");?>/"+idu.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(!resp.error)
					{
						idu.editar = false;
						listarIDUS();
						ref.editar_IDU = {};
					}
					else
					{
						ref.error.error = true;
						ref.error.actualizar = resp.error;
						alertActualizar.innerHTML = ref.error.actualizar;
					}
				}).error(function(err){

				});
			}

			ref.cancelarIDU = function(idu){
				idu.editar = false;
				listarIDUS();
				ref.error.error = false;
				ref.error.actualizar = "";
				/*var alertActualizar = document.getElementById("alert-actualizar");
				alertActualizar.innerHTML = "";*/
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}
			
			listarIDUS();
			listarTipoIDU();
		}])
		.directive('bsPopover',function(){
			return function(scope,element,attrs){
				element.find("span[rel=popover]").popover();
			}
		});
</script>
</body>
</html>