<section class="container-fluid">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="page-header">
				<h2>Bodegas Virtuales</h2>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<ul class="nav nav-pills nav-justified">
						<li role="presentation" ng-class="{active:ctrl.navPanel.index == 0}">
							<a href ng-click="ctrl.navPanel.cambiarIndex(0)">Objetos</a>
						</li>
						<li role="presentation" ng-class="{active:ctrl.navPanel.index == 1}">
							<a href ng-click="ctrl.navPanel.cambiarIndex(1)">Sitios</a>
						</li>
						<li role="presentation" ng-class="{active:ctrl.navPanel.index == 2}">
							<a href ng-click="ctrl.navPanel.cambiarIndex(2)">Almacenes</a>
						</li>
					</ul>
				</div>
				<div class="panel-body ng-cloak" ng-if="ctrl.navPanel.index == 0">
					<ul class="nav nav-pills nav-justified">
						<li role="presentation" ng-class="{active:ctrl.navObjetos.index == 0}">
							<a href ng-click="ctrl.navObjetos.cambiarIndex(0); ctrl.buscarGruposElectrogenos()">Grupos Electrógenos</a>
						</li>
						<li role="presentation" ng-class="{active:ctrl.navObjetos.index == 1}">
							<a href ng-click="ctrl.navObjetos.cambiarIndex(1); ctrl.buscarObjeto()">Aires Acondicionados</a>
						</li>
					</ul><br>
					<div ng-if="ctrl.navObjetos.index == 0">
						<table class="table table-condensed table-hover" ng-if="!ctrl.cargando">
							<thead>
								<tr>
									<th style="width:200px">Acciones</th>
									<th colspan="2">Grupo Electrógeno</th>
									<th>Proveniente</th>
									<th>Almacenado en</th>
									<th>Historial</th>
								</tr>
							</thead>
							<tbody ng-if="ctrl.grupos_electrogenos.length">
								<tr ng-repeat-start="grupo in ctrl.grupos_electrogenos">
									<td>
										<button class="btn btn-xs btn-primary" ng-click="ctrl.mostrarDetalles(grupo)" ng-show="!grupo.mostrar">Info</button>
										<button class="btn btn-xs btn-info" ng-click="ctrl.ocultarDetalles(grupo)" ng-hide="!grupo.mostrar">Ocultar</button>
										<button class="btn btn-xs btn-success" ng-click="ctrl.reactivandoGrupoElectrogeno(grupo)">Reactivar</button>
										<button class="btn btn-xs btn-warning" ng-click="ctrl.moviendoGrupoElectrogeno(grupo)">Mover</button>
									</td>
									<td colspan="2">Marca: {{grupo.tipo_grupo_electrogeno.marca}} - Modelo: {{grupo.tipo_grupo_electrogeno.modelo}}</td>
									<td>{{grupo.logs_grupos_electrogenos[0].evento.proveniente.nombre_completo}}</td>
									<td>
										<span ng-if="grupo.almacen == null">Sitio: {{grupo.sitio.nombre_completo}}</span>
										<span ng-if="grupo.almacen != null">Almacén: {{grupo.almacen.almacen}}</span>
									</td>
									<td style="font-size:8pt;">Almacenado: {{grupo.logs_grupos_electrogenos[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}}<br>por {{grupo.logs_grupos_electrogenos[0].usuario.nombres}} {{grupo.logs_grupos_electrogenos[0].usuario.apellido_paterno}}</td>
								</tr>
								<tr ng-repeat-end ng-hide="!grupo.mostrar">
									<td colspan="6">
										<div class="col-xs-12">
											<div class="panel panel-info">
												<div class="panel-heading text-center">
													Información
												</div>
												<div class="panel-body">
													<table class="table table-condensed">
														<tbody>
															<tr class="text-center" style="background-color:#d2f3ce;">
																<td colspan="6">Grupo Electrógeno</td>
															</tr>
															<tr>
																<td class="info">Marca</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.tipo_grupo_electrogeno.marca}}</span>
																	<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_grupo_electrogeno.marca" ng-change="grupo.tipo_grupo_electrogeno.id = '';">
																		<option value="">Elegir marca</option>
																		<option ng-repeat="marca in ctrl.tipo_grupo_electrogeno.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
																	</select>
																</td>
																<td class="info">Modelo</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.tipo_grupo_electrogeno.modelo}}</span>
																	<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_grupo_electrogeno.id">
																		<option value="">Elegir modelo</option>
																		<option ng-if="grupo.tipo_grupo_electrogeno.marca" ng-repeat="modelo in ctrl.tipo_grupo_electrogeno.modelos | filter:{marca:grupo.tipo_grupo_electrogeno.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
																	</select>
																</td>
																<td class="info">Número de serie</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.numero_serie_grupo_electrogeno}}</span>
																	<input class="form-control" ng-hide="!grupo.editar" ng-model="grupo.numero_serie_grupo_electrogeno"/>
																</td>
															</tr>
															<tr class="text-center" style="background-color:#b9d1e5;">
																<td colspan="6">Motor</td>
															</tr>
															<tr>
																<td class="info">Marca</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.tipo_motor_ge.marca}}</span>
																	<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_motor_ge.marca" ng-change="grupo.tipo_motor_ge.id = '';">
																		<option value="">Elegir marca</option>
																		<option ng-repeat="marca in ctrl.tipo_motor_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
																	</select>
																</td>
																<td class="info">Modelo</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.tipo_motor_ge.modelo}}</span>
																	<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_motor_ge.id">
																		<option value="">Elegir modelo</option>
																		<option ng-if="grupo.tipo_motor_ge.marca" ng-repeat="modelo in ctrl.tipo_motor_ge.modelos | filter:{marca:grupo.tipo_motor_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
																	</select>
																</td>
																<td class="info">Número de serie</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.numero_serie_motor}}</span>
																	<input class="form-control" ng-hide="!grupo.editar" ng-model="grupo.numero_serie_motor"/>
																</td>
															</tr>
															<tr class="text-center" style="background-color:#d7e7b4;">
																<td colspan="6">Generador</td>
															</tr>
															<tr>
																<td class="info">Marca</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.tipo_generador_ge.marca}}</span>
																	<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_generador_ge.marca" ng-change="grupo.tipo_generador_ge.id = '';">
																		<option value="">Elegir marca</option>
																		<option ng-repeat="marca in ctrl.tipo_generador_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
																	</select>
																</td>
																<td class="info">Modelo</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.tipo_generador_ge.modelo}}</span>
																	<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_generador_ge.id">
																		<option value="">Elegir modelo</option>
																		<option ng-if="grupo.tipo_generador_ge.marca" ng-repeat="modelo in ctrl.tipo_generador_ge.modelos | filter:{marca:grupo.tipo_generador_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
																	</select>
																</td>
																<td class="info">Número de serie</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.numero_serie_generador}}</span>
																	<input class="form-control" ng-hide="!grupo.editar" ng-model="grupo.numero_serie_generador"/>
																</td>
															</tr>
															<tr>
																<td class="info">Potencia</td>
																<td class="default">
																	<span ng-if="potencia.id == grupo.tipo_generador_ge.id" ng-repeat="potencia in ctrl.tipo_generador_ge.modelos">{{potencia.potencia}} kW</span>
																</td>
																<td class="info">Corriente</td>
																<td class="default">
																	<span ng-if="corriente.id == grupo.tipo_generador_ge.id" ng-repeat="corriente in ctrl.tipo_generador_ge.modelos">{{corriente.corriente}} A</span>
																</td>
																<td class="info"></td>
																<td class="info"></td>
															</tr>
															<tr class="text-center" style="background-color:#e7d7b4;">
																<td colspan="6">AVR</td>
															</tr>
															<tr>
																<td class="info">Marca</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.tipo_avr_ge.marca}}</span>
																	<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_avr_ge.marca" ng-change="grupo.tipo_avr_ge.id = '';">
																		<option value="">Elegir marca</option>
																		<option ng-repeat="marca in ctrl.tipo_avr_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
																	</select>
																</td>
																<td class="info">Modelo</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.tipo_avr_ge.modelo}}</span>
																	<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_avr_ge.id">
																		<option value="">Elegir modelo</option>
																		<option ng-if="grupo.tipo_avr_ge.marca" ng-repeat="modelo in ctrl.tipo_avr_ge.modelos | filter:{marca:grupo.tipo_avr_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
																	</select>
																</td>
																<td class="info">Número de serie</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.numero_serie_avr}}</span>
																	<input class="form-control" ng-hide="!grupo.editar" ng-model="grupo.numero_serie_avr"/>
																</td>
															</tr>
															<tr class="text-center bg-success">
																<td colspan="6">Controlador GE</td>
															</tr>
															<tr>
																<td class="info">Marca</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.tipo_controlador_ge.marca}}</span>
																	<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_controlador_ge.marca" ng-change="grupo.tipo_controlador_ge.id = '';">
																		<option value="">Elegir marca</option>
																		<option ng-repeat="marca in ctrl.tipo_controlador_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
																	</select>
																</td>
																<td class="info">Modelo</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.tipo_controlador_ge.modelo}}</span>
																	<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_controlador_ge.id">
																		<option value="">Elegir modelo</option>
																		<option ng-if="grupo.tipo_controlador_ge.marca" ng-repeat="modelo in ctrl.tipo_controlador_ge.modelos | filter:{marca:grupo.tipo_controlador_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
																	</select>
																</td>
																<td class="info">Número de serie</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.numero_serie_controlador_ge}}</span>
																	<input class="form-control" ng-hide="!grupo.editar" ng-model="grupo.numero_serie_controlador_ge"/>
																</td>
															</tr>
															<tr class="text-center" style="background-color:#f7f7ca;">
																<td colspan="6">Controlador TTA</td>
															</tr>
															<tr>
																<td class="info">Marca</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.tipo_controlador_tta.marca}}</span>
																	<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_controlador_tta.marca" ng-change="grupo.tipo_controlador_tta.id = '';">
																		<option value="">Elegir marca</option>
																		<option ng-repeat="marca in ctrl.tipo_controlador_tta.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
																	</select>
																</td>
																<td class="info">Modelo</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.tipo_controlador_tta.modelo}}</span>
																	<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_controlador_tta.id">
																		<option value="">Elegir modelo</option>
																		<option ng-if="grupo.tipo_controlador_tta.marca" ng-repeat="modelo in ctrl.tipo_controlador_tta.modelos | filter:{marca:grupo.tipo_controlador_tta.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
																	</select>
																</td>
																<td class="info">Número de serie</td>
																<td class="default">
																	<span ng-show="!grupo.editar">{{grupo.numero_serie_controlador_tta}}</span>
																	<input class="form-control" ng-hide="!grupo.editar" ng-model="grupo.numero_serie_controlador_tta"/>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<div class="panel-footer text-center">
													<button class="btn btn-sm btn-warning" ng-click="ctrl.editarGrupo(grupo)" ng-show="!grupo.editar">Editar</button>
													<button class="btn btn-sm btn-success" ng-click="ctrl.grabarGrupo(grupo)" ng-hide="!grupo.editar" ng-disabled="grupo.disabled()">Grabar</button>
													<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarGrupo(grupo)" ng-hide="!grupo.editar">Cancelar</button>
													<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoGrupo(grupo)" ng-show="!grupo.editar">Eliminar</button>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="text-center">
							<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<button class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:15px;" ng-click="ctrl.almacenarGrupo()"><i class="fa fa-plus"></i> Almacenar Grupo Electrógeno</button>
							</div>
						</div>
					</div>
				</div>
				
				<div class="panel-body ng-cloak" ng-if="ctrl.navPanel.index == 1">
					<label style="width:100%" ng-if="!ctrl.cargando">
						<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.busqueda_Grupo.sitio" ng-change="ctrl.listarGrupoSitio(ctrl.busqueda_Grupo.sitio); ctrl.cargando == true;"></selectize>
					</label>
					<ul class="nav nav-pills nav-justified" ng-if="ctrl.grupos_electrogenos_sitio.length && ctrl.busqueda_Grupo.sitio && !ctrl.listando">
						<li role="presentation" ng-class="{active:ctrl.navSitios.index == 0}">
							<a href ng-click="ctrl.navSitios.cambiarIndex(0);">Grupos Electrógenos</a>
						</li>
						<li role="presentation" ng-class="{active:ctrl.navSitios.index == 1}">
							<a href ng-click="ctrl.navSitios.cambiarIndex(1);">Aires Acondicionados</a>
						</li>
					</ul><br>
					<div ng-if="ctrl.navSitios.index == 0">
						<table class="table table-condensed table-striped" ng-if="ctrl.grupos_electrogenos_sitio.length && ctrl.busqueda_Grupo.sitio && !ctrl.listando">
							<thead>
								<tr>
									<th>Acciones</th>
									<th colspan="2">Información</th>
									<th>Proveniente</th>
									<th colspan="2">Historial</th>
								</tr>
							</thead>
							<tbody ng-repeat-start="grupo in ctrl.grupos_electrogenos_sitio" ng-if="ctrl.grupos_electrogenos_sitio.length">
								<tr>
									<td>
										<button class="btn btn-xs btn-primary" ng-click="ctrl.mostrarDetalles(grupo)" ng-show="!grupo.mostrar">Info</button>
										<button class="btn btn-xs btn-info" ng-click="ctrl.ocultarDetalles(grupo)" ng-hide="!grupo.mostrar">Ocultar</button>
										<button class="btn btn-xs btn-success" ng-click="ctrl.reactivandoGrupoElectrogeno(grupo)">Reactivar</button>
										<button class="btn btn-xs btn-warning" ng-click="ctrl.moviendoGrupoElectrogeno(grupo)">Mover</button>
									</td>
									<td colspan="2">Marca: {{grupo.tipo_grupo_electrogeno.marca}} - Modelo: {{grupo.tipo_grupo_electrogeno.modelo}}</td>
									<td>{{grupo.logs_grupos_electrogenos[0].evento.proveniente.nombre_completo}}</td>
									<td style="font-size:8pt;" colspan="2">Almacenado: {{grupo.logs_grupos_electrogenos[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}}<br>por {{grupo.logs_grupos_electrogenos[0].usuario.nombres}} {{grupo.logs_grupos_electrogenos[0].usuario.apellido_paterno}}</td>
								</tr>
							</tbody>
							<tbody ng-repeat-end ng-hide="!grupo.mostrar">
								<tr class="text-center" style="background-color:#d2f3ce;">
									<td colspan="6">Grupo Electrógeno</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_grupo_electrogeno.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_grupo_electrogeno.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_grupo_electrogeno}}</td>
								</tr>
								<tr class="text-center" style="background-color:#b9d1e5;">
									<td colspan="6">Motor</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_motor_ge.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_motor_ge.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_motor}}</td>
								</tr>
								<tr class="text-center" style="background-color:#d7e7b4;">
									<td colspan="6">Generador</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_generador_ge.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_generador_ge.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_generador}}</td>
								</tr>
								<tr>
									<td class="info">Potencia</td>
									<td class="default">{{grupo.tipo_generador_ge.potencia}} kW</td>
									<td class="info">Corriente</td>
									<td class="default">{{grupo.tipo_generador_ge.corriente}} A</td>
									<td class="info" colspan="2"></td>
								</tr>
								<tr class="text-center" style="background-color:#e7d7b4;">
									<td colspan="6">AVR</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_avr_ge.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_avr_ge.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_avr}}</td>
								</tr>
								<tr class="text-center bg-success">
									<td colspan="6">Controlador GE</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_controlador_ge.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_controlador_ge.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_controlador_ge}}</td>
								</tr>
								<tr class="text-center" style="background-color:#f7f7ca;">
									<td colspan="6">Controlador TTA</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_controlador_tta.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_controlador_tta.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_controlador_tta}}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="alert alert-warning" ng-if="!ctrl.grupos_electrogenos_sitio.length && ctrl.busqueda_Grupo.sitio && !ctrl.listando">
						No hay objetos almacenados en este sitio.
					</div>
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando || ctrl.listando"></span>
					</div>
				</div>
				
				<div class="panel-body ng-cloak" ng-if="ctrl.navPanel.index == 2">
					<select class="form-control" ng-model="ctrl.busqueda_Grupo.almacen" ng-change="ctrl.listarGrupoAlmacen(ctrl.busqueda_Grupo.almacen)" ng-if="!ctrl.cargando">
						<option value="">Elegir almacen</option>
						<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
					</select><br>
					<ul class="nav nav-pills nav-justified" ng-if="ctrl.grupos_electrogenos_almacen.length && ctrl.busqueda_Grupo.almacen && !ctrl.listando">
						<li role="presentation" ng-class="{active:ctrl.navAlmacenes.index == 0}">
							<a href ng-click="ctrl.navAlmacenes.cambiarIndex(0);">Grupos Electrógenos</a>
						</li>
						<li role="presentation" ng-class="{active:ctrl.navAlmacenes.index == 1}">
							<a href ng-click="ctrl.navAlmacenes.cambiarIndex(1);">Aires Acondicionados</a>
						</li>
					</ul><br>
					<div ng-if="ctrl.navAlmacenes.index == 0">
						<table class="table table-condensed table-striped" ng-if="ctrl.grupos_electrogenos_almacen.length && ctrl.busqueda_Grupo.almacen && !ctrl.listando">
							<thead>
								<tr>
									<th>Acciones</th>
									<th colspan="2">Información</th>
									<th>Proveniente</th>
									<th colspan="2">Historial</th>
								</tr>
							</thead>
							<tbody ng-repeat-start="grupo in ctrl.grupos_electrogenos_almacen" ng-if="ctrl.grupos_electrogenos_almacen.length && (grupo.almacen.id == ctrl.busqueda_Grupo.almacen)">
								<tr>
									<td>
										<button class="btn btn-xs btn-primary" ng-click="ctrl.mostrarDetalles(grupo)" ng-show="!grupo.mostrar">Info</button>
										<button class="btn btn-xs btn-info" ng-click="ctrl.ocultarDetalles(grupo)" ng-hide="!grupo.mostrar">Ocultar</button>
										<button class="btn btn-xs btn-success" ng-click="ctrl.reactivandoGrupoElectrogeno(grupo)">Reactivar</button>
										<button class="btn btn-xs btn-warning" ng-click="ctrl.moviendoGrupoElectrogeno(grupo)">Mover</button>
									</td>
									<td colspan="2">Marca: {{grupo.tipo_grupo_electrogeno.marca}} - Modelo: {{grupo.tipo_grupo_electrogeno.modelo}}</td>
									<td>{{grupo.logs_grupos_electrogenos[0].evento.proveniente.nombre_completo}}</td>
									<td style="font-size:8pt;" colspan="2">Almacenado: {{grupo.logs_grupos_electrogenos[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}}<br>por {{grupo.logs_grupos_electrogenos[0].usuario.nombres}} {{grupo.logs_grupos_electrogenos[0].usuario.apellido_paterno}}</td>
								</tr>
							</tbody>
							<tbody ng-repeat-end ng-hide="!grupo.mostrar">
								<tr class="text-center" style="background-color:#d2f3ce;">
									<td colspan="6">Grupo Electrógeno</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_grupo_electrogeno.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_grupo_electrogeno.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_grupo_electrogeno}}</td>
								</tr>
								<tr class="text-center" style="background-color:#b9d1e5;">
									<td colspan="6">Motor</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_motor_ge.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_motor_ge.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_motor}}</td>
								</tr>
								<tr class="text-center" style="background-color:#d7e7b4;">
									<td colspan="6">Generador</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_generador_ge.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_generador_ge.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_generador}}</td>
								</tr>
								<tr>
									<td class="info">Potencia</td>
									<td class="default">{{grupo.tipo_generador_ge.potencia}} kW</td>
									<td class="info">Corriente</td>
									<td class="default">{{grupo.tipo_generador_ge.corriente}} A</td>
									<td class="info" colspan="2"></td>
								</tr>
								<tr class="text-center" style="background-color:#e7d7b4;">
									<td colspan="6">AVR</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_avr_ge.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_avr_ge.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_avr}}</td>
								</tr>
								<tr class="text-center bg-success">
									<td colspan="6">Controlador GE</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_controlador_ge.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_controlador_ge.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_controlador_ge}}</td>
								</tr>
								<tr class="text-center" style="background-color:#f7f7ca;">
									<td colspan="6">Controlador TTA</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_controlador_tta.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_controlador_tta.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_controlador_tta}}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="alert alert-warning" ng-if="!ctrl.grupos_electrogenos_almacen.length && ctrl.busqueda_Grupo.almacen && !ctrl.listando">
						No hay objetos almacenados en este almacén.
					</div>
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando || ctrl.listando"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div id="reactivarGrupoElectrogeno" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Poner Grupo Electrógeno en funcionamiento</h3>
	
	<p>Buscar sitio de destino:</p>

	<label style="width:100%" ng-if="!ctrl.cargando">
		<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.reactivar_GrupoElectrogeno.sitio.id"></selectize>
	</label>
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-2x" ng-if="ctrl.cargando">Cargando lista de sitios...</span>
	</div>
	
	<br><textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea poner en funcionamiento el elemento..." ng-model="ctrl.reactivar_GrupoElectrogeno.comentario"></textarea>
	
	<button class="btn btn-success" ng-click="ctrl.reactivarGrupoElectrogeno(ctrl.reactivar_GrupoElectrogeno)" ng-disabled="!ctrl.reactivar_GrupoElectrogeno.sitio.id || !ctrl.reactivar_GrupoElectrogeno.comentario">Poner en funcionamiento</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarReactivacionGE()">Cancelar</button>
</div>

<div id="moverGrupoElectrogeno" class="popup" style="max-width:600px;">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Mover Grupo Electrógeno a otro almacén</h3>
	
	<p>Elegir lugar de almacenamiento:</p>

	<div class="row">
		<div class="col-xs-12 text-center">
			<div class="btn-group btn-group-justified" role="group" aria-label="...">
				<div class="btn-group" role="group">
					<button type="radio" class="btn btn-default" ng-model="ctrl.mover_GrupoElectrogeno.opcion_sitio" ng-click="ctrl.mover_GrupoElectrogeno.opcion_sitio = true; ctrl.mover_GrupoElectrogeno.opcion_almacen = false; ctrl.opcionSitioM()" ng-class="{active:ctrl.mover_GrupoElectrogeno.opcion_sitio == true}">Sitio</button>
				</div>
				<div class="btn-group" role="group">
					<button type="radio" class="btn btn-default" ng-model="ctrl.mover_GrupoElectrogeno.opcion_almacen" ng-click="ctrl.mover_GrupoElectrogeno.opcion_almacen = true; ctrl.mover_GrupoElectrogeno.opcion_sitio = false; ctrl.opcionAlmacenM()" ng-class="{active:ctrl.mover_GrupoElectrogeno.opcion_almacen == true}">Almacen</button>
				</div>
			</div>
		</div>
	</div>
	
	<label style="width:100%" ng-if="ctrl.mover_GrupoElectrogeno.opcion_sitio">
		<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.mover_GrupoElectrogeno.sitio"></selectize>
	</label>
	
	<select class="form-control" ng-if="ctrl.mover_GrupoElectrogeno.opcion_almacen" ng-model="ctrl.mover_GrupoElectrogeno.almacen">
		<option value="0">Elegir almacen</option>
		<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
	</select>

	<br>
	<button class="btn btn-success" ng-click="ctrl.moverGrupoElectrogeno(ctrl.mover_GrupoElectrogeno)" ng-disabled="((!ctrl.mover_GrupoElectrogeno.opcion_sitio || !ctrl.mover_GrupoElectrogeno.sitio) && (!ctrl.mover_GrupoElectrogeno.opcion_almacen || ctrl.mover_GrupoElectrogeno.almacen == 0))">Mover</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarMovimientoGE()">Cancelar</button>
</div>

<div id="almacenarGrupo" class="popup" style="max-width:1000px;">
	<span class="button b-close" ng-click="ctrl.cancelarAlmacenamientoGrupo()">
		<span>X</span>
	</span>
	
	<h3>Almacenar Grupo Electrógeno</h3>
	
	<p>Elegir lugar de almacenamiento:</p>

	<div class="row">
		<div class="col-xs-12 text-center">
			<div class="btn-group btn-group-justified" role="group" aria-label="...">
				<div class="btn-group" role="group">
					<button type="radio" class="btn btn-default" ng-model="ctrl.almacenar_Grupo.opcion_sitio" ng-click="ctrl.almacenar_Grupo.opcion_sitio = true; ctrl.almacenar_Grupo.opcion_almacen = false; ctrl.opcionSitio()" ng-class="{active:ctrl.almacenar_Grupo.opcion_sitio == true}">Sitio</button>
				</div>
				<div class="btn-group" role="group">
					<button type="radio" class="btn btn-default" ng-model="ctrl.almacenar_Grupo.opcion_almacen" ng-click="ctrl.almacenar_Grupo.opcion_almacen = true; ctrl.almacenar_Grupo.opcion_sitio = false; ctrl.opcionAlmacen()" ng-class="{active:ctrl.almacenar_Grupo.opcion_almacen == true}">Almacen</button>
				</div>
			</div>
		</div>
	</div>
	
	<label style="width:100%" ng-if="ctrl.almacenar_Grupo.opcion_sitio">
		<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenar_Grupo.sitio"></selectize>
	</label>
	
	<select class="form-control" ng-if="ctrl.almacenar_Grupo.opcion_almacen" ng-model="ctrl.almacenar_Grupo.almacen">
		<option value="0">Elegir almacen</option>
		<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
	</select>
	
	<br>
	<form>
		<table class="table table-condensed">
			<tbody>
				<tr class="text-center" style="background-color:#d2f3ce;">
					<td colspan="6">Grupo Electrógeno</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.almacenar_Grupo.tipo_grupo_electrogeno.marca" ng-change="ctrl.almacenar_Grupo.tipo_grupo_electrogeno.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_grupo_electrogeno.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.almacenar_Grupo.tipo_grupo_electrogeno.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.almacenar_Grupo.tipo_grupo_electrogeno.marca" ng-repeat="modelo in ctrl.tipo_grupo_electrogeno.modelos | filter:{marca:ctrl.almacenar_Grupo.tipo_grupo_electrogeno.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.almacenar_Grupo.numero_serie_grupo_electrogeno"/>
					</td>
				</tr>
				<tr class="text-center" style="background-color:#b9d1e5;">
					<td colspan="6">Motor</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.almacenar_Grupo.tipo_motor_ge.marca" ng-change="ctrl.almacenar_Grupo.tipo_motor_ge.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_motor_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.almacenar_Grupo.tipo_motor_ge.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.almacenar_Grupo.tipo_motor_ge.marca" ng-repeat="modelo in ctrl.tipo_motor_ge.modelos | filter:{marca:ctrl.almacenar_Grupo.tipo_motor_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.almacenar_Grupo.numero_serie_motor"/>
					</td>
				</tr>
				<tr class="text-center" style="background-color:#d7e7b4;">
					<td colspan="6">Generador</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.almacenar_Grupo.tipo_generador_ge.marca" ng-change="ctrl.almacenar_Grupo.tipo_generador_ge.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_generador_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.almacenar_Grupo.tipo_generador_ge.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.almacenar_Grupo.tipo_generador_ge.marca" ng-repeat="modelo in ctrl.tipo_generador_ge.modelos | filter:{marca:ctrl.almacenar_Grupo.tipo_generador_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.almacenar_Grupo.numero_serie_generador"/>
					</td>
				</tr>
				<tr>
					<td class="info">Potencia</td>
					<td class="default">
						<span ng-if="potencia.id == ctrl.almacenar_Grupo.tipo_generador_ge.id" ng-repeat="potencia in ctrl.tipo_generador_ge.modelos">{{potencia.potencia}} kW</span>
					</td>
					<td class="info">Corriente</td>
					<td class="default">
						<span ng-if="corriente.id == ctrl.almacenar_Grupo.tipo_generador_ge.id" ng-repeat="corriente in ctrl.tipo_generador_ge.modelos">{{corriente.corriente}} A</span>
					</td>
					<td class="info" colspan="2"></td>
				</tr>
				<tr class="text-center" style="background-color:#e7d7b4;">
					<td colspan="6">AVR</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.almacenar_Grupo.tipo_avr_ge.marca" ng-change="ctrl.almacenar_Grupo.tipo_avr_ge.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_avr_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.almacenar_Grupo.tipo_avr_ge.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.almacenar_Grupo.tipo_avr_ge.marca" ng-repeat="modelo in ctrl.tipo_avr_ge.modelos | filter:{marca:ctrl.almacenar_Grupo.tipo_avr_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.almacenar_Grupo.numero_serie_avr"/>
					</td>
				</tr>
				<tr class="text-center bg-success">
					<td colspan="6">Controlador GE</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.almacenar_Grupo.tipo_controlador_ge.marca" ng-change="ctrl.almacenar_Grupo.tipo_controlador_ge.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_controlador_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.almacenar_Grupo.tipo_controlador_ge.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.almacenar_Grupo.tipo_controlador_ge.marca" ng-repeat="modelo in ctrl.tipo_controlador_ge.modelos | filter:{marca:ctrl.almacenar_Grupo.tipo_controlador_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.almacenar_Grupo.numero_serie_controlador_ge"/>
					</td>
				</tr>
				<tr class="text-center" style="background-color:#f7f7ca;">
					<td colspan="6">Controlador TTA</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.almacenar_Grupo.tipo_controlador_tta.marca" ng-change="ctrl.almacenar_Grupo.tipo_controlador_tta.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_controlador_tta.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.almacenar_Grupo.tipo_controlador_tta.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.almacenar_Grupo.tipo_controlador_tta.marca" ng-repeat="modelo in ctrl.tipo_controlador_tta.modelos | filter:{marca:ctrl.almacenar_Grupo.tipo_controlador_tta.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.almacenar_Grupo.numero_serie_controlador_tta"/>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarGrupo(ctrl.almacenar_Grupo)" ng-disabled="!ctrl.almacenar_Grupo.tipo_grupo_electrogeno.id || !ctrl.almacenar_Grupo.tipo_motor_ge.id || !ctrl.almacenar_Grupo.tipo_generador_ge.id || !ctrl.almacenar_Grupo.tipo_avr_ge.id || !ctrl.almacenar_Grupo.tipo_controlador_ge.id || !ctrl.almacenar_Grupo.tipo_controlador_tta.id">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamientoGrupo()">Cancelar</button>
	</form>
</div>

<div id="eliminarGrupo" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar Grupo Electrógeno</h3>
	
	<p>¿Está seguro de eliminar el grupo electrógeno?</p>
	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Grupo.comentario"></textarea>
	
	<button class="btn btn-success" ng-click="ctrl.eliminarGrupo()" ng-disabled="!ctrl.eliminar_Grupo.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>