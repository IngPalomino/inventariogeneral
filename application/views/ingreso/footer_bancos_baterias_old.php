<script type="text/javascript">
	$( function() {
		$('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
	} );
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargando = true;
			ref.procesando = false;

			ref.bancos_baterias = [];
			ref.nuevoBanco = {
				cantidad_bancos: ""
			};
			ref.eliminar_Banco = {};
			ref.eliminar_Celda = {};
			ref.rectificadores = [];
			ref.tipo_bancos_baterias = [];
			ref.marcas_tipo_bancos_baterias = [];

			listarBancosBaterias = function(){
				$http({
					url:"<?php echo base_url("index.php/bancos_baterias/listar_sitio_controlador");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					data = data.rectificadores;
					if(data.length)
					{
						data.forEach(function(dato,ind,objeto){
							objeto[ind].bancos.forEach(function(banco,indy,objetoy){
								objetoy[indy].celdas_controlador_rectif_bancos_baterias.forEach(function(celda,indz,objetoz){
									try{ objetoz[indz].fecha_instalacion = new Date(objetoz[indz].fecha_instalacion + " 00:00:00"); }
									catch(err){ objetoz[indz].fecha_instalacion = null; }
								});
								
								objetoy[indy].logs_controlador_rectif_bancos_baterias.forEach(function(modificacion,indz,objetoz){
									try{ objetoz[indz].created_at = new Date(objetoz[indz].created_at); }
									catch(err){ objetoz[indz].created_at = null; }
								});
							
								try{ objetoy[indy].created_at = new Date(objetoy[indy].created_at); } catch(err){ objetoy[indy].created_at = null; }
							});
						});
						
						ref.bancos_baterias = data;
						
						ref.bancos_baterias.forEach(function(rectif,indx,objx){
							objx[indx].disabled = function(){
								var dis = false;

								objx[indx].bancos.forEach(function(banco,indz,objz){
									objz[indz].celdas_controlador_rectif_bancos_baterias.forEach(function(celda,indy,objy){
										if(objy[indy].tipo_bancos_baterias.id == "" || objz[indz].fecha_instalacion == "")
										{
											dis = true;
										}
									});
								});
								
								return dis;
							}
						});
						
						ref.cargando = false;
					}
					else
					{
						ref.cargando = false;
					}
					console.log(ref.bancos_baterias);
				}).error(function(err){
					ref.cargando = false;
				});
			}
			
			listarRectificadores = function(){
				$http({
					url:"<?php echo base_url("index.php/rectificadores/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET",
					headers:{'X-Requested-With' : 'XMLHttpRequest'}
				}).success(function(data){
					if(data.length)
					{
						ref.rectificadores = data;
					}
					else
					{

					}
					/*console.log(data);*/
				}).error(function(err){

				});
			}
			
			listarTipoBancosBaterias = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_bancos_baterias/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipo_bancos_baterias = data;
					}
					else
					{

					}
					/*console.log(data);*/
				}).error(function(err){

				});
			}
			
			listarMarcasTipoBancosBaterias = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_bancos_baterias/listar_marcas");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.marcas_tipo_bancos_baterias = data;
					}
					else
					{

					}
					/*console.log(data);*/
				}).error(function(err){

				});
			}
			
			ref.nuevo_banco = function(){
				$("#nuevoBanco").bPopup();
			}
			
			ref.cancelarCreacionBanco = function(){
				$("#nuevoBanco").bPopup().close();
				ref.nuevoBanco = {};
			}
			
			ref.agregarBanco = function(banco){
				console.log(banco);
				var data = "",
					campos = Object.keys(banco),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(banco[campo] != null)
						{
							if(typeof banco[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+banco[campo];
							}
							else
							{
								if(campo == "fecha_instalacion")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+banco[campo].toISOString();
								}
								else
								{
									if(banco[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(banco[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+banco[campo].id;
									}
										
								}
							}

							contador++;
						}
					}
				});
				
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("index.php/bancos_baterias/banco_bateria/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevoBanco").bPopup().close();
					listarBancosBaterias();
					ref.nuevoBanco = {};
				}).error(function(err){

				});
			}
			
			ref.eliminandoBanco = function(info){
				ref.eliminar_Banco = info;
				$("#eliminarBanco").bPopup();
			}
			
			ref.cancelarEliminacion = function(){
				$("#eliminarBanco").bPopup().close();
				ref.eliminar_Banco = {};
			}
			
			ref.eliminarBanco = function(){
				var data = "comentario="+ref.eliminar_Banco.comentario;

				$http({
					url:"<?php echo base_url("index.php/bancos_baterias/bancos_baterias");?>"+"/"+ref.eliminar_Banco.controlador_rectif,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){console.log(data);
					if(data.error)
					{

					}
					else
					{
						ref.bancos_baterias = [];
						listarBancosBaterias();
						ref.eliminar_Banco = {};
						$('#eliminarBanco').bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.editarBanco = function(rectif){
				rectif.editar = true;
			}
			
			ref.grabarBanco = function(rectif){
				borrarPropiedades(rectif.bancos,"$$hashKey");
				borrarPropiedades(rectif.bancos,"created_at");
				borrarPropiedades(rectif.bancos,"mostrar");
				ref.procesando = true;

				var data = "",
					campos = Object.keys(rectif.bancos),
					contador = 0;
					
				rectif.bancos.forEach(function(banco,ind,obj){
					obj[ind]["tipo_bancos_baterias"] = banco.tipo_bancos_baterias.id;
					
					banco.celdas_controlador_rectif_bancos_baterias.forEach(function(celda,indx,objx){
						objx[indx]["tipo_bancos_baterias"] = celda.tipo_bancos_baterias.id;
					});
				});
				/*console.log(rectif.bancos);*/
				data = "data="+JSON.stringify(rectif.bancos);

				$http({
					url:"<?php echo base_url("index.php/bancos_baterias/bancos_baterias");?>",
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){console.log(resp);
					rectif.editar = false;
					listarBancosBaterias();
					ref.procesando = false;
				}).error(function(err){
					ref.procesando = false;
				});
				/*console.log(data);*/
			}
			
			ref.cancelarBanco = function(rectif){
				rectif.editar = false;
				listarBancosBaterias();
			}
			
			ref.eliminandoCelda = function(info){
				ref.eliminar_Celda = info;
				$("#eliminarCelda").bPopup();
			}
			
			ref.cancelarEliminacionCelda = function(){
				$("#eliminarCelda").bPopup().close();
				ref.eliminar_Celda = {};
			}
			
			ref.eliminarCelda = function(){
			var data = "comentario="+ref.eliminar_Celda.comentario;
			
				$http({
					url:"<?php echo base_url("index.php/bancos_baterias/banco_bateria");?>"+"/"+ref.eliminar_Celda.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){console.log(data);
					if(data.error)
					{

					}
					else
					{
						ref.bancos_baterias = [];
						listarBancosBaterias();
						ref.eliminar_Celda = {};
						$('#eliminarCelda').bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.mostrarCeldas = function(rectif){
				rectif.mostrar = true;
			}
			
			ref.ocultarCeldas = function(rectif){
				rectif.mostrar = false;
			}
			
			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}

			listarBancosBaterias();
			listarRectificadores();
			listarTipoBancosBaterias();
			listarMarcasTipoBancosBaterias();
		}]);
</script>
</body>
</html>