<script type="text/javascript">
    $( function() {
		$('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
	} );
	angular
		.module('app',[])
		.service("serviciosEspeciales",function($http,$q){
			var resultado = $q.defer();
			
			this.listarModelosControlador = function(marca){
				return $http({
					url:"<?php echo base_url("index.php/tipo_controlador_rectif/listar_modelos");?>/"+marca,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						resultado.resolve(data);
						
						listado = resultado.promise;

						return listado;
					}
					else
					{

					}
				}).error(function(err){

				});
			};
			
			/*Modelo de Módulos*/
			this.listarModelosModRectificador = function(marca){
				return $http({
					url:"<?php echo base_url("index.php/tipo_modulos_rectif/listar_modelos"); ?>/"+marca,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						resultado.resolve(data);
						listadoMod = resultado.promise;
						return listadoMod;
					}
					else
					{
						
					}
				}).error(function(err){
					
				});
			};
		})
		.controller('controlador', ["$http","$q","serviciosEspeciales",function($http,$q,serviciosEspeciales){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.rectificadores = [];

			ref.eliminar_Rectificador = {};
			ref.eliminar_ModRectificador = {};
			
			ref.tipo_controlador_rectif = [];
			ref.ubicacion_controlador_rectif = [];
			ref.tipo_modulos_rectif = [];
			ref.modulos_rectif = [];
			
			ref.nuevoRectificador = {
				tipo_controlador_rectif: {
					marca: "",
					modelo: ""
				},
				tipos_controlador_rectif: ref.modulos_rectif,
				listarModelosControlador: function(){
					rectif = this;
					serviciosEspeciales
						.listarModelosControlador(rectif.tipo_controlador_rectif.marca)
						.then(
							function(res){
								rectif.tipos_controlador_rectif = res.data;
							},
							function(err){}
						);
				},
				ingNuevoModRect: function(controlador_rectif = "0", tipos_modulos_rectif = []){
					this.controlador_rectif = controlador_rectif.id;
					this.eliminado = 0;
					this.numero_serie = "";
					this.tipo_modulos_rectif = {
						amperaje: 0,
						capacidad: 0,
						voltaje: 0,
						marca: "",
						modelo: "",
						id: ""
					};
					this.tipos_modulos_rectif = tipos_modulos_rectif;
				},
				modulos_rectif: [],
				listarModulosRectif: function(){
					rectif = this;

					rectif.modulos_rectif.forEach(function(modulo,indm,objetom){
						serviciosEspeciales
							.listarModelosModRectificador(rectif.tipo_controlador_rectif.id)
							.then(
								function(res){
									objetom[indm].tipos_modulos_rectif = res.data;
								},
								function(err){}
							);
					})
				},
				
				deshabilitarGrabar: function(){
								
					var disable = false;
					
					this.modulos_rectif.forEach(function(modulo,indy,objetoy){
						if(modulo.tipo_modulos_rectif.id == "" || modulo.numero_serie == "")
						{
							disable = true;
						}
					});
					
					return disable;
				}
			};
			
			listarRectificadores = function(){
				$http({
					url:"<?php echo base_url("index.php/rectificadores/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.rectificadores = data;

						ref.rectificadores.forEach(function(rectificador,indx,objetox){
							objetox[indx].tipos_controlador_rectif = [];

							objetox[indx].listarModelosControlador = function(){
								rectif = this;
								serviciosEspeciales
									.listarModelosControlador(rectif.tipo_controlador_rectif.marca)
									.then(
										function(res){
											rectif.tipos_controlador_rectif = res.data;
										},
										function(err){}
									);
							};
							
							objetox[indx].capacidadInstalada = function(){
								var capInstalada = 0;
								
								this.modulos_rectif.forEach(function(modulo,indy,objetoy){
									if(modulo.eliminado != '1')
									{
										capInstalada += Number(modulo.tipo_modulos_rectif.capacidad);
									}
								});
								
								return capInstalada;
							};
							
							objetox[indx].modulosInstalados = function(){
								
								var modulosInstalados = 0;
								
								this.modulos_rectif.forEach(function(modulo,indy,objetoy){
									if(modulo.eliminado != '1')
									{
										modulosInstalados++;
									}
								});
								
								return modulosInstalados;
							};
							
							serviciosEspeciales
								.listarModelosControlador(rectificador.tipo_controlador_rectif.marca)
								.then(
									function(res){
										objetox[indx].tipos_controlador_rectif = res.data;
									},
									function(err){}
								);
								
							rectificador.modulos_rectif.forEach(function(modulo,indy,objetoy){
								serviciosEspeciales
									.listarModelosModRectificador(rectificador.tipo_controlador_rectif.id)
									.then(
										function(res){
											objetoy[indy].tipos_modulos_rectif = res.data;
										},
										function(err){}
									);
							});
							
							objetox[indx].ingNuevoModRectif = function(controlador_rectif = "0", tipos_modulos_rectif = []){
								this.controlador_rectif = controlador_rectif;
								this.eliminado = 0;
								this.numero_serie = "";
								this.tipo_modulos_rectif = {
									amperaje: 0,
									capacidad: 0,
									voltaje: 0,
									marca: "",
									modelo: "",
									id: 0
								};
								this.tipos_modulos_rectif = tipos_modulos_rectif;
							};
							
							objetox[indx].deshabilitarGrabar = function(){
								
								var disable = false;
								
								this.modulos_rectif.forEach(function(modulo,indy,objetoy){
									if(modulo.tipo_modulos_rectif.id == 0)
									{
										disable = true;
									}
								});
								
								return disable;
							};
						});
						
						data.forEach(function(dato,ind,objeto){
							try { objeto[ind].anio_servicio = new Date(objeto[ind].anio_servicio + " 00:00:00"); }catch(err){ objeto[ind].anio_servicio = null;}
							objeto[ind].capacidad_maxima = Number(objeto[ind].capacidad_maxima);
						});
						
						console.log(ref.rectificadores);
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoControladorRectif = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_controlador_rectif/listar_marcas");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipo_controlador_rectif = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarUbicacionControladorRectif = function(){
				$http({
					url:"<?php echo base_url("index.php/ubicacion_controlador_rectif/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.ubicacion_controlador_rectif = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			ref.listarTipoModulosRectif = function(rectificador){
				rectificador.modulos_rectif.forEach(function(modulo,indy,objetoy){
					serviciosEspeciales
						.listarModelosModRectificador(rectificador.tipo_controlador_rectif.id)
						.then(
							function(res){
								objetoy[indy].tipos_modulos_rectif = res.data;
							},
							function(err){}
						);
				});
			}
			
			listarModulosRectif = function(){
				$http({
					url:"<?php echo base_url("index.php/modulos_rectif/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.modulos_rectif = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.listarModelosControlador = function(marca,objeto){
				$http({
					url:"<?php echo base_url("index.php/tipo_controlador_rectif/listar_modelos");?>/"+marca,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						objeto = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			ref.listarModelosModRectificador = function(tipos_controlador_rectif,objeto){
				$http({
					url:"<?php echo base_url("index.php/tipo_modulos_rectif/listar_modelos"); ?>/"+tipo_controlador_rectif,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						objeto = data;
					}
					else
					{
						
					}
				}).error(function(err){
					
				});
			}
			
			ref.actualizarModCapacidad = function(modulo,tipo_modulo){
				modulo.tipo_modulos_rectif.capacidad = modulo.tipo_modulo.capacidad;
			}
			
			ref.nuevo_rectificador = function(){
				$("#nuevoRectificador").bPopup();
			}
			
			ref.cancelarCreacionRectificador = function(){
				$("#nuevoRectificador").bPopup().close();

				ref.nuevoRectificador = {};
				ref.nuevoRectificador.modulos_rectif = [];

				/*ref.nuevoRectificador = {
					tipo_controlador_rectif: {
						marca: "",
						modelo: ""
					},
					tipos_controlador_rectif: ref.modulos_rectif,
					listarModelosControlador: function(){
						rectif = this;
						serviciosEspeciales
							.listarModelosControlador(rectif.tipo_controlador_rectif.marca)
							.then(
								function(res){
									rectif.tipos_controlador_rectif = res.data;
								},
								function(err){}
							);
					},
					ingNuevoModRect: function(controlador_rectif = "0", tipos_modulos_rectif = []){
						this.controlador_rectif = controlador_rectif.id;
						this.eliminado = 0;
						this.numero_serie = "";
						this.tipo_modulos_rectif = {
							amperaje: 0,
							capacidad: 0,
							voltaje: 0,
							marca: "",
							modelo: ""
						};
						this.tipos_modulos_rectif = tipos_modulos_rectif;
					},
					modulos_rectif: [],
					listarModulosRectif: function(){
						rectif = this;

						rectif.modulos_rectif.forEach(function(modulo,indm,objetom){
							serviciosEspeciales
								.listarModelosModRectificador(rectif.tipo_controlador_rectif.id)
								.then(
									function(res){
										objetom[indm].tipos_modulos_rectif = res.data;
									},
									function(err){}
								);
						})
					}
				};*/
			}
			
			ref.agregarRectificador = function(rectificador){
				borrarPropiedades(rectificador,"tipos_controlador_rectif");
				borrarPropiedades(rectificador,"ingNuevoModRect");
				borrarPropiedades(rectificador,"listarModelosControlador");
				borrarPropiedades(rectificador,"listarModulosRectif");
				borrarPropiedades(rectificador,"$$hashKey");
				borrarPropiedades(rectificador,"created_at");
				borrarPropiedades(rectificador,"tipos_modulos_rectif");
				/*borrarPropiedades(rectificador.modulos_rectif,"tipo_controlador_rectif");*/
			
				var data = "",
					campos = Object.keys(rectificador),
					contador = 0;

				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "editar" && campo != "eliminado")
					{
						if(rectificador[campo] != null)
						{
							/*if(contador > 0)
							{
								data += "&";
							}*/

							if(typeof rectificador[campo] != "object")
							{
								if(typeof rectificador[campo] != "function")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+rectificador[campo];
								}
							}
							else
							{
								if(campo == "anio_servicio")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+rectificador[campo].toISOString();
								}
								else
								{
									if(rectificador[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(rectificador[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+rectificador[campo].id;
									}
								}
							}

							contador++;
						}
					}
				});

				$http({
					url:"<?php echo base_url("index.php/rectificadores/rectificador/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevoRectificador").bPopup().close();
					listarRectificadores();

					ref.nuevoRectificador = {
						tipo_controlador_rectif: {
							marca: "",
							modelo: ""
						},
						tipos_controlador_rectif: ref.modulos_rectif,
						listarModelosControlador: function(){
							rectif = this;
							serviciosEspeciales
								.listarModelosControlador(rectif.tipo_controlador_rectif.marca)
								.then(
									function(res){
										rectif.tipos_controlador_rectif = res.data;
									},
									function(err){}
								);
						},
						ingNuevoModRect: function(controlador_rectif = "0", tipos_modulos_rectif = []){
							this.controlador_rectif = controlador_rectif.id;
							this.eliminado = 0;
							this.numero_serie = "";
							this.tipo_modulos_rectif = {
								amperaje: 0,
								capacidad: 0,
								voltaje: 0,
								marca: "",
								modelo: ""
							};
							this.tipos_modulos_rectif = tipos_modulos_rectif;
						},
						modulos_rectif: [],
						listarModulosRectif: function(){
							rectif = this;

							rectif.modulos_rectif.forEach(function(modulo,indm,objetom){
								serviciosEspeciales
									.listarModelosModRectificador(rectif.tipo_controlador_rectif.id)
									.then(
										function(res){
											objetom[indm].tipos_modulos_rectif = res.data;
										},
										function(err){}
									);
							})
						}
					};
				}).error(function(err){

				});
			}
			
			ref.eliminandoRectificador = function(info){
				ref.eliminar_Rectificador = info;
				$("#eliminarRectificador").bPopup();
			}
			
			ref.cancelarEliminacion = function(){
				$("#eliminarRectificador").bPopup().close();
				ref.eliminar_Rectificador = {};
			}
			
			ref.eliminarRectificador = function(){
				var data = "comentario="+ref.eliminar_Rectificador.comentario;

				$http({
					url:"<?php echo base_url("index.php/rectificadores/rectificador");?>"+"/"+ref.eliminar_Rectificador.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.rectificadores = [];
						listarRectificadores();
						ref.eliminar_Rectificador = {};
						$('#eliminarRectificador').bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.editarRectificador = function(rectificador){
				rectificador.editar = true;
			}
			
			ref.grabarRectificador = function(rectificador){console.log(rectificador);
				borrarPropiedades(rectificador,"$$hashKey");
				borrarPropiedades(rectificador,"created_at");
				borrarPropiedades(rectificador,"tipos_modulos_rectif");
				borrarPropiedades(rectificador.modulos_rectif,"tipo_controlador_rectif");
			
				var data = "",
					campos = Object.keys(rectificador),
					contador = 0;

				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado")
					{
						if(rectificador[campo] != null)
						{
							/*if(contador > 0)
							{
								data += "&";
							}*/

							if(typeof rectificador[campo] != "object")
							{
								if(typeof rectificador[campo] != "function")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+rectificador[campo];
								}
							}
							else
							{
								if(campo == "anio_servicio")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+rectificador[campo].toISOString();
								}
								else
								{
									if(rectificador[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(rectificador[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+rectificador[campo].id;
									}
								}
							}

							contador++;
						}
					}
				});

				console.log(data);
				
				$http({
					url:"<?php echo base_url("index.php/rectificadores/rectificador");?>/"+rectificador.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					rectificador.editar = false;
					listarRectificadores();
				}).error(function(err){

				});
			}
			
			ref.cancelarRectificador = function(rectificador){
				rectificador.editar = false;
				listarRectificadores();
			}

			ref.nuevoModRectificador = function(rectificador){
				serviciosEspeciales
					.listarModelosModRectificador(rectificador.tipo_controlador_rectif.id)
					.then(
						function(res){
							rectificador.modulos_rectif.push(new rectificador.ingNuevoModRectif(rectificador.id,"",res.data));
	
						},
						function(err){}
					);
			}
			
			ref.nuevoModRectif = function(nuevoRectificador){
				console.log(nuevoRectificador);
				serviciosEspeciales
					.listarModelosModRectificador(nuevoRectificador.tipo_controlador_rectif.id)
					.then(
						function(res){
							nuevoRectificador.modulos_rectif.push(new nuevoRectificador.ingNuevoModRect(nuevoRectificador,"",res.data));
						},
						function(err){}
					);
			}
			
			ref.eliminarNuevoModRectif = function(index){
				ref.nuevoRectificador.modulos_rectif.splice(index,1);
			}
			
			ref.eliminandoModRectificador = function(modRectificador){
				ref.eliminar_ModRectificador = modRectificador;
				$("#eliminarModRectificador").bPopup();
			}
			
			ref.cancelarEliminacionModRectificador = function(){
				$("#eliminarModRectificador").bPopup().close();
				ref.eliminar_ModRectificador.comentario = "";
				ref.eliminar_ModRectificador = {};
			}
			
			ref.eliminarModRectificador = function(modRectificador){
				var data = "",
					campos = Object.keys(modRectificador),
					contador = 0;

				data += "comentario="+modRectificador.comentario;

				$http({
					url:"<?php echo base_url("index.php/modulos_rectif/rectificador");?>/"+modRectificador.id,
					method:"DELETE",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					console.log(data);
					if(!(data.error))
					{
						$("#eliminarModRectificador").bPopup().close();
						ref.eliminar_ModRectificador.comentario = "";
						ref.eliminar_ModRectificador = {};
						ref.editar = false;
						
						listarRectificadores();
					}
					else
					{
						ref.error.error = true;
						ref.error.actualizar = data.error;
					}
				}).error(function(err){
					
				});
			}
			
			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}
			
			listarRectificadores();
			listarTipoControladorRectif();
			listarUbicacionControladorRectif();
			/*listarTipoModulosRectif();*/
			listarModulosRectif();
		}]);
</script>
</body>
</html>