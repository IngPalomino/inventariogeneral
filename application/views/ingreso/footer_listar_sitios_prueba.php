<!--<p>
	<?php print_r($session);?>
</p>-->

<script type="text/javascript">
	/*$("input.fecha").datepicker({dateFormat:"dd/mm/yy"});*/

	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";
			ref.error.crear = "";
			ref.error.actualizar = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			var limit = 50,
				offset = 0;

			ref.busqueda = {
				codigo:"",
				nombre:"",
				departamento:"",
				provincia:"",
				distrito:""
			};

			ref.options = [
				{id: 1, title: 'Spectrometer'},
				{id: 2, title: 'Star Chart'},
				{id: 3, title: 'Laser Pointer'}
			];

			ref.config = {
				create: true,
				valueField: 'id',
				labelField: 'title',
				delimiter: '|',
				placeholder: 'Departamentos...',
				onInitialize: function(selectize){
					// receives the selectize object as an argument
				},
				maxItems: 3
			};

			ref.sitios = [];
			ref.sitioEditar = {
								codigo:"",
								nombre:"",
								departamento:"",
								provincia:"",
								distrito:"",
								latitud:0,
								longitud:0,
								direccion:"",
								in_building:false,
								coubicado:false,
								agregador:false,
								dorsal:false,
								greenfield_rooftop:false,
								altura_edificacion:0,
								ubicacion_equipos:{id:0},
								tipo_torre:{id:0},
								altura_torre:0,
								anio_construccion:(new Date()).getFullYear(),
								camuflaje:0,
								prioridad:4,
								fecha_inicio_contrato:new Date(),
								fecha_fin_contrato:new Date(),
								area_piso:0,
								factor_uso_torre:0,
								coubicador:{id:0},
								nombre_sitio_coubicador:"",
								codigo_torrera:"",
								operador_coubicante:0,
								nombre_sitio_coubicante:"",
								estado:1,
								fecha_estado_on_air:new Date(),
								proyecto:0,
								contrata_constructora:0,
								tipo_solucion:0,
								proveedor_mantenimiento:0,
								consideracion_acceso:"",
								acceso_libre_24h:0,
								nivel_riesgo:0,
								consideraciones:""
								};
			ref.sitioNuevo = {
								codigo:"",
								nombre:"",
								departamento:"",
								provincia:"",
								distrito:"",
								latitud:0,
								longitud:0,
								direccion:"",
								in_building:false,
								coubicado:false,
								agregador:false,
								dorsal:false,
								greenfield_rooftop:false,
								altura_edificacion:0,
								ubicacion_equipos:0,
								tipo_torre:0,
								altura_torre:0,
								anio_construccion:(new Date()).getFullYear(),
								camuflaje:0,
								prioridad:4,
								fecha_inicio_contrato:new Date(),
								fecha_fin_contrato:new Date(),
								area_piso:0,
								factor_uso_torre:0,
								coubicador:0,
								nombre_sitio_coubicador:"",
								codigo_torrera:"",
								operador_coubicante:0,
								nombre_sitio_coubicante:"",
								estado:1,
								fecha_estado_on_air:new Date(),
								proyecto:{id:0},
								contrata_constructora:{id:0},
								tipo_solucion:{id:0},
								proveedor_mantenimiento:{id:0},
								consideracion_acceso:"",
								acceso_libre_24h:0,
								nivel_riesgo:0,
								consideraciones:""
								};
			ref.sitioEliminar = {};

			ref.departamentos = [];
			ref.provincias = [];
			ref.distritos = [];

			ref.ubicaciones = [];
			ref.tipos_torre = [];
			ref.camuflajes = [];
			ref.coubicadores = [];
			ref.operadores_coubicante = [];
			ref.proyectos = [];
			ref.constructoras = [];
			ref.soluciones = [];
			ref.proveedores_mantenimiento = [];

			listar = function(limit,offset){
				$http({
					url:"<?php echo base_url("index.php/status_site/listar");?>"+"/"+limit+"/"+offset,
					method:"GET"
				}).success(function(data){
					cantidadSitios = ref.sitios.length;
					data.forEach(function(sitio,ind,data){

						ref.sitios[cantidadSitios+ind] = sitio;
					});
				}).error(function(err){
					console.log(err);
				});
			};

			listar_zonas = function(){
				$http({
					url:"<?php echo base_url("index.php/zonas/listar/departamentos");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.departamentos = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			ref.listar_provincias = function(departamento){
				$http({
					url:"<?php echo base_url("index.php/zonas/listar/provincias");?>"+"/"+encodeURI(departamento),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.provincias = data
						ref.distritos = [];
					}
				}).error(function(err){

				});
			}

			ref.listar_distritos = function(departamento,provincia){
				$http({
					url:"<?php echo base_url("index.php/zonas/listar/distritos");?>"+"/"+encodeURI(departamento)+"/"+encodeURI(provincia),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.distritos = data
					}
				}).error(function(err){

				});
			}

			listar_ubicaciones = function(){
				$http({
					url:"<?php echo base_url("index.php/ubicacion_equipos/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.ubicaciones = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_tipo_torre = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_torre/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipos_torre = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_camuflajes = function(){
				$http({
					url:"<?php echo base_url("index.php/camuflaje/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.camuflajes = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_coubicadores = function(){
				$http({
					url:"<?php echo base_url("index.php/coubicador/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.coubicadores = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_coubicantes = function(){
				$http({
					url:"<?php echo base_url("index.php/coubicante/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.operadores_coubicante = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_proyectos = function(){
				$http({
					url:"<?php echo base_url("index.php/proyecto/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.proyectos = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_contructoras = function(){
				$http({
					url:"<?php echo base_url("index.php/contrata_constructora/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.constructoras = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_soluciones = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_solucion/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.soluciones = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_proveedores_mantenimiento = function(){
				$http({
					url:"<?php echo base_url("index.php/proveedor_mantenimiento/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.proveedores_mantenimiento = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			ref.editar = function(sitio){
				$("#editarSitio").bPopup();

				$http({
					url:"<?php echo base_url("index.php/status_site/sitio");?>"+"/"+sitio.id,
					method:"GET"
				}).success(function(data){
					if(data)
					{
						ref.listar_provincias(data[0].zona_peru.departamento);
						ref.listar_distritos(data[0].zona_peru.departamento,data[0].zona_peru.provincia);

						data[0].latitud = Number(data[0].latitud);
						data[0].longitud = Number(data[0].longitud);
						data[0].in_building = Boolean(Number(data[0].in_building));
						data[0].agregador = Boolean(Number(data[0].agregador));
						data[0].dorsal = Boolean(Number(data[0].dorsal));
						data[0].greenfield_rooftop = Boolean(Number(data[0].greenfield_rooftop));
						data[0].altura_edificacion = Number(data[0].altura_edificacion);
						data[0].altura_torre = Number(data[0].altura_torre);
						data[0].anio_construccion = Number(data[0].anio_construccion);
						data[0].fecha_estado_on_air = new Date(data[0].fecha_estado_on_air + " 00:00:00");
						data[0].fecha_inicio_contrato = new Date(data[0].fecha_inicio_contrato + " 00:00:00");
						data[0].fecha_fin_contrato = new Date(data[0].fecha_fin_contrato + " 00:00:00");
						data[0].area_piso = Number(data[0].area_piso);
						data[0].factor_uso_torre = Number(data[0].factor_uso_torre);
						data[0].acceso_libre_24h = Boolean(Number(data[0].acceso_libre_24h));

						ref.sitioEditar = data[0];
					}
				}).error(function(err){

				});
			}

			ref.grabar = function(){
				var campos = Object.keys(ref.sitioEditar),
					data = "";

				campos.forEach(function(campo,ind,objeto){
					if(!( (campo != "fecha_estado_on_air") && (campo != "fecha_inicio_contrato") && (campo != "fecha_fin_contrato") ))
					{
						data += campo+"="+ref.sitioEditar[campo].toISOString();
					}
					else
					{
						if(!( campo != "zona_peru" ))
						{
							data += "departamento="+ref.sitioEditar[campo]["departamento"]+"&";
							data += "provincia="+ref.sitioEditar[campo]["provincia"]+"&";
							data += "distrito="+ref.sitioEditar[campo]["distrito"];
						}
						else
						{
							if( (campo == "coubicador") && (ref.sitioEditar[campo] != null) )
							{
								data += "coubicador="+ref.sitioEditar[campo]["id"]+"&";
							}
							else
							{
								if( (campo == "ubicacion_equipos") && (ref.sitioEditar[campo] != null) )
								{
									data += "ubicacion_equipos="+ref.sitioEditar[campo]["id"]+"&";
								}
								else
								{
									if( (campo == "tipo_torre") && (ref.sitioEditar[campo] != null) )
									{
										data += "tipo_torre="+ref.sitioEditar[campo]["id"]+"&";
									}
									else
									{
										if( (campo == "proyecto") && (ref.sitioEditar[campo] != null) )
										{
											data += "proyecto="+ref.sitioEditar[campo]["id"]+"&";
										}
										else
										{
											if( (campo == "contrata_constructora") && (ref.sitioEditar[campo] != null) )
											{
												data += "contrata_constructora="+ref.sitioEditar[campo]["id"]+"&";
											}
											else
											{
												if( (campo == "tipo_solucion") && (ref.sitioEditar[campo] != null) )
												{
													data += "tipo_solucion="+ref.sitioEditar[campo]["id"]+"&";
												}
												else
												{
													if( (campo == "proveedor_mantenimiento") && (ref.sitioEditar[campo] != null) )
													{
														data += "proveedor_mantenimiento="+ref.sitioEditar[campo]["id"]+"&";
													}
													else
													{
														if( (campo == "camuflaje") && (ref.sitioEditar[campo] != null) )
														{
															data += "camuflaje="+ref.sitioEditar[campo]["id"]+"&";
														}
														else
														{
															if( (campo == "operador_coubicante") && (ref.sitioEditar[campo] != null) )
															{
																data += "operador_coubicante="+ref.sitioEditar[campo]["id"]+"&";
															}
															else
															{
																data += campo+"="+ref.sitioEditar[campo];
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}

					if(ind <= (objeto.length - 2))
					{
						data += "&";
					}
				});

				$http({
					url:"<?php echo base_url("index.php/status_site/sitio");?>"+"/"+ref.sitioEditar.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					console.log(resp);
					if(!(data.error))
					{
						listar(50,0);
						ref.sitioNuevo = {};
						$("#crearSitio").bPopup().close();
					}
					else
					{
						ref.error.error = true;
						ref.error.actualizar = data.error;
					}
				})
				.error(function(err){

				});
			}

			ref.crear = function(){
				var campos = Object.keys(ref.sitioNuevo),
					data = "";

				campos.forEach(function(campo,ind,objeto){
					if(!( (campo == "fecha_estado_on_air") || (campo == "fecha_inicio_contrato") || (campo == "fecha_fin_contrato") ))
					{
						data += campo+"="+ref.sitioNuevo[campo];
					}
					else
					{
						data += campo+"="+ref.sitioNuevo[campo].toISOString();
					}

					if(ind <= (objeto.length - 2))
					{
						data += "&";
					}
				});

				$http({
					url:"<?php echo base_url("index.php/status_site/sitio");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(!(data.error))
					{
						listar(50,0);
						ref.sitioNuevo = {};
						$("#crearSitio").bPopup().close();
					}
					else
					{
						ref.error.error = true;
						ref.error.crear = data.error;
					}
				})
				.error(function(err){

				});
			}

			ref.cancelarCreacion = function(){
				ref.sitioNuevo = {};
				$("#crearSitio").bPopup().close();
			}

			ref.eliminando = function(sitio){
				ref.sitioEliminar = sitio;
				$("#eliminarSitio").bPopup();
			}

			ref.eliminar = function(){
				$http({
					url:"<?php echo base_url("index.php/sitios/sitio");?>"+"/"+ref.sitioEliminar.id,
					method:"DELETE"
				}).success(function(data){
					listar();
					ref.sitioEliminar = {};
					$("#eliminarSitio").bPopup().close();
				}).error(function(err){

				});
			}

			ref.cancelarEliminacion = function(){
				ref.sitioEliminar = {};
				$("#eliminarSitio").bPopup().close();
			}

			ref.cargar_mas = function(){
				offset += limit;

				listar(limit, offset);
			}

			ref.listener_coubicador = function(id){
				if(id < 2)
				{
					ref.sitioEditar.nombre_sitio_coubicador = "N/A";
					ref.sitioEditar.codigo_torrera = "N/A";
				}
			}

			ref.listener_coubicante = function(id){
				if(id < 2)
				{
					ref.sitioEditar.nombre_sitio_coubicante = "N/A";
				}
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};

			listar(50,0);
			listar_zonas();
			listar_ubicaciones();
			listar_tipo_torre();
			listar_camuflajes();
			listar_coubicadores();
			listar_coubicantes();
			listar_proyectos();
			listar_contructoras();
			listar_soluciones();
			listar_proveedores_mantenimiento();
		}]);
</script>
</body>
</html>