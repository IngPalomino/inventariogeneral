<script type="text/javascript">
	$( function() {
		$('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
	} );
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session  = <?php echo json_encode($session);?>;
			ref.id_mso   = <?php echo json_encode($id_mso) ;?>;
			ref.id_sitio = <?php echo json_encode($id_sitio) ;?>;

			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargando = true;
			ref.procesando = false;

			ref.sitios = [];
			ref.almacenes = [];
			ref.config = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Buscar sitio',
				maxItems: 1
			};

			ref.aires_acondicionados = [];
			ref.nuevoAire = {};
			ref.eliminar_Aire = {};
			ref.almacenar_Aire = {};
			ref.tipo_aires_acondicionados = [];
			ref.tipo_compresor = [];
			ref.tipo_condensador = [];
			ref.tipo_evaporador = [];
			ref.tipo_refrigerante = [];

            //navbar
			buscarSala = function(){
				 //console.log("test"+ref.id_mso);
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("sala_tecnica/listar_sala")?>"+"/"+ ref.id_mso,
					method:"GET"
				}).success(function(data){
					ref.cargando = false;
					if(data.length)
					{
						ref.salas = data;
					 
					 // console.log(ref.salas[0]['nombre']);
					  ref.salaname = ref.salas[0]['nombre'];
					}
					else
					{
						
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});
			}
			
           busca_items = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("mso_categoria_tipo/get_all")?>"+"/",
					method:"GET"
				}).success(function(data){
					//console.log(data);
					ref.cargando = false;
					if(data.length)
					{

					ref.dataItem = data;					
					
					}
					else
					{
						
					}             

				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});				
                }
             //navbar

			listarAiresAcondicionados = function(){
				$http({
					url:"<?php echo base_url("index.php/aires_acondicionados/listar_sitio_mso");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.aires_acondicionados = data;
						
						data.forEach(function(dato,ind,objeto){
							try { objeto[ind].fecha_instalacion = new Date(objeto[ind].fecha_instalacion + " 00:00:00"); }catch(err){ objeto[ind].fecha_instalacion = null;}
							try { objeto[ind].created_at = Date.parse(objeto[ind].created_at); }catch(err){ objeto[ind].created_at = null;}
							
							objeto[ind].logs_aires_acondicionados.forEach(function(modificacion,indx,objetox){
								objetox[indx].created_at = Date.parse(objetox[indx].created_at);
							});
						});

						ref.aires_acondicionados.forEach(function(aire,indx,objx){
							objx[indx].disabled = function(){
								var dis = false;

								if(this.tipo_aires_acondicionados.id == "" || this.tipo_compresor.id == "" || this.tipo_condensador == "" || this.tipo_evaporador == "")
								{
									dis = true;
								}

								return dis;
							}
						});
						
						ref.cargando = false;
					}
					else
					{
						ref.cargando = false;
					}
					/*console.log(data);*/
				}).error(function(err){
					ref.cargando = false;
				});
			}
			
			listarTipoAiresAcondicionados = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_aires_acondicionados/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_aires_acondicionados = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoCompresor = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_compresor/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_compresor = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoCondensador = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_condensador/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipo_condensador = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoEvaporador = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_evaporador/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipo_evaporador = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarTipoRefrigerante = function(){
				$http({
					url:"<?php echo base_url("tipo_refrigerante/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipo_refrigerante = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarSitios = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar_sitios");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sitios = data;
					}
					else
					{
						ref.sitios = [];
					}
				}).error(function(err){

				});
			}
			
			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.almacenes = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			ref.nuevo_aire = function(){
				$("#nuevoAire").bPopup();
			}
			
			ref.cancelarCreacionAire = function(){
				$("#nuevoAire").bPopup().close();
				ref.nuevoAire = {};
			}
			
			ref.agregarAire = function(aire){
				var data = "",
					campos = Object.keys(aire),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(aire[campo] != null)
						{
							if(typeof aire[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+aire[campo];
							}
							else
							{
								if(campo == "fecha_instalacion")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+aire[campo].toISOString();
								}
								else
								{
									if(aire[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(aire[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+aire[campo].id;
									}
										
								}
								data += "&id_sala_tecnica"+"="+ref.id_mso;
							}

							contador++;
						}
					}
				});
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("index.php/aires_acondicionados/aire_acondicionado/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					ref.nuevoAire = {};
					$("#nuevoAire").bPopup().close();
					listarAiresAcondicionados();
				}).error(function(err){

				});
			}
			
			ref.eliminandoAire = function(info){
				ref.eliminar_Aire = info;
				$("#eliminarAire").bPopup();
			}
			
			ref.cancelarEliminacion = function(){
				$("#eliminarAire").bPopup().close();
				ref.eliminar_Aire = {};
			}
			
			ref.eliminarAire = function(){
				var data = "comentario="+ref.eliminar_Aire.comentario;

				$http({
					url:"<?php echo base_url("index.php/aires_acondicionados/aire_acondicionado");?>"+"/"+ref.eliminar_Aire.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.aires_acondicionados = [];
						listarAiresAcondicionados();
						ref.eliminar_Aire = {};
						$('#eliminarAire').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.editarAire = function(aire){
				aire.editar = true;
			}
			
			ref.grabarAire = function(aire){console.log(aire);
				borrarPropiedades(aire,"logs_aires_acondicionados");
				ref.procesando = true;
				
				var data = "",
					campos = Object.keys(aire),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(aire[campo] != null)
						{
							if(typeof aire[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+aire[campo];
							}
							else
							{
								if(campo == "fecha_instalacion")
								{
									if(contador > 0)
									{
										data += "&";
									}
									try{ data += campo+"="+aire[campo].toISOString(); }
									catch(err){ data += campo+"=null"; }
								}
								else
								{
									if(aire[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(aire[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+aire[campo].id;
									}
										
								}
							}

							contador++;
						}
					}
				});
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("index.php/aires_acondicionados/aire_acondicionado");?>/"+aire.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					aire.editar = false;
					listarAiresAcondicionados();
					ref.procesando = false;
				}).error(function(err){
					ref.procesando = false;
				});
			}

			ref.cancelarAire = function(aire){
				aire.editar = false;
				listarAiresAcondicionados();
			}
			
			ref.elegirCapacidad = function(id,aire){
				ref.tipo_aires_acondicionados.modelos.forEach(function(modelo){
					if(modelo.id == id)
					{
						aire.tipo_aires_acondicionados.capacidad = modelo.capacidad;
						
					}
				});
			}
			
			ref.elegirCapacidadNuevo = function(id,nuevoAire){
				ref.tipo_aires_acondicionados.modelos.forEach(function(modelo){
					if(modelo.id == id)
					{
						ref.nuevoAire.tipo_aires_acondicionados.capacidad = modelo.capacidad;
						
					}
				});
			}
			
			ref.cambioTipo = function(tipo_equipo,aire){
				if(tipo_equipo == "Mochila")
				{
					aire.tipo_condensador = 1;
					aire.tipo_evaporador = 1;
					aire.num_serie_condensador = 'N/A';
					aire.num_serie_evaporador = 'N/A';
					aire.num_serie_aire_acon = '';
				}
				else
				{
					aire.tipo_condensador = "";
					aire.tipo_evaporador = "";
					aire.num_serie_condensador = '';
					aire.num_serie_evaporador = '';
					aire.num_serie_aire_acon = 'N/A';
				}
			}
			
			ref.cambioTipoNuevo = function(tipo_equipo,nuevoAire){
				if(tipo_equipo == "Mochila")
				{
					ref.nuevoAire.tipo_condensador = 1;
					ref.nuevoAire.tipo_evaporador = 1;
					ref.nuevoAire.num_serie_condensador = 'N/A';
					ref.nuevoAire.num_serie_evaporador = 'N/A';
					ref.nuevoAire.num_serie_aire_acon = '';
				}
				else
				{
					ref.nuevoAire.tipo_condensador = "";
					ref.nuevoAire.tipo_evaporador = "";
					ref.nuevoAire.num_serie_condensador = '';
					ref.nuevoAire.num_serie_evaporador = '';
					ref.nuevoAire.num_serie_aire_acon = 'N/A';
				}
			}

			ref.almacenandoAire = function(aire){
				ref.almacenar_Aire = aire;
				ref.almacenar_Aire.tipo_almacen = 1;
				$("#almacenarAire").bPopup();
			}

			ref.almacenarAire = function(aire) {
				var data = "tipo_refrigerante="+((aire.tipo_refrigerante != null)? aire.tipo_refrigerante.id : null)
							+"&tipo_aires_acondicionados="+aire.tipo_aires_acondicionados.id
							+"&num_serie_aire_acon="+aire.num_serie_aire_acon
							+"&tipo_compresor="+aire.tipo_compresor.id
							+"&tipo_condensador="+aire.tipo_condensador.id
							+"&num_serie_condensador="+aire.num_serie_condensador
							+"&tipo_evaporador="+aire.tipo_evaporador.id
							+"&num_serie_evaporador="+aire.num_serie_evaporador
							+"&estado="+aire.estado
							+"&proveniente_sitio="+aire.sitio
							+"&tipo_almacen="+aire.tipo_almacen
							+"&almacen="+((aire.tipo_almacen == 1)? aire.almacen : aire.a_sitio)
							+"&comentario="+aire.comentario;
				/*console.log(data);*/
				$http({
					url: "<?php echo base_url("aires_acondicionados/aire_acondicionado/almacenar"); ?>/"+aire.id,
					method: "POST",
					data: data,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function successCallback(response) {
					ref.aires_acondicionados = [];
					listarAiresAcondicionados();
					$("#almacenarAire").bPopup().close();
				}, function errorCallback(response) {

				});
			}

			ref.cancelarAlmacenamiento = function(){
				ref.almacenar_Aire = {};
				$("#almacenarAire").bPopup().close();
			}

			ref.opcionSitio = function(){
				ref.almacenar_Aire.tipo_almacen = 2;
			}

			ref.opcionAlmacen = function(){
				ref.almacenar_Aire.tipo_almacen = 1;
			}
			
			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}

			listarAiresAcondicionados();
			listarTipoAiresAcondicionados();
			listarTipoCompresor();
			listarTipoCondensador();
			listarTipoEvaporador();
			listarTipoRefrigerante();
			listarSitios();
			listarAlmacenes();			
			buscarSala();						
			busca_items();

		}]);
</script>
</body>
</html>