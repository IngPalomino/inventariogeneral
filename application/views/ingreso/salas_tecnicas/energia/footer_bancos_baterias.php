<script type="text/javascript">
	$( function() {
		$('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
	} );
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.id_mso = <?php echo json_encode($id_mso) ;?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargando = false;
			ref.procesando = false;
			ref.procesandoRegistro = false;
			ref.sitios = [];
			ref.almacenes = [];
			ref.config = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Buscar sitio',
				maxItems: 1
			};
			
			ref.bancos_baterias = [];
			ref.rectificadores = [];
			ref.tipo_bancos_baterias = [];
			
			ref.nuevoBanco = {
				 cantidad_bancos: ""
			};						
            //id_sala_tecnica: ref.id_mso
			ref.eliminar_Banco = {};
			ref.almacenar_Banco = {};
			
			listarBancosBaterias = function(){
				ref.procesandoRegistro = true;
				$http({
					url:"<?php echo base_url("bancos_baterias/listar_sitio_mso");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					console.log(data);
					if(data.length)
					{
						data.forEach(function(dato,ind,objeto){
							objeto[ind].bancos.forEach(function(banco,indy,objetoy){
								objetoy[indy].celdas_controlador_rectif_bancos_baterias.forEach(function(celda,indz,objetoz){
									try{ objetoz[indz].fecha_instalacion = new Date(objetoz[indz].fecha_instalacion + " 00:00:00"); }
									catch(err){ objetoz[indz].fecha_instalacion = null; }
								});
								
								objetoy[indy].logs_controlador_rectif_bancos_baterias.forEach(function(modificacion,indz,objetoz){
									try{ objetoz[indz].created_at = new Date(objetoz[indz].created_at); }
									catch(err){ objetoz[indz].created_at = null; }
								});
							
								try{ objetoy[indy].created_at = new Date(objetoy[indy].created_at); } catch(err){ objetoy[indy].created_at = null; }
								try{ objetoy[indy].fecha_instalacion = new Date(objetoy[indy].fecha_instalacion + " 00:00:00"); } catch(err){ objetoy[indy].fecha_instalacion = null; }
							});
						});
						
						ref.bancos_baterias = data;
						
						ref.bancos_baterias.forEach(function(rectif,indx,objx){
							objx[indx].bancos.forEach(function(banco,indz,objz){
								objz[indz].disabled = function(){
									var dis = false;
									
									if(objz[indz].tipo_bancos_baterias.id == "" || objz[indz].fecha_instalacion == "")
									{
										dis = true;
									}
									
									objz[indz].celdas_controlador_rectif_bancos_baterias.forEach(function(celda,indy,objy){
										if(objy[indy].fecha_instalacion == "" || objy[indy].numero_serie == "")
										{
											dis = true;
										}
									});
									return dis;
								}
							});
						});
						
						ref.procesandoRegistro = false;
					}
					else
					{
						ref.procesandoRegistro = false;
					}
				}).error(function(err){
					ref.procesandoRegistro = false;
				});
			}
			
			
			listarRectificadores = function(){
				$http({
					url:"<?php echo base_url("rectificadores/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET",
					headers:{'X-Requested-With' : 'XMLHttpRequest'}
				}).success(function(data){
					if(data.length)
					{
						ref.rectificadores = data;
					}
					else
					{

					}
					/*console.log(data);*/
				}).error(function(err){

				});
			}

			
			listarTipoBancosBaterias = function(){
				$http({
					url:"<?php echo base_url("tipo_bancos_baterias/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_bancos_baterias = data;
					}
					else
					{

					}
					/*console.log(data);*/
				}).error(function(err){

				});
			}

			listarSitios = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar_sitios");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sitios = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.almacenes = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			ref.nuevo_banco = function(){
				$("#nuevoBanco").bPopup();
			}
			

			
			ref.agregarBanco = function(banco){
				var data = "",
					campos = Object.keys(banco),
					contador = 0;
              var id_sala_tecnica = $("#id_sala_tecnica").val();
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled" && campo != "id_sala_tecnica")
					{
						if(banco[campo] != null)
						{
							if(typeof banco[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+banco[campo];
								
							}
							else
							{
								if(campo == "fecha_instalacion")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+banco[campo].toISOString();
									
								}
								else
								{
									if(banco[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(banco[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+banco[campo].id;
									}
										
								}

								data += "&id_sala_tecnica"+"="+ref.id_mso;
							}

							contador++;
						}
					}
				});	

							
				//console.log(data);
                $("#nuevoBanco").bPopup().close();
                ref.procesandoRegistro = true;
				$http({
					url:"<?php echo base_url("bancos_baterias/banco_bateria/sitio");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					//$("#nuevoBanco").bPopup().close();
					ref.procesandoRegistro = false;
					listarBancosBaterias();
					ref.nuevoBanco = {};
				}).error(function(err){

				});
			}
			
			ref.cancelarCreacionBanco = function(){
				$("#nuevoBanco").bPopup().close();
				ref.nuevoBanco = {};
			}
			
			ref.editarBanco = function(banco){
				banco.editar = true;
				banco.mostrar = true;
			}
			
			ref.grabarBanco = function(banco){
				borrarPropiedades(banco,"logs_controlador_rectif_bancos_baterias");
				ref.procesando = true;
				
				var data = "",
					campos = Object.keys(banco),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(banco[campo] != null)
						{
							if(typeof banco[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+banco[campo];
							}
							else
							{
								if(campo == "fecha_instalacion")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+banco[campo].toISOString();
								}
								else
								{
									if(banco[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(banco[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+banco[campo].id;
									}
										
								}
							}

							contador++;
						}
					}
				});
				console.log(data);
				$http({
					url:"<?php echo base_url("bancos_baterias/banco_bateria");?>/"+banco.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					banco.editar = false;
					listarBancosBaterias();
					ref.procesando = false;
				}).error(function(err){
					ref.procesando = false;
				});
			}
			
			ref.cancelarBanco = function(banco){
				banco.editar = false;
				listarBancosBaterias();
			}
			
			ref.eliminandoBanco = function(info){
				ref.eliminar_Banco = info;
				$("#eliminarBanco").bPopup();
			}
			
			ref.cancelarEliminacion = function(){
				$("#eliminarBanco").bPopup().close();
				ref.eliminar_Banco = {};
			}
			
			ref.eliminarBanco = function(){
				var data = "comentario="+ref.eliminar_Banco.comentario;

				$http({
					url:"<?php echo base_url("bancos_baterias/banco_bateria");?>"+"/"+ref.eliminar_Banco.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){console.log(data);
					if(data.error)
					{

					}
					else
					{
						ref.bancos_baterias = [];
						listarBancosBaterias();
						ref.eliminar_Banco = {};
						$('#eliminarBanco').bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.mostrarCeldas = function(banco){
				banco.mostrar = true;
			}
			
			ref.ocultarCeldas = function(banco){
				banco.mostrar = false;
			}
			
			ref.observacionCeldas = function(celda){
				celda.observacion = true;
			}
			
			ref.eliminarObservacionCelda = function(celda){
				celda.observacion = false;
			}

			ref.almacenandoBanco = function(banco){
				ref.almacenar_Banco = banco;
				ref.almacenar_Banco.tipo_almacen = 1;
				$("#almacenarBanco").bPopup();
			}
			ref.almacenarBanco = function(banco){
				var data = "tipo_bancos_baterias="+banco.tipo_bancos_baterias.id
						+"&confiabilidad="+banco.confiabilidad
						+"&celdas_banco_baterias="+angular.toJson(banco.celdas_controlador_rectif_bancos_baterias)
						+"&estado="+banco.estado
						+"&proveniente_sitio="+banco.sitio
						+"&tipo_almacen="+banco.tipo_almacen
						+"&almacen="+((banco.tipo_almacen == 1)? banco.almacen : banco.a_sitio)
						+"&comentario="+banco.comentario
						+"&id_sala_tecnica="+ref.id_mso;
				console.log(data);
				$http({
					url:"<?php echo base_url("bancos_baterias/banco_baterias/almacenar");?>/"+banco.id,
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#almacenarBanco").bPopup().close();
					ref.bancos_baterias = [];
					listarBancosBaterias();
					ref.almacenar_Banco = {};
				}).error(function(err){

				});
			}
			
			ref.cancelarAlmacenamiento = function(){
				ref.almacenar_Banco = {};
				$("#almacenarBanco").bPopup().close();
			}

			ref.bancoOpcionSitio = function(){
				ref.almacenar_Banco.tipo_almacen = 2;
			}

			ref.bancoOpcionAlmacen = function(){
				ref.almacenar_Banco.tipo_almacen = 1;
			}
			
			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}
            //navbar
			buscarSala = function(){            	
            	
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("sala_tecnica/listar_sala")?>"+"/"+ ref.id_mso,
					method:"GET"
				}).success(function(data){
					ref.cargando = false;
					if(data.length)
					{
						ref.salas = data;
						//console.log(ref.salas[0]['nombre']);
					  ref.salaname = ref.salas[0]['nombre'];
					}
					else
					{
						
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});
			}
            busca_items = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("mso_categoria_tipo/get_all")?>"+"/",
					method:"GET"
				}).success(function(data){
					//console.log(data);
					ref.cargando = false;
					if(data.length)
					{
					ref.dataItem = data;					
					
					}
					else
					{
						
					}              

				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});				
                }

             //navbar		
			buscarSala();						
			busca_items();
			listarBancosBaterias();
			listarRectificadores();
			listarTipoBancosBaterias();
			listarSitios();
			listarAlmacenes();
		}])
		.directive('bsPopover',function(){
			return function(scope,element,attrs){
				element.find("span[rel=popover]").popover();
			}
		});
</script>
</body>
</html>