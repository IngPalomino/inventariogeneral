<script type="text/javascript">
	angular
	.module('app',['selectize'])
	.controller('controlador', ["$http",function($http){
		var ref = this;
		ref.formLogIn = {};
		ref.formLogIn.usuario = "";
		ref.formLogIn.contrasena = "";
		ref.reestablecerContrasena = {};

		ref.error = {};
		ref.error.error = false;
		ref.error.logIn = "";
		ref.session = <?php echo json_encode($session);?>;
		ref.id_mso = <?php echo json_encode($id_mso) ;?>;
		ref.idSitio = <?php echo json_encode($id_sitio) ;?>;

		ref.auth = {};
		ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
		ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
		ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
		ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
		
		ref.cargando = false;
		ref.procesando = false;
		ref.procesandoRegistro = false;
		ref.oldSitio = "";		
		ref.config = {
			valueField: 'id',
			labelField: 'nombre_completo',
			searchField: 'nombre_completo',
			delimiter: '|',
			placeholder: 'Buscar sitio',
			maxItems: 1
		};
		    ref.ups = [];
			ref.nuevoUps = {};
			ref.eliminar_Ups = {};
			ref.almacenar_Ups = {};
			ref.tipo_ups = [];
			ref.sitios = [];
			ref.almacenes = [];

		ref.bancos_baterias = [];
		ref.tipo_bancos_baterias = [];

		listarUps = function(){
			$http({
				url:"<?php echo base_url("index.php/ups/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
				method:"GET"
			}).success(function(data){
				if(data.length)
				{
					ref.ups = data;
				//console.log(data);					
					data.forEach(function(dato,ind,objeto){
						try { objeto[ind].created_at = Date.parse(objeto[ind].created_at); }catch(err){ objeto[ind].created_at = null;}
						
						objeto[ind].logs_ups.forEach(function(modificacion,indx,objetox){
							objetox[indx].created_at = Date.parse(objetox[indx].created_at);
						});
					});
					
					ref.ups.forEach(function(ups,indx,objx){
						objx[indx].disabled = function(){
							var dis = false;						
							if(this.tipo_ups.id == "")
							{
								dis = true;
							}								
							return dis;
						}
						
					});						
					ref.cargando = false;
				}
				else
				{
					ref.cargando = false;
				}
			}).error(function(err){

			});
		}	
		
		listarTipoUps = function(){
			$http({
				url:"<?php echo base_url("index.php/tipo_ups/listar");?>",
				method:"GET"
			}).success(function(data){
				if(data.marcas.length)
				{
					ref.tipo_ups = data;
				}
				else
				{

				}
			}).error(function(err){

			});
		}	

		listarTrasformador = function(){		       
			$http({
				url:"<?php echo base_url("index.php/tipo_transformador/listar");?>",
				method:"GET"
			}).success(function(data){
				if(data.length)
				{
					ref.tipo_trasformador = data;		
				}
				else
				{

				}
			}).error(function(err){

			});
		}

		listarBancosBaterias = function(){
			ref.procesandoRegistro = true;
				$http({
					url:"<?php echo base_url("bancos_baterias/listar_sitio_mso");?>"+"/"+ref.idSitio,
					method:"GET"
				}).success(function(data){
					//console.log(data);
					if(data.length)
					{
						data.forEach(function(dato,ind,objeto){
							objeto[ind].bancos.forEach(function(banco,indy,objetoy){
								objetoy[indy].celdas_controlador_rectif_bancos_baterias.forEach(function(celda,indz,objetoz){
									try{ objetoz[indz].fecha_instalacion = new Date(objetoz[indz].fecha_instalacion + " 00:00:00"); }
									catch(err){ objetoz[indz].fecha_instalacion = null; }
								});
								
								objetoy[indy].logs_controlador_rectif_bancos_baterias.forEach(function(modificacion,indz,objetoz){
									try{ objetoz[indz].created_at = new Date(objetoz[indz].created_at); }
									catch(err){ objetoz[indz].created_at = null; }
								});
							
								try{ objetoy[indy].created_at = new Date(objetoy[indy].created_at); } catch(err){ objetoy[indy].created_at = null; }
								try{ objetoy[indy].fecha_instalacion = new Date(objetoy[indy].fecha_instalacion + " 00:00:00"); } catch(err){ objetoy[indy].fecha_instalacion = null; }
							});
						});
						
					ref.bancos_baterias = data;						
					//	console.log(ref.bancos_baterias[0].bancos[0].tipo_bancos_baterias);
                     ref.item_banco = ref.bancos_baterias[0].bancos;
                      //console.log(ref.item_banco);
					  ref.procesandoRegistro = false;
					}
					else
					{
						ref.procesandoRegistro = false;
					}
				}).error(function(err){
					ref.procesandoRegistro = false;
			});
		}

		listarSitios = function(){
			$http({
				url:"<?php echo base_url("almacenes/listar_sitios");?>",
				method:"GET"
			}).success(function(data){
				if(data.length)
				{
					ref.sitios = data;
				}
				else
				{

				}
			}).error(function(err){

			});
		}

		listarAlmacenes = function(){
			$http({
				url:"<?php echo base_url("almacenes/listar");?>",
				method:"GET"
			}).success(function(data){
				if(data.length)
				{
					ref.almacenes = data;
				}
				else
				{

				}
			}).error(function(err){

			});
		}

		ref.nuevo_ups = function(){
			$("#nuevoUps").bPopup();
		}

		ref.cancelarCreacionUps = function(){
			$("#nuevoUps").bPopup().close();
			ref.nuevoUps = {};
		}

		ref.agregarUps = function(ups){
			var data = "",
			campos = Object.keys(ups),
			contador = 0;				
			campos.forEach(function(campo,ind,objeto){
				if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
				{
					if(ups[campo] != null)
					{
						if(typeof ups[campo] != "object")
						{
							if(contador > 0)
							{
								data += "&";
							}
							data += campo+"="+ups[campo];
						}
						else
						{
							if(campo == "fecha_servicio")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+ups[campo].toISOString();
							}
							else
							{
								if(ups[campo].length != undefined)
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+JSON.stringify(ups[campo]);
								}
								else
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+ups[campo].id;
								}

							}
							data += "&id_sala_tecnica"+"="+ref.id_mso;
						}

						contador++;
					}
				}
			});
			/*console.log(data);*/
			 $("#nuevoUps").bPopup().close();
              ref.procesandoRegistro = true;				
			$http({
				url:"<?php echo base_url("index.php/ups/ups/sitio");?>"+"/"+"<?php echo $sitio;?>",
				method:"POST",
				data:data,
				headers:{'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(resp){
				//$("#nuevoUps").bPopup().close();
				ref.procesandoRegistro = false;
				listarUps();
				ref.nuevoUps = {};
			}).error(function(err){

			});

		}		


		ref.eliminandoUps = function(info){
			ref.eliminar_Ups = info;
			$("#eliminarUps").bPopup();
		}

		ref.cancelarEliminacion = function(){
			$("#eliminarUps").bPopup().close();
			ref.eliminar_Ups = {};
		}

		ref.eliminarUps = function(){
			var data = "comentario="+ref.eliminar_Ups.comentario;
            $('#eliminarUps').bPopup().close();
            ref.procesandoRegistro = true;
			$http({
				url:"<?php echo base_url("index.php/ups/ups");?>"+"/"+ref.eliminar_Ups.id,
				method:"DELETE",
				data: data,
				headers:{'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data){
				ref.procesandoRegistro = false;
				if(data.error)
				{

				}
				else
				{
					ref.ups = [];
					listarUps();
					ref.eliminar_Ups = {};
					//$('#eliminarUps').bPopup().close();
				}
			}).error(function(err){

			});
		}


		ref.editarUps = function(ups)
		{
			ups.editar = true;
		}

		ref.grabarUps = function(ups){
			borrarPropiedades(ups,"mantenimientos_ups");
			borrarPropiedades(ups,"logs_ups");
			ref.procesando = true;

			var data = "",
			campos = Object.keys(ups),
			contador = 0;

			campos.forEach(function(campo,ind,objeto){
				if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
				{
					if(ups[campo] != null)
					{
						if(typeof ups[campo] != "object")
						{
							if(contador > 0)
							{
								data += "&";
							}
							data += campo+"="+ups[campo];
						}
						else
						{
							if(campo == "fecha_servicio")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+ups[campo].toISOString();
							}
							else
							{
								if(ups[campo].length != undefined)
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+JSON.stringify(ups[campo]);
								}
								else
								{
									if(contador > 0)
									{
										data += "&";
									}

									data += campo+"="+ups[campo].id;
								}

							}
						}

						contador++;
					}
				}
			});
			/*console.log(data);*/
			
			
			$http({
				url:"<?php echo base_url("index.php/ups/ups");?>/"+ups.id,
				method:"PUT",
				data:data,
				headers:{'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(resp){
				ups.editar = false;
				listarUps();
				ref.procesando = false;
			}).error(function(err){

			});
		}

		ref.cancelarUps = function(ups){
			ups.editar = false;
			listarUps();
		}	

		ref.almacenandoUps = function(info){
			ref.almacenar_Ups = info;
			//console.log(ref.almacenar_Ups);
			ref.almacenar_Ups.tipo_almacen = 1;
			console.log(ref.almacenar_Ups);
			 $("#almacenarUps").bPopup({
			 	amsl: 0
			 });
		}

		ref.almacenarUps = function(ups){                   
                //console.log(ups);
				var data = "tipo_ups="+ref.almacenar_Ups.tipo_ups.id
				+"&numero_serie="+ref.almacenar_Ups.numero_serie		
				+"&ubicacion="+ref.almacenar_Ups.ubicacion
				+"&capacidad="+ref.almacenar_Ups.capacidad
				+"&fases="+ref.almacenar_Ups.fases
				+"&subestacion="+ref.almacenar_Ups.subestacion
				+"&controlador_rectif_bancos_baterias="+ref.almacenar_Ups.controlador_rectif_bancos_baterias
				+"&voltaje_entrada="+ref.almacenar_Ups.voltaje_entrada
				+"&voltaje_salida="+ref.almacenar_Ups.voltaje_salida
				+"&estado="+ups.estado
				+"&proveniente_sitio="+ref.almacenar_Ups.sitio
				+"&almacen_previo="+ref.almacenar_Ups.almacen_previo
				+"&tipo_almacen="+((ref.almacenar_Ups.tipo_almacen == 1)? ups.almacen : ref.almacenar_Ups.a_sitio)
				+"&id_sala_tecnica="+ref.id_mso
				+"&comentario="+ups.comentario;
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("ups/ups/almacenar");?>/"+ref.idSitio,
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#almacenarUps").bPopup().close();
					ref.ups = [];
					listarUps();
					ref.almacenar_Ups = {};
				}).error(function(err){

				});
		}

		ref.cancelarAlmacenamientoUps = function(){
				$("#almacenarUps").bPopup().close();
				ref.almacenar_Ups = {};
			}

			ref.UpsOpcionSitio = function(){
				ref.almacenar_Ups.tipo_almacen = 2;
			}

			ref.UpsOpcionAlmacen = function(){
				ref.almacenar_Ups.tipo_almacen = 1;
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
				+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});
				return objeto;
			}
            //navbar
            buscarSala = function(){
            	ref.cargando = true;
            	$http({
            		url:"<?php echo base_url("sala_tecnica/listar_sala")?>"+"/"+ ref.id_mso,
            		method:"GET"
            	}).success(function(data){
            		ref.cargando = false;
            		if(data.length)
            		{
            			ref.salas = data;
						//console.log(ref.salas[0]['nombre']);
						ref.salaname = ref.salas[0]['nombre'];
					}
					else
					{
						
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});
			}			
			busca_items = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("mso_categoria_tipo/get_all")?>"+"/",
					method:"GET"
				}).success(function(data){
					//console.log(data);
					ref.cargando = false;
					if(data.length)
					{
						ref.dataItem = data;					
						
					}
					else
					{
						
					}             

				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});				
			}
             //navbar		
             buscarSala();						
             busca_items();			
             listarUps();			
             listarTipoUps();
             listarTrasformador();
			 listarBancosBaterias();		
			 listarSitios();
			 listarAlmacenes();
			}]);
		</script>
	</body>
	</html>