<script type="text/javascript">
	function base_url(segment = '') {
		return '<?php echo base_url(); ?>' + segment;
	}

	function serializeObj(obj) {
		var result = [];
		for (var property in obj) {
			if (property != 'id' && property != 'created_at' && typeof obj[property] != 'function') {
				if (Array.isArray(obj[property])) {
					result.push(encodeURIComponent(property) + '=' + angular.toJson(obj[property]));
				} else if (typeof obj[property] === 'object') {
					try { result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]['id'])); }
					catch(err) { result.push(encodeURIComponent(property) + '=' + null); }
				} else if (property == 'tipo_almacen') {
					if (obj[property] == 1) {
						result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
						result.push('almacen=' + encodeURIComponent(obj['almacen']));
					} else if (obj[property] == 2) {
						result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
						result.push('almacen=' + encodeURIComponent(obj['a_sitio']));
						delete obj['a_sitio'];
					}
				} else {
					result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
				}
			}
		}

		return result.join('&');
	}

	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.id_mso = <?php echo json_encode($id_mso);?>;
			ref.id_mso = <?php echo json_encode($id_mso);?>;

			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargando = false;
			ref.subestacion = [];
			
			// ref.config = {
			// 				create: true,
			// 				valueField: 'id',
			// 				labelField: 'conexion_linea_electrica',
			// 				delimiter: '|',
			// 				placeholder: 'Conexion(es)',
			// 				onInitialize: function(selectize){
			// 					// receives the selectize object as an argument
			// 				},
			// 				maxItems: 2
			// 			};

			ref.sitios = [];
			ref.almacenes = [];
			ref.configSitios = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Buscar sitio',
				maxItems: 1
			};

			// ref.nuevoSubestacion = {
			// 	conexiones_linea_electrica: [],
			// };
			ref.eliminar_Subestacion = {};
			// ref.nuevoPropietario = {};
			// ref.eliminar_Propietario = {};

			// ref.conexiones_linea_electrica = [];
			// ref.sistemas_linea_electrica = [];
			// ref.propietarios_linea_electrica = [];
			ref.tipo_transformador = [];
			// ref.tipo_transformix = [];
			
			// ref.linea_nuevo_propietario = {};
			ref.almacenarObjeto = {};

			listarSubestacion  = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("subestacion/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						data.forEach(function(dato,ind,objeto){
							try{ objeto[ind].fecha_servicio = new Date(objeto[ind].fecha_servicio + " 00:00:00"); }catch(err){ objeto[ind].fecha_servicio = null; }
							objeto[ind].distancia = Number(objeto[ind].distancia);
							objeto[ind].postes = Number(objeto[ind].postes);
							objeto[ind].potencia_transformador = objeto[ind].potencia_transformador ? Number(objeto[ind].potencia_transformador) : null;
							objeto[ind].potencia_transformix = objeto[ind].potencia_transformix ? Number(objeto[ind].potencia_transformix) : null;
							try{ objeto[ind].created_at = Date.parse(objeto[ind].created_at); }catch(err){ objeto[ind].created_at = null; }
							
							objeto[ind].conexiones_linea_electrica = [];
							objeto[ind].lineas_electricas_conexion_linea_electrica.forEach(function(conexion,indx,objx){
								objeto[ind].conexiones_linea_electrica.push(conexion.conexion_linea_electrica.id);
							});
							
							objeto[ind].logs_lineas_electricas.forEach(function(modificacion,indx,objetox){
								try{ objetox[indx].created_at = Date.parse(objetox[indx].created_at); }catch(err){ objetox[indx].created_at = null; }
							});

							objeto[ind].obj_transformador = {
								id: objeto[ind].id,
								proveniente_sitio: objeto[ind].sitio,
								elemento: 1,
								tipo_elemento: objeto[ind].tipo_transformador ? objeto[ind].tipo_transformador.id : null,
								numero_serie: objeto[ind].num_serie_transformador,
								potencia: objeto[ind].potencia_transformador,
								conexion: objeto[ind].conexion_transformador
							};

							objeto[ind].obj_transformix = {
								id: objeto[ind].id,
								proveniente_sitio: objeto[ind].sitio,
								elemento: 2,
								tipo_elemento: objeto[ind].tipo_transformix ? objeto[ind].tipo_transformix.id : null,
								numero_serie: objeto[ind].num_serie_transformix,
								potencia: objeto[ind].potencia_transformix,
								conexion: objeto[ind].conexion_transformix
							};
						});
						
						ref.lineas_electricas = data;
						ref.cargando = false;
						//console.log(ref.lineas_electricas);
					}
					else
					{
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			// listarSistemasLinea = function(){
			// 	$http({
			// 		url:"<?php //echo base_url("sistema_linea_electrica/listar");?>",
			// 		method:"GET"
			// 	}).success(function(data){
			// 		if(data.length)
			// 		{
			// 			ref.sistemas_linea_electrica = data;
			// 		}
			// 		else
			// 		{

			// 		}
			// 	}).error(function(err){

			// 	});
			// }

			// listarConexionesLinea = function(){
			// 	$http({
			// 		url:"<?php //echo base_url("conexion_linea_electrica/listar");?>",
			// 		method:"GET"
			// 	}).success(function(data){
			// 		if(data.length)
			// 		{
			// 			ref.conexiones_linea_electrica = data;
			// 		}
			// 		else
			// 		{

			// 		}
			// 	}).error(function(err){

			// 	});
			// }
			
			
		//implementaba abajo	
			listarTipoTransformador = function(){
				$http({
					url:"<?php echo base_url("tipo_transformador/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
					  ref.tipo_transformador = data;
						//console.log(data);
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarTipoTransformador_modelo = function(){				
				var $id = $("#tipo_transformador_marca").val();				
				$http({
					url:"<?php echo base_url("tipo_transformador/listar_modelo");?>/"+$id,
					method:"POST"
				}).success(function(data){
					if(data.length)
					{
					  ref.tipo_transformador_modelo = data;
					 // console.log(data);
					}
					else
					{
					}
				}).error(function(err){
				});
			}
			
			listarFases = function(){
				$http({
					url:"<?php echo base_url("fases/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
					  ref.fases = data;
						console.log(ref.fases);
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarSitios = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar_sitios");?>",
					method:"GET"
				}).then(function successCallback(resp) {
					if(resp.data.length) {
						ref.sitios = resp.data;
					} else {
						ref.sitios = [];
					}
				}, function errorCallback(resp) {

				});
			}

			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).then(function successCallback(resp) {
					if(resp.data.length) {
						ref.almacenes = resp.data;
					} else {
						ref.almacenes = [];
					}
				}, function errorCallback(resp) {

				});
			}

			ref.nuevo_subestacion = function(){				
				$('#formsubtacion')[0].reset();
				$("#nuevoSubestacion").bPopup();
			}

			ref.cancelarCreacionSubestacion = function(){
				$("#nuevoSubestacion").bPopup().close();
				ref.nuevoSubestacion = {};
			}

			ref.agregarSubestacion = function(subestacion){
				console.log(subestacion);
				var data = "",
					campos = Object.keys(subestacion),
					contador = 0;				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado")
					{
						if(subestacion[campo] != null)
						{
							if(typeof subestacion[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+subestacion[campo];
							}
							else
							{
								if(campo == "fecha_servicio")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+subestacion[campo].toISOString();
								}
								else
								{
									if(subestacion[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(subestacion[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+subestacion[campo].id;
									}
										
								}
							}

							contador++;
						}
					}
				});
				console.log(data);
				$http({
					//url:"<?php //echo base_url("index.php/subestacion/subestacion/sitio");?>/"+"<?php// echo $sitio;?>",
					url:"<?php echo base_url("subestacion/crear_sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevoSubestacion").bPopup().close();
					listarSubestacion();
				}).error(function(err){

				});
			}

			ref.eliminandoSubestacion = function(info){
				ref.eliminar_Subestacion = info;
				$("#eliminarSubestacion").bPopup();
			}

			ref.cancelarEliminacion = function(){
				$("#eliminarSubestacion").bPopup().close();
				ref.eliminar_Subestacion = {};
			}

			ref.eliminarSubestacion = function(){
				var data = "comentario="+ref.eliminar_Subestacion.comentario;

				$http({
					url:"<?php echo base_url("index.php/subestacion/subestacion");?>"+"/"+ref.eliminar_Subestacion.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.subestacion = [];
						listarSubestacion();
						ref.eliminar_Subestacion = {};
						$('#eliminarSubestacion').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.editarSubestacion = function(Subestacion){
				subestacion.editar = true;
			}

			ref.grabarSubestacion = function(Subestacion){console.log(subestacion);
				borrarPropiedades(subestacion,"$$hashKey");
				borrarPropiedades(subestacion,"created_at");
				var data = "",
					campos = Object.keys(subestacion),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado")
					{
						if(linea[campo] != null)
						{
							if(typeof subestacion[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+subestacion[campo];
							}
							else
							{
								if(campo == "fecha_servicio")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+subestacion[campo].toISOString();
								}
								else
								{
									if(subestacion[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+JSON.stringify(subestacion[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+subestacion[campo].id;
									}
										
								}
							}

							contador++;
						}
					}
				});
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("index.php/subestacion/subestacion");?>/"+subestacion.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					subestacion.editar = false;
					listarSubestacion();
				}).error(function(err){

				});
			}

			ref.cancelarSubestacion = function(Subestacion){
				subestacion.editar = false;
				listarSubestacion();
			}
			
			// ref.nuevo_propietario = function(Subestacion){
			// 	ref.linea_nuevo_propietario = subestacion;
			// 	$("#nuevoPropietario").bPopup();
			// }
			
			// ref.agregarNuevoPropietario = function(objPropietario){
			// 	var data = "",
			// 		campos = Object.keys(objPropietario),
			// 		contador = 0;
					
			// 	/*console.log(ref.linea_nuevo_propietario.id);*/

			// 	campos.forEach(function(campo,ind,objeto){
			// 		if(campo != "$$hashKey")
			// 		{
			// 			if(objPropietario[campo] != null)
			// 			{
			// 				if(contador > 0)
			// 				{
			// 					data += "&";
			// 				}
							
			// 				data += campo+"="+objPropietario[campo];

			// 				contador++;
			// 			}
			// 		}
			// 	}); 
				
			// 	data += "&linea_electrica="+ref.linea_nuevo_propietario.id;

			// 	/*console.log(data);*/

			// 	$http({
			// 		url:"<?php echo base_url("index.php/propietarios_linea_electrica/propietario");?>/"+objPropietario.id,
			// 		method:"POST",
			// 		data:data,
			// 		headers:{'Content-Type': 'application/x-www-form-urlencoded'}
			// 	}).success(function(data){
			// 		console.log(data);
			// 		if(!(data.error))
			// 		{
			// 			$("#nuevoPropietario").bPopup().close();
			// 			ref.nuevoPropietario = {
			// 				propietario: "",
			// 				contrato_servidumbre: 0,
			// 			};
			// 			ref.editar = false;
			// 			listarLineas();
			// 		}
			// 		else
			// 		{
			// 			ref.error.error = true;
			// 			ref.error.actualizar = data.error;
			// 		}
			// 	}).error(function(err){
					
			// 	});
			// }
			
			// ref.cancelarNuevoPropietario = function(){
			// 	$("#nuevoPropietario").bPopup().close();
			// 	ref.nuevoPropietario = {};
			// }
			
			// ref.eliminandoPropietario = function(objPropietario){
			// 	ref.eliminar_Propietario = objPropietario;
			// 	$("#eliminarPropietario").bPopup();
			// }
			
			// ref.cancelarEliminacionPropietario = function(){
			// 	$("#eliminarPropietario").bPopup().close();
			// 	ref.eliminar_Propietario.comentario = "";
			// 	ref.eliminar_Propietario = {};
			// }
			
			// ref.eliminarPropietario = function(objPropietario){
			// 	var data = "",
			// 		campos = Object.keys(objPropietario),
			// 		contador = 0;

			// 	data += "comentario="+objPropietario.comentario;

			// 	$http({
			// 		url:"<?php echo base_url("index.php/propietarios_linea_electrica/propietario");?>/"+objPropietario.id,
			// 		method:"DELETE",
			// 		data:data,
			// 		headers:{'Content-Type': 'application/x-www-form-urlencoded'}
			// 	}).success(function(data){
			// 		/*console.log(data);*/
			// 		if(!(data.error))
			// 		{
			// 			$("#eliminarPropietario").bPopup().close();
			// 			ref.eliminar_Propietario.comentario = "";
			// 			ref.eliminar_Propietario = {};
			// 			ref.editar = false;
						
			// 			listarLineas();
			// 		}
			// 		else
			// 		{
			// 			ref.error.error = true;
			// 			ref.error.actualizar = data.error;
			// 		}
			// 	}).error(function(err){
					
			// 	});
			// }

			$('#almacenar').on('show.bs.modal', function(e) {
				var button = $(e.relatedTarget);
				var recipient = button.data('whatever');
				var obj = button.data('ts');
				var modal = $(this);
				modal.find('.modal-title').text('Almacenar ' + recipient);
				ref.almacenarObjeto = obj;
				ref.almacenarObjeto.tipo_almacen = 1;
			});

			$('#almacenar').on('hidden.bs.modal', function(e) {
				ref.almacenarObjeto = {};
			});

			ref.almacenar = function(info) {
				$http({
					url: base_url('lineas_electricas/almacenar/' + info.id),
					method: 'POST',
					data: serializeObj(info),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function successCallback(resp) {
					listarLineas();
					$('#almacenar').modal('hide');
					ref.almacenarObjeto = {};
				}, function errorCallback(resp) {

				});
			}

			ref.opcionSitio = function(){
				ref.almacenarObjeto.tipo_almacen = 2;
			}

			ref.opcionAlmacen = function(){
				ref.almacenarObjeto.tipo_almacen = 1;
			}

			ref.disabledAlmacenar = function() {
				var dis = false;

				if ( ! Object.keys(ref.almacenarObjeto).length) {
					dis = true;
				}

				if ((ref.almacenarObjeto.tipo_almacen == 1 && ! ref.almacenarObjeto.almacen) || (ref.almacenarObjeto.tipo_almacen == 2 && ! ref.almacenarObjeto.a_sitio) || ! ref.almacenarObjeto.estado || ! ref.almacenarObjeto.comentario) {
					dis = true;
				}

				return dis;
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}

			//navbar
			buscarSala = function(){            	
            	
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("sala_tecnica/listar_sala")?>"+"/"+ ref.id_mso,
					method:"GET"
				}).success(function(data){
					ref.cargando = false;
					if(data.length)
					{
					  ref.salas = data;
						//console.log(ref.salas[0]['nombre']);
					  ref.salaname = ref.salas[0]['nombre'];
					}
					else
					{
						
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});
			}
            busca_items = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("mso_categoria_tipo/get_all")?>"+"/",
					method:"GET"
				}).success(function(data){
					//console.log(data);
					ref.cargando = false;
					if(data.length)
					{
					ref.dataItem = data;					
					
					}
					else
					{
						
					}              

				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});				
                }

             //navbar		
			

			buscarSala();						
			busca_items();
			listarSubestacion();			
			//listarConexionesLinea();			
			listarTipoTransformador();			
			listarSitios();
			listarAlmacenes();
			listarFases();
			//listarTipoTransformador_modelo();
		}])
		
		.directive('bsPopover',function(){
			return function(scope,element,attrs){
				element.find("span[rel=popover]").popover();
			}
		});
</script>
</body>
</html>