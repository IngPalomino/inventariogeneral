<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="sub in ctrl.subestacion">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Sub Estación {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>							
							<tr>
								<td class="info">Marca del Transformador</td>
								<td class="default">
									<span ng-show="!sub.editar">{{sub.tipo_transformador.marca}}</span>
									<input ng-hide="!sub.editar" class="form-control" type="text" ng-model="sub.tipo_transformador.marca"/>
								</td>
								<td class="info">Modelo</td>
								<td class="default">
									<span ng-show="!sub.editar">{{sub.numero_serie_transformador}}</span>
								    <input ng-hide="!numero_serie.editar" class="form-control" type="text" ng-model="numero_serie.tipo_transformador.marca"/>
							
								</td>
							</tr>
							<tr>
								<td class="info">Potencia</td>
								<td class="default">
									<span ng-show="!sub.editar">
                                    {{sub.potencia}} KVA
                                    </span>
                                    <input ng-hide="!sub.editar" class="form-control" type="text" ng-model="sub.potencia"/>
								</td>
								<td class="info">Número de Serie</td>
						        <td>
						        <span ng-show="!sub.editar">{{sub.numero_serie}}</span>
                                 <input ng-hide="!sub.editar" class="form-control" type="text" ng-model="sub.numero_serie"/>
						        </td>
							</tr>

							<tr>
								<td class="info">Fases</td>
								<td class="default">
									<span ng-show="!sub.editar">{{sub.fases}}</span>
									<input ng-hide="!sub.editar" class="form-control" type="text"  ng-model="sub.fases"/>
								</td>

                                <td class="info">Conexión</td>
								<td class="default" ><span ng-show="!sub.editar">{{sub.conexion}}</span>
								<input ng-hide="!sub.editar" class="form-control" type="text"  ng-model="sub.conexion"/>
								</td>
							</tr>


							<tr>
								<td class="info">Tipo</td>
								<td class="default">
										<span ng-show="!sub.editar">{{sub.tipo}}</span>
										<input ng-hide="!sub.editar" class="form-control" type="text" ng-model="sub.distancia"/>
								</td>
								<td class="info">Voltaje de Entrada(KVA)</td>
								<td class="default">
									<span ng-show="!sub.editar">{{sub.voltaje_entrada}}</span>
									<input ng-hide="!sub.editar" class="form-control" type="text"  ng-model="sub.voltaje_entrada"/>
								</td>
							</tr>
							<tr>
								<td class="info">Voltaje de Salida(KVA)</td>
                                <td class="default">
									<span ng-show="!sub.editar">{{sub.voltaje_salida}}</span>
									<input ng-hide="!sub.editar" class="form-control" type="text"  ng-model="sub.salida"/>
								</td>							
							</tr>				
							<tr>
								<td colspan="2"></td>
								<td style="font-size:8.5pt;list-style:none;text-align:right;" colspan="2">
									<span ng-if="!sub.logs_subestacion.length || sub.logs_subestacion[0].evento.tipo != 'INSERT'">Creado: {{sub.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>
									<span ng-if="sub.logs_subestacion[0].evento.tipo == 'INSERT'">Creado: {{sub.logs_subestacion[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{sub.logs_subestacion[0].usuario.nombres}} {{sub.logs_subestacion[0].usuario.apellido_paterno}}</span>
									<br>
									<span ng-if="sub.logs_subestacion[1].evento.tipo == 'UPDATE'">Actualizado: {{sub.logs_subestacion[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{sub.logs_subestacion[1].usuario.nombres}} {{sub.logs_subestacion[1].usuario.apellido_paterno}}</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editarSubestacion(sub)" ng-show="!sub.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
					<button class="btn btn-sm btn-success" ng-click="ctrl.grabarSubestacion(sub)" ng-hide="!sub.editar">Grabar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarSubestacion(sub)" ng-hide="!sub.editar">Cancelar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoSubestacion(sub)" ng-show="!sub.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.subestacion.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:15px;" ng-click="ctrl.nuevo_subestacion()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir Sub Estación</button>
		</div>
	</div>
</div>

<div id="nuevoSubestacion" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	<h3>Nueva Sub Estación</h3>
	<form id="formsubtacion">
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Marca </td>
					<td class="default">                    
                     <select id="tipo_transformador_marca" onchange="listarTipoTransformador_modelo()"  class="form-control" ng-model="ctrl.nuevoSubestacion.tipo_transformador_marca.marca">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_transformador" value="{{marca.id}}">{{marca.marca}}</option>
					 </select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">					
					     <select class="form-control" ng-model="ctrl.nuevoSubestacion.tipo_transformador_modelo">
							<option value="">Elegir modelo</option>
							<option  ng-repeat="modelo in ctrl.tipo_transformador_modelo" value="{{modelo.id}}">{{modelo.modelo}}</option>
					 </select>
					</td>
				</tr>
				<tr>
					<td class="info">Potencia</td>
					<td class="default">
					  <input type="text" class="form-control input-sm" ng-model="ctrl.nuevoSubestacion.potencia" />
					</td>
					<td class="info">Número de Serie</td>
					<td class="default">
						<input class="form-control" type="text"  ng-model="ctrl.nuevoSubestacion.numero_serie" />
					</td>
				</tr>
				<tr>
					<td class="info">Fases</td>
					<td class="default">						
						 <select  class="form-control" ng-model="ctrl.nuevoSubestacion.fases" >	
                           <option value="">Selecione Fase</option>

						 	<option value="{{fases.id}}"  ng-repeat="fases in ctrl.fases">{{fases.fases}}</option>
						  </select>
					</td>
					<td class="info">Conexión</td>
					<td class="default">
						<input class="form-control" type="text"  ng-model="ctrl.nuevoSubestacion.conexion" />
					</td>
				</tr>
				<tr>
					<td class="info">Tipo</td>
					<td class="default">
                       <input class="form-control" type="text"  ng-model="ctrl.nuevoSubestacion.tipo_transformador_tipo" />
					</td>
					<td class="info">Voltaje de Entrada(KVA)</td>
					<td class="default">
						<div class="input-group">
						<input class="form-control" type="text"  ng-model="ctrl.nuevoSubestacion.voltaje_entrada"/>
						<div class="input-group-addon">KVA</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Voltaje de Salida(KVA)</td>
					<td class="default">
						<div class="input-group">
							<input class="form-control" type="text"  ng-model="ctrl.nuevoSubestacion.potencia_transformador"/>
							<div class="input-group-addon">KVA</div>
						</div>
					</td>					
				</tr>				
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarSubestacion(ctrl.nuevoSubestacion)" ng-disabled="!ctrl.nuevoSubestacion.tipo_transformador.marca || !ctrl.nuevoSubestacion.tipo_transformador_modelo || !ctrl.nuevoSubestacion.potencia || !ctrl.nuevoSubestacion.fases || !ctrl.nuevoSubestacion.conexion || !ctrl.nuevoSubestacion.tipo || !ctrl.nuevoSubestacion.voltaje_entrada || !ctrl.nuevoSubestacion.potencia_transformador">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionSubestacion()">Cancelar</button>
	</form>
</div>

<div id="eliminarSubestacion" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar Sub Estación</h3>
	<p>¿Está seguro de eliminar la Sub Estación?</p>
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Subestacion.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.eliminarSubestacion()" ng-disabled="!ctrl.eliminar_Subestacion.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>

<!-- <div id="nuevoPropietario" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	<h3>Nuevo propietario</h3>
	<form>
		<div class="panel panel-primary">
			<div class="panel-body">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<td class="info text-right">Nombre del propietario</td>
							<td class="active">
								<input class="form-control" ng-model="ctrl.nuevoPropietario.propietario" maxlength="35"/>
							</td>
							<td class="info text-right">Contrato de servidumbre</td>
							<td class="active">
								<div class="input-group">
									<div class="input-group-addon">S/ </div>
									<input type="number" step="any" class="form-control" ng-model="ctrl.nuevoPropietario.contrato_servidumbre" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="info">Teléfono</td>
							<td class="active">
								<input class="form-control" ng-model="ctrl.nuevoPropietario.telefono" type="text"/>
							</td>
							<td class="info">Correo Electrónico</td>
							<td class="active">
								<input class="form-control" ng-model="ctrl.nuevoPropietario.correo" type="email"/>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="panel-footer text-center">
				<button class="btn btn-success btn-sm" ng-click="ctrl.agregarNuevoPropietario(ctrl.nuevoPropietario)" ng-disabled="!ctrl.nuevoPropietario.propietario || !ctrl.nuevoPropietario.propietario">Agregar</button>
				<button class="btn btn-danger btn-sm" ng-click="ctrl.cancelarNuevoPropietario()">Cancelar</button>
			</div>
		</div>
	</form>
</div> -->
<!-- <div id="eliminarPropietario" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	<h3>Eliminar propietario</h3>
	<form>
		<div class="panel panel-primary">
			<div class="panel-body">
				<p>Explique la razón por la que quiere eliminar el propietario "{{ctrl.eliminar_Propietario.propietario}}"</p>
				<textarea class="form-control" style="width:100%;" rows="3" placeholder="Razón..." ng-model="ctrl.eliminar_Propietario.comentario"></textarea>
			</div>
			<div class="panel-footer text-center">
				<button class="btn btn-success btn-sm" ng-disabled="!ctrl.eliminar_Propietario.comentario" ng-click="ctrl.eliminarPropietario(ctrl.eliminar_Propietario)">Eliminar</button>
				<button class="btn btn-danger btn-sm" ng-click="ctrl.cancelarEliminacionPropietario()">Cancelar</button>
			</div>
		</div>
	</form>
</div>
 -->

<div class="modal fade" id="almacenar" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs nav-justified">
					<li class="active"><a data-toggle="tab" href="#almacen" ng-click="ctrl.opcionAlmacen()">Almacén</a></li>
					<li><a data-toggle="tab" href="#sitio" ng-click="ctrl.opcionSitio()">Sitio</a></li>
				</ul>
				<div class="tab-content">
					<div id="almacen" class="tab-pane fade in active">
						<select class="form-control" ng-model="ctrl.almacenarObjeto.almacen">
							<option value="">Elegir almacén</option>
							<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
						</select>
					</div>
					<div id="sitio" class="tab-pane fade">
						<label style="width:100%">
							<selectize config="ctrl.configSitios" options='ctrl.sitios' ng-model="ctrl.almacenarObjeto.a_sitio"></selectize>
						</label>
					</div>
				</div>
			   <select class="form-control" ng-model="ctrl.almacenarObjeto.estado">
					<option value="">Seleccionar estado del equipo</option>
					<option value="1">Nuevo</option>
					<option value="2">Operativo</option>
					<option value="3">De baja</option>
				</select>
				<br>
				<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea almacenar el elemento..." ng-model="ctrl.almacenarObjeto.comentario"></textarea>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" ng-click="ctrl.almacenar(ctrl.almacenarObjeto)" ng-disabled="ctrl.disabledAlmacenar()">Almacenar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>