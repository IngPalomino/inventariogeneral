<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="rectificador in ctrl.rectificadores" ng-if="ctrl.rectificadores.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Planta Rectificadora {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr>
								<td class="info">Marca</td>
								<td class="default" colspan="2">
									<span ng-show="!rectificador.editar">{{rectificador.tipo_controlador_rectif.marca}}</span>
									<select class="form-control" ng-hide="!rectificador.editar" ng-model="rectificador.tipo_controlador_rectif.marca" ng-change="rectificador.tipo_controlador_rectif.id = ''; rectificador.modulosCambio()">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.tipo_controlador_rectif.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
									</select>
								</td>
								<td class="info">Controlador</td>
								<td class="default" colspan="2">
									<span ng-show="!rectificador.editar">{{rectificador.tipo_controlador_rectif.modelo}}</span>
									<select class="form-control" ng-hide="!rectificador.editar" ng-model="rectificador.tipo_controlador_rectif.id" ng-change="rectificador.modulosCambio()">
										<option value="">Elegir controlador</option>
										<option ng-if="rectificador.tipo_controlador_rectif.marca" ng-repeat="controlador in ctrl.tipo_controlador_rectif.modelos | filter:{marca:rectificador.tipo_controlador_rectif.marca}" value="{{controlador.id}}">{{controlador.modelo}}</option>
									</select>
								</td>
							</tr>
						

							<tr>
								<td class="info">Fecha de puesta en servicio</td>
								<td class="default" colspan="2">
									<span ng-show="!rectificador.editar">{{rectificador.anio_servicio | date:"dd/MM/yyyy"}}</span>
									<input class="form-control" type="date" ng-hide="!rectificador.editar" ng-model="rectificador.anio_servicio"/>
								</td> 
								
								<td class="info">Part Number</td>
				                <td class="default" colspan="2">
						<span ng-show="!rectificador.editar">{{rectificador.numero_serie}}</span>
									<input class="form-control" type="text" ng-hide="!rectificador.editar" ng-model="rectificador.numero_serie"/>
				                </td>
							</tr>

						<tr>
							
								<td class="info">Energización</td>
									<td class="default" colspan="2">
									<span ng-show="!rectificador.editar">{{rectificador.energizacion_controlador_rectif.energizacion_controlador_rectif}}</span>
									<select class="form-control" ng-hide="!rectificador.editar" ng-model="rectificador.energizacion_controlador_rectif.id">
										<option value="">Elegir energización</option>
										<option ng-repeat="energizacion in ctrl.energizacion_rectificador" value="{{energizacion.id}}">{{energizacion.energizacion_controlador_rectif}}</option>
									</select>
								</td> 
								<td class="info">Capacidad Máxima</td>
								<td class="default" colspan="2">
									<div class="input-group">
										<span ng-show="!rectificador.editar">{{rectificador.capacidad_maxima}} kW</span>
										<input class="form-control" type="number" min="1" step="any" ng-hide="!rectificador.editar" ng-model="rectificador.capacidad_maxima"/>
										<div class="input-group-addon" ng-hide="!rectificador.editar">kW</div>
									</div>
								</td>
							</tr>




							<tr>
								<td class="info"></td>
								<td class="default" colspan="2">
									
								</td>
								<td class="info">Ubicación</td>
								<td class="default" colspan="2">
									<span ng-show="!rectificador.editar">{{rectificador.ubicacion_controlador_rectif.ubicacion_controlador_rectif}}</span>
									<select class="form-control" ng-hide="!rectificador.editar" ng-model="rectificador.ubicacion_controlador_rectif.id">
										<option value="">Elegir ubicación</option>
										<option ng-repeat="ubicacion in ctrl.ubicacion_rectificador" value="{{ubicacion.id}}">{{ubicacion.ubicacion_controlador_rectif}}
										</option>
									</select>
								</td>
							</tr>






							<tr class="bg-success text-center">
								<td colspan="6">Módulos Rectificadores</td>
							</tr>
							<tr ng-repeat="modulo in rectificador.modulos_rectif" ng-if="modulo.eliminado == '0'">
								<td class="info">
									<button class="btn btn-danger btn-xs" ng-hide="!rectificador.editar" ng-click="ctrl.eliminandoModRectificador(modulo)"><i class="fa fa-times"></i></button> Modelo
								</td>
								<td class="default">
									<span ng-show="!rectificador.editar">{{modulo.tipo_modulos_rectif.modelo}}</span>
									<select class="form-control" ng-hide="!rectificador.editar" ng-model="modulo.tipo_modulos_rectif.id">
										<option value="">Elegir modelo</option>
										<option ng-if="rectificador.tipo_controlador_rectif.id && (modelo.tipo_controlador_rectif == rectificador.tipo_controlador_rectif.id)" ng-repeat="modelo in ctrl.tipo_modulos_rectif" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
								<td class="info">Capacidad</td>
								<td class="default">
									<span ng-if="capacidad.id == modulo.tipo_modulos_rectif.id" ng-repeat="capacidad in ctrl.tipo_modulos_rectif">{{capacidad.capacidad}} kW</span>
								</td>
								<td class="info">Número de Serie</td>
								<td class="default">
									<span ng-show="!rectificador.editar">{{modulo.numero_serie}}</span>
									<input class="form-control" type="text" ng-hide="!rectificador.editar" ng-model="modulo.numero_serie"/>
								</td>
							</tr>
							<tr ng-hide="!rectificador.editar">
								<td colspan="6">
									<button class="btn btn-primary" style="width:100%" ng-click="ctrl.nuevoModRectificador(rectificador)">
										<span class="fa fa-plus"></span> Agregar Módulo Rectificador
									</button>
								</td>
							</tr>
							<tr class="warning">
								<td>Modulos Instalados</td>
								<td colspan="2">{{rectificador.modulosInstalados()}}</td>
								<td>Capacidad Instalada</td>
								<td colspan="2">{{rectificador.capacidadInstalada()}} kW</td>
							</tr>
							<tr>
								<td colspan="4"></td>
								<td style="font-size:8.5pt;list-style:none;text-align:right;" colspan="2">
									<span ng-if="!rectificador.logs_controlador_rectif.length || rectificador.logs_controlador_rectif[0].evento.tipo != 'INSERT'">Creado: {{rectificador.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>
									<span ng-if="rectificador.logs_controlador_rectif[0].evento.tipo == 'INSERT'">Creado: {{rectificador.logs_controlador_rectif[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{rectificador.logs_controlador_rectif[0].usuario.nombres}} {{rectificador.logs_controlador_rectif[0].usuario.apellido_paterno}}</span>
									<br>
									<span ng-if="rectificador.logs_controlador_rectif[1].evento.tipo == 'UPDATE'">Actualizado: {{rectificador.logs_controlador_rectif[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{rectificador.logs_controlador_rectif[1].usuario.nombres}} {{rectificador.logs_controlador_rectif[1].usuario.apellido_paterno}}</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editarRectificador(rectificador)" ng-show="!rectificador.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
					<button class="btn btn-sm btn-success" ng-click="ctrl.grabarRectificador(rectificador)" ng-hide="!rectificador.editar" ng-disabled="!rectificador.tipo_controlador_rectif.id || !rectificador.anio_servicio || !rectificador.capacidad_maxima || !rectificador.ubicacion_controlador_rectif.id || rectificador.disabled()">Grabar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarRectificador(rectificador)" ng-hide="!rectificador.editar">Cancelar</button>
					<button class="btn btn-sm btn-info" ng-click="ctrl.almacenandoRectificador(rectificador)" ng-show="!rectificador.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Almacenar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoRectificador(rectificador)" ng-show="!rectificador.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.rectificadores.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:15px;" ng-click="ctrl.nuevo_rectificador()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir Planta Rectificadora</button>
		</div>
	</div><br>
	<div class="row ng-cloak" ng-if="ctrl.rectificadores_eliminados.length" ng-hide="ctrl.auth.au1">
		<div class="col-xs-12 text-center">
			<p class="text-muted credit alert alert-warning">Existen rectificadores eliminados <button class="btn btn-default btn-xs" ng-click="ctrl.mostrarEliminados()">Mostrar</button></p>
		</div>
	</div>
</div>

<div id="nuevoRectificador" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Nueva Planta Rectificadora</h3>
	
	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Marca</td>
					<td class="default" colspan="2">
						<select class="form-control" ng-model="ctrl.nuevoRectificador.tipo_controlador_rectif.marca" ng-change="ctrl.nuevoRectificador.tipo_controlador_rectif.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_controlador_rectif.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Controlador</td>
					<td class="default" colspan="2">
						<select class="form-control" ng-model="ctrl.nuevoRectificador.tipo_controlador_rectif.id">
							<option value="">Elegir controlador</option>
							<option ng-if="ctrl.nuevoRectificador.tipo_controlador_rectif.marca" ng-repeat="controlador in ctrl.tipo_controlador_rectif.modelos | filter:{marca:ctrl.nuevoRectificador.tipo_controlador_rectif.marca}" value="{{controlador.id}}">{{controlador.modelo}}</option>
						</select>
					</td>
				</tr>

                 <tr>
					<td class="info">Fecha de puesta en servicio</td>
					<td class="default" colspan="2">
						<input class="form-control" type="text" id="datepicker" size="30" ng-model="ctrl.nuevoRectificador.anio_servicio"/>
					</td>
					<td class="info">Part Number</td>
					<td class="default" colspan="2">
						<div class="input-group">
							<input class="form-control" type="text" id="partnumber" size="30" ng-model="ctrl.nuevoRectificador.partnumber" />
						</div>
					</td>
				</tr>



				<tr>
					<td class="info">Energización</td>
					<td class="default" colspan="2">
						<select class="form-control" ng-model="ctrl.nuevoRectificador.energizacion_controlador_rectif.id">
							<option value="">Elegir energización</option>
							<option ng-repeat="energizacion in ctrl.energizacion_rectificador" value="{{energizacion.id}}">{{energizacion.energizacion_controlador_rectif}}</option>
						</select>
					</td>
					<td class="info">Capacidad Máxima</td>
					<td class="default" colspan="2">
						<div class="input-group">
							<input class="form-control" type="number" step="any" min="1" ng-model="ctrl.nuevoRectificador.capacidad_maxima"/>
							<div class="input-group-addon">kW</div>
						</div>
					</td>
				</tr>

				<tr>
					<td class="info"></td>
					<td class="info" colspan="2">
						
					</td>
					<td class="info">Ubicación</td>
					<td class="default" colspan="2">
						<select class="form-control" ng-model="ctrl.nuevoRectificador.ubicacion_controlador_rectif.id">
							<option value="">Elegir ubicación</option>
							<option ng-repeat="ubicacion in ctrl.ubicacion_rectificador" value="{{ubicacion.id}}">{{ubicacion.ubicacion_controlador_rectif}}</option>
						</select>
					</td>
				</tr>
				<tr class="success text-center">
					<td colspan="6">Módulos Rectificadores</td>
				</tr>
				<tr class="ng-cloak" ng-repeat="modRectif in ctrl.nuevoRectificador.modulos_rectif">
					<td class="info">
						<button class="btn btn-danger btn-xs" ng-click="ctrl.eliminarNuevoModRectif($index)"><i class="fa fa-times"></i></button> Modelo
					</td>
					<td class="default">
						<select class="form-control" ng-model="modRectif.tipo_modulos_rectif.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoRectificador.tipo_controlador_rectif.id && (modelo.tipo_controlador_rectif == ctrl.nuevoRectificador.tipo_controlador_rectif.id)" ng-repeat="modelo in ctrl.tipo_modulos_rectif" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Capacidad</td>
					<td class="default">
						<span ng-if="capacidad.id == modRectif.tipo_modulos_rectif.id" ng-repeat="capacidad in ctrl.tipo_modulos_rectif">{{capacidad.capacidad}} kW</span>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="modRectif.numero_serie"/>
					</td>
				</tr>
				<tr>
					<td colspan="6">
						<button class="btn btn-primary" style="width:100%" ng-click="ctrl.nuevoModRectif(ctrl.nuevoRectificador)" ng-disabled="!ctrl.nuevoRectificador.tipo_controlador_rectif.marca || !ctrl.nuevoRectificador.tipo_controlador_rectif.id">
							<span class="fa fa-plus"></span> Agregar Módulo Rectificador
						</button>
					</td>
				</tr>
			</tbody>
		</table>		
		<button class="btn btn-success" ng-click="ctrl.agregarRectificador(ctrl.nuevoRectificador)" ng-disabled="!ctrl.nuevoRectificador.tipo_controlador_rectif.id || !ctrl.nuevoRectificador.anio_servicio || !ctrl.nuevoRectificador.capacidad_maxima || !ctrl.nuevoRectificador.ubicacion_controlador_rectif.id || !ctrl.nuevoRectificador.modulos_rectif.length ||
        !ctrl.nuevoRectificador.partnumber ||
		 ctrl.nuevoRectificador.disabled()">Agregar</button>

		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionRectificador()">Cancelar</button>
	</form>
</div>

<div id="eliminarRectificador" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar Rectificador</h3>
	
	<p>¿Está seguro de eliminar el rectificador?</p>
	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Rectificador.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.eliminarRectificador()" ng-disabled="!ctrl.eliminar_Rectificador.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>

<div id="eliminarModRectificador" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar Módulo Rectificador</h3>

	<form>
		<div class="panel panel-primary">
			<div class="panel-body">
				<p>Explique la razón por la que quiere eliminar el Módulo Rectificador</p>
				<textarea class="form-control" style="width:100%;" rows="3" placeholder="Razón..." ng-model="ctrl.eliminar_ModRectificador.comentario"></textarea>
			</div>
			<div class="panel-footer text-center">
				<button class="btn btn-success btn-sm" ng-disabled="!ctrl.eliminar_ModRectificador.comentario" ng-click="ctrl.eliminarModRectificador()">Eliminar</button>
				<button class="btn btn-danger btn-sm" ng-click="ctrl.cancelarEliminacionModRectificador()">Cancelar</button>
			</div>
		</div>
	</form>
</div>

<div id="mostrarEliminados" class="popup" style="max-width:1000px;">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<div class="col-xs-12">
		<h3>Rectificadores Eliminados</h3>
	</div>
	
	<div class="container" style="max-width:950px;max-height:500px;overflow-y:scroll;">
		<div class="row">
			<div class="col-xs-12" ng-repeat="rectificador_eliminado in ctrl.rectificadores_eliminados">
				<div class="panel panel-primary">
					<div class="panel-heading text-center">
						Planta Rectificadora Eliminada {{$index + 1}}
					</div>
					<div class="panel-body">
						<table class="table table-condensed">
							<tbody>
								<tr>
									<td class="info">Marca</td>
									<td class="default" colspan="2">{{rectificador_eliminado.tipo_controlador_rectif.marca}}</td>
									<td class="info">Controlador</td>
									<td class="default" colspan="2">{{rectificador_eliminado.tipo_controlador_rectif.modelo}}</td>
								</tr>
								<tr>
									<td class="info">Fecha de puesta en servicio</td>
									<td class="default" colspan="2">{{rectificador_eliminado.anio_servicio | date:"dd/MM/yyyy"}}</td>
									<td class="info">Capacidad Máxima</td>
									<td class="default" colspan="2">{{rectificador_eliminado.capacidad_maxima}} kW</td>
								</tr>
								<tr>
									<td class="info">Energización</td>
									<td class="default" colspan="2">{{rectificador_eliminado.energizacion_controlador_rectif.energizacion_controlador_rectif}}</td>
									<td class="info" colspan="3"></td>
								</tr>
								<tr class="bg-success text-center">
									<td colspan="6">Módulos Rectificadores</td>
								</tr>
								<tr ng-repeat="modulo in rectificador_eliminado.modulos_rectif" ng-if="modulo.eliminado == '0'">
									<td class="info">Modelo</td>
									<td class="default">{{modulo.tipo_modulos_rectif.modelo}}</td>
									<td class="info">Capacidad</td>
									<td class="default">{{modulo.tipo_modulos_rectif.capacidad}} kW</td>
									<td class="info">Número de Serie</td>
									<td class="default">{{modulo.numero_serie}}</td>
								</tr>
								<tr class="bg-warning">
									<td>Módulos Instalados</td>
									<td colspan="2">{{rectificador_eliminado.modulosInstalados()}}</td>
									<td>Capacidad Instalada</td>
									<td colspan="2">{{rectificador_eliminado.capacidadInstalada()}} kW</td>
								</tr>
								<tr>
									<td colspan="4"></td>
									<td colspan="2" style="font-size:8.5pt;text-align:right;">
										Eliminado: {{rectificador_eliminado.logs_controlador_rectif[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}}<br>
										Por: {{rectificador_eliminado.logs_controlador_rectif[0].usuario.nombres}} {{rectificador_eliminado.logs_controlador_rectif[0].usuario.apellido_paterno}}<br>
										Motivo: {{rectificador_eliminado.logs_controlador_rectif[0].comentario}}
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="panel-footer text-center">
						<button class="btn btn-sm btn-success" ng-click="ctrl.restaurarRectificador(rectificador_eliminado)">Restaurar</button>
						<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoDefinitivoRectificador(rectificador_eliminado)" ng-disabled="ctrl.auth.au1">Eliminación Definitiva</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="eliminarDefinitivoRectificador" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar Planta Rectificadora Definitivamente</h3>
	
	<p class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> ESTA ACCIÓN NO ES REVERSIBLE Y ELIMINARÁ LA PLANTA RECTIFICADORA COMPLETAMENTE DE LA BASE DE DATOS.</p>
	
	<p>¿Está seguro de eliminar la planta rectificadora?</p>
	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_DefinitivoRectificador.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.eliminarDefinitivoRectificador()" ng-disabled="!ctrl.eliminar_DefinitivoRectificador.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionDefinitivoRectificador()">Cancelar</button>
</div>

<div id="almacenarRectificador" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Almacenar en Bodega Virtual</h3>

	<div style="max-width:400px;max-height:300px;overflow:auto;">
		<p>Elegir módulos:</p>
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="checkbox" ng-repeat="modulo in ctrl.almacenar_Rectificador.modulos_rectif | filter:{eliminado:0}">
					<label><input type="checkbox" ng-model="modulo.check"> {{modulo.tipo_modulos_rectif.modelo}} - {{modulo.numero_serie}}</label>
				</div>
			</div>
		</div>
		<p>Elegir destino:</p>
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a data-toggle="tab" href="#almacen" ng-click="ctrl.opcionAlmacen()">Almacén</a></li>
			<li><a data-toggle="tab" href="#sitio" ng-click="ctrl.opcionSitio()">Sitio</a></li>
		</ul>
		<div class="tab-content">
			<div id="almacen" class="tab-pane fade in active">
				<select class="form-control" ng-model="ctrl.almacenar_Rectificador.almacen">
					<option value="">Elegir almacén</option>
					<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
				</select>
			</div>
			<div id="sitio" class="tab-pane fade">
				<label style="width:100%">
					<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenar_Rectificador.a_sitio"></selectize>
				</label>
			</div>
		</div>
		<select class="form-control" ng-model="ctrl.almacenar_Rectificador.estado">
			<option value="">Seleccionar estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select><br>

		<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea almacenar el elemento..." ng-model="ctrl.almacenar_Rectificador.comentario"></textarea>
	</div>

	<button class="btn btn-success" ng-click="ctrl.almacenarRectificador(ctrl.almacenar_Rectificador)" ng-disabled="!ctrl.disabledAlmacenamiento()">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamiento()">Cancelar</button>
</div>