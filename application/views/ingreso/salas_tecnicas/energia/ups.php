

<style>
   .default.altura_td{height: 47px;width: 203px;}
</style>
<br>
<div class="container">
   <div class="text-center">
      <span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
   </div>
   <div class="row ng-cloak">
      <div class="col-xs-12" ng-repeat="ups in ctrl.ups" ng-if="ctrl.ups.length">
         <div class="panel panel-primary">
            <div class="panel-heading text-center">
               Ups {{$index + 1}}
            </div>          
           <div class="panel-body">
               <table class="table table-condensed">
                  <tbody>
                     <tr class="text-center" style="background-color:#d2f3ce;">
                        <td colspan="6">Ups</td>
                     </tr>
                     <tr>
                        <td class="info">Ubicación</td>
                        <td class="default altura_td">
                           <span ng-show="!ups.editar">{{ups.ubicacion}}</span>
                           <input class="form-control" ng-hide="!ups.editar" ng-model="ups.ubicacion"/>
                        </td>
                        <td class="info">Marca</td>
                        <td class="default altura_td">
                           <span ng-show="!ups.editar">{{ups.tipo_ups.marca}}</span>
                           <select class="form-control" ng-hide="!ups.editar" ng-model="ups.tipo_ups.marca" ng-change="ups.tipo_ups.id = '';">
                              <option value="">Elegir marca</option>
                              <option ng-repeat="marca in ctrl.tipo_ups.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
                           </select>
                        </td>
                        <td class="info">Modelo</td>
                        <td class="default altura_td">
                           <span ng-show="!ups.editar">{{ups.tipo_ups.modelo}}</span>
                           <select class="form-control" ng-hide="!ups.editar" ng-model="ups.tipo_ups.id">
                              <option value="">Elegir modelo</option>
                              <option ng-if="ups.tipo_ups.marca" ng-repeat="modelo in ctrl.tipo_ups.modelos | filter:{marca:ups.tipo_ups.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
                           </select>
                        </td>
                     </tr>
                     <tr>
                        <td class="info">Número de serie</td>
                        <td class="default altura_td">
                           <span ng-show="!ups.editar">{{ups.numero_serie}}</span>
                           <input class="form-control" ng-hide="!ups.editar" ng-model="ups.numero_serie"/>
                        </td>
                        <td class="info">Capacidad</td>
                        <td class="default altura_td">
                           <span ng-show="!ups.editar">{{ups.capacidad}}</span>
                           <input class="form-control" ng-hide="!ups.editar" ng-model="ups.capacidad"/>
                        </td>
                        <td class="info">Fases</td>
                        <td class="default altura_td">
                           <span ng-show="!ups.editar">{{ups.fases}}</span>
                           <input class="form-control" ng-hide="!ups.editar" ng-model="ups.fases"/>
                        </td>
                     </tr>
                     <tr>
                        <td class="info">Voltaje Entrada (VAC)</td>
                        <td class="default altura_td">
                           <span ng-show="!ups.editar">{{ups.voltaje_entrada}}</span>
                           <input class="form-control" ng-hide="!ups.editar" ng-model="ups.voltaje_entrada"/>
                        </td>
                        <td class="info">Asociar Tansformador</td>
                        <td class="default altura_td">
                           
                           <span ng-show="!ups.editar">
                         {{ ups.tipo_trasformador}}</span>
                           <select class="form-control" ng-hide="!ups.editar" ng-model="ups.tipo_trasformador">
                           <option value="">Elegir Transformador</option>
                           <option  ng-repeat="traformador in ctrl.tipo_trasformador" value="{{traformador.marca}}">{{traformador.marca}}</option>
                           </select>
                        </td>
                        <td class="info">Asociar Banco de Baterias</td>
                        <td class="default altura_td">
                           <span ng-show="!ups.editar">{{ups.controlador_rectif_bancos_baterias}}</span>
                           <select ng-hide="!ups.editar" class="form-control" ng-model="ctrl.ups.controlador_rectif_bancos_baterias">
                              <option value="">Elegir Banco Baterias</option>
                            

                              <option ng-repeat="banco in ctrl.item_banco"   value="ID:{{banco.id}}|{{banco.tipo_bancos_baterias.marca}} - {{banco.tipo_bancos_baterias.modelo}}">
                                 ID:{{banco.id}}|{{banco.tipo_bancos_baterias.marca}} - {{banco.tipo_bancos_baterias.modelo}}
                              </option>


                           </select>
                        </td>
                     </tr>
                     <tr>
                        <td class="info">Voltaje Salida (VAC)</td>
                        <td class="default altura_td">
                           <span ng-show="!ups.editar">{{ups.voltaje_salida}}</span>
                    <input class="form-control" ng-hide="!ups.editar" ng-model="ups.voltaje_salida"/>
                        </td>
                        <td class="info"></td>
                        <td class="info">
                        </td>
                        <td class="info"></td>
                        <td class="info">									
                        </td>
                     </tr>
                     <tr>
                        <td colspan="4"></td>
                        <td style="font-size:8.5pt;list-style:none;text-align:right;" colspan="2">
                           <span ng-if="!ups.logs_ups.length || ups.logs_ups[0].evento.tipo != 'INSERT'">Creado: {{ups.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>
                           <span ng-if="ups.logs_ups[0].evento.tipo == 'INSERT'">Creado: {{ups.logs_ups[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{ups.logs_ups[0].usuario.nombres}} {{ups.logs_ups[0].usuario.apellido_paterno}}</span>
                           <br>
                           <span ng-if="ups.logs_ups[1].evento.tipo == 'UPDATE'">Actualizado: {{ups.logs_ups[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{ups.logs_ups[1].usuario.nombres}} {{ups.logs_ups[1].usuario.apellido_paterno}}</span>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>




            <div class="text-center">
               <span class="fa fa-spinner fa-spin fa-2x" ng-if="ctrl.procesando"></span>
            </div>
            <div class="panel-footer text-center">
               <button class="btn btn-sm btn-warning" ng-click="ctrl.editarUps(ups)" ng-show="!ups.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
               <button class="btn btn-sm btn-success" ng-click="ctrl.grabarUps(ups)" ng-hide="!ups.editar" ng-disabled="ups.disabled()">Grabar</button>
               <button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarUps(ups)" ng-hide="!ups.editar">Cancelar</button>
             

               <button class="btn btn-sm btn-info" ng-click="ctrl.almacenandoUps(ups)" ng-show="!ups.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Almacenar</button>


               <button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoUps(ups)" ng-show="!ups.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
            </div>
         </div>
      </div>
      <div class="col-xs-12 ng-cloak" ng-if="!ctrl.ups.length && !ctrl.cargando">
         <div class="well text-center">
            No hay información
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xs-12">
         <button class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:15px;" ng-click="ctrl.nuevo_ups()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir Ups</button>
      </div>
   </div>
</div>
<div id="nuevoUps" class="popup">
   <span class="button b-close">
   <span>X</span>
   </span>
   <h3>Nuevo Ups</h3>
   <form>
      <table class="table table-condensed">
         <tbody>
            <tr class="text-center" style="background-color:#d2f3ce;">
               <!-- <td colspan="6">Ups</td> -->
            </tr>
            <tr>
               <td class="info">Ubicación</td>
               <td class="default">
                  <input class="form-control" ng-model="ctrl.nuevoUps.ubicacion"/>
               </td>
               <td class="info">Marca</td>
               <td class="default">
                  <select class="form-control" ng-model="ctrl.nuevoUps.tipo_ups.marca" ng-change="ctrl.nuevoUps.tipo_ups.id = '';">
                     <option value="">Elegir marca</option>
                     <option ng-repeat="marca in ctrl.tipo_ups.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
                  </select>
               </td>
               <td class="info">Modelo</td>
               <td class="default">
                  <select class="form-control" ng-model="ctrl.nuevoUps.tipo_ups.id">
                     <option value="">Elegir modelo</option>
                     <option ng-if="ctrl.nuevoUps.tipo_ups.marca" ng-repeat="modelo in ctrl.tipo_ups.modelos | filter:{marca:ctrl.nuevoUps.tipo_ups.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
                  </select>
               </td>
            </tr>
           
            <tr>
               <td class="info">Nro Serie</td>
               <td class="default">
                  <input class="form-control" ng-model="ctrl.nuevoUps.numero_serie"/>
               </td>
               <td class="info">Capacidad</td>
               <td class="default">
                  <input class="form-control" ng-model="ctrl.nuevoUps.capacidad"/>
               </td>
               <td class="info">Fases</td>
               <td class="default">
                <input class="form-control" ng-model="ctrl.nuevoUps.fases"/>
               </td>
            </tr>

            <tr>
               <td class="info">Voltage Entrada (VAC)</td>
               <td class="default">
                  <input class="form-control" ng-model="ctrl.nuevoUps.voltaje_entrada"/>
               </td>
               <td class="info">Asociar Transformador</td>
               <td class="default">
                  <select class="form-control" ng-model="ctrl.nuevoUps.tipo_trasformador.transformador">
                     <option value="">Elegir Trasformador</option>
                     <option  ng-repeat="traformador in ctrl.tipo_trasformador" value="{{traformador.id}}">{{traformador.marca}}</option>
                  </select>
               </td>
               <td class="info">Asociars Banco Baterias</td>
               <td class="default">
                  <select class="form-control"  ng-model="ctrl.nuevoUps.controlador_rectif_bancos_baterias">
                    <option value="">Elegir Banco Baterias</option>
                     <option ng-repeat="banco in ctrl.item_banco"   value="ID:{{banco.id}}|{{banco.tipo_bancos_baterias.marca}} - {{banco.tipo_bancos_baterias.modelo}}">
                                 ID:{{banco.id}}|{{banco.tipo_bancos_baterias.marca}} - {{banco.tipo_bancos_baterias.modelo}}
                              </option>


               </td>
            </tr>
            <tr>
               <td class="info">Voltaje Salida (VAC)</td>
               <td class="default">
                  <input class="form-control" ng-model="ctrl.nuevoUps.voltaje_salida"/>
               </td>
            </tr>
         </tbody>
      </table>
      <button class="btn btn-success" ng-click="ctrl.agregarUps(ctrl.nuevoUps)"
         ng-disabled="!ctrl.nuevoUps.ubicacion || !ctrl.nuevoUps.tipo_ups.marca || !ctrl.nuevoUps.tipo_ups.id || !ctrl.nuevoUps.numero_serie || !ctrl.nuevoUps.capacidad || !ctrl.nuevoUps.fases|| !ctrl.nuevoUps.voltaje_entrada || !ctrl.nuevoUps.tipo_trasformador.transformador || !ctrl.nuevoUps.controlador_rectif_bancos_baterias || !ctrl.nuevoUps.voltaje_salida">Agregar</button>
      <button class="btn btn-danger" ng-click="ctrl.cancelarCreacionups()">Cancelar</button>
   </form>
</div>
<div id="eliminarUps" class="popup">
   <span class="button b-close">
   <span>X</span>
   </span>	
   <h3>Eliminar Ups</h3>
   <p>¿Está seguro de eliminar el Ups?</p>
   <textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Ups.comentario"></textarea>
   <button class="btn btn-success" ng-click="ctrl.eliminarUps()" ng-disabled="!ctrl.eliminar_Ups.comentario">Eliminar</button>
   <button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>
<div id="almacenarUps" class="popup">
   <span class="button b-close">
   <span>X</span>
   </span>
   <h3>Almacenar Ups</h3>
   <p>Elegir destino:{{ctrl.almacenar_Ups.numero_serie}} </p>
   <div style="max-width:400px">
      <ul class="nav nav-tabs nav-justified">
         <li class="active"><a data-toggle="tab" href="#almacen" ng-click="ctrl.upsOpcionAlmacen()">Almacén</a></li>
         <li><a data-toggle="tab" href="#sitio" ng-click="ctrl.ups()">Sitio</a></li>
      </ul>
      <div class="tab-content">
         <div id="almacen" class="tab-pane fade in active">
            <select class="form-control" ng-model="ctrl.almacenar_ups.almacen">
               <option value="">Elegir almacén</option>
               <option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
            </select>
         </div>
         <div id="sitio" class="tab-pane fade">
            <label style="width:100%">
               <selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenar_ups.a_sitio"></selectize>
            </label>
         </div>
      </div>
      <select class="form-control" ng-model="ctrl.almacenar_ups.estado">
         <option value="">Seleccionar estado del equipo</option>
         <option value="1">Nuevo</option>
         <option value="2">Operativo</option>
         <option value="3">De baja</option>
      </select>
   </div>
   <br>
   <textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea almacenar el elemento..." ng-model="ctrl.almacenar_ups.comentario"></textarea>
   <button class="btn btn-success" ng-click="ctrl.almacenarUps(ctrl.almacenar_ups)" ng-disabled="!ctrl.almacenar_ups.estado || !ctrl.almacenar_ups.comentario || (ctrl.almacenar_ups.tipo_almacen == 1 && !ctrl.almacenar_ups.almacen) || (ctrl.almacenar_ups.tipo_almacen == 2 && !ctrl.almacenar_ups.a_sitio)">Almacenar</button>
   <button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamientoUps()">Cancelar</button>
</div>
<!-- cargador -->
<div ng-if="ctrl.procesandoRegistro" class="procesand" style="position: fixed;top: 45%;z-index: 99999;left: 40%;right: 40%;display: block;">
   <center>
      <span class="fa fa-spinner fa-spin fa-5x" ></span>
   </center>
</div>
<!-- cargador -->

