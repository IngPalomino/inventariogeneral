<!--<p>
	<?php print_r($session);?>
</p>-->

<script type="text/javascript">
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover();
	});

	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.antenasMicroondas = [];
			ref.antenasGUL = [];

			ref.marcasMicroondas = [];
			ref.marcasModelosGUL = {};
			ref.modelosMicroondas = [];
			ref.modelosGUL = [];
			ref.versionesMicroondas = [];

			ref.nuevaAntenaMicroondas = {};
			ref.editarAntenaMicroondas = {};
			ref.eliminarAntenaMicroondas = {};

			ref.nuevaAntenaGUL = {};

			ref.inputs = {
				marcaGUL: ""
			};

			ref.extras = {
				GUL: {}
			};

			listarAntenas = function(){
				$http({
					url:"<?php echo base_url("index.php/antenas_microondas/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenasMicroondas = data;
					}
					else
					{

					}
				}).error(function(err){

				});

				$http({
					url:"<?php echo base_url("index.php/antenas_gul/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenasGUL = data;

						ref.antenasGUL.forEach(function(antGUL,indx,objx){
							objx[indx].tipo_antena_gul.extras = JSON.parse(objx[indx].tipo_antena_gul.extras);
						});
					}
					else
					{

					}
				}).error(function(err){

				});

				$http({
					url:"<?php echo base_url("index.php/antenas_iden/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenasIDEN = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.range = function(min,max,step){
				var step = step || 1,
					rango = [];

				for(i = min; i <= max; i+=step)
				{
					rango.push(i);
				}

				return rango;
			}

			/*********************************************************************************************************/
			/*	Antenas Microondas																					 */
			/*********************************************************************************************************/

			listarMarcasMicroondas = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_antena_microondas/marcas");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.marcasMicroondas = data;
					}
				}).error(function(err){

				});
			}

			ref.listarModelosMicroondas = function(marca){
				$http({
					url:"<?php echo base_url("index.php/tipo_antena_microondas/modelos");?>"+"/"+encodeURI(marca),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.modelosMicroondas = data;
					}
				}).error(function(err){

				});
			}

			/*ref.listarVersionesMicroondas = function(marca,modelo){
				$http({
					url:"<?php echo base_url("index.php/tipo_antena_microondas/versiones");?>"+"/"+encodeURI(marca)+"/"+encodeURI(modelo),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.versionesMicroondas = data;
					}
				}).error(function(err){

				});
			}*/

			ref.agregarMicroondas = function(){
				var data = "numero_serie="+ref.nuevaAntenaMicroondas.numero_serie
							+"&numero_inventario="+ref.nuevaAntenaMicroondas.numero_inventario
							+"&marca="+ref.nuevaAntenaMicroondas.marca
							+"&modelo="+ref.nuevaAntenaMicroondas.modelo
							+"&version="+ref.nuevaAntenaMicroondas.version;

				$http({
					url:"<?php echo base_url("index.php/antenas_microondas/antena/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.nuevaAntenaMicroondas = {};
						listarAntenas();
						$('#nuevaAntenaMicroondas').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarCreacionMicroondas = function(){
				ref.nuevaAntenaMicroondas = {};
				$('#nuevaAntenaMicroondas').bPopup().close();
			}

			ref.editar_antenaMicroondas = function(id){
				$http({
					url:"<?php echo base_url("index.php/antenas_microondas/antena");?>"+"/"+id,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.editarAntenaMicroondas = data[0];
						ref.listarModelosMicroondas(ref.editarAntenaMicroondas.tipo_antena_microondas.marca);
						ref.listarVersionesMicroondas(ref.editarAntenaMicroondas.tipo_antena_microondas.marca,ref.editarAntenaMicroondas.tipo_antena_microondas.modelo);
						$('#editarAntenaMicroondas').bPopup();
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.actualizarMicroondas = function(id){
				var data = "numero_serie="+ref.editarAntenaMicroondas.numero_serie
							+"&numero_inventario="+ref.editarAntenaMicroondas.numero_inventario
							+"&marca="+ref.editarAntenaMicroondas.tipo_antena_microondas.marca
							+"&modelo="+ref.editarAntenaMicroondas.tipo_antena_microondas.modelo
							+"&version="+ref.editarAntenaMicroondas.tipo_antena_microondas.version;

				$http({
					url:"<?php echo base_url("index.php/antenas_microondas/antena/");?>"+"/"+id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.length)
					{

					}
					else
					{
						$('#editarAntenaMicroondas').bPopup().close();
						ref.editarAntenaMicroondas = {};
						listarAntenas();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEdicionMicroondas = function(){
				ref.editarAntenaMicroondas = {};
				$('#editarAntenaMicroondas').bPopup().close();
			}

			ref.eliminandoAntenaMicroondas = function(id){
				$http({
					url:"<?php echo base_url("index.php/antenas_microondas/antena");?>"+"/"+id,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.eliminarAntenaMicroondas = data[0];
						$('#eliminarAntenaMicroondas').bPopup();
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.eliminarMicroondas = function(){
				$http({
					url:"<?php echo base_url("index.php/antenas_microondas/antena");?>"+"/"+ref.eliminarAntenaMicroondas.id,
					method:"DELETE"
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.antenasMicroondas = [];
						listarAntenas();
						ref.eliminarAntenaMicroondas = {};
						$('#eliminarAntenaMicroondas').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEliminacionMicroondas = function(){
				ref.eliminarAntenaMicroondas = {};
				$('#eliminarAntenaMicroondas').bPopup().close();
			}

			/*********************************************************************************************************/
			/*	Antenas GUL																							 */
			/*********************************************************************************************************/

			constructorAntenaGUL = function(numero_serie = "", sector = 0, altura = 0, azimuth = 0, tilt_electrico = 0, tilt_mecanico = 0){
				this.tipo_antena_gul = 0;
				this.numero_serie = numero_serie;
				this.sector = sector;
				this.altura = altura;
				this.azimuth = azimuth;
				this.tilt_electrico = tilt_electrico;
				this.tilt_mecanico = tilt_mecanico;
			}

			listarMarcasModelosGUL = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_antena_gul/listarMarcasModelos");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.marcasModelosGUL = data;
					}
				}).error(function(err){

				});
			}

			ref.filtroModelosGUL = function(modelo){
				var valido = true;

				if( modelo.marca != ref.inputs.marcaGUL)
				{
					valido = false;
				}

				return valido;
			}

			ref.leerExtrasGUL = function(id){
				ref.extras.GUL = {};

				ref.marcasModelosGUL.modelos.forEach(function(modelo){
					if(modelo.id == id)
					{
						ref.extras.GUL = JSON.parse(modelo.extras);
					}
				});

				if(ref.extras.GUL != {})
				{
					var extras = Object.keys(ref.extras.GUL),
						camposConstructor = {
							numero_serie: "",
							sector: 0,
							altura: 0,
							azimuth: 0,
							tilt_electrico: 0,
							tilt_mecanico: 0
						};

					extras.forEach(function(extra){
						if(extra == "multiplicidad_campos")
						{
							var campos = Object.keys(ref.extras.GUL.multiplicidad_campos)

							campos.forEach(function(campo,indx,objx){
								camposConstructor[campo] = [];
							});
						}
					});
				}
				else
				{
					ref.nuevaAntenaGUL = new constructorAntenaGUL();
				}
			}

			ref.popupNuevaAntenaGUL = function(){
				ref.inputs.marcaGUL = "";
				ref.nuevaAntenaGUL.modelo = '';
				$('#nuevaAntenaGUL').bPopup();
			}

			ref.agregarGUL = function(){
				console.log(ref.nuevaAntenaGUL);
				/*var data = "numero_serie="+ref.nuevaAntenaGUL.numero_serie
							+"&tipo_antena_gul="+ref.nuevaAntenaGUL.tipo_antena_gul
							+"&sector="+ref.nuevaAntenaGUL.sector
							+"&altura="+ref.nuevaAntenaGUL.altura
							+"&azimuth="+ref.nuevaAntenaGUL.azimuth
							+"&tilt_electrico="+ref.nuevaAntenaGUL.tilt_electrico
							+"&tilt_mecanico="+ref.nuevaAntenaGUL.tilt_mecanico;

				$http({
					url:"<?php echo base_url("index.php/antenas_gul/antena/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.nuevaAntenaGUL = {};
						listarAntenas();
						$('#nuevaAntenaGUL').bPopup().close();
					}
				}).error(function(err){

				});*/
			}

			ref.cancelarCreacionGUL = function(){
				ref.nuevaAntenaGUL = {};
				$('#nuevaAntenaGUL').bPopup().close();
			}

			ref.editar_antenaGUL = function(id){
				$http({
					url:"<?php echo base_url("index.php/antenas_gul/antena");?>"+"/"+id,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.editarAntenaGUL = data[0];
						ref.editarAntenaGUL["altura"] = Number(ref.editarAntenaGUL["altura"]);
						ref.editarAntenaGUL["azimuth"] = Number(ref.editarAntenaGUL["azimuth"]);
						ref.editarAntenaGUL["tilt_electrico"] = Number(ref.editarAntenaGUL["tilt_electrico"]);
						ref.editarAntenaGUL["tilt_mecanico"] = Number(ref.editarAntenaGUL["tilt_mecanico"]);
						ref.inputs.marcaGUL = ref.editarAntenaGUL.tipo_antena_gul.marca;
						$('#editarAntenaGUL').bPopup();
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.actualizarGUL = function(id){
				var campos = Object.keys(ref.editarAntenaGUL),
					data = "";

				campos.forEach(function(campo,ind,objeto){
					if(campo == "tipo_antena_gul")
					{
						data += "marca="+ref.editarAntenaGUL.tipo_antena_gul.marca+"&";
						data += "modelo="+ref.editarAntenaGUL.tipo_antena_gul.modelo+"&";
					}
					else
					{
						if(campo == "created_at")
						{

						}
						else
						{
							data += campo+"="+ref.editarAntenaGUL[campo];

							if( ind < (objeto.length-2) )
							{
								data += "&";
							}
						}
					}
				});

				$http({
					url:"<?php echo base_url("index.php/antenas_gul/antena");?>"+"/"+id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.length)
					{

					}
					else
					{
						$('#editarAntenaGUL').bPopup().close();
						ref.editarAntenaGUL = {};
						listarAntenas();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEdicionGUL = function(){
				ref.editarAntenaGUL = {};
				$('#editarAntenaGUL').bPopup().close();
			}

			ref.eliminar_antenaGUL = function(id){
				$http({
					url:"<?php echo base_url("index.php/antenas_gul/antena");?>"+"/"+id,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.eliminarAntenaGUL = data[0];
						$('#eliminarAntenaGUL').bPopup();
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.eliminarGUL = function(){
				var data = "comentario="+ref.eliminarAntenaGUL.comentario;

				$http({
					url:"<?php echo base_url("index.php/antenas_gul/antena");?>"+"/"+ref.eliminarAntenaGUL.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.antenasGUL = [];
						listarAntenas();
						ref.eliminarAntenaGUL = {};
						$('#eliminarAntenaGUL').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEliminacionGUL = function(){
				ref.eliminarAntenaGUL = {};
				$('#eliminarAntenaGUL').bPopup().close();
			}

			/*********************************************************************************************************/
			/*	Antenas iDEN																						 */
			/*********************************************************************************************************/

			listarMarcasIDEN = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_antena_iden/marcas");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.marcasIDEN = data;
					}
				}).error(function(err){

				});
			}

			ref.listarModelosIDEN = function(marca){
				$http({
					url:"<?php echo base_url("index.php/tipo_antena_iden/modelos");?>"+"/"+encodeURI(marca),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.modelosIDEN = data;
					}
				}).error(function(err){

				});
			}

			ref.agregarIDEN = function(){
				var data = "numero_serie="+ref.nuevaAntenaIDEN.numero_serie
							+"&marca="+ref.nuevaAntenaIDEN.marca
							+"&modelo="+ref.nuevaAntenaIDEN.modelo
							+"&sector="+ref.nuevaAntenaIDEN.sector
							+"&altura="+ref.nuevaAntenaIDEN.altura
							+"&azimuth="+ref.nuevaAntenaIDEN.azimuth
							+"&tilt_electrico="+ref.nuevaAntenaIDEN.tilt_electrico
							+"&tilt_mecanico="+ref.nuevaAntenaIDEN.tilt_mecanico;

				$http({
					url:"<?php echo base_url("index.php/antenas_iden/antena/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.nuevaAntenaIDEN = {};
						listarAntenas();
						$('#nuevaAntenaIDEN').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarCreacionIDEN = function(){
				ref.nuevaAntenaIDEN = {};
				$('#nuevaAntenaIDEN').bPopup().close();
			}

			ref.editar_antenaIDEN = function(id){
				$http({
					url:"<?php echo base_url("index.php/antenas_iden/antena");?>"+"/"+id,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.editarAntenaIDEN = data[0];
						ref.editarAntenaIDEN["altura"] = Number(ref.editarAntenaIDEN["altura"]);
						ref.editarAntenaIDEN["azimuth"] = Number(ref.editarAntenaIDEN["azimuth"]);
						ref.editarAntenaIDEN["tilt_electrico"] = Number(ref.editarAntenaIDEN["tilt_electrico"]);
						ref.editarAntenaIDEN["tilt_mecanico"] = Number(ref.editarAntenaIDEN["tilt_mecanico"]);
						ref.listarModelosIDEN(ref.editarAntenaIDEN.tipo_antena_iden.marca);
						$('#editarAntenaIDEN').bPopup();
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.actualizarIDEN = function(id){
				var campos = Object.keys(ref.editarAntenaIDEN),
					data = "";

				campos.forEach(function(campo,ind,objeto){
					if(campo == "tipo_antena_iden")
					{
						data += "marca="+ref.editarAntenaIDEN.tipo_antena_iden.marca+"&";
						data += "modelo="+ref.editarAntenaIDEN.tipo_antena_iden.modelo+"&";
					}
					else
					{
						if(campo == "created_at")
						{

						}
						else
						{
							data += campo+"="+ref.editarAntenaIDEN[campo];

							if( ind < (objeto.length-2) )
							{
								data += "&";
							}
						}
					}
				});

				$http({
					url:"<?php echo base_url("index.php/antenas_iden/antena");?>"+"/"+id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.length)
					{

					}
					else
					{
						$('#editarAntenaIDEN').bPopup().close();
						ref.editarAntenaIDEN = {};
						listarAntenas();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEdicionIDEN = function(){
				ref.editarAntenaIDEN = {};
				$('#editarAntenaIDEN').bPopup().close();
			}

			ref.eliminar_antenaIDEN = function(id){
				$http({
					url:"<?php echo base_url("index.php/antenas_iden/antena");?>"+"/"+id,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.eliminarAntenaIDEN = data[0];
						$('#eliminarAntenaIDEN').bPopup();
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.eliminarIDEN = function(){
				var data = "comentario="+ref.eliminarAntenaIDEN.comentario;

				$http({
					url:"<?php echo base_url("index.php/antenas_iden/antena");?>"+"/"+ref.eliminarAntenaIDEN.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.antenasIDEN = [];
						listarAntenas();
						ref.eliminarAntenaIDEN = {};
						$('#eliminarAntenaIDEN').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEliminacionIDEN = function(){
				ref.eliminarAntenaIDEN = {};
				$('#eliminarAntenaIDEN').bPopup().close();
			}

			/*********************************************************************************************************/
			/*	Sesión																								 */
			/*********************************************************************************************************/

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};

			listarAntenas();
			listarMarcasMicroondas();
			listarMarcasModelosGUL();
			listarMarcasIDEN();
		}]);
</script>
</body>
</html>