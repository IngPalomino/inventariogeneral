<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="grupo in ctrl.grupos_electrogenos" ng-if="ctrl.grupos_electrogenos.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Grupo Electrógeno {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr class="text-center" style="background-color:#d2f3ce;">
								<td colspan="6">Grupo Electrógeno</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.tipo_grupo_electrogeno.marca}}</span>
									<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_grupo_electrogeno.marca" ng-change="grupo.tipo_grupo_electrogeno.id = '';">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.tipo_grupo_electrogeno.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
									</select>
								</td>
								<td class="info">Modelo</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.tipo_grupo_electrogeno.modelo}}</span>
									<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_grupo_electrogeno.id">
										<option value="">Elegir modelo</option>
										<option ng-if="grupo.tipo_grupo_electrogeno.marca" ng-repeat="modelo in ctrl.tipo_grupo_electrogeno.modelos | filter:{marca:grupo.tipo_grupo_electrogeno.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
								<td class="info">Número de serie</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.numero_serie_grupo_electrogeno}}</span>
									<input class="form-control" ng-hide="!grupo.editar" ng-model="grupo.numero_serie_grupo_electrogeno"/>
								</td>
							</tr>
							<tr class="text-center" style="background-color:#b9d1e5;">
								<td colspan="6">Motor</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.tipo_motor_ge.marca}}</span>
									<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_motor_ge.marca" ng-change="grupo.tipo_motor_ge.id = '';">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.tipo_motor_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
									</select>
								</td>
								<td class="info">Modelo</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.tipo_motor_ge.modelo}}</span>
									<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_motor_ge.id">
										<option value="">Elegir modelo</option>
										<option ng-if="grupo.tipo_motor_ge.marca" ng-repeat="modelo in ctrl.tipo_motor_ge.modelos | filter:{marca:grupo.tipo_motor_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
								<td class="info">Número de serie</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.numero_serie_motor}}</span>
									<input class="form-control" ng-hide="!grupo.editar" ng-model="grupo.numero_serie_motor"/>
								</td>
							</tr>
							<tr class="text-center" style="background-color:#d7e7b4;">
								<td colspan="6">Generador</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.tipo_generador_ge.marca}}</span>
									<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_generador_ge.marca" ng-change="grupo.tipo_generador_ge.id = '';">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.tipo_generador_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
									</select>
								</td>
								<td class="info">Modelo</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.tipo_generador_ge.modelo}}</span>
									<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_generador_ge.id">
										<option value="">Elegir modelo</option>
										<option ng-if="grupo.tipo_generador_ge.marca" ng-repeat="modelo in ctrl.tipo_generador_ge.modelos | filter:{marca:grupo.tipo_generador_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
								<td class="info">Número de serie</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.numero_serie_generador}}</span>
									<input class="form-control" ng-hide="!grupo.editar" ng-model="grupo.numero_serie_generador"/>
								</td>
							</tr>
							<tr>
								<td class="info">Potencia</td>
								<td class="default">
									<span ng-if="potencia.id == grupo.tipo_generador_ge.id" ng-repeat="potencia in ctrl.tipo_generador_ge.modelos">{{potencia.potencia}} kW</span>
								</td>
								<td class="info">Corriente</td>
								<td class="default">
									<span ng-if="corriente.id == grupo.tipo_generador_ge.id" ng-repeat="corriente in ctrl.tipo_generador_ge.modelos">{{corriente.corriente}} A</span>
								</td>
								<td class="info"></td>
								<td class="info"></td>
							</tr>
							<tr class="text-center" style="background-color:#e7d7b4;">
								<td colspan="6">AVR</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.tipo_avr_ge.marca}}</span>
									<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_avr_ge.marca" ng-change="grupo.tipo_avr_ge.id = '';">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.tipo_avr_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
									</select>
								</td>
								<td class="info">Modelo</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.tipo_avr_ge.modelo}}</span>
									<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_avr_ge.id">
										<option value="">Elegir modelo</option>
										<option ng-if="grupo.tipo_avr_ge.marca" ng-repeat="modelo in ctrl.tipo_avr_ge.modelos | filter:{marca:grupo.tipo_avr_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
								<td class="info">Número de serie</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.numero_serie_avr}}</span>
									<input class="form-control" ng-hide="!grupo.editar" ng-model="grupo.numero_serie_avr"/>
								</td>
							</tr>
							<tr class="text-center bg-success">
								<td colspan="6">Controlador GE</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.tipo_controlador_ge.marca}}</span>
									<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_controlador_ge.marca" ng-change="grupo.tipo_controlador_ge.id = '';">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.tipo_controlador_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
									</select>
								</td>
								<td class="info">Modelo</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.tipo_controlador_ge.modelo}}</span>
									<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_controlador_ge.id">
										<option value="">Elegir modelo</option>
										<option ng-if="grupo.tipo_controlador_ge.marca" ng-repeat="modelo in ctrl.tipo_controlador_ge.modelos | filter:{marca:grupo.tipo_controlador_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
								<td class="info">Número de serie</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.numero_serie_controlador_ge}}</span>
									<input class="form-control" ng-hide="!grupo.editar" ng-model="grupo.numero_serie_controlador_ge"/>
								</td>
							</tr>
							<tr class="text-center" style="background-color:#f7f7ca;">
								<td colspan="6">Controlador TTA</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.tipo_controlador_tta.marca}}</span>
									<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_controlador_tta.marca" ng-change="grupo.tipo_controlador_tta.id = '';">
										<option value="">Elegir marca</option>
										<option ng-repeat="marca in ctrl.tipo_controlador_tta.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
									</select>
								</td>
								<td class="info">Modelo</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.tipo_controlador_tta.modelo}}</span>
									<select class="form-control" ng-hide="!grupo.editar" ng-model="grupo.tipo_controlador_tta.id">
										<option value="">Elegir modelo</option>
										<option ng-if="grupo.tipo_controlador_tta.marca" ng-repeat="modelo in ctrl.tipo_controlador_tta.modelos | filter:{marca:grupo.tipo_controlador_tta.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
									</select>
								</td>
								<td class="info">Número de serie</td>
								<td class="default">
									<span ng-show="!grupo.editar">{{grupo.numero_serie_controlador_tta}}</span>
									<input class="form-control" ng-hide="!grupo.editar" ng-model="grupo.numero_serie_controlador_tta"/>
								</td>
							</tr>
							<tr>
								<td colspan="4"></td>
								<td style="font-size:8.5pt;list-style:none;text-align:right;" colspan="2">
									<span ng-if="!grupo.logs_grupos_electrogenos.length || grupo.logs_grupos_electrogenos[0].evento.tipo != 'INSERT'">Creado: {{grupo.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>
									<span ng-if="grupo.logs_grupos_electrogenos[0].evento.tipo == 'INSERT'">Creado: {{grupo.logs_grupos_electrogenos[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{grupo.logs_grupos_electrogenos[0].usuario.nombres}} {{grupo.logs_grupos_electrogenos[0].usuario.apellido_paterno}}</span>
									<br>
									<span ng-if="grupo.logs_grupos_electrogenos[1].evento.tipo == 'UPDATE'">Actualizado: {{grupo.logs_grupos_electrogenos[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{grupo.logs_grupos_electrogenos[1].usuario.nombres}} {{grupo.logs_grupos_electrogenos[1].usuario.apellido_paterno}}</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="text-center">
					<span class="fa fa-spinner fa-spin fa-2x" ng-if="ctrl.procesando"></span>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editarGrupo(grupo)" ng-show="!grupo.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
					<button class="btn btn-sm btn-success" ng-click="ctrl.grabarGrupo(grupo)" ng-hide="!grupo.editar" ng-disabled="grupo.disabled()">Grabar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarGrupo(grupo)" ng-hide="!grupo.editar">Cancelar</button>
					<button class="btn btn-sm btn-info" ng-click="ctrl.almacenandoGrupoElectrogeno(grupo)" ng-show="!grupo.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Almacenar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoGrupo(grupo)" ng-show="!grupo.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.grupos_electrogenos.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:15px;" ng-click="ctrl.nuevo_grupo()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir Grupo Electrógeno</button>
		</div>
	</div>
</div>

<div id="nuevoGrupo" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Nuevo Grupo Electrógeno</h3>
	
	<form>
		<table class="table table-condensed">
			<tbody>
				<tr class="text-center" style="background-color:#d2f3ce;">
					<td colspan="6">Grupo Electrógeno</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_grupo_electrogeno.marca" ng-change="ctrl.nuevoGrupo.tipo_grupo_electrogeno.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_grupo_electrogeno.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_grupo_electrogeno.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoGrupo.tipo_grupo_electrogeno.marca" ng-repeat="modelo in ctrl.tipo_grupo_electrogeno.modelos | filter:{marca:ctrl.nuevoGrupo.tipo_grupo_electrogeno.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.nuevoGrupo.numero_serie_grupo_electrogeno"/>
					</td>
				</tr>
				<tr class="text-center" style="background-color:#b9d1e5;">
					<td colspan="6">Motor</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_motor_ge.marca" ng-change="ctrl.nuevoGrupo.tipo_motor_ge.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_motor_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_motor_ge.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoGrupo.tipo_motor_ge.marca" ng-repeat="modelo in ctrl.tipo_motor_ge.modelos | filter:{marca:ctrl.nuevoGrupo.tipo_motor_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.nuevoGrupo.numero_serie_motor"/>
					</td>
				</tr>
				<tr class="text-center" style="background-color:#d7e7b4;">
					<td colspan="6">Generador</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_generador_ge.marca" ng-change="ctrl.nuevoGrupo.tipo_generador_ge.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_generador_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_generador_ge.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoGrupo.tipo_generador_ge.marca" ng-repeat="modelo in ctrl.tipo_generador_ge.modelos | filter:{marca:ctrl.nuevoGrupo.tipo_generador_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.nuevoGrupo.numero_serie_generador"/>
					</td>
				</tr>
				<tr>
					<td class="info">Potencia</td>
					<td class="default">
						<span ng-if="potencia.id == ctrl.nuevoGrupo.tipo_generador_ge.id" ng-repeat="potencia in ctrl.tipo_generador_ge.modelos">{{potencia.potencia}} kW</span>
					</td>
					<td class="info">Corriente</td>
					<td class="default">
						<span ng-if="corriente.id == ctrl.nuevoGrupo.tipo_generador_ge.id" ng-repeat="corriente in ctrl.tipo_generador_ge.modelos">{{corriente.corriente}} A</span>
					</td>
					<td class="info" colspan="2"></td>
				</tr>
				<tr class="text-center" style="background-color:#e7d7b4;">
					<td colspan="6">AVR</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_avr_ge.marca" ng-change="ctrl.nuevoGrupo.tipo_avr_ge.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_avr_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_avr_ge.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoGrupo.tipo_avr_ge.marca" ng-repeat="modelo in ctrl.tipo_avr_ge.modelos | filter:{marca:ctrl.nuevoGrupo.tipo_avr_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.nuevoGrupo.numero_serie_avr"/>
					</td>
				</tr>
				<tr class="text-center bg-success">
					<td colspan="6">Controlador GE</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_controlador_ge.marca" ng-change="ctrl.nuevoGrupo.tipo_controlador_ge.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_controlador_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_controlador_ge.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoGrupo.tipo_controlador_ge.marca" ng-repeat="modelo in ctrl.tipo_controlador_ge.modelos | filter:{marca:ctrl.nuevoGrupo.tipo_controlador_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.nuevoGrupo.numero_serie_controlador_ge"/>
					</td>
				</tr>
				<tr class="text-center" style="background-color:#f7f7ca;">
					<td colspan="6">Controlador TTA</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_controlador_tta.marca" ng-change="ctrl.nuevoGrupo.tipo_controlador_tta.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_controlador_tta.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_controlador_tta.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoGrupo.tipo_controlador_tta.marca" ng-repeat="modelo in ctrl.tipo_controlador_tta.modelos | filter:{marca:ctrl.nuevoGrupo.tipo_controlador_tta.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.nuevoGrupo.numero_serie_controlador_tta"/>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarGrupo(ctrl.nuevoGrupo)" ng-disabled="!ctrl.nuevoGrupo.tipo_grupo_electrogeno.id || !ctrl.nuevoGrupo.tipo_motor_ge.id || !ctrl.nuevoGrupo.tipo_generador_ge.id || !ctrl.nuevoGrupo.tipo_avr_ge.id || !ctrl.nuevoGrupo.tipo_controlador_ge.id || !ctrl.nuevoGrupo.tipo_controlador_tta.id">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionGrupo()">Cancelar</button>
	</form>
</div>

<div id="eliminarGrupo" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar Grupo Electrógeno</h3>
	
	<p>¿Está seguro de eliminar el grupo electrógeno?</p>
	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Grupo.comentario"></textarea>
	
	<button class="btn btn-success" ng-click="ctrl.eliminarGrupo()" ng-disabled="!ctrl.eliminar_Grupo.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>

<div id="almacenarGrupoElectrogeno" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Almacenar Grupo Electrógeno</h3>

	<p>Elegir destino:</p>

	<div style="max-width:400px">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a data-toggle="tab" href="#almacen" ng-click="ctrl.grupoElectrogenoOpcionAlmacen()">Almacén</a></li>
			<li><a data-toggle="tab" href="#sitio" ng-click="ctrl.grupoElectrogenoOpcionSitio()">Sitio</a></li>
		</ul>
		<div class="tab-content">
			<div id="almacen" class="tab-pane fade in active">
				<select class="form-control" ng-model="ctrl.almacenar_GrupoElectrogeno.almacen">
					<option value="">Elegir almacén</option>
					<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
				</select>
			</div>
			<div id="sitio" class="tab-pane fade">
				<label style="width:100%">
					<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenar_GrupoElectrogeno.a_sitio"></selectize>
				</label>
			</div>
		</div>
		<select class="form-control" ng-model="ctrl.almacenar_GrupoElectrogeno.estado">
			<option value="">Seleccionar estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select>
	</div><br>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea almacenar el elemento..." ng-model="ctrl.almacenar_GrupoElectrogeno.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.almacenarGrupoElectrogeno(ctrl.almacenar_GrupoElectrogeno)" ng-disabled="!ctrl.almacenar_GrupoElectrogeno.estado || !ctrl.almacenar_GrupoElectrogeno.comentario || (ctrl.almacenar_GrupoElectrogeno.tipo_almacen == 1 && !ctrl.almacenar_GrupoElectrogeno.almacen) || (ctrl.almacenar_GrupoElectrogeno.tipo_almacen == 2 && !ctrl.almacenar_GrupoElectrogeno.a_sitio)">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamientoGrupoElectrogeno()">Cancelar</button>
</div>