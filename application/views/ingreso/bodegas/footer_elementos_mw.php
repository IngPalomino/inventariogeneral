listarElementosMW = function() {
	$http({
		url: base_url("elementos_mw/listar"),
		method: "GET"
	}).then(function successCallback(resp) {
		if (resp.data.length) {
			ref.elemento_mw = resp.data;
		} else {
			ref.elemento_mw = [];
		}
	}, function errorCallback(resp) {

	});
}

ref.listarTipoElementoMW = function(tipo) {
	var data = '';

	ref.elemento_mw.forEach(function(elemento) {
		if (elemento.id == tipo) {
			data = elemento.tipo_elemento.substring(3);
		}
	});

	$http({
		url: base_url(data + '/listar'),
		method: 'GET'
	}).then(function successCallback(resp) {
		if (resp.data.marcas.length) {
			ref.tipo_elemento_mw = resp.data;
		} else {
			ref.tipo_elemento_mw = {};
		}
	}, function errorCallback(resp) {

	});
}

ref.disabledNuevoElementoMW = function() {
	if ( (ref.almacenarObj.tipo_almacen == 1 && ! ref.almacenarObj.almacen) || (ref.almacenarObj.tipo_almacen == 2 && ! ref.almacenarObj.a_sitio) || ! ref.almacenarObj.estado || ! ref.almacenarObj.elemento_mw || ! ref.almacenarObj.tipo_elemento_mw.id || ! ref.almacenarObj.numero_serie) {
		return true;
	}
}

listarElementosMW();