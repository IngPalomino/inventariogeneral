<script type="text/javascript">
	function serializeObj(obj, toDelete = []) {
		var result = [];

		if (!Array.isArray(toDelete)) {
			toDelete = toDelete.split(",");
		}

		if (toDelete.length) {
			toDelete.forEach(function(del) {
				delete obj[del];
			});
		}

		for (var property in obj) {
			if (property != 'id' && property != 'created_at' && property != null && typeof obj[property] != 'function') {
				if (Array.isArray(obj[property])) {
					result.push(encodeURIComponent(property) + '=' + angular.toJson(obj[property]));
				} else if (typeof obj[property] === 'object') {
					try { result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]['id'])); }
					catch(err) { result.push(encodeURIComponent(property) + '=' + null); }
				} else if (property == 'tipo_almacen') {
					if (obj[property] == 1) {
						result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
						result.push('almacen=' + encodeURIComponent(obj['almacen']));
					} else if (obj[property] == 2) {
						result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
						result.push('almacen=' + encodeURIComponent(obj['a_sitio']));
						delete obj['a_sitio'];
					}
				} else {
					result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
				}
			}
		}
		return result.join('&');

	}

	function base_url(segment = '') {
		return '<?php echo base_url(); ?>' + segment;
	}

	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http", "$scope", function($http, $scope) {
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			ref.auth.evaluador = (ref.session.evaluador == 1)? '' : 'disabled';

			ref.loading = false;
			ref.searched = false;

			ref.almacenes = [];
			ref.sitios = [];

			ref.config = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Buscar sitio',
				maxItems: 1
			};

			ref.configAlmacenes = {
				valueField: 'id',
				labelField: 'almacen',
				searchField: 'almacen',
				delimiter: '|',
				placeholder: 'Buscar almacen',
				maxItems: 1,
				inputClass: "form-control selectize-input",
				dropdownParent: "body",
				sortField: [
					{field: 'almacen', direction: 'asc'}
				]
			};

			ref.configSitios = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Buscar sitio',
				maxItems: 1,
				inputClass: "form-control selectize-input",
				dropdownParent: "body",
				sortField: [
					{field: 'nombre_completo', direction: 'asc'}
				]
			};

			ref.configObjetos = {
				valueField: 'objeto',
				labelField: 'nombre',
				searchField: 'nombre',
				delimiter: '|',
				placeholder: 'Buscar objeto',
				maxItems: 1,
				inputClass: "form-control selectize-input",
				dropdownParent: "body",
				sortField: [
					{field: 'nombre', direction: 'asc'}
				]
			};

			ref.busqueda = {
				almacen: "",
				sitio: "",
				objeto: ""
			};

			ref.almacenarObj = {
				tipo_almacen: 1
			};

			ref.objetos = [];
			ref.elementos_busqueda = [];
			ref.eliminarElemento = {};
			ref.moverElemento = {};


			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						console.log(data);
						ref.almacenes = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarSitios = function(){
				$http({
					url:"<?php echo base_url("status_site/listar_sitios_total");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sitios = data;
					}
				}).error(function(err){

				});
			}

			listarObjetos = function(){
				$http({
					url:"<?php echo base_url("objetos_bodegas/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.objetos = data;
					}
				}).error(function(err){

				});
			}

			listarObjetosAlt = function() {
				$http({
					url: base_url("objetos_bodegas/listar"),
					method: "POST",
					data: "objeto=antenas_iden",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function successCallback(resp) {
					if (resp.data.length) {
						ref.objetos_alt = resp.data;
					} else {
						ref.objetos_alt = [];
					}
				}, function errorCallback(resp) {

				});
			}

			ref.buscarAlmacenes = function(almacen){
				ref.loading = true;
				ref.busqueda.sitio = "";
				ref.busqueda.objeto = "";

				$http({
					url:"<?php echo base_url("objetos_bodegas/busqueda");?>",
					method:"POST",
					data:"almacen="+almacen,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){console.log(data);
					ref.loading = false;
					ref.searched = true;
					if(data.length)
					{
						ref.elementos_busqueda = data;
					}
					else
					{
						ref.elementos_busqueda = [];
					}
				}).error(function(err){
					ref.loading = false;
				});
			}

			ref.buscarSitios = function(sitio){
				ref.loading = true;
				ref.busqueda.almacen = "";
				ref.busqueda.objeto = "";

				$http({
					url:"<?php echo base_url("objetos_bodegas/busqueda");?>",
					method:"POST",
					data:"sitio="+sitio,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					ref.loading = false;
					ref.searched = true;
					if(data.length)
					{
						ref.elementos_busqueda = data;
					}
					else
					{
						ref.elementos_busqueda = [];
					}
				}).error(function(err){
					ref.loading = false;
				});
			}

			ref.buscarObjetos = function(objeto){
				ref.loading = true;
				ref.busqueda.almacen = "";
				ref.busqueda.sitio = "";

				$http({
					url:"<?php echo base_url("objetos_bodegas/busqueda");?>",
					method:"POST",
					data:"objeto="+objeto,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					ref.loading = false;
					ref.searched = true;
					if(data.length)
					{
						ref.elementos_busqueda = data;
						//console.log(ref.elementos_busqueda);
						ref.datainfo = JSON.parse(ref.elementos_busqueda[0].info);
						//console.log(ref.datainfo);
					}
					else
					{
						ref.elementos_busqueda = [];
					}
				}).error(function(err){
					ref.loading = false;
				});
			}

			$('#almacenarObjeto').on('hidden.bs.modal', function() {
				ref.almacenarObj = {
					tipo_almacen: 1
				};
				ref.tipo_elemento_mw = {};
				ref.nuevoBanco = {
					tipo_almacen: 1,
					celdas: [],
					nuevaCelda: function() {
						this.numero_serie = '';
					}
				};
			});

			ref.almacenandoObjeto = function() {
                //llama al popup de elementos 
				$("#almacenarObjeto").modal("show");
				$('#almacenarObjeto').modal('handleUpdate');
				ref.editarPopup = false;
				ref.almacenarPopup = true;
			}

			$('#editarObjeto').on('hidden.bs.modal', function() {
				ref.editarObj = {};
			});

			ref.editandoObjeto = function(info, link) {
				$('#editarObjeto').modal('show');
				$('#editarObjeto').modal('handleUpdate');
				ref.editarPopup = true;
				ref.almacenarPopup = false;
				ref.editarObj = info;
				ref.editarObj.link = link;
				ref.objetoEditar = link;

				if (ref.objetoEditar == 'elementos_mw') {
					ref.listarTipoElementoMW(ref.editarObj.elemento_mw.id);
				}

				if (ref.objetoEditar == 'elementos_sistema_combustible') {
					ref.listarTipoElementoSC(ref.editarObj.elemento_sistema_combustible.id);
				}

				if (ref.objetoEditar == 'bancos_baterias') {
					ref.editarObj.confiabilidad = Number(ref.editarObj.confiabilidad);
				}
			}

			ref.almacenando = function() {
				$("#almacenar").bPopup();
				ref.editarPopup = false;
				ref.almacenarPopup = true;
			}

			ref.almacenar = function(info, toDelete = []) {
				/*console.log(serializeObj(info));*/

				$http({
					url: base_url("bodega_" + ref.objetoAlmacenar + "/almacenar"),
					method: "POST",
					data: serializeObj(info, toDelete),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function successCallback(resp) {
					if (resp.data.error) {
						ref.error.almacenar = resp.data.message;
					} else {
						$("#almacenar").bPopup().close();
						$('#almacenarObjeto').modal('hide');
						info = {};
						ref.error.almacenar = "";
					}
				}, function errorCallback(resp) {

				});
			}

			ref.cancelarAlmacenamiento = function(info) {
				$("#almacenar").bPopup().close();
				info = {};
			}

			ref.getContent = function(content, original, borrar) {
				document.querySelector(content).appendChild(document.querySelector(original));
				document.querySelector(borrar).innerHTML = "";
			}

			ref.panel = {
				index: 1,
				opcionSitio: function(obj) {
					this.index = 2;
					obj.tipo_almacen = 2;
				},
				opcionAlmacen: function(obj) {
					this.index = 1;
					obj.tipo_almacen = 1;
				}
			};

			ref.opcionSitio = function(obj){
				obj.tipo_almacen = 2;
			}

			ref.opcionAlmacen = function(obj){
				obj.tipo_almacen = 1;
			}

			ref.moviendo = function(elemento, link) {
				ref.moverElemento = elemento;
				ref.moverElemento.link = link;
				ref.moverElemento.tipo_almacen = 1;
				$("#mover").bPopup();
			}

			ref.mover = function() {
				var data = "tipo_almacen=" + ref.moverElemento.tipo_almacen
							+"&almacen=" + ((ref.moverElemento.tipo_almacen == 1)? ref.moverElemento.almacen : ref.moverElemento.a_sitio)
							+"&comentario=" + ref.moverElemento.comentario;

				$http({
					url: base_url("bodega_" +ref.moverElemento.link + "/mover/" + ref.moverElemento.id),
					method: "PUT",
					data: data,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function successCallback(resp) {
					ref.elementos_busqueda = [];
					ref.searched = false;
					ref.moverElemento = {};
					$("#mover").bPopup().close();
				}, function errorCallback(resp) {

				});
			}

			ref.cancelarMovimiento = function() {
				ref.moverElemento = {};
				$("#mover").bPopup().close();
			}

			ref.editando = function(elemento, link) {
				ref.editar = elemento;
				ref.editar.link = link;
				ref.objetoEditar = link;
				$("#editar").bPopup();
				ref.almacenarPopup = false;
				ref.editarPopup = true;

				ref.editar.hasOwnProperty("capacidad") ? ref.editar.capacidad = Number(ref.editar.capacidad) : "";
				ref.editar.hasOwnProperty("consumo") ? ref.editar.consumo = Number(ref.editar.consumo) : "";
			}

			ref.grabar = function(info, toDelete = []) {
				/*console.log(serializeObj(info, toDelete));*/
				$http({
					url: base_url("bodega_" + info.link + "/actualizar/" + info.id),
					method: "POST",
					data: serializeObj(info, toDelete),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function successCallback(resp) {
					ref.elementos_busqueda = [];
					ref.searched = false;
					ref.editar = {};
					$("#editar").bPopup().close();
					$('#editarObjeto').modal('hide');
				}, function errorCallback(resp) {

				});
			}

			ref.cancelarEdicion = function() {
				ref.editar = {};
				$("#editar").bPopup().close();
			}

			ref.eliminando = function(elemento, link) {
				ref.eliminarElemento = elemento;
				ref.eliminarElemento.link = link;
				$("#eliminar").bPopup();

		
			}

			ref.eliminar = function() {
				$http({
					url: base_url("bodega_" + ref.eliminarElemento.link + "/eliminar/" + ref.eliminarElemento.id),
					method: "POST",
					data: "comentario=" + ref.eliminarElemento.comentario,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function successCallback(resp) {
					
                    ref.elementos_busqueda = [];
					ref.searched = false;
					ref.eliminarElemento = {};


					$("#eliminar").bPopup().close();

				}, function errorCallback(resp) {

				});
			}

			ref.cancelarEliminacion = function() {
				ref.eliminarElemento = {};
				$("#eliminar").bPopup().close();
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						ref.reestablecerContrasena = {};
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
				ref.reestablecerContrasena = {};
			};

			listarAlmacenes();
			listarSitios();
			listarObjetos();
			listarObjetosAlt();

			<?php //include("/var/www/html/oym/inventarioGeneral/application/views/ingreso/bodegas/footer_aires_acondicionados.php"); ?>
			<?php //include("/var/www/html/oym/inventarioGeneral/application/views/ingreso/bodegas/footer_antenas_gul.php"); ?>
			<?php //include("/var/www/html/oym/inventarioGeneral/application/views/ingreso/bodegas/footer_bancos_baterias.php"); ?>
			<?php //include("/var/www/html/oym/inventarioGeneral/application/views/ingreso/bodegas/footer_grupos_electrogenos.php"); ?>
			<?php //include("/var/www/html/oym/inventarioGeneral/application/views/ingreso/bodegas/footer_modulos_rectif.php"); ?>
			<?php //include("/var/www/html/oym/inventarioGeneral/application/views/ingreso/bodegas/footer_tanques_combustible.php"); ?>
			<?php //include("/var/www/html/oym/inventarioGeneral/application/views/ingreso/bodegas/footer_elementos_mw.php"); ?>

//----------- local
            <?php include("footer_aires_acondicionados.php"); ?>
			<?php include("footer_antenas_gul.php"); ?>
			<?php include("footer_bancos_baterias.php"); ?>
			<?php include("footer_grupos_electrogenos.php"); ?>
			<?php include("footer_modulos_rectif.php"); ?>
			<?php include("footer_tanques_combustible.php"); ?>
			<?php include("footer_elementos_mw.php"); ?>
			<?php include("footer_elementos_sistema_combustible.php"); ?>

		}]);
</script>
</body>
</html>