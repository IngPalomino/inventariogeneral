<div ng-show="ctrl.almacenarPopup">
	<h3>Almacenar Módulos Rectificadores</h3>

	<div style="max-width:500px;height:350px;overflow:auto;">
		<ul class="nav nav-tabs nav-justified">
			<li ng-class="{active: ctrl.panel.index == 1}"><a href ng-click="ctrl.panel.opcionAlmacen(ctrl.nuevosModulos)">Almacén</a></li>
			<li ng-class="{active: ctrl.panel.index == 2}"><a href ng-click="ctrl.panel.opcionSitio(ctrl.nuevosModulos)">Sitio</a></li>
		</ul>
		<div ng-show="ctrl.panel.index == 1">
			<select class="form-control" ng-model="ctrl.nuevosModulos.almacen">
				<option value="">Elegir almacén</option>
				<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
			</select>
		</div>
		<div ng-show="ctrl.panel.index == 2">
			<label style="width:100%">
				<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.nuevosModulos.a_sitio"></selectize>
			</label>
		</div>
		<select class="form-control" ng-model="ctrl.nuevosModulos.estado">
			<option value="">Seleccionar estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select>

		<table class="table table-condensed">
			<tbody> 
				
				<tr>
					<td class="info">Modelos</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevosModulos.tipo_modulos_rectif">
							<option value="">Elegir modelo</option>
							<option ng-repeat="modelo in ctrl.tipo_modulos_rectif" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>

				<tr>
					<td class="info">Cantidad de Módulos</td>
					<td class="default">
						<input class="form-control" type="number" ng-model="ctrl.nuevosModulos.cantidad" ng-change="ctrl.numeroModulos(ctrl.nuevosModulos, ctrl.nuevosModulos.cantidad)">
					</td>
				</tr>
				<tr ng-show="ctrl.nuevosModulos.numeros_serie.length">
					<td class="success text-center" colspan="2">Números de Serie</td>
				</tr>
				<tr ng-repeat="modulo in ctrl.nuevosModulos.numeros_serie">
					<td class="info">Módulo {{$index + 1}}</td>
					<td class="active">
						<input class="form-control" type="text" placeholder="Número de Serie" ng-model="modulo.numero_serie">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<button class="btn btn-success" ng-click="ctrl.almacenar(ctrl.nuevosModulos)" ng-disabled="ctrl.disabledNuevosModulos()">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamiento(ctrl.nuevosModulos)">Cancelar</button>
</div>

<div ng-show="ctrl.editarPopup">
	<h3>Editar Módulos Rectificadores</h3>

	<div style="max-width:500px;overflow:auto;">
		<table class="table table-condensed">
			<tbody>
			
				<tr>
					<td class="info">Estado del equipo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.editar.estado.id">
							<option value="">Seleccionar estado del equipo</option>
							<option value="1">Nuevo</option>
							<option value="2">Operativo</option>
							<option value="3">De baja</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.editar.tipo_modulos_rectif.id">
							<option value="">Elegir modelo</option>
							<option ng-repeat="modelo in ctrl.tipo_modulos_rectif" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>

				<tr>
					<td class="info">Número de Serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="ctrl.editar.numero_serie">
					</td>
				</tr>


			</tbody>
		</table>
	</div>
	<button class="btn btn-success" ng-click="ctrl.grabar(ctrl.editar)">Guardar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEdicion()">Cancelar</button>
</div>