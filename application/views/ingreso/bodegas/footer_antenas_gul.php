ref.nuevaAntenaGUL = {
	tipo_almacen: 1
};

listarTipoAntenasGUL = function(){
	$http({
		url: "<?php echo base_url("tipo_antena_gul/listar");?>",
		method: "GET"
	}).then(function successCallback(resp) {
		if(resp.data.marcas.length) {
			ref.tipo_antena_gul = resp.data;
		} else {
			ref.tipo_antena_gul = [];
		}
	}, function errorCallback(resp) {

	});
}

ref.disabledNuevaAntenaGUL = function() {
	if ( (ref.nuevaAntenaGUL.tipo_almacen == 1 && !ref.nuevaAntenaGUL.almacen) || (ref.nuevaAntenaGUL.tipo_almacen == 2 && !ref.nuevaAntenaGUL.a_sitio) || ! ref.nuevaAntenaGUL.estado || ! ref.nuevaAntenaGUL.numero_serie || ! ref.nuevaAntenaGUL.tipo_antena_gul.id) {
		return true;
	}
}

listarTipoAntenasGUL();