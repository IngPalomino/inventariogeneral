<div ng-show="ctrl.almacenarPopup">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Almacenar Elemento Microondas</h4>
	</div>
	<div class="modal-body">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<ul class="nav nav-tabs nav-justified">
						<li ng-class="{active: ctrl.panel.index == 1}"><a href ng-click="ctrl.panel.opcionAlmacen(ctrl.almacenarObj)">Almacén</a></li>
						<li ng-class="{active: ctrl.panel.index == 2}"><a href ng-click="ctrl.panel.opcionSitio(ctrl.almacenarObj)">Sitio</a></li>
					</ul>
					<div ng-show="ctrl.panel.index == 1">
						<select class="form-control" ng-model="ctrl.almacenarObj.almacen">
							<option value="">Elegir almacén</option>
							<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
						</select>
					</div>
					<div ng-show="ctrl.panel.index == 2">
						<label style="width:100%">
							<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenarObj.a_sitio"></selectize>
						</label>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4 text-right"><label>Estado del equipo:</label></div>
				<div class="col-md-7">
					<select class="form-control input-sm" ng-model="ctrl.almacenarObj.estado">
						<option value="">Seleccionar estado del equipo</option>
						<option value="1">Nuevo</option>
						<option value="2">Operativo</option>
						<option value="3">De baja</option>
					</select>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4 text-right"><label>Elemento:</label></div>
				<div class="col-md-7">
					<select class="form-control input-sm" ng-model="ctrl.almacenarObj.elemento_mw" ng-change="ctrl.listarTipoElementoMW(ctrl.almacenarObj.elemento_mw); ctrl.almacenarObj.tipo_elemento_mw.marca = ''; ctrl.almacenarObj.tipo_elemento_mw.id = '';">
						<option value="">Seleccionar elemento</option>
						<option ng-repeat="elemento in ctrl.elemento_mw" value="{{elemento.id}}">{{elemento.elemento_mw}}</option>
					</select>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4 text-right"><label>Marca:</label></div>
				<div class="col-md-7">
					<select class="form-control input-sm" ng-model="ctrl.almacenarObj.tipo_elemento_mw.marca" ng-change="ctrl.almacenarObj.tipo_elemento_mw.id = '';">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_elemento_mw.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4 text-right"><label>Modelo:</label></div>
				<div class="col-md-7">
					<select class="form-control input-sm" ng-model="ctrl.almacenarObj.tipo_elemento_mw.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.almacenarObj.tipo_elemento_mw.marca" ng-repeat="modelo in ctrl.tipo_elemento_mw.modelos | filter: {marca: ctrl.almacenarObj.tipo_elemento_mw.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4 text-right"><label>Número de serie:</label></div>
				<div class="col-md-7">
					<input class="form-control input-sm" type="text" ng-model="ctrl.almacenarObj.numero_serie">
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
		<button class="btn btn-primary" ng-click="ctrl.almacenar(ctrl.almacenarObj)" ng-disabled="ctrl.disabledNuevoElementoMW()">Almacenar</button>
	</div>
</div>

<div ng-show="ctrl.editarPopup">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Editar Elemento Microondas</h4>
	</div>
	<div class="modal-body">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4 text-right"><label>Estado del equipo:</label></div>
				<div class="col-md-7">
					<select class="form-control input-sm" ng-model="ctrl.editarObj.estado.id">
						<option value="">Seleccionar estado del equipo</option>
						<option value="1">Nuevo</option>
						<option value="2">Operativo</option>
						<option value="3">De baja</option>
					</select>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4 text-right"><label>Elemento:</label></div>
				<div class="col-md-7">
					<select class="form-control input-sm" ng-model="ctrl.editarObj.elemento_mw.id" ng-change="ctrl.listarTipoElementoMW(ctrl.editarObj.elemento_mw.id); ctrl.editarObj.tipo_elemento_mw.marca = ''; ctrl.editarObj.tipo_elemento_mw.id = '';">
						<option value="">Seleccionar elemento</option>
						<option ng-repeat="elemento in ctrl.elemento_mw" value="{{elemento.id}}">{{elemento.elemento_mw}}</option>
					</select>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4 text-right"><label>Marca:</label></div>
				<div class="col-md-7">
					<select class="form-control input-sm" ng-model="ctrl.editarObj.tipo_elemento_mw.marca" ng-change="ctrl.editarObj.tipo_elemento_mw.id = '';">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_elemento_mw.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4 text-right"><label>Modelo:</label></div>
				<div class="col-md-7">
					<select class="form-control input-sm" ng-model="ctrl.editarObj.tipo_elemento_mw.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editarObj.tipo_elemento_mw.marca" ng-repeat="modelo in ctrl.tipo_elemento_mw.modelos | filter: {marca: ctrl.editarObj.tipo_elemento_mw.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4 text-right"><label>Número de serie:</label></div>
				<div class="col-md-7">
					<input class="form-control input-sm" type="text" ng-model="ctrl.editarObj.numero_serie">
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
		<button class="btn btn-primary" ng-click="ctrl.grabar(ctrl.editarObj)" ng-disabled="ctrl.disabledEditarElementoMW()">Grabar</button>
	</div>
</div>