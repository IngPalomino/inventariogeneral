ref.nuevosModulos = {
	tipo_almacen: 1,
	numeros_serie: [],
	nuevoNumeroSerie: function() {
		this.numero_serie = "";
	}
};

listarTipoModulosRectif = function() {
	$http({
		url: "<?php echo base_url("tipo_modulos_rectif/listar");?>",
		method: "GET"
	}).then(function successCallback(resp) {
		if (resp.data.length) {
			ref.tipo_modulos_rectif = resp.data;
		} else {
			ref.tipo_modulos_rectif = [];
		}
	}, function errorCallback(resp) {

	});
}

ref.numeroModulos = function(modulos, numero) {
	modulos.numeros_serie = [];
	
	for (var i = 0; i < numero; i++) {
		modulos.numeros_serie.push(new modulos.nuevoNumeroSerie());
	}
}

ref.disabledNuevosModulos = function() {
	var dis = false;
	if ( (ref.nuevosModulos.tipo_almacen == 1 && ! ref.nuevosModulos.almacen) || (ref.nuevosModulos.tipo_almacen == 2 && ! ref.nuevosModulos.a_sitio) || ! ref.nuevosModulos.estado || ! ref.nuevosModulos.tipo_modulos_rectif || ! ref.nuevosModulos.numeros_serie.length) {
		dis = true;
	}

	if (ref.nuevosModulos.numeros_serie.length) {
		ref.nuevosModulos.numeros_serie.forEach(function(modulo) {
			if ( ! modulo.numero_serie) {
				dis = true;
			}
		});
	}

	return dis;
}

listarTipoModulosRectif();