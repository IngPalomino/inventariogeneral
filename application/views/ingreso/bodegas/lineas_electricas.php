<div ng-show="ctrl.almacenarPopup">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Almacenar Elemento de Línea Eléctrica</h4>
	</div>
	<div class="modal-body">
		<div class="container-fluid modal-format">
			<div class="col-md-10 col-md-offset-1">
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs nav-justified">
							<li ng-class="{active: ctrl.panel.index == 1}"><a href ng-click="ctrl.panel.opcionAlmacen(ctrl.almacenarObj)">Almacén</a></li>
							<li ng-class="{active: ctrl.panel.index == 2}"><a href ng-click="ctrl.panel.opcionSitio(ctrl.almacenarObj)">Sitio</a></li>
						</ul>
						<div ng-show="ctrl.panel.index == 1">
							<select class="form-control" ng-model="ctrl.almacenarObj.almacen">
								<option value="">Elegir almacén</option>
								<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
							</select>
						</div>
						<div ng-show="ctrl.panel.index == 2">
							<label style="width:100%">
								<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.almacenarObj.a_sitio"></selectize>
							</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5"><label>Estado del equipo:</label></div>
					<div class="col-md-7">
						<select class="form-control" ng-model="ctrl.almacenarObj.estado">
							<option value="">Seleccionar estado del equipo</option>
							<option value="1">Nuevo</option>
							<option value="2">Operativo</option>
							<option value="3">De baja</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5"><label>Elemento:</label></div>
					<div class="col-md-7">
						<select class="form-control" ng-model="ctrl.almacenarObj.elemento">
							<option value="">Seleccionar elemento</option>
							<option value="1">Transformador</option>
							<option value="2">Transformix</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5"><label>Marca:</label></div>
					<div class="col-md-7">
						<select class="form-control" ng-model="ctrl.almacenarObj.tipo_elemento">
							<option value="">Elegir marca</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5"><label>Número de Serie:</label></div>
					<div class="col-md-7">
						<input class="form-control" type="text" ng-model="ctrl.almacenarObj.numero_serie">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
		<button class="btn btn-primary">Almacenar</button>
	</div>
</div>

<div ng-show="ctrl.editarPopup">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Editar Elemento de Línea Eléctrica</h4>
	</div>
	<div class="modal-body"></div>
</div>