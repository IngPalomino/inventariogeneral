ref.nuevoBanco = {
	tipo_almacen: 1,
	celdas: [],
	nuevaCelda: function() {
		this.numero_serie = "";
	}
};


listarTipoBancosBaterias = function(){
	$http({
		url: base_url("tipo_bancos_baterias/listar"),
		method: "GET"
	}).then(function successCallback(resp) {
		if(resp.data.marcas.length) {
			ref.tipo_bancos_baterias = resp.data;
		} else {
			ref.tipo_bancos_baterias = [];
		}
	}, function errorCallback(resp) {

	});
}

ref.nuevaCelda = function(banco) {
	banco.celdas.push(new banco.nuevaCelda());
}

ref.eliminarCelda = function(index, banco) {
	banco.celdas.splice(index, 1);
}

ref.disabledNuevoBanco = function() {
	var dis = false;

	if ( (ref.nuevoBanco.tipo_almacen == 1 && ! ref.nuevoBanco.almacen) || (ref.nuevoBanco.tipo_almacen == 2 && ! ref.nuevoBanco.a_sitio) || ! ref.nuevoBanco.estado || ! ref.nuevoBanco.tipo_bancos_baterias.id || ! ref.nuevoBanco.confiabilidad || ! ref.nuevoBanco.celdas.length) {
		dis = true;
	}

	if (ref.nuevoBanco.celdas.length) {
		ref.nuevoBanco.celdas.forEach(function(celda) {
			if ( ! celda.numero_serie) {
				dis = true;
			}
		});
	}

	return dis;
}

listarTipoBancosBaterias();