ref.nuevoAire = {
	tipo_almacen: 1
};

listarTipoAiresAcondicionados = function() {
	$http({
		url: "<?php echo base_url('tipo_aires_acondicionados/listar');?>",
		method: "GET"
	}).then(function successCallback(resp) {
		if(resp.data.marcas.length) {
			ref.tipo_aires_acondicionados = resp.data;
		} else {
			ref.tipo_aires_acondicionados = [];
		}
	}, function errorCallback(resp) {

	});
}

listarTipoCompresor = function() {
	$http({
		url: "<?php echo base_url('tipo_compresor/listar');?>",
		method: "GET"
	}).then(function successCallback(resp) {
		if(resp.data.marcas.length) {
			ref.tipo_compresor = resp.data;
		} else {
			ref.tipo_compresor = [];
		}
	}, function errorCallback(resp) {

	});
}

listarTipoCondensador = function() {
	$http({
		url: "<?php echo base_url('tipo_condensador/listar');?>",
		method: "GET"
	}).then(function successCallback(resp) {
		if(resp.data.length) {
			ref.tipo_condensador = resp.data;
		} else {
			ref.tipo_condensador = [];
		}
	}, function errorCallback(resp) {

	});
}

listarTipoEvaporador = function() {
	$http({
		url: "<?php echo base_url('tipo_evaporador/listar');?>",
		method: "GET"
	}).then(function successCallback(resp) {
		if(resp.data.length) {
			ref.tipo_evaporador = resp.data;
		} else {
			ref.tipo_evaporador = [];
		}
	}, function errorCallback(resp) {

	});
}

listarTipoRefrigerante = function() {
	$http({
		url: "<?php echo base_url('tipo_refrigerante/listar');?>",
		method: "GET"
	}).then(function successCallback(resp){
		if(resp.data.length) {
			ref.tipo_refrigerante = resp.data;
		} else {
			ref.tipo_refrigerante = [];
		}
	}, function errorCallback(resp) {

	});
}

ref.cambioTipo = function(tipo_equipo,aire) {
	if(tipo_equipo == "Mochila") {
		aire.tipo_condensador = 1;
		aire.tipo_evaporador = 1;
		aire.num_serie_condensador = 'N/A';
		aire.num_serie_evaporador = 'N/A';
		aire.num_serie_aire_acon = '';
	} else {
		aire.tipo_condensador = "";
		aire.tipo_evaporador = "";
		aire.num_serie_condensador = '';
		aire.num_serie_evaporador = '';
		aire.num_serie_aire_acon = 'N/A';
	}
}

ref.cambioTipoNuevo = function(tipo_equipo, nuevoAire) {
	if(tipo_equipo == "Mochila") {
		ref.nuevoAire.tipo_condensador = 1;
		ref.nuevoAire.tipo_evaporador = 1;
		ref.nuevoAire.num_serie_condensador = 'N/A';
		ref.nuevoAire.num_serie_evaporador = 'N/A';
		ref.nuevoAire.num_serie_aire_acon = '';
	} else {
		ref.nuevoAire.tipo_condensador = "";
		ref.nuevoAire.tipo_evaporador = "";
		ref.nuevoAire.num_serie_condensador = '';
		ref.nuevoAire.num_serie_evaporador = '';
		ref.nuevoAire.num_serie_aire_acon = 'N/A';
	}
}

ref.disabledNuevoAire = function() {
	if ( (ref.nuevoAire.tipo_almacen == 1 && !ref.nuevoAire.almacen) || (ref.nuevoAire.tipo_almacen == 2 && !ref.nuevoAire.a_sitio) || ! ref.nuevoAire.tipo_refrigerante || ! ref.nuevoAire.tipo_aires_acondicionados.id || ! ref.nuevoAire.num_serie_aire_acon || ! ref.nuevoAire.tipo_compresor.id) {
		return true;
	}
}

ref.disabledEditarAire = function() {
	if ( ! ref.editar.estado.id) {
		return true;
	}
}

listarTipoAiresAcondicionados();
listarTipoCompresor();
listarTipoCondensador();
listarTipoEvaporador();
listarTipoRefrigerante();