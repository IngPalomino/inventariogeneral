ref.nuevoGrupo = {
	tipo_almacen: 1
};

listarTipoGrupoElectrogeno = function() {
	$http({
		url: base_url("tipo_grupo_electrogeno/listar"),
		method: "GET"
	}).then(function successCallback(resp) {
		if (resp.data.marcas.length) {
			ref.tipo_grupo_electrogeno = resp.data;
		} else {
			ref.tipo_grupo_electrogeno = [];
		}
	}, function errorCallback(resp) {

	});
}

listarTipoMotorGe = function(){
	$http({
		url: base_url("tipo_motor_ge/listar"),
		method: "GET"
	}).then(function successCallback(resp) {
		if (resp.data.marcas.length) {
			ref.tipo_motor_ge = resp.data;
		} else {
			ref.tipo_motor_ge = [];
		}
	}, function errorCallback(resp) {

	});
}

listarTipoGeneradorGe = function(){
	$http({
		url: base_url("tipo_generador_ge/listar"),
		method: "GET"
	}).then(function successCallback(resp) {
		if (resp.data.marcas.length) {
			ref.tipo_generador_ge = resp.data;
		} else {
			ref.tipo_generador_ge = [];
		}
	}, function errorCallback(resp) {

	});
}

listarTipoAVRGe = function(){
	$http({
		url: base_url("tipo_avr_ge/listar"),
		method: "GET"
	}).then(function successCallback(resp) {
		if (resp.data.marcas.length) {
			ref.tipo_avr_ge = resp.data;
		} else {
			ref.tipo_avr_ge = [];
		}
	}, function errorCallback(resp) {

	});
}

listarTipoControladorGe = function(){
	$http({
		url: base_url("tipo_controlador_ge/listar"),
		method: "GET"
	}).then(function successCallback(resp) {
		if (resp.data.marcas.length) {
			ref.tipo_controlador_ge = resp.data;
		} else {
			ref.tipo_controlador_ge = [];
		}
	}, function errorCallback(resp) {

	});
}

listarTipoControladorTTA = function(){
	$http({
		url: base_url("tipo_controlador_tta/listar"),
		method: "GET"
	}).then(function successCallback(resp) {
		if (resp.data.marcas.length) {
			ref.tipo_controlador_tta = resp.data;
		} else {
			ref.tipo_controlador_tta = [];
		}
	}, function errorCallback(resp) {

	});
}

ref.disabledNuevoGrupo = function() {
	if ( (ref.nuevoGrupo.tipo_almacen == 1 && !ref.nuevoGrupo.almacen) || (ref.nuevoGrupo.tipo_almacen == 2 && !ref.nuevoGrupo.a_sitio) || ! ref.nuevoGrupo.estado || ! ref.nuevoGrupo.tipo_grupo_electrogeno.id || ! ref.nuevoGrupo.numero_serie_grupo_electrogeno || ! ref.nuevoGrupo.tipo_motor_ge.id || ! ref.nuevoGrupo.numero_serie_motor || ! ref.nuevoGrupo.tipo_generador_ge.id || ! ref.nuevoGrupo.numero_serie_generador || ! ref.nuevoGrupo.tipo_avr_ge.id || ! ref.nuevoGrupo.numero_serie_avr || ! ref.nuevoGrupo.tipo_controlador_ge.id || ! ref.nuevoGrupo.numero_serie_controlador_ge) {
		return true;
	}
}

listarTipoGrupoElectrogeno();
listarTipoMotorGe();
listarTipoGeneradorGe();
listarTipoAVRGe();
listarTipoControladorGe();
listarTipoControladorTTA();