<div ng-show="ctrl.almacenarPopup">
	<h3>Almacenar Grupo Electrógeno</h3>
	
	<div style="max-width:900px;height:450px;overflow:auto;">
		<ul class="nav nav-tabs nav-justified">
			<li ng-class="{active: ctrl.panel.index == 1}"><a href ng-click="ctrl.panel.opcionAlmacen(ctrl.nuevoGrupo)">Almacén</a></li>
			<li ng-class="{active: ctrl.panel.index == 2}"><a href ng-click="ctrl.panel.opcionSitio(ctrl.nuevoGrupo)">Sitio</a></li>
		</ul>
		<div ng-show="ctrl.panel.index == 1">
			<select class="form-control" ng-model="ctrl.nuevoGrupo.almacen">
				<option value="">Elegir almacén</option>
				<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
			</select>
		</div>
		<div ng-show="ctrl.panel.index == 2">
			<label style="width:100%">
				<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.nuevoGrupo.a_sitio"></selectize>
			</label>
		</div>
		<select class="form-control" ng-model="ctrl.nuevoGrupo.estado">
			<option value="">Seleccionar estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select>

		<table class="table table-condensed">
			<tbody>
				<tr class="text-center" style="background-color:#d2f3ce;">
					<td colspan="6">Grupo Electrógeno</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_grupo_electrogeno.marca" ng-change="ctrl.nuevoGrupo.tipo_grupo_electrogeno.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_grupo_electrogeno.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_grupo_electrogeno.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoGrupo.tipo_grupo_electrogeno.marca" ng-repeat="modelo in ctrl.tipo_grupo_electrogeno.modelos | filter:{marca:ctrl.nuevoGrupo.tipo_grupo_electrogeno.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.nuevoGrupo.numero_serie_grupo_electrogeno"/>
					</td>
				</tr>
				<tr class="text-center" style="background-color:#b9d1e5;">
					<td colspan="6">Motor</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_motor_ge.marca" ng-change="ctrl.nuevoGrupo.tipo_motor_ge.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_motor_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_motor_ge.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoGrupo.tipo_motor_ge.marca" ng-repeat="modelo in ctrl.tipo_motor_ge.modelos | filter:{marca:ctrl.nuevoGrupo.tipo_motor_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.nuevoGrupo.numero_serie_motor"/>
					</td>
				</tr>
				<tr class="text-center" style="background-color:#d7e7b4;">
					<td colspan="6">Generador</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_generador_ge.marca" ng-change="ctrl.nuevoGrupo.tipo_generador_ge.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_generador_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_generador_ge.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoGrupo.tipo_generador_ge.marca" ng-repeat="modelo in ctrl.tipo_generador_ge.modelos | filter:{marca:ctrl.nuevoGrupo.tipo_generador_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.nuevoGrupo.numero_serie_generador"/>
					</td>
				</tr>
				<tr>
					<td class="info">Potencia</td>
					<td class="default">
						<span ng-if="potencia.id == ctrl.nuevoGrupo.tipo_generador_ge.id" ng-repeat="potencia in ctrl.tipo_generador_ge.modelos">{{potencia.potencia}} kW</span>
					</td>
					<td class="info">Corriente</td>
					<td class="default">
						<span ng-if="corriente.id == ctrl.nuevoGrupo.tipo_generador_ge.id" ng-repeat="corriente in ctrl.tipo_generador_ge.modelos">{{corriente.corriente}} A</span>
					</td>
					<td class="info" colspan="2"></td>
				</tr>
				<tr class="text-center" style="background-color:#e7d7b4;">
					<td colspan="6">AVR</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_avr_ge.marca" ng-change="ctrl.nuevoGrupo.tipo_avr_ge.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_avr_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_avr_ge.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoGrupo.tipo_avr_ge.marca" ng-repeat="modelo in ctrl.tipo_avr_ge.modelos | filter:{marca:ctrl.nuevoGrupo.tipo_avr_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.nuevoGrupo.numero_serie_avr"/>
					</td>
				</tr>
				<tr class="text-center bg-success">
					<td colspan="6">Controlador GE</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_controlador_ge.marca" ng-change="ctrl.nuevoGrupo.tipo_controlador_ge.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_controlador_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_controlador_ge.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoGrupo.tipo_controlador_ge.marca" ng-repeat="modelo in ctrl.tipo_controlador_ge.modelos | filter:{marca:ctrl.nuevoGrupo.tipo_controlador_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.nuevoGrupo.numero_serie_controlador_ge"/>
					</td>
				</tr>
				<tr class="text-center" style="background-color:#f7f7ca;">
					<td colspan="6">Controlador TTA</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_controlador_tta.marca" ng-change="ctrl.nuevoGrupo.tipo_controlador_tta.id = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_controlador_tta.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoGrupo.tipo_controlador_tta.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoGrupo.tipo_controlador_tta.marca" ng-repeat="modelo in ctrl.tipo_controlador_tta.modelos | filter:{marca:ctrl.nuevoGrupo.tipo_controlador_tta.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de serie</td>
					<td class="default">
						<input class="form-control" ng-model="ctrl.nuevoGrupo.numero_serie_controlador_tta"/>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<button class="btn btn-success" ng-click="ctrl.almacenar(ctrl.nuevoGrupo)" ng-disabled="ctrl.disabledNuevoGrupo()">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamiento()">Cancelar</button>
</div>

<div ng-show="ctrl.editarPopup">
	<h3>Editar grupo electrógeno</h3>

	<div style="max-width:900px;height:450px;overflow:auto;">
	<table class="table table-condensed">
		<tbody>
			<tr>
				<td class="info" colspan="2"></td>
				<td class="info">Estado del equipo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.estado.id">
						<option value="">Seleccionar estado del equipo</option>
						<option value="1">Nuevo</option>
						<option value="2">Operativo</option>
						<option value="3">De baja</option>
					</select>
				</td>
				<td class="info" colspan="2"></td>
			</tr>
			<tr class="text-center" style="background-color:#d2f3ce;">
				<td colspan="6">Grupo Electrógeno</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_grupo_electrogeno.marca" ng-change="ctrl.editar.tipo_grupo_electrogeno.id = '';">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_grupo_electrogeno.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</td>
				<td class="info">Modelo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_grupo_electrogeno.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editar.tipo_grupo_electrogeno.marca" ng-repeat="modelo in ctrl.tipo_grupo_electrogeno.modelos | filter:{marca:ctrl.editar.tipo_grupo_electrogeno.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</td>
				<td class="info">Número de serie</td>
				<td class="default">
					<input class="form-control" ng-model="ctrl.editar.numero_serie_grupo_electrogeno"/>
				</td>
			</tr>
			<tr class="text-center" style="background-color:#b9d1e5;">
				<td colspan="6">Motor</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_motor_ge.marca" ng-change="ctrl.editar.tipo_motor_ge.id = '';">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_motor_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</td>
				<td class="info">Modelo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_motor_ge.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editar.tipo_motor_ge.marca" ng-repeat="modelo in ctrl.tipo_motor_ge.modelos | filter:{marca:ctrl.editar.tipo_motor_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</td>
				<td class="info">Número de serie</td>
				<td class="default">
					<input class="form-control" ng-model="ctrl.editar.numero_serie_motor"/>
				</td>
			</tr>
			<tr class="text-center" style="background-color:#d7e7b4;">
				<td colspan="6">Generador</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_generador_ge.marca" ng-change="ctrl.editar.tipo_generador_ge.id = '';">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_generador_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</td>
				<td class="info">Modelo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_generador_ge.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editar.tipo_generador_ge.marca" ng-repeat="modelo in ctrl.tipo_generador_ge.modelos | filter:{marca:ctrl.editar.tipo_generador_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</td>
				<td class="info">Número de serie</td>
				<td class="default">
					<input class="form-control" ng-model="ctrl.editar.numero_serie_generador"/>
				</td>
			</tr>
			<tr>
				<td class="info">Potencia</td>
				<td class="default">
					<span ng-if="potencia.id == ctrl.editar.tipo_generador_ge.id" ng-repeat="potencia in ctrl.tipo_generador_ge.modelos">{{potencia.potencia}} kW</span>
				</td>
				<td class="info">Corriente</td>
				<td class="default">
					<span ng-if="corriente.id == ctrl.editar.tipo_generador_ge.id" ng-repeat="corriente in ctrl.tipo_generador_ge.modelos">{{corriente.corriente}} A</span>
				</td>
				<td class="info" colspan="2"></td>
			</tr>
			<tr class="text-center" style="background-color:#e7d7b4;">
				<td colspan="6">AVR</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_avr_ge.marca" ng-change="ctrl.editar.tipo_avr_ge.id = '';">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_avr_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</td>
				<td class="info">Modelo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_avr_ge.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editar.tipo_avr_ge.marca" ng-repeat="modelo in ctrl.tipo_avr_ge.modelos | filter:{marca:ctrl.editar.tipo_avr_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</td>
				<td class="info">Número de serie</td>
				<td class="default">
					<input class="form-control" ng-model="ctrl.editar.numero_serie_avr"/>
				</td>
			</tr>
			<tr class="text-center bg-success">
				<td colspan="6">Controlador GE</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_controlador_ge.marca" ng-change="ctrl.editar.tipo_controlador_ge.id = '';">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_controlador_ge.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</td>
				<td class="info">Modelo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_controlador_ge.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editar.tipo_controlador_ge.marca" ng-repeat="modelo in ctrl.tipo_controlador_ge.modelos | filter:{marca:ctrl.editar.tipo_controlador_ge.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</td>
				<td class="info">Número de serie</td>
				<td class="default">
					<input class="form-control" ng-model="ctrl.editar.numero_serie_controlador_ge"/>
				</td>
			</tr>
			<tr class="text-center" style="background-color:#f7f7ca;">
				<td colspan="6">Controlador TTA</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_controlador_tta.marca" ng-change="ctrl.editar.tipo_controlador_tta.id = '';">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_controlador_tta.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</td>
				<td class="info">Modelo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_controlador_tta.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editar.tipo_controlador_tta.marca" ng-repeat="modelo in ctrl.tipo_controlador_tta.modelos | filter:{marca:ctrl.editar.tipo_controlador_tta.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</td>
				<td class="info">Número de serie</td>
				<td class="default">
					<input class="form-control" ng-model="ctrl.editar.numero_serie_controlador_tta"/>
				</td>
			</tr>
		</tbody>
	</table>
	</div>

	<button class="btn btn-success" ng-click="ctrl.grabar(ctrl.editar, 'logs_bodegas_grupos_electrogenos,almacen_previo,__proto__,elemento')">Grabar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEdicion()">Cancelar</button>
</div>