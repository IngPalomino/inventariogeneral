<br>
<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4">
			<div class="form-group">
				<label>Almacenes</label>
				<div class="input-group">
					
					<selectize config="ctrl.configAlmacenes" options="ctrl.almacenes" ng-model="ctrl.busqueda.almacen" ng-change="ctrl.searched = false"></selectize>



					<div class="input-group-btn">
						<button class="btn btn-default" ng-click="ctrl.buscarAlmacenes(ctrl.busqueda.almacen)" ng-disabled="!ctrl.busqueda.almacen || ctrl.loading">
							<i class="fa fa-search" ng-show="!ctrl.loading"></i>
							<i class="fa fa-spinner fa-spin" ng-show="ctrl.loading"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4">
			<div class="form-group">
				<label>Sitios</label>
				<div class="input-group">
					<selectize config="ctrl.configSitios" options="ctrl.sitios" ng-model="ctrl.busqueda.sitio" ng-change="ctrl.searched = false"></selectize>
					<div class="input-group-btn">
						<button class="btn btn-default" ng-click="ctrl.buscarSitios(ctrl.busqueda.sitio)" ng-disabled="!ctrl.busqueda.sitio || ctrl.loading">
							<i class="fa fa-search" ng-show="!ctrl.loading"></i>
							<i class="fa fa-spinner fa-spin" ng-show="ctrl.loading"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-4">
			<div class="form-group">
				<label class="test2018">Objetos</label>
				<div class="input-group">
					<selectize config="ctrl.configObjetos" options="ctrl.objetos" ng-model="ctrl.busqueda.objeto" ng-change="ctrl.searched = false"></selectize>
					<div class="input-group-btn">
						<button class="btn btn-default" ng-click="ctrl.buscarObjetos(ctrl.busqueda.objeto)" ng-disabled="!ctrl.busqueda.objeto || ctrl.loading">
							<i class="fa fa-search" ng-show="!ctrl.loading"></i>
							<i class="fa fa-spinner fa-spin" ng-show="ctrl.loading"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="row" ng-if="ctrl.elementos_busqueda.length && !ctrl.loading">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Resultados
				</div>
				<div class="panel-body">
					<div class="panel-group" id="accordion">
						<div ng-if="objeto.cantidad >0"  class="panel panel-default" ng-repeat="objeto in ctrl.elementos_busqueda">
							<div  class="panel-heading">
								<h4 class="panel-title">
									<a href="#{{objeto.link}}" data-toggle="collapse" data-parent="#accordion">{{objeto.objeto}}<span class="badge">{{objeto.cantidad}}</span></a>
								</h4>
							</div>
							<div id="{{objeto.link}}" class="panel-collapse collapse">
								<div class="panel-body" style="max-height:270px;overflow:auto;">
									<div ng-switch on="objeto.link">
										<div ng-switch-default>
											<table class="table table-condensed table-condensed table-responsive">
												<thead>
													<tr>
														<th><i class="fa fa-cogs"></i></th>
														<th>Marca</th>
														<th>Modelo</th>
														<th>Proveniente</th>
														<th>Estado</th>
														<th>Almacen</th>
														<th>Historial</th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="elemento in objeto.info">
														<td>
															<div class="dropdown">
																<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdown{{objeto.link}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																	<i class="fa fa-cogs"></i>
																	<span class="caret"></span>
																</button>
																<ul class="dropdown-menu" aria-labelledby="dropdown{{objeto.link}}">
																	<li><a href ng-click="ctrl.editando(elemento, objeto.link)"><i class="fa fa-edit"></i> Editar</a></li>
																	<li><a href ng-click="ctrl.moviendo(elemento, objeto.link)"><i class="fa fa-arrows"></i> Mover</a></li>
																	<li role="separator" class="divider"></li>
																	<li><a href ng-click="ctrl.eliminando(elemento, objeto.link)"><i class="fa fa-close"></i> Eliminar</a></li>
																</ul>
															</div>
														</td>
														<td>{{elemento[objeto.tipo].marca}}</td>
														<td>{{elemento[objeto.tipo].modelo}}</td>
														<td>{{elemento.proveniente_sitio.nombre_completo}}</td>
														<td>
															<span style="text-transform:uppercase;" ng-class="{'label label-success':elemento.estado.id == 1, 'label label-warning':elemento.estado.id == 2, 'label label-danger':elemento.estado.id == 3}">
																{{elemento.estado.estado}}
															</span>
														</td>
														<td>{{elemento.almacen.almacen}}</td>
														<td style="font-size:8pt;list-style:none;">
															<span ng-if="!elemento[objeto.logs].length || elemento[objeto.logs][0].evento.tipo != 'INSERT'">Almacenado: {{elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
															<span ng-if="elemento[objeto.logs][0].evento.tipo == 'INSERT'">Almacenado: {{elemento[objeto.logs][0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][0].usuario.nombres}} {{elemento[objeto.logs][0].usuario.apellido_paterno}}</span>
															<br>
															<span ng-if="elemento[objeto.logs][1].evento.tipo == 'UPDATE'">Actualizado: {{elemento[objeto.logs][1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][1].usuario.nombres}} {{elemento[objeto.logs][1].usuario.apellido_paterno}}</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div ng-switch-when="elementos_mw">
											<table class="table table-condensed table-condensed table-responsive">
												<thead>
													<tr>
														<th><i class="fa fa-cogs"></i></th>
														<th>Elemento</th>
														<th>Marca</th>
														<th>Modelo</th>
														<th>Estado</th>
														<th>Almacen</th>
														<th>Historial</th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="elemento in objeto.info">
														<td>
															<div class="dropdown">
																<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdown{{objeto.link}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																	<i class="fa fa-cogs"></i>
																	<span class="caret"></span>
																</button>
																<ul class="dropdown-menu" aria-labelledby="dropdown{{objeto.link}}">
																	<li><a href ng-click="ctrl.editandoObjeto(elemento, objeto.link)"><i class="fa fa-edit"></i> Editar</a></li>
																	<li><a href ng-click="ctrl.moviendo(elemento, objeto.link)"><i class="fa fa-arrows"></i> Mover</a></li>
																	<li role="separator" class="divider"></li>
																	<li><a href ng-click="ctrl.eliminando(elemento, objeto.link)"><i class="fa fa-close"></i> Eliminar</a></li>
																</ul>
															</div>
														</td>
														<td>{{elemento.elemento_mw.elemento_mw}}</td>
														<td>{{elemento[objeto.tipo].marca}}</td>
														<td>{{elemento[objeto.tipo].modelo}}</td>
														<td>
															<span style="text-transform:uppercase;" ng-class="{'label label-success':elemento.estado.id == 1, 'label label-warning':elemento.estado.id == 2, 'label label-danger':elemento.estado.id == 3}">
																{{elemento.estado.estado}}
															</span>
														</td>
														<td>{{elemento.almacen.almacen}}</td>
														<td style="font-size:8pt;list-style:none;">
															<span ng-if="!elemento[objeto.logs].length || elemento[objeto.logs][0].evento.tipo != 'INSERT'">Almacenado: {{elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
															<span ng-if="elemento[objeto.logs][0].evento.tipo == 'INSERT'">Almacenado: {{elemento[objeto.logs][0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][0].usuario.nombres}} {{elemento[objeto.logs][0].usuario.apellido_paterno}}</span>
															<br>
															<span ng-if="elemento[objeto.logs][1].evento.tipo == 'UPDATE'">Actualizado: {{elemento[objeto.logs][1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][1].usuario.nombres}} {{elemento[objeto.logs][1].usuario.apellido_paterno}}</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>


										<div   ng-switch-when="elementos_sistema_combustible" >
											<table class="table table-condensed table-condensed table-responsive">
												<thead>
													<tr>
														<th><i class="fa fa-cogs"></i></th>
														<th>Elemento</th>
														<th>Marca</th>
														<th>Modelo</th>
														<th>Nro. Serie</th>
														<th>Estado</th>
														<th>Almacen</th>
														<th>Historial</th>
													</tr>
												</thead>
												<tbody>


										<tr ng-repeat="elemento in ctrl.datainfo">
														<td>
															<div class="dropdown">
																<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdown{{objeto.link}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																	<i class="fa fa-cogs"></i>
																	<span class="caret"></span>
																</button>
																<ul class="dropdown-menu" aria-labelledby="dropdown{{objeto.link}}">
																	<li class="test sistema electrico">
																		<a href ng-click="ctrl.editandoObjeto(elemento, objeto.link)"><i class="fa fa-edit"></i> Editar</a>
																	</li>
																	<li><a href ng-click="ctrl.moviendo(elemento, objeto.link)"><i class="fa fa-arrows"></i> Mover</a></li>
																	<li role="separator" class="divider"></li>
																	<li><a href ng-click="ctrl.eliminando(elemento, objeto.link)"><i class="fa fa-close"></i> Eliminar</a></li>
																</ul>
															</div>
														</td>
														<td>{{elemento.elemento_sistema_combustible.elemento_sistema_combustible}}</td>

														<td>{{elemento[objeto.tipo].marca}}</td>
														<td>{{elemento[objeto.tipo].modelo}}</td>
									<td>{{elemento.numero_serie}}</td>
														<td>
															<span style="text-transform:uppercase;" ng-class="{'label label-success':elemento.estado.id == 1, 'label label-warning':elemento.estado.id == 2, 'label label-danger':elemento.estado.id == 3}">
																{{elemento.estado.estado}}
															</span>
														</td>
														<td>{{elemento.almacen.almacen}}</td>
														<td style="font-size:8pt;list-style:none;">
															<span ng-if="!elemento[objeto.logs].length || elemento[objeto.logs][0].evento.tipo != 'INSERT'">Almacenado: {{elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
															<span ng-if="elemento[objeto.logs][0].evento.tipo == 'INSERT'">Almacenado: {{elemento[objeto.logs][0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][0].usuario.nombres}} {{elemento[objeto.logs][0].usuario.apellido_paterno}}</span>
															<br>
															<span ng-if="elemento[objeto.logs][1].evento.tipo == 'UPDATE'">Actualizado: {{elemento[objeto.logs][1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][1].usuario.nombres}} {{elemento[objeto.logs][1].usuario.apellido_paterno}}</span>
														</td>
													</tr>



												</tbody>
											</table>
										</div>

										<div ng-switch-when="tanques_combustible">
											<table class="table table-condensed table-condensed table-responsive">
												<thead>
													<tr>
														<th><i class="fa fa-cogs"></i></th>
														<th>Tipo</th>
														<th>Proveniente</th>
														<th>Estado</th>
														<th>Almacen</th>
														<th>Historial</th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="elemento in objeto.info">
														<td>
															<div class="dropdown">
																<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdown{{objeto.link}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																	<i class="fa fa-cogs"></i>
																	<span class="caret"></span>
																</button>
																<ul class="dropdown-menu" aria-labelledby="dropdown{{objeto.link}}">
																	<li><a href ng-click="ctrl.editando(elemento, objeto.link)"><i class="fa fa-edit"></i> Editar</a></li>
																	<li><a href ng-click="ctrl.moviendo(elemento, objeto.link)"><i class="fa fa-arrows"></i> Mover</a></li>
																	<li role="separator" class="divider"></li>
																	<li><a href ng-click="ctrl.eliminando(elemento, objeto.link)"><i class="fa fa-close"></i> Eliminar</a></li>
																</ul>
															</div>
														</td>
														<td>
															<span ng-if="elemento[objeto.tipo] == 1">Tanque Interno</span>
															<span ng-if="elemento[objeto.tipo] == 2">Tanque Externo</span>
														</td>
														<td>{{elemento.proveniente_sitio.nombre_completo}}</td>
														<td>
															<span style="text-transform:uppercase;" ng-class="{'label label-success':elemento.estado.id == 1, 'label label-warning':elemento.estado.id == 2, 'label label-danger':elemento.estado.id == 3}">
																{{elemento.estado.estado}}
															</span>
														</td>
														<td>{{elemento.almacen.almacen}}</td>
														<td style="font-size:8pt;list-style:none;">
															<span ng-if="!elemento[objeto.logs].length || elemento[objeto.logs][0].evento.tipo != 'INSERT'">Almacenado: {{elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
															<span ng-if="elemento[objeto.logs][0].evento.tipo == 'INSERT'">Almacenado: {{elemento[objeto.logs][0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][0].usuario.nombres}} {{elemento[objeto.logs][0].usuario.apellido_paterno}}</span>
															<br>
															<span ng-if="elemento[objeto.logs][1].evento.tipo == 'UPDATE'">Actualizado: {{elemento[objeto.logs][1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][1].usuario.nombres}} {{elemento[objeto.logs][1].usuario.apellido_paterno}}</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div ng-switch-when="lineas_electricas">
											<table class="table table-condensed table-condensed table-responsive">
												<thead>
													<tr>
														<th><i class="fa fa-cogs"></i></th>
														<th>Elemento</th>
														<th>Marca</th>
														<th>Proveniente</th>
														<th>Estado</th>
														<th>Almacen</th>
														<th>Historial</th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="elemento in objeto.info">
														<td>
															<div class="dropdown">
																<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdown{{objeto.link}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																	<i class="fa fa-cogs"></i>
																	<span class="caret"></span>
																</button>
																<ul class="dropdown-menu" aria-labelledby="dropdown{{objeto.link}}">
																	<li><a href ng-click="ctrl.editandoObjeto(elemento, objeto.link)"><i class="fa fa-edit"></i> Editar</a></li>
																	<li><a href ng-click="ctrl.moviendo(elemento, objeto.link)"><i class="fa fa-arrows"></i> Mover</a></li>
																	<li role="separator" class="divider"></li>
																	<li><a href ng-click="ctrl.eliminando(elemento, objeto.link)"><i class="fa fa-close"></i> Eliminar</a></li>
																</ul>
															</div>
														</td>
														<td>
															<span ng-if="elemento.elemento == 1">Transformador</span>
															<span ng-if="elemento.elemento == 2">Transformix</span>
														</td>
														<td>{{elemento[objeto.tipo].marca}}</td>
														<td>{{elemento.proveniente_sitio.nombre_completo}}</td>
														<td>
															<span style="text-transform:uppercase;" ng-class="{'label label-success':elemento.estado.id == 1, 'label label-warning':elemento.estado.id == 2, 'label label-danger':elemento.estado.id == 3}">
																{{elemento.estado.estado}}
															</span>
														</td>
														<td>{{elemento.almacen.almacen}}</td>
														<td style="font-size:8pt;list-style:none;">
															<span ng-if="!elemento[objeto.logs].length || elemento[objeto.logs][0].evento.tipo != 'INSERT'">Almacenado: {{elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
															<span ng-if="elemento[objeto.logs][0].evento.tipo == 'INSERT'">Almacenado: {{elemento[objeto.logs][0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][0].usuario.nombres}} {{elemento[objeto.logs][0].usuario.apellido_paterno}}</span>
															<br>
															<span ng-if="elemento[objeto.logs][1].evento.tipo == 'UPDATE'">Actualizado: {{elemento[objeto.logs][1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][1].usuario.nombres}} {{elemento[objeto.logs][1].usuario.apellido_paterno}}</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div ng-switch-when="bancos_baterias">
											<table class="table table-condensed table-condensed table-responsive">
												<thead>
													<tr>
														<th><i class="fa fa-cogs"></i></th>
														<th>Marca</th>
														<th>Modelo</th>
														<th>Proveniente</th>
														<th>Estado</th>
														<th>Almacen</th>
														<th>Historial</th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="elemento in objeto.info">
														<td>
															<div class="dropdown">
																<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdown{{objeto.link}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																	<i class="fa fa-cogs"></i>
																	<span class="caret"></span>
																</button>
																<ul class="dropdown-menu" aria-labelledby="dropdown{{objeto.link}}">
																	<li><a href ng-click="ctrl.editandoObjeto(elemento, objeto.link)"><i class="fa fa-edit"></i> Editar</a></li>
																	<li><a href ng-click="ctrl.moviendo(elemento, objeto.link)"><i class="fa fa-arrows"></i> Mover</a></li>
																	<li role="separator" class="divider"></li>
																	<li><a href ng-click="ctrl.eliminando(elemento, objeto.link)"><i class="fa fa-close"></i> Eliminar</a></li>
																</ul>
															</div>
														</td>
														<td>{{elemento[objeto.tipo].marca}}</td>
														<td>{{elemento[objeto.tipo].modelo}}</td>
														<td>{{elemento.proveniente_sitio.nombre_completo}}</td>
														<td>
															<span style="text-transform:uppercase;" ng-class="{'label label-success':elemento.estado.id == 1, 'label label-warning':elemento.estado.id == 2, 'label label-danger':elemento.estado.id == 3}">
																{{elemento.estado.estado}}
															</span>
														</td>
														<td>{{elemento.almacen.almacen}}</td>
														<td style="font-size:8pt;list-style:none;">
															<span ng-if="!elemento[objeto.logs].length || elemento[objeto.logs][0].evento.tipo != 'INSERT'">Almacenado: {{elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
															<span ng-if="elemento[objeto.logs][0].evento.tipo == 'INSERT'">Almacenado: {{elemento[objeto.logs][0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][0].usuario.nombres}} {{elemento[objeto.logs][0].usuario.apellido_paterno}}</span>
															<br>
															<span ng-if="elemento[objeto.logs][1].evento.tipo == 'UPDATE'">Actualizado: {{elemento[objeto.logs][1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][1].usuario.nombres}} {{elemento[objeto.logs][1].usuario.apellido_paterno}}</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="alert alert-danger" ng-if="!ctrl.loading && !ctrl.elementos_busqueda.length && ctrl.searched && (ctrl.busqueda.almacen || ctrl.busqueda.sitio || ctrl.busqueda.objeto)">
				<i class="fa fa-exclamation-triangle"></i>
				<span ng-if="ctrl.busqueda.almacen">No hay elementos almacenados en esta bodega.</span>
				<span ng-if="ctrl.busqueda.sitio">No hay elementos almacenados en este sitio.</span>
				<span ng-if="ctrl.busqueda.objeto">No se han almacenado {{ctrl.busqueda.objeto}} en alguna bodega.</span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="input-group">
				<select class="form-control" ng-model="ctrl.objetoAlmacenar">
					<option value="">Seleccionar elemento a almacenar</option>
					<option ng-repeat="objeto in ctrl.objetos_alt" value="{{objeto.objeto}}">{{objeto.nombre_singular}}</option>
				</select>
				




				<div class="input-group-btn">
					<button class="btn btn-default" style="width:100%" ng-click="ctrl.almacenando()" ng-disabled="!ctrl.objetoAlmacenar" ng-if="ctrl.objetoAlmacenar != 'elementos_mw' && ctrl.objetoAlmacenar != 'lineas_electricas' && ctrl.objetoAlmacenar != 'bancos_baterias'&& ctrl.objetoAlmacenar != 'elementos_sistema_combustible' ">
						<i class="fa fa-plus"></i> Almacenar Objeto
					</button>
					<button class="btn btn-default" style="width:100%" ng-click="ctrl.almacenandoObjeto()" ng-disabled="!ctrl.objetoAlmacenar" ng-if="ctrl.objetoAlmacenar == 'elementos_mw' || 
                    ctrl.objetoAlmacenar == 'elementos_sistema_combustible'
                    || ctrl.objetoAlmacenar == 'lineas_electricas' || ctrl.objetoAlmacenar == 'bancos_baterias'">
						<i class="fa fa-plus"></i> Almacenar Objeto
					</button>
				</div>


                 <!--<div class="input-group-btn">
					<button class="btn btn-default" style="width:100%" ng-click="ctrl.almacenando()" ng-disabled="!ctrl.objetoAlmacenar" ng-if="ctrl.objetoAlmacenar != 'elementos_sistema_combustible' && ctrl.objetoAlmacenar != 'lineas_electricas' && ctrl.objetoAlmacenar != 'bancos_baterias'">
						<i class="fa fa-plus"></i> Almacenar Objeto
					</button>
					<button class="btn btn-default" style="width:100%" ng-click="ctrl.almacenandoObjeto()" ng-disabled="!ctrl.objetoAlmacenar" ng-if="ctrl.objetoAlmacenar == 'elementos_sistema_combustible' || ctrl.objetoAlmacenar == 'lineas_electricas' || ctrl.objetoAlmacenar == 'bancos_baterias'">
						<i class="fa fa-plus"></i> Almacenar Objeto
					</button>
				</div> -->








			</div>
		</div>
	</div>
</div>

<div id="almacenar" class="popup">
	<span class="button b-close">X</span>

	<div id="content" ng-include="ctrl.objetoAlmacenar"></div>
	<div ng-show="ctrl.error.almacenar">{{ctrl.error.almacenar}}</div>
</div>

<div id="editar" class="popup">
	<span class="button b-close">X</span>

	<div id="contentEditar" ng-include="ctrl.objetoEditar"></div>
	<div ng-show="ctrl.error.almacenar">{{ctrl.error.almacenar}}</div>
</div>

<div class="modal fade" id="almacenarObjeto" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div ng-include="ctrl.objetoAlmacenar"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="editarObjeto" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div ng-include="ctrl.objetoEditar"></div>
		</div>
	</div>
</div>

<div id="mover" class="popup">
	<span class="button b-close">X</span>

	<h3>Mover Objeto</h3>

	<div style="max-width:400px">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a data-toggle="tab" href="#almacen" ng-click="ctrl.opcionAlmacen(ctrl.moverElemento)">Almacén</a></li>
			<li><a data-toggle="tab" href="#sitio" ng-click="ctrl.opcionSitio(ctrl.moverElemento)">Sitio</a></li>
		</ul>
		<div class="tab-content">
			<div id="almacen" class="tab-pane fade in active">
				<select class="form-control" ng-model="ctrl.moverElemento.almacen">
					<option value="">Elegir almacén</option>
					<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
				</select>
			</div>
			<div id="sitio" class="tab-pane fade">
				<label style="width:100%">
					<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.moverElemento.a_sitio"></selectize>
				</label>
			</div>
		</div>
	</div><br>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea mover el elemento..." ng-model="ctrl.moverElemento.comentario"></textarea>
	<button class="btn btn-success" ng-click="ctrl.mover(ctrl.moverElemento)" ng-disabled="!ctrl.moverElemento.comentario || (ctrl.moverElemento.tipo_almacen == 1 && !ctrl.moverElemento.almacen) || (ctrl.moverElemento.tipo_almacen == 2 && !ctrl.moverElemento.a_sitio)">Mover</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarMovimiento()">Cancelar</button>
</div>

<div id="eliminar" class="popup">
	<span class="button b-close">X</span>	
	<h3>Eliminar Elemento</h3>	
	<p>¿Está seguro de eliminar el elemento?</p>	
	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminarElemento.comentario"></textarea>	
	<button class="btn btn-success" ng-click="ctrl.eliminar()" ng-disabled="!ctrl.eliminarElemento.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>