<div ng-show="ctrl.almacenarPopup">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Almacenar Banco de Baterías</h4>
	</div>
	<div class="modal-body">
		<div class="container-fluid modal-format">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<ul class="nav nav-tabs nav-justified">
						<li ng-class="{active: ctrl.panel.index == 1}"><a href ng-click="ctrl.panel.opcionAlmacen(ctrl.nuevoBanco)">Almacén</a></li>
						<li ng-class="{active: ctrl.panel.index == 2}"><a href ng-click="ctrl.panel.opcionSitio(ctrl.nuevoBanco)">Sitio</a></li>
					</ul>
					<div ng-show="ctrl.panel.index == 1">
						<select class="form-control" ng-model="ctrl.nuevoBanco.almacen">
							<option value="">Elegir almacén</option>
							<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
						</select>
					</div>
					<div ng-show="ctrl.panel.index == 2">
						<label style="width:100%">
							<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.nuevoBanco.a_sitio"></selectize>
						</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-1"><label>Estado del equipo:</label></div>
				<div class="col-md-6">
					<select class="form-control" ng-model="ctrl.nuevoBanco.estado">
						<option value="">Seleccionar estado del equipo</option>
						<option value="1">Nuevo</option>
						<option value="2">Operativo</option>
						<option value="3">De baja</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-1"><label>Marca:</label></div>
				<div class="col-md-6">
					<select class="form-control" ng-model="ctrl.nuevoBanco.tipo_bancos_baterias.marca" ng-change="ctrl.nuevoBanco.tipo_bancos_baterias.id = '';">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_bancos_baterias.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-1"><label>Modelo:</label></div>
				<div class="col-md-6">
					<select class="form-control" ng-model="ctrl.nuevoBanco.tipo_bancos_baterias.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.nuevoBanco.tipo_bancos_baterias.marca" ng-repeat="modelo in ctrl.tipo_bancos_baterias.modelos | filter:{marca:ctrl.nuevoBanco.tipo_bancos_baterias.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-1"><label>Confiabilidad:</label></div>
				<div class="col-md-6">
					<input class="form-control" type="number" ng-model="ctrl.nuevoBanco.confiabilidad">
				</div>
			</div>
			<div class="row" ng-if="ctrl.nuevoBanco.celdas.length">
				<div class="col-md-10 col-md-offset-1">
					<label>Celdas:</label>
					<div class="input-group" ng-repeat="celda in ctrl.nuevoBanco.celdas" style="padding-bottom:5px;">
						<input class="form-control" type="text" placeholder="Número de Serie" ng-model="celda.numero_serie">
						<div class="input-group-btn">
							<button class="btn btn-danger" ng-click="ctrl.eliminarCelda($index, ctrl.nuevoBanco)"><i class="fa fa-times"></i></button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<button type="button" class="btn btn-primary btn-block" ng-click="ctrl.nuevaCelda(ctrl.nuevoBanco)"><i class="fa fa-plus"></i> Agregar Celda</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
		<button class="btn btn-primary" ng-disabled="ctrl.disabledNuevoBanco()" ng-click="ctrl.almacenar(ctrl.nuevoBanco)">Almacenar</button>
	</div>
</div>

<div ng-show="ctrl.editarPopup">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Editar Banco de Baterías</h4>
	</div>
	<div class="modal-body">
		<div class="container-fluid modal-format">
			<div class="row">
				<div class="col-md-4 col-md-offset-1"><label>Estado del equipo:</label></div>
				<div class="col-md-6">
					<select class="form-control" ng-model="ctrl.editarObj.estado.id">
						<option value="">Seleccionar estado del equipo</option>
						<option value="1">Nuevo</option>
						<option value="2">Operativo</option>
						<option value="3">De baja</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-1"><label>Marca:</label></div>
				<div class="col-md-6">
					<select class="form-control" ng-model="ctrl.editarObj.tipo_bancos_baterias.marca" ng-change="ctrl.editarObj.tipo_bancos_baterias.id = '';">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_bancos_baterias.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-1"><label>Modelo:</label></div>
				<div class="col-md-6">
					<select class="form-control" ng-model="ctrl.editarObj.tipo_bancos_baterias.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editarObj.tipo_bancos_baterias.marca" ng-repeat="modelo in ctrl.tipo_bancos_baterias.modelos | filter:{marca:ctrl.editarObj.tipo_bancos_baterias.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-1"><label>Confiabilidad:</label></div>
				<div class="col-md-6">
					<input class="form-control" type="number" ng-model="ctrl.editarObj.confiabilidad">
				</div>
			</div>
			<div class="row" ng-if="ctrl.editarObj.bodega_celdas_bancos_baterias.length">
				<div class="col-md-10 col-md-offset-1">
					<label>Celdas:</label>
					<div class="input-group" ng-repeat="celda in ctrl.editarObj.bodega_celdas_bancos_baterias" style="padding-bottom:5px;">
						<div class="input-group-addon">
							Celda {{$index + 1}}
						</div>
						<input class="form-control" type="text" placeholder="Número de Serie" ng-model="celda.numero_serie">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
		<button class="btn btn-primary" ng-disabled="ctrl.disabledBanco()" ng-click="ctrl.grabar(ctrl.editarObj)">Guardar</button>
	</div>
</div>