<div ng-show="ctrl.almacenarPopup">
	<h3>Almacenar tanque de combustible</h3>

	<div style="max-width:400px;height:350px;overflow:auto;">
		<ul class="nav nav-tabs nav-justified">
			<li ng-class="{active: ctrl.panel.index == 1}"><a href ng-click="ctrl.panel.opcionAlmacen(ctrl.nuevoTanqueCombustible)">Almacén</a></li>
			<li ng-class="{active: ctrl.panel.index == 2}"><a href ng-click="ctrl.panel.opcionSitio(ctrl.nuevoTanqueCombustible)">Sitio</a></li>
		</ul>
		<div ng-show="ctrl.panel.index == 1">
			<select class="form-control" ng-model="ctrl.nuevoTanqueCombustible.almacen">
				<option value="">Elegir almacén</option>
				<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
			</select>
		</div>
		<div ng-show="ctrl.panel.index == 2">
			<label style="width:100%">
				<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.nuevoTanqueCombustible.a_sitio"></selectize>
			</label>
		</div>
		<select class="form-control" ng-model="ctrl.nuevoTanqueCombustible.estado">
			<option value="">Seleccionar estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select>

		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Tipo de tanque</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.nuevoTanqueCombustible.tipo_tanque_combustible">
							<option value="">Elegir tipo</option>
							<option value="1">Tanque Interno</option>
							<option value="2">Tanque Externo</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Capacidad</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevoTanqueCombustible.capacidad">
							<div class="input-group-addon">gl</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Consumo</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.nuevoTanqueCombustible.consumo">
							<div class="input-group-addon">gl/hr</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<button class="btn btn-success" ng-click="ctrl.almacenar(ctrl.nuevoTanqueCombustible)" ng-disabled="ctrl.disabledNuevoTanque()">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamiento(ctrl.nuevoTanqueCombustible)">Cancelar</button>
</div>

<div ng-show="ctrl.editarPopup">
	<h3>Editar tanque de combustible</h3>

	<div style="max-width:400px;overflow:auto;">
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Estado del equipo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.editar.estado.id">
							<option value="">Seleccionar estado del equipo</option>
							<option value="1">Nuevo</option>
							<option value="2">Operativo</option>
							<option value="3">De baja</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Tipo de tanque</td>
					<td class="default">
						<select class="form-control input-sm" ng-model="ctrl.editar.tipo_tanque_combustible">
							<option value="">Elegir tipo</option>
							<option value="1">Tanque Interno</option>
							<option value="2">Tanque Externo</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Capacidad</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editar.capacidad">
							<div class="input-group-addon">gl</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Consumo</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" step="any" min="0" class="form-control input-sm" ng-model="ctrl.editar.consumo">
							<div class="input-group-addon">gl/hr</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<button class="btn btn-success" ng-click="ctrl.grabar(ctrl.editar)">Guardar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEdicion()">Cancelar</button>
</div>