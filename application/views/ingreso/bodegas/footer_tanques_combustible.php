ref.nuevoTanqueCombustible = {
	tipo_almacen: 1
};

ref.disabledNuevoTanque = function() {
	if ( (ref.nuevoTanqueCombustible.tipo_almacen == 1 && ! ref.nuevoTanqueCombustible.almacen) || (ref.nuevoTanqueCombustible.tipo_almacen == 2 && ! ref.nuevoTanqueCombustible.a_sitio) || ! ref.nuevoTanqueCombustible.tipo_tanque_combustible || ! ref.nuevoTanqueCombustible.capacidad || ! ref.nuevoTanqueCombustible.consumo) {
		return true;
	}
};