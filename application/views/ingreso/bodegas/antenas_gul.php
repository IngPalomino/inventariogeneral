<div ng-show="ctrl.almacenarPopup">
	<h3>Almacenar antena GUL</h3>

	<form>
		<div style="max-width:500px;max-height:400px;overflow:auto;">
			<ul class="nav nav-tabs nav-justified">
				<li ng-class="{active: ctrl.panel.index == 1}"><a href ng-click="ctrl.panel.opcionAlmacen(ctrl.nuevaAntenaGUL)">Almacén</a></li>
				<li ng-class="{active: ctrl.panel.index == 2}"><a href ng-click="ctrl.panel.opcionSitio(ctrl.nuevaAntenaGUL)">Sitio</a></li>
			</ul>
			<div ng-show="ctrl.panel.index == 1">
				<select class="form-control" ng-model="ctrl.nuevaAntenaGUL.almacen">
					<option value="">Elegir almacén</option>
					<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
				</select>
			</div>
			<div ng-show="ctrl.panel.index == 2">
				<label style="width:100%">
					<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.nuevaAntenaGUL.a_sitio"></selectize>
				</label>
			</div>
			<select class="form-control" ng-model="ctrl.nuevaAntenaGUL.estado">
				<option value="">Seleccionar estado del equipo</option>
				<option value="1">Nuevo</option>
				<option value="2">Operativo</option>
				<option value="3">De baja</option>
			</select>
			<table class="table table-condensed">
				<tbody>
					<tr>
						<td class="info">Número de serie</td>
						<td class="default">
							<input type="text" class="form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.numero_serie">
						</td>
					</tr>
					<tr>
						<td class="info">Marca</td>
						<td class="default">
							<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.tipo_antena_gul.marca" ng-change="ctrl.nuevaAntenaGUL.tipo_antena_gul.id = ''; ctrl.cambioMarca()">
								<option value="">Elegir marca</option>
								<option ng-repeat="marca in ctrl.tipo_antena_gul.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="info">Modelo</td>
						<td class="default">
							<select class="form-control input-sm" ng-model="ctrl.nuevaAntenaGUL.tipo_antena_gul.id">
								<option value="">Elegir modelo</option>
								<option ng-if="ctrl.nuevaAntenaGUL.tipo_antena_gul.marca" ng-repeat="modelo in ctrl.tipo_antena_gul.modelos | filter:{marca:ctrl.nuevaAntenaGUL.tipo_antena_gul.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div><br>
		<button class="btn btn-success" ng-click="ctrl.almacenar(ctrl.nuevaAntenaGUL)" ng-disabled="ctrl.disabledNuevaAntenaGUL()">Almacenar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamiento()">Cancelar</button>
	</form>
</div>

<div ng-show="ctrl.editarPopup">
	<h3>Editar Antena GUL</h3>

	<table class="table table-condensed">
		<tbody>
			<tr>
				<td class="info">Estado del equipo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.estado.id">
						<option value="">Seleccionar estado del equipo</option>
						<option value="1">Nuevo</option>
						<option value="2">Operativo</option>
						<option value="3">De baja</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="info">Número de serie</td>
				<td class="default">
					<input type="text" class="form-control input-sm" ng-model="ctrl.editar.numero_serie">
				</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">
					<select class="form-control input-sm" ng-model="ctrl.editar.tipo_antena_gul.marca" ng-change="ctrl.editar.tipo_antena_gul.id = ''; ctrl.cambioMarca()">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_antena_gul.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="info">Modelo</td>
				<td class="default">
					<select class="form-control input-sm" ng-model="ctrl.editar.tipo_antena_gul.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editar.tipo_antena_gul.marca" ng-repeat="modelo in ctrl.tipo_antena_gul.modelos | filter:{marca:ctrl.editar.tipo_antena_gul.marca}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>

	<button class="btn btn-success" ng-click="ctrl.grabar(ctrl.editar, 'logs_bodegas_antenas_gul,almacen_previo,__proto__')">Grabar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEdicion()">Cancelar</button>
</div>