<div ng-show="ctrl.almacenarPopup">
	<script type="text/javascript">
		$( function() {
			$('.datepickerNuevo').datepicker({ dateFormat: 'yy-mm-dd' }).val();
		} );
	</script>
	<h3>Almacenar Aire Acondicionado</h3>

	<div style="max-width:700px;height:350px;overflow:auto;">
		<ul class="nav nav-tabs nav-justified">
			<li ng-class="{active: ctrl.panel.index == 1}"><a href ng-click="ctrl.panel.opcionAlmacen(ctrl.nuevoAire)">Almacén</a></li>
			<li ng-class="{active: ctrl.panel.index == 2}"><a href ng-click="ctrl.panel.opcionSitio(ctrl.nuevoAire)">Sitio</a></li>
		</ul>
		<div ng-show="ctrl.panel.index == 1">
			<select class="form-control" ng-model="ctrl.nuevoAire.almacen">
				<option value="">Elegir almacén</option>
				<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
			</select>
		</div>
		<div ng-show="ctrl.panel.index == 2">
			<label style="width:100%">
				<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.nuevoAire.a_sitio"></selectize>
			</label>
		</div>
		<select class="form-control" ng-model="ctrl.nuevoAire.estado">
			<option value="">Seleccionar estado del equipo</option>
			<option value="1">Nuevo</option>
			<option value="2">Operativo</option>
			<option value="3">De baja</option>
		</select>
	
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info" colspan="2">Refrigerante</td>
					<td class="default" colspan="2">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_refrigerante">
							<option value="">Elegir refrigerante</option>
							<option ng-repeat="refrigerante in ctrl.tipo_refrigerante" value="{{refrigerante.id}}">{{refrigerante.tipo_refrigerante}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_aires_acondicionados.marca" ng-change="ctrl.nuevoAire.tipo_aires_acondicionados.id = ''; ctrl.nuevoAire.tipo_compresor.id = ''; ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo = '';">
							<option value="">Elegir marca</option>
							<option ng-repeat="marca in ctrl.tipo_aires_acondicionados.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Tipo de Equipo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo" ng-change="ctrl.cambioTipoNuevo(ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo,nuevoAire)">
							<option value="">Elegir tipo de equipo</option>
							<option ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.marca" ng-repeat="equipo in ctrl.tipo_aires_acondicionados.tipos_equipo | filter:{marca:ctrl.nuevoAire.tipo_aires_acondicionados.marca}" value="{{equipo.tipo_equipo}}">{{equipo.tipo_equipo}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Capacidad</td>
					<td class="default">
						<div class="input-group">
							<select class="form-control" ng-model="ctrl.nuevoAire.tipo_aires_acondicionados.capacidad" ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo == 'Mochila'">
								<option value="">Elegir capacidad</option>
								<option ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.marca && ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo" ng-repeat="capacidad in ctrl.tipo_aires_acondicionados.capacidades | filter:{marca:ctrl.nuevoAire.tipo_aires_acondicionados.marca, tipo_equipo:ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo}" value="{{capacidad.capacidad}}">{{capacidad.capacidad}}</option>
							</select>
							<div ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo == 'Mochila'" class="input-group-addon">Ton</div>
							<select class="form-control" ng-model="ctrl.nuevoAire.tipo_aires_acondicionados.id" ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo != 'Mochila'" ng-change="ctrl.elegirCapacidadNuevo(ctrl.nuevoAire.tipo_aires_acondicionados.id,ctrl.nuevoAire)">
								<option value="">Elegir capacidad</option>
								<option ng-if="(ctrl.nuevoAire.tipo_aires_acondicionados.marca == capacidad.marca) && (ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo == capacidad.tipo_equipo)" ng-repeat="capacidad in ctrl.tipo_aires_acondicionados.capacidades2 | filter:{marca:ctrl.nuevoAire.tipo_aires_acondicionados.marca, tipo_equipo:ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo}" value="{{capacidad.id}}">{{capacidad.capacidad}}</option>
							</select>
							<div ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo != 'Mochila'" class="input-group-addon">Ton</div>
						</div>
					</td>
					<td class="info">Fecha de Instalación</td>
					<td class="default">
						<input class="form-control datepickerNuevo" type="text" size="30" ng-model="ctrl.nuevoAire.fecha_instalacion"/>
					</td>
				</tr>
				<tr ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo == 'Mochila'">
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_aires_acondicionados.id" ng-change="ctrl.elegirCapacidadNuevo(ctrl.nuevoAire.tipo_aires_acondicionados.id,ctrl.nuevoAire)">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.marca && ctrl.nuevoAire.tipo_aires_acondicionados.capacidad" ng-repeat="modelo in ctrl.tipo_aires_acondicionados.modelos | filter:{marca:ctrl.nuevoAire.tipo_aires_acondicionados.marca, capacidad:ctrl.nuevoAire.tipo_aires_acondicionados.capacidad}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de Serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="ctrl.nuevoAire.num_serie_aire_acon"/>
					</td>
				</tr>
				<tr style="background-color:#d7bde2;" class="text-center">
					<td colspan="4">Compresor</td>
				</tr>
				<tr>
					<td class="info">Marca</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_compresor.marca" ng-change="ctrl.nuevoAire.tipo_compresor.id = '';">
							<option value="">Elegir marca</option>
							<option ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.capacidad" ng-repeat="marca in ctrl.tipo_compresor.marcas | filter:{capacidad:ctrl.nuevoAire.tipo_aires_acondicionados.capacidad}" value="{{marca.marca}}">{{marca.marca}}</option>
						</select>
					</td>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_compresor.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoAire.tipo_compresor.marca" ng-repeat="modelo in ctrl.tipo_compresor.modelos | filter:{marca:ctrl.nuevoAire.tipo_compresor.marca, capacidad:ctrl.nuevoAire.tipo_aires_acondicionados.capacidad}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
				</tr>
				<tr class="success text-center" ng-if-start="(ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo) && (ctrl.nuevoAire.tipo_aires_acondicionados.tipo_equipo != 'Mochila')">
					<td colspan="4">Condensador</td>
				</tr>
				<tr>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_condensador.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.capacidad" ng-repeat="condensador in ctrl.tipo_condensador | filter:{capacidad:ctrl.nuevoAire.tipo_aires_acondicionados.capacidad}" value="{{condensador.id}}">{{condensador.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de Serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="ctrl.nuevoAire.num_serie_condensador"/>
					</td>
				</tr>
				<tr style="background-color:#fae5d3" class="text-center">
					<td colspan="4">Evaporador</td>
				</tr>
				<tr ng-if-end>
					<td class="info">Modelo</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoAire.tipo_evaporador.id">
							<option value="">Elegir modelo</option>
							<option ng-if="ctrl.nuevoAire.tipo_aires_acondicionados.capacidad" ng-repeat="modelo in ctrl.tipo_evaporador | filter:{capacidad:ctrl.nuevoAire.tipo_aires_acondicionados.capacidad}" value="{{modelo.id}}">{{modelo.modelo}}</option>
						</select>
					</td>
					<td class="info">Número de Serie</td>
					<td class="default">
						<input class="form-control" type="text" ng-model="ctrl.nuevoAire.num_serie_evaporador"/>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<button class="btn btn-success" ng-click="ctrl.almacenar(ctrl.nuevoAire)" ng-disabled="ctrl.disabledNuevoAire()">Almacenar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarAlmacenamiento(ctrl.nuevoAire)">Cancelar</button>
</div>

<div ng-show="ctrl.editarPopup">
	<script type="text/javascript">
		$( function() {
			$('.datepickerEditar').datepicker({ dateFormat: 'yy-mm-dd' }).val();
		} );
	</script>
	<h3>Editar Aire Acondicionado</h3>

	<table class="table table-condensed">
		<tbody>
			<tr>
				<td class="info">Estado</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.estado.id">
						<option value="">Seleccionar estado del equipo</option>
						<option value="1">Nuevo</option>
						<option value="2">Operativo</option>
						<option value="3">De baja</option>
					</select>
				</td>
				<td class="info">Refrigerante</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_refrigerante.id">
						<option value="">Elegir refrigerante</option>
						<option ng-repeat="refrigerante in ctrl.tipo_refrigerante" value="{{refrigerante.id}}">{{refrigerante.tipo_refrigerante}}</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_aires_acondicionados.marca" ng-change="ctrl.editar.tipo_aires_acondicionados.id = ''; ctrl.editar.tipo_compresor.id = ''; ctrl.editar.tipo_aires_acondicionados.tipo_equipo = '';">
						<option value="">Elegir marca</option>
						<option ng-repeat="marca in ctrl.tipo_aires_acondicionados.marcas" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</td>
				<td class="info">Tipo de Equipo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_aires_acondicionados.tipo_equipo" ng-change="ctrl.cambioTipo(ctrl.editar.tipo_aires_acondicionados.tipo_equipo,ctrl.editar)">
						<option value="">Elegir tipo de equipo</option>
						<option ng-if="ctrl.editar.tipo_aires_acondicionados.marca" ng-repeat="equipo in ctrl.tipo_aires_acondicionados.tipos_equipo | filter:{marca:ctrl.editar.tipo_aires_acondicionados.marca}" value="{{equipo.tipo_equipo}}">{{equipo.tipo_equipo}}</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="info">Capacidad</td>
				<td class="default">
					<div class="input-group">
						<select class="form-control" ng-model="ctrl.editar.tipo_aires_acondicionados.capacidad" ng-if="ctrl.editar.tipo_aires_acondicionados.tipo_equipo == 'Mochila'">
							<option value="">Elegir capacidad</option>
							<option ng-if="ctrl.editar.tipo_aires_acondicionados.marca && ctrl.editar.tipo_aires_acondicionados.tipo_equipo" ng-repeat="capacidad in ctrl.tipo_aires_acondicionados.capacidades | filter:{marca:ctrl.editar.tipo_aires_acondicionados.marca, tipo_equipo:ctrl.editar.tipo_aires_acondicionados.tipo_equipo}" value="{{capacidad.capacidad}}">{{capacidad.capacidad}}</option>
						</select>
						<div ng-if="ctrl.editar.tipo_aires_acondicionados.tipo_equipo == 'Mochila'" class="input-group-addon">Ton</div>
						<select class="form-control" ng-model="ctrl.editar.tipo_aires_acondicionados.id" ng-if="ctrl.editar.tipo_aires_acondicionados.tipo_equipo != 'Mochila'" ng-change="ctrl.elegirCapacidadNuevo(ctrl.editar.tipo_aires_acondicionados.id,ctrl.editar)">
							<option value="">Elegir capacidad</option>
							<option ng-if="(ctrl.editar.tipo_aires_acondicionados.marca == capacidad.marca) && (ctrl.editar.tipo_aires_acondicionados.tipo_equipo == capacidad.tipo_equipo)" ng-repeat="capacidad in ctrl.tipo_aires_acondicionados.capacidades2 | filter:{marca:ctrl.editar.tipo_aires_acondicionados.marca, tipo_equipo:ctrl.editar.tipo_aires_acondicionados.tipo_equipo}" value="{{capacidad.id}}">{{capacidad.capacidad}}</option>
						</select>
						<div ng-if="ctrl.editar.tipo_aires_acondicionados.tipo_equipo != 'Mochila'" class="input-group-addon">Ton</div>
					</div>
				</td>
				<td class="info">Fecha de Instalación</td>
				<td class="default">
					<input class="form-control datepickerEditar" type="text" size="30" ng-model="ctrl.editar.fecha_instalacion"/>
				</td>
			</tr>
			<tr ng-if="ctrl.editar.tipo_aires_acondicionados.tipo_equipo == 'Mochila'">
				<td class="info">Modelo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_aires_acondicionados.id" ng-change="ctrl.elegirCapacidadNuevo(ctrl.editar.tipo_aires_acondicionados.id,ctrl.editar)">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editar.tipo_aires_acondicionados.marca && ctrl.editar.tipo_aires_acondicionados.capacidad" ng-repeat="modelo in ctrl.tipo_aires_acondicionados.modelos | filter:{marca:ctrl.editar.tipo_aires_acondicionados.marca, capacidad:ctrl.editar.tipo_aires_acondicionados.capacidad}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</td>
				<td class="info">Número de Serie</td>
				<td class="default">
					<input class="form-control" type="text" ng-model="ctrl.editar.num_serie_aire_acon"/>
				</td>
			</tr>
			<tr style="background-color:#d7bde2;" class="text-center">
				<td colspan="4">Compresor</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_compresor.marca" ng-change="ctrl.editar.tipo_compresor.id = '';">
						<option value="">Elegir marca</option>
						<option ng-if="ctrl.editar.tipo_aires_acondicionados.capacidad" ng-repeat="marca in ctrl.tipo_compresor.marcas | filter:{capacidad:ctrl.editar.tipo_aires_acondicionados.capacidad}" value="{{marca.marca}}">{{marca.marca}}</option>
					</select>
				</td>
				<td class="info">Modelo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_compresor.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editar.tipo_compresor.marca" ng-repeat="modelo in ctrl.tipo_compresor.modelos | filter:{marca:ctrl.editar.tipo_compresor.marca, capacidad:ctrl.editar.tipo_aires_acondicionados.capacidad}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</td>
			</tr>
			<tr class="success text-center" ng-if-start="(ctrl.editar.tipo_aires_acondicionados.tipo_equipo) && (ctrl.editar.tipo_aires_acondicionados.tipo_equipo != 'Mochila')">
				<td colspan="4">Condensador</td>
			</tr>
			<tr>
				<td class="info">Modelo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_condensador.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editar.tipo_aires_acondicionados.capacidad" ng-repeat="condensador in ctrl.tipo_condensador | filter:{capacidad:ctrl.editar.tipo_aires_acondicionados.capacidad}" value="{{condensador.id}}">{{condensador.modelo}}</option>
					</select>
				</td>
				<td class="info">Número de Serie</td>
				<td class="default">
					<input class="form-control" type="text" ng-model="ctrl.editar.num_serie_condensador"/>
				</td>
			</tr>
			<tr style="background-color:#fae5d3" class="text-center">
				<td colspan="4">Evaporador</td>
			</tr>
			<tr ng-if-end>
				<td class="info">Modelo</td>
				<td class="default">
					<select class="form-control" ng-model="ctrl.editar.tipo_evaporador.id">
						<option value="">Elegir modelo</option>
						<option ng-if="ctrl.editar.tipo_aires_acondicionados.capacidad" ng-repeat="modelo in ctrl.tipo_evaporador | filter:{capacidad:ctrl.editar.tipo_aires_acondicionados.capacidad}" value="{{modelo.id}}">{{modelo.modelo}}</option>
					</select>
				</td>
				<td class="info">Número de Serie</td>
				<td class="default">
					<input class="form-control" type="text" ng-model="ctrl.editar.num_serie_evaporador"/>
				</td>
			</tr>
		</tbody>
	</table>

	<button class="btn btn-success" ng-click="ctrl.grabar(ctrl.editar, 'logs_bodegas_aires_acondicionados,almacen_previo,__proto__')">Guardar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEdicion()">Cancelar</button>
</div>