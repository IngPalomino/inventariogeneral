listarElementosSC = function() {
	$http({
		url: base_url("elementos_sistema_combustible/listar"),
		method: "GET"
	}).then(function successCallback(resp) {
		if (resp.data.length) {
			ref.elemento_sistema_combustible = resp.data;
		} else {
			ref.elemento_sistema_combustible = [];
		}
	}, function errorCallback(resp) {

	});
}

ref.listarTipoElementoSC = function(tipo) {
	var data = '';

	ref.elemento_sistema_combustible.forEach(function(elemento) {
		if (elemento.id == tipo) {
			data = elemento.tipo_elemento.substring(3);
		}
	});

	$http({
		url: base_url(data + '/listar'),
		method: 'GET'
	}).then(function successCallback(resp) {
		if (resp.data.marcas.length) {
			ref.tipo_elemento_sistema_combustible = resp.data;
		} else {
			ref.tipo_elemento_sistema_combustible = {};
		}
	}, function errorCallback(resp) {

	});
}

ref.disabledNuevoElementoSC = function() {
	if ( (ref.almacenarObj.tipo_almacen == 1 && ! ref.almacenarObj.almacen) || (ref.almacenarObj.tipo_almacen == 2 && ! ref.almacenarObj.a_sitio) || ! ref.almacenarObj.estado || ! ref.almacenarObj.elemento_sistema_combustible || ! ref.almacenarObj.tipo_elemento_sistema_combustible.id || ! ref.almacenarObj.numero_serie) {
		return true;
	}
}

listarElementosSC();