<br><input id="sitio" type="hidden" value="<?php echo $sitio; ?>">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">Tipo de Torre</div>
				<div class="panel-body" id="panelTorre">
					<div class="col-md-4 text-center">

						<!--   torre -->
                     <div id="range"></div>

                     <!--   torre -->
						<a href data-toggle="modal" data-target="#torreModal">
							<img ng-if="ctrl.infoSitio.tipo_torre.id == 11" ng-src="<?php echo base_url('assets/images/tipo_torre/{{ctrl.infoSitio.tipo_torre.id}}/{{ctrl.infoSitio.altura_torre}}.png'); ?>" height="500">
							<img ng-if="ctrl.infoSitio.tipo_torre.id == 3 || ctrl.infoSitio.tipo_torre.id == 5 || ctrl.infoSitio.tipo_torre.id == 4 || ctrl.infoSitio.tipo_torre.id == 6 || ctrl.infoSitio.tipo_torre.id == 7 || ctrl.infoSitio.tipo_torre.id == 8 || ctrl.infoSitio.tipo_torre.id == 30 || ctrl.infoSitio.tipo_torre.id == 29" ng-src="<?php echo base_url('assets/images/tipo_torre/{{ctrl.infoSitio.tipo_torre.id}}.png'); ?>" height="500">
						</a>
						<div class="Title-Edificacion-footer"><strong>Altura edificación</strong></div>
					</div>
					<div id="rectangle" class="col-md-6" style="height: 500px;">

						<div style="width: 3px; background-color: #000; position: absolute; top: 0; bottom: 0; height: 500px; left: 99%">&nbsp;</div>
						<div ng-repeat="sector in ctrl.antenas_mw_sectores">
							<div style="width: 3px; background-color: #000; position: absolute; top: 0; bottom: 0; height: 500px; left: {{$index * 33}}%">&nbsp;</div>
							<a href ng-repeat="antena in sector.antenas" bs-popover>
								<img src="<?php echo base_url('assets/images/antenas/mw.png'); ?>" rel="popover" data-title="Antena Microondas" data-content="{{antena.content}}" style="position: absolute;top: {{antena.top}}%;bottom: {{antena.bottom}}%; left: {{antena.left}}%" height="20">
							</a>
						</div>


<!-- microondas pendiente -->
						<div ng-repeat="sector in ctrl.antenas_mw_sectores_pendiente">
							<div style="width: 3px; background-color: #000; position: absolute; top: 0; bottom: 0; height: 500px; left: {{$index * 33}}%">&nbsp;</div>
							<a href ng-repeat="antena in sector.antenas" bs-popover>
								<img src="<?php echo base_url('assets/images/antenas/{{antena.foto}}'); ?>" rel="popover" data-title="Antena Microondas" data-content="{{antena.content}}" style="top: {{antena.top}}%;position: absolute;bottom: {{antena.bottom}}%; left: {{antena.left}}%" height="20">
							</a>
						</div>
<!-- microondas pendiente -->


						<div ng-repeat="sector in ctrl.antenas_gul_sectores">
							<div style="width: 3px; background-color: #000; position: absolute; top: 0; bottom: 0; height: 500px; left: {{$index * 33}}%">&nbsp;</div>
							<a href ng-repeat="antena in sector.antenas" bs-popover>
								<img src="<?php echo base_url('assets/images/antenas/gul.png'); ?>" rel="popover" data-title="Antena GUL" data-content="{{antena.content}}" style="position: absolute;top: {{antena.top}}%;bottom: {{antena.bottom}}%; left: {{antena.left}}%" height="40">
							</a>
						</div>


                   <div ng-repeat="sector in ctrl.antenas_gul_pendiente_sectores">
							<div style="width: 3px; background-color: #000; position: absolute; top: 0; bottom: 0; height: 500px; left: {{$index * 33}}%">&nbsp;</div>
						
							<a href ng-repeat="antena in sector.antenas" bs-popover>
								<img src="<?php echo base_url('assets/images/antenas/{{antena.foto}}'); ?>" rel="popover" data-title="Antena GUL Pendiente" data-content="{{antena.content}}" style="position: absolute;top: {{antena.top}}%;bottom: {{antena.bottom}}%; left: {{antena.left}}%" height="40">
							</a> 
					</div>




						<div ng-repeat="sector in ctrl.antenas_iden_sectores">
							<div style="width: 3px; background-color: #000; position: absolute; top: 0; bottom: 0; height: 500px; left: {{$index * 33}}%">&nbsp;</div>
							<a href ng-repeat="antena in sector.antenas" style="position:absolute;bottom:{{antena.bottom}}%;left: {{antena.left}}%;" bs-popover>
								<div rel="popover" data-title="Antenas iDEN" data-content="{{antena.content}}">
									<img ng-repeat="cantidad in antena.sectores" src="<?php echo base_url('assets/images/antenas/iden.png'); ?>" height="40" style="padding-right: 5px;">
								</div>
							</a>
						</div>



					</div>
                         <div class=" text-center">
					        <!--   torre -->
                    	    <div id="range2" style="float:right;"></div>
                    	   <!--   torre -->
                    	   <div class="Title-Edificacion-footer2"><strong>Altura edificación</strong></div>
                    	</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="torreModal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body text-center" style="height: 600px;overflow: auto;">
				<img ng-if="ctrl.infoSitio.tipo_torre.id == 11" id="torreImg" ng-src="<?php echo base_url('assets/images/tipo_torre/{{ctrl.infoSitio.tipo_torre.id}}/{{ctrl.infoSitio.altura_torre}}.png'); ?>">
				<img ng-if="ctrl.infoSitio.tipo_torre.id == 3 || ctrl.infoSitio.tipo_torre.id == 5 || ctrl.infoSitio.tipo_torre.id == 4 || ctrl.infoSitio.tipo_torre.id == 6 || ctrl.infoSitio.tipo_torre.id == 7 || ctrl.infoSitio.tipo_torre.id == 8 || ctrl.infoSitio.tipo_torre.id == 30 || ctrl.infoSitio.tipo_torre.id == 29" id="torreImg" ng-src="<?php echo base_url('assets/images/tipo_torre/{{ctrl.infoSitio.tipo_torre.id}}.png'); ?>">
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" ng-click="ctrl.resetZoom()"><i class="fa fa-history fa-2x"></i></button>
				<button class="btn btn-info" ng-click="ctrl.updateZoom(-0.1)" ng-disabled="ctrl.zoomLevel <= 0.2"><i class="fa fa-search-minus fa-2x"></i></button>
				<button class="btn btn-primary" ng-click="ctrl.updateZoom(0.1)" ng-disabled="ctrl.zoomLevel >= 2"><i class="fa fa-search-plus fa-2x"></i></button>
			</div>
		</div>
	</div>
</div>