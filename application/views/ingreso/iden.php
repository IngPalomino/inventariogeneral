<br>
<div class="container">
	<div class="text-center">
		<i class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></i>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="iden in ctrl.iden" ng-if="!ctrl.cargando && ctrl.iden.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					iDEN
				</div>
				<div class="panel-body">
					<table class="table table-condensed table-responsive" style="table-layout:fixed;">
						<tr>
							<td class="info">Site Name</td>
							<td class="default">{{iden.site_name}}</td>
							<td class="info">Antennas</td>
							<td class="default">{{iden.antennas}}</td>
							<td class="info">BRs</td>
							<td class="default">{{iden.brs}}</td>
						</tr>
						<tr>
							<td class="info">Duplexor</td>
							<td class="default">{{iden.duplexor}}</td>
							<td class="info">QBRs</td>
							<td class="default">{{iden.qbrs}}</td>
							<td class="info">Cabinet MSER</td>
							<td class="default">{{iden.cabinet_mser}}</td>
						</tr>
						<tr>
							<td class="info">Cabinet RF</td>
							<td class="default">{{iden.cabinet_rf}}</td>
							<td class="info">Cabinet Control</td>
							<td class="default">{{iden.cabinet_control}}</td>
							<td class="info">RFN</td>
							<td class="default">{{iden.rfn}}</td>
						</tr>
						<tr>
							<td class="info">EBTS Type</td>
							<td class="default">{{iden.ebts_type}}</td>
							<td class="info">iBSC Name</td>
							<td class="default">{{iden.ibsc_name}}</td>
							<td class="info">ACG ID (EBTS Controller)</td>
							<td class="default">{{iden.acg_id}}</td>
						</tr>
						<tr>
							<td class="info">Site IP Address</td>
							<td class="default">{{iden.site_ip_address}}</td>
							<td class="info">ACG Port</td>
							<td class="default">{{iden.acg_port}}</td>
							<td class="info">OMC Port</td>
							<td class="default">{{iden.omc_port}}</td>
						</tr>
						<tr>
							<td class="default" colspan="2"></td>
							<td class="success">Link Status</td>
							<td class="success">{{iden.link_status}}</td>
						</tr>
					</table>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editandoIden(iden)" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoIden(iden)" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.iden.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-default add-button-lg" ng-click="ctrl.nuevo_iden()"><i class="fa fa-plus" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"></i> Añadir Información de iDEN</button>
		</div>
	</div>
</div>

<div id="editarIden" class="popup" style="width:1000px;">
	<span class="button b-close" ng-click="ctrl.cancelarEdicionIden()">
		<span>X</span>
	</span>
	
	<h3>Editar información iDEN</h3>
	
	<form>
		<div id="editarIdenData" style="max-width:950px;height:120px;overflow:scroll;"></div><br>
		<button class="btn btn-success" ng-click="ctrl.grabarIden(ctrl.editarIden)" ng-disabled="ctrl.procesando">Grabar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionIden()" ng-disabled="ctrl.procesando">Cancelar</button>
	</form>
</div>

<div id="nuevoIden" class="popup" style="width:1000px;">
	<span class="button b-close" ng-click="ctrl.cancelarCreacionIden()">
		<span>X</span>
	</span>
	
	<h3>Nueva información iDEN</h3>
	
	<form>
		<div id="nuevoIdenData" style="max-width:950px;height:120px;overflow:scroll;"></div><br>
		<button class="btn btn-success" ng-click="ctrl.agregarIden()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionIden()">Cancelar</button>
	</form>
</div>

<div id="eliminarIden" class="popup">
	<span class="button b-close" ng-click="ctrl.cancelarEliminacionIden()">
		<span>X</span>
	</span>
	
	<h3>Eliminar información iDEN</h3>
	
	<br>
	<p>¿Está seguro de eliminar la información?</p>
	
	<br>
	<button class="btn btn-success" ng-click="ctrl.eliminarIden()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionIden()">Cancelar</button>
</div>