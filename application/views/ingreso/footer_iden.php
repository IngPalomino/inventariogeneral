<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargando = false;
			ref.procesando = false;
			
			ref.iden = [];
			ref.editarIden = {};
			ref.eliminar_Iden = {};
			var hot_editar_iden = {};
			var hot_nuevo_iden = {};

			listarIden = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("iden/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.iden = data;
						ref.cargando = false;
					}
					else
					{
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			ref.editandoIden = function(info){
				$("#editarIden").bPopup({
					amsl: 0
				});

				ref.editarIden = info;

				var container = document.getElementById('editarIdenData');
				hot_editar_iden = new Handsontable(container,
				{
					data: ref.editarIden,
					minRows: 1,
					minCols: 15,
					colHeaders: ["Site Name","BRs","Duplexor","QBRs","Cabinet MSER","Cabinet RF","Cabinet Control","RFN","EBTS Type","iBSC Name","ACG ID (EBTS Controller)","Site IP Address","ACG Port","OMC Port","Link Status"],
					rowHeaders: false,
					columns: [
						{data: 'site_name'},
						{data: 'brs'},
						{data: 'duplexor'},
						{data: 'qbrs'},
						{data: 'cabinet_mser'},
						{data: 'cabinet_rf'},
						{data: 'cabinet_control'},
						{data: 'rfn'},
						{data: 'ebts_type'},
						{data: 'ibsc_name'},
						{data: 'acg_id'},
						{data: 'site_ip_address'},
						{data: 'acg_port'},
						{data: 'omc_port'},
						{data: 'link_status'},
					]
				});
			}

			ref.grabarIden = function(iden){
				ref.procesando = true;
				var data = "",
					campos = Object.keys(iden),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(iden[campo] != null)
						{
							if(typeof iden[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+iden[campo];
							}
							else
							{
								if(iden[campo].length != undefined)
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+JSON.stringify(iden[campo]);
								}
								else
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+iden[campo].id;
								}
							}

							contador++;
						}
					}
				});
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("iden/iden");?>/"+iden.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#editarIden").bPopup().close();
					ref.editarIden = {};
					listarIden();
					ref.procesando = false;
				}).error(function(err){
					ref.procesando = false;
				});
			}

			ref.cancelarEdicionIden = function(){
				$("#editarIden").bPopup().close();
				ref.editarIden = {};
				hot_editar_iden.destroy();
				listarIden();
			}

			ref.nuevo_iden = function(){
				$("#nuevoIden").bPopup({
					amsl: 0
				});
				
				var container = document.getElementById('nuevoIdenData');
				hot_nuevo_iden = new Handsontable(container,
				{
					minRows: 1,
					minCols: 15,
					colHeaders: ["Site Name","BRs","Duplexor","QBRs","Cabinet MSER","Cabinet RF","Cabinet Control","RFN","EBTS Type","iBSC Name","ACG ID (EBTS Controller)","Site IP Address","ACG Port","OMC Port","Link Status"],
					rowHeaders: false
				});
			}

			ref.agregarIden = function(){
				var i = 0;
				var iden_data = [];
				
				while(hot_nuevo_iden.getData()[i][0] != null)
				{
					var row = hot_nuevo_iden.getData()[i];
					var linea = {};
					row.forEach(function(dato,ind,objeto){
						linea["site_name"] = objeto[0];
						linea["brs"] = objeto[1];
						linea["duplexor"] = objeto[2];
						linea["qbrs"] = objeto[3];
						linea["cabinet_mser"] = objeto[4];
						linea["cabinet_rf"] = objeto[5];
						linea["cabinet_control"] = objeto[6];
						linea["rfn"] = objeto[7];
						linea["ebts_type"] = objeto[8];
						linea["ibsc_name"] = objeto[9];
						linea["acg_id"] = objeto[10];
						linea["site_ip_address"] = objeto[11];
						linea["acg_port"] = objeto[12];
						linea["omc_port"] = objeto[13];
						linea["link_status"] = objeto[14];
					});
					i++;
					iden_data.push(linea);
				}
				
				var data = "data="+encodeURIComponent(JSON.stringify(iden_data));
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("iden/iden/sitio");?>/"+"<?php echo $sitio;?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#nuevoIden").bPopup().close();
					listarIden();
					hot_nuevo_iden.destroy();
				}).error(function(err){

				});
			}

			ref.cancelarCreacionIden = function(){
				$("#nuevoIden").bPopup().close();
				hot_nuevo_iden.destroy();
			}

			ref.eliminandoIden = function(info){
				ref.eliminar_Iden = info;
				$("#eliminarIden").bPopup();
			}
			
			ref.eliminarIden = function(){
				$http({
					url:"<?php echo base_url("iden/iden");?>/"+ref.eliminar_Iden.id,
					method:"DELETE",
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.iden = [];
						listarIden();
						ref.eliminar_Iden = {};
						$("#eliminarIden").bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.cancelarEliminacionIden = function(){
				ref.eliminar_Iden = {};
				$("#eliminarIden").bPopup().close();
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			listarIden();
		}]);
</script>
</body>
</html>