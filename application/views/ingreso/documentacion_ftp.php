<br>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Documentación
				</div>
				<div class="panel-body ng-cloak">
				
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>

					</div>
                    <button  style="display:block;" ng-click="ctrl.openArchivoPadre()" ng-if="!ctrl.procesando">Crear Carpeta Padre</button>
                       
					<div id="docs">
					</div>
					
					<div class="alert alert-danger" ng-if="!ctrl.cargando && !ctrl.procesando && !ctrl.info">
						No hay información
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<div id="eliminarArchivo" class="popup">
	<span class="button b-close" ng-if="!ctrl.procesando">
		<span>X</span>
	</span>	
	<h3>Eliminar Archivo</h3>
	<br><br>
	<p>¿Está seguro de eliminar el elemento?</p>
	<br><br>
	<button class="btn btn-success" ng-click="ctrl.eliminarArchivo()" ng-if="!ctrl.procesando">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()" ng-if="!ctrl.procesando">Cancelar</button>
	<br>
	<br>
	<div class="text-center" ng-if="ctrl.procesando">
		<span class="fa fa-spinner fa-spin"></span> Eliminando archivo...
	</div>
</div>








<div id="eliminarArchivoDir" class="popup">
	<span class="button b-close" ng-if="!ctrl.procesando">
		<span>X</span>
	</span>	
	<h3>Eliminar Carpeta</h3>
	<br><br>
	<p>¿Está seguro de eliminar el elemento?</p>
	<br><br>
	<button class="btn btn-success" ng-click="ctrl.eliminarArchivoDir()" ng-if="!ctrl.procesando">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacionDir()" ng-if="!ctrl.procesando">Cancelar</button>
	<br>
	<br>
	<div class="text-center" ng-if="ctrl.procesando">
		<span class="fa fa-spinner fa-spin"></span> Eliminando archivo...
	</div>
</div>









<div id="crearArchivo" class="popup">
	<span class="button b-close" ng-if="!ctrl.procesando">
		<span>X</span>
	</span>	
	<h3>Crear Carpetas</h3>
	<br><br>
	<p>¿Está seguro de crear el elemento?</p>
	<br><br>
     <input type="text" name="nombredirectorio" id="nombredirectorio" ng-model="ctrl.crear" >     
	<button class="btn btn-success" ng-disabled="!ctrl.crear"  ng-click="ctrl.crearArchivo()" ng-if="!ctrl.procesando" >Crear</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarCreacion()" ng-if="!ctrl.procesando">Cancelar</button>
	<br><br>
	<div class="text-center" ng-if="ctrl.procesando">
		<span class="fa fa-spinner fa-spin"></span> Creando archivo...
	</div>
</div>







<div id="crearArchivoPadre" class="popup">
	<span class="button b-close" ng-if="!ctrl.procesando">
		<span>X</span>
	</span>	
	<h3>Crear Carpeta Padre</h3>
	<br><br>
	<p>¿Está seguro de crear el elemento?</p>
	<br><br>
     <input type="text" name="nombredirectoriopadre" id="nombredirectoriopadre" ng-model="ctrl.crearpadre" >     
	<button class="btn btn-success" ng-disabled="!ctrl.crearpadre"  ng-click="ctrl.crearArchivoPadre()" ng-if="!ctrl.procesando" >Crear</button>
	<button class="btn btn-danger" ng-click="ctrl.closeArchivoPadre()" ng-if="!ctrl.procesando">Cancelar</button>
	<br><br>
	<div class="text-center" ng-if="ctrl.procesando">
		<span class="fa fa-spinner fa-spin"></span> Creando archivo...
	</div>
</div>







<div id="subirArchivo" class="popup">
	<span class="button b-close" ng-if="!ctrl.procesando">
		<span>X</span>
	</span>
	
	<h3>Subir Archivo</h3>
	<br><br>
	<p>Seleccione el archivo que desea subir:</p>
	<br>
	<form ng-if="!ctrl.procesando">
		<input type="file" id="file" name="file" /><br>
		<button class="btn btn-success" ng-click="ctrl.subirArchivo()">Subir</button>
	</form>
	
	<div class="text-center" ng-if="ctrl.procesando">
		<span class="fa fa-spinner fa-spin"></span> Subiendo archivo...
	</div>
</div>

<div id="renombrarArchivo" class="popup">
	<span class="button b-close" ng-if="!ctrl.procesando">
		<span>X</span>
	</span>
	
	<h3>Renombrar Archivo</h3>
	<br><br>
	<p>Escriba el nuevo nombre del archivo:</p>
	<br>
	<form ng-if="!ctrl.procesando">
		<input type="text" id="nombre" name="nombre" maxlength="30" onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9\._()\s*\-]/, '')" ng-model="ctrl.renombrar"/>
		<button class="btn btn-success" ng-click="ctrl.renombrarArchivo()" ng-disabled="!ctrl.renombrar">Renombrar</button>
	</form>
	
	<div class="text-center" ng-if="ctrl.procesando">
		<span class="fa fa-spinner fa-spin"></span> Renombrando archivo...
	</div>
</div>



<div id="renombrarArchivoDir" class="popup">
	<span class="button b-close" ng-if="!ctrl.procesando">
		<span>X</span>
	</span>
	
	<h3>Renombrar Carpeta</h3>
	<br><br>
	<p>Escriba el nuevo nombre de la carpeta:</p>
	<br>
	<form ng-if="!ctrl.procesando">
		<input type="text" id="nombredir" name="nombredir" maxlength="30" onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9\._()\s*\-]/, '')" ng-model="ctrl.renombrardir"/>
		<button class="btn btn-success" ng-click="ctrl.renombrarArchivoDir()" ng-disabled="!ctrl.renombrardir">Renombrar</button>
	</form>
	
	<div class="text-center" ng-if="ctrl.procesando">
		<span class="fa fa-spinner fa-spin"></span> Renombrando Carpeta...
	</div>
</div>








<!-- creando carpetas  -->
<!-- <div id="crearArchivo" class="popup">
	<span class="button b-close" ng-if="!ctrl.procesando">
		<span>X</span>
	</span>	
	<h3>Crear Carpeta</h3>
	<br><br>
	<p>Escriba el Nombre de Carpeta:</p>
	<br>
	<form ng-if="!ctrl.procesando">
		<input type="text" id="nombre" name="nombre" maxlength="30" onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9\._()\s*\-]/, '')" ng-model="ctrl.crear"/>
		<button class="btn btn-success" ng-click="ctrl.crearArchivo()" ng-disabled="!ctrl.crear">Crear</button>
	</form>
	
	<div class="text-center" ng-if="ctrl.procesando">
		<span class="fa fa-spinner fa-spin"></span> Creando carpeta...
	</div>
</div> -->
<!-- creando carpetas  -->