<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="suministro in ctrl.suministros" ng-if="!ctrl.cargando && ctrl.suministros.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Suministro {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr>
								<td class="info">Tipo de alimentación</td>
								<td class="default">
									<span ng-show="!suministro.editar">{{suministro.tipo_alimentacion.tipo_alimentacion}}</span>
									<select class="form-control" ng-hide="!suministro.editar" ng-model="suministro.tipo_alimentacion.id">
										<option value="">Elegir tipo de alimentación</option>
										<option ng-repeat="alimentacion in ctrl.tipo_alimentacion" value="{{alimentacion.id}}">{{alimentacion.tipo_alimentacion}}</option>
									</select>
								</td>
								<td class="info">Concesionaria</td>
								<td class="default">
									<span ng-show="!suministro.editar">{{suministro.concesionaria_suministro.concesionaria_suministro}}</span>
									<select class="form-control" ng-hide="!suministro.editar" ng-model="suministro.concesionaria_suministro.id">
										<option value="">Elegir concesionaria</option>
										<option ng-repeat="concesionaria in ctrl.concesionarias_suministro" value="{{concesionaria.id}}">{{concesionaria.concesionaria_suministro}}</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="info">Conexión</td>
								<td class="default">
									<span ng-show="!suministro.editar">{{suministro.conexion_suministro.conexion_suministro}}</span>
									<select class="form-control" ng-hide="!suministro.editar" ng-model="suministro.conexion_suministro.id">
										<option value="">Elegir conexión</option>
										<option ng-repeat="conexion in ctrl.conexiones_suministro" value="{{conexion.id}}">{{conexion.conexion_suministro}}</option>
									</select>
								</td>
								<td class="info">Suministro</td>
								<td class="default">
									<span ng-show="!suministro.editar">{{suministro.numero_suministro}}</span>
									<input class="form-control" placeholder="Escribir el # suministro" ng-hide="!suministro.editar" ng-model="suministro.numero_suministro" />
								</td>
							</tr>
							<tr>
								<td class="info">Potencia contratada</td>
								<td class="default">
									<span ng-show="!suministro.editar">{{suministro.potencia_contratada}} KW</span>
									<div class="input-group" ng-hide="!suministro.editar">
										<input class="form-control" type="number" step="any" min="0" ng-model="suministro.potencia_contratada" />
										<div class="input-group-addon">KW</div>
									</div>
								</td>
								<td class="info">Opcion tarifaria</td>
								<td class="default">
									<span ng-show="!suministro.editar">{{suministro.opcion_tarifaria.opcion_tarifaria}}</span>
									<select class="form-control" ng-hide="!suministro.editar" ng-model="suministro.opcion_tarifaria.id">
										<option value="">Elegir opción tarifaria</option>
										<option ng-repeat="opcion in ctrl.opcion_tarifaria" value="{{opcion.id}}">{{opcion.opcion_tarifaria}}</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="info">Nivel de tensión</td>
								<td class="default">
									<span ng-show="!suministro.editar">{{suministro.nivel_tension}} KV</span>
									<div class="input-group" ng-hide="!suministro.editar">
										<input class="form-control" type="number" step="any" min="0" ng-model="suministro.nivel_tension" />
										<div class="input-group-addon">KV</div>
									</div>
								</td>
								<td class="info">Sistema</td>
								<td class="default">
									<span ng-show="!suministro.editar">{{suministro.sistema_suministro.sistema_suministro}}</span>
									<select class="form-control" ng-hide="!suministro.editar" ng-model="suministro.sistema_suministro.id">
										<option value="">Elegir sistema</option>
										<option ng-repeat="sistema in ctrl.sistemas_suministro" value="{{sistema.id}}">{{sistema.sistema_suministro}}</option>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan="2"></td>
								<td style="font-size:8.5pt;list-style:none;text-align:right;" colspan="2">
									<span ng-if="!suministro.logs_suministros.length || suministro.logs_suministros[0].evento.tipo != 'INSERT'">Creado: {{suministro.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por Base de Datos</span>
									<span ng-if="suministro.logs_suministros[0].evento.tipo == 'INSERT'">Creado: {{suministro.logs_suministros[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{suministro.logs_suministros[0].usuario.nombres}} {{suministro.logs_suministros[0].usuario.apellido_paterno}}</span>
									<br>
									<span ng-if="suministro.logs_suministros[1].evento.tipo == 'UPDATE'">Actualizado: {{suministro.logs_suministros[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{suministro.logs_suministros[1].usuario.nombres}} {{suministro.logs_suministros[1].usuario.apellido_paterno}}</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-sm btn-warning" ng-click="ctrl.editarSuministro(suministro)" ng-show="!suministro.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Editar</button>
					<button class="btn btn-sm btn-success" ng-click="ctrl.grabarSuministro(suministro)" ng-hide="!suministro.editar">Grabar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarSuministro(suministro)" ng-hide="!suministro.editar">Cancelar</button>
					<button class="btn btn-sm btn-danger" ng-click="ctrl.eliminandoSuministro(suministro)" ng-show="!suministro.editar" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">Eliminar</button>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="ctrl.info_sitio.coubicador.id != 1 && !ctrl.cargando">
			<div class="well text-center">
				Está coubicado en {{ctrl.info_sitio.coubicador.coubicador}}
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.suministros.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-default" style="width:100%;margin-bottom:15px;margin-top:15px;" ng-click="ctrl.nuevo_suministro()" ng-disabled="<?php echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>"><i class="fa fa-plus"></i> Añadir Suministro</button>
		</div>
	</div>
</div>

<div id="nuevoSuministro" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nuevo suministro</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Tipo de alimentación</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoSuministro.tipo_alimentacion">
							<option value="">Elegir tipo de alimentación</option>
							<option ng-repeat="alimentacion in ctrl.tipo_alimentacion" value="{{alimentacion.id}}">{{alimentacion.tipo_alimentacion}}</option>
						</select>
					</td>
					<td class="info">Concesionaria</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoSuministro.concesionaria_suministro">
							<option value="">Elegir concesionaria</option>
							<option ng-repeat="concesionaria in ctrl.concesionarias_suministro" value="{{concesionaria.id}}">{{concesionaria.concesionaria_suministro}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Conexión</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoSuministro.conexion_suministro">
							<option value="">Elegir conexión</option>
							<option ng-repeat="conexion in ctrl.conexiones_suministro" value="{{conexion.id}}">{{conexion.conexion_suministro}}</option>
						</select>
					</td>
					<td class="info">Suministro</td>
					<td class="default">
						<input class="form-control" placeholder="Escribir el # suministro" ng-model="ctrl.nuevoSuministro.numero_suministro" />
					</td>
				</tr>
				<tr>
					<td class="info">Potencia contratada</td>
					<td class="default">
						<div class="input-group">
							<input class="form-control" type="number" step="any" min="0" ng-model="ctrl.nuevoSuministro.potencia_contratada" />
							<div class="input-group-addon">KW</div>
						</div>
					</td>
					<td class="info">Opcion tarifaria</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoSuministro.opcion_tarifaria">
							<option value="">Elegir opción tarifaria</option>
							<option ng-repeat="opcion in ctrl.opcion_tarifaria" value="{{opcion.id}}">{{opcion.opcion_tarifaria}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Nivel de tensión</td>
					<td class="default">
						<div class="input-group">
							<input class="form-control" type="number" step="any" min="0" ng-model="ctrl.nuevoSuministro.nivel_tension" />
							<div class="input-group-addon">KV</div>
						</div>
					</td>
					<td class="info">Sistema</td>
					<td class="default">
						<select class="form-control" ng-model="ctrl.nuevoSuministro.sistema_suministro">
							<option value="">Elegir sistema</option>
							<option ng-repeat="sistema in ctrl.sistemas_suministro" value="{{sistema.id}}">{{sistema.sistema_suministro}}</option>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregarSuministro(ctrl.nuevoSuministro)" ng-disabled="!ctrl.nuevoSuministro.concesionaria_suministro || !ctrl.nuevoSuministro.conexion_suministro || !ctrl.nuevoSuministro.numero_suministro || !ctrl.nuevoSuministro.numero_suministro || !ctrl.nuevoSuministro.potencia_contratada || !ctrl.nuevoSuministro.opcion_tarifaria || !ctrl.nuevoSuministro.nivel_tension || !ctrl.nuevoSuministro.sistema_suministro">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacionSuministro()">Cancelar</button>
	</form>
</div>

<div id="eliminarSuministro" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar suministro</h3>

	<p>¿Está seguro de eliminar el suministro?</p>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.eliminar_Suministro.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.eliminarSuministro()" ng-disabled="!ctrl.eliminar_Suministro.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>