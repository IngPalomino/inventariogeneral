<br>
<div class="container">
	<form class="form-inline">
		<button class="btn btn-default btn-xs" ng-click="ctrl.nuevo_Enlace()"><span class="fa fa-plus"></span> Nuevo enlace</button>
	</form>
	<br>
	<table class="table table-striped table-condensed">
		<thead>
			<tr>
				<th>IDU</th>
				<th>Configuración</th>
				<th>Sitio Far End</th>
				<th>IDU Far End</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
					<button class="btn btn-warning btn-xs">Cambio de frecuencia</button>
					<button class="btn btn-warning btn-xs">Cambio de protección</button>
					<button class="btn btn-danger btn-xs">Eliminar</button>
					<button class="btn btn-info btn-xs">Re-enrutar</button>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div id="nuevoEnlace" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Nuevo enlace</h3>

	<form style="max-height:500px;overflow-y:scroll;">
		<table class="table table-condensed">
			<thead>
				<tr>
					<td class="text-center" colspan="2"><b>Near End</b></td>
					<td class="text-center" colspan="2"><b>Far End</b></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="info">Sitio</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.nuevoEnlace.sitio_ne" readonly>
					</td>
					<td class="info">Sitio</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.nuevoEnlace.sitio_fe">
					</td>
				</tr>
				<tr>
					<td class="info">IDU NE ID</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.nuevoEnlace.ne_id_ne">
					</td>
					<td class="info">IDU NE ID</td>
					<td class="default">
						<input type="text" class="form-control input-sm" ng-model="ctrl.nuevoEnlace.ne_id_fe">
					</td>
				</tr>
				<tr>
					<td class="info">Configuración y protección</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" min="1" class="form-control" style="max-width:60px;">
							<div class="input-group-addon">+</div>
							<input type="number" min="0" class="form-control" style="max-width:60px;">
							<div class="input-group-addon">
								<select>
									<option></option>
								</select>
							</div>
							<div class="input-group-addon">
								<select>
									<option></option>
								</select>
							</div>
						</div>
					</td>
					<td class="info">Configuración y protección</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<input type="number" min="1" class="form-control" style="max-width:60px;">
							<div class="input-group-addon">+</div>
							<input type="number" min="0" class="form-control" style="max-width:60px;">
							<div class="input-group-addon">
								<select>
									<option value="">-</option>
									<option value="XPIC">XPIC</option>
								</select>
							</div>
							<div class="input-group-addon">
								<select>
									<option>-</option>
									<option>HSB</option>
									<option>SD</option>
									<option>FD</option>
								</select>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Ancho de banda (Bandwidth)</td>
					<td class="default">
						<div class="input-group">
							<input type="number" min="0" class="form-control input-sm">
							<div class="input-group-addon">MHz</div>
						</div>
					</td>
					<td class="info">Ancho de banda (Bandwidth)</td>
					<td class="default">
						<div class="input-group">
							<input type="number" min="0" class="form-control input-sm">
							<div class="input-group-addon">MHz</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Modulación</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<select class="form-control">
								<option></option>
							</select>
							<div class="input-group-addon">
								<select>
									<option></option>
								</select>
							</div>
						</div>
					</td>
					<td class="info">Modulación</td>
					<td class="default">
						<div class="input-group input-group-sm">
							<select class="form-control">
								<option></option>
							</select>
							<div class="input-group-addon">
								<select>
									<option></option>
								</select>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Capacidad</td>
					<td class="default">
						<div class="input-group">
							<input type="number" min="0" class="form-control input-sm">
							<div class="input-group-addon">Mbit/s</div>
						</div>
					</td>
					<td class="info">Capacidad</td>
					<td class="default">
						<div class="input-group">
							<input type="number" min="0" class="form-control input-sm">
							<div class="input-group-addon">Mbit/s</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Frecuencia de transmisión</td>
					<td class="default">
						<div class="input-group">
							<input type="number" min="0" class="form-control input-sm">
							<div class="input-group-addon">KHz</div>
						</div>
					</td>
					<td class="info">Frecuencia de transmisión</td>
					<td class="default">
						<div class="input-group">
							<input type="number" min="0" class="form-control input-sm">
							<div class="input-group-addon">KHz</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Potencia de transmisión</td>
					<td class="default">
						<div class="input-group">
							<input type="number" min="0" class="form-control input-sm">
							<div class="input-group-addon">dBm</div>
						</div>
					</td>
					<td class="info">Potencia de transmisión</td>
					<td class="default">
						<div class="input-group">
							<input type="number" min="0" class="form-control input-sm">
							<div class="input-group-addon">dBm</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info" colspan="2">Distancia de enlace</td>
					<td colspan="2">
						<div class="input-group">
							<input type="number" min="0" class="form-control input-sm">
							<div class="input-group-addon">km</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info">Azimuth</td>
					<td class="default">
						<div class="input-group">
							<input type="number" min="0" max="360" class="form-control input-sm">
							<div class="input-group-addon">°</div>
						</div>
					</td>
					<td class="info">Azimuth</td>
					<td class="default">
						<div class="input-group">
							<input type="number" min="0" max="360" class="form-control input-sm">
							<div class="input-group-addon">°</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="info" colspan="4">Equipos</td>
				</tr>
				<tr>
					<td class="info">Antena</td>
					<td>
						<button class="btn btn-info btn-xs">Crear antena</button>
						<button class="btn btn-success btn-xs">Agregar antena suelta</button>
					</td>
					<td class="info">Antena</td>
					<td>
						<button class="btn btn-info btn-xs">Crear antena</button>
						<button class="btn btn-success btn-xs">Agregar antena suelta</button>
					</td>
				</tr>
				<tr>
					<td class="info">ODU</td>
					<td>
						<button class="btn btn-info btn-xs">Crear ODU</button>
						<button class="btn btn-success btn-xs">Agregar ODU suelta</button>
					</td>
					<td class="info">ODU</td>
					<td>
						<button class="btn btn-info btn-xs">Crear ODU</button>
						<button class="btn btn-success btn-xs">Agregar ODU suelta</button>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.agregar()">Agregar</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacion()">Cancelar</button>
	</form>
</div>