<!--<p>
	<?php print_r($session);?>
</p>-->

<script type="text/javascript">
	angular
		.module('app',['ngMessages'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.editar = false;
			ref.info_torre = [];
			ref.editar_torre = [];
			
			ref.tipos_torre = [];
			ref.camuflajes = [];
			ref.departamentos = [];
			ref.provincias = [];
			ref.distritos = [];
			ref.ubicacion_equipos = [];
			ref.coubicadores = [];
			ref.operadores_coubicantes = [];
			ref.proyectos = [];
			ref.contratas_constructoras = [];
			ref.tipos_solucion = [];
			ref.proveedores_mantenimiento = [];
			ref.tipo_proyecto = [];

			ref.nuevo_contrato = {};
			ref.eliminar_contrato = {};

			ref.nuevo_contacto = {
				contrato: {},
				contacto: "",
				telefono: [],
				correo: []
			};
			ref.eliminar_contacto = {};

			ref.tipo_estacion = [
				{id: 1, tipo_estacion: "Greenfield"},
				{id: 2, tipo_estacion: "Rooftop"},
				{id: 3, tipo_estacion: "COW"}
			];

			torre = function(){
				$http({
					url:"<?php echo base_url("index.php/status_site/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						data[0].latitud = Number(data[0].latitud);
						data[0].longitud = Number(data[0].longitud);
						data[0].altura_edificacion = Number(data[0].altura_edificacion);
						data[0].altura_torre = Number(data[0].altura_torre);
						data[0].factor_uso_torre = (data[0].factor_uso_torre? Number(data[0].factor_uso_torre) : null);
						data[0].anio_construccion = Number(data[0].anio_construccion);
						data[0].acceso_libre_24h = Boolean(Number(data[0].acceso_libre_24h));
						data[0].in_building = Boolean(Number(data[0].in_building));
						/*data[0].coubicado = Boolean(Number(data[0].coubicado));*/
						data[0].agregador = Boolean(Number(data[0].agregador));
						data[0].dorsal = Boolean(Number(data[0].dorsal));
						try{ data[0].fecha_estado_on_air = new Date(data[0].fecha_estado_on_air + " 00:00:00"); }catch(err){ data[0].fecha_estado_on_air = null; }

						data[0].contratos.forEach(function(contrato,ind,objeto){
							objeto[ind].area = Number(objeto[ind].area);
							try{ objeto[ind].fecha_inicio = new Date(objeto[ind].fecha_inicio + " 00:00:00"); }catch(err){ objeto[ind].fecha_inicio = null; }
							try{ objeto[ind].fecha_fin = new Date(objeto[ind].fecha_fin + " 00:00:00"); }catch(err){ objeto[ind].fecha_fin = null; }

							contrato.contactos_contrato.forEach(function(contacto,indx,objx){
								objx[indx].telefono = JSON.parse(contacto.telefono);
								objx[indx].correo = JSON.parse(contacto.correo);
							});
						});

						try{ data[0].created_at = Date.parse(data[0].created_at); } catch(err){ data[0].created_at = null; }

						data[0].logs_status_site.forEach(function(log,ind,objeto){
							try{ objeto[ind].created_at = Date.parse(objeto[ind].created_at); } catch(err){ objeto[ind].created_at = null; }
						});

						ref.info_torre = data[0];
						/*console.log(data);*/
					}
					else
					{
						
					}
				}).error(function(err){

				});
			}

			listar_tipo_torre = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_torre/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipos_torre = data
					}
				}).error(function(err){

				});
			}

			listar_camuflajes = function(){
				$http({
					url:"<?php echo base_url("index.php/camuflaje/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.camuflajes = data
					}
				}).error(function(err){

				});
			}

			listar_zonas = function(){
				$http({
					url:"<?php echo base_url("index.php/zonas/listar/departamentos");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.departamentos = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_ubicacion_equipos = function(){
				$http({
					url:"<?php echo base_url("index.php/ubicacion_equipos/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.ubicacion_equipos = data
					}
				}).error(function(err){

				});
			}

			listar_tipos_solucion = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_solucion/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipos_solucion = data
					}
				}).error(function(err){

				});
			}

			listar_proveedores_mantenimiento = function(){
				$http({
					url:"<?php echo base_url("index.php/proveedor_mantenimiento/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.proveedores_mantenimiento = data
					}
				}).error(function(err){

				});
			}

			listarTipoProyecto = function() {
				$http({
					url: "<?php echo base_url("tipo_proyecto/listar"); ?>",
					method: "GET"
				}).then(function successCallback(resp) {
					if(resp.data.length) {
						ref.tipo_proyecto = resp.data;
					} else {
						ref.tipo_proyecto = [];
					}
				}, function errorCallback(resp) {

				});
			}

			ref.info = {
				info: 1,
				mostrarInfo: function(vista){
					this.info = vista;
				}
			};

			ref.listar_provincias = function(departamento){
				$http({
					url:"<?php echo base_url("index.php/zonas/listar/provincias");?>"+"/"+encodeURI(departamento),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.provincias = data
						ref.distritos = [];
					}
				}).error(function(err){

				});
			}

			ref.listar_distritos = function(departamento,provincia){
				$http({
					url:"<?php echo base_url("index.php/zonas/listar/distritos");?>"+"/"+encodeURI(departamento)+"/"+encodeURI(provincia),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.distritos = data
					}
				}).error(function(err){

				});
			}

			listar_coubicadores = function(){
				$http({
					url:"<?php echo base_url("index.php/coubicador/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.coubicadores = data
					}
				}).error(function(err){

				});
			}

			listar_coubicantes = function(){
				$http({
					url:"<?php echo base_url("index.php/coubicante/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.operadores_coubicantes = data
					}
				}).error(function(err){

				});
			}

			listar_proyectos = function(){
				$http({
					url:"<?php echo base_url("index.php/proyecto/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.proyectos = data
					}
				}).error(function(err){

				});
			}

			listar_contratas_constructoras = function(){
				$http({
					url:"<?php echo base_url("index.php/contrata_constructora/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.contratas_constructoras = data
					}
				}).error(function(err){

				});
			}

			ref.editarTorre = function(){
				ref.editar = true;
				ref.listar_provincias(ref.info_torre.zona_peru.departamento);
				ref.listar_distritos(ref.info_torre.zona_peru.departamento,ref.info_torre.zona_peru.provincia)
				/*$http({
					url:"<?php echo base_url("index.php/status_site/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						data[0].latitud = Number(data[0].latitud);
						data[0].longitud = Number(data[0].longitud);
						data[0].altura_edificacion = Number(data[0].altura_edificacion);
						data[0].altura_torre = Number(data[0].altura_torre);
						data[0].anio_construccion = Number(data[0].anio_construccion);
						data[0].in_building = Boolean(Number(data[0].in_building));
						data[0].coubicado = Boolean(Number(data[0].coubicado));
						data[0].agregador = Boolean(Number(data[0].agregador));
						data[0].dorsal = Boolean(Number(data[0].dorsal));

						ref.editar_torre = data[0];

						ref.listar_provincias(ref.editar_torre.zona_peru.departamento)
						ref.listar_distritos(ref.editar_torre.zona_peru.departamento,ref.editar_torre.zona_peru.provincia)

						ref.editar = true
					}
					else
					{
						
					}
				}).error(function(err){

				});*/
			}

			ref.cambioGreenfieldRooftop = function(){
				if(ref.info_torre.greenfield_rooftop == "0")
				{
					ref.info_torre.altura_edificacion = 0;
				}
				else
				{
					ref.info_torre.altura_edificacion = Number(ref.info_torre.altura_edificacion);
				}
			}

			ref.cambioCoubicadoEn = function(){
				if(ref.info_torre.coubicador.id == "1" || ref.info_torre.coubicador.id == "")
				{
					ref.info_torre.nombre_sitio_coubicador = "N/A";
					ref.info_torre.codigo_torrera = "N/A";
				}
				else
				{
					ref.info_torre.coubicado = true;
					ref.info_torre.nombre_sitio_coubicador = "";
					ref.info_torre.codigo_torrera = "";
				}
			}

			ref.cambioCoubicante = function(){
				if(ref.info_torre.operador_coubicante.id == "1" || ref.info_torre.operador_coubicante.id == "")
				{
					ref.info_torre.nombre_sitio_coubicante = "N/A";
				}
				else
				{
					ref.info_torre.nombre_sitio_coubicante = "";
				}
			}

			ref.cancelarEdicion = function(){
				torre();
				ref.editar = false;

				ref.error.error = false;
				ref.error.actualizar = "";
			}

			ref.grabar = function(){
				var data = "",
					popSitio = ref.info_torre,
					campos = Object.keys(popSitio),
					contador = 0;

				borrarPropiedades(popSitio,"logs_status_site");
				/*console.log(popSitio);*/
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "codigo" && campo != "nombre" && campo != "nombre_completo")
					{
						if(popSitio[campo] != null)
						{
							if(typeof popSitio[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								
                                data += campo+"="+popSitio[campo];

							}
							else
							{
								if(campo == "fecha_estado_on_air" || campo == "fecha_inicio_contrato" || campo == "fecha_fin_contrato")
								{
									if(contador > 0)
									{
										data += "&";
									}
									try{data += campo+"="+popSitio[campo].toISOString();}
									catch(err){ data += campo+"=null"; }
								}
								else if(campo == "zona_peru")
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += "departamento="+popSitio[campo]['departamento']+"&";
									data += "provincia="+popSitio[campo]['provincia']+"&";
									data += "distrito="+popSitio[campo]['distrito'];
								}
								else
								{
									if(popSitio[campo].length != undefined)
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+angular.toJson(popSitio[campo]);
									}
									else
									{
										if(contador > 0)
										{
											data += "&";
										}
										data += campo+"="+popSitio[campo].id;
									}
										
								}
							}

							contador++;
						}
					}
				});
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("index.php/status_site/sitio/$sitio");?>",
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					console.log(data);
					if(!(data.error))
					{
						torre();
						ref.editar = false;
					}
					else
					{
						ref.error.error = true;
						ref.error.actualizar = data.error;
					}
				}).error(function(err){
					
				});
			}

			ref.nuevoContrato = function(){
				$("#nuevoContrato").bPopup();
				ref.nuevo_contrato = {
					tiene_fecha_fin: false,
					indeterminado: false
				};
			}

			ref.sinNuevoContrato = function(){
				$("#sinNuevoContrato").bPopup({
					onOpen: function(){
						document.getElementById("sinNuevoContrato").className = "popup active";
					},
					onClose: function(){
						document.getElementById("sinNuevoContrato").className = "popup";
					}
				});
			}

			ref.cancelarNuevoContrato = function(){
				$("#nuevoContrato").bPopup().close();
				ref.nuevo_contrato = {
					tiene_fecha_fin: false,
					indeterminado: false
				};
			}

			ref.cancelarSinNuevoContrato = function(){
				$("#sinNuevoContrato").bPopup().close();
				ref.nuevo_contrato = {};
			}

			ref.agregarNuevoContrato = function(contrato){
				var data = "",
					campos = Object.keys(contrato),
					contador = 0;

				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(contrato[campo] != null)
						{
							if(contador > 0)
							{
								data += "&";
							}

							if(campo == "fecha_inicio" || campo == "fecha_fin")
							{
								try{
									var fecha = contrato[campo].toISOString();
									if( !(typeof fecha === typeof undefined) )
									{
										data += campo+"="+contrato[campo].toISOString();
									}
									else
									{
										data += campo+"="+null;
									}
								}
								catch(err){

								}
							}
							else
							{
								data += campo+"="+contrato[campo];
							}

							contador++;
						}
					}
				});

				data += "&sitio=<?php echo $sitio;?>";
				console.log(data);

				$http({
					url:"<?php echo base_url("contratos/contrato");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(!(data.error))
					{
						$("#nuevoContrato").bPopup().close();
						if($("#sinNuevoContrato").hasClass("popup active"))
						{
							$("#sinNuevoContrato").bPopup().close();
						}
						ref.nuevo_contrato = {
							tiene_fecha_fin: false,
							indeterminado: false
						};
						torre();
						ref.editar = false;
					}
					else
					{
						ref.error.error = true;
						ref.error.actualizar = data.error;
					}
				}).error(function(err){
					
				});
			}

			ref.eliminandoContrato = function(contrato){
				ref.eliminar_contrato = contrato;
				$("#eliminarContrato").bPopup();
			}

			ref.cancelarEliminacionContrato = function(){
				$("#eliminarContrato").bPopup().close();
				ref.eliminar_contrato.comentario = "";
				ref.eliminar_contrato = {};
			}

			ref.eliminarContrato = function(contrato){
				var data = "",
					campos = Object.keys(contrato),
					contador = 0;

				data += "comentario="+contrato.comentario;

				$http({
					url:"<?php echo base_url("index.php/contratos/contrato");?>/"+contrato.id,
					method:"DELETE",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					/*console.log(data);*/
					if(!(data.error))
					{
						$("#eliminarContrato").bPopup().close();
						ref.eliminar_contrato.comentario = "";
						ref.eliminar_contrato = {};
						torre();
						ref.editar = false;
					}
					else
					{
						ref.error.error = true;
						ref.error.actualizar = data.error;
					}
				}).error(function(err){
					
				});
			}

			function telefono(telf)
			{
				this.telefono = telf;
			}

			function correo(email)
			{
				this.correo = email;
			}

			ref.nuevoContacto = function(contrato){
				ref.nuevo_contacto = {
					contrato: contrato,
					contacto: "",
					telefono: [],
					correo: []
				};
				ref.nuevo_contacto.telefono.push(new telefono(""));
				ref.nuevo_contacto.correo.push(new correo(""));
				$("#nuevoContacto").bPopup();
			}

			ref.agregarTelefonoNuevoContacto = function(){
				ref.nuevo_contacto.telefono.push(new telefono(""));
			}

			ref.agregarCorreoNuevoContacto = function(){
				ref.nuevo_contacto.correo.push(new correo(""));
			}

			ref.agregarNuevoContacto = function(contacto){
				var data = "",
					campos = Object.keys(contacto),
					contador = 0;

				campos.forEach(function(campo,ind,objeto){
					if(campo != "$$hashKey")
					{
						if(contacto[campo] != null)
						{
							if(contador > 0)
							{
								data += "&";
							}

							if(campo == "contrato")
							{
								data += "contrato="+contacto[campo].id;
							}
							else if(campo == "telefono" || campo == "correo")
							{
								contacto[campo].forEach(function(telcor,indx,obj){
									delete telcor["$$hashKey"];
								});
								data += campo+"="+JSON.stringify(contacto[campo]);
							}
							else
							{
								data += campo+"="+contacto[campo];
							}

							contador++;
						}
					}
				});

				/*console.log(data);*/

				$http({
					url:"<?php echo base_url("index.php/contactos_contrato/contacto");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					console.log(data);
					if(!(data.error))
					{
						$("#nuevoContacto").bPopup().close();
						ref.nuevo_contacto = {
							contrato: {},
							contacto: "",
							telefono: [],
							correo: []
						};
						torre();
						ref.editar = false;
					}
					else
					{
						ref.error.error = true;
						ref.error.actualizar = data.error;
					}
				}).error(function(err){
					
				});
			}

			ref.eliminandoContacto = function(contacto){
				ref.eliminar_contacto = contacto;
				$("#eliminarContacto").bPopup();
			}

			ref.cancelarEliminacionContacto = function(){
				$("#eliminarContacto").bPopup().close();
				ref.eliminar_contacto.comentario = "";
				ref.eliminar_contacto = {};
			}

			ref.eliminarContacto = function(contacto){
				var data = "",
					campos = Object.keys(contacto),
					contador = 0;

				data += "comentario="+contacto.comentario;

				$http({
					url:"<?php echo base_url("index.php/contactos_contrato/contacto");?>/"+contacto.id,
					method:"DELETE",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					/*console.log(data);*/
					if(!(data.error))
					{
						$("#eliminarContacto").bPopup().close();
						ref.eliminar_contacto.comentario = "";
						ref.eliminar_contacto = {};
						torre();
						ref.editar = false;
					}
					else
					{
						ref.error.error = true;
						ref.error.actualizar = data.error;
					}
				}).error(function(err){
					
				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};

			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}

			torre();
			listar_tipo_torre();
			listar_zonas();
			listar_camuflajes();
			listar_ubicacion_equipos();
			listar_coubicadores();
			listar_coubicantes();
			listar_proyectos();
			listar_contratas_constructoras();
			listar_tipos_solucion();
			listar_proveedores_mantenimiento();
			listarTipoProyecto();
		}]);
</script>
</body>
</html>

<!--<p>
	<?php print_r($session);?>
</p>-->

<!--<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.editar = false;
			ref.info_torre = [];
			ref.editar_torre = [];
			
			ref.tipos_torre = [];
			ref.camuflajes = [];
			ref.departamentos = [];
			ref.provincias = [];
			ref.distritos = [];

			torre = function(){
				$http({
					url:"<?php echo base_url("index.php/status_site/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.info_torre = data[0];
					}
					else
					{
						
					}
				}).error(function(err){

				});
			}

			listar_tipo_torre = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_torre/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.tipos_torre = data
					}
				}).error(function(err){

				});
			}

			listar_camuflajes = function(){
				$http({
					url:"<?php echo base_url("index.php/camuflaje/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.camuflajes = data
					}
				}).error(function(err){

				});
			}

			listar_zonas = function(){
				$http({
					url:"<?php echo base_url("index.php/zonas/listar/departamentos");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.departamentos = data
					}
				}).error(function(err){
					console.log(err);
				});
			}

			ref.listar_provincias = function(departamento){
				$http({
					url:"<?php echo base_url("index.php/zonas/listar/provincias");?>"+"/"+encodeURI(departamento),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.provincias = data
						ref.distritos = [];
					}
				}).error(function(err){

				});
			}

			ref.listar_distritos = function(departamento,provincia){
				$http({
					url:"<?php echo base_url("index.php/zonas/listar/distritos");?>"+"/"+encodeURI(departamento)+"/"+encodeURI(provincia),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.distritos = data
					}
				}).error(function(err){

				});
			}

			ref.editarTorre = function(){
				$http({
					url:"<?php echo base_url("index.php/status_site/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						data[0].latitud = Number(data[0].latitud);
						data[0].longitud = Number(data[0].longitud);
						data[0].altura_edificacion = Number(data[0].altura_edificacion);
						data[0].altura_torre = Number(data[0].altura_torre);
						data[0].anio_construccion = Number(data[0].anio_construccion);
						data[0].in_building = Boolean(Number(data[0].in_building));
						data[0].coubicado = Boolean(Number(data[0].coubicado));
						data[0].agregador = Boolean(Number(data[0].agregador));
						data[0].dorsal = Boolean(Number(data[0].dorsal));

						ref.editar_torre = data[0];

						ref.listar_provincias(ref.editar_torre.zona_peru.departamento)
						ref.listar_distritos(ref.editar_torre.zona_peru.departamento,ref.editar_torre.zona_peru.provincia)

						ref.editar = true
					}
					else
					{
						
					}
				}).error(function(err){

				});
			}

			ref.establecerAltura = function(){
				if(ref.editar_torre.greenfield_rooftop == "0")
				{
					ref.editar_torre.altura_edificacion = 0;
				}
				else
				{
					ref.editar_torre.altura_edificacion = Number(ref.info_torre.altura_edificacion);
				}
			}

			ref.cancelarEdicion = function(){
				torre();
				ref.editar = false;

				ref.error.error = false;
				ref.error.actualizar = "";
			}

			ref.grabar = function(){
				var data = "codigo="+ref.editar_torre.codigo
							+"&nombre="+ref.editar_torre.nombre
							+"&departamento="+ref.editar_torre.zona_peru.departamento
							+"&provincia="+ref.editar_torre.zona_peru.provincia
							+"&distrito="+ref.editar_torre.zona_peru.distrito
							+"&latitud="+ref.editar_torre.latitud
							+"&longitud="+ref.editar_torre.longitud
							+"&direccion="+ref.editar_torre.direccion
							+"&in_building="+ref.editar_torre.in_building
							+"&coubicado="+ref.editar_torre.coubicado
							+"&agregador="+ref.editar_torre.agregador
							+"&dorsal="+ref.editar_torre.dorsal
							+"&greenfield_rooftop="+Boolean(Number(ref.editar_torre.greenfield_rooftop))
							+"&altura_edificacion="+ref.editar_torre.altura_edificacion
							+"&tipo_torre="+ref.editar_torre.tipo_torre.id
							+"&altura_torre="+ref.editar_torre.altura_torre
							+"&anio_construccion="+ref.editar_torre.anio_construccion
							+"&camuflaje="+ref.editar_torre.camuflaje.id;

				$http({
					url:"<?php echo base_url("index.php/status_site/sitio/$sitio");?>",
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(!(data.error))
					{
						torre();
						ref.editar_torre = {};
						ref.editar = false;
					}
					else
					{
						ref.error.error = true;
						ref.error.actualizar = data.error;
					}
				}).error(function(err){
					
				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){

				});
			};

			torre();
			listar_tipo_torre();
			listar_zonas();
			listar_camuflajes();
		}]);
</script>
</body>
</html>-->