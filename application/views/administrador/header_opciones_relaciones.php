<br>
<div class="container">
	<nav class="navbar navbar-info navbar-curve">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarSite" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!--<a class="navbar-brand image" href><img src="assets/images/logo-transp.png" height="42px"></a>-->
			</div>
			<div id="navbarSite" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li>
						<a href="<?php echo base_url("index.php/administrar/relaciones/cols");?>" role="button">Por columnas</a>
					</li>
					<li>
						<a href="<?php echo base_url("index.php/administrar/relaciones/glob");?>" role="button">Vista global</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</div>