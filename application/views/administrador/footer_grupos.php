<script type="text/javascript">
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;

			ref.session.grupos.forEach(function(grupo){
				if(grupo.grupo == 2)
				{
					ref.administrador = true;
				}
			});

			ref.grupos = [];
			ref.modulos = [];

			ref.config = {
				valueField: 'id',
				labelField: 'modulo',
				searchField: 'modulo',
				delimiter: '|',
				placeholder: 'Seleccionar modulo(s)'
			};

			listarGrupos = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("grupos/listar");?>",
					method:"GET"
				}).success(function(data){
					ref.cargando = false;
					if(data.info.length)
					{
						ref.grupos = data.info;

						ref.grupos.forEach(function(grupo,ind,objeto){
							objeto[ind].permisos = [];
							objeto[ind].grupos_modulos.forEach(function(modulo,indx,objetox){
								objeto[ind].permisos.push(modulo.modulo);
							});

							objeto[ind].permiso = function(){
								var r = true;

								if(ref.administrador && (objeto[ind].id == 1))
								{
									r = false;
								}

								return r;
							}
						});

						console.log(ref.grupos);
					}
					else
					{
						ref.grupos = [];
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});
			}

			listarModulos = function(){
				$http({
					url:"<?php echo base_url("modulos/select");?>",
					method:"GET"
				}).success(function(data){
					if(data.info.length)
					{
						ref.modulos = data.info;
						/*console.log(ref.grupos);*/
					}
					else
					{
						ref.modulos = [];
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			ref.editandoGrupo = function(info){
				ref.editarGrupo = info;
				ref.editarGrupo_e = objEditar(ref.editarGrupo);
				$("#editarGrupo").bPopup();
			}

			ref.grabarGrupo = function(){
				ref.error = {};
				var alertEditar = document.getElementById("alert-editar");
				alertEditar.innerHTML = "";

				var data = "";
				
				data += ((ref.editarGrupo.nombre != ref.editarGrupo_e.nombre)? "nombre="+ref.editarGrupo_e.nombre+"&" : "" );
				data += ((ref.editarGrupo.descripcion != ref.editarGrupo_e.descripcion)? "descripcion="+ref.editarGrupo_e.descripcion+"&" : "" );
				data += ((JSON.stringify(ref.editarGrupo.permisos) != JSON.stringify(ref.editarGrupo_e.permisos))? "permisos="+JSON.stringify(ref.editarGrupo_e.permisos) : "" );

				$http({
					url:"<?php echo base_url("administrador/grupos/grupo");?>/"+ref.editarGrupo.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(!data.error)
					{
						listarGrupos();
						ref.editarGrupo = {};
						ref.editarGrupo_e = {};
						$("#editarGrupo").bPopup().close();
					}
					else
					{
						ref.error.error = true;
						ref.error.editar = data.mensaje;
						alertEditar.innerHTML = ref.error.editar;
					}
				}).error(function(err){

				});
			}




           ref.eliminandoGrupo = function(info){
			 	ref.eliminarGrupo = info;
			 	$http({
					url:"<?php echo base_url("administrador/grupos/grupo");?>"+"/"+ref.eliminarGrupo.id,
					method:"DELETE"
				}).success(function(data){
					listarGrupos();					
				}).error(function(err){

				});				
			}

			




			ref.cancelarEdicion = function(){
				$("#editarGrupo").bPopup().close();
				ref.editarGrupo = {};
				ref.editarGrupo_e = {};
				ref.error.error = false;
				ref.error.editar = "";
				var alertEditar = document.getElementById("alert-editar");
				alertEditar.innerHTML = "";
				listarGrupos();
			}

			ref.nuevo_grupo = function()
			{
				$("#nuevoGrupo").bPopup({
					onOpen: function(){
						ref.nuevoGrupo = {};
					}
				});
			}

			ref.crearGrupo = function(grupo){
				ref.error = {};
				var alertCrear = document.getElementById("alert-crear");
				alertCrear.innerHTML = "";

				var data = "nombre="+encodeURIComponent(grupo.nombre)
							+"&descripcion="+encodeURIComponent(grupo.descripcion)
							+"&permisos="+JSON.stringify(grupo.permisos);
				console.log(data);

				$http({
					url:"<?php echo base_url("administrador/grupos/grupo");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(!data.error)
					{
						$("#nuevoGrupo").bPopup().close();
						ref.nuevoGrupo = {};
						listarGrupos();
					}
					else
					{
						ref.error.error = true;
						ref.error.crear = data.mensaje;
						alertCrear.innerHTML = ref.error.crear;
					}
				}).error(function(err){
					
				});
			}

			ref.cancelarCreacion = function(){
				$("#nuevoGrupo").bPopup().close();
				ref.nuevoGrupo = {};
				ref.error = {};
				var alertCrear = document.getElementById("alert-crear");
				alertCrear.innerHTML = "";
			}

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			objEditar = function(obj){
		        var tipo = typeof(obj),
		            copia,
		            propNames;

		        if(tipo != "object")
		        {
		            copia = JSON.parse(JSON.stringify(obj));
		        }
		        else
		        {
		            if( (obj != null) && (obj != undefined) )
		            {
		                if( Array.isArray(obj) )
		                {
		                    copia = [];
		                    obj.forEach(function(r,i,o){
		                        reg = objEditar(r);
		                        copia.push(reg);
		                    });
		                }
		                else
		                {
		                    try{
		                        copia = new Date(obj.getTime());
		                    }catch(err){
		                        copia = Object.create( Object.getPrototypeOf(obj) );
		                        propNames = Object.getOwnPropertyNames(obj);

		                        propNames.forEach(function(name){
		                            var tipo2 = typeof(obj[name]);

		                            if(tipo2 != "object")
		                            {
		                                var desc = Object.getOwnPropertyDescriptor(obj, name);
		                                Object.defineProperty(copia, name, desc);
		                            }
		                            else
		                            {
		                                copia[name] = objEditar(obj[name]);
		                            }
		                        });
		                    }
		                }
		            }
		            else
		            {
		                copia = obj;
		            }
		        }

		        return copia;
		    }

			listarGrupos();
			listarModulos();
		}]);
</script>
</body>
</html>