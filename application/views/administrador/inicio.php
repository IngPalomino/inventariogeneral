<section class="container panel-menu ng-cloak">
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo site_url('administrar/usuarios'); ?>" class="btn boton" role="button">
				<table class="table">
					<tbody>
						<tr><td><span class="fa fa-user fa-5x"></span></td></td></tr>
						<tr><td>Usuarios</td></tr>
					</tbody>
				</table>
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo site_url('administrar/grupos'); ?>" class="btn boton" role="button">
				<table class="table">
					<tbody>
						<tr><td><span class="fa fa-users fa-5x"></span></td></td></tr>
						<tr><td>Grupos</td></tr>
					</tbody>
				</table>
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo site_url('administrar/objetos'); ?>" class="btn boton" ng-class="<?php echo !isSuperAdmin($session["id_usuario"])? "'disabled'" : "";?>" role="button">
				<table class="table">
					<tbody>
						<tr><td><span class="fa fa-object-group fa-5x"></span></td></td></tr>
						<tr><td>Objetos</td></tr>
					</tbody>
				</table>
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo site_url('administrar/relaciones/cols'); ?>" class="btn boton" ng-class="<?php echo !isSuperAdmin($session["id_usuario"])? "'disabled'" : "";?>" role="button">
				<table class="table">
					<tbody>
						<tr><td><span class="fa fa-code-fork fa-rotate-90 fa-5x"></span></td></td></tr>
						<tr><td>Relaciones</td></tr>
					</tbody>
				</table>
			</a>
		</div>
	</div><br>
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo site_url('administrar/dependencias'); ?>" class="btn boton" ng-class="<?php echo !isSuperAdmin($session["id_usuario"])? "'disabled'" : "";?>" role="button">
				<table class="table">
					<tbody>
						<tr><td><span class="fa fa-code-fork fa-5x"></span></td></td></tr>
						<tr><td>Dependencias</td></tr>
					</tbody>
				</table>
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo site_url('administrar/ingreso_masivo'); ?>" class="btn boton" role="button">
				<table class="table">
					<tbody>
						<tr><td><span class="fa fa-database fa-5x"></span></td></td></tr>
						<tr><td>Ingreso masivo</td></tr>
					</tbody>
				</table>
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo site_url('administrar/aprobar_antena'); ?>" class="btn boton" role="button">
				<table class="table">
					<tbody>
						<tr><td><span class="fa fa-building fa-5x"></span></td></td></tr>
						<tr><td>Reserva Espacio de Torre</td></tr>
					</tbody>
				</table>
			</a>
		</div>


	</div>
</section>