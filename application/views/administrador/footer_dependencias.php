<!--<p>
	<?php print_r($session);?>
</p>-->

<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.dependencias = [];

			ref.dependenciaNueva = {
				tabla_primaria : "0",
				objetos_primario : "0",
				tabla_secundaria : "0",
				objetos_secundario : "0",
				operador_primario : "*",
				operador_secundario : "*",
				maximo : 1,
				caracteristicas : ""
			}

			ref.db_tablas = [];
			ref.db_tablasPrimarias = [];
			ref.db_tablasSecundarias = [];
			ref.colsTablaPrimaria = [];
			ref.colsTablaSecundaria = [];

			listar_dependencias = function(){
				$http({
					url:"<?php echo base_url("index.php/dependencias/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.dependencias = data;
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			listar_tablas = function(){
				$http({
					url:"<?php echo base_url("index.php/basedatos/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.db_tablas = data;

						data.forEach(function(tabla){
							var obj = {tabla:""};

							if(!(tabla.Tables_in_db_inventario_general == "tb_logs" || tabla.Tables_in_db_inventario_general == "tb_usuarios" || tabla.Tables_in_db_inventario_general == "tb_relaciones" || tabla.Tables_in_db_inventario_general == "tb_dependencias"))
							{
								obj.tabla = tabla.Tables_in_db_inventario_general;
								ref.db_tablasPrimarias.push(obj);
							}
						});
					}
					else
					{

					}
				}).error(function(data){

				});
			}

			ref.seleccionPrimaria = function(){
				ref.db_tablasSecundarias = [];
				ref.colsTablaSecundaria = [];
				ref.dependenciaNueva.maximo = 1;
				if(ref.dependenciaNueva.tabla_primaria != "0")
				{
					ref.db_tablasPrimarias.forEach(function(tabla){
						var obj = {tabla:""};

						if(tabla.tabla != ref.dependenciaNueva.tabla_primaria)
						{
							obj.tabla = tabla.tabla;
							ref.db_tablasSecundarias.push(obj);
						}
					});

					$http({
						url:"<?php echo base_url("index.php/basedatos/listar/columnas");?>"+"/"+ref.dependenciaNueva.tabla_primaria,
						method:"GET"
					}).success(function(data){
						if(data.length)
						{
							data.splice(data.length-2,2);

							ref.colsTablaPrimaria = data;
						}
					}).error(function(err){

					});
				}
			}

			ref.seleccionSecundaria = function(){
				if(ref.dependenciaNueva.tabla_secundaria != "0")
				{
					$http({
						url:"<?php echo base_url("index.php/basedatos/listar/columnas");?>"+"/"+ref.dependenciaNueva.tabla_secundaria,
						method:"GET"
					}).success(function(data){
						if(data.length)
						{
							data.splice(data.length-2,2);

							ref.colsTablaSecundaria = data;
						}
					}).error(function(err){

					});
				}
			}

			ref.crear = function(){
				var data = "tabla_primaria="+ref.dependenciaNueva.tabla_primaria
							+"&objetos_primario="+ref.dependenciaNueva.objetos_primario
							+"&tabla_secundaria="+ref.dependenciaNueva.tabla_secundaria
							+"&objetos_secundario="+ref.dependenciaNueva.objetos_secundario
							+"&operador_primario="+ref.dependenciaNueva.operador_primario
							+"&operador_secundario="+ref.dependenciaNueva.operador_secundario
							+"&maximo="+ref.dependenciaNueva.maximo;

				$http({
					url:"<?php echo base_url("index.php/dependencias/dependencia");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					listar_dependencias();
				}).error(function(err){

				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			listar_dependencias();
			listar_tablas();
		}]);
</script>
</body>
</html>