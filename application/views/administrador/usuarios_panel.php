<br>
<div class="container">
	<nav class="navbar navbar-info navbar-curve">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarSite" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div id="navbarSite" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li>
						<a href role="button">Listar</a>
					</li>
					<li>
						<a href class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Movimientos <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href>Todos</a></li>
							<li><a href>Por usuario</a></li>
							<li><a href>Por elemento</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</div>