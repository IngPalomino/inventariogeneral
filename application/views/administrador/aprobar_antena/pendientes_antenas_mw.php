<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h4>Aprobar Solicitudes</h4>
			</div>
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url("administrar"); ?>">Administrar</a></li>
				<li><a href="<?php echo base_url("administrar/aprobar_antena"); ?>">Aprobar  Solicitud</a></li>
				<li class="active">Elemento - Solicitudes</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary with-nav-tabs">
				<div class="panel-heading text-center">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#solicitud" data-toggle="tab"><strong>Solicitud Reserva de antenas  </strong></a></li>
						<li><a href="#pendientes" data-toggle="tab"><strong>Filas Pendientes de Aprobación</strong> <span class="badge">{{ctrl.filas_pendientes.length}}</span></a></li>
						<li><a href="#reservado" data-toggle="tab"><strong>Filas Reservadas</strong> <span class="badge">{{ctrl.filas_reservado.length}}</span></a></li>
                        <li><a href="#instalado_sitio" data-toggle="tab"><strong>Filas Instaladas</strong> <span class="badge">{{ctrl.filas_instalado.length}}</span></a></li>
						
					</ul>
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div id="solicitud" class="tab-pane fade in active">
							<table class="table table-condensed table-responsive">
								<tbody>
									<tr>
										<td class="info text-right">Fecha de solicitud</td>
										<td class="active">{{ctrl.elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}}</td>
									</tr>
									<tr>
										<td class="info text-right">Usuario solicitante</td>
										<td class="active">{{ctrl.elemento.nombres}} {{ctrl.elemento.apellido_paterno}}</td>
									</tr>
									<tr>
										<td class="info text-right">Módulo</td>
										<td class="active">Antenas Microondas</td>
									</tr>
								    <tr>
										<td class="info text-right">Estado</td>
										<td class="active">
	<label style="font-size:12px;padding:5px;" ng-class="{'naranja': ctrl.elemento.estado_aprobacion == 1, 'red': ctrl.elemento.estado_aprobacion == 2 ,'label label-success': ctrl.elemento.estado_aprobacion == 3}" >
										{{ctrl.elemento.estadotexto}}
									   </label>
									</td>
									</tr>									
								</tbody>
							</table>
						</div>

                         <div id="pendientes" class="tab-pane fade">
							<div style="max-height:300px;overflow-y:auto;">
								<table class="table table-condensed table-responsive table-hover small-font">
									<thead>
										<tr>
										<th><input type="checkbox" ng-click="ctrl.selectAllPendiente()" ng-model="ctrl.isAllSelected"></th>	
											<th>Sitio</th>
											<th>Marca</th>
											<th>Modelo</th>
											<th>Número de Serie</th>
											<th>Altura</th>
											<th>Azimuth</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="fila in ctrl.filas_pendientes">
											<td><input type="checkbox" ng-model="fila.check" ng-change="ctrl.antenaSeleccionada()" class="check_pendiente" value="{{fila.id}}"></td>
											<td>{{fila.sitio}}</td>
											<td>{{fila.tipo_antena_mw.marca}}</td>
											<td>{{fila.tipo_antena_mw.modelo}}</td>
											<td>{{fila.numero_serie}}</td>
											<td>{{fila.altura}} m</td>
											<td>
												<span ng-if="fila.tipo_antena_gul.marca != 'TELNET'">{{fila.azimuth}}°</span>
												<span ng-if="fila.tipo_antena_gul.marca == 'TELNET'">
													<span>{{fila.azimuth_telnet}}
													</span>
												</span>
											</td>											
										</tr>
									</tbody>
								</table>
							</div>

							<div class="panel-footer text-center">
								<button class="btn btn-success" ng-disabled="!ctrl.disabledFilasPendiente()" ng-click="ctrl.aprobandoFilasPendiente(ctrl.filas_pendientes)">Aprobar Selección</button>
								<button class="btn btn-danger" ng-disabled="!ctrl.disabledFilasPendiente()" ng-click="ctrl.ReprobandoFilasPendiente(ctrl.filas_pendientes)">Rechazar seleccìón</button>
							</div>

						</div>
						<div id="reservado" class="tab-pane fade">
							<div style="max-height:300px;overflow-y:auto;">
							<table class="table table-condensed table-responsive table-hover small-font">
								<thead>
									<tr>
										<th><input type="checkbox" ng-click="ctrl.selectAllReservado()" ng-model="ctrl.isAllSelectedReservado"></th>
									    <th>Estado</th>
										<th>Sitio</th>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Número de Serie</th>
										<th>Altura</th>
										<th>Azimuth</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="fila in ctrl.filas_reservado">
										<td><input type="checkbox" class="check_reservado" ng-model="fila.check" value="{{fila.id}}   ng-change="ctrl.antenaSeleccionadaReservado()"></td>
										<td><span ng-if="fila.rechazado != '0'" class="label label-danger">Rechazado</span>
									<span ng-if="fila.rechazado == '0'" class="label label-success">Validado</span>
										</td>										
										<td>{{fila.sitio}}</td>
										<td>{{fila.tipo_antena_mw.marca}}</td>
										<td>{{fila.tipo_antena_mw.modelo}}</td>
										<td>{{fila.numero_serie}}</td>
										<td>{{fila.altura}} m</td>
										<td>
											<span ng-if="fila.tipo_antena_mw.marca != 'TELNET'">{{fila.azimuth}}°</span>
											<span ng-if="fila.tipo_antena_mw.marca == 'TELNET'">
													<span>{{fila.azimuth_telnet}}</span>
											</span>
										</td>
									
									</tr>
								</tbody>
							</table>
							</div>

							<div class="panel-footer text-center">
								<button class="btn btn-success" ng-disabled="!ctrl.disabledFilasReservado()" ng-click="ctrl.aprobandoFilasReservado(ctrl.filas_reservado)">Aprobar Selección</button>
								
								<button class="btn btn-danger" ng-disabled="!ctrl.disabledFilasReservado()" ng-click="ctrl.ReprobandoFilasReservado(ctrl.filas_reservado)">Rechazar selección</button>
							</div>



						</div>


						
<div id="instalado_sitio" class="tab-pane fade">
							<div style="max-height:300px;overflow-y:auto;">
							<table class="table table-condensed table-responsive table-hover small-font">
								<thead>
									<tr>
										<th>Estado</th>									
										<th>Sitio</th>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Número de Serie</th>
										<th>Altura</th>
										<th>Azimuth</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="fila in ctrl.filas_instalado">
										<td>
									<span ng-if="fila.rechazado != '0'" class="label label-danger">Rechazado</span>
									<span ng-if="fila.rechazado == '0'" class="label label-success">Validado</span>
										</td>
										<td>{{fila.sitio}}</td>
										<td>{{fila.tipo_antena_mw.marca}}</td>
										<td>{{fila.tipo_antena_mw.modelo}}</td>
										<td>{{fila.numero_serie}}</td>										
										<td>{{fila.altura}} m</td>
										<td>
											<span ng-if="fila.tipo_antena_gul.marca != 'TELNET'">{{fila.azimuth}}°</span>
											<span ng-if="fila.tipo_antena_gul.marca == 'TELNET'">
													<span>{{fila.azimuth_telnet}}</span>
											</span>
										</td>
										
									</tr>
								</tbody>
							</table>
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>





<div id="aprobarFilasPendiente" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Aprobar Reserva  Espacio </h3>

	<br>
	<p>¿Está seguro de aprobar la reserva de espacio para las antenas seleccionadas<!-- Está seguro de aprobar reserva de espacio las antenas seleccionadas -->?</p>
	<br>

	<button class="btn btn-sm btn-success" ng-click="ctrl.aprobarFilasPendiente()" ng-hide="ctrl.procesandoFilas">Aprobar</button>
	<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelandoFilasPendiente()" ng-hide="ctrl.procesandoFilas">Cancelar</button>


	<div class="progress" ng-hide="!ctrl.procesandoFilas">
		<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{ctrl.porcentajeAvance()}}" aria-valuemin="0" aria-valuemax="100" style="width:{{ctrl.porcentajeAvance()}}%">
			{{ctrl.porcentajeAvance()}}%
		</div>
	</div>
</div>



<!-- reprobando -->

<div id="ReprobarFilasPendiente" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Rechazando Reserva  Espacio </h3>

	<br>
	<p>¿Está seguro de Rechazar reserva de espacio las antenas seleccionadas?</p>
	<br>

	<button class="btn btn-sm btn-success" ng-click="ctrl.ReprobarFilasPendiente()" ng-hide="ctrl.procesandoFilas">Rechazar</button>
	<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelandoFilasPendiente()" ng-hide="ctrl.procesandoFilas">Cancelar</button>

</div>
<!-- reprobando -->








<div id="rechazarFilas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Rechazar filas</h3>

	<br>
	<p>¿Está seguro de rechazar las filas seleccionadas?</p>
	<br>

	<button class="btn btn-sm btn-success" ng-click="ctrl.rechazarFilas()">Rechazar</button>
	<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarRechazo()">Cancelar</button>
</div>







<div id="aprobarFilasReservado" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Filas Reservadas</h3>

	<br>
	<p>¿Está seguro de aprobar la instalación?</p>
	<br>

	<button class="btn btn-sm btn-success" ng-click="ctrl.aprobarFilasReservado()" ng-hide="ctrl.procesandoFilasReservado">Instalar</button>
	<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarAprobacionReservado()" ng-hide="ctrl.procesandoFilasReservado">Cancelar</button>

	<div class="progress" ng-hide="!ctrl.procesandoFilasReservado">
		<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{ctrl.porcentajeAvanceReservado()}}" aria-valuemin="0" aria-valuemax="100" style="width:{{ctrl.porcentajeAvanceReservado()}}%">
			{{ctrl.porcentajeAvanceReservado()}}%
		</div>
	</div>
</div>




<div id="ReprobarFilasReservado" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Filas Reservadas Rechazadas</h3>

	<br>
	<p>¿Está seguro de Rechazar la instalación?</p>
	<br>

	<button class="btn btn-sm btn-success" ng-click="ctrl.ReprobarFilasReservado()" ng-hide="ctrl.procesandoFilasReservado">Rechazar</button>
	<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarAprobacionReservado()" ng-hide="ctrl.procesandoFilasReservado">Cancelar</button>

	
</div>