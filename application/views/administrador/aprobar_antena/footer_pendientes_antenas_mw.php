<script type="text/javascript">
	angular
	.module('app',[])
	.controller('controlador', ["$http",function($http){
		var ref = this;
		ref.formLogIn = {};
		ref.formLogIn.usuario = "";
		ref.formLogIn.contrasena = "";
		ref.reestablecerContrasena = {};

		ref.error = {};
		ref.error.error = false;
		ref.error.logIn = "";

		ref.session = <?php echo json_encode($session);?>;
		ref.auth = {};
		ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
		ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
		ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
		ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
		ref.auth.evaluador = (ref.session.evaluador == 1)? '' : 'disabled';

		ref.elemento = {};
		ref.filas_pendientes = [];
		ref.filas_reservado = [];

		// ref.filas_reservado = [];		
  //       ref.filas_instalado = [];

  ref.filas_validadas = [];
  ref.filasAprobadas = [];
  ref.filasRechazadas = [];


  ref.isAllSelected = false;
  ref.isAllSelectedReservado = false;

  listarElemento = function(){
  	$http({
  		url:"<?php echo base_url("solicitud_instalacion_antenas/listar_elemento_mw");?>/"+"<?php echo $elemento; ?>",
  		method:"GET"
  	}).success(function(data){



  		if(Object.keys(data).length)
  		{
  			//ref.elemento = data;
  			ref.elemento = data[0];
  			console.log(ref.elemento);
  			var t = ref.elemento.created_at? ref.elemento.created_at.split(/[- :]/) : "";
  			try{ ref.elemento.created_at = new Date(Number(t[0]), Number(t[1] - 1), Number(t[2]), Number(t[3]), Number(t[4]), Number(t[5])); } catch(err){ ref.elemento.created_at = null; }

  			ref.elemento.pendiente = Number(ref.elemento.pendiente);
  			ref.elemento.validado = Number(ref.elemento.validado);
  			ref.elemento.rechazado = Number(ref.elemento.rechazado);  		  
			//estados
			if(ref.elemento.estado_aprobacion == 3)
			{
				ref.elemento.estadotexto = "Instalado";
			}else if(ref.elemento.estado_aprobacion == 2)
			{
				ref.elemento.estadotexto = "Reservado";
			}else{
				ref.elemento.estadotexto = "Pendiente";
			}
			//estados
			//estados rechazo
			if(ref.elemento.rechazo == 0)
			{
			//ref.elemento.rechazotexto = "Validado";
		}else
		{
			ref.elemento.rechazotexto = '<span class="red">Rechazado</span>';
		}
			//estados rechazo
            //


            ref.elemento.avance = function(){
            	return Math.round(((ref.elemento.validado + ref.elemento.rechazado) * 100) / (ref.elemento.pendiente + ref.elemento.validado + ref.elemento.rechazado));
            }
            if(ref.elemento.pendiente == 0 && ref.elemento.estado.id != 4)
            {
            	$http({
            		url:"<?php echo base_url("solicitud_instalacion_antenas/solicitudes/solicitud");?>/"+ref.elemento.id,
            		method:"PUT",
            		headers:{'Content-Type': 'application/x-www-form-urlencoded'}
            	}).success(function(data){
            		if(!data.error)
            		{
            			listarElemento();
            		}
            		else
            		{

            		}
            	}).error(function(err){

            	});
            }
        }
        else
        {
        	ref.elemento = {};
        }
    }).error(function(err){
    	/*console.log(err);*/
    });
}




listarFilasPendientes = function(){
	$http({
		url:"<?php echo base_url("solicitud_instalacion_antenas/filas_pendientes_mw/listar");?>/"+"<?php echo $elemento; ?>",
		method:"GET"
	}).success(function(data){
		if(data.length)
		{
			ref.filas_pendientes = data;
			ref.filas_pendientes.forEach(function(dato, ind, objeto){
				objeto[ind].check = false;
			});
		}else{
			ref.filas_pendientes = [];
		}
	}).error(function(err){
						//console.log(err);
					});
}

listarFilasReservado = function(){
	$http({
		url:"<?php echo base_url("solicitud_instalacion_antenas/filas_reservado_mw/listar");?>/"+"<?php echo $elemento; ?>",
		method:"GET"
	}).success(function(data){
		if(data.length)
		{
			ref.filas_reservado = data;
   // ref.filas_reservado.forEach(function(dato, ind, objeto){
   // 	objeto[ind].check = false;
   // });
}
else
{
	ref.filas_reservado = [];
}
}).error(function(err){
					//console.log(err);
				});
}


listarFilaInstalado = function(){
	$http({
		url:"<?php echo base_url("solicitud_instalacion_antenas/filas_instalado_mw/listar");?>/"+"<?php echo $elemento; ?>",
		method:"GET"
	}).success(function(data){
		if(data.length)
		{
			ref.filas_instalado = data;
			// ref.filas_instalado.forEach(function(dato, ind, objeto){
   //         	objeto[ind].check = false;
   //         });
        }else
        {
       	ref.filas_instalado = [];
        }
   }).error(function(err){
					//console.log(err);
				});
}





ref.aprobandoFilas = function(info){
	info.forEach(function(dato){
		if(dato.check)
		{
			ref.filasAprobadas.push(dato);
		}
	});

	$("#aprobarFilas").bPopup();
}





// Pendiente
ref.disabledFilasPendiente = function(){
	var dis = false;
	
	ref.filas_pendientes.forEach(function(t){
		dis = dis || t.check;
	});
	
	return dis;
}

ref.selectAllPendiente = function(){
	ref.filas_pendientes.forEach(function(antena){
		antena.check = ref.isAllSelected;
	});
}

ref.aprobandoFilasPendiente = function(info){
	info.forEach(function(dato){
		if(dato.check)
		{
			ref.filasAprobadas.push(dato);
		}
	});

	$("#aprobarFilasPendiente").bPopup();
}


//reprobando
ref.ReprobandoFilasPendiente = function(info){
	info.forEach(function(dato){
		if(dato.check)
		{
			ref.filasAprobadas.push(dato);
		}
	});

	$("#ReprobarFilasPendiente").bPopup();
}

ref.ReprobarFilasPendiente = function(){
	var ids = [];
	$("input:checkbox:checked.check_pendiente").each(function() {
		ids.push($(this).val());
	            // console.log(arrids);
	        });

              //var data = "arr_ids="+'6';

              var data = "ids="+ids;

              $http({ 
              	url:"<?php echo base_url("solicitud_instalacion_antenas/update_filas_pendiente_reservado_rechazo_mw");?>",
              	method:"POST",
              	data:data,
              	headers:{'Content-Type': 'application/x-www-form-urlencoded'}
              }).success(function(data){
              	listarElemento();
              	listarFilasPendientes();
              	listarFilasReservado();
              	listarFilaInstalado();
              	$("#ReprobarFilasPendiente").bPopup().close();


              	if(data.length)
              	{
              		ref.filas_reservado = data;
						//console.log(data);
						// ref.filas_reservado.forEach(function(dato, ind, objeto){
						// 	objeto[ind].check = false;
						// });
					}
					else
					{
						ref.filas_reservado = [];
					}
				}).error(function(err){
					//console.log(err);
				});
			}
//----------


//Reservado Rechazo
ref.ReprobandoFilasReservado = function(info){
	info.forEach(function(dato){
		if(dato.check)
		{
			ref.filasAprobadas.push(dato);
		}
	});

	$("#ReprobarFilasReservado").bPopup();
}

ref.ReprobarFilasReservado = function(){
	var ids = [];
	$("input:checkbox:checked.check_reservado").each(function() {
		ids.push($(this).val());
	            // console.log(arrids);
	        });
	var data = "ids="+ids;
	$http({ 
		url:"<?php echo base_url("solicitud_instalacion_antenas/update_filas_reservado_instalado_rechazo_mw");?>",
		method:"POST",
		data:data,
		headers:{'Content-Type': 'application/x-www-form-urlencoded'}
	}).success(function(data){

		listarElemento();
		listarFilasPendientes();
		listarFilasReservado();
		listarFilaInstalado();
		$("#ReprobarFilasReservado").bPopup().close();

		if(data.length)
		{
			ref.filas_reservado = data;
						//console.log(data);
						ref.filas_reservado.forEach(function(dato, ind, objeto){
							objeto[ind].check = false;
						});
					}
					else
					{
						ref.filas_reservado = [];
					}
				}).error(function(err){
					//console.log(err);
				});


			}
//Reservado Rechazo



ref.cancelandoFilasPendiente = function(info){
	$("#aprobarFilasPendiente").bPopup().close();
	$("#ReprobarFilasPendiente").bPopup().close();
}

ref.cancelarAprobacionReservado = function(info){
	$("#aprobarFilasReservado").bPopup().close();
	$("#ReprobarFilasReservado").bPopup().close();
}

ref.aprobarFilasPendiente = function(){
	var ids = [];
	$("input:checkbox:checked.check_pendiente").each(function() {
		ids.push($(this).val());
	            // console.log(arrids);
	        });
              //var data = "arr_ids="+'6';
              var data = "ids="+ids;
              $http({ 
              	url:"<?php echo base_url("solicitud_instalacion_antenas/update_filas_pendiente_reservado_mw");?>",
              	method:"POST",
              	data:data,
              	headers:{'Content-Type': 'application/x-www-form-urlencoded'}
              }).success(function(data){
              	listarElemento();
              	listarFilasPendientes();
              	listarFilasReservado();
              	listarFilaInstalado();
              	$("#aprobarFilasPendiente").bPopup().close();

              	if(data.length)
              	{
              		ref.filas_reservado = data;
						//console.log(data);
						// ref.filas_reservado.forEach(function(dato, ind, objeto){
						// 	objeto[ind].check = false;
						// });
					}
					else
					{
						ref.filas_reservado = [];
					}
				}).error(function(err){
					//console.log(err);
				});



			}
// Pendiente
// Reservado
ref.disabledFilasReservado = function(){
	var dis = false;
	
	ref.filas_reservado.forEach(function(t){
		dis = dis || t.check;
	});
	
	return dis;
}

// ref.rechazandoFilasReservado =function(){
// 	ref.filas_reservado.forEach(function(t){
// 		 t.check;
// 	});
// }


ref.selectAllReservado = function(){
	ref.filas_reservado.forEach(function(antena2){
		antena2.check = ref.isAllSelectedReservado;
	});
}
ref.aprobandoFilasReservado = function(info){
	info.forEach(function(dato){
		if(dato.check)
		{
			ref.filasAprobadas.push(dato);
		}
	});

	$("#aprobarFilasReservado").bPopup();
}

ref.aprobarFilasReservado = function(){
	var ids = [];
	$("input:checkbox:checked.check_reservado").each(function() {
		ids.push($(this).val());
	            // console.log(arrids);
	        });
	var data = "ids="+ids;
	$http({ 
		url:"<?php echo base_url("solicitud_instalacion_antenas/update_filas_reservado_instalado_mw");?>",
		method:"POST",
		data:data,
		headers:{'Content-Type': 'application/x-www-form-urlencoded'}
	}).success(function(data){
		//console.log(data+"testdata");

		listarElemento();
		listarFilasPendientes();
		listarFilasReservado();
		listarFilaInstalado();
		$("#aprobarFilasReservado").bPopup().close();

		if(data.length)
		{
		    //alert("test1");
		    ref.filas_reservado = data;						
		    // ref.filas_reservado.forEach(function(dato, ind, objeto){
		    // 	objeto[ind].check = false;
		    // });
		}else{
			ref.filas_reservado = [];
						//alert("test2");
					}
				}).error(function(err){					


				});


			}
// Reservado
//---instalado nuevo



ref.disabledFilasInstalado = function(){
	var dis = false;
	
	ref.filas_Instalado.forEach(function(t){
		dis = dis || t.check;
	});
	
	return dis;
}

ref.selectAllInstalado = function(){
	ref.filas_Instalado.forEach(function(antena){
		antena.check = ref.isAllSelected;
	});
}

ref.aprobandoFilasInstalado = function(info){
	info.forEach(function(dato){
		if(dato.check)
		{
			ref.filasAprobadas.push(dato);
		}
	});

	$("#aprobarFilasInstalado").bPopup();
}

ref.cancelandoFilasInstalado = function(info){
	$("#aprobarFilasInstalado").bPopup().close();
}


ref.aprobarFilasInstalado = function(){
	var ids = [];
	$("input:checkbox:checked.check_instalado").each(function() {
		ids.push($(this).val());
	            // console.log(arrids);
	        });

              //var data = "arr_ids="+'6';

              var data = "ids="+ids;

              $http({ 
              	url:"<?php echo base_url("solicitud_instalacion_antenas/update_filas_pendiente_instalado_mw");?>",
              	method:"POST",
              	data:data,
              	headers:{'Content-Type': 'application/x-www-form-urlencoded'}
              }).success(function(data){
              	listarElemento();
              	listarFilasPendientes();
              	listarFilasReservado();
              	listarFilaInstalado();
              	
              	$("#aprobarFilasInstalado").bPopup().close();

              	if(data.length)
              	{
              		ref.filas_instalatado = data;
						//console.log(data);
						ref.filas_instalatado.forEach(function(dato, ind, objeto){
							objeto[ind].check = false;
						});
					}
					else
					{
						ref.filas_instalado = [];
					}
				}).error(function(err){
					//console.log(err);
				});

			}
//instalado

ref.cancelarAprobacion = function(){
	ref.filasAprobadas = [];
	$("#aprobarFilas").bPopup().close();
	ref.procesandoFilas = false;
}




ref.rechazandoFilas = function(info){
	info.forEach(function(dato){
		if(dato.check)
		{
			ref.filasRechazadas.push(dato);
		}
	});

	$("#rechazarFilas").bPopup();
}

// ref.rechazarFilas = function(){
// 	var t = 0;
// 	for(var i = 0; i < ref.filasRechazadas.length; i++)
// 	{
// 		var antena = ref.filasRechazadas[i];

// 		$http({
// 			url:"<?php //echo base_url("solicitud_instalacion_antenas/antenas_gul/rechazar");?>/"+antena.id,
// 			method:"PUT",
// 			headers:{'Content-Type': 'application/x-www-form-urlencoded'}
// 		}).success(function(data){
// 			if(!data.error)
// 			{
// 				t++;

// 				if(t == ref.filasRechazadas.length)
// 				{
// 					ref.filasRechazadas = [];
// 					ref.filas_pendientes = [];
// 					ref.filas_validadas = [];
// 					listarElemento();
// 					listarFilasPendientes();
// 					listarFilasValidadas();
// 					ref.isAllSelected = false;
// 					$("#rechazarFilas").bPopup().close();
// 				}
// 			}
// 			else
// 			{

// 			}
// 		}).error(function(err){
			
// 		});
// 	}
// }

ref.cancelarRechazo = function(){
	ref.filasRechazadas = [];
	$("#rechazarFilas").bPopup().close();
}

ref.selectAll = function(){
	ref.filas_pendientes.forEach(function(antena){
		antena.check = ref.isAllSelected;
	});
}

ref.antenaSeleccionada = function(){
	ref.isAllSelected = ref.filas_pendientes.every(function(antena){return antena.check;});
}





ref.disabledFilas = function(){
	var dis = false;
	
	ref.filas_pendientes.forEach(function(t){
		dis = dis || t.check;
	});
	
	return dis;
}

ref.iniciar_sesion = function(){
	var data = "usuario="+ref.formLogIn.usuario
	+"&contrasena="+ref.formLogIn.contrasena;

	$http({
		url:"<?php echo base_url("sesion/iniciar");?>",
		method:"POST",
		data:data,
		headers:{'Content-Type': 'application/x-www-form-urlencoded'}
	}).success(function(data){
		if(data.error)
		{
			ref.error.error = true;
			ref.error.logIn = data.error;
		}
		else
		{
			if(data.logged)
			{
				location.reload();
			}
		}
	}).error(function(err){
		/*console.log(err);*/
	});
};

ref.cerrar_sesion = function(){
	$http({
		url:"<?php echo base_url("sesion/cerrar");?>",
		method:"GET"
	}).success(function(data){
		if(!data.logged)
		{
			window.location = "<?php echo base_url();?>";
		}
	}).error(function(err){
		/*console.log(err);*/
	});
};

ref.olvide_contrasena = function(){
	$("#reestablecerContrasena").bPopup({
		amsl: 0,
		onClose: function(){
			ref.reestablecerContrasena = {};
			ref.success = {};
			ref.success.forgot = "";
		},
		onOpen: function(){
			ref.reestablecerContrasena = {};
			ref.success = {};
			ref.success.forgot = "";
			var alertForgot = document.getElementById("alert-forgot");
			alertForgot.innerHTML = "";
			ref.error = {};
		}
	});
};

ref.reestablecer_contrasena = function(){
	ref.procesando = true;
	var alertForgot = document.getElementById("alert-forgot");
	alertForgot.innerHTML = "";
	ref.error = {};

	var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

	$http({
		url:"<?php echo base_url("recuperar_contrasena");?>",
		method:"POST",
		data:data,
		headers:{'Content-Type': 'application/x-www-form-urlencoded'}
	}).success(function(resp){
		if(resp.error)
		{
			ref.error.error = true;
			ref.error.forgot = resp.error;
			alertForgot.innerHTML = ref.error.forgot;
			ref.procesando = false;
		}
		else
		{
			ref.reestablecerContrasena = {};
			setTimeout(ref.cancelar_contrasena,"3000");
			ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
			alertForgot.innerHTML = "";
			ref.error = {};
			ref.procesando = false;
		}
	}).error(function(err){
		/*console.log(err);*/
		ref.procesando = false;
	});
};

ref.cancelar_contrasena = function(){
	$("#reestablecerContrasena").bPopup().close();
	ref.reestablecerContrasena = {};
};

listarElemento();
listarFilasPendientes();
listarFilasReservado();
listarFilaInstalado();

}]);
</script>
</body>
</html>