<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h3>Solicitudes de Ingreso Masivo</h3>
			</div>
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url("administrar"); ?>">Administrar</a></li>
				<li class="active">Ingreso Masivo</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Solicitudes
				</div>
				<div class="panel-body"  style="overflow-y:auto;">
					<table class="table table-condensed table-hover table-responsive">
						<thead>
							<tr>
								<th><i class="fa fa-cogs"></i></th>
								<th>Fecha y hora</th>
								<th>Usuario solicitante</th>
								<th>Módulo</th>
								<th>Estado</th>
								<th style="width:12%">Avance</th>
								<th>Fecha de Validación</th>
								<th>Validado por</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="solicitud in ctrl.solicitudes">
								<td>
									<a href="<?php echo base_url("administrar/ingreso_masivo/elemento/{{solicitud.id}}"); ?>" class="btn btn-primary btn-xs">Ver</a>
								</td>
								<td>{{solicitud.created_at | date:"dd/MM/yyyy - HH:mm:ss"}}</td>
								<td>{{solicitud.usuario.nombres}} {{solicitud.usuario.apellido_paterno}}</td>
								<td>{{solicitud.tabla.modulo}}</td>
								<td>
									<span ng-class="{'label label-warning': solicitud.estado.id == 1, 'label label-success': solicitud.estado.id == 4}">{{solicitud.estado.estado}}</span>
								</td>
								<td>
									<div class="progress">
										<div ng-class="{'progress-bar progress-bar-success': solicitud.avance() >= 80, 'progress-bar progress-bar-warning': solicitud.avance() >= 40 && solicitud.avance() < 80, 'progress-bar progress-bar-danger': solicitud.avance() < 40}" role="progressbar" aria-valuenow="{{solicitud.avance()}}" aria-valuemin="0" aria-valuemax="100" style="width:{{solicitud.avance()}}%">
											{{solicitud.avance()}}%
										</div>
									</div>
								</td>
								<td>{{solicitud.fecha_fin}}</td>
								<td>{{solicitud.validador}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>