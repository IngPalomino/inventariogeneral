<br>
<div class="container">
	<button class="btn btn-default btn-sm" onclick="$('#crearDependencia').bPopup();"><span class="fa fa-plus"></span> Crear dependencia</button>
</div>
<div class="container">
	<table class="table table-condensed table-striped">
		<thead>
			<tr>
				<th>Tabla primaria</th>
				<th>Objeto primario</th>
				<th>Tabla secundaria</th>
				<th>Objeto secundario</th>
				<th>Característica</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="dependencia in ctrl.dependencias">
				<td>{{dependencia.tabla_primaria}}</td>
				<td>{{dependencia.objeto_primario}}</td>
				<td>{{dependencia.tabla_secundaria}}</td>
				<td>{{dependencia.objeto_secundario}}</td>
				<td>{{dependencia.caracteristicas}}</td>
			</tr>
		</tbody>
	</table>
</div>

<div id="crearDependencia" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Crear dependencia</h3>

	<form>
		<table class="table table-condensed">
			<tbody>
				<tr>
					<td class="info">Tabla primaria</td>
					<td class="active">
						<select class="form-control" ng-change="ctrl.seleccionPrimaria()" ng-model="ctrl.dependenciaNueva.tabla_primaria">
							<option value="0">Elija una tabla</option>
							<option ng-repeat="tabla in ctrl.db_tablasPrimarias" value="{{tabla.tabla}}">{{tabla.tabla}}</option>
						</select>
					</td>
					<td class="info">Tabla secundaria</td>
					<td class="active">
						<select class="form-control" ng-change="ctrl.seleccionSecundaria()" ng-model="ctrl.dependenciaNueva.tabla_secundaria">
							<option value="0">Elija una tabla</option>
							<option ng-repeat="tabla in ctrl.db_tablasSecundarias" value="{{tabla.tabla}}">{{tabla.tabla}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">Objeto primario</td>
					<td class="active">
						<select class="form-control" ng-model="ctrl.dependenciaNueva.objetos_primario">
							<option value="0">Elija un objeto</option>
							<option ng-repeat="objeto in ctrl.colsTablaPrimaria" value="{{objeto.Field}}">{{objeto.Field}}</option>
						</select>
					</td>
					<td class="info">Objeto secundario</td>
					<td class="active">
						<select class="form-control"  ng-model="ctrl.dependenciaNueva.objetos_secundario">
							<option value="0">Elija un objeto</option>
							<option ng-repeat="objeto in ctrl.colsTablaSecundaria" value="{{objeto.Field}}">{{objeto.Field}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="info">
						Opciones
					</td>
					<td class="active">
						<select class="form-control" ng-model="ctrl.dependenciaNueva.operador_primario">
							<option value="*">*</option>
							<option value="==">==</option>
							<option value="!=">!=</option>
						</select>
						<select class="form-control" ng-hide="ctrl.dependenciaNueva.operador_primario == '*'">
							<option></option>
						</select>
					</td>
					<td class="info">
						Opciones
					</td>
					<td class="active">
						<select class="form-control" ng-model="ctrl.dependenciaNueva.operador_secundario">
							<option value="*">*</option>
							<option value="==">==</option>
							<option value="!=">!=</option>
						</select>
						<select class="form-control" ng-hide="ctrl.dependenciaNueva.operador_secundario == '*'">
							<option></option>
						</select>
						<label>Máximo: <input type="text" class="form-control" ng-model="ctrl.dependenciaNueva.maximo"></label>
					</td>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success" ng-click="ctrl.crear()">Crear</button>
		<button class="btn btn-danger" ng-click="ctrl.cancelarCreacion()">Cancelar</button>
	</form>
</div>