<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h3><?php echo $title; ?></h3>
			</div>
			<ol class="breadcrumb">
				<li><a href="<?php echo site_url(); ?>">Inicio</a></li>
				<li><a href="<?php echo site_url('administrar'); ?>">Administrar</a></li>
				<li class="active">Usuarios</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="btn-group">
				<button class="btn btn-sm btn-default" ng-click="ctrl.nuevoUsuario()"><i class="fa fa-user-plus"></i> Crear Usuario</button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped table-responsive">
				<thead>
					<tr>
						<th>ID</th>
						<th>Usuario</th>
						<th>Nombres</th>
						<th>Apellido Paterno</th>
						<th>Grupos</th>
						<th>Teléfono</th>
						<th>Fecha de Creación</th>
						<th>Estado</th>
					</tr>
					<tr>
						<th>Filtros</th>
						<th><input class="form-control input-sm" type="text" ng-model="ctrl.search.usuario"></th>
						<th><input class="form-control input-sm" type="text" ng-model="ctrl.search.nombres"></th>
						<th><input class="form-control input-sm" type="text" ng-model="ctrl.search.apellido"></th>
						<th><input class="form-control input-sm" type="text" ng-model="ctrl.search.grupo"></th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="usuario in ctrl.usuarios | filter: ctrl.filtroUsuarios">
						<td>{{usuario.id_usuario}}</td>
						<td><button class="btn btn-primary btn-xs" ng-click="ctrl.editandoUsuario(usuario)">{{usuario.correo}}</button></td>
						<td>{{usuario.nombres}}</td>
						<td>{{usuario.apellido_paterno}}</td>
						<td>
							<span ng-repeat="grupo in usuario.usuarios_grupos" ng-if="usuario.usuarios_grupos.length < 3">
								<br ng-if="$index > 0">
								{{grupo.nombre}}
							</span>
							<span ng-if="usuario.usuarios_grupos.length > 2">
								{{usuario.usuarios_grupos.length}} Grupos
							</span>
						</td>
						<td><span class="label label-default">{{usuario.telefono}}</span></td>
						<td>{{usuario.created_at | date: 'dd/MM/yyyy - HH:mm:ss'}}</td>
						<td>
							<span class="label label-danger" ng-if="!usuario.activo">INACTIVO</span>
							<span class="label label-success" ng-if="usuario.activo">ACTIVO</span>
						</td>
					</tr>
				</tbody>
			</table>
			<div ng-loading="ctrl.usuarios"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="editarUsuario" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="buttn" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Editar Usuario</h4>
			</div>
			<form ng-submit="ctrl.grabarUsuario()">
				<div class="modal-body form-horizontal form-app">
					<div class="form-group">
						<label class="col-md-4">Estado</label>
						<div class="col-md-8">
							<label class="switch">
								<input type="checkbox" ng-model="ctrl.editarUsuarioForm_e.activo" ng-model-options="{updateOn: submit}">
								<div class="slider round"></div>
							</label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Correo Electrónico</label>
						<div class="col-md-8">
							<input class="form-control" type="text" ng-model="ctrl.editarUsuarioForm_e.correo">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Nombres</label>
						<div class="col-md-8">
							<input class="form-control" type="text" ng-model="ctrl.editarUsuarioForm_e.nombres">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Apellido Paterno</label>
						<div class="col-md-8">
							<input class="form-control" type="text" ng-model="ctrl.editarUsuarioForm_e.apellido_paterno">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Apellido Materno</label>
						<div class="col-md-8">
							<input class="form-control" type="text" ng-model="ctrl.editarUsuarioForm_e.apellido_materno">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Contraseña</label>
						<div class="col-md-8">
							<div class="input-group">
								<input class="form-control" type="password" placeholder="Dejar en blanco si no desea cambiar la contraseña." ng-model="ctrl.editarUsuarioForm_e.contrasena">
								<div class="input-group-btn">
									<button class="btn btn-default" type="button" ng-click="ctrl.usarRandom(ctrl.editarUsuarioForm_e)">Random</button>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Confirmar Contraseña</label>
						<div class="col-md-8">
							<input class="form-control" type="password" placeholder="Dejar en blanco si no desea cambiar la contraseña." ng-model="ctrl.editarUsuarioForm_e.confirmar_contrasena">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Teléfono</label>
						<div class="col-md-8">
							<input class="form-control" type="text" ng-model="ctrl.editarUsuarioForm_e.telefono">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Grupos de usuario</label>
						<div class="col-md-8">
							<selectize config="ctrl.config" options="ctrl.grupos" ng-model="ctrl.editarUsuarioForm_e.grupos"></selectize>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="alert alert-danger" ng-show="ctrl.editarUsuarioMessages.error">{{ctrl.editarUsuarioMessages.error}}</div>
							<div class="alert alert-success" ng-show="ctrl.editarUsuarioMessages.success">{{ctrl.editarUsuarioMessages.success}}</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" data-dismiss="modal" type="button">Cancelar</button>
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="nuevoUsuario" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="buttn" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Crear Usuario</h4>
			</div>
			<form ng-submit="ctrl.crearUsuario()">
				<div class="modal-body form-horizontal form-app">
					<div class="form-group">
						<label class="col-md-4">Correo Electrónico</label>
						<div class="col-md-8">
							<input class="form-control" type="text" ng-model="ctrl.nuevoUsuarioForm.correo">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Nombres</label>
						<div class="col-md-8">
							<input class="form-control" type="text" ng-model="ctrl.nuevoUsuarioForm.nombres">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Apellido Paterno</label>
						<div class="col-md-8">
							<input class="form-control" type="text" ng-model="ctrl.nuevoUsuarioForm.apellido_paterno">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Apellido Materno</label>
						<div class="col-md-8">
							<input class="form-control" type="text" ng-model="ctrl.nuevoUsuarioForm.apellido_materno">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Contraseña</label>
						<div class="col-md-8">
							<div class="input-group">
								<input class="form-control" type="password" placeholder="Contraseña" ng-model="ctrl.nuevoUsuarioForm.contrasena">
								<div class="input-group-btn">
									<button class="btn btn-default" type="button" ng-click="ctrl.usarRandom(ctrl.nuevoUsuarioForm)">Random</button>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Confirmar Contraseña</label>
						<div class="col-md-8">
							<input class="form-control" type="password" placeholder="Confirmar contraseña." ng-model="ctrl.nuevoUsuarioForm.confirmar_contrasena">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Teléfono</label>
						<div class="col-md-8">
							<input class="form-control" type="text" ng-model="ctrl.nuevoUsuarioForm.telefono">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Grupos de usuario</label>
						<div class="col-md-8">
							<selectize config="ctrl.config" options="ctrl.grupos" ng-model="ctrl.nuevoUsuarioForm.grupos"></selectize>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="alert alert-danger" ng-show="ctrl.crearUsuarioMessages.error">{{ctrl.crearUsuarioMessages.error}}</div>
							<div class="alert alert-success" ng-show="ctrl.crearUsuarioMessages.success">{{ctrl.crearUsuarioMessages.success}}</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
					<button class="btn btn-primary" type="submit" ng-disabled="ctrl.disabledCrear()">Crear</button>
				</div>
			</form>
		</div>
	</div>
</div>