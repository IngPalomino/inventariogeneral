<!--<p>
	<?php print_r($session);?>
</p>-->

<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.usuarios = {};
			ref.usuarioEditar = {};
			ref.usuarioNuevo = {};
			ref.usuarioEliminar = {};

			listar = function(){
				$http({
					url:"<?php echo base_url("index.php/usuarios/listar");?>",
					method:"GET"
				}).success(function(data){
					var ind = 0;
					data.forEach(function(usuario){
						if(usuario.id_usuario == ref.session.id_usuario)
						{
							data.splice(ind,1);
						}
						else if(usuario.autorizacion > ref.session.auth)
						{
							data.splice(ind,1);
						}
						else
						{
							ind++;
						}
					});

					ref.usuarios = data;
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.editar = function(usuario){
				$("#editarUsuario").bPopup();

				$http({
					url:"<?php echo base_url("index.php/usuarios/usuario");?>"+"/"+usuario.id_usuario,
					method:"GET"
				}).success(function(data){
					if(data)
					{
						ref.usuarioEditar = data[0];
					}
				}).error(function(err){

				});
			}

			ref.grabar = function(){
				$("#editarUsuario").bPopup().close();

				var data = "nombres="+ref.usuarioEditar.nombres
							+"&apellidos="+ref.usuarioEditar.apellidos
							+"&autorizacion="+ref.usuarioEditar.autorizacion;

				$http({
					url:"<?php echo base_url("index.php/usuarios/usuario");?>"+"/"+ref.usuarioEditar.id_usuario,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					listar();
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			ref.crear = function(){
				var data = "nombres="+ref.usuarioNuevo.nombres
							+"&apellidos="+ref.usuarioNuevo.apellidos
							+"&usuario="+ref.usuarioNuevo.usuario
							+"&autorizacion="+ref.usuarioNuevo.autorizacion;

				$http({
					url:"<?php echo base_url("index.php/usuarios/usuario");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					listar();
					ref.usuarioNuevo = {};
					$("#crearUsuario").bPopup().close();
				}).error(function(err){

				});
			}

			ref.cancelarCreacion = function(){
				ref.usuarioNuevo = {};
				$("#crearUsuario").bPopup().close();
			}

			ref.eliminando = function(usuario){
				ref.usuarioEliminar = usuario;
				$("#eliminarUsuario").bPopup();
			}

			ref.eliminar = function(){
				$http({
					url:"<?php echo base_url("index.php/usuarios/usuario");?>"+"/"+ref.usuarioEliminar.id_usuario,
					method:"DELETE"
				}).success(function(data){
					listar();
					ref.usuarioEliminar = {};
					$("#eliminarUsuario").bPopup().close();
				}).error(function(err){

				});
			}

			ref.cancelarEliminacion = function(){
				ref.usuarioEliminar = {};
				$("#eliminarUsuario").bPopup().close();
			}

			ref.restablecerContrasena = function(usuario){
				ref.usuarioEditar = usuario;
				$("#reestablecerContrasena").bPopup();
			}

			ref.reestablecer = function(){
				$http({
					url:"<?php echo base_url("index.php/usuarios/reestablecer");?>"+"/"+ref.usuarioEditar.id_usuario,
					method:"PUT"
				}).success(function(data){
					ref.usuarioEditar = {};
					$("#reestablecerContrasena").bPopup().close();
				}).error(function(err){

				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			listar();
		}]);
</script>
</body>
</html>