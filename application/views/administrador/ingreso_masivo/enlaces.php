<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h4>{{ctrl.elemento.tabla.modulo}}</h4>
			</div>
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url("administrar"); ?>">Administrar</a></li>
				<li><a href="<?php echo base_url("administrar/ingreso_masivo"); ?>">Ingreso Masivo</a></li>
				<li class="active">Elemento - {{ctrl.elemento.tabla.modulo}}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary with-nav-tabs">
				<div class="panel-heading text-center">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#solicitud" data-toggle="tab"><strong>Solicitud de {{ctrl.elemento.tabla.modulo}}</strong></a></li>
						<li><a href="#pendientes" data-toggle="tab"><strong>Filas Pendientes de validación</strong> <span class="badge">{{ctrl.filas_pendientes.length}}</span></a></li>
						<li><a href="#validadas" data-toggle="tab"><strong>Filas Validadas</strong> <span class="badge">{{ctrl.filas_validadas.length}}</span></a></li>
					</ul>
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div id="solicitud" class="tab-pane fade in active">
							<table class="table table-condensed table-responsive">
								<tbody>
									<tr>
										<td class="info text-right">Fecha de solicitud</td>
										<td class="active">{{ctrl.elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}}</td>
									</tr>
									<tr>
										<td class="info text-right">Usuario solicitante</td>
										<td class="active">{{ctrl.elemento.usuario.nombres}} {{ctrl.elemento.usuario.apellido_paterno}}</td>
									</tr>
									<tr>
										<td class="info text-right">Módulo</td>
										<td class="active">{{ctrl.elemento.tabla.modulo}}</td>
									</tr>
									<tr>
										<td class="info text-right">Estado</td>
										<td class="active">
											<span ng-class="{'label label-warning': ctrl.elemento.estado.id == 1, 'label label-success': ctrl.elemento.estado.id == 4}">{{ctrl.elemento.estado.estado}}</span>
										</td>
									</tr>
									<tr>
										<td class="info text-right">Avance</td>
										<td class="active">
											<div class="progress">
												<div ng-class="{'progress-bar progress-bar-success': ctrl.elemento.avance() >= 80, 'progress-bar progress-bar-warning': ctrl.elemento.avance() >= 40 && ctrl.elemento.avance() < 80, 'progress-bar progress-bar-danger': ctrl.elemento.avance() < 40}" role="progressbar" aria-valuenow="{{ctrl.elemento.avance()}}" aria-valuemin="0" aria-valuemax="100" style="width:{{ctrl.elemento.avance()}}%">
													{{ctrl.elemento.avance()}}%
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td class="info text-right">Fecha de Finalización</td>
										<td class="active">
											<span ng-if="!ctrl.elemento.fecha_fin">Sin Finalizar</span>
											<span ng-if="ctrl.elemento.fecha_fin">{{ctrl.elemento.fecha_fin}}</span>
										</td>
									</tr>
									<!--<tr>
										<td class="info text-right">Validado por</td>
										<td class="active">{{ctrl.elemento.validador.nombres}}</td>
									</tr>-->
								</tbody>
							</table>
						</div>
						<div id="pendientes" class="tab-pane fade">
							<div style="max-height:300px;overflow-y:auto;">
								<table class="table table-condensed table-responsive table-hover small-font" style="width: 4000px;overflow-x: auto;">
									<thead>
										<tr>
											<th colspan="2"></th>
											<th class="success" colspan="3" rowspan="2">Near End</th>
											<th class="danger" colspan="3" rowspan="2">Far End</th>
											<th class="success" colspan="4" rowspan="2">Near End</th>
											<th class="danger" colspan="4" rowspan="2">Far End</th>
											<th class="success" colspan="5">Near End</th>
											<th class="danger" colspan="5">Far End</th>
											<th class="info" colspan="9" rowspan="2">Configuración del Enlace</th>
										</tr>
										<tr>
											<th colspan="16"></th>
											<th colspan="5">Antena (s)</th>
											<th colspan="5">Antena (s)</th>
										</tr>
										<tr>
											<th><input type="checkbox" ng-click="ctrl.selectAll()" ng-model="ctrl.isAllSelected"></th>
											<th>Tipo</th>
											<th>Código</th>
											<th>Nombre</th>
											<th>IDU ID</th>
											<th>Código</th>
											<th>Nombre</th>
											<th>IDU ID</th>
											<th>ODU</th>
											<th>Número de Serie</th>
											<th>Ftx</th>
											<th>Ptx</th>
											<th>ODU</th>
											<th>Número de Serie</th>
											<th>Ftx</th>
											<th>Ptx</th>
											<th>Marca</th>
											<th>Modelo</th>
											<th>Número de Serie</th>
											<th>Altura</th>
											<th>Azimuth</th>
											<th>Marca</th>
											<th>Modelo</th>
											<th>Número de Serie</th>
											<th>Altura</th>
											<th>Azimuth</th>
											<th>Polaridad</th>
											<th>Configuración</th>
											<th>Ancho de Banda/BW</th>
											<th>Modulación</th>
											<th>Capacidad</th>
											<th>Modulación Adaptativa (AM)</th>
											<th>Modulación AM</th>
											<th>Capacidad AM</th>
											<th>Banda</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="fila in ctrl.filas_pendientes" ng-class="{'active': fila.check}">
											<td><input type="checkbox" ng-model="fila.check" ng-change="ctrl.enlaceSeleccionado()"></td>
											<td>
												<span class="label label-warning" ng-if="fila.id_actualizar">UPDATE</span>
												<span class="label label-info" ng-if="!fila.id_actualizar">INSERT</span>
											</td>
											<td>{{fila.idu_1.sitio.codigo}}</td>
											<td>{{fila.idu_1.sitio.nombre_completo}}</td>
											<td>{{fila.idu_1.ne_id}}</td>
											<td>{{fila.idu_2.sitio.codigo}}</td>
											<td>{{fila.idu_2.sitio.nombre_completo}}</td>
											<td>{{fila.idu_2.ne_id}}</td>
											<td>{{fila.odus_ne[0].tipo_odu.tipo}}</td>
											<td>
												<span ng-repeat="odu in fila.odus_ne">
													<span ng-if="$index > 0">/ </span>
													{{odu.numero_serie}}
												</span>
											</td>
											<td>{{fila.odus_ne[0].ftx}} MHz</td>
											<td>{{fila.odus_ne[0].ptx}} dBm</td>
											<td>{{fila.odus_fe[0].tipo_odu.tipo}}</td>
											<td>
												<span ng-repeat="odu in fila.odus_fe">
													<span ng-if="$index > 0">/ </span>
													{{odu.numero_serie}}
												</span>
											</td>
											<td>{{fila.odus_fe[0].ftx}} MHz</td>
											<td>{{fila.odus_fe[0].ptx}} dBm</td>
											<td>
												<span ng-repeat="antena in fila.antenas_ne">
													<span ng-if="$index > 0">/ </span>
													{{antena.tipo_antena_mw.marca}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_ne">
													<span ng-if="$index > 0">/ </span>
													{{antena.tipo_antena_mw.modelo}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_ne">
													<span ng-if="$index > 0">/ </span>
													{{antena.numero_serie}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_ne">
													<span ng-if="$index > 0">/ </span>
													{{antena.altura}} m
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_ne">
													<span ng-if="$index > 0">/ </span>
													{{antena.azimuth}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_fe">
													<span ng-if="$index > 0">/ </span>
													{{antena.tipo_antena_mw.marca}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_fe">
													<span ng-if="$index > 0">/ </span>
													{{antena.tipo_antena_mw.modelo}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_fe">
													<span ng-if="$index > 0">/ </span>
													{{antena.numero_serie}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_fe">
													<span ng-if="$index > 0">/ </span>
													{{antena.altura}} m
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_fe">
													<span ng-if="$index > 0">/ </span>
													{{antena.azimuth}}
												</span>
											</td>
											<td>
												<span ng-switch on="fila.polaridad">
													<span ng-switch-when="1">V</span>
													<span ng-switch-when="2">H</span>
													<span ng-switch-when="3">V+H</span>
												</span>
											</td>
											<td>{{fila.configuracion}}</td>
											<td>{{fila.ancho_banda}} MHz</td>
											<td>{{fila.modulacion}}</td>
											<td>{{fila.capacidad}} Mbit/s</td>
											<td>
												<span ng-if="fila.modulacion_adaptativa == 1">SI</span>
												<span ng-if="fila.modulacion_adaptativa == 0">NO</span>
											</td>
											<td>{{fila.modulacion_am}}</td>
											<td>
												<span ng-if="fila.capacidad_am == 0">N/A</span>
												<span ng-if="fila.capacidad_am != 0">{{fila.capacidad_am}} Mbit/s</span>
											</td>
											<td>{{fila.banda}} GHz</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="panel-footer text-center">
								<button class="btn btn-success" ng-disabled="!ctrl.disabledFilas()" ng-click="ctrl.aprobandoFilas(ctrl.filas_pendientes)">Aprobar seleccìón</button>
								<button class="btn btn-danger" ng-disabled="!ctrl.disabledFilas()" ng-click="ctrl.rechazandoFilas(ctrl.filas_pendientes)">Rechazar seleccìón</button>
							</div>
						</div>
						<div id="validadas" class="tab-pane fade">
							<div style="max-height:300px;overflow-y:auto;">
								<table class="table table-condensed table-responsive table-hover small-font" style="width: 4000px;overflow-x: auto;">
									<thead>
										<tr>
											<th colspan="2"></th>
											<th class="success" colspan="3" rowspan="2">Near End</th>
											<th class="danger" colspan="3" rowspan="2">Far End</th>
											<th class="success" colspan="4" rowspan="2">Near End</th>
											<th class="danger" colspan="4" rowspan="2">Far End</th>
											<th class="success" colspan="5">Near End</th>
											<th class="danger" colspan="5">Far End</th>
											<th class="info" colspan="9" rowspan="2">Configuración del Enlace</th>
										</tr>
										<tr>
											<th colspan="16"></th>
											<th colspan="5">Antena (s)</th>
											<th colspan="5">Antena (s)</th>
										</tr>
										<tr>
											<th>Estado</th>
											<th>Tipo</th>
											<th>Código</th>
											<th>Nombre</th>
											<th>IDU ID</th>
											<th>Código</th>
											<th>Nombre</th>
											<th>IDU ID</th>
											<th>ODU</th>
											<th>Número de Serie</th>
											<th>Ftx</th>
											<th>Ptx</th>
											<th>ODU</th>
											<th>Número de Serie</th>
											<th>Ftx</th>
											<th>Ptx</th>
											<th>Marca</th>
											<th>Modelo</th>
											<th>Número de Serie</th>
											<th>Altura</th>
											<th>Azimuth</th>
											<th>Marca</th>
											<th>Modelo</th>
											<th>Número de Serie</th>
											<th>Altura</th>
											<th>Azimuth</th>
											<th>Polaridad</th>
											<th>Configuración</th>
											<th>Ancho de Banda/BW</th>
											<th>Modulación</th>
											<th>Capacidad</th>
											<th>Modulación Adaptativa (AM)</th>
											<th>Modulación AM</th>
											<th>Capacidad AM</th>
											<th>Banda</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="fila in ctrl.filas_validadas">
											<td>
												<span ng-class="{'label label-danger': fila.estado.id == 3, 'label label-success': fila.estado.id == 2}">{{fila.estado.estado}}</span>
											</td>
											<td>
												<span class="label label-warning" ng-if="fila.id_actualizar">UPDATE</span>
												<span class="label label-info" ng-if="!fila.id_actualizar">INSERT</span>
											</td>
											<td>{{fila.idu_1.sitio.codigo}}</td>
											<td>{{fila.idu_1.sitio.nombre_completo}}</td>
											<td>{{fila.idu_1.ne_id}}</td>
											<td>{{fila.idu_2.sitio.codigo}}</td>
											<td>{{fila.idu_2.sitio.nombre_completo}}</td>
											<td>{{fila.idu_2.ne_id}}</td>
											<td>{{fila.odus_ne[0].tipo_odu.tipo}}</td>
											<td>
												<span ng-repeat="odu in fila.odus_ne">
													<span ng-if="$index > 0">/ </span>
													{{odu.numero_serie}}
												</span>
											</td>
											<td>{{fila.odus_ne[0].ftx}} MHz</td>
											<td>{{fila.odus_ne[0].ptx}} dBm</td>
											<td>{{fila.odus_fe[0].tipo_odu.tipo}}</td>
											<td>
												<span ng-repeat="odu in fila.odus_fe">
													<span ng-if="$index > 0">/ </span>
													{{odu.numero_serie}}
												</span>
											</td>
											<td>{{fila.odus_fe[0].ftx}} MHz</td>
											<td>{{fila.odus_fe[0].ptx}} dBm</td>
											<td>
												<span ng-repeat="antena in fila.antenas_ne">
													<span ng-if="$index > 0">/ </span>
													{{antena.tipo_antena_mw.marca}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_ne">
													<span ng-if="$index > 0">/ </span>
													{{antena.tipo_antena_mw.modelo}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_ne">
													<span ng-if="$index > 0">/ </span>
													{{antena.numero_serie}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_ne">
													<span ng-if="$index > 0">/ </span>
													{{antena.altura}} m
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_ne">
													<span ng-if="$index > 0">/ </span>
													{{antena.azimuth}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_fe">
													<span ng-if="$index > 0">/ </span>
													{{antena.tipo_antena_mw.marca}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_fe">
													<span ng-if="$index > 0">/ </span>
													{{antena.tipo_antena_mw.modelo}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_fe">
													<span ng-if="$index > 0">/ </span>
													{{antena.numero_serie}}
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_fe">
													<span ng-if="$index > 0">/ </span>
													{{antena.altura}} m
												</span>
											</td>
											<td>
												<span ng-repeat="antena in fila.antenas_fe">
													<span ng-if="$index > 0">/ </span>
													{{antena.azimuth}}
												</span>
											</td>
											<td>
												<span ng-switch on="fila.polaridad">
													<span ng-switch-when="1">V</span>
													<span ng-switch-when="2">H</span>
													<span ng-switch-when="3">V+H</span>
												</span>
											</td>
											<td>{{fila.configuracion}}</td>
											<td>{{fila.ancho_banda}} MHz</td>
											<td>{{fila.modulacion}}</td>
											<td>{{fila.capacidad}} Mbit/s</td>
											<td>
												<span ng-if="fila.modulacion_adaptativa == 1">SI</span>
												<span ng-if="fila.modulacion_adaptativa == 0">NO</span>
											</td>
											<td>{{fila.modulacion_am}}</td>
											<td>
												<span ng-if="fila.capacidad_am == 0">N/A</span>
												<span ng-if="fila.capacidad_am != 0">{{fila.capacidad_am}} Mbit/s</span>
											</td>
											<td>{{fila.banda}} GHz</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="aprobarFilas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Aprobar filas</h3>

	<br>
	<p>¿Está seguro de aprobar las filas seleccionadas?</p>
	<br>

	<button class="btn btn-sm btn-success" ng-click="ctrl.aprobarFilas()" ng-hide="ctrl.procesandoFilas">Aprobar</button>
	<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarAprobacion()" ng-hide="ctrl.procesandoFilas">Cancelar</button>

	<div class="progress" ng-hide="!ctrl.procesandoFilas">
		<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{ctrl.porcentajeAvance()}}" aria-valuemin="0" aria-valuemax="100" style="width:{{ctrl.porcentajeAvance()}}%">
			{{ctrl.porcentajeAvance()}}%
		</div>
	</div>
</div>

<div id="rechazarFilas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Rechazar filas</h3>

	<br>
	<p>¿Está seguro de rechazar las filas seleccionadas?</p>
	<br>

	<button class="btn btn-sm btn-success" ng-click="ctrl.rechazarFilas()">Rechazar</button>
	<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarRechazo()">Cancelar</button>
</div>