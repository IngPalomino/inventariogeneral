<!--<p>
	<?php print_r($session);?>
</p>-->

<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$scope","$http",function($scope,$http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.elemento = "<?php echo $elemento;?>";
			ref.validaciones_pendientes = [];

			ref.submit = {
				archivo: null,
				archivoOK: false
			};

			function capitalizeFL (texto)
			{
				return texto.charAt(0).toUpperCase() + texto.slice(1);
			}

			function listar_validaciones_pendientes(tipo)
			{
				$http({
					url:"<?php echo base_url() ?>"+"index.php/ingreso_masivo/listar_pendientes/"+tipo,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.validaciones_pendientes = data;
					}
				}).error(function(err){

				});
			}

			ref.analizarArchivo = function(tipo){
				ref.submit.archivo = document.getElementById(tipo).files[0];
				if(ref.submit.archivo)
				{
					var	fileSplitted = ref.submit.archivo.name.split(".");

					if(fileSplitted[fileSplitted.length - 1] == "xlsx")
					{
						var fdata = new FormData();
						fdata.append(tipo,ref.submit.archivo);

						$.ajax({
							url:"<?php echo base_url() ?>"+"index.php/ingreso_masivo/upload_file/"+tipo,
							method:'POST',
							data:fdata,
							contentType:false,
							processData:false,
							dataType:"json",
							success: function(data){
								var lim = document.createElement("LI"),
									lir = document.createElement("LI"),
									textom = document.createTextNode(data.mensaje),
									textor = document.createTextNode(data.resultado);

								lim.appendChild(textom);
								lir.appendChild(textor);

								document.getElementById("console"+capitalizeFL(tipo)).appendChild(lim);
								document.getElementById("console"+capitalizeFL(tipo)).appendChild(lir);

								if(data.bool)
								{
									var li = document.createElement("LI"),
										texto = document.createTextNode("Analizando...");

									li.appendChild(texto);
									document.getElementById("console"+capitalizeFL(tipo)).appendChild(li);

									$.ajax({
										url:"<?php echo base_url() ?>"+"index.php/ingreso_masivo/check_file/"+tipo,
										method:"GET",
										dataType:"json",
										success: function(data){
											if(!data.estado)
											{
												var li = document.createElement("LI"),
													texto = document.createTextNode("Se encontraron los siguientes errores:");
												li.className = "text-danger";
												li.appendChild(texto);
												document.getElementById("console"+capitalizeFL(tipo)).appendChild(li);

												data.mensajes.forEach(function(mensaje,ind,mensajes){
													var li = document.createElement("LI"),
														texto = document.createTextNode(mensaje);
													li.className = "text-danger";
													li.appendChild(texto);
													document.getElementById("console"+capitalizeFL(tipo)).appendChild(li);
												});

												var li = document.createElement("LI"),
													texto = document.createTextNode("Por favor corregir estos errores antes de seguir el proceso de actualización.");
												li.className = "text-danger";
												li.appendChild(texto);
												document.getElementById("console"+capitalizeFL(tipo)).appendChild(li);
											}
											else
											{
												var li = document.createElement("LI"),
													texto = document.createTextNode("No se encontraron errores, el archivo se puede subir para actualizar la base de datos.");
												li.className = "text-success";
												li.appendChild(texto);
												document.getElementById("console"+capitalizeFL(tipo)).appendChild(li);

												var li = document.createElement("LI"),
													texto = document.createTextNode(data.mensajes);
												li.className = "text-success";
												li.appendChild(texto);
												document.getElementById("console"+capitalizeFL(tipo)).appendChild(li);

												$scope.$apply(function(){
													ref.submit.archivoOk = true;
												});
											}
										}
									});
								}
								else
								{
									var li = document.createElement("LI"),
										texto = document.createTextNode("No se pudo subir el archivo");
									li.className = "text-danger";
									li.appendChild(texto);
									document.getElementById("console"+capitalizeFL(tipo)).appendChild(li);
								}
							}
						});
					}
					else
					{
						var li = document.createElement("LI"),
							texto = document.createTextNode("No se admiten archivos con otra extensión que no sea '.xlsx'");
						li.className = "text-danger";
						li.appendChild(texto);
						document.getElementById("console"+capitalizeFL(tipo)).appendChild(li);
					}
				}
				else
				{
					var li = document.createElement("LI"),
						texto = document.createTextNode("Por favor elija un archivo");
					li.className = "text-danger";
					li.appendChild(texto);
					document.getElementById("console"+capitalizeFL(tipo)).appendChild(li);
					return false;
				}

				/*console.log(ref.validaciones_pendientes);*/
			}

			ref.updateConArchivo = function(tipo){
					$.ajax({
						url:"<?php echo base_url() ?>"+"index.php/ingreso_masivo/update_file/"+tipo,
						method:'GET',
						success: function(data){
							var li = document.createElement("LI"),
								texto = document.createTextNode("Se han actualizado/ingresado "+data+" elementos en la base de datos.");
							li.className = "text-success";
							li.appendChild(texto);
							document.getElementById("console"+capitalizeFL(tipo)).appendChild(li);

							listar_validaciones_pendientes(ref.elemento);
						}
					});
				}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.borrarConsole = function(tipo){
				var lista = document.getElementById("console"+capitalizeFL(tipo));
				var children = lista.childNodes;
				for (var i = children.length - 1; i >= 0; i--)
				{
					document.getElementById("console"+capitalizeFL(tipo)).removeChild(children[i]);
				}
			}

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			listar_validaciones_pendientes(ref.elemento);
		}]);
</script>
</body>
</html>