<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;

			ref.session = <?php echo json_encode($session);?>;

			ref.elemento = {};
			ref.filas_pendientes = [];
			ref.filas_validadas = [];
			ref.filasAprobadas = [];
			ref.filasRechazadas = [];

			ref.isAllSelected = false;

			listarElemento = function(){
				$http({
					url:"<?php echo base_url("ingreso_masivo/listar_elemento");?>/"+"<?php echo $elemento; ?>",
					method:"GET"
				}).success(function(data){
					if(Object.keys(data).length)
					{
						ref.elemento = data;
						/*console.log(data);*/

						var t = ref.elemento.created_at? ref.elemento.created_at.split(/[- :]/) : "";
						try{ ref.elemento.created_at = new Date(Number(t[0]), Number(t[1] - 1), Number(t[2]), Number(t[3]), Number(t[4]), Number(t[5])); } catch(err){ ref.elemento.created_at = null; }

						ref.elemento.pendiente = Number(ref.elemento.pendiente);
						ref.elemento.validado = Number(ref.elemento.validado);
						ref.elemento.rechazado = Number(ref.elemento.rechazado);

						ref.elemento.avance = function(){
							return Math.round(((ref.elemento.validado + ref.elemento.rechazado) * 100) / (ref.elemento.pendiente + ref.elemento.validado + ref.elemento.rechazado));
						}

						if(ref.elemento.pendiente == 0 && ref.elemento.estado.id != 4)
						{
							$http({
								url:"<?php echo base_url("ingreso_masivo/solicitudes/solicitud");?>/"+ref.elemento.id,
								method:"PUT",
								headers:{'Content-Type': 'application/x-www-form-urlencoded'}
							}).success(function(data){
								if(!data.error)
								{
									listarElemento();
								}
								else
								{

								}
							}).error(function(err){
								
							});
						}
					}
					else
					{
						ref.elemento = {};
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			listarFilasPendientes = function(){
				$http({
					url:"<?php echo base_url("ingreso_masivo/filas_pendientes/listar");?>/"+"<?php echo $elemento; ?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.filas_pendientes = data;

						ref.filas_pendientes.forEach(function(fila) {
							try{ fila.anio_servicio = new Date(fila.anio_servicio + " 00:00:00"); } catch(err){ fila.anio_servicio = null; }
						});
						//console.log(data);

						ref.filas_pendientes.forEach(function(dato, ind, objeto){
							objeto[ind].check = false;
						});
					}
					else
					{
						ref.filas_pendientes = [];
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			listarFilasValidadas = function(){
				$http({
					url:"<?php echo base_url("ingreso_masivo/filas_validadas/listar");?>/"+"<?php echo $elemento; ?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.filas_validadas = data;

						ref.filas_validadas.forEach(function(fila) {
							try{ fila.anio_servicio = new Date(fila.anio_servicio + " 00:00:00"); } catch(err){ fila.anio_servicio = null; }
						});
					}
					else
					{
						ref.filas_validadas = [];
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			ref.aprobandoFilas = function(info){
				info.forEach(function(dato){
					if(dato.check)
					{
						ref.filasAprobadas.push(dato);
					}
				});

				$("#aprobarFilas").bPopup();
			}

			ref.aprobarFilas = function(){
				ref.procesandoFilas = true;
				var t = 0;
				for(var i = 0; i < ref.filasAprobadas.length; i++)
				{
					var elemento = ref.filasAprobadas[i];
					var data = "id_actualizar="+elemento.id_actualizar
								+"&sitio="+elemento.sitio.id
								+"&tipo_controlador_rectif="+elemento.tipo_controlador_rectif.id
								+"&anio_servicio="+elemento.anio_servicio.toISOString()
								+"&numero_serie="+elemento.numero_serie
								+"&capacidad_maxima="+elemento.capacidad_maxima
								+"&energizacion_controlador_rectif="+elemento.energizacion_controlador_rectif.id
								+"&ubicacion_controlador_rectif="+elemento.ubicacion_controlador_rectif.id
								+"&modulos_rectif="+angular.toJson(elemento.temp_modulos_rectif);
								//console.log(data);

					$http({
						url:"<?php echo base_url("ingreso_masivo/rectificadores/aprobar");?>/"+elemento.id,
						method:"POST",
						data:data,
						headers:{'Content-Type': 'application/x-www-form-urlencoded'}
					}).success(function(data){
						if(!data.error)
						{
							t++;

							ref.porcentajeAvance = function(){
								return Math.round((t * 100) / ref.filasAprobadas.length);
							}

							if(t == ref.filasAprobadas.length)
							{
								ref.filas_pendientes = [];
								ref.filas_validadas = [];
								listarElemento();
								listarFilasPendientes();
								listarFilasValidadas();
								ref.isAllSelected = false;
								setTimeout(ref.cancelarAprobacion, 3000);
							}
						}
						else
						{

						}
					}).error(function(err){
						
					});
				}
			}

			ref.cancelarAprobacion = function(){
				ref.filasAprobadas = [];
				$("#aprobarFilas").bPopup().close();
				ref.procesandoFilas = false;
			}

			ref.rechazandoFilas = function(info){
				info.forEach(function(dato){
					if(dato.check)
					{
						ref.filasRechazadas.push(dato);
					}
				});

				$("#rechazarFilas").bPopup();
			}

			ref.rechazarFilas = function(){
				var t = 0;
				for(var i = 0; i < ref.filasRechazadas.length; i++)
				{
					var antena = ref.filasRechazadas[i];

					$http({
						url:"<?php echo base_url("ingreso_masivo/rectificadores/rechazar");?>/"+antena.id,
						method:"PUT",
						headers:{'Content-Type': 'application/x-www-form-urlencoded'}
					}).success(function(data){
						if(!data.error)
						{
							t++;

							if(t == ref.filasRechazadas.length)
							{
								ref.filasRechazadas = [];
								ref.filas_pendientes = [];
								ref.filas_validadas = [];
								listarElemento();
								listarFilasPendientes();
								listarFilasValidadas();
								ref.isAllSelected = false;
								$("#rechazarFilas").bPopup().close();
							}
						}
						else
						{

						}
					}).error(function(err){
						
					});
				}
			}

			ref.cancelarRechazo = function(){
				ref.filasRechazadas = [];
				$("#rechazarFilas").bPopup().close();
			}

			ref.selectAll = function(){
				ref.filas_pendientes.forEach(function(antena){
					antena.check = ref.isAllSelected;
				});
			}
			
			ref.rectificadorSeleccionado = function(){
				ref.isAllSelected = ref.filas_pendientes.every(function(antena){return antena.check;});
			}

			ref.disabledFilas = function(){
				var dis = false;
				
				ref.filas_pendientes.forEach(function(t){
					dis = dis || t.check;
				});
				
				return dis;
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			listarElemento();
			listarFilasPendientes();
			listarFilasValidadas();
		}]);
</script>
</body>
</html>