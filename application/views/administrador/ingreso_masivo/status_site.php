<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<div class="well">
				<label>Plantilla de ingreso:</label>
				<a href="<?php echo base_url("index.php/ingreso_masivo/plantilla_status_site");?>">plantilla_status_site.xlsx</a>
				<hr>
				<label>Si no desea toda la plantilla elija los campos:</label>
				<form id="formPlantillaStatusSite" action="<?php echo base_url("index.php/ingreso_masivo/plantilla_$elemento");?>" enctype="application/x-www-form-urlencoded" method="post" target="_self" novalidate>
					<div class="row">
						<div class="col-xs-6 col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" aria-label="" name="prioridad" ng-model="ctrl.submit.desc_plantilla.prioridad">
								</span>
								<input type="text" class="form-control input-sm" aria-label="" disabled="disabled" placeholder="Prioridad">
							</div>
						</div>

						<div class="col-xs-6 col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" aria-label="" name="ubicacion" ng-model="ctrl.submit.desc_plantilla.ubicacion">
								</span>
								<input type="text" class="form-control input-sm" aria-label="" disabled="disabled" placeholder="Ubicación">
								<!-- Dirección, Departamento, Provincia, Distrito -->
							</div>
						</div>

						<div class="col-xs-6 col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" aria-label="" name="coordenadas" ng-model="ctrl.submit.desc_plantilla.coordenadas">
								</span>
								<input type="text" class="form-control input-sm" aria-label="" disabled="disabled" placeholder="Coordenadas">
								<!-- Latitud, Longitud -->
							</div>
						</div>

						<div class="col-xs-6 col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" aria-label="" name="contratos_contactos" ng-model="ctrl.submit.desc_plantilla.contratos_contactos">
								</span>
								<input type="text" class="form-control input-sm" aria-label="" disabled="disabled" placeholder="Contratos y contactos">
							</div>
						</div>

						<div class="col-xs-6 col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" aria-label="" name="torre_estacion" ng-model="ctrl.submit.desc_plantilla.torre_estacion">
								</span>
								<input type="text" class="form-control input-sm" aria-label="" disabled="disabled" placeholder="Torre y estación">
							</div>
						</div>

						<div class="col-xs-6 col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" aria-label="" name="caracteristicas_especiales" ng-model="ctrl.submit.desc_plantilla.caracteristicas_especiales">
								</span>
								<input type="text" class="form-control input-sm" aria-label="" disabled="disabled" placeholder="Características especiales">
							</div>
						</div>

						<div class="col-xs-6 col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" aria-label="" name="torrera_coubicador" ng-model="ctrl.submit.desc_plantilla.torrera_coubicador">
								</span>
								<input type="text" class="form-control input-sm" aria-label="" disabled="disabled" placeholder="Torrera / Coubicador">
							</div>
						</div>

						<div class="col-xs-6 col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" aria-label="" name="operador_coubicante" ng-model="ctrl.submit.desc_plantilla.operador_coubicante">
								</span>
								<input type="text" class="form-control input-sm" aria-label="" disabled="disabled" placeholder="Operador coubicante">
							</div>
						</div>

						<div class="col-xs-6 col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" aria-label="" name="construccion" ng-model="ctrl.submit.desc_plantilla.construccion">
								</span>
								<input type="text" class="form-control input-sm" aria-label="" disabled="disabled" placeholder="Construcción">
							</div>
						</div>

						<div class="col-xs-6 col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" aria-label="" name="mantenimiento_acceso" ng-model="ctrl.submit.desc_plantilla.mantenimiento_acceso">
								</span>
								<input type="text" class="form-control input-sm" aria-label="" disabled="disabled" placeholder="Mantenimiento y acceso">
							</div>
						</div>
					</div>
					<hr>
					<div class="row text-center">
						<button class="btn btn-primary btn-sm" type="submit" ng-disabled="!ctrl.submit.desc_plantilla.prioridad && !ctrl.submit.desc_plantilla.ubicacion && !ctrl.submit.desc_plantilla.coordenadas && !ctrl.submit.desc_plantilla.contratos_contactos && !ctrl.submit.desc_plantilla.torre_estacion && !ctrl.submit.desc_plantilla.caracteristicas_especiales && !ctrl.submit.desc_plantilla.torrera_coubicador && !ctrl.submit.desc_plantilla.operador_coubicante && !ctrl.submit.desc_plantilla.construccion && !ctrl.submit.desc_plantilla.mantenimiento_acceso">Descargar plantilla</button>
					</div>
				</form>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Validaciones pendientes:
				</div>
				<div class="panel-body">
					<table class="table table-stripped">
						<thead>
							<tr>
								<th>Orden</th>
								<th>Fecha de ingreso</th>
								<th>Usuario</th>
							</tr>
						</thead>
						<tbody>
							<tr class="ng-cloak" ng-repeat="pendiente in ctrl.validaciones_pendientes">
								<td>{{pendiente.id}}</td>
								<td>{{pendiente.created_at}}</td>
								<td>{{pendiente.usuario.nombres}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<br>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<div class="form-group">
				<label>Archivo:</label>
				<input class="btn btn-default" id="fileTorre" type="file"></input>
			</div>
			<div class="form-group">
				<button class="btn btn-info" ng-disabled="ctrl.submit.archivo.length" ng-click="ctrl.analizarArchivo('fileTorre')">Analizar archivo</button>
				<button class="btn btn-success" ng-disabled="!ctrl.submit.archivoOk" ng-click="ctrl.updateConArchivo('fileTorre')">Subir archivo para validación</button>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Resultados:
				</div>
				<div class="panel-body" style="height: 200px;word-wrap: break-word;overflow-y: scroll;">
					<ul id="consoleFileTorre" style="list-style: none;">

					</ul>
				</div>
				<div class="panel-footer text-center">
					<button class="btn btn-danger" ng-click="ctrl.borrarConsole('fileTorre')">Borrar historial</button>
				</div>
			</div>
		</div>
	</div>
</section>