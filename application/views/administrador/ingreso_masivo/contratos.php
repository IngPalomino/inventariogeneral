<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h4>{{ctrl.elemento.tabla.modulo}}</h4>
			</div>
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url("administrar"); ?>">Administrar</a></li>
				<li><a href="<?php echo base_url("administrar/ingreso_masivo"); ?>">Ingreso Masivo</a></li>
				<li class="active">Elemento - {{ctrl.elemento.tabla.modulo}}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary with-nav-tabs">
				<div class="panel-heading text-center">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#solicitud" data-toggle="tab"><strong>Solicitud de {{ctrl.elemento.tabla.modulo}}</strong></a></li>
						<li><a href="#pendientes" data-toggle="tab"><strong>Filas Pendientes de validación <span class="badge">{{ctrl.filas_pendientes.length}}</span></strong></a></li>
						<li><a href="#validadas" data-toggle="tab"><strong>Filas Validadas <span class="badge">{{ctrl.filas_validadas.length}}</span></strong></a></li>
					</ul>
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div id="solicitud" class="tab-pane fade in active">
							<table class="table table-condensed table-responsive">
								<tbody>
									<tr>
										<td class="info text-right">Fecha de solicitud</td>
										<td class="active">{{ctrl.elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}}</td>
									</tr>
									<tr>
										<td class="info text-right">Usuario solicitante</td>
										<td class="active">{{ctrl.elemento.usuario.nombres}} {{ctrl.elemento.usuario.apellido_paterno}}</td>
									</tr>
									<tr>
										<td class="info text-right">Módulo</td>
										<td class="active">{{ctrl.elemento.tabla.modulo}}</td>
									</tr>
									<tr>
										<td class="info text-right">Estado</td>
										<td class="active">
											<span ng-class="{'label label-warning': ctrl.elemento.estado.id == 1, 'label label-success': ctrl.elemento.estado.id == 4}">{{ctrl.elemento.estado.estado}}</span>
										</td>
									</tr>
									<tr>
										<td class="info text-right">Avance</td>
										<td class="active">
											<div class="progress">
												<div ng-class="{'progress-bar progress-bar-success': ctrl.elemento.avance() >= 80, 'progress-bar progress-bar-warning': ctrl.elemento.avance() >= 40 && ctrl.elemento.avance() < 80, 'progress-bar progress-bar-danger': ctrl.elemento.avance() < 40}" role="progressbar" aria-valuenow="{{ctrl.elemento.avance()}}" aria-valuemin="0" aria-valuemax="100" style="width:{{ctrl.elemento.avance()}}%">
													{{ctrl.elemento.avance()}}%
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td class="info text-right">Fecha de Finalización</td>
										<td class="active">
											<span ng-if="!ctrl.elemento.fecha_fin">Sin Finalizar</span>
											<span ng-if="ctrl.elemento.fecha_fin">{{ctrl.elemento.fecha_fin}}</span>
										</td>
									</tr>
									<!--<tr>
										<td class="info text-right">Validado por</td>
										<td class="active">{{ctrl.elemento.validador.nombres}}</td>
									</tr>-->
								</tbody>
							</table>
						</div>
						<div id="pendientes" class="tab-pane fade">
							<div style="max-height:300px;overflow-y:auto;">
								<table class="table table-condensed table-responsive table-hover small-font" style="width:2000px;overflow-x:auto;">
									<thead>
										<tr>
											<th><input type="checkbox" ng-click="ctrl.selectAll()" ng-model="ctrl.isAllSelected"></th>
											<th>Tipo</th>
											<th>Sitio</th>
											<th style="width:8%">ID Contrato</th>
											<th>Departamento</th>
											<th>Provincia</th>
											<th>Distrito</th>
											<th style="width:8%;">Dirección</th>
											<th>Área de piso</th>
											<th>Fecha inicio contrato</th>
											<th>Fecha fin contrato</th>
											<th>Arrendador</th>
											<th style="width:20%">Contactos</th>
											<th>Teléfonos</th>
											<th>Correos</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="fila in ctrl.filas_pendientes">
											<td><input type="checkbox" ng-model="fila.check" ng-change="ctrl.popSeleccionado()"></td>
											<td>
												<span class="label label-warning" ng-if="fila.id_actualizar">UPDATE</span>
												<span class="label label-info" ng-if="!fila.id_actualizar">INSERT</span>
											</td>
											<td>{{fila.sitio.nombre_completo}}</td>
											<td>{{fila.id_contrato}}</td>
											<td>{{fila.zona_peru.departamento}}</td>
											<td>{{fila.zona_peru.provincia}}</td>
											<td>{{fila.zona_peru.distrito}}</td>
											<td>{{fila.direccion}}</td>
											<td>{{fila.area}} m2</td>
											<td>{{fila.fecha_inicio | date:"dd/MM/yyyy"}}</td>
											<td>{{fila.fecha_fin | date:"dd/MM/yyyy"}}</td>
											<td>{{fila.arrendador}}</td>
											<td>
												<span ng-repeat="contacto in fila.temp_contactos_contrato">
													<span ng-if="$index > 0">/ </span>
													{{contacto.contacto}}
												</span>
											</td>
											<td>
												<span ng-repeat="contacto in fila.temp_contactos_contrato">
													<span ng-repeat="telefono in contacto.telefono">
														<span class="label label-default">{{telefono.telefono}}</span>
													</span>
												</span>
											</td>
											<td>
												<span ng-repeat="contacto in fila.temp_contactos_contrato">
													<span ng-repeat="correo in contacto.correo">
														<span class="label label-default">{{correo.correo}}</span>
													</span>
												</span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="panel-footer text-center">
								<button class="btn btn-success" ng-disabled="!ctrl.disabledFilas()" ng-click="ctrl.aprobandoFilas(ctrl.filas_pendientes)">Aprobar seleccìón</button>
								<button class="btn btn-danger" ng-disabled="!ctrl.disabledFilas()" ng-click="ctrl.rechazandoFilas(ctrl.filas_pendientes)">Rechazar seleccìón</button>
							</div>
						</div>
						<div id="validadas" class="tab-pane fade">
							<div style="max-height:300px;overflow-y:auto;">
								<table class="table table-condensed table-responsive table-hover small-font" style="width:2000px;overflow-x:auto;">
									<thead>
										<tr>
											<th>Estado</th>
											<th>Tipo</th>
											<th>Sitio</th>
											<th style="width:8%">ID Contrato</th>
											<th>Departamento</th>
											<th>Provincia</th>
											<th>Distrito</th>
											<th style="width:8%;">Dirección</th>
											<th>Área de piso</th>
											<th>Fecha inicio contrato</th>
											<th>Fecha fin contrato</th>
											<th>Arrendador</th>
											<th style="width:20%">Contactos</th>
											<th>Teléfonos</th>
											<th>Correos</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="fila in ctrl.filas_validadas">
											<td>
												<span ng-class="{'label label-danger': fila.estado.id == 3, 'label label-success': fila.estado.id == 2}">{{fila.estado.estado}}</span>
											</td>
											<td>
												<span class="label label-warning" ng-if="fila.id_actualizar">UPDATE</span>
												<span class="label label-info" ng-if="!fila.id_actualizar">INSERT</span>
											</td>
											<td>{{fila.sitio.nombre_completo}}</td>
											<td>{{fila.id_contrato}}</td>
											<td>{{fila.zona_peru.departamento}}</td>
											<td>{{fila.zona_peru.provincia}}</td>
											<td>{{fila.zona_peru.distrito}}</td>
											<td>{{fila.direccion}}</td>
											<td>{{fila.area}} m2</td>
											<td>{{fila.fecha_inicio | date:"dd/MM/yyyy"}}</td>
											<td>{{fila.fecha_fin | date:"dd/MM/yyyy"}}</td>
											<td>{{fila.arrendador}}</td>
											<td>
												<span ng-repeat="contacto in fila.temp_contactos_contrato">
													<span ng-if="$index > 0">/ </span>
													{{contacto.contacto}}
												</span>
											</td>
											<td>
												<span ng-repeat="contacto in fila.temp_contactos_contrato">
													<span ng-repeat="telefono in contacto.telefono">
														<span class="label label-default">{{telefono.telefono}}</span>
													</span>
												</span>
											</td>
											<td>
												<span ng-repeat="contacto in fila.temp_contactos_contrato">
													<span ng-repeat="correo in contacto.correo">
														<span class="label label-default">{{correo.correo}}</span>
													</span>
												</span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="aprobarFilas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Aprobar filas</h3>

	<br>
	<p>¿Está seguro de aprobar las filas seleccionadas?</p>
	<br>

	<button class="btn btn-sm btn-success" ng-click="ctrl.aprobarFilas()" ng-hide="ctrl.procesandoFilas">Aprobar</button>
	<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarAprobacion()" ng-hide="ctrl.procesandoFilas">Cancelar</button>

	<div class="progress" ng-hide="!ctrl.procesandoFilas">
		<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{ctrl.porcentajeAvance()}}" aria-valuemin="0" aria-valuemax="100" style="width:{{ctrl.porcentajeAvance()}}%">
			{{ctrl.porcentajeAvance()}}%
		</div>
	</div>
</div>

<div id="rechazarFilas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Rechazar filas</h3>

	<br>
	<p>¿Está seguro de rechazar las filas seleccionadas?</p>
	<br>

	<button class="btn btn-sm btn-success" ng-click="ctrl.rechazarFilas()">Rechazar</button>
	<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarRechazo()">Cancelar</button>
</div>