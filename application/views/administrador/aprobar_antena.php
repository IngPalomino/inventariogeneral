<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h3>Solicitudes aprobar instalación de antena </h3>
			</div>
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url("administrar"); ?>">Administrar</a></li>
				<li class="active">Aprobar instalación de antena</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">

<!-- microondas  -->
<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Solicitudes Antenas Microondas
				</div>
				<div class="panel-body"  style="overflow-y:auto;">
					<table class="table table-condensed table-hover table-responsive">
						<thead>
							<tr>
								<th><i class="fa fa-cogs"></i></th>
								<th>Fecha y hora</th>
								<th>Sitio</th>
								<th>Nombre Sitio</th>
								<th>Usuario solicitante</th>
								<th>Módulo</th>							
								<th>Estado</th>
								
							</tr>
						</thead>
<tbody>
	    <tr ng-repeat="solicitud in ctrl.solicitudes_microondas">
								<td>
								<a href="<?php echo base_url("administrar/aprobar_antena/mw/elemento/{{solicitud.id}}"); ?>" class="btn btn-primary btn-xs">Ver </a>
								</td>
								<td>
									{{solicitud.created_at | date:"dd/MM/yyyy - HH:mm:ss"}}</td>
                                <td>{{solicitud.sitio}}</td>
                                <td>{{solicitud.nombre_sitio}}</td>

								<td><i class="fa fa-user text-primary" aria-hidden="true"></i>
{{solicitud.nombres}} {{solicitud.apellido_paterno}}</td>
								<td><i class="fa fa-circle" aria-hidden="true"></i>
									
   Infraestructura / {{solicitud.modulo}}Antenas de Microondas</td>

								<td>									
                                   <span   ng-if="solicitud.estado_aprobacion < '3'" class="label label-danger naranja">Pendiente</span>
									<span ng-if="solicitud.estado_aprobacion == '3'" class="label label-success">Finalizado</span>
								</td>
			
							</tr>
						</tbody>
					</table>
				</div>
			</div>
<!-- microondas  -->













			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Solicitudes Antenas GUL
				</div>
				<div class="panel-body"  style="overflow-y:auto;">
					<table class="table table-condensed table-hover table-responsive">
						<thead>
							<tr>
								<th><i class="fa fa-cogs"></i></th>
								<th>Fecha y hora</th>
								<th>Sitio</th>
								<th>Nombre Sitio</th>
								<th>Usuario solicitante</th>
								<th>Módulo</th>							
								<th>Estado</th>
								
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="solicitud in ctrl.solicitudes">
								<td>
									<a href="<?php echo base_url("administrar/aprobar_antena/elemento/{{solicitud.id}}"); ?>" class="btn btn-primary btn-xs">Ver</a>
								</td>
								<td>
									{{solicitud.created_at | date:"dd/MM/yyyy - HH:mm:ss"}}</td>
                                <td>{{solicitud.sitio}}</td>
                                <td>{{solicitud.nombre_completo}}</td>
								<td><i class="fa fa-user text-primary" aria-hidden="true"></i>
{{solicitud.nombres}} {{solicitud.apellido_paterno}}</td>
								<td><i class="fa fa-minus fa-rotate-90" aria-hidden="true"></i>
									
   Infraestructura / {{solicitud.modulo}}GUL</td>
								<td>
									
                                   <span   ng-if="solicitud.estadonumero < '2'" class="label label-danger naranja">Pendiente</span>
									<span ng-if="solicitud.estadonumero == '2'" class="label label-success">Finalizado</span>

								</td>
			
							</tr>
						</tbody>
					</table>
				</div>
			</div>








		</div>
	</div>
</div>