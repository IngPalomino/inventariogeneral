<!--<p>
	<?php print_r($session);?>
</p>-->

<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.relaciones = [];

			ref.relacionNueva = [];
			ref.relacionNueva.tabla_primaria = "0";
			ref.relacionNueva.columna_primaria = "0";
			ref.relacionNueva.tabla_secundaria = "0";
			ref.relacionNueva.columna_secundaria = "0";
			ref.relacionNueva.relacionPrim = "1";
			ref.relacionNueva.relacionSec = "1";
			ref.relacionNueva.relacion = ref.relacionNueva.relacionPrim + ":" + ref.relacionNueva.relacionSec;

			ref.db_tablas = [];
			ref.db_tablasPrimarias = [];
			ref.db_tablasSecundarias = [];
			ref.colsTablaPrimaria = [];
			ref.colsTablaSecundaria = [];
			
			ref.eliminar_Relacion = {};
			ref.editarRelacion = {};

			listar_relaciones = function(){
				$http({
					url:"<?php echo base_url("index.php/relaciones/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.relaciones = data;
					}
				}).error(function(err){

				});
			}

			listar_tablas = function(){
				$http({
					url:"<?php echo base_url("index.php/basedatos/listar");?>",
					method:"GET"
				}).success(function(data){
					console.log(data);
					if(data.length)
					{
						ref.db_tablas = data;

						data.forEach(function(tabla){
							var obj = {tabla:""};

							if(!(tabla.Tables_in_db_inventario_general == "tb_logs" || tabla.Tables_in_db_inventario_general == "tb_usuarios" || tabla.Tables_in_db_inventario_general == "tb_relaciones" || tabla.Tables_in_db_inventario_general == "tb_dependencias"))
							{
								obj.tabla = tabla.Tables_in_db_oym_inventario_general;
								ref.db_tablasPrimarias.push(obj);
							}
						});
					}
					else
					{

					}
				}).error(function(data){

				});
			}

			ref.seleccionPrimaria = function(){
				ref.db_tablasSecundarias = [];
				ref.relacionNueva.tabla_secundaria = "0";
				ref.colsTablaSecundaria = [];
				if(ref.relacionNueva.tabla_primaria != "0")
				{
					ref.db_tablasPrimarias.forEach(function(tabla){
						var obj = {tabla:""};

						if(tabla.tabla != ref.relacionNueva.tabla_primaria)
						{
							obj.tabla = tabla.tabla;
							ref.db_tablasSecundarias.push(obj);
						}
					});

					$http({
						url:"<?php echo base_url("index.php/basedatos/listar/columnas");?>"+"/"+ref.relacionNueva.tabla_primaria,
						method:"GET"
					}).success(function(data){
						if(data.length)
						{
							data.splice(data.length-1,2);

							ref.colsTablaPrimaria = data;
						}
					}).error(function(err){

					});
				}
			}

			ref.seleccionSecundaria = function(){
				if(ref.relacionNueva.tabla_secundaria != "0")
				{
					$http({
						url:"<?php echo base_url("index.php/basedatos/listar/columnas");?>"+"/"+ref.relacionNueva.tabla_secundaria,
						method:"GET"
					}).success(function(data){
						if(data.length)
						{
							data.splice(data.length-1,2);

							ref.colsTablaSecundaria = data;
						}
					}).error(function(err){

					});
				}
			}

			ref.crear = function(){
				var data = "tabla_primaria="+ref.relacionNueva.tabla_primaria
							+"&columna_primaria="+ref.relacionNueva.columna_primaria
							+"&tabla_secundaria="+ref.relacionNueva.tabla_secundaria
							+"&columna_secundaria="+ref.relacionNueva.columna_secundaria
							+"&relacion="+ref.relacionNueva.relacionPrim+":"+ref.relacionNueva.relacionSec;

				$http({
					url:"<?php echo base_url("index.php/relaciones/relacion");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					ref.relacionNueva.tabla_primaria = "0";
					ref.relacionNueva.columna_primaria = "0";
					ref.relacionNueva.tabla_secundaria = "0";
					ref.relacionNueva.columna_secundaria = "0";
					ref.relacionNueva.relacionPrim = "1";
					ref.relacionNueva.relacionSec = "1";

					listar_relaciones();

					$("#nuevaRelacion").bPopup().close();
				}).error(function(err){

				});
			}
			
			ref.cancelarCreacion = function(){
				$("#nuevaRelacion").bPopup().close();
			}
			
			ref.eliminandoRelacion = function(info){
				ref.eliminar_Relacion = info;
				$("#eliminarRelacion").bPopup();
			}
			
			ref.cancelarEliminacion = function(){
				$("#eliminarRelacion").bPopup().close();
				ref.eliminar_Relacion = {};
			}
			
			ref.eliminarRelacion = function(){
				$http({
					url:"<?php echo base_url("index.php/relaciones/relacion");?>"+"/"+ref.eliminar_Relacion.id,
					method:"DELETE"
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.relaciones = [];
						listar_relaciones();
						ref.eliminar_Relacion = {};
						$('#eliminarRelacion').bPopup().close();
					}
				}).error(function(err){

				});
			}
			
			ref.editar_Relacion = function(id){
				$http({
					url:"<?php echo base_url("index.php/relaciones/relacion");?>"+"/"+id,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.editarRelacion = data[0];
						$('#editarRelacion').bPopup();
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.actualizarRelacion = function(id){
				var campos = Object.keys(ref.editarRelacion),
					data = "";

				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled")
					{
						if(campo == "created_at")
						{

						}
						else
						{
							data += campo+"="+ref.editarRelacion[campo];

							if( ind < (objeto.length-2) )
							{
								data += "&";
							}
						}
					}
				});
				console.log(data);
				$http({
					url:"<?php echo base_url("index.php/relaciones/relacion");?>"+"/"+id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.length)
					{

					}
					else
					{
						$('#editarRelacion').bPopup().close();
						ref.editarRelacion = {};
						listar_relaciones();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEdicionRelacion = function(){
				ref.editarRelacion = {};
				$('#editarRelacion').bPopup().close();
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			listar_relaciones();
			listar_tablas();
		}]);
</script>
</body>
</html>