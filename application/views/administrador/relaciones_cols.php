<br>
<div class="container">
	<button class="btn btn-info btn-sm" onclick="$('#nuevaRelacion').bPopup();">
		<span class="fa fa-plus"></span> Nueva relación
	</button>
</div>
<div class="container">
	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Primarias</th>
				<th>Relación</th>
				<th>Secundarias</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="relacion in ctrl.relaciones">
				<td>{{relacion.tabla_primaria}} : {{relacion.columna_primaria}}</td>
				<td>{{relacion.relacion}}</td>
				<td>{{relacion.tabla_secundaria}} : {{relacion.columna_secundaria}}</td>
				<td>
					<button class="btn btn-warning btn-sm" ng-click="ctrl.editar_Relacion(relacion.id)">Editar</button>
					<button class="btn btn-danger btn-sm" ng-click="ctrl.eliminandoRelacion(relacion)">Eliminar</button>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div id="nuevaRelacion" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Crear nueva relación</h3>
	<br>
	
	<table class="table table-condensed">
		<thead>
			<tr>
				<th colspan="2">Tabla primaria</th>
				<th colspan="2">Tabla secundaria</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="info">Tabla</td>
				<td class="active">
					<select ng-change="ctrl.seleccionPrimaria()" ng-model="ctrl.relacionNueva.tabla_primaria">
						<option value="0">Elija una tabla</option>
						<option ng-repeat="tabla in ctrl.db_tablasPrimarias">{{tabla.tabla}}</option>
					</select>
				</td>
				<td class="info">Tabla</td>
				<td class="active">
					<select  ng-change="ctrl.seleccionSecundaria()" ng-model="ctrl.relacionNueva.tabla_secundaria">
						<option value="0">Elija una tabla</option>
						<option ng-repeat="tabla in ctrl.db_tablasSecundarias">{{tabla.tabla}}</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="info">Columna</td>
				<td class="active">
					<select ng-model="ctrl.relacionNueva.columna_primaria">
						<option value="0">Elija una columna</option>
						<option ng-repeat="columna in ctrl.colsTablaPrimaria">{{columna.Field}}</option>
					</select>
				</td>
				<td class="info">Columna</td>
				<td class="active">
					<select ng-model="ctrl.relacionNueva.columna_secundaria">
						<option value="0">Elija una columna</option>
						<option ng-repeat="columna in ctrl.colsTablaSecundaria">{{columna.Field}}</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="info">De</td>
				<td class="active">
					<select ng-model="ctrl.relacionNueva.relacionPrim">
						<option value="1">1</option>
						<option value="N">N</option>
					</select>
				</td>
				<td class="info">A</td>
				<td class="active">
					<select  ng-model="ctrl.relacionNueva.relacionSec">
						<option value="1">1</option>
						<option value="N">N</option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	<br>
	<button class="btn btn-success" ng-click="ctrl.crear()">Crear</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarCreacion()">Cancelar</button>
</div>

<div id="eliminarRelacion" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>
	
	<h3>Eliminar Relación</h3>
	
	<p>¿Está seguro de eliminar la relación?</p>
	
	<br><br>
	
	<button class="btn btn-success" ng-click="ctrl.eliminarRelacion()">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>

<div id="editarRelacion" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Editar relación</h3>
	<br>
	
	<table class="table table-condensed">
		<thead>
			<tr>
				<th colspan="2">Tabla primaria</th>
				<th colspan="2">Tabla secundaria</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="info">Tabla</td>
				<td class="active">
					<select ng-model="ctrl.editarRelacion.tabla_primaria">
						<option value="0">Elija una tabla</option>
						<option ng-repeat="tabla in ctrl.db_tablas" value="{{tabla.tabla}}">{{tabla.tabla}}</option>
					</select>
				</td>
				<td class="info">Tabla</td>
				<td class="active">
					<select ng-model="ctrl.editarRelacion.tabla_secundaria">
						<option value="0">Elija una tabla</option>
						<option ng-repeat="tabla in ctrl.db_tablas">{{tabla.tabla}}</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="info">Columna</td>
				<td class="active">
					<select ng-model="ctrl.editarRelacion.columna_primaria">
						<option value="0">Elija una columna</option>
						<option ng-repeat="columna in ctrl.colsTablaPrimaria">{{columna.Field}}</option>
					</select>
				</td>
				<td class="info">Columna</td>
				<td class="active">
					<select ng-model="ctrl.editarRelacion.columna_secundaria">
						<option value="0">Elija una columna</option>
						<option ng-repeat="columna in ctrl.colsTablaSecundaria">{{columna.Field}}</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="info">De</td>
				<td class="active">
					<select ng-model="ctrl.editarRelacion.relacionPrim">
						<option value="1">1</option>
						<option value="N">N</option>
					</select>
				</td>
				<td class="info">A</td>
				<td class="active">
					<select ng-model="ctrl.editarRelacion.relacionSec">
						<option value="1">1</option>
						<option value="N">N</option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	<br>
	<button class="btn btn-success" ng-click="ctrl.actualizarRelacion()">Guardar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEdicionRelacion()">Cancelar</button>
</div>