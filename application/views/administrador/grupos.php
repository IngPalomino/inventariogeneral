<br>
<div class="container">
	<nav class="navbar navbar-info navbar-curve">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarSite" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div id="navbarSite" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li>
						<a href role="button">Listar</a>
					</li>
					<!--<li>
						<a href class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Movimientos <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href>Todos</a></li>
							<li><a href>Por usuario</a></li>
							<li><a href>Por elemento</a></li>
						</ul>
					</li>-->
				</ul>
			</div>
		</div>
	</nav>
</div>

<br>
<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-group">
				<button class="btn btn-sm btn-default" ng-click="ctrl.nuevo_grupo()"><i class="fa fa-user-plus"></i> Crear Grupo</button>
				<button class="btn btn-sm btn-default"><i class="fa fa-envelope"></i> Enviar correo masivo a un grupo</button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="text-center" ng-if="ctrl.cargando">
				<i class="fa fa-spinner fa-spin fa-4x"></i>
			</div>
			<table class="table table-condensed table-striped" ng-if="!ctrl.cargando && ctrl.grupos.length">
				<thead>
					<tr>
						<th>#</th>
						<th><i class="fa fa-cogs"></i></th>
						<th>Grupo</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="grupo in ctrl.grupos">
						<td>{{$index + 1}}</td>
						<td>
							<div class="dropdown">
								<button class="btn btn-primary btn-xs dropdown-toggle" ng-disabled="!grupo.permiso()" type="button" id="dropdownGruposMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									Acciones
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownGruposMenu">
									<li><a href ng-click="ctrl.editandoGrupo(grupo)"><i class="fa fa-edit"></i> Editar</a></li>
									<li role="separator" class="divider"></li>
									<li><a href ng-click="ctrl.eliminandoGrupo(grupo)"><i class="fa fa-close"></i> Eliminar</a></li>
								</ul>
							</div>
						</td>
						<td>{{grupo.nombre}}</td>
						<td>{{grupo.descripcion}}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div id="editarGrupo" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Editar grupo</h3>
	<form style="width:400px;">
		<table class="table table-condensed">
			<tr>
				<td class="info">Nombre</td>
				<td class="active">
					<input class="form-control input-sm" type="text" ng-model="ctrl.editarGrupo_e.nombre">
				</td>
			</tr>
			<tr>
				<td class="info">Descripción</td>
				<td class="active">
					<input class="form-control input-sm" type="text" ng-model="ctrl.editarGrupo_e.descripcion">
				</td>
			</tr>
			<tr>
				<td class="info">Permisos</td>
				<td class="active">
					<selectize config="ctrl.config" options="ctrl.modulos" ng-model="ctrl.editarGrupo_e.permisos"></selectize>
				</td>
			</tr>
		</table>
		<div class="alert alert-danger" id="alert-editar" ng-hide="!ctrl.error.editar"></div>
		<button class="btn btn-sm btn-success" ng-click="ctrl.grabarGrupo()" ng-disabled="!ctrl.editarGrupo_e.nombre || !ctrl.editarGrupo_e.descripcion || !ctrl.editarGrupo_e.permisos.length">Guardar</button>
		<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarEdicion()">Cancelar</button>
	</form>
</div>

<div id="nuevoGrupo" class="popup" >
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Crear Grupo</h3>

	<p><i>*Todos los campos son obligatorios</i></p>

	<form style="width:400px;">
		<table class="table table-condensed">
			<tr>
				<td class="info">Nombre del grupo</td>
				<td class="active">
					<input class="form-control input-sm" type="text" ng-model="ctrl.nuevoGrupo.nombre">
				</td>
			</tr>
			<tr>
				<td class="info">Descripción</td>
				<td class="active">
					<input class="form-control" type="text" ng-model="ctrl.nuevoGrupo.descripcion">
				</td>
			</tr>
			<tr>
				<td class="info">Permisos</td>
				<td class="active">
					<selectize config="ctrl.config" options="ctrl.modulos" ng-model="ctrl.nuevoGrupo.permisos"></selectize>
				</td>
			</tr>
		</table>
		<div class="alert alert-danger" id="alert-crear" ng-hide="!ctrl.error.crear"></div>
		<button class="btn btn-sm btn-success" ng-disabled="!ctrl.nuevoGrupo.nombre || !ctrl.nuevoGrupo.descripcion || !ctrl.nuevoGrupo.permisos.length" ng-click="ctrl.crearGrupo(ctrl.nuevoGrupo)">Crear</button>
		<button class="btn btn-sm btn-danger" ng-click="ctrl.cancelarCreacion()">Cancelar</button>
	</form>
</div>