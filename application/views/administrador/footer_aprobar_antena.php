<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			ref.auth.evaluador = (ref.session.evaluador == 1)? '' : 'disabled';

			ref.solicitudes = [];			
            ref.solicitudes_microondas = [];
			listarSolicitudes = function(){

//-------- microondas------------
                $http({
					url:"<?php echo base_url("solicitud_instalacion_antenas/solicitudes_mw_listar_all");?>",
					method:"GET"
				}).success(function(data){
                         //console.log(data);


					if(data.length)
					{
						ref.solicitudes_microondas = data;
                       
 //ref.solicitudes_microondas.enlace.idu_1.sitio.nombre_completo;

						ref.solicitudes_microondas.forEach(function(dato, ind, objeto){
					var t = objeto[ind].created_at? objeto[ind].created_at.split(/[- :]/) : "";
							try{ objeto[ind].created_at = new Date(Number(t[0]), Number(t[1] - 1), Number(t[2]), Number(t[3]), Number(t[4]), Number(t[5])); } catch(err){ objeto[ind].created_at = null; }
							objeto[ind].pendiente = Number(objeto[ind].pendiente);
							objeto[ind].validado = Number(objeto[ind].validado);
							objeto[ind].rechazado = Number(objeto[ind].rechazado);
                            objeto[ind].nombre_sitio = objeto[ind].enlace.idu_1.sitio.nombre_completo;
							objeto[ind].avance = function(){
								return Math.round(((this.validado + this.rechazado) * 100)/ (this.pendiente + this.validado + this.rechazado));
							};
						});
					}
					else
					{
						ref.solicitudes_microondas = [];
					}
				}).error(function(err){
					/*console.log(err);*/
				});
//-------- microondas----------------
				$http({
					url:"<?php echo base_url("index.php/solicitud_instalacion_antenas/listar_solicitudes");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.solicitudes = data;
						ref.solicitudes.forEach(function(dato, ind, objeto){
					var t = objeto[ind].created_at? objeto[ind].created_at.split(/[- :]/) : "";
							try{ objeto[ind].created_at = new Date(Number(t[0]), Number(t[1] - 1), Number(t[2]), Number(t[3]), Number(t[4]), Number(t[5])); } catch(err){ objeto[ind].created_at = null; }
							objeto[ind].pendiente = Number(objeto[ind].pendiente);
							objeto[ind].validado = Number(objeto[ind].validado);
							objeto[ind].rechazado = Number(objeto[ind].rechazado);
                           
							objeto[ind].avance = function(){
								return Math.round(((this.validado + this.rechazado) * 100)/ (this.pendiente + this.validado + this.rechazado));
							};




						});
					}
					else
					{
						ref.solicitudes = [];
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						ref.reestablecerContrasena = {};
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
				ref.reestablecerContrasena = {};
			};

			listarSolicitudes();

		}]);
</script>
</body>
</html>