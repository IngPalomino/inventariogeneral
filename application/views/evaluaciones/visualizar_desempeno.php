<br>
<div class="container-fluid" style="margin-bottom: 25px;">
	<div class="row">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="row" style="margin-bottom:5px">
				<div class="col-sm-5 text-right">Evaluación</div>
				<div class="col-sm-7">
					<select class="form-control" ng-model="ctrl.inputs.evaluacion" ng-change="ctrl.seleccionEvaluacion()">
						<option value="">Evaluación...</option>
						<option ng-repeat="evaluacion in ctrl.inputs.evaluaciones" value="{{evaluacion.id}}">{{evaluacion.nombre}}</option>
					</select>
				</div>
			</div>
			<div class="row" style="margin-bottom:5px">
				<div class="col-sm-5 text-right">Año</div>
				<div class="col-sm-7">
					<select class="form-control" ng-model="ctrl.inputs.anio" ng-change="ctrl.seleccionAnio()">
						<option value="">Año...</option>
						<option ng-repeat="anio in ctrl.inputs.anios" value="{{anio.anio}}">{{anio.anio}}</option>
					</select>
				</div>
			</div>
			<div class="row" style="margin-bottom:5px">
				<div class="col-sm-5 text-right">Orden evaluación</div>
				<div class="col-sm-7">
					<select class="form-control" ng-model="ctrl.inputs.periodo" ng-change="ctrl.seleccionPeriodo()">
						<option value="">Orden...</option>
						<option ng-repeat="periodo in ctrl.inputs.periodos" value="{{periodo.id}}">{{periodo.num_periodo}}</option>
					</select>
				</div>
			</div>
			<div class="row" style="margin-bottom:5px">
				<div class="col-sm-5 text-right">Fecha inicio</div>
				<div class="col-sm-7 ng-cloak">
					{{ctrl.resultado.created_at}}
				</div>
			</div>
			<div class="row" style="margin-bottom:5px">
				<div class="col-sm-5 text-right">Contrata</div>
				<div class="col-sm-7">
					<select class="form-control" ng-model="ctrl.inputs.contrata" ng-change="ctrl.seleccionContrata()">
						<option value="">*</option>
						<option ng-repeat="contrata in ctrl.resultado.contrata" value="{{contrata.id}}">{{contrata.contrata}}</option>
					</select>
				</div>
			</div>
			<br/>
			<div class="panel panel-primary ng-cloak" ng-if="ctrl.inputs.contrata == ''">
				<div class="panel-heading text-center">
					Por contratas <span class="btn btn-info" ng-click="ctrl.refrescar()"><i class="fa fa-refresh"></i></span>
				</div>
				<div class="panel-body">
					<table class="table table-striped table-condensed">
						<thead>
							<tr>
								<th>Contrata</th>
								<th>Evaluados</th>
								<th>Total a evaluar</th>
								<th>Desempeño</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="contrata in ctrl.resultado.contrata">
								<td>{{contrata.contrata}}</td>
								<td>{{ctrl.resultado.calculoEvaluados(contrata.id)}}</td>
								<td>{{ctrl.resultado.evaluadosContrata(contrata.id)}}</td>
								<td>{{ctrl.resultado.desempenoContrata(contrata.id) | number:2}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="panel panel-primary ng-cloak" ng-if="ctrl.inputs.contrata != ''">
				<div class="panel-heading text-center">
					Por técnicos - {{ctrl.resultado.nombreContrata(ctrl.inputs.contrata)}} <span class="btn btn-info" ng-click="ctrl.refrescar()"><i class="fa fa-refresh"></i></span>
				</div>
				<div class="panel-body" style="height: 425px; overflow: scroll;">
					<table class="table table-striped table-condensed">
						<thead>
							<tr>
								<th>Técnico</th>
								<th>Tipo de técnico</th>
								<th>Zona</th>
								<th>Evaluador</th>
								<th>Desempeño</th>
							</tr>
							<tr>
								<th>
									<input class="form-control" ng-model="ctrl.inputs.tecnico" />
								</th>
								<th>
									<input class="form-control" ng-model="ctrl.inputs.tipo_tecnico" />
								</th>
								<th>
									<input class="form-control" ng-model="ctrl.inputs.zona" />
								</th>
								<th>
									<select class="form-control" ng-model="ctrl.inputs.evaluador">
										<option value="">*</option>
										<option ng-repeat="evaluador in ctrl.resultado.evaluadores" value="{{evaluador.evaluador}}">{{evaluador.nombres}} {{evaluador.apellido_paterno}}</option>
									</select>
								</th>
								<th>
									<select class="form-control" ng-model="ctrl.inputs.desempeno">
										<option value="">*</option>
										<option value="excelente">Excelente</option>
										<option value="regular">Regular</option>
										<option value="no_cumple">No cumple</option>
									</select>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="evaluado in ctrl.resultado.avance_evaluacion | filter:ctrl.filtroTecnicos">
								<td>{{evaluado.nombre}} {{evaluado.apellidos}}</td>
								<td>{{evaluado.tipo_tecnico}}</td>
								<td>{{evaluado.zona_texto}}</td>
								<td>{{evaluado.evaluador_nombres}} {{evaluado.evaluador_apellido_paterno}}</td>
								<td>{{evaluado.resultado}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>