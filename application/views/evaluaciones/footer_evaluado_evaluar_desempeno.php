<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$scope","$http",function($scope,$http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.evaluado = {};

			infoEvaluado = function(){
				$http({
					url:"<?php echo base_url("index.php/evaluaciones/avance_evaluacion/".$evaluacionPeriodo."/".$evaluado);?>",
					method:"GET"
				}).success(function(data){
					ref.evaluado = data;

					ref.evaluado.contenido.forEach(function(criterio,indx,objetox){
						objetox[indx].calcularPuntaje = function(){
							var puntajeMaximo = 0,
								sumaPuntaje = 0,
								puntajeTotal = 0;

							/*puntajeMaximo = this.preguntas.length * 5;*/

							this.preguntas.forEach(function(pregunta,indy,objetoy){
								(pregunta.respuesta > 0)? (puntajeMaximo+=5) : (puntajeMaximo = puntajeMaximo) ;
								sumaPuntaje += Number(pregunta.respuesta);
							});

							if(puntajeMaximo > 0){ puntajeTotal = sumaPuntaje/puntajeMaximo; }

							return puntajeTotal * 100;
						}
					});

					ref.evaluado.calculoPuntajeFinal = function(){
						var puntajeFinal = 0,
							pesoFinal = 0;

						this.contenido.forEach(function(criterio,indx,objetox){
							(criterio.calcularPuntaje() > 0)? (pesoFinal += Number(criterio.peso)): (pesoFinal = pesoFinal);
							puntajeFinal += Number(criterio.peso) * criterio.calcularPuntaje();
						});

						return puntajeFinal/pesoFinal;
					}
				}).error(function(err){

				});
			};

			ref.registrarEvaluacion = function(){
				var data = "",
					evaluacion = ref.evaluado,
					campos = Object.keys(evaluacion),
					contador = 0;

				borrarPropiedades(evaluacion,"$$hashKey");
				borrarPropiedades(evaluacion,"created_at");
				borrarPropiedades(evaluacion,"ejecutado");

				/*console.log(evaluacion);*/

				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey")
					{
						if(evaluacion[campo] != null)
						{
							if(contador > 0)
							{
								data += "&";
							}


							if(typeof evaluacion[campo] != "function")
							{
								if(typeof evaluacion[campo] != "object")
								{
									data += campo+"="+evaluacion[campo];
								}
								else
								{
									if(campo == "contenido")
									{
										data += "respuestas="+JSON.stringify(evaluacion[campo]);
									}
									else
									{
										data += campo+"="+evaluacion[campo].id;
									}
								}
							}

							contador++;
						}
					}
				});

				data += "&resultado"+"="+evaluacion.calculoPuntajeFinal();
				data += "&evaluador"+"="+ref.session.id_usuario;

				/*console.log(data);*/

				$http({
					url:"<?php echo base_url("index.php/evaluaciones/avance_evaluacion");?>/"+evaluacion.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data == "true" || data === true)
					{
						window.location.assign("http://10.30.17.6/oym/inventarioGeneral/index.php/evaluaciones/evaluar_desempeno/"+"<?php echo $evaluacionPeriodo; ?>");
					}
				}).error(function(err){

				});
			};

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') &&  (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}

			infoEvaluado();
		}])
		.directive('bsPopover',function(){
			return function(scope,element,attrs){
				element.find("span[rel=popover]").popover();
			}
		});
</script>
</body>
</html>