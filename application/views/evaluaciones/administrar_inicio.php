<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url("index.php/evaluaciones/administrar/nueva_evaluacion");?>" class="btn boton" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-plus fa-5x"></span></td></td></tr>
							<tr><td>Crear evaluación</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url("index.php/evaluaciones/administrar/evaluaciones");?>" class="btn boton" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-pencil-square-o fa-5x"></span></td></td></tr>
							<tr><td>Administrar evaluaciones</td></tr>
						</tbody>
					</table>
				</a>
			</div>
		</div>
	</div>
</section>