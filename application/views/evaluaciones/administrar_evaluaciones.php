<br>
<div class="container-fluid" style="margin-bottom: 25px;">
	<div class="row">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="panel panel-primary ng-cloak">
				<div class="panel-heading text-center">
					<b>Evaluaciones activas</b>
				</div>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>Nombre de evaluación</th>
								<th>Periodo</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="evaluacion in ctrl.evaluaciones">
								<td>{{evaluacion.nombre}}</td>
								<td>
									<span ng-hide="evaluacion.edicion">{{evaluacion.periodo}}</span>
									<div class="input-group" ng-show="evaluacion.edicion">
										<input class="form-control" type="number" min="0" ng-model="evaluacion.periodo_cantidad" />
										<div class="input-group-addon">
											<select ng-model="evaluacion.periodo_unidad">
												<option value="">Unidad</option>
												<option value="Semana(s)">Semana(s)</option>
												<option value="Mes(es)">Mes(es)</option>
												<option value="Año(s)">Año(s)</option>
											</select>
										</div>
									</div>
								</td>
								<td>
									<button class="btn btn-warning btn-sm" ng-hide="evaluacion.edicion" ng-click="evaluacion.editar()">Editar</button>
									<button class="btn btn-success btn-sm" ng-show="evaluacion.edicion" ng-click="ctrl.grabar(evaluacion)">Grabar</button>
									<button class="btn btn-danger btn-sm" ng-show="evaluacion.edicion" ng-click="ctrl.cancelar()">Cancelar</button>
									<button class="btn btn-danger btn-sm" ng-hide="evaluacion.edicion" ng-click="ctrl.eliminando(evaluacion)">Eliminar</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="eliminarEvaluacion" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Eliminar evaluación</h3>

	<p>¿Está seguro de eliminar la evaluación "{{ctrl.evaluacion_eliminar.nombre}}"?</p>

	<textarea rows="3" style="width: 100%;resize: none;margin-bottom: 10px;" class="form-control" placeholder="Escriba por qué desea eliminar el elemento..." ng-model="ctrl.evaluacion_eliminar.comentario"></textarea>

	<button class="btn btn-success" ng-click="ctrl.eliminarEvaluacion()" ng-disabled="!ctrl.evaluacion_eliminar.comentario">Eliminar</button>
	<button class="btn btn-danger" ng-click="ctrl.cancelarEliminacion()">Cancelar</button>
</div>