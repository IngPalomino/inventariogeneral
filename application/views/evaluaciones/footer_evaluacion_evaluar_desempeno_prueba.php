<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.evaluados = {
				pendientes: []
			};

			ref.filtros_input = {
				nombre_tecnico: "",
				zona: "",
				tipo_tecnico: "",
				contrata: ""
			};

			listarEvaluados = function(){
				$http({
					url:"<?php echo base_url("index.php/evaluaciones/avance_evaluacion/listar_evaluados/".$evaluacionPeriodo);?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.evaluados.pendientes = data;
						console.log(data);
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.filtros = function(objeto){
				var valido = true;
				if( ref.filtros_input.nombre_tecnico != "" )
				{
					if( !( objeto.tecnico_contrata.nombre.toLowerCase().includes(ref.filtros_input.nombre_tecnico.toLowerCase()) || objeto.tecnico_contrata.apellidos.toLowerCase().includes(ref.filtros_input.nombre_tecnico.toLowerCase()) ) )
					{
						valido = false;
					}
				}

				if( ref.filtros_input.zona != "" )
				{
					if( !( objeto.tecnico_contrata.zona_texto.toLowerCase().includes(ref.filtros_input.zona.toLowerCase()) ) )
					{
						valido = false;
					}
				}

				if( ref.filtros_input.tipo_tecnico != "" )
				{
					if( !( objeto.tecnico_contrata.nombre_tipo_tecnico.toLowerCase().includes(ref.filtros_input.tipo_tecnico.toLowerCase()) ) )
					{
						valido = false;
					}
				}

				if( ref.filtros_input.contrata != "" )
				{
					if( !( objeto.tecnico_contrata.nombre_contrata.toLowerCase().includes(ref.filtros_input.contrata.toLowerCase()) ) )
					{
						valido = false;
					}
				}

				return valido;
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			listarEvaluados();
		}]);
</script>
</body>
</html>