<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url("index.php/evaluaciones/evaluar_desempeno");?>" class="btn boton" ng-class="ctrl.auth.evaluador" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-check-circle-o fa-5x"></span></td></td></tr>
							<tr><td>Evaluar desempeño</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url("index.php/evaluaciones/visualizar_desempeno");?>" class="btn boton" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-eye fa-5x"></span></td></td></tr>
							<tr><td>Visualizar desempeño</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url("index.php/evaluaciones/administrar");?>" class="btn boton" ng-class="ctrl.auth.au1" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-pencil fa-5x"></span></td></td></tr>
							<tr><td>Administrar</td></tr>
						</tbody>
					</table>
				</a>
			</div>
		</div>
	</div>
</section>