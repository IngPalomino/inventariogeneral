<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.evaluaciones = [];
			ref.evaluacion_eliminar = {};

			listarEvaluacionesActivas = function(){
				$http({
					url:"<?php echo base_url("index.php/evaluaciones/evaluacion/listar_activas");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.evaluaciones = data;
						
						ref.evaluaciones.forEach(function(evaluacion,indx,objetox){
							objetox[indx].edicion = false;

							objetox[indx].editar = function(){
								this.edicion = !this.edicion;
							};

							var partes_periodo = objetox[indx].periodo.split(" ");

							objetox[indx].periodo_cantidad = Number(partes_periodo[0]);
							objetox[indx].periodo_unidad = partes_periodo[1];
						});
					}
					else
					{
						ref.evaluaciones = [];
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			ref.cancelar = function(){
				listarEvaluacionesActivas();
			}

			ref.grabar = function(evaluacion){
				var data = "";

				data += "periodo="+evaluacion.periodo_cantidad+" "+evaluacion.periodo_unidad;

				$http({
					url:"<?php echo base_url("index.php/evaluaciones/evaluacion");?>/"+evaluacion.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					/*if(data.error)
					{*/
						listarEvaluacionesActivas();
					/*}
					else
					{
						
					}*/
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			ref.eliminando = function(evaluacion){
				ref.evaluacion_eliminar = evaluacion;
				$("#eliminarEvaluacion").bPopup();
			}

			ref.cancelarEliminacion = function(){
				ref.evaluacion_eliminar = {};
				listarEvaluacionesActivas();
				$("#eliminarEvaluacion").bPopup().close();
			}

			ref.eliminarEvaluacion = function(){
				var data = "comentario="+ref.evaluacion_eliminar.comentario;

				$http({
					url:"<?php echo base_url("index.php/evaluaciones/evaluacion");?>/"+ref.evaluacion_eliminar.id,
					method:"DELETE",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					ref.evaluacion_eliminar = {};
					listarEvaluacionesActivas();
					$("#eliminarEvaluacion").bPopup().close();
				}).error(function(err){

				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			listarEvaluacionesActivas();
		}]);
</script>
</body>
</html>