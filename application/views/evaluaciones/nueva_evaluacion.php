<br>
<div class="container-fluid" style="margin-bottom: 25px;">
	<div class="page-header">
		<div class="row">
			<div class="col-sm-offset-1 col-sm-3 text-right">
				Título de la evaluación:
			</div>
			<div class="col-sm-4">
				<input class="form-control" ng-model="ctrl.evaluacion.nombre" />
			</div>
			<div class="col-sm-3">
				<button class="btn btn-success" style="width:100%" ng-click="ctrl.registarEvaluacion()" ng-disabled="ctrl.evaluacion.evaluarDatosIngreso()">
					<span class="fa fa-floppy-o"></span> Registrar evaluación
				</button>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-offset-1 col-sm-5">
			<label>
				Periodo de evaluación: 
				<div class="input-group">
					<input class="form-control" type="number" min="1" ng-model="ctrl.evaluacion.periodo_cantidad" />
					<div class="input-group-addon">
						<select ng-model="ctrl.evaluacion.periodo_unidad">
							<option value="">Unidad</option>
							<option value="Semana(s)">Semana(s)</option>
							<option value="Mes(es)">Mes(es)</option>
							<option value="Año(s)">Año(s)</option>
						</select>
					</div>
				</div>
			</label>
		</div>
		<div class="col-sm-5">
			<label>
				Contrata(s) a evaluar: 
				<!--<select class="form-control" ng-model="ctrl.evaluacion.contrata">
					<option value="">Contrata</option>
					<option ng-repeat="contrata in ctrl.contratas" value="{{contrata.id}}">{{contrata.contrata}}</option>
				</select>-->
				<selectize config="ctrl.config" options='ctrl.contratas' ng-model="ctrl.evaluacion.contrata"></selectize>
			</label>
		</div>
	</div>

	<div class="panel panel-primary ng-cloak" ng-repeat="criterio in ctrl.evaluacion.contenido">
		<div class="panel-heading">
			<div class="row">
				<div class="col-sm-2 text-right">
					Criterio:
				</div>
				<div class="col-sm-4">
					<input class="form-control" ng-model="criterio.criterio"/>
				</div>
				<div class="col-sm-2 text-right">
					Peso:
				</div>
				<div class="col-sm-4">
					<div class="input-group">
						<input class="form-control" type="number" step="any" min="0" ng-model="criterio.peso" />
						<div class="input-group-addon">
							%
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>Sub-criterio</th>
						<th>Pregunta</th>
						<th>Nota informativa</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="sub_criterio in criterio.preguntas">
						<td>
							<div class="input-group">
								<div class="input-group-addon btn btn-danger">
									<span class="fa fa-times"></span>
								</div>
								<input class="form-control" size="8" max="255" ng-model="sub_criterio.sub_criterio" />
							</div>
						</td>
						<td>
							<textarea class="form-control" style="width:100%;resize:none" max="255" ng-model="sub_criterio.pregunta" /></textarea>
						</td>
						<td>
							<textarea class="form-control" style="width:100%;resize:none" max="255" ng-model="sub_criterio.nota_informativa" /></textarea>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="panel-footer text-center">
			<div class="btn btn-info" ng-click="ctrl.nuevoSubCriterio(criterio)">
				<span class="fa fa-plus"></span> Agregar nuevo sub-criterio
			</div>
		</div>
	</div>
	<button class="btn btn-primary" style="width:100%" ng-click="ctrl.nuevoCriterio()">
		<span class="fa fa-plus"></span> Agregar nuevo criterio
	</button>
</div>