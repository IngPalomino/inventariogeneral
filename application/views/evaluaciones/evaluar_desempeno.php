<br>
<div class="container-fluid" style="margin-bottom: 25px;">
	<div class="row">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="panel panel-primary ng-cloak">
				<div class="panel-heading text-center">
					<b>Evaluaciones pendientes</b>
				</div>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>Evaluación</th>
								<th>Personas a Evaluar / Personas faltantes</th>
								<th>Avance de evaluación</th>
								<th>Tiempo restante</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="evaluacion in ctrl.evaluaciones_periodo.pendientes">
								<td>
									<a href="<?php echo base_url('index.php/evaluaciones/evaluar_desempeno');?>/{{evaluacion.id}}">{{evaluacion.evaluacion.nombre}}</a>
								</td>
								<td>{{evaluacion.calcularTotal()}} / {{evaluacion.calcularFaltantes()}}</td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>