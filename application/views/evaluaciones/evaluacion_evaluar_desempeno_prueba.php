<br>
<div class="container-fluid" style="margin-bottom: 25px;">
	<div class="row">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="panel panel-primary ng-cloak">
				<div class="panel-heading text-center">
					<b>Evaluación: </b>
				</div>
				<div class="panel-body" style="	height: 525px; overflow: scroll;">
					<table class="table">
						<thead>
							<tr>
								<th>Técnico</th>
								<th>Zona</th>
								<th>Tipo técnico</th>
								<th>Contrata</th>
							</tr>
							<tr>
								<th>
									<input class="form-control" ng-model="ctrl.filtros_input.nombre_tecnico" />
								</th>
								<th>
									<input class="form-control" ng-model="ctrl.filtros_input.zona" />
								</th>
								<th>
									<input class="form-control" ng-model="ctrl.filtros_input.tipo_tecnico" />
								</th>
								<th>
									<input class="form-control" ng-model="ctrl.filtros_input.contrata" />
								</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="evaluado in ctrl.evaluados.pendientes | filter:ctrl.filtros">
								<td>
									<a href="<?php echo base_url('index.php/evaluaciones/evaluar_desempeno/'.$evaluacionPeriodo);?>/{{evaluado.tecnico_contrata.id}}">{{evaluado.tecnico_contrata.nombre}} {{evaluado.tecnico_contrata.apellidos}}</a>
								</td>
								<td>
									{{evaluado.tecnico_contrata.zona_texto}}
								</td>
								<td>
									{{evaluado.tecnico_contrata.nombre_tipo_tecnico}}
								</td>
								<td>{{evaluado.tecnico_contrata.nombre_contrata}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>