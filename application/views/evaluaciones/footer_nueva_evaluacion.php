<script type="text/javascript">
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.contratas = [];

			ref.config = {
							create: true,
							valueField: 'id',
							labelField: 'contrata',
							delimiter: '|',
							placeholder: 'Contrata(s)',
							onInitialize: function(selectize){
								// receives the selectize object as an argument
							},
							maxItems: 3
						};

			ref.evaluacion = {
				nombre: "",
				periodo_cantidad: 1,
				periodo_unidad: "",
				periodo: "",
				contrata: [],
				contenido: [],
				calcularPesoRestante: function(){
					resto = 100;
					this.contenido.forEach(function(criterio,ind,objeto){
						resto -= criterio.peso;
					});
					return resto;
				},
				evaluarPeso: function(peso){
					acumulado = 0;
					this.contenido.forEach(function(criterio,ind,objeto){
						acumulado += criterio.peso;
					});

					if(peso <= acumulado)
					{
						return true;
					}
					else
					{
						return false;
					}
				},
				evaluarAcumulado: function(){
					acumulado = 0;
					this.contenido.forEach(function(criterio,ind,objeto){
						acumulado += criterio.peso;
					});

					if(acumulado == 100)
					{
						return true;
					}
					else
					{
						return false;
					}
				},
				nuevoCriterio: function(criterio = "",peso = (ref.evaluacion.calcularPesoRestante()),preguntas = []){
					this.criterio = criterio;
					this.peso = peso;
					this.preguntas = preguntas;
				},
				nuevoSubCriterio: function(sub_criterio = "",pregunta = "",nota_informativa = ""){
					this.sub_criterio = sub_criterio;
					this.pregunta = pregunta;
					this.nota_informativa = nota_informativa;
				},
				evaluarDatosIngreso: function(){
					var respuesta = true;

					respuesta = !this.nombre || !(this.periodo_cantidad > 0) || !this.periodo_unidad || !(this.contrata.length > 0) || !(this.contenido.length > 0);

					this.contenido.forEach(function(criterio,indx,contenido){
						respuesta = respuesta || !criterio.criterio || !(criterio.peso > 0) || !(criterio.preguntas.length > 0);

						criterio.preguntas.forEach(function(pregunta,indy,preguntas){
							respuesta = respuesta || !pregunta.sub_criterio || !pregunta.pregunta || !pregunta.nota_informativa;
						});
					});

					respuesta = respuesta || !this.evaluarAcumulado();

					return respuesta;
				}
			};

			listar_contratas = function(){
				$http({
					url:"<?php echo base_url("index.php/evaluaciones/contrata/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.contratas = data;
					}
				}).error(function(err){

				});
			}

			ref.nuevoCriterio = function(){
				ref.evaluacion.contenido.push(new ref.evaluacion.nuevoCriterio());
			}

			ref.nuevoSubCriterio = function(criterio){
				criterio.preguntas.push(new ref.evaluacion.nuevoSubCriterio());
			}

			ref.registarEvaluacion = function(){
				if(ref.evaluacion.evaluarAcumulado())
				{
					ref.evaluacion.periodo = ref.evaluacion.periodo_cantidad + " " + ref.evaluacion.periodo_unidad;

					var data = "",
						evaluacion = ref.evaluacion,
						campos = Object.keys(evaluacion),
						contador = 0;
					
					evaluacion = borrarPropiedades(evaluacion,"$$hashKey");
					evaluacion = borrarPropiedades(evaluacion,"periodo_cantidad");
					evaluacion = borrarPropiedades(evaluacion,"periodo_unidad");

					campos.forEach(function(campo,ind,objeto){
						if(evaluacion[campo] != null)
						{
							if(typeof evaluacion[campo] != "function")
							{
								if(contador > 0){ data += "&"; }

								if(typeof evaluacion[campo] != "object")
								{
									data += campo+"="+evaluacion[campo];
								}
								else
								{
									data += campo+"="+JSON.stringify(evaluacion[campo]);
								}

								contador++;
							}

							
						}
					});

					/*console.log(evaluacion);
					console.log(data);*/

					$http({
						url:"<?php echo base_url("index.php/Evaluaciones_evaluacion/insertar");?>",
						method:"POST",
						data:data,
						headers:{'Content-Type': 'application/x-www-form-urlencoded'}
					}).success(function(data){
						if(data.error)
						{
							ref.error.error = true;
							ref.error.logIn = data.error;
						}
						else
						{
							if(data.logged)
							{
								location.reload();
							}
						}
					}).error(function(err){

					});
				}
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') &&  (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}

			listar_contratas();
		}]);
</script>
</body>
</html>