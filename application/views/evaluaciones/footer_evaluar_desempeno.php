<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.evaluaciones_periodo = {
				pendientes: []
			};

			listarEvaluacionesPendientes = function(){
				$http({
					url:"<?php echo base_url("index.php/evaluaciones/evaluaciones_periodo/listar_pendientes");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.evaluaciones_periodo.pendientes = data;

						ref.evaluaciones_periodo.pendientes.forEach(function(pendiente,ind,objeto){
							objeto[ind].calcularTotal = function(){
								return this.evaluados.length;
							};

							objeto[ind].calcularEvaluados = function(){
								var evaluados = 0;

								this.evaluados.forEach(function(evaluado,ind,objeto){
									if(Number(evaluado.ejecutado))
									{
										evaluados++;
									}
								});

								return evaluados;
							};

							objeto[ind].calcularFaltantes = function(){
								var faltantes = 0;

								this.evaluados.forEach(function(evaluado,ind,objeto){
									if(!Number(evaluado.ejecutado))
									{
										faltantes++;
									}
								});

								return faltantes;
							};
						});
					}
					else
					{

					}
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			listarEvaluacionesPendientes();
		}]);
</script>
</body>
</html>