<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.inputs = {
				evaluacion: "",
				evaluaciones: [],
				anio: "",
				anios: [],
				periodo: "",
				periodos: [],
				contrata: "",
				tecnico: "",
				tipo_tecnico: "",
				desempeno: "",
				zona: "",
				evaluador: ""
			};

			ref.resultado = {};

			listarEvaluaciones = function(){
				$http({
					url:"<?php echo base_url("index.php/evaluaciones_evaluacion/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.inputs.evaluaciones = data;
					}
					else
					{
						ref.inputs.evaluaciones = [];
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			ref.seleccionEvaluacion = function(){
				if(ref.inputs.evaluacion != "" && ref.inputs.evaluacion != undefined)
				{
					$http({
						url:"<?php echo base_url("index.php/evaluaciones_evaluaciones_periodo/listar_anios");?>/"+ref.inputs.evaluacion,
						method:"GET"
					}).success(function(data){
						if(data.length)
						{
							ref.inputs.anios = data;
						}
						else
						{
							ref.inputs.anios = [];
						}
					}).error(function(err){
						/*console.log(err);*/
					});
				}
				else
				{
					ref.inputs.anios = [];
				}
			}

			ref.seleccionAnio = function(){
				if(ref.inputs.anio != "" && ref.inputs.anio != undefined)
				{
					$http({
						url:"<?php echo base_url("index.php/evaluaciones_evaluaciones_periodo/listar_periodos");?>/"+ref.inputs.evaluacion+"/"+ref.inputs.anio,
						method:"GET"
					}).success(function(data){
						if(data.length)
						{
							ref.inputs.periodos = data;
						}
						else
						{
							ref.inputs.periodos = [];
						}
					}).error(function(err){
						/*console.log(err);*/
					});
				}
				else
				{
					ref.inputs.periodos = [];
				}
			}

			ref.seleccionPeriodo = function(){
				if(ref.inputs.periodo != "" && ref.inputs.periodo != undefined)
				{
					$http({
						url:"<?php echo base_url("index.php/evaluaciones_evaluaciones_periodo/info_avance_desempeno");?>/"+ref.inputs.periodo,
						method:"GET"
					}).success(function(data){
						if(data.length)
						{
							data[0].avance_evaluacion.forEach(function(tecnico,indx,objetox){
								if(tecnico.resultado != null)
								{
									objetox[indx].resultado = Number(objetox[indx].resultado);
								}
							});

							ref.resultado = data[0];

							ref.resultado.desempenoContrata = function(id){
								var desempeno = 0,
									evaluados = 0;

								this.avance_evaluacion.forEach(function(evaluado,indx,objx){
									if( (evaluado.ejecutado == "1") && (evaluado.resultado != null) && (evaluado.contrata == Number(id)) )
									{
										evaluados++;
										desempeno += evaluado.resultado;
									}
								});

								if(evaluados != 0)
								{
									desempeno = desempeno / evaluados;
								}

								return desempeno;
							}

							ref.resultado.calculoEvaluados = function(id){
								var evaluados = 0;

								this.avance_evaluacion.forEach(function(evaluado,indx,objx){
									if( (evaluado.ejecutado == "1") && (evaluado.resultado != null) && (evaluado.contrata == Number(id)) )
									{
										evaluados++;
									}
								});

								return evaluados;
							}

							ref.resultado.evaluadosContrata = function(id){
								var evaluados = 0;

								this.avance_evaluacion.forEach(function(evaluado,indx,objx){
									if( evaluado.contrata == Number(id) )
									{
										evaluados++;
									}
								});

								return evaluados;
							}

							ref.resultado.nombreContrata = function(id){
								var nombre = "";

								this.contrata.forEach(function(contrata){
									if(contrata.id == id)
									{
										nombre = contrata.contrata;
									}
								});

								return nombre;
							}
						}
						else
						{
							ref.resultado = {};
						}

						/*console.log(ref.resultado);*/
					}).error(function(err){
						/*console.log(err);*/
					});
				}
				else
				{
					ref.resultado = {};
				}
			}

			ref.filtroTecnicos = function(objeto){
				var valido = true;
				if( ref.inputs.contrata != "" )
				{
					if( !( objeto.contrata == ref.inputs.contrata ) )
					{
						valido = false;
					}
				}

				if( ref.inputs.tecnico != "" )
				{
					if( !( objeto.nombre.toLowerCase().includes(ref.inputs.tecnico.toLowerCase()) || objeto.apellidos.toLowerCase().includes(ref.inputs.tecnico.toLowerCase()) ) )
					{
						valido = false;
					}
				}

				if( ref.inputs.tipo_tecnico != "" )
				{
					if( !( objeto.tipo_tecnico.toLowerCase().includes(ref.inputs.tipo_tecnico.toLowerCase()) ) )
					{
						valido = false;
					}
				}

				if( ref.inputs.zona != "" )
				{
					if( !( objeto.zona_texto.toLowerCase().includes(ref.inputs.zona.toLowerCase()) ) )
					{
						valido = false;
					}
				}

				if( ref.inputs.desempeno != "" )
				{
					if( ( (ref.inputs.desempeno == "no_cumple") && (objeto.resultado >= 70) ) || ( (ref.inputs.desempeno == "regular") && ((objeto.resultado < 70) || (objeto.resultado >= 90)) ) || ( (ref.inputs.desempeno == "excelente") && (objeto.resultado < 90) ) )
					{
						valido = false;
					}
				}

				if( objeto.resultado == null)
				{
					valido = false;
				}

				if( ref.inputs.evaluador != "" )
				{
					if( objeto.evaluador != ref.inputs.evaluador )
					{
						valido = false;
					}
				}

				return valido;
			}

			ref.refrescar = function(){
				ref.seleccionPeriodo();
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			listarEvaluaciones();
		}]);
</script>
</body>
</html>