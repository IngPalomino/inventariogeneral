<br>
<div class="container-fluid" style="margin-bottom: 25px;">
	<div class="page-header">
		<div class="row">
			<div class="col-sm-offset-3 col-sm-6 text-center">
				Evaluación: {{ctrl.evaluado.evaluacion.nombre}}
			</div>
		</div>
		<div class="row">
			<div class="col-sm-offset-1 col-sm-5 text-right">
				Técnico: {{ctrl.evaluado.tecnico_contrata.nombre}} {{ctrl.evaluado.tecnico_contrata.apellidos}}
			</div>
			<div class="col-sm-5">
				Contrata: {{ctrl.evaluado.tecnico_contrata.nombre_contrata}}
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="panel panel-primary ng-cloak" ng-repeat="criterio in ctrl.evaluado.contenido">
				<div class="panel-heading">
					<div class="row">
						<div class="col-sm-6">
							Criterio: {{criterio.nombre}}
						</div>
						<div class="col-sm-6">
							Peso: {{criterio.peso}} %
						</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="row" ng-repeat="sub_criterio in criterio.preguntas" bs-popover>
						<div class="col-sm-4">
							{{sub_criterio.nombre}}
						</div>
						<div class="col-sm-4">
							<span class="fa fa-question-circle" rel="popover" data-container="body" data-toggle="popover" data-placement="top" data-title="Nota informativa" data-content="{{sub_criterio.nota_informativa}}"></span> {{sub_criterio.pregunta}}
						</div>
						<div class="col-sm-4">
							<form ng-init="sub_criterio.respuesta = 0">
								<label><input type="radio" ng-model="sub_criterio.respuesta" value="0" /> N/A</label>
								<label><input type="radio" ng-model="sub_criterio.respuesta" value="1" /> 1</label>
								<label><input type="radio" ng-model="sub_criterio.respuesta" value="2" /> 2</label>
								<label><input type="radio" ng-model="sub_criterio.respuesta" value="3" /> 3</label>
								<label><input type="radio" ng-model="sub_criterio.respuesta" value="4" /> 4</label>
								<label><input type="radio" ng-model="sub_criterio.respuesta" value="5" /> 5</label>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-danger ng-cloak">
				<div class="panel-heading text-center">
					Resultados
				</div>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>Criterio</th>
								<th>Peso %</th>
								<th>Puntaje %</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="criterio in ctrl.evaluado.contenido">
								<td>{{criterio.nombre}}</td>
								<td>{{criterio.peso}}</td>
								<td>
									<span ng-if="criterio.calcularPuntaje() > 0">{{criterio.calcularPuntaje() | number:2}}</span>
									<span ng-if="criterio.calcularPuntaje() == 0">N/A</span>
								</td>
							</tr>
							<tr class="danger">
								<td colspan="2" class="text-right">Total</td>
								<td>{{ctrl.evaluado.calculoPuntajeFinal()  | number:2}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<button style="width: 100%" class="btn btn-success" ng-click="ctrl.registrarEvaluacion()">
				<span class="fa fa-floppy-o"></span> Registrar evaluación
			</button>
		</div>
	</div>
</div>