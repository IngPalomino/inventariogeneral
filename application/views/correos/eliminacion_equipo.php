<!DOCTYPE html>
<html>
<head>
	<title><?php echo $evento["accion"]; ?></title>
	<style>
		body {font-family:"Calibri","sans-serif";}
	</style>
</head>
<body>
	<p>Se ha realizado una <?php echo $evento["accion"]; ?>.</p>
	<p>Datos generales:</p>
	<ul>
		<?php if(isset($session["responsable"]) && $session["responsable"]): ?>
		<li>Responsable: <?php echo $session["responsable"]; ?></li>
		<?php endif; ?>
		<li>Usuario que realizó la eliminación: <?php echo $session["nombres"]." ".$session["apellidos"]; ?></li>
		<li>Sitio: <?php echo $evento["sitio"]["nombre_completo"] ?></li>
		<li>Fecha y hora: <?php echo date("d/m/Y H:i:s"); ?></li>
	</ul>
	<p>Datos del elemento eliminado:</p>
	<?php echo array_to_ul($evento["elemento"][0]); ?>
	<p><strong>Motivo de eliminación: <?php echo $evento["comentario"]; ?></strong></p>
	<p>Validar: <?php echo base_url($evento['link']); ?></p>
	<p><i>*No responder a esta dirección de correo electrónico.<br>*Mensaje enviado automáticamente desde el Inventario General v2.0.</i></p>
</body>
</html>