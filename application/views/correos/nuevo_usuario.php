<!DOCTYPE html>
<html>
<head>
	<title>Información de nuevo usuario</title>
</head>
<body>
	<p>Estimado(a) <?php echo $nombres.' '.$apellido_paterno.' '.$apellido_materno; ?>,</p>
	<p>Bienvenido(a) al Inventario de Redes de Entel Perú. Puede ingresar al inventario con las credenciales especificadas a continuación:</p>
	<p>
		<ul>
			<li>Usuario: <?php echo $correo; ?></li>
			<li>Contraseña: <?php echo $contrasena; ?></li>
		</ul>
	</p>
	<p>Por favor cambie su contraseña al ingresar al inventario.</p>
	<p><a href="http://10.30.17.6/oym/inventarioGeneral">http://10.30.17.6/oym/inventarioGeneral</a></p>
	<p>Inventario de Redes Entel Perú</p>
</body>
</html>