<!DOCTYPE html>
<html>
<head>
	<title>Información de cambio de contraseña</title>
</head>
<body>
	<p>Estimado(a) <?php echo $nombres." ".$apellido_paterno." ".$apellido_materno; ?>,</p>
	<p>Se le informa que se ha cambiado la contraseña de su usuario:</p>
	<p>
		<ul>
			<li>Usuario: <?php echo $correo; ?></li>
			<li>Contraseña: <?php echo $confirmar_contrasena; ?></li>
		</ul>
	</p>
	<p>Inventario de Redes de Entel Perú</p>
</body>
</html>