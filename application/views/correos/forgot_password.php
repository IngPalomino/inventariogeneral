<!DOCTYPE html>
<html>
<head>
	<title>Olvidé mi contraseña</title>

	<style>
		body {font-family:"Calibri","sans-serif";}
	</style>
</head>
<body>
	<p>Estimado(a) <?php echo $user_info["nombres"]." ".$user_info["apellido_paterno"]; ?>,</p>
	<p>Usted ha solicitado reestablecer su contraseña del Inventario General de Redes de Entel Perú. Este link expirará en 15 minutos.<br>
	{unwrap}<a href="<?php echo $link; ?>">Haga clic aquí para reestablecerla</a>{/unwrap}.</p>
	<p>Si no puede visualizar o hacer clic en el link, copie y pegue {unwrap}<?php echo $link; ?>{/unwrap} en su explorador web.</p>
	<p>Inventario de Redes Entel Perú.</p>
</body>
</html>