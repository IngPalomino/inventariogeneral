<!DOCTYPE html>
<html>
<head>
	<title>Modificación de Aires Acondicionados</title>
</head>
<body>
	<p>Se ha realizado una <?php echo $evento["accion"]; ?>.</p>
	<p>Datos generales:</p>
	<ul>
		<li>Sitio: <?php echo $evento["sitio"]["nombre_completo"] ?></li>
		<li>Usuario que realizó la modificación: <?php echo $session["nombres"]." ".$session["apellidos"]; ?></li>
		<li>Tabla: <?php echo $evento["tabla"]; ?></li>
		<li>ID: <?php echo $evento["id"]; ?></li>
		<!--<li>Cambios: <?php echo $evento["cambios"]; ?></li>-->
	</ul>
	<p>Cambios realizados:</p>
	<ul>
		<li>Datos antiguos:
			<ul>
				<?php foreach($evento["cambios"]["datos_viejos"] as $dato_viejo): ?>
					<?php foreach($dato_viejo as $key => $value): ?>
					<li><?php echo $key.": ".(is_array($value)? json_encode($value) : $value); ?></li>
					<?php endforeach; ?>
				<?php endforeach; ?>
			</ul>
		</li>
		<li>Datos nuevos:
			<ul>
				<?php foreach($evento["cambios"]["datos_nuevos"] as $dato_nuevo): ?>
					<?php foreach($dato_nuevo as $key => $value): ?>
					<li><?php echo $key.": ".(is_array($value)? json_encode($value) : $value); ?></li>
					<?php endforeach; ?>
				<?php endforeach; ?>
			</ul>
		</li>
	</ul>
	<p><i>*Mensaje enviado automáticamente desde el Inventario General.*</i></p>
</body>
</html>