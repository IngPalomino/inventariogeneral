<!DOCTYPE html>
<html>
<head>
	<title><?php echo $evento["accion"]; ?></title>
	<style>
		body {font-family:"Calibri","sans-serif";}
	</style>
</head>
<body>
	<p>Se ha realizado una <?php echo $evento["accion"]; ?>.</p>
	<p>Datos generales:</p>
	<ul>
		<?php if(isset($session["responsable"]) && $session["responsable"]): ?>
		<li>Responsable: <?php echo $session["responsable"]; ?></li>
		<?php endif; ?>
		<li>Usuario que realizó la modificación: <?php echo $session["nombres"]." ".$session["apellidos"]; ?></li>
		<li>Sitio: <?php echo $evento["sitio"]["nombre_completo"] ?></li>
		<li>Fecha y hora: <?php echo date("d/m/Y H:i:s"); ?></li>
	</ul>
	<p>Cambios realizados:</p>
	<ul>
		<li><u><strong>Datos antiguos</strong></u>:
			<ul>
				<?php foreach($evento["cambios"]["datos_viejos"] as $dato_viejo): ?>
					<?php foreach($dato_viejo as $key => $value): ?>
					<?php echo $key.": ".(is_array($value)? json_encode($value) : $value); ?>
					<?php endforeach; ?>
				<?php endforeach; ?>
			</ul>
		</li>
		<li><u><strong>Datos nuevos</strong></u>:
			<ul>
				<?php foreach($evento["cambios"]["datos_nuevos"] as $dato_nuevo): ?>
					<?php foreach($dato_nuevo as $key => $value): ?>
					<?php echo $key.": ".(is_array($value)? json_encode($value) : $value); ?>
					<?php endforeach; ?>
				<?php endforeach; ?>
			</ul>
		</li>
	</ul>
	<p>Más información: <?php echo base_url($evento['link']); ?></p>
	<p><i>*No responder a esta dirección de correo electrónico.<br>*Mensaje enviado automáticamente desde el Inventario General v2.0.</i></p>
</body>
</html>