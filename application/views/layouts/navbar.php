<div class="container-fluid">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand image" href="<?php echo site_url(); ?>"><img src="<?php echo base_url('assets/images/logo-transp.png'); ?>" height="42px"></a>
	</div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
			<li><a href="http://10.30.17.6/oym/app/"><span class="fa fa-home fa-1x"></span> Inicio</a></li>
			<li><a href="<?php echo site_url();?>"><span class="fa fa-archive fa-1x"></span> Inventario</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<?php if ( ! logged_in()): ?>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Acceder <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li>
						<form id="form_log_in" class="log-in-menu">
							<input name="usuario" type="text" class="input-log-in-menu" placeholder="Usuario" ng-model="ctrl.loginForm.usuario"><br>
							<input name="contrasena" type="password" class="input-log-in-menu" placeholder="Contraseña" ng-model="ctrl.loginForm.contrasena"><br>
							<span class="bg-danger msj-error" ng-show="ctrl.loginMessages.error">{{ctrl.loginMessages.error}}<br></span>
							<button class="btn btn-info btn-sm" ng-click="ctrl.iniciarSesion()">Iniciar sesión</button><br>
							<span style="font-size:8pt;"><a href type="button" ng-click="ctrl.restablecerContrasena()">Olvidé mi contraseña</a></span>
						</form>
					</li>
				</ul>
			</li>
			<?php else: ?>
			<li class="dropdown">
				<a href class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
					<span class="fa fa-bell"></span> <span class="badge">0</span>
				</a>
				<ul class="dropdown-menu">
					<li>
						<a href class="btn btn-sm btn-info control-sesion">
							Recordatorios <span class="badge">0</span>
						</a>
					</li>
					<?php if (is_admin()): ?>
					<li role="separator" class="divider"></li>
					<li>
						<a href class="btn btn-sm btn-warning control-sesion">
							Validaciones <span class="badge">0</span>
						</a>
					</li>
					<?php endif; ?>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-user"></span> <span><?php echo $session['nombres']; ?></span> <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo site_url('perfil');?>" class="btn btn-sm btn-info control-sesion"><span class="fa fa-cog"></span> Editar perfil</a></li>
					<?php if (is_admin()): ?>
						<li><a href="<?php echo site_url('administrar');?>" class="btn btn-sm btn-info control-sesion"><span class="fa fa-list"></span> Administrar</a></li>
					<?php endif; ?>
					<li><a href class="btn btn-sm btn-danger control-sesion" ng-click="ctrl.cerrarSesion()"><span class="fa fa-user-times"></span> Cerrar sesión</a></li>
				</ul>
			</li>
			<?php endif; ?>
		</ul>
	</div>
</div>