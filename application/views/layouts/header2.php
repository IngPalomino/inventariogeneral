<!DOCTYPE html>
<html ng-app="app">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Inventario General">
    <meta name="author" content="Alex Nima Pérez">

	<title><?php echo (isset($title) && ! empty($title))? $title.' - ' : ''; ?>Inventario General</title>
	
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/images/logo-transp.png'); ?>"/>

	<?php foreach ($css as $style): ?>
	<link href="<?php echo base_url("assets/styles/{$style}.css"); ?>" rel="stylesheet">
	<?php endforeach; ?>
	<?php foreach ($js as $javascript): ?>
	<script type="text/javascript" src="<?php echo base_url("assets/javascript/{$javascript}.js"); ?>"></script>
	<?php endforeach; ?>
</head>
<body class="ng-cloak" ng-controller="controlador as ctrl">
	<nav class="navbar navbar-default">
    	<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand image" href="http://10.30.17.6/oym/inventarioGeneral/"><img src="<?php echo base_url('assets/images/logo-transp.png'); ?>" height="42px"></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="http://10.30.17.6/oym/app/"><span class="fa fa-home fa-1x"></span> Inicio</a></li>
					<li><a href="<?php echo base_url();?>"><span class="fa fa-archive fa-1x"></span> Inventario</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<?php if ( ! $this->auth->logged_in()): ?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Acceder <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li>
								<form id="form_log_in" class="log-in-menu">
									<input name="usuario" type="text" class="input-log-in-menu" placeholder="Usuario" ng-model="ctrl.formLogIn.usuario"><br>
									<input name="contrasena" type="password" class="input-log-in-menu" placeholder="Contraseña" ng-model="ctrl.formLogIn.contrasena"><br>
									<span class="bg-danger msj-error" ng-show="ctrl.error.error">{{ctrl.error.logIn}}<br></span>
									<button class="btn btn-info btn-sm" ng-click="ctrl.iniciar_sesion()">Iniciar sesión</button><br>
									<span style="font-size:8pt;"><a href ng-click="ctrl.olvide_contrasena()">Olvidé mi contraseña</a></span><br>
									<!--<span style="font-size:8pt;"><a href type="button" data-toggle="modal" data-target="#forgotPassword">Olvidé mi contraseña prueba</a></span>-->
								</form>
							</li>
						</ul>
					</li>
					<?php else: ?>
					<li class="dropdown">
						<a href class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<span class="fa fa-bell"></span> <span class="badge">0</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href class="btn btn-sm btn-info control-sesion">
									Recordatorios <span class="badge">0</span>
								</a>
							</li>
							<?php if ($this->auth->is_admin()): ?>
							<li role="separator" class="divider"></li>
							<li>
								<a href class="btn btn-sm btn-warning control-sesion">
									Validaciones <span class="badge">0</span>
								</a>
							</li>
							<?php endif; ?>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-user"></span> <span><?php echo $session['nombres']; ?></span> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('perfil');?>" class="btn btn-sm btn-info control-sesion"><span class="fa fa-cog"></span> Editar perfil</a></li>
							<?php if ($this->auth->is_admin()): ?>
								<li><a href="<?php echo base_url('administrar');?>" class="btn btn-sm btn-info control-sesion"><span class="fa fa-list"></span> Administrar</a></li>
							<?php endif; ?>
							<li><a href ng-click="ctrl.cerrar_sesion()" class="btn btn-sm btn-danger control-sesion"><span class="fa fa-user-times"></span> Cerrar sesión</a></li>
						</ul>
					</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</nav>

<?php if (!$this->auth->logged_in()): ?>
<div id="reestablecerContrasena" class="popup">
    <span class="button b-close">
        <span>X</span>
    </span>

    <h3>Restablecer Contraseña</h3>

    <p>
        <div class="alert alert-info">Ingrese su correo electrónico para restablecer su contraseña,<br>recibirá un link con instrucciones.</div>
    </p>

    <form>
        <table class="table table-condensed">
            <tbody>
                <tr>
                    <td class="info">Correo electrónico</td>
                    <td class="default">
                        <input type="text" class="form-control" ng-model="ctrl.reestablecerContrasena.correo">
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="row text-center" ng-if="ctrl.procesando">
            <i class="fa fa-cog fa-spin"></i> Procesando solicitud...
        </div>
        <div id="alert-forgot" class="alert alert-danger" ng-hide="!ctrl.error.forgot"></div>
        <div class="alert alert-success" ng-hide="!ctrl.success.forgot">{{ctrl.success.forgot}}</div>
        <button class="btn btn-success" ng-click="ctrl.reestablecer_contrasena()" ng-hide="ctrl.procesando" ng-disabled="!ctrl.reestablecerContrasena.correo">Enviar</button>
        <button class="btn btn-danger" ng-click="ctrl.cancelar_contrasena()" ng-hide="ctrl.procesando">Cancelar</button>
    </form>
</div>
<div class="modal fade" id="forgotPassword" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Restablecer Contraseña</h3>
			</div>
			<form>
				<div class="modal-body">
					<p class="alert alert-info">Ingrese su correo electrónico para restablecer su contraseña, recibirá un link con instrucciones.</p>
					<div class="form-group">
						<label>Correo Electrónico:</label>
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-user"></i></div>
							<input type="email" class="form-control" ng-model="ctrl.passwordReset.correo">
						</div>
					</div>
					<div class="form-group">
						<div class="alert alert-danger" ng-show="ctrl.messages.error">{{ctrl.messages.error}}</div>
						<div class="alert alert-success" ng-show="ctrl.messages.success">{{ctrl.messages.success}}</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary" ng-disabled="!ctrl.passwordReset.correo">Enviar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<?php endif; ?>