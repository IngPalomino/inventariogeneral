<?php if ( ! $this->auth->logged_in()): ?>
<div class="modal fade" id="forgotPassword" role="dialog" ng-controller="sharedController as ctrl">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Restablecer Contraseña</h3>
			</div>
			<div class="modal-body">
				<p class="alert alert-info">Ingrese su correo electrónico para restablecer su contraseña, recibirá un link con instrucciones.</p>
				<div class="form-group">
					<label>Correo Electrónico:</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-user"></i></div>
						<input type="text" class="form-control" ng-model="ctrl.forgotPasswordForm.correo">
					</div>
				</div>
				<div class="form-group">
					<div class="alert alert-danger" ng-show="ctrl.forgotPasswordMessages.error">{{ctrl.forgotPasswordMessages.error}}</div>
					<div class="alert alert-success" ng-show="ctrl.forgotPasswordMessages.success">{{ctrl.forgotPasswordMessages.success}}</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button class="btn btn-primary" ng-click="ctrl.enviarForgotPassword()" ng-disabled="!ctrl.forgotPasswordForm.correo">Enviar</button>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<?php foreach ($js as $javascript): ?>
<script type="text/javascript" src="<?php echo base_url("assets/javascript/{$javascript}.js"); ?>"></script>
<?php endforeach; ?>
<?php foreach ($modules as $module): ?>
<script type="text/javascript" src="<?php echo base_url("modules/{$module}.js"); ?>"></script>
<?php endforeach; ?>

<script type="text/javascript">
	(function() {
		'use strict';

		angular
			.module('app')
			.service('phpService', phpService);

		function phpService() {
			this.base_url = base_url;

			function base_url(segment = '') {
				return '<?php echo base_url(); ?>' + segment;
			}
		}
	})();
</script>
</body>
</html>