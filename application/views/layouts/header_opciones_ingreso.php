<br>
<div class="container">
	<nav class="navbar navbar-info navbar-curve">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarSite" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!--<a class="navbar-brand image" href><img src="assets/images/logo-transp.png" height="42px"></a>-->
				<a class="navbar-brand" href="<?php echo base_url("ingreso/sitios/resumen/{$sitio}");?>"><span class="label label-info"><?php echo $infoSitio[0]['nombre_completo'] ?></span></a>
			</div>
			<div id="navbarSite" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li>
						<a href="<?php echo base_url("ingreso/sitios/gis/{$sitio}");?>">GIS</a>
					</li>
					<li>
						<a href="<?php echo base_url("ingreso/sitios/resumen/{$sitio}");?>">Resumen</a>
					</li>
					<li>
						<a href="<?php echo base_url("ingreso/sitios/torre/{$sitio}");?>">POP</a>
					</li>
					<li>
						<a href class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Infraestructura <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url("ingreso/sitios/infraestructura/antenas/{$sitio}");?>">Antenas</a></li>
							<li><a href="<?php echo base_url("ingreso/sitios/infraestructura/idus/{$sitio}");?>">IDU's</a></li>
							<li><a href="<?php echo base_url("ingreso/sitios/infraestructura/enlaces/{$sitio}");?>">Enlaces</a></li>
						</ul>
					</li>
					<li>
						<a href class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Energía <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url("ingreso/sitios/energia/grupos_electrogenos/{$sitio}");?>">Grupos Electrógenos</a></li>
							<li><a href="<?php echo base_url("ingreso/sitios/energia/rectificadores/{$sitio}");?>">Rectificadores</a></li>
							<li><a href="<?php echo base_url("ingreso/sitios/energia/bancos_baterias/{$sitio}");?>">Bancos de batería</a></li>
							<li><a href="<?php echo base_url("ingreso/sitios/energia/suministros/{$sitio}");?>">Suministros</a></li>
							<li><a href="<?php echo base_url("ingreso/sitios/energia/lineas_electricas/{$sitio}");?>">Líneas eléctricas</a></li>
							<li><a href="<?php echo base_url("ingreso/sitios/energia/tanques_combustible/{$sitio}");?>">Tanques de combustible</a></li>
							<li><a href="<?php echo base_url("ingreso/sitios/energia/aires_acondicionados/{$sitio}");?>">Aires acondicionados</a></li>
						</ul>
					</li>
					<li>
						<a href class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Electrónica <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url("ingreso/sitios/electronica/nodo_b/{$sitio}");?>">Nodo B</a></li>
							<li><a href="<?php echo base_url("ingreso/sitios/electronica/microondas/{$sitio}");?>">Microondas</a></li>
							<li><a href="<?php echo base_url("ingreso/sitios/electronica/wimax/{$sitio}");?>">Wimax</a></li>
							<li><a href="<?php echo base_url("ingreso/sitios/electronica/iden/{$sitio}");?>">iDEN</a></li>
						</ul>
					</li>
					<li>
						<a href="<?php echo base_url("ingreso/sitios/documentacion/{$sitio}");?>">Documentación</a>
					</li>
					<li><a href="<?php echo base_url("ingreso/sitios/capacity/{$sitio}"); ?>">Capacity</a></li>
					<!--<li>
						<a href="http://10.44.18.2/index.htm" target="_blank"><i class="fa fa-camera"></i></a>
					</li>-->
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<?php
						$c_url = current_url();
						$visualizar_url = str_replace('ingreso', 'ver', $c_url);
					?>
					<!--<li><a class="btn" href="<?php echo $visualizar_url;?>" role="button">Ir a visualización</a></li>-->
					<li><a data-toggle="tooltip" title="Ir a modo visualización" class="btn change-icon" href="<?php echo $visualizar_url;?>" role="button" style="padding:11px;">
						<i class="fa fa-edit fa-2x"></i>
						<i class="fa fa-eye fa-2x"></i>
					</a></li>
				</ul>
			</div>
		</div>
	</nav>
</div>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>