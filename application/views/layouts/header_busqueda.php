<br>
<div class="container">
	<div>
		<div class="form-inline">
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label>POPs</label>
						<form class="input-group" ng-submit="ctrl.buscar_sitios()">
							<input type="text" name="inputSitios" class="form-control" placeholder="Ejemplo: 0130002,0130001_LM_Oquendo" style="width:100%" ng-model="ctrl.busquedaSitios" required>
							<div class="input-group-btn">
								<button class="btn btn-default" ng-click="ctrl.buscar_sitios()" ng-disabled="!ctrl.busquedaSitios">
									<span class="fa fa-search" ng-if="!ctrl.buscandoSitios"></span>
									<span class="fa fa-spinner fa-spin ng-cloak" ng-if="ctrl.buscandoSitios"></span>
								</button>
							</div>
						</form>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label>IDU's</label>
						<div class="input-group">
							<input type="text" name="inputIdus" class="form-control" placeholder="Ingrese NE ID ó serie" style="width:100%">
							<div class="input-group-btn">
								<div class="btn btn-default">
									<span class="fa fa-search"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label># Suministro</label>
						<form class="input-group" ng-submit="ctrl.buscar_sitios_suministro()">
							<input type="text" name="inputSitiosSuministro" class="form-control" placeholder="Ingrese n&uacute;mero de suministro" style="width:100%" ng-model="ctrl.busquedaSitiosSuministro">
							<div class="input-group-btn">
								<button class="btn btn-default" ng-click="ctrl.buscar_sitios_suministro()" ng-disabled="!ctrl.busquedaSitiosSuministro">
									<span class="fa fa-search" ng-if="!ctrl.buscandoSitiosSuministro"></span>
									<span class="fa fa-spinner fa-spin ng-cloak" ng-if="ctrl.buscandoSitiosSuministro"></span>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>