<br>
<div class="container">
	<nav class="navbar navbar-info navbar-curve orange_menu">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarSite" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>				
				<a class="navbar-brand"  href="<?php echo base_url("resumen/salastecnicas/mso/$id_mso");?>"><span class="label label-info"><?php echo "Sala Técnica "; ?>{{ctrl.salaname}}</span>
				</a>
				<input type="hidden" value="<?php echo $id_mso; ?>" name="id_mso" id="id_mso">
			</div>
			<div id="navbarSite" class="navbar-collapse collapse orange_menu">
				<ul class="nav navbar-nav">					
                   <li>
						<a href class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Energía <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li ng-repeat="item in ctrl.dataItem  | filter:{id_categoria:1} | orderBy:'nombre'">
								<a  href="<?php echo base_url("ver/salastecnicas/mso/energia/{{item.link_name}}/$id_mso");?>">									
                                 <span ng-if="item.develop==0"><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                                 <span ng-if="item.develop==1"><i class="fa fa-circle" aria-hidden="true"></i></span>

								{{item.nombre}}</a>
							</li>
						</ul>
					</li>
					<li>
						<a href class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gabinetes <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li ng-repeat="item in ctrl.dataItem | filter:{id_categoria:2}">
								<a   href="<?php echo base_url("ver/salastecnicas/mso/gabinetes/{{item.link_name}}/$id_mso");?>">{{item.nombre}}</a></li>
						</ul>
					</li>
					<li>
						<a href class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Clima <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li ng-repeat="item in ctrl.dataItem | filter:{id_categoria:3}">
							<a  href="<?php echo base_url("ver/salastecnicas/mso/clima/{{item.link_name}}/$id_mso");?>">{{item.nombre}}</a></li>							
						</ul>
					</li>
					<li>
						<a href class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Otros <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li ng-repeat="item in ctrl.dataItem | filter:{id_categoria:4}">
								<a  href="<?php echo base_url("ver/salastecnicas/mso/otros/{{item.link_name}}/$id_mso");?>">{{item.nombre}}</a></li>
						</ul>
					</li>
					<li>
						<a href="<?php echo base_url("ver/sitios/resumen/$id_sitio");?>" class="dropdown-toggle"  role="button" aria-haspopup="true" aria-expanded="false">Sitio  <i class="fa fa-jpy fa-rotate-180 fa-2x"></i></a>
					</li>					
				</ul>
               <ul class="nav navbar-nav navbar-right">
					<?php
						$c_url = current_url();
						$editar_url = str_replace('ver', 'ingreso', $c_url);
					?>
					<!--<li><a class="btn" href="<?php echo $editar_url;?>" ng-class="<?php echo logged_in()? "" : "'disabled'"; ?>" role="button">Editar sitio</a></li>-->
					<li><a data-toggle="tooltip" title="Ir a modo edición" class="btn change-icon" href="<?php echo $editar_url;?>" ng-class="<?php echo logged_in()? "" : "'disabled'"; ?>" role="button" style="padding:11px;">
						<i class="fa fa-eye fa-2x"></i>
						<i class="fa fa-edit fa-2x"></i>
					</a></li>
				</ul>
			</div>
		</div>
	</nav>
</div>