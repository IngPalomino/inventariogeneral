<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<a href="<?php echo site_url("ver");?>" class="btn boton" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-eye fa-5x"></span></td></tr>
							<tr><td>Visualización</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<a href="<?php echo site_url("ingreso");?>" class="btn boton" ng-class="<?php echo $this->auth->logged_in()? "" : "'disabled'" ?>" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-pencil-square-o fa-5x"></span></td></tr>
							<tr><td>Ingreso y Modificación</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<a href="<?php echo site_url("reportes");?>" class="btn boton" role="button"><!-- ng-class="ctrl.auth.au3" -->
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-file-text-o fa-5x"></span></td></tr>
							<tr><td>Reportes</td></tr>
						</tbody>
					</table>
				</a>
			</div>

		<div class="col-md-3 col-sm-6 col-xs-12" >
			<a   href="<?php echo site_url('solicitante'); ?>" class="btn boton" role="button"
				ng-class="<?php echo $this->auth->logged_in()? "" : "'disabled'" ?>">
				<table class="table">
					<tbody>
						<tr><td><span class="fa fa-building fa-5x"></span></td></td></tr>
						<tr ><td style="word-wrap: break-word;">Reserva Espacio de Torre</td></tr>
					</tbody>
				</table>
			</a>
		</div>


<div class="col-md-3 col-sm-6 col-xs-12" >
		<a   href="<?php echo site_url('salastecnicas'); ?>" class="btn boton" role="button"
				ng-class="<?php echo $this->auth->logged_in()? "" : "'disabled'" ?>">
				<table class="table">
					<tbody>
						<tr><td><span class="fa fa-columns fa-5x"></span></td></td></tr>
						<tr ><td style="word-wrap: break-word;">Salas Técnicas</td></tr>
					</tbody>
				</table>
		</a>
</div>





		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<a href="<?php echo site_url("evaluaciones");?>" class="btn boton" ng-if="ctrl.auth.evaluador == ''" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-bar-chart fa-5x"></span></td></tr>
							<tr><td>Evaluaciones</td></tr>
						</tbody>
					</table>
				</a>
			</div>
		</div>
	</div>
</section>