<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";
			ref.disabled = false;
			ref.success = {};

			ref.session = <?php echo json_encode($session);?>;

			ref.reestablecerContrasena = {};

			ref.reestablecer_contrasena = function(){
				var alertForgot = document.getElementById("alert-reset");
				var alertSuccess = document.getElementById("alert-success");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "contrasena="+encodeURIComponent(ref.reset_password.contrasena)
							+"&contrasenaconf="+encodeURIComponent(ref.reset_password.contrasenaconf)
							+"&token="+encodeURIComponent("<?php echo $token; ?>");

				$http({
					url:"<?php echo base_url("reset_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
					}
					else
					{
						ref.success.forgot = "Se ha reestablecido su contraseña.<br>Se redireccionará a la página de inicio de sesión en 3 segundos.";
						alertSuccess.innerHTML = ref.success.forgot;
						ref.reset_password = {};
						setTimeout(function(){window.location = "<?php echo base_url(); ?>";},3000);
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.disabled = false;
				});
			}
		}]);
</script>
</body>
</html>