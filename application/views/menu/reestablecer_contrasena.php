<br>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" style="margin-top:50px;">
			<div class="panel panel-info">
				<div class="panel-heading">
                    <div class="panel-title text-center">
                    	Restablecer Contraseña
                    </div>
                </div>
                <div style="padding-top:30px" class="panel-body">
                	<div class="alert alert-info" ng-hide="ctrl.success.forgot">
                		Hola <?php echo $usuario["nombres"]; ?>, puede proceder a restablecer su contraseña.
                	</div>
                	<div id="alert-success" class="alert alert-success" ng-hide="!ctrl.success.forgot"></div>
                	<form id="form_log_in" class="log-in-menu">
                        <div id="alert-reset" class="alert alert-danger" ng-hide="!ctrl.error.forgot"></div>
                		<div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" name="contrasena" value="" placeholder="Nueva contraseña" ng-model="ctrl.reset_password.contrasena">
                        </div>
                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" name="contrasenaconf" placeholder="Confirmar contraseña" ng-model="ctrl.reset_password.contrasenaconf">
                        </div>
                        <div class="form-group">
							<button class="btn btn-lg btn-primary btn-block" ng-click="ctrl.reestablecer_contrasena()" ng-disabled="ctrl.disabled">Enviar</button>
						</div>
                	</form>
                </div>
                <div class="panel-footer">
                	<a href="<?php echo base_url(); ?>">Ir a la página de inicio.</a>
                </div>
			</div>
		</div>
	</div>
</div>