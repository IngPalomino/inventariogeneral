<!--<p>
	<?php print_r($session);?>
</p>-->

<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.validaciones_pendientes = [];

			listar = function(){
				$http({
					url:"<?php echo base_url("index.php/ingreso_masivo/listar");?>",
					method:"GET"
				}).success(function(data){
					ref.validaciones_pendientes = data;
					
					ref.validaciones_pendientes.forEach(function(validacion,ind,objeto){
						objeto[ind].pendiente = Number(objeto[ind].pendiente);
						objeto[ind].validado = Number(objeto[ind].validado);
						objeto[ind].cancelado = Number(objeto[ind].cancelado);
						objeto[ind].revisado = function(){
							return (this.validado + this.cancelado);
						}
						objeto[ind].total = function(){
							return (this.pendiente + this.validado + this.cancelado);
						}
						objeto[ind].fechaCreado = function(){
							var fecha = new Date(this.created_at);
							return ("00"+fecha.getDate()).substr(-2)+"/"+("00"+(fecha.getMonth()+1)).substr(-2)+"/"+fecha.getFullYear()+" "+("00"+fecha.getHours()).substr(-2)+":"+("00"+fecha.getMinutes()).substr(-2)+":"+("00"+fecha.getSeconds()).substr(-2);
						}
						objeto[ind].validadosPorCiento = function(){
							return (this.validado * 100)/(this.pendiente + this.validado + this.cancelado);
						}
						objeto[ind].canceladosPorCiento = function(){
							return (this.cancelado * 100)/(this.pendiente + this.validado + this.cancelado);
						}
					});

					console.log(ref.validaciones_pendientes);
				}).error(function(err){

				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			listar();
		}]);
</script>
</body>
</html>