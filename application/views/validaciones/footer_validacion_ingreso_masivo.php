<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";
			ref.reestablecerContrasena = {};

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.validacion = {
				"id": <?php echo $validacion["id"]?>,
				"tabla": "<?php echo $validacion["tabla"]?>",
				"validaciones": <?php echo json_encode($validacion["validaciones"])?>
			};

			ref.validacionCancelada = {
				id:0,
				comentario:""
			};

			/*listar = function(){
				$http({
					url:"<?php echo base_url("index.php/ingreso_masivo/listar_detalles");?>"+"/"+ref.validacion.id+"/"+ref.validacion.tabla,
					method:"GET"
				}).success(function(data){
					data.forEach(function(validacion,indi,objetoi){
						var campos = Object.keys(validacion);

						campos.forEach(function(campo,indj){
							if(validacion[campo] == null)
							{
								delete validacion[campo];
								delete objetoi[indi][ref.validacion.tabla.substr(3)][campo];
							}
						});
					});

					ref.validaciones = data;
				}).error(function(err){

				});
			};*/

			ref.validando = function(id_im,campo,elemento,id_elemento){
				var data = "id_ingreso_masivo="+id_im+
							"&campo_ingreso_masivo="+campo+
							"&elemento="+elemento+
							"&id_elemento="+id_elemento;

				$http({
					url:"<?php echo base_url("index.php/validaciones/validar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					location.reload();
				}).error(function(err){

				});
			}

			ref.cancelando = function(id_im,campo,elemento,id_elemento){
				$("#cancelarValidacion").bPopup();

				ref.validacionCancelada.id = id_im;
				ref.validacionCancelada.elemento = elemento;
			}

			ref.cancelarCancelacion = function(){
				ref.validacionCancelada = {
					id:0,
					comentario:""
				};

				$("#cancelarValidacion").bPopup().close();
			}

			ref.aceptarCancelacion = function(){
				var data = "comentario_cancelacion="+ref.validacionCancelada.comentario+
							"&elemento="+ref.validacionCancelada.elemento;

				$http({
					url:"<?php echo base_url("index.php/validaciones/cancelar");?>/"+ref.validacionCancelada.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					location.reload();
				}).error(function(err){

				});
			}

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			/*listar();*/
		}]);
</script>
</body>
</html>