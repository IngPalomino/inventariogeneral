<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="panel panel-info">
				<div class="panel-body">
					<table class="table table-condensed table-hover ng-cloak">
						<thead>
							<tr>
								<th>Sitio</th>
								<th>Campo</th>
								<th>Valor actual</th>
								<th>Valor nuevo</th>
								<th>Estado</th>
								<th>Acción</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($validacion["validaciones"] as $fila): ?>
							<tr>
								<td>
									<?php echo $fila[substr($validacion["tabla"],3)]["nombre_completo"]; ?>
								</td>
								<?php foreach($fila as $campo => $valor): ?>
								<?php if( ($campo != "id") && ($campo != "ingreso_masivo") && ($campo != "estado_validacion") && ($campo != substr($validacion["tabla"],3)) && ($campo != "usuario_revision") && ($campo != "fecha_revision") && ($campo != "comentario_cancelacion") ): ?>
								<td>
									<?php echo $campo; ?>
								</td>
								<td>
									<?php $valor_actual = $fila[substr($validacion["tabla"],3)][$campo]; ?>
									<?php if(is_array($valor_actual)): ?>
										<?php foreach($valor_actual as $subcampo => $subvalor): ?>
											<?php if( ($subcampo != "id") && ($subcampo != "created_at") ): ?>
												<?php echo $subvalor; ?>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php else: ?>
										<?php echo $valor_actual; ?>
									<?php endif; ?>
								</td>
								<td>
									<!--<?php print_r($valor) ?>-->
									<?php if(is_array($valor)): ?>
										<?php foreach($valor as $subcampo => $subvalor): ?>
											<?php if( ($subcampo != "id") && ($subcampo != "created_at") ): ?>
												<?php echo $subvalor; ?>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php else: ?>
										<?php echo $valor; ?>
									<?php endif; ?>
								</td>
								<td>
									<div class="label label-warning" ng-if="<?php echo $fila["estado_validacion"]; ?> == 0">PENDIENTE</div>
									<div class="label label-success" ng-if="<?php echo $fila["estado_validacion"]; ?> == 1">VALIDADO</div>
									<div class="label label-danger" ng-if="<?php echo $fila["estado_validacion"]; ?> == 2">CANCELADO</div>
								</td>
								<td>
									<button class="btn btn-sm btn-success" ng-if="<?php echo $fila["estado_validacion"]; ?> < 1" ng-click="ctrl.validando(<?php echo $fila["id"].",'".$campo."','".substr($validacion["tabla"],3)."',".$fila[substr($validacion["tabla"],3)]["id"]; ?>)">Validar</button>
									<button class="btn btn-sm btn-warning" ng-if="<?php echo $fila["estado_validacion"]; ?> < 1" ng-click="ctrl.cancelando(<?php echo $fila["id"].",'".$campo."','".substr($validacion["tabla"],3)."',".$fila[substr($validacion["tabla"],3)]["id"]; ?>)">Cancelar</button>
								</td>
								<?php endif; ?>
								<?php endforeach; ?>
							</tr>
							<?php endforeach; ?>

							<!--<tr ng-repeat="validacion in ctrl.validacion.validaciones">
								<td>
									{{validacion.<?php echo substr($validacion["tabla"],3); ?>.nombre_completo}}
								</td>
								<td ng-repeat="(campo,valor) in validacion" ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != '<?php echo substr($validacion["tabla"],3); ?>')">
									{{campo}}
								</td>
							</tr>-->
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<div id="cancelarValidacion" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>¿Está seguro?</h3>

	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<textarea ng-model="ctrl.validacionCancelada.comentario" class="form-control" placeholder="Comente por qué esta cancelando esta actualización" rows="3" style="width: 100%;height: 100%;resize: none;margin-bottom: 10px;"></textarea>
		</div>
		
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-xs-6">
				<button style="width: 100%;" class="btn btn-success" ng-click="ctrl.aceptarCancelacion()" ng-disabled="!ctrl.validacionCancelada.comentario">Aceptar</button>
			</div>
			<div class="col-xs-6">
				<button style="width: 100%;" class="btn btn-danger" ng-click="ctrl.cancelarCancelacion()">Cancelar</button>
			</div>
		</div>
	</div>
</div>