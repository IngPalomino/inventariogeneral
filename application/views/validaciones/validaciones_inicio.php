<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading text-center">
					<h3>Validaciones de Ingreso Masivo</h3>
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<thead>
							<tr>
								<th>Acción</th>
								<th>Orden</th>
								<th>Administrador</th>
								<th>Fecha Ingreso</th>
								<th>Fecha Finalización</th>
								<th>Revisados</th>
								<th>No Validados</th>
								<th>Avance</th>
								<th>Estado</th>
							</tr>
						</thead>
						<tbody>
							<!--<tr>
								<td>
									<button class="btn btn-primary btn-xs">Validar</button>
									<button class="btn btn-warning btn-xs">Cancelar</button>
								</td>
								<td>
									27
								</td>
								<td>
									Miguel Angel Alcalá
								</td>
								<td>
									12/09/2016
								</td>
								<td>
									PENDIENTE
								</td>
								<td>
									55
								</td>
								<td>
									45
								</td>
								<td>
									<div class="progress">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width:23%;">
											<span style="color:lightblue"><b>23%</b></span>
										</div>
										<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width:32%;">
											<span style="color:lightblue"><b>32%</b></span>
										</div>
									</div>
								</td>
								<td>
									VALIDANDO
								</td>
							</tr>-->
							<tr ng-repeat="validacion in ctrl.validaciones_pendientes">
								<td>
									<a href="<?php echo base_url('index.php/validaciones/ingreso_masivo'); ?>/{{validacion.id}}/{{validacion.tabla}}" class="btn btn-primary btn-xs">Validar</a>
									<a class="btn btn-warning btn-xs">Cancelar</a>
								</td>
								<td>
									{{validacion.id}}
								</td>
								<td>
									{{validacion.usuario.nombres}}
								</td>
								<td>
									{{validacion.fechaCreado()}}
								</td>
								<td>
									{{validacion.fecha_finalizacion || "PENDIENTE"}}
								</td>
								<td>
									{{validacion.revisado()}}
								</td>
								<td>
									{{validacion.pendiente}}
								</td>
								<td>
									<div class="progress">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{validacion.validadosPorCiento()}}" aria-valuemin="0" aria-valuemax="100" style="width:{{validacion.validadosPorCiento()}}%;">
											<span style="color:lightblue"><b>{{validacion.validadosPorCiento() | number:1}}%</b></span>
										</div>
										<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{validacion.canceladosPorCiento()}}" aria-valuemin="0" aria-valuemax="100" style="width:{{validacion.canceladosPorCiento()}}%;">
											<span style="color:lightblue"><b>{{validacion.canceladosPorCiento() | number:1}}%</b></span>
										</div>
									</div>
								</td>
								<td>
									
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--<div class="panel-footer">
					
				</div>-->
			</div>
		</div>
	</div>
</section>