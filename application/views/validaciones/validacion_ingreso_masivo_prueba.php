<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading text-center">
					<b>POP</b>
				</div>
				<div class="panel-body" style="height:375px;overflow:scroll;">
					<table class="table table-condensed table-hover ng-cloak">
						<thead>
							<tr>
								<th>Sitio</th>
								<th>Campo</th>
								<th>Valor actual</th>
								<th>Valor nuevo</th>
								<th>Estado</th>
								<th>Acción</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="fila in ctrl.validacion.validaciones">
								<td>{{fila[ctrl.subCadenaTabla(ctrl.validacion.tabla)].nombre_completo}}</td>
								<td ng-repeat-start="(campo,valor) in fila" ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacion.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')">
									{{campo}}
								</td>
								<td ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacion.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')">
									<span ng-if="ctrl.tipode(fila[ctrl.subCadenaTabla(ctrl.validacion.tabla)][campo]) == 'object'">
										<span ng-repeat="(subcampo,subvalor) in fila[ctrl.subCadenaTabla(ctrl.validacion.tabla)][campo]" ng-if="(subcampo != 'id') && (subcampo != 'created_at')">
											{{subvalor}}
										</span>
									</span>
									<span ng-if="ctrl.tipode(fila[ctrl.subCadenaTabla(ctrl.validacion.tabla)][campo]) != 'object'">
										{{fila[ctrl.subCadenaTabla(ctrl.validacion.tabla)][campo]}}
									</span>
								</td>
								<td ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacion.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')">
									<span ng-if="ctrl.tipode(valor) == 'object'">
										<span ng-repeat="(subcampo,subvalor) in valor" ng-if="(subcampo != 'id') && (subcampo != 'created_at')">
											{{subvalor}}
										</span>
									</span>
									<span ng-if="ctrl.tipode(valor) != 'object'">
										{{valor}}
									</span>
								</td>
								<td ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacion.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')">
									<div class="label label-warning" ng-if="fila.estado_validacion == 0">PENDIENTE</div>
									<div class="label label-success" ng-if="fila.estado_validacion == 1">VALIDADO</div>
									<div class="label label-danger" ng-if="fila.estado_validacion == 2">CANCELADO</div>
								</td>
								<td ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacion.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')" ng-repeat-end>
									<button class="btn btn-sm btn-success" ng-if="fila.estado_validacion < 1" ng-click="ctrl.validando(fila.id,campo,ctrl.subCadenaTabla(ctrl.validacion.tabla),fila[ctrl.subCadenaTabla(ctrl.validacion.tabla)].id)">Validar</button>
									<button class="btn btn-sm btn-warning" ng-if="fila.estado_validacion < 1" ng-click="ctrl.cancelando(fila.id,campo,ctrl.subCadenaTabla(ctrl.validacion.tabla),fila[ctrl.subCadenaTabla(ctrl.validacion.tabla)].id)">Cancelar</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer">
					
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading text-center">
					<b>Contratos</b>
				</div>
				<div class="panel-body" style="height:375px;overflow:scroll;">
					<table class="table table-condensed table-hover ng-cloak">
						<thead>
							<tr>
								<th>Sitio</th>
								<th>Campo</th>
								<th>Valor actual</th>
								<th>Valor nuevo</th>
								<th>Estado</th>
								<th>Acción</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="fila in ctrl.validacionContratos.validaciones">
								<td>{{fila[ctrl.subCadenaTabla(ctrl.validacionContratos.tabla)].nombre_completo}}</td>
								<td ng-repeat-start="(campo,valor) in fila" ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacionContratos.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')">
									{{campo}}
								</td>
								<td ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacionContratos.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')">
									<span ng-if="ctrl.tipode(fila[ctrl.subCadenaTabla(ctrl.validacionContratos.tabla)][campo]) == 'object'">
										<span ng-repeat="(subcampo,subvalor) in fila[ctrl.subCadenaTabla(ctrl.validacionContratos.tabla)][campo]" ng-if="(subcampo != 'id') && (subcampo != 'created_at')">
											{{subvalor}}
										</span>
									</span>
									<span ng-if="ctrl.tipode(fila[ctrl.subCadenaTabla(ctrl.validacionContratos.tabla)][campo]) != 'object'">
										{{fila[ctrl.subCadenaTabla(ctrl.validacionContratos.tabla)][campo]}}
									</span>
								</td>
								<td ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacionContratos.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')">
									<span ng-if="ctrl.tipode(valor) == 'object'">
										<span ng-repeat="(subcampo,subvalor) in valor" ng-if="(subcampo != 'id') && (subcampo != 'created_at')">
											{{subvalor}}
										</span>
									</span>
									<span ng-if="ctrl.tipode(valor) != 'object'">
										{{valor}}
									</span>
								</td>
								<td ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacionContratos.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')">
									<div class="label label-warning" ng-if="fila.estado_validacion == 0">PENDIENTE</div>
									<div class="label label-success" ng-if="fila.estado_validacion == 1">VALIDADO</div>
									<div class="label label-danger" ng-if="fila.estado_validacion == 2">CANCELADO</div>
								</td>
								<td ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacionContratos.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')" ng-repeat-end>
									<button class="btn btn-sm btn-success" ng-if="fila.estado_validacion < 1" ng-click="ctrl.validando(fila.id,campo,ctrl.subCadenaTabla(ctrl.validacionContratos.tabla),fila[ctrl.subCadenaTabla(ctrl.validacionContratos.tabla)].id)">Validar</button>
									<button class="btn btn-sm btn-warning" ng-if="fila.estado_validacion < 1" ng-click="ctrl.cancelando(fila.id,campo,ctrl.subCadenaTabla(ctrl.validacionContratos.tabla),fila[ctrl.subCadenaTabla(ctrl.validacionContratos.tabla)].id)">Cancelar</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer">
					
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading text-center">
					<b>Contactos</b>
				</div>
				<div class="panel-body" style="height:375px;overflow:scroll;">
					<table class="table table-condensed table-hover ng-cloak">
						<thead>
							<tr>
								<th>Sitio</th>
								<th>Campo</th>
								<th>Valor actual</th>
								<th>Valor nuevo</th>
								<th>Estado</th>
								<th>Acción</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="fila in ctrl.validacionContactos.validaciones">
								<td>{{fila[ctrl.subCadenaTabla(ctrl.validacionContactos.tabla)].nombre_completo}}</td>
								<td ng-repeat-start="(campo,valor) in fila" ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacionContactos.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')">
									{{campo}}
								</td>
								<td ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacionContactos.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')">
									<span ng-if="ctrl.tipode(fila[ctrl.subCadenaTabla(ctrl.validacionContactos.tabla)][campo]) == 'object'">
										<span ng-repeat="(subcampo,subvalor) in fila[ctrl.subCadenaTabla(ctrl.validacionContactos.tabla)][campo]" ng-if="(subcampo != 'id') && (subcampo != 'created_at')">
											{{subvalor}}
										</span>
									</span>
									<span ng-if="ctrl.tipode(fila[ctrl.subCadenaTabla(ctrl.validacionContactos.tabla)][campo]) != 'object'">
										{{fila[ctrl.subCadenaTabla(ctrl.validacionContactos.tabla)][campo]}}
									</span>
								</td>
								<td ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacionContactos.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')">
									<span ng-if="ctrl.tipode(valor) == 'object'">
										<span ng-repeat="(subcampo,subvalor) in valor" ng-if="(subcampo != 'id') && (subcampo != 'created_at')">
											{{subvalor}}
										</span>
									</span>
									<span ng-if="ctrl.tipode(valor) != 'object'">
										{{valor}}
									</span>
								</td>
								<td ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacionContactos.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')">
									<div class="label label-warning" ng-if="fila.estado_validacion == 0">PENDIENTE</div>
									<div class="label label-success" ng-if="fila.estado_validacion == 1">VALIDADO</div>
									<div class="label label-danger" ng-if="fila.estado_validacion == 2">CANCELADO</div>
								</td>
								<td ng-if="(campo != 'id') && (campo != 'ingreso_masivo') && (campo != 'estado_validacion') && (campo != ctrl.subCadenaTabla(ctrl.validacionContactos.tabla)) && (campo != 'usuario_revision') && (campo != 'fecha_revision') && (campo != 'comentario_cancelacion')" ng-repeat-end>
									<button class="btn btn-sm btn-success" ng-if="fila.estado_validacion < 1" ng-click="ctrl.validando(fila.id,campo,ctrl.subCadenaTabla(ctrl.validacionContactos.tabla),fila[ctrl.subCadenaTabla(ctrl.validacionContactos.tabla)].id)">Validar</button>
									<button class="btn btn-sm btn-warning" ng-if="fila.estado_validacion < 1" ng-click="ctrl.cancelando(fila.id,campo,ctrl.subCadenaTabla(ctrl.validacionContactos.tabla),fila[ctrl.subCadenaTabla(ctrl.validacionContactos.tabla)].id)">Cancelar</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer">
					
				</div>
			</div>
		</div>
	</div>
</section>

<div id="cancelarValidacion" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>¿Está seguro?</h3>

	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<textarea ng-model="ctrl.validacionCancelada.comentario" class="form-control" placeholder="Comente por qué esta cancelando esta actualización" rows="3" style="width: 100%;height: 100%;resize: none;margin-bottom: 10px;"></textarea>
		</div>
		
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-xs-6">
				<button style="width: 100%;" class="btn btn-success" ng-click="ctrl.aceptarCancelacion()" ng-disabled="!ctrl.validacionCancelada.comentario">Aceptar</button>
			</div>
			<div class="col-xs-6">
				<button style="width: 100%;" class="btn btn-danger" ng-click="ctrl.cancelarCancelacion()">Cancelar</button>
			</div>
		</div>
	</div>
</div>