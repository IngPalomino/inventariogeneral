<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url("reportes/predeterminados"); ?>" class="btn boton" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-file-text-o fa-5x"></span></td></td></tr>
							<tr><td>Reportes predeterminados</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url("reportes/personalizados"); ?>" class="btn boton" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-file-text fa-5x"></span></td></td></tr>
							<tr><td>Reportes personalizados</td></tr>
						</tbody>
					</table>
				</a>
			</div>
		</div>
	</div>
</section>