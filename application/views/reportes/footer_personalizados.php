<script type="text/javascript">
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.cargando = false;

			ref.sitios = [];
			ref.info_sitio = [];
			ref.objetos_sitio = [];

			ref.config = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Info completa de un sitio',
				maxItems: 1
			};

			listarSitios = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar_sitios"); ?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sitios = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}

			ref.listarInfo = function(sitio){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("objetos/resumen_sitio"); ?>/"+sitio,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.info_sitio = data;

						ref.info_sitio.forEach(function(dato,ind,objeto){
							objeto[ind].check = false;
						});
						
						ref.cargando = false;
						ref.isAllSelectedObjetos = false;
						console.log(data);
					}
					else
					{
						ref.cargando = false;
						ref.info_sitio = [];
						ref.isAllSelectedObjetos = false;
					}
				}).error(function(err){
					console.log(err);
					ref.cargando = false;
				});
			}

			ref.disabledSitioIndividual = function()
			{
				var dis = false;
				
				ref.info_sitio.forEach(function(t){
					dis = dis || t.check;
				});
				
				return dis;
			}

			ref.selectAllObjetos = function(){
				ref.info_sitio.forEach(function(objeto){
					objeto.check = ref.isAllSelectedObjetos;
				});
			}
			
			ref.objetoSeleccionado = function(){
				ref.isAllSelectedObjetos = ref.info_sitio.every(function(objeto){return objeto.check;});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						ref.reestablecerContrasena = {};
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};

			listarSitios();
		}]);
</script>
</body>
</html>