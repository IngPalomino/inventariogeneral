<div class="container-fluid panel-menu">
	<div class="row ng-cloak">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/personalizados/sitio_individual"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Reporte individual de sitio
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12">
								<select selectize config="ctrl.config" options="ctrl.sitios" ng-model="ctrl.busquedaSitio" ng-change="ctrl.listarInfo(ctrl.busquedaSitio)" name="sitio"></select>
							</div>
						</div>
					</div>
					<div class="text-center panel-body" ng-if="ctrl.cargando">
						<i class="fa fa-spinner fa-5x fa-spin"></i>
					</div>
					<div class="panel-body" ng-if="!ctrl.cargando && ctrl.info_sitio.length">
						<div class="row">
							<div class="col-xs-12">
								<p>Seleccionar objetos a incluir en el reporte:</p>
								<p><label style="cursor:pointer;font-weight:normal;user-select:none;-moz-user-select:none;"><input type="checkbox" ng-click="ctrl.selectAllObjetos()" ng-model="ctrl.isAllSelectedObjetos"> Seleccionar todo</label></p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6" ng-repeat="objeto in ctrl.info_sitio | filter:{objeto:'!Antenas Microondas'}">
								<label style="cursor:pointer;font-weight:normal;user-select:none;-moz-user-select:none;"><input type="checkbox" ng-model="objeto.check" ng-change="ctrl.objetoSeleccionado()" name="{{objeto.objeto}}"> {{objeto.objeto}}</label>
							</div>
						</div>
					</div>
					<div class="panel-body" ng-if="!ctrl.cargando && !ctrl.info_sitio.length && ctrl.busquedaSitio">
						<div class="alert alert-danger">Este sitio no tiene objetos.</div>
					</div>
					<div class="panel-footer text-center">
						<button class="btn btn-sm btn-primary" ng-disabled="ctrl.cargando || !ctrl.busquedaSitio || (ctrl.info_sitio.length && !ctrl.disabledSitioIndividual())">Descargar</button>
					</div>
				</form>
			</div>
			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/grupos_electrogenos_almacenados"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Grupos Electrógenos Almacenados
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/grupos_electrogenos_almacenados"); ?>">Click aquí</a> para descargar toda la información de los Grupos Electrógenos Almacenados.</li>
							<!--<li>Elegir al menos una opción para habilitar la descarga.</li>-->
						</ul>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_odus_microondas.departamento && !ctrl.form_odus_microondas.provincia && !ctrl.form_odus_microondas.distrito && !ctrl.form_odus_microondas.grupo_electrogeno">Descargar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>