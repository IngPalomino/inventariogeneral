<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/pop"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Status Site / POP (Point of Presence)
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/pop"); ?>">Click aquí</a> para descargar toda la información.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_pop.departamento" ng-change="ctrl.listar_provincias(ctrl.form_pop.departamento,'form_pop'); ctrl.form_pop.provincia = ''; ctrl.form_pop.distrito = '';" ng-disabled="ctrl.form_pop.sitio">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_pop.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_pop.provincia" ng-change="ctrl.listar_distritos(ctrl.form_pop.departamento,ctrl.form_pop.provincia,'form_pop'); ctrl.form_pop.distrito = '';" ng-disabled="ctrl.form_pop.sitio">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_pop.provincias" ng-if="ctrl.form_pop.departamento">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_pop.distrito" ng-disabled="ctrl.form_pop.sitio">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_pop.distritos" ng-if="ctrl.form_pop.departamento && ctrl.form_pop.provincia">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="proyecto" ng-model="ctrl.form_pop.proyecto" ng-disabled="ctrl.form_pop.sitio">
								<option value="">Elegir proyecto</option>
								<option value="{{proyecto.id}}" ng-repeat="proyecto in ctrl.form_pop.proyectos">
									{{proyecto.proyecto}}
								</option>
							</select>
						</div>
						<!--<div class="col-xs-6 col-xs-offset-3">
							<select name="sitio" selectize config="ctrl.config" options="ctrl.form_pop.sitios" ng-model="ctrl.form_pop.sitio" ng-disabled="ctrl.form_pop.departamento || ctrl.form_pop.proyecto">
							</select>
						</div>-->
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_pop.departamento && !ctrl.form_pop.provincia && !ctrl.form_pop.distrito && !ctrl.form_pop.proyecto && !ctrl.form_pop.sitio">Descargar</button>
					</div>
				</form>
			</div>

			<div class="col-sm-6 col-xs-12">
				<form class="panel panel-primary" action="<?php echo base_url("reportes/predeterminados/antenas_gul"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Antenas GUL
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/antenas_gul"); ?>">Click aquí</a> para descargar toda la información de las antenas GUL.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_antenas_gul.departamento" ng-change="ctrl.listar_provincias(ctrl.form_antenas_gul.departamento,'form_antenas_gul')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_antenas_gul.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_antenas_gul.provincia" ng-change="ctrl.listar_distritos(ctrl.form_antenas_gul.departamento,ctrl.form_antenas_gul.provincia,'form_antenas_gul')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_antenas_gul.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_antenas_gul.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_antenas_gul.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_antena_gul" ng-model="ctrl.form_antenas_gul.tipo_antena_gul">
								<option value="">Elegir tipo de antena GUL</option>
								<option value="{{tipo_antena_gul.id}}" ng-repeat="tipo_antena_gul in ctrl.form_antenas_gul.tipo_antenas_gul">
									{{tipo_antena_gul.marca}} - {{tipo_antena_gul.modelo}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_antenas_gul.departamento && !ctrl.form_antenas_gul.provincia && !ctrl.form_antenas_gul.distrito && !ctrl.form_antenas_gul.tipo_antena_gul">Descargar</button>
					</div>
				</form>
			</div>
		</div>

		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-sm-6 col-xs-12">
				<form class="panel panel-primary" action="<?php echo base_url("reportes/predeterminados/antenas_iden"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Antenas iDEN
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/antenas_iden"); ?>">Click aquí</a> para descargar toda la información de las antenas iDEN.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_antenas_iden.departamento" ng-change="ctrl.listar_provincias(ctrl.form_antenas_iden.departamento,'form_antenas_iden')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_antenas_iden.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_antenas_iden.provincia" ng-change="ctrl.listar_distritos(ctrl.form_antenas_iden.departamento,ctrl.form_antenas_iden.provincia,'form_antenas_iden')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_antenas_iden.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_antenas_iden.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_antenas_iden.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_antena_in_building" ng-model="ctrl.form_antenas_iden.tipo_antena">
								<option value="">Elegir tipo de antena</option>
								<option value="{{tipo_antena_in_building.tipo}}" ng-repeat="tipo_antena_iden in ctrl.form_antenas_iden.tipo_antena_in_building">
									{{tipo_antena_iden.tipo}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_antenas_iden.departamento && !ctrl.form_antenas_iden.provincia && !ctrl.form_antenas_iden.distrito && !ctrl.form_antenas_iden.tipo_antena">Descargar</button>
					</div>
				</form>
			</div>

			<div class="col-sm-6 col-xs-12">
				<form class="panel panel-primary" action="<?php echo base_url("reportes/predeterminados/antenas_in_building"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Antenas In Building
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/antenas_in_building"); ?>">Click aquí</a> para descargar toda la información de las antenas In Building.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_antenas_in_building.departamento" ng-change="ctrl.listar_provincias(ctrl.form_antenas_in_building.departamento,'form_antenas_in_building')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_antenas_in_building.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_antenas_in_building.provincia" ng-change="ctrl.listar_distritos(ctrl.form_antenas_in_building.departamento,ctrl.form_antenas_in_building.provincia,'form_antenas_in_building')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_antenas_in_building.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_antenas_in_building.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_antenas_in_building.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_antena_in_building" ng-model="ctrl.form_antenas_in_building.tipo_antena">
								<option value="">Elegir tipo de antena</option>
								<option value="{{tipo_antena_in_building.tipo}}" ng-repeat="tipo_antena_in_building in ctrl.form_antenas_in_building.tipo_antena_in_building">
									{{tipo_antena_in_building.tipo}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_antenas_in_building.departamento && !ctrl.form_antenas_in_building.provincia && !ctrl.form_antenas_in_building.distrito && !ctrl.form_antenas_in_building.tipo_antena">Descargar</button>
					</div>
				</form>
			</div>
		</div>
			
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-sm-6 col-xs-12">
				<form class="panel panel-primary" action="<?php echo base_url("reportes/predeterminados/idus"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						IDUs
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/idus"); ?>">Click aquí</a> para descargar toda la información de las IDUs.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_idus.departamento" ng-change="ctrl.listar_provincias(ctrl.form_idus.departamento,'form_idus')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_idus.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_idus.provincia" ng-change="ctrl.listar_distritos(ctrl.form_idus.departamento,ctrl.form_idus.provincia,'form_idus')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_idus.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_idus.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_idus.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_idu" ng-model="ctrl.form_idus.modelo_idu">
								<option value="">Elegir tipo de idu</option>
								<option value="{{tipo_idu.id}}" ng-repeat="tipo_idu in ctrl.form_idus.tipo_idu.modelos">
									{{tipo_idu.modelo}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_idus.departamento && !ctrl.form_idus.provincia && !ctrl.form_idus.distrito && !ctrl.form_idus.modelo_idu">Descargar</button>
					</div>
				</form>
			</div>

			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/enlaces"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Enlaces Microondas
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/enlaces"); ?>">Click aquí</a> para descargar toda la información de los Enlaces.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_enlaces.departamento" ng-change="ctrl.listar_provincias(ctrl.form_enlaces.departamento,'form_enlaces')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_enlaces.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_enlaces.provincia" ng-change="ctrl.listar_distritos(ctrl.form_enlaces.departamento,ctrl.form_enlaces.provincia,'form_enlaces')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_enlaces.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_enlaces.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_enlaces.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_enlace" ng-model="ctrl.form_enlaces.enlace">
								<option value="">Elegir tipo enlace</option>
								<option value="{{concesionaria_suministro.id}}" ng-repeat="concesionaria_suministro in ctrl.form_enlaces.concesionarias_suministro">
									{{concesionaria_suministro.concesionaria_suministro}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_enlaces.departamento && !ctrl.form_enlaces.provincia && !ctrl.form_enlaces.distrito && !ctrl.form_enlaces.concesionaria_suministro">Descargar</button>
					</div>
				</form>
			</div>
		</div>
		
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/suministros"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Suministros
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/suministros"); ?>">Click aquí</a> para descargar toda la información de los Suministros.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_suministros.departamento" ng-change="ctrl.listar_provincias(ctrl.form_suministros.departamento,'form_suministros')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_suministros.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_suministros.provincia" ng-change="ctrl.listar_distritos(ctrl.form_suministros.departamento,ctrl.form_suministros.provincia,'form_suministros')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_suministros.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_suministros.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_suministros.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="concesionaria_suministro" ng-model="ctrl.form_suministros.concesionaria_suministro">
								<option value="">Elegir concesionaria de suministro</option>
								<option value="{{concesionaria_suministro.id}}" ng-repeat="concesionaria_suministro in ctrl.form_suministros.concesionarias_suministro">
									{{concesionaria_suministro.concesionaria_suministro}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_suministros.departamento && !ctrl.form_suministros.provincia && !ctrl.form_suministros.distrito && !ctrl.form_suministros.concesionaria_suministro">Descargar</button>
					</div>
				</form>
			</div>

			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/tanques_combustible"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Tanques de combustible
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/tanques_combustible"); ?>">Click aquí</a> para descargar toda la información de los Tanques de combustible.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_tanques_combustible.departamento" ng-change="ctrl.listar_provincias(ctrl.form_tanques_combustible.departamento,'form_tanques_combustible')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_tanques_combustible.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_tanques_combustible.provincia" ng-change="ctrl.listar_distritos(ctrl.form_tanques_combustible.departamento,ctrl.form_tanques_combustible.provincia,'form_tanques_combustible')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_tanques_combustible.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_tanques_combustible.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_tanques_combustible.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_tanque_combustible" ng-model="ctrl.form_tanques_combustible.tipo_tanque_combustible">
								<option value="">Elegir tipo de tanque</option>
								<option value="1">Tanque Interno</option>
								<option value="2">Tanque Externo</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_tanques_combustible.departamento && !ctrl.form_tanques_combustible.provincia && !ctrl.form_tanques_combustible.distrito && !ctrl.form_tanques_combustible.tipo_tanque_combustible">Descargar</button>
					</div>
				</form>
			</div>
		</div>
			
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/lineas_electricas"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Líneas Eléctricas
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/lineas_electricas"); ?>">Click aquí</a> para descargar toda la información de las Líneas Eléctricas.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_lineas_electricas.departamento" ng-change="ctrl.listar_provincias(ctrl.form_lineas_electricas.departamento,'form_lineas_electricas')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_lineas_electricas.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_lineas_electricas.provincia" ng-change="ctrl.listar_distritos(ctrl.form_lineas_electricas.departamento,ctrl.form_lineas_electricas.provincia,'form_lineas_electricas')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_lineas_electricas.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_lineas_electricas.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_lineas_electricas.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="conexion_linea_electrica" ng-model="ctrl.form_lineas_electricas.linea_electrica">
								<option value="">Elegir tipo de Línea Eléctrica</option>
								<option value="{{conexion_linea_electrica.id}}" ng-repeat="conexion_linea_electrica in ctrl.form_lineas_electricas.conexion_linea_electrica">
									{{conexion_linea_electrica.conexion_linea_electrica}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_lineas_electricas.departamento && !ctrl.form_lineas_electricas.provincia && !ctrl.form_lineas_electricas.distrito && !ctrl.form_lineas_electricas.linea_electrica">Descargar</button>
					</div>
				</form>
			</div>

			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/rectificadores"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Rectificadores
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/rectificadores"); ?>">Click aquí</a> para descargar toda la información de los Rectificadores. </li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_rectificadores.departamento" ng-change="ctrl.listar_provincias(ctrl.form_rectificadores.departamento,'form_rectificadores')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_rectificadores.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_rectificadores.provincia" ng-change="ctrl.listar_distritos(ctrl.form_rectificadores.departamento,ctrl.form_rectificadores.provincia,'form_rectificadores')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_rectificadores.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_rectificadores.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_rectificadores.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_controlador_rectif" ng-model="ctrl.form_rectificadores.controlador_rectif">
								<option value="">Elegir marca de rectificador</option>
								<option value="{{tipo_controlador_rectif.marca}}" ng-repeat="tipo_controlador_rectif in ctrl.form_rectificadores.tipo_controlador_rectif.marcas">
									{{tipo_controlador_rectif.marca}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_rectificadores.departamento && !ctrl.form_rectificadores.provincia && !ctrl.form_rectificadores.distrito && !ctrl.form_rectificadores.controlador_rectif">Descargar</button>
					</div>
				</form>
			</div>
		</div>
		
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/aires_acondicionados"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Aires Acondicionados
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/aires_acondicionados"); ?>">Click aquí</a> para descargar toda la información de los Aires Acondicionados.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_aires_acondicionados.departamento" ng-change="ctrl.listar_provincias(ctrl.form_aires_acondicionados.departamento,'form_aires_acondicionados')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_aires_acondicionados.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_aires_acondicionados.provincia" ng-change="ctrl.listar_distritos(ctrl.form_aires_acondicionados.departamento,ctrl.form_aires_acondicionados.provincia,'form_aires_acondicionados')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_aires_acondicionados.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_aires_acondicionados.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_aires_acondicionados.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_aires_acondicionados" ng-model="ctrl.form_aires_acondicionados.tipo_equipo">
								<option value="">Elegir tipo de equipo</option>
								<option value="{{tipo_equipo.tipo_equipo}}" ng-repeat="tipo_equipo in ctrl.form_aires_acondicionados.tipo_aires_acondicionados">
									{{tipo_equipo.tipo_equipo}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_aires_acondicionados.departamento && !ctrl.form_aires_acondicionados.provincia && !ctrl.form_aires_acondicionados.distrito && !ctrl.form_aires_acondicionados.tipo_equipo">Descargar</button>
					</div>
				</form>
			</div>

			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/bancos_baterias"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Bancos de Baterías
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/bancos_baterias"); ?>">Click aquí</a> para descargar toda la información de los Bancos de Baterías.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
							<!--<li>Incluir información de celdas <input type="checkbox" name="celdas_bancos" ng-model="ctrl.form_bancos_baterias.celdas"/></li>
							<li>
								<select class="form-control" name="seccion" ng-model="ctrl.form_bancos_baterias.seccion">
									<option value="">Elegir sección</option>
									<option></option>
								</select>
							</li>-->
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_bancos_baterias.departamento" ng-change="ctrl.listar_provincias(ctrl.form_bancos_baterias.departamento,'form_bancos_baterias')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_bancos_baterias.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_bancos_baterias.provincia" ng-change="ctrl.listar_distritos(ctrl.form_bancos_baterias.departamento,ctrl.form_bancos_baterias.provincia,'form_bancos_baterias')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_bancos_baterias.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_bancos_baterias.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_bancos_baterias.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_bancos_baterias" ng-model="ctrl.form_bancos_baterias.bancos_baterias">
								<option value="">Elegir marca de banco</option>
								<option value="{{tipo_bancos_baterias.marca}}" ng-repeat="tipo_bancos_baterias in ctrl.form_bancos_baterias.tipo_bancos_baterias.marcas">
									{{tipo_bancos_baterias.marca}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_bancos_baterias.departamento && !ctrl.form_bancos_baterias.provincia && !ctrl.form_bancos_baterias.distrito && !ctrl.form_bancos_baterias.bancos_baterias">Descargar</button>
					</div>
				</form>
			</div>
		</div>
		
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/grupos_electrogenos"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Grupos Electrógenos
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/grupos_electrogenos"); ?>">Click aquí</a> para descargar toda la información de los Grupos Electrógenos.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_grupos_electrogenos.departamento" ng-change="ctrl.listar_provincias(ctrl.form_grupos_electrogenos.departamento,'form_grupos_electrogenos')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_grupos_electrogenos.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_grupos_electrogenos.provincia" ng-change="ctrl.listar_distritos(ctrl.form_grupos_electrogenos.departamento,ctrl.form_grupos_electrogenos.provincia,'form_grupos_electrogenos')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_grupos_electrogenos.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_grupos_electrogenos.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_grupos_electrogenos.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_grupo_electrogeno" ng-model="ctrl.form_grupos_electrogenos.grupo_electrogeno">
								<option value="">Elegir marca de grupo electrógeno</option>
								<option value="{{tipo_grupo_electrogeno.marca}}" ng-repeat="tipo_grupo_electrogeno in ctrl.form_grupos_electrogenos.tipo_grupo_electrogeno">
									{{tipo_grupo_electrogeno.marca}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_grupos_electrogenos.departamento && !ctrl.form_grupos_electrogenos.provincia && !ctrl.form_grupos_electrogenos.distrito && !ctrl.form_grupos_electrogenos.grupo_electrogeno">Descargar</button>
					</div>
				</form>
			</div>

			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/antenas_nodo_b"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Antenas Nodo B
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/antenas_nodo_b"); ?>">Click aquí</a> para descargar toda la información de las Antenas Nodo B.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_antenas_nodo_b.departamento" ng-change="ctrl.listar_provincias(ctrl.form_antenas_nodo_b.departamento,'form_antenas_nodo_b')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_antenas_nodo_b.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_antenas_nodo_b.provincia" ng-change="ctrl.listar_distritos(ctrl.form_antenas_nodo_b.departamento,ctrl.form_antenas_nodo_b.provincia,'form_antenas_nodo_b')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_antenas_nodo_b.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_antenas_nodo_b.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_antenas_nodo_b.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_bancos_baterias" ng-model="ctrl.form_antenas_nodo_b.bancos_baterias">
								<option value="">Elegir</option>
								<option value="{{tipo_bancos_baterias.marca}}" ng-repeat="tipo_bancos_baterias in ctrl.form_antenas_nodo_b.tipo_bancos_baterias.marcas">
									{{tipo_bancos_baterias.marca}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_antenas_nodo_b.departamento && !ctrl.form_antenas_nodo_b.provincia && !ctrl.form_antenas_nodo_b.distrito && !ctrl.form_antenas_nodo_b.bancos_baterias">Descargar</button>
					</div>
				</form>
			</div>
		</div>
		
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/tarjetas_nodo_b"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Tarjetas Nodo B
					</div>
					<div class="panel-body">
						<ul>
							<li><button type="button" class="btn btn-warning btn-sm" ng-click="ctrl.tarjetas_nodo_b()">Click aquí</button>
							<!--<a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/tarjetas_nodo_b"); ?>">Click aquí</a>--> para descargar toda la información de las Antenas Microondas.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_tarjetas_nodo_b.departamento" ng-change="ctrl.listar_provincias(ctrl.form_tarjetas_nodo_b.departamento,'form_tarjetas_nodo_b')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_tarjetas_nodo_b.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_tarjetas_nodo_b.provincia" ng-change="ctrl.listar_distritos(ctrl.form_tarjetas_nodo_b.departamento,ctrl.form_tarjetas_nodo_b.provincia,'form_tarjetas_nodo_b')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_tarjetas_nodo_b.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_tarjetas_nodo_b.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_tarjetas_nodo_b.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_bancos_baterias" ng-model="ctrl.form_tarjetas_nodo_b.bancos_baterias">
								<option value="">Elegir</option>
								<option value="{{tipo_bancos_baterias.marca}}" ng-repeat="tipo_bancos_baterias in ctrl.form_tarjetas_nodo_b.tipo_bancos_baterias.marcas">
									{{tipo_bancos_baterias.marca}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_tarjetas_nodo_b.departamento && !ctrl.form_tarjetas_nodo_b.provincia && !ctrl.form_tarjetas_nodo_b.distrito && !ctrl.form_tarjetas_nodo_b.bancos_baterias">Descargar</button>
					</div>
				</form>
			</div>

			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/gabinetes_nodo_b"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Gabinetes Nodo B
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/gabinetes_nodo_b"); ?>">Click aquí</a> para descargar toda la información de los Gabinetes Nodo B.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_gabinetes_nodo_b.departamento" ng-change="ctrl.listar_provincias(ctrl.form_gabinetes_nodo_b.departamento,'form_gabinetes_nodo_b')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_gabinetes_nodo_b.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_gabinetes_nodo_b.provincia" ng-change="ctrl.listar_distritos(ctrl.form_gabinetes_nodo_b.departamento,ctrl.form_gabinetes_nodo_b.provincia,'form_gabinetes_nodo_b')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_gabinetes_nodo_b.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_gabinetes_nodo_b.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_gabinetes_nodo_b.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_grupo_electrogeno" ng-model="ctrl.form_gabinetes_nodo_b.grupo_electrogeno">
								<option value="">Elegir</option>
								<option value="{{tipo_grupo_electrogeno.marca}}" ng-repeat="tipo_grupo_electrogeno in ctrl.form_gabinetes_nodo_b.tipo_grupo_electrogeno">
									{{tipo_grupo_electrogeno.marca}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_gabinetes_nodo_b.departamento && !ctrl.form_gabinetes_nodo_b.provincia && !ctrl.form_gabinetes_nodo_b.distrito && !ctrl.form_gabinetes_nodo_b.grupo_electrogeno">Descargar</button>
					</div>
				</form>
			</div>
		</div>
		
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/tarjetas_microondas"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Tarjetas Microondas
					</div>
					<div class="panel-body">
						<ul>
							<li><button type="button" class="btn btn-warning btn-sm" ng-click="ctrl.tarjetas_microondas()">Click aquí</button>
							<!--<a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/tarjetas_microondas"); ?>">Click aquí</a>--> para descargar toda la información de las Antenas Microondas.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_tarjetas_microondas.departamento" ng-change="ctrl.listar_provincias(ctrl.form_tarjetas_microondas.departamento,'form_tarjetas_microondas')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_tarjetas_microondas.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_tarjetas_microondas.provincia" ng-change="ctrl.listar_distritos(ctrl.form_tarjetas_microondas.departamento,ctrl.form_tarjetas_microondas.provincia,'form_tarjetas_microondas')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_tarjetas_microondas.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_tarjetas_microondas.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_tarjetas_microondas.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_bancos_baterias" ng-model="ctrl.form_tarjetas_microondas.bancos_baterias">
								<option value="">Elegir</option>
								<option value="{{tipo_bancos_baterias.marca}}" ng-repeat="tipo_bancos_baterias in ctrl.form_tarjetas_microondas.tipo_bancos_baterias.marcas">
									{{tipo_bancos_baterias.marca}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_tarjetas_microondas.departamento && !ctrl.form_tarjetas_microondas.provincia && !ctrl.form_tarjetas_microondas.distrito && !ctrl.form_tarjetas_microondas.bancos_baterias">Descargar</button>
					</div>
				</form>
			</div>

			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/odus_microondas"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						ODUs Microondas
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/odus_microondas"); ?>">Click aquí</a> para descargar toda la información de las ODUs Microondas.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_odus_microondas.departamento" ng-change="ctrl.listar_provincias(ctrl.form_odus_microondas.departamento,'form_odus_microondas')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_odus_microondas.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_odus_microondas.provincia" ng-change="ctrl.listar_distritos(ctrl.form_odus_microondas.departamento,ctrl.form_odus_microondas.provincia,'form_odus_microondas')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_odus_microondas.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_odus_microondas.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_odus_microondas.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_grupo_electrogeno" ng-model="ctrl.form_odus_microondas.grupo_electrogeno">
								<option value="">Elegir</option>
								<option value="{{tipo_grupo_electrogeno.marca}}" ng-repeat="tipo_grupo_electrogeno in ctrl.form_odus_microondas.tipo_grupo_electrogeno">
									{{tipo_grupo_electrogeno.marca}}
								</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_odus_microondas.departamento && !ctrl.form_odus_microondas.provincia && !ctrl.form_odus_microondas.distrito && !ctrl.form_odus_microondas.grupo_electrogeno">Descargar</button>
					</div>
				</form>
			</div>
		</div>

		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/wimax"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Wimax
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/wimax"); ?>">Click aquí</a> para descargar toda la información de Wimax.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_wimax.departamento" ng-change="ctrl.listar_provincias(ctrl.form_wimax.departamento,'form_wimax')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_wimax.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_wimax.provincia" ng-change="ctrl.listar_distritos(ctrl.form_wimax.departamento,ctrl.form_wimax.provincia,'form_wimax')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_wimax.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_wimax.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_wimax.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_wimax" ng-model="ctrl.form_wimax.tipo_wimax">
								<option value="">Elegir tecnología</option>
								<option value="2.5">Wimax 2.5</option>
								<option value="3.5">Wimax 3.5</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_wimax.departamento && !ctrl.form_wimax.provincia && !ctrl.form_wimax.distrito && !ctrl.form_wimax.tipo_wimax">Descargar</button>
					</div>
				</form>
			</div>

			<div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/iden"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						iDEN
					</div>
					<div class="panel-body">
						<ul>
							<li><a class="btn btn-warning btn-sm" href="<?php echo base_url("reportes/predeterminados/iden"); ?>">Click aquí</a> para descargar toda la información de iDEN.</li>
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="col-xs-6">
							<select class="form-control" name="departamento" ng-model="ctrl.form_iden.departamento" ng-change="ctrl.listar_provincias(ctrl.form_iden.departamento,'form_iden')">
								<option value="">Elegir departamento</option>
								<option value="{{departamento.departamento}}" ng-repeat="departamento in ctrl.form_iden.departamentos">
									{{departamento.departamento}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="provincia" ng-model="ctrl.form_iden.provincia" ng-change="ctrl.listar_distritos(ctrl.form_iden.departamento,ctrl.form_iden.provincia,'form_iden')">
								<option value="">Elegir provincia</option>
								<option value="{{provincia.provincia}}" ng-repeat="provincia in ctrl.form_iden.provincias">
									{{provincia.provincia}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="distrito" ng-model="ctrl.form_iden.distrito">
								<option value="">Elegir distrito</option>
								<option value="{{distrito.distrito}}" ng-repeat="distrito in ctrl.form_iden.distritos">
									{{distrito.distrito}}
								</option>
							</select>
						</div>
						<div class="col-xs-6">
							<select class="form-control" name="tipo_iden" ng-model="ctrl.form_iden.tipo_iden">
								<option value="">Elegir</option>
							</select>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.form_iden.departamento && !ctrl.form_iden.provincia && !ctrl.form_iden.distrito && !ctrl.form_iden.tipo_iden">Descargar</button>
					</div>
				</form>
			</div>

        <div class="col-sm-6 col-xs-12">
				<form action="<?php echo base_url("reportes/predeterminados/bodegas"); ?>" method="POST" enctype="application/x-www-form-urlencoded" target="_self" class="panel panel-primary" novalidate>
					<div class="panel-heading text-center">
						Almacenes
					</div>
					
					<div class="panel-body">
                         <ul>							
							<li>Elegir al menos una opción para habilitar la descarga.</li>
						</ul>

						<div class="col-xs-6">
					<div class="input-group">

					 <selectize id="ng_all_almacen" style="width:200px;" config="ctrl.configAlmacenes" options="ctrl.almacenes" ng-model="ctrl.busqueda.almacen" ng-change="ctrl.searched = false"></selectize>	
					 <input type="hidden"  id="almacen" name="almacen">
					<!--  
					 <!-- <div class="input-group-btn">
						<button class="btn btn-default" ng-click="ctrl.buscarAlmacenes(ctrl.busqueda.almacen)" ng-disabled="!ctrl.busqueda.almacen || ctrl.loading">
							<i class="fa fa-search" ng-show="!ctrl.loading"></i>
							<i class="fa fa-spinner fa-spin" ng-show="ctrl.loading"></i>
						</button> 
					</div>--> 

				   </div>


						</div>
						<div class="col-xs-6">
							
						</div>
						<div class="col-xs-6">
							
						</div>
						<div class="col-xs-6">
							
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-sm btn-primary" ng-disabled="!ctrl.busqueda.almacen">Descargar</button>
					</div>
				</form>
			</div>






		</div>
	</div>
</section>

<div ng-repeat="ventanita in ctrl.ventanitas" ng-if="ctrl.ventanitas.length > 0">
	<iframe width="0" height="1" ng-src="{{ventanita.ind}}" ng-if="ventanita.ind != undefined">
	</iframe>
</div>

<div ng-repeat="ventanita in ctrl.partes_reportes" ng-if="ctrl.partes_reportes.length">
	<iframe width="0" height="1" ng-src="{{ventanita.ind}}" ng-if="ventanita.ind != undefined">
	</iframe>
</div>