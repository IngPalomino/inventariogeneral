<script type="text/javascript">
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.ventanitas = [];
			ref.partes_reportes = [];
			
			ref.config = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Info completa de un sitio',
				maxItems: 1
			};

			ref.form_pop = {
				departamentos: [],
				provincias: [],
				departamentos: [],
				proyectos: [],
				sitios: []
			};

			ref.form_antenas_gul = {
				departamentos: [],
				provincias: [],
				departamentos: [],
				tipo_antenas_gul: []
			};

			ref.form_antenas_iden = {
				departamentos: [],
				provincias: [],
				departamentos: [],
				tipo_antenas_iden: []
			};
			
			ref.form_antenas_in_building = {
				departamentos: [],
				provincias: [],
				departamentos: [],
				tipo_antena_in_building: []
			};
			
			ref.form_idus = {
				departamentos: [],
				provincias: [],
				departamentos: [],
				tipo_idu: []
			};
			
			ref.form_enlaces = {
				departamentos: [],
				provincias: [],
				departamentos: []
			};

			ref.form_suministros = {
				departamentos: [],
				provincias: [],
				departamentos: [],
				concesionarias_suministro: []
			};

			ref.form_tanques_combustible = {
				departamentos: [],
				provincias: [],
				departamentos: []
			};
			
			ref.form_lineas_electricas = {
				departamentos: [],
				provincias: [],
				departamentos: [],
				conexion_linea_electrica: []
			};
			
			ref.form_rectificadores = {
				departamentos: [],
				provincias: [],
				departamentos: [],
				tipo_controlador_rectif: []
			};
			
			ref.form_aires_acondicionados = {
				departamentos: [],
				provincias: [],
				departamentos: [],
				tipo_aires_acondicionados: []
			};
			
			ref.form_bancos_baterias = {
				departamentos: [],
				provincias: [],
				departamentos: [],
				tipo_bancos_baterias: []
			};
			
			ref.form_grupos_electrogenos = {
				departamentos: [],
				provincias: [],
				departamentos: [],
				tipo_grupo_electrogeno: []
			};
			
			ref.form_antenas_nodo_b = {
				departamentos: [],
				provincias: [],
				departamentos: []
			};
			
			ref.form_tarjetas_nodo_b = {
				departamentos: [],
				provincias: [],
				departamentos: []
			};
			
			ref.form_gabinetes_nodo_b = {
				departamentos: [],
				provincias: [],
				departamentos: []
			};
			
			ref.form_tarjetas_microondas = {
				departamentos: [],
				provincias: [],
				departamentos: []
			};
			
			ref.form_odus_microondas = {
				departamentos: [],
				provincias: [],
				departamentos: []
			};

			ref.form_wimax = {
				departamentos: [],
				provincias: [],
				departamentos: []
			};

			ref.form_iden = {
				departamentos: [],
				provincias: [],
				departamentos: []
			};		
  //almacen
            ref.loading = false;
			ref.searched = false;
			ref.almacenes = [];
			ref.configAlmacenes = {
				valueField: 'id',
				labelField: 'almacen',
				searchField: 'almacen',
				delimiter: '|',
				placeholder: 'Buscar almacen',
				maxItems: 1,
				inputClass: "form-control selectize-input",
				dropdownParent: "body",				
				sortField: [
					{field: 'almacen', direction: 'asc'}
				]
			};
			ref.configObjetos = {
				valueField: 'objeto',
				labelField: 'nombre',
				searchField: 'nombre',
				delimiter: '|',
				placeholder: 'Buscar objeto',
				maxItems: 1,
				inputClass: "form-control selectize-input",
				dropdownParent: "body"
			};
			ref.busqueda = {
				almacen: "",
				sitio: "",
				objeto: ""
			};
			ref.objetos = [];
			ref.elementos_busqueda = [];
//almacen
			listar_departamentos = function(form){
				$http({
					url:"<?php echo base_url("zonas/listar/departamentos");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref[form].departamentos = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}

			ref.listar_provincias = function(departamento,form){
				$http({
					url:"<?php echo base_url("zonas/listar/provincias");?>"+"/"+encodeURI(departamento),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref[form].provincias = data
						ref[form].distritos = [];
					}
				}).error(function(err){

				});
			}

			ref.listar_distritos = function(departamento,provincia,form){
				$http({
					url:"<?php echo base_url("zonas/listar/distritos");?>"+"/"+encodeURI(departamento)+"/"+encodeURI(provincia),
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref[form].distritos = data
					}
				}).error(function(err){

				});
			}

			listar_proyectos = function(form){
				$http({
					url:"<?php echo base_url("proyecto/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref[form].proyectos = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}
			
			listar_sitios = function(form){
				$http({
					url:"<?php echo base_url("almacenes/listar_sitios");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref[form].sitios = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_antenas_gul = function(form){
				$http({
					url:"<?php echo base_url("tipo_antena_gul/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref[form].tipo_antenas_gul = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}
			
			listar_tipo_antena_in_building = function(form){
				$http({
					url:"<?php echo base_url("tipo_antena_in_building/listar_tipo_antena");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref[form].tipo_antena_in_building = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}
			
			listar_tipo_idu = function(form){
				$http({
					url:"<?php echo base_url("tipo_idu/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.modelos.length)
					{
						ref[form].tipo_idu = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}

			listar_concesionarias_suministro = function(form){
				$http({
					url:"<?php echo base_url("concesionaria_suministro/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref[form].concesionarias_suministro = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}
			
			listar_conexion_linea_electrica = function(form){
				$http({
					url:"<?php echo base_url("conexion_linea_electrica/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref[form].conexion_linea_electrica = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}
			
			listar_tipo_controlador_rectif = function(form){
				$http({
					url:"<?php echo base_url("tipo_controlador_rectif/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref[form].tipo_controlador_rectif = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}
			
			listar_tipo_aires_acondicionados = function(form){
				$http({
					url:"<?php echo base_url("tipo_aires_acondicionados/listar_tipos_equipo");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref[form].tipo_aires_acondicionados = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}
			
			listar_tipo_bancos_baterias = function(form){
				$http({
					url:"<?php echo base_url("tipo_bancos_baterias/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref[form].tipo_bancos_baterias = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}
			
			listar_tipo_grupo_electrogeno = function(form){
				$http({
					url:"<?php echo base_url("tipo_grupo_electrogeno/listar");?>",
					method:"GET"
				}).success(function(data){
					data = data.marcas;
					if(data.length)
					{
						ref[form].tipo_grupo_electrogeno = data;
					}
				}).error(function(err){
					console.log(err);
				});
			}

		//almacen
            listarObjetos = function(){
				$http({
					url:"<?php echo base_url("objetos_bodegas/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.objetos = data;
					}
				}).error(function(err){

				});
			}



			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.almacenes = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.almacenes = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
            
            $("#ng_all_almacen").change(function(){
            var almacen = $("#ng_all_almacen").val();
            $("#almacen").val(almacen);
	        });



			ref.buscarAlmacenes = function(almacen){
				// ref.loading = true;
				// ref.busqueda.sitio = "";
				// ref.busqueda.objeto = "";

				$http({
					url:"<?php echo base_url("predeterminados/bodegas");?>",
					method:"POST",
					data:"almacen="+almacen,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					
				}).error(function(err){
					ref.loading = false;
				});
			}

			ref.buscarObjetos = function(objeto){
				ref.loading = true;
				ref.busqueda.almacen = "";
				ref.busqueda.sitio = "";

				$http({
					url:"<?php echo base_url("objetos_bodegas/busqueda");?>",
					method:"POST",
					data:"objeto="+objeto,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					ref.loading = false;
					ref.searched = true;
					if(data.length)
					{
						ref.elementos_busqueda = data;
						ref.datainfo = JSON.parse(ref.elementos_busqueda[0].info);
					}
					else
					{
						ref.elementos_busqueda = [];
					}
				}).error(function(err){
					ref.loading = false;
				});
			}
         //almacen			
			ref.tarjetas_microondas = function(){
				ref.ventanitas = [
					{ind:"<?php echo base_url("reportes/predeterminados/tarjetas_microondas/0"); ?>"},
					{ind:"<?php echo base_url("reportes/predeterminados/tarjetas_microondas/1"); ?>"},
					{ind:"<?php echo base_url("reportes/predeterminados/tarjetas_microondas/2"); ?>"}
				];
			}
			
			ref.tarjetas_nodo_b = function(){
				ref.partes_reportes = [
					{ind:"<?php echo base_url("reportes/predeterminados/tarjetas_nodo_b/0"); ?>"},
					{ind:"<?php echo base_url("reportes/predeterminados/tarjetas_nodo_b/1"); ?>"},
					{ind:"<?php echo base_url("reportes/predeterminados/tarjetas_nodo_b/2"); ?>"},
					{ind:"<?php echo base_url("reportes/predeterminados/tarjetas_nodo_b/3"); ?>"},
					{ind:"<?php echo base_url("reportes/predeterminados/tarjetas_nodo_b/4"); ?>"},
					{ind:"<?php echo base_url("reportes/predeterminados/tarjetas_nodo_b/5"); ?>"},
					{ind:"<?php echo base_url("reportes/predeterminados/tarjetas_nodo_b/6"); ?>"}
				];
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						ref.reestablecerContrasena = {};
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};

			listar_departamentos("form_pop");
			listar_proyectos("form_pop");
			listar_sitios("form_pop");

			listar_departamentos("form_antenas_gul");
			listar_antenas_gul("form_antenas_gul");

			listar_departamentos("form_antenas_iden");
			
			listar_departamentos("form_antenas_in_building");
			listar_tipo_antena_in_building("form_antenas_in_building");
			
			listar_departamentos("form_idus");
			listar_tipo_idu("form_idus");

			listar_departamentos("form_suministros");
			listar_concesionarias_suministro("form_suministros");

			listar_departamentos("form_tanques_combustible");
			
			listar_departamentos("form_lineas_electricas");
			listar_conexion_linea_electrica("form_lineas_electricas");
			
			listar_departamentos("form_rectificadores");
			listar_tipo_controlador_rectif("form_rectificadores");
			
			listar_departamentos("form_aires_acondicionados");
			listar_tipo_aires_acondicionados("form_aires_acondicionados");
			
			listar_departamentos("form_bancos_baterias");
			listar_tipo_bancos_baterias("form_bancos_baterias");
			
			listar_departamentos("form_grupos_electrogenos");
			listar_tipo_grupo_electrogeno("form_grupos_electrogenos");
			
			listar_departamentos("form_enlaces");
			
			listar_departamentos("form_antenas_nodo_b");
			
			listar_departamentos("form_tarjetas_nodo_b");
			
			listar_departamentos("form_gabinetes_nodo_b");
			
			listar_departamentos("form_tarjetas_microondas");
			
			listar_departamentos("form_odus_microondas");

			listar_departamentos("form_wimax");

			listar_departamentos("form_iden");
			listarAlmacenes();
			listarObjetos();
		}]);
</script>
</body>
</html>