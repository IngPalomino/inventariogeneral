<!DOCTYPE html>
<html ng-app="app">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Inventario General">
    <meta name="author" content="Alex Nima Pérez">

	<title><?php echo (isset($title) && ! empty($title))? $title.' - ' : ''; ?>Inventario General</title>
	
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/images/logo-transp.png'); ?>"/>

	<?php foreach ($css as $style): ?>
	<link href="<?php echo base_url("assets/styles/{$style}.css"); ?>" rel="stylesheet">
	<?php endforeach; ?>
</head>
<body class="ng-cloak" ng-controller="controlador as ctrl">
	<nav class="navbar navbar-default" ng-controller="sharedController as ctrl">
		<?php get_navbar(); ?>
	</nav>

	<?php
		if (isset($site_view) && ! empty($site_view))
		{
			get_navbar_site($site_view);
		}
	?>

	<?php get_content($content); ?>

	<?php if ( ! logged_in()): ?>
	<div class="modal fade" id="forgotPassword" role="dialog" ng-controller="sharedController as ctrl">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="modal-title">Restablecer Contraseña</h3>
				</div>
				<div class="modal-body">
					<p class="alert alert-info">Ingrese su correo electrónico para restablecer su contraseña, recibirá un link con instrucciones.</p>
					<div class="form-group">
						<label>Correo Electrónico:</label>
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-user"></i></div>
							<input type="text" class="form-control" ng-model="ctrl.forgotPasswordForm.correo">
						</div>
					</div>
					<div class="form-group">
						<div class="alert alert-danger" ng-show="ctrl.forgotPasswordMessages.error">{{ctrl.forgotPasswordMessages.error}}</div>
						<div class="alert alert-success" ng-show="ctrl.forgotPasswordMessages.success">{{ctrl.forgotPasswordMessages.success}}</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button class="btn btn-primary" ng-click="ctrl.enviarForgotPassword()" ng-disabled="!ctrl.forgotPasswordForm.correo">Enviar</button>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<?php foreach ($js as $javascript): ?>
	<script type="text/javascript" src="<?php echo base_url("assets/javascript/{$javascript}.js"); ?>"></script>
	<?php endforeach; ?>
	<?php foreach ($modules as $module): ?>
	<script type="text/javascript" src="<?php echo base_url("modules/{$module}.js"); ?>"></script>
	<?php endforeach; ?>

	<script type="text/javascript">
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>

	<script type="text/javascript">
		(function() {
			'use strict';

			angular
				.module('app')
				.factory('phpService', phpService);

			function phpService() {
				var service = {
					base_url: base_url
				};

				return service;

				function base_url(segment = '') {
					return '<?php echo base_url(); ?>' + segment;
				}
			}
		})();
	</script>
</body>
</html>