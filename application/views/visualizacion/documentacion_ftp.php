<br>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Documentación
				</div>
				<div class="panel-body ng-cloak">
				
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
					</div>
					<div id="docs">
					</div>
					
					<div class="alert alert-danger" ng-if="!ctrl.cargando && !ctrl.procesando && !ctrl.info">
						<i class="fa fa-exclamation-triangle"></i> No existe un site folder.
					</div>
				</div>
			</div>
		</div>
	</div>
</div>