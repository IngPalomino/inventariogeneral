<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Equipments Inventory
				</div>
				<div class="panel-body">
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
					</div>
					<table class="table table-condensed table-bordered wimax" ng-if="!ctrl.cargando && ctrl.wimax.length">
						<thead>
							<tr class="success">
								<th>BTS Model</th>
								<th>WiMax Code</th>
								<th>BTS Name</th>
								<th>BTS Number</th>
								<th>IP Address</th>
								<th>Entity Type</th>
								<th>Entity Count</th>
							</tr>
						</thead>
						<tbody>
							<tr class="warning" ng-repeat="wimax in ctrl.wimax">
								<td>{{wimax.bts_model}}</td>
								<td>{{wimax.wimax_code}}</td>
								<td>{{wimax.bts_name}}</td>
								<td>{{wimax.bts_number}}</td>
								<td>{{wimax.ip_address}}</td>
								<td>{{wimax.entity_type}}</td>
								<td>{{wimax.entity_count}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Detailed Equipments Inventory
				</div>
				<div class="panel-body">
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargandoDetailed"></span>
					</div>
					<table class="table table-condensed table-striped table-bordered wimax" ng-if="!ctrl.cargandoDetailed && ctrl.wimax_detailed.length">
						<thead>
							<tr class="success">
								<th>BTS Name</th>
								<th style="white-space:nowrap;">Entity Type</th>
								<th>Entity Serial Number</th>
								<th>Entity Hardware Version Number</th>
								<th>Entity Hardware Revision Number</th>
								<th>Entity Operational Software Version Number</th>
								<th>Entity Boot Software Version Number</th>
							</tr>
						</thead>
						<tbody>
							<tr class="warning" ng-repeat="wimax in ctrl.wimax_detailed">
								<td>{{wimax.bts_name}}</td>
								<td style="white-space:nowrap;">{{wimax.entity_type}}</td>
								<td style="white-space:nowrap;">{{wimax.entity_serial_number}}</td>
								<td>{{wimax.entity_hardware_version_number}}</td>
								<td>{{wimax.entity_hardware_revision_number}}</td>
								<td>{{wimax.entity_operational_software_version_number}}</td>
								<td>{{wimax.entity_boot_software_version_number}}</td>
							</tr>
						</tbody>
					</table>
					<div class="col-xs-12 ng-cloak" ng-if="!ctrl.wimax_detailed.length && !ctrl.cargandoDetailed">
						<div class="well text-center">
							No hay información
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>