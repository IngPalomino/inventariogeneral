<br>
<div class="container ng-cloak">
	<div class="text-center">
		<i class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></i>
	</div>
	<div class="col-xs-12 well" ng-if="!ctrl.cargando && !ctrl.enlaces.length">
		No hay enlaces en este sitio.
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary" ng-if="ctrl.enlaces.length">
				<div class="panel-heading text-center">
					Enlaces del sitio <?php echo $infoSitio[0]["nombre_completo"] ?>
				</div>
				<div class="panel-body" style="max-height:150px;overflow-y:scroll;">
					<table class="table table-striped table-condensed">
						<thead>
							<tr>
								<th>IDU</th>
								<th>Sitio Far End</th>
								<th>IDU Far End</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="enlace in ctrl.enlaces" ng-class="{success:enlace.id == ctrl.enlace.id}">
								<td>{{enlace.idu_1.ne_id}}</td>
								<td>{{enlace.idu_2.sitio.nombre_completo}}</td>
								<td>{{enlace.idu_2.ne_id}}</td>
								<td><button class="btn btn-info btn-xs" ng-click="ctrl.listarEnlace(enlace); enlace.index = $index;">Más info</button></td>							
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12" ng-if="!ctrl.cargando">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Información del Enlace <span ng-if="ctrl.enlace.len">{{ctrl.enlaces[ctrl.enlace.index].idu_1.idu_name}} to {{ctrl.enlaces[ctrl.enlace.index].idu_2.idu_name}}</span>
				</div>
				<div class="panel-body" ng-if="ctrl.enlace.len" style="max-height:400px;overflow-y:scroll;">
					<table class="table table-condensed" style="table-layout:fixed;">
						<thead>
							<tr>
								<th class="success" colspan="2" style="text-align:center;"><strong>Near End</strong></th>
								<th class="danger" colspan="2" style="text-align:center;"><strong>Far End</strong></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="2">
									<table class="table table-condensed" ng-repeat="odu in ctrl.enlaces[ctrl.enlace.index].odus_enlaces | filter:ctrl.filtroEquiposNE | filter:{eliminado:0}">
										<thead>
											<tr>
												<th colspan="2" style="text-align:center;">ODU {{$index + 1}}</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="info">Tipo</td>
												<td class="default">{{odu.tipo_odu.tipo}}</td>
											</tr>
											<tr>
												<td class="info">N° de serie</td>
												<td class="default">{{odu.numero_serie}}</td>
											</tr>
											<tr>
												<td class="info">Ftx</td>
												<td class="default">{{odu.ftx}} MHz</td>
											</tr>
											<tr>
												<td class="info">Ptx</td>
												<td class="default">{{odu.ptx}} dBm</td>
											</tr>
										</tbody>
									</table>
								
									<table class="table table-condensed" ng-repeat="antena in ctrl.enlaces[ctrl.enlace.index].antenas_mw_enlaces | filter:ctrl.filtroEquiposNE | filter:{eliminado:0}">
										<thead >
											<tr>
												<th colspan="2" style="text-align:center;">Antena {{$index + 1}}</th>
											</tr>
										</thead>
										<tbody >
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Marca</td>
												<td class="default">{{antena.tipo_antena_mw.marca}}</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Modelo</td>
												<td class="default">{{antena.tipo_antena_mw.modelo}}</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">N° de serie</td>
												<td class="default">{{antena.numero_serie}}</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Altura</td>
												<td class="default">{{antena.altura}} m</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Azimuth</td>
												<td class="default">{{antena.azimuth}}°</td>
											</tr>
										</tbody>
									</table>




								</td>
								<td colspan="2">
									<table class="table table-condensed" ng-repeat="odu in ctrl.enlaces[ctrl.enlace.index].odus_enlaces | filter:ctrl.filtroEquiposFE | filter:{eliminado:0}">
										<thead>
											<tr>
												<th colspan="2" style="text-align:center;">ODU {{$index + 1}}</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="info">Tipo</td>
												<td class="default">{{odu.tipo_odu.tipo}}</td>
											</tr>
											<tr>
												<td class="info">N° de serie</td>
												<td class="default">{{odu.numero_serie}}</td>
											</tr>
											<tr>
												<td class="info">Ftx</td>
												<td class="default">{{odu.ftx}} MHz</td>
											</tr>
											<tr>
												<td class="info">Ptx</td>
												<td class="default">{{odu.ptx}} dBm</td>
											</tr>
										</tbody>
									</table>
									<table class="table table-condensed" ng-repeat="antena in ctrl.enlaces[ctrl.enlace.index].antenas_mw_enlaces | filter:ctrl.filtroEquiposFE | filter:{eliminado:0}">
										<thead>
											<tr>
												<th colspan="2" style="text-align:center;">Antena {{$index + 1}}</th>
											</tr>
										</thead>
										<tbody>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Marca</td>
												<td class="default">{{antena.tipo_antena_mw.marca}}</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Modelo</td>
												<td class="default">{{antena.tipo_antena_mw.modelo}}</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">N° de serie</td>
												<td class="default">{{antena.numero_serie}}</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Altura</td>
												<td class="default">{{antena.altura}} m</td>
											</tr>
											<tr ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
												<td class="info" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">Azimuth</td>
												<td class="default">{{antena.azimuth}}°</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr class="text-center info">
								<td colspan="4"><strong>Configuración del enlace</strong></td>
							</tr>
							<tr>
								<td class="info" colspan="2">Configuración</td>
								<td class="default" colspan="2">{{ctrl.enlaces[ctrl.enlace.index].configuracion}}</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Polaridad</td>
								<td class="default" colspan="2">
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].polaridad == 1">V</span>
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].polaridad == 2">H</span>
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].polaridad == 3">V+H</span>
								</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Ancho de Banda (BW)</td>
								<td class="default" colspan="2">{{ctrl.enlaces[ctrl.enlace.index].ancho_banda}} MHz</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Modulación</td>
								<td class="default" colspan="2">{{ctrl.enlaces[ctrl.enlace.index].modulacion}}</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Capacidad</td>
								<td class="default" colspan="2">{{ctrl.enlaces[ctrl.enlace.index].capacidad}} Mbit/s</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Modulación adaptativa (AM)</td>
								<td class="default" colspan="2">
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].modulacion_adaptativa == 0">NO</span>
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].modulacion_adaptativa == 1">SI</span>
								</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Modulación AM</td>
								<td class="default" colspan="2">{{ctrl.enlaces[ctrl.enlace.index].modulacion_am}}</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Capacidad AM</td>
								<td class="default" colspan="2">
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].capacidad_am != 0">{{ctrl.enlaces[ctrl.enlace.index].capacidad_am}}</span>
									<span ng-if="ctrl.enlaces[ctrl.enlace.index].capacidad_am == 0">N/A</span>
								</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Banda</td>
								<td class="default" colspan="2">{{ctrl.enlaces[ctrl.enlace.index].banda}} GHz</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>