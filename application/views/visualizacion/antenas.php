<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12">
			<h4>Microondas</h4>
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Enlace</th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Número de Serie</th>
						<th>Altura</th>
					</tr>
				</thead>
				<tbody>
				    <!-- pendiente -->
                    <tr ng-repeat="antena in ctrl.antenasMicroondasPendiente" ng-class="{'red': antena.estado_aprobacion == '2' , 'naranja': antena.estado_aprobacion == '1'}">
						<td>{{antena.enlace.idu_1.sitio.nombre_completo}} <==> {{antena.enlace.idu_2.sitio.nombre_completo}}</td>
						<td>{{antena.tipo_antena_mw.marca}}</td>
						<td>{{antena.tipo_antena_mw.modelo}}</td>
						<td>{{antena.numero_serie}}</td>
						<td>{{antena.altura}} m</td>
					</tr> 
					 <!-- pendiente -->
					<tr ng-repeat="antena in ctrl.antenasMicroondas">
						<td>{{antena.enlace.idu_1.sitio.nombre_completo}} <==> {{antena.enlace.idu_2.sitio.nombre_completo}}</td>
						<td>{{antena.tipo_antena_mw.marca}}</td>
						<td>{{antena.tipo_antena_mw.modelo}}</td>
						<td>{{antena.numero_serie}}</td>
						<td>{{antena.altura}} m</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<h4>GUL</h4>
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Sector</th>
						<th>Altura</th>
						<th>Azimuth</th>
						<th>Tilt Mecánico</th>
						<th>Tilt Eléctrico</th>
						<th>Numero de Serie</th>
					</tr>
				</thead>
				<tbody>







					<!-- solicitud reserva de espacio  -->
					<tr ng-repeat="antenape in ctrl.antenasGULPendiente" ng-class="{'red': antenape.estado == '1' , 'naranja': antenape.estado == '0'}" >	
						<!-- <td> -->
				<!-- <div class="dropdown">
					<button class="btn btn-info btn-xs dropdown-toggle" type="button" id="dropdownAntenasGULPendiente" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" ng-disabled="<?php //echo isPermitted($session["id_usuario"], $modulo, $sitio)? "false" : "true"; ?>">
						Acciones
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownAntenasGULPendiente">
						<li><a href ng-click="ctrl.editar_antenaGULPendiente(antenape)"><i class="fa fa-edit"></i> Editar</a></li>
						<li><a href ng-click="ctrl.almacenandoAntenaGULPendiente(antenape)"><i class="fa fa-check"></i> Almacenar</a></li>
						<li role="separator" class="divider"></li>
						<li><a href ng-click="ctrl.eliminar_antenaGULPendiente(antenape)"><i class="fa fa-close"></i> Eliminar</a></li>
					</ul>
				</div> -->
			<!-- </td>
			-->

			<td >{{antenape.tipo_antena_gul.marca}}</td>
			<td>{{antenape.tipo_antena_gul.modelo}}</td>
			<td>
				<span ng-if="antenape.tipo_antena_gul.marca != 'TELNET'">{{antenape.sector}}</span>
				<span ng-repeat="sector in antenape.info_telnet" ng-if="antenape.tipo_antena_gul.marca == 'TELNET'">
					<span ng-if="$index > 0 && sector.sector">/</span>
					<span>{{sector.sector}}</span>
				</span>
			</td>
			<td>{{antenape.altura}} m</td>
			<td>
				<span ng-if="antenape.tipo_antena_gul.marca != 'TELNET'">{{antenape.azimuth}}°</span>
				<span ng-repeat="sector in antenape.info_telnet" ng-if="antenape.tipo_antena_gul.marca == 'TELNET'">
					<span ng-if="$index > 0">/</span>
					<span>{{sector.azimuth}}°</span>
				</span>
			</td>
			<td>
				<span ng-if="antenape.tipo_antena_gul.marca != 'TELNET'">{{antenape.tilt_mecanico}}°</span>
				<span ng-repeat="sector in antenape.info_telnet" ng-if="antenape.tipo_antena_gul.marca == 'TELNET'">
					<span ng-if="$index > 0">/</span>
					<span>{{sector.tilt_mecanico}}°</span>
				</span>
			</td>
			<td>
				<span ng-if="antenape.tipo_antena_gul.marca != 'TELNET'">{{antenape.tilt_electrico}}°</span>
				<span ng-repeat="sector in antenape.info_telnet" ng-if="antenape.tipo_antena_gul.marca == 'TELNET'">
					<span ng-if="$index > 0">/</span>
					<span>{{sector.tilt_electrico}}°</span>
				</span>
			</td>
			<td>{{antenape.numero_serie}}</td>
			<!-- <td style="font-size:8pt;">
				<span>Creado: {{antenape.created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{antenape.nombres}} {{antenape.apellido_paterno}}  </span> -->
				<!-- 
				<span ng-if="antenape.logs_antenas_gul_pendiente[0].evento.tipo == 'INSERT'">Creado: {{antenape.logs_antenas_gul_pendiente[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{antenape.logs_antenas_gul_pendiente[0].idusuario.nombres}} {{antenape.logs_antenas_gul_pendiente[0].idusuario.apellido_paterno}}</span>
				<br>
				<span ng-if="antenape.logs_antenas_gul_pendiente[1].evento.tipo == 'UPDATE'">Actualizado: {{antenape.logs_antenas_gul_pendiente[1].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}} por {{antenape.logs_antenas_gul_pendiente[1].idusuario.nombres}} {{antenape.logs_antenas_gul_pendiente[1].idusuario.apellido_paterno}}</span> -->
				<!-- </td> -->
			</tr>
			<!-- solicitud reserva de espacio -->
			<tr ng-repeat="antena in ctrl.antenasGUL">
				<td>{{antena.tipo_antena_gul.marca}}</td>
				<td>{{antena.tipo_antena_gul.modelo}}</td>
				<td>
				<span ng-if="antena.tipo_antena_gul.marca != 'TELNET'">{{antena.sector}}</span>
				<span ng-if="antena.tipo_antena_gul.marca == 'TELNET'">	
                <span ng-if="antena.info_telnet.length > 0">
						<span ng-repeat="sector in antena.info_telnet" ng-if="antena.tipo_antena_gul.marca == 'TELNET'">
									<span ng-if="$index > 0 && sector.sector">/</span>
									<span>{{sector.sector}}</span>
						</span>
                </span>
                <span ng-if="antena.info_telnet.length == 0">
					{{antena.sector}}
                </span>
                </span>
				</td>
				<td>{{antena.altura}} m</td>
				<td>
					<span ng-if="antena.tipo_antena_gul.marca != 'TELNET'">{{antena.azimuth}}°</span>
                    <span ng-if="antena.tipo_antena_gul.marca == 'TELNET'">
                    <span ng-if="antena.info_telnet.length > 0">
					<span ng-repeat="sector in antena.info_telnet" ng-if="antena.tipo_antena_gul.marca == 'TELNET'">
						<span ng-if="$index > 0">/</span>
						<span>{{sector.azimuth}}°</span>
					</span>
                         </span>
                          <span ng-if="antena.info_telnet.length == 0">
                          	<span>{{antena.azimuth}}°</span>
                         </span>

						</span>
				</td>
				<td>



                        <span ng-if="antena.tipo_antena_gul.marca != 'TELNET'">{{antena.tilt_mecanico}}°</span>
                        
                        <span ng-if="antena.tipo_antena_gul.marca == 'TELNET'">

                         <span ng-if="antena.info_telnet.length > 0">
                         	<span ng-repeat="sector in antena.info_telnet" ng-if="antena.tipo_antena_gul.marca == 'TELNET'">
								<span ng-if="$index > 0">/</span>
								<span>{{sector.tilt_mecanico}}°</span>
							</span>
                         </span>
                          <span ng-if="antena.info_telnet.length == 0">
                          	<span>{{antena.tilt_mecanico}}°</span>
                         </span>

						</span>










				</td>
				<td>
					<span ng-if="antena.tipo_antena_gul.marca != 'TELNET'">{{antena.tilt_electrico}}°</span>







                      <span ng-if="antena.tipo_antena_gul.marca == 'TELNET'">

                         <span ng-if="antena.info_telnet.length > 0">
					<span ng-repeat="sector in antena.info_telnet" ng-if="antena.tipo_antena_gul.marca == 'TELNET'">
						<span ng-if="$index > 0">/</span>
						<span>{{sector.tilt_electrico}}°</span>
					</span>
                         </span>
                          <span ng-if="antena.info_telnet.length == 0">
                          	<span>{{antena.tilt_electrico}}°</span>
                         </span>

						</span>




				</td>
				<td>{{antena.numero_serie}}</td>
			</tr>
		</tbody>
	</table>
</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<h4>iDEN</h4>
		<table class="table table-condensed table-hover">
			<thead>
				<tr>
					<th>Marca</th>
					<th>Modelo</th>
					<th>Sector</th>
					<th>Altura</th>
					<th>Azimuth</th>
					<th>Tilt Mecánico</th>
					<th>Tilt Eléctrico</th>
					<th>Números de Serie</th>
					<th>Cantidad</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="antena in ctrl.antenasIDEN">
					<td>{{antena.tipo_antena_iden.marca}}</td>
					<td>{{antena.tipo_antena_iden.modelo}}</td>
					<td>{{antena.sector}}</td>
					<td>{{antena.altura}} m</td>
					<td>{{antena.azimuth}}°</td>
					<td>{{antena.tilt_mecanico}}°</td>
					<td>{{antena.tilt_electrico}}°</td>
					<td>{{antena.numero_serie}}</td>
					<td class="info">{{antena.cantidad}}</td>
				</tr>
			</tbody>
		</table>
	</div>
		<!--<div class="col-xs-6">
			<h4>Wimax</h4>
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Versión</th>
						<th>Altura</th>
						<th>Cantidad</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="4"></td>
						<td class="info">#</td>
					</tr>
				</tbody>
			</table>
		</div>-->
	</div>
	<div class="row ng-cloak" ng-if="ctrl.info_sitio.in_building == 1">
		<div class="col-xs-12">
			<h4>In Building</h4>
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Cantidad</th>
						<th>Tipo</th>
						<th>Configuración</th>
						<th>Banda</th>
						<th>Peso</th>
						<th>Dimensiones</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="antena in ctrl.antenas_in_building">
						<td>{{antena.tipo_antena_in_building.marca}}</td>
						<td>{{antena.tipo_antena_in_building.modelo}}</td>
						<td>{{antena.cantidad}}</td>
						<td>{{antena.tipo_antena_in_building.tipo}}</td>
						<td>{{antena.tipo_antena_in_building.configuracion}}</td>
						<td>{{antena.tipo_antena_in_building.banda}} GHz</td>
						<td>{{antena.tipo_antena_in_building.peso}} Kg</td>
						<td>{{antena.tipo_antena_in_building.dimensiones}} mm</td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<td class="info">#</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>