<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>Antenas Microondas</h2>
			</div>
			<ol class="breadcrumb">				
				<li class="active">Antenas Microondas</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Antenas Microondas
				</div>
				<div class="panel-body">
					<div class="col-md-12 col-sm-12 col-xs-12">
					
						<div class="panel panel-default">
							<div class="panel-heading text-center">
								Mis solicitudes
							</div>
							<div class="panel-body" style="height: 276px;word-wrap: break-word;overflow-y: scroll;">
								<table class="table table-condensed table-responsive">
									<thead>
										<tr>
											<th>#</th>
											<th>Fecha y Hora</th>
											<th>Usuario Solicitante</th>
											<th>Sitio</th>
											<th>Nombre Sitio</th>
											<th>Estado</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="solicitud in ctrl.mis_solicitudes">
											<td>{{$index + 1}}</td>
											<td>{{solicitud.created_at | date:"dd/MM/yyyy - HH:mm:ss"}}</td>
											<td>
<i class="fa fa-user text-primary" aria-hidden="true"></i> {{solicitud.nombres}}{{solicitud.nombres}}</td>
											<td>{{solicitud.sitio}}</td>
											<td>{{solicitud.nombre_sitio}}</td>
											<td>
												<span   ng-if="solicitud.estado_aprobacion == '1'" class="label label-danger naranja">Pendiente</span>
												<span   ng-if="solicitud.estado_aprobacion == '2'" class="label label-danger red">Reservado</span>
									<span ng-if="solicitud.estado_aprobacion == '3'" class="label label-success">Finalizado</span>
											</td>
											<td><!-- <input type="checkbox"> --></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- <div class="panel-footer text-center">
								<button class="btn btn-danger">Eliminar selección</button>
							</div> -->
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>