<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo site_url("solicitante/gul");?>" class="btn boton" ng-class="<?php echo $this->auth->logged_in()? "" : "'disabled'" ?>"  role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-minus fa-rotate-90 fa-5x"></span></td></tr>
							<tr><td>GUL</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo site_url("solicitante/mw");?>" class="btn boton" ng-class="<?php echo $this->auth->logged_in()? "" : "'disabled'" ?>" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-circle fa-5x"></span></td></tr>
							<tr><td>Microondas</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			
		




		</div>
	</div>
	<br/>

</section>