<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="rectif in ctrl.bancos_baterias" ng-if="ctrl.bancos_baterias.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Bancos de Baterías del Rectificador {{rectif.marca}} - {{rectif.modelo}} - Energización: {{rectif.energizacion_controlador_rectif}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<thead>
							<tr>
								<th>ID</th>
								<th>Acciones</th>
								<th>Marca</th>
								<th>Modelo</th>
								<th>Capacidad/Banco</th>
								<th>Fecha de Instalación</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat-start="banco in rectif.bancos">
								<td>{{banco.id}}</td>
								<td>
									<button class="btn btn-primary btn-xs" ng-click="ctrl.mostrarCeldas(banco)" ng-show="!banco.mostrar">Ver Celdas</button> 
									<button class="btn btn-info btn-xs" ng-click="ctrl.ocultarCeldas(banco)" ng-hide="!banco.mostrar">Ocultar Celdas</button>
								</td>
								<td>{{banco.tipo_bancos_baterias.marca}}</td>
								<td>{{banco.tipo_bancos_baterias.modelo}}</td>
								<td>{{banco.tipo_bancos_baterias.capacidad}} Ah</td>
								<td>{{banco.fecha_instalacion | date:"dd/MM/yyyy"}}</td>
							</tr>
							<tr ng-repeat-end ng-hide="!banco.mostrar">
								<td colspan="2"></td>
								<td colspan="4">
									<div class="col-xs-12">
										<div class="panel panel-success">
											<div class="panel-heading text-center">
												Celdas del Banco {{$index + 1}}
											</div>
											<div class="panel-body">
												<table class="table table-condensed table-striped">
													<thead>
														<tr>
															<th>Capacidad</th>
															<th>Fecha de Instalación</th>
															<th>Número de Serie</th>
															<th>Observación</th>
														</tr>
													</thead>
													<tbody>
														<tr ng-repeat="celda in banco.celdas_controlador_rectif_bancos_baterias">
															<td>{{banco.tipo_bancos_baterias.capacidad}} Ah</td>
															<td>{{celda.fecha_instalacion | date:"dd/MM/yyyy"}}</td>
															<td>{{celda.numero_serie}}</td>
															<td>
																<span ng-if="celda.tipo_bancos_baterias == 0">-</span>
																<span ng-if="celda.tipo_bancos_baterias != 0">{{celda.tipo_bancos_baterias.marca}} - {{celda.tipo_bancos_baterias.modelo}}</span>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.bancos_baterias.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
</div>