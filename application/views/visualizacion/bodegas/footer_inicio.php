<script type="text/javascript">
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			ref.auth.evaluador = (ref.session.evaluador == 1)? '' : 'disabled';

			ref.loading = false;
			ref.searched = false;

			ref.almacenes = [];
			ref.sitios = [];

			ref.configAlmacenes = {
				valueField: 'id',
				labelField: 'almacen',
				searchField: 'almacen',
				delimiter: '|',
				placeholder: 'Buscar almacen',
				maxItems: 1,
				inputClass: "form-control selectize-input",
				dropdownParent: "body",				
				sortField: [
					{field: 'almacen', direction: 'asc'}
				]
			};

			ref.configSitios = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Buscar sitio',
				maxItems: 1,
				inputClass: "form-control selectize-input",
				dropdownParent: "body"
			};

			ref.configObjetos = {
				valueField: 'objeto',
				labelField: 'nombre',
				searchField: 'nombre',
				delimiter: '|',
				placeholder: 'Buscar objeto',
				maxItems: 1,
				inputClass: "form-control selectize-input",
				dropdownParent: "body"
			};

			ref.busqueda = {
				almacen: "",
				sitio: "",
				objeto: ""
			};

			ref.objetos = [];
			ref.elementos_busqueda = [];

			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.almacenes = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarSitios = function(){
				$http({
					url:"<?php echo base_url("status_site/listar_sitios_total");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sitios = data;
					}
				}).error(function(err){

				});
			}

			listarObjetos = function(){
				$http({
					url:"<?php echo base_url("objetos_bodegas/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.objetos = data;
					}
				}).error(function(err){

				});
			}

			ref.buscarAlmacenes = function(almacen){
				ref.loading = true;
				ref.busqueda.sitio = "";
				ref.busqueda.objeto = "";

				$http({
					url:"<?php echo base_url("objetos_bodegas/busqueda");?>",
					method:"POST",
					data:"almacen="+almacen,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					ref.loading = false;
					ref.searched = true;
					if(data.length)
					{
						ref.elementos_busqueda = data;
					}
					else
					{
						ref.elementos_busqueda = [];
					}
				}).error(function(err){
					ref.loading = false;
				});
			}

			ref.buscarSitios = function(sitio){
				ref.loading = true;
				ref.busqueda.almacen = "";
				ref.busqueda.objeto = "";

				$http({
					url:"<?php echo base_url("objetos_bodegas/busqueda");?>",
					method:"POST",
					data:"sitio="+sitio,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					ref.loading = false;
					ref.searched = true;
					if(data.length)
					{
						ref.elementos_busqueda = data;
					}
					else
					{
						ref.elementos_busqueda = [];
					}
				}).error(function(err){
					ref.loading = false;
				});
			}

			ref.buscarObjetos = function(objeto){
				ref.loading = true;
				ref.busqueda.almacen = "";
				ref.busqueda.sitio = "";

				$http({
					url:"<?php echo base_url("objetos_bodegas/busqueda");?>",
					method:"POST",
					data:"objeto="+objeto,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					ref.loading = false;
					ref.searched = true;
					if(data.length)
					{
						ref.elementos_busqueda = data;
						ref.datainfo = JSON.parse(ref.elementos_busqueda[0].info);
					}
					else
					{
						ref.elementos_busqueda = [];
					}
				}).error(function(err){
					ref.loading = false;
				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						ref.reestablecerContrasena = {};
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
				ref.reestablecerContrasena = {};
			};

			listarAlmacenes();
			listarSitios();
			listarObjetos();
		}]);
</script>
</body>
</html>