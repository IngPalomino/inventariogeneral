<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>Búsqueda por Almacenes</h2>
			</div>
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url("ver/bodegas"); ?>">Bodegas</a></li>
				<li class="active">Almacenes</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="col-xs-12 col-md-6">
						<select class="form-control" ng-model="ctrl.busquedaAlmacen" ng-change="ctrl.busquedaObjeto = '';">
							<option value="">Elegir almacén</option>
							<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
						</select>
					</div>
					<div class="col-xs-12 col-md-6">
						<select class="form-control" ng-model="ctrl.busquedaObjeto" ng-change="ctrl.listarObjeto(ctrl.busquedaObjeto,ctrl.busquedaAlmacen)" ng-disabled="!ctrl.busquedaAlmacen">
							<option value="">Elegir objeto</option>
							<option ng-repeat="objeto in ctrl.objetos" value="{{objeto.id}}">{{objeto.objeto}}</option>
						</select>
					</div><br><br><br>
					<div class="text-center">
						<i class="fa fa-spinner fa-spin fa-4x" ng-if="ctrl.cargando"></i>
					</div>
					<div ng-switch on="ctrl.busquedaObjeto">
						<div ng-if="!ctrl.cargando" ng-switch-when="1">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a class="disabled" data-toggle="tab" href="#grupos_electrogenos">Grupos Electrógenos</a></li>
								<li><a data-toggle="tab" href="#elementos">Elementos</a></li>
							</ul>
							<div class="tab-content">
								<div id="grupos_electrogenos" class="tab-pane fade in active">
									<table class="table table-responsive table-hover">
										<thead>
											<tr>
												<th><i class="fa fa-cogs"></i></th>
												<th>Marca</th>
												<th>Modelo</th>
												<th>Estado</th>
												<th>Proveniente</th>
												<th>Historial</th>
											</tr>
										</thead>
										<tbody>
											<tr ng-repeat="grupo in ctrl.grupos_electrogenos.grupos">
												<td>
													<div class="dropdown">
														<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdownGruposElectrogenosMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
															Acciones
															<span class="caret"></span>
														</button>
														<ul class="dropdown-menu" aria-labelledby="dropdownGruposElectrogenosMenu">
															<li><a href ng-click="ctrl.verGrupoElectrogeno(grupo)"><i class="fa fa-edit"></i> Más información</a></li>
														</ul>
													</div>
												</td>
												<td>{{grupo.tipo_grupo_electrogeno.marca}}</td>
												<td>{{grupo.tipo_grupo_electrogeno.modelo}}</td>
												<td>
													<span style="text-transform: uppercase;" ng-class="{'label label-success':grupo.estado.id == 1, 'label label-warning':grupo.estado.id == 2, 'label label-danger':grupo.estado.id == 3}">{{grupo.estado.estado}}</span>
												</td>
												<td>{{grupo.proveniente_sitio.nombre_completo}}</td>
												<td style="font-size:8pt;list-style:none;">
													<span ng-if="!grupo.logs_bodega_grupos_electrogenos.length || grupo.logs_bodega_grupos_electrogenos[0].evento.tipo != 'INSERT'">Almacenado: {{grupo.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
													<span ng-if="grupo.logs_bodega_grupos_electrogenos[0].evento.tipo == 'INSERT'">Almacenado: {{grupo.logs_bodega_grupos_electrogenos[0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{grupo.logs_bodega_grupos_electrogenos[0].usuario.nombres}} {{grupo.logs_bodega_grupos_electrogenos[0].usuario.apellido_paterno}}</span>
													<br>
													<span ng-if="grupo.logs_bodega_grupos_electrogenos[1].evento.tipo == 'UPDATE'">Actualizado: {{grupo.logs_bodega_grupos_electrogenos[1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{grupo.logs_bodega_grupos_electrogenos[1].usuario.nombres}} {{grupo.logs_bodega_grupos_electrogenos[1].usuario.apellido_paterno}}</span>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div id="elementos" class="tab-pane fade">
									<table class="table table-responsive table-hover">
										<thead>
											<tr>
												<th><i class="fa fa-cogs"></i></th>
												<th>Elemento</th>
												<th>Marca</th>
												<th>Proveniente</th>
												<th>Almacenado en</th>
												<th>Historial</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
						<div ng-if="!ctrl.cargando" ng-switch-when="2">
							<table class="table table-responsive table-hover">
								<thead>
									<tr>
										<th><i class="fa fa-cogs"></i></th>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Estado</th>
										<th>Proveniente</th>
										<th>Historial</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="antena in ctrl.antenas_mw">
										<td>
											<div class="dropdown">
												<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdownAntenasMicroondasMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
													Acciones
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu" aria-labelledby="dropdownAntenasMicroondasMenu">
													<li><a href ng-click="ctrl.verAntenaMicroondas(antena)"><i class="fa fa-edit"></i> Más información</a></li>
												</ul>
											</div>
										</td>
										<td>{{antena.tipo_antena_mw.marca}}</td>
										<td>{{antena.tipo_antena_mw.modelo}}</td>
										<td>
											<span style="text-transform: uppercase;" ng-class="{'label label-success':antena.estado.id == 1, 'label label-warning':antena.estado.id == 2, 'label label-danger':antena.estado.id == 3}">{{antena.estado.estado}}</span>
										</td>
										<td>{{antena.proveniente_sitio.nombre_completo}}</td>
										<td style="font-size:8pt;">
											<span ng-if="!antena.logs_bodega_antenas_mw.length || antena.logs_bodega_antenas_mw[0].evento.tipo != 'INSERT'">Almacenado: {{antena.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
											<span ng-if="antena.logs_bodega_antenas_mw[0].evento.tipo == 'INSERT'">Almacenado: {{antena.logs_bodega_antenas_mw[0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{antena.logs_bodega_antenas_mw[0].usuario.nombres}} {{antena.logs_bodega_antenas_mw[0].usuario.apellido_paterno}}</span>
											<br>
											<span ng-if="antena.logs_bodega_antenas_mw[1].evento.tipo == 'UPDATE'">Actualizado: {{antena.logs_bodega_antenas_mw[1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{antena.logs_bodega_antenas_mw[1].usuario.nombres}} {{antena.logs_bodega_antenas_mw[1].usuario.apellido_paterno}}</span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div ng-if="!ctrl.cargando" ng-switch-when="3">
							<table class="table table-responsive table-hover">
								<thead>
									<tr>
										<th><i class="fa fa-cogs"></i></th>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Estado</th>
										<th>Proveniente</th>
										<th>Historial</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="antena in ctrl.antenas_gul">
										<td>
											<div class="dropdown">
												<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdownAntenasGULMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
													Acciones
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu" aria-labelledby="dropdownAntenasGULMenu">
													<li><a href ng-click="ctrl.verAntenaGUL(antena)"><i class="fa fa-edit"></i> Más información</a></li>
												</ul>
											</div>
										</td>
										<td>{{antena.tipo_antena_gul.marca}}</td>
										<td>{{antena.tipo_antena_gul.modelo}}</td>
										<td>
											<span style="text-transform: uppercase;" ng-class="{'label label-success':antena.estado.id == 1, 'label label-warning':antena.estado.id == 2, 'label label-danger':antena.estado.id == 3}">{{antena.estado.estado}}</span>
										</td>
										<td>{{antena.proveniente_sitio.nombre_completo}}</td>
										<td style="font-size:8pt;white-space: nowrap;">
											<span ng-if="!antena.logs_bodega_antenas_gul.length || antena.logs_bodega_antenas_gul[0].evento.tipo != 'INSERT'">Almacenado: {{antena.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
											<span ng-if="antena.logs_bodega_antenas_gul[0].evento.tipo == 'INSERT'">Almacenado: {{antena.logs_bodega_antenas_gul[0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{antena.logs_bodega_antenas_gul[0].usuario.nombres}} {{antena.logs_bodega_antenas_gul[0].usuario.apellido_paterno}}</span>
											<br>
											<span ng-if="antena.logs_bodega_antenas_gul[1].evento.tipo == 'UPDATE'">Actualizado: {{antena.logs_bodega_antenas_gul[1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{antena.logs_bodega_antenas_gul[1].usuario.nombres}} {{antena.logs_bodega_antenas_gul[1].usuario.apellido_paterno}}</span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div ng-if="!ctrl.cargando" ng-switch-when="4">
							<table class="table table-responsive table-hover">
								<thead>
									<tr>
										<th>Acciones</th>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Estado</th>
										<th>Proveniente</th>
										<th>Historial</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="antena in ctrl.antenas_iden">
										<td>
											<div class="dropdown">
												<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdownAntenasIDENMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
													Acciones
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu" aria-labelledby="dropdownAntenasIDENMenu">
													<li><a href ng-click="ctrl.verAntenaIDEN(antena)"><i class="fa fa-edit"></i> Más información</a></li>
												</ul>
											</div>
										</td>
										<td>{{antena.tipo_antena_iden.marca}}</td>
										<td>{{antena.tipo_antena_iden.modelo}}</td>
										<td>
											<span style="text-transform: uppercase;" ng-class="{'label label-success':antena.estado.id == 1, 'label label-warning':antena.estado.id == 2, 'label label-danger':antena.estado.id == 3}">{{antena.estado.estado}}</span>
										</td>
										<td>{{antena.proveniente_sitio.nombre_completo}}</td>
										<td style="font-size:8pt;">
											<span ng-if="!antena.logs_bodega_antenas_iden.length || antena.logs_bodega_antenas_iden[0].evento.tipo != 'INSERT'">Almacenado: {{antena.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
											<span ng-if="antena.logs_bodega_antenas_iden[0].evento.tipo == 'INSERT'">Almacenado: {{antena.logs_bodega_antenas_iden[0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{antena.logs_bodega_antenas_iden[0].usuario.nombres}} {{antena.logs_bodega_antenas_iden[0].usuario.apellido_paterno}}</span>
											<br>
											<span ng-if="antena.logs_bodega_antenas_iden[1].evento.tipo == 'UPDATE'">Actualizado: {{antena.logs_bodega_antenas_iden[1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{antena.logs_bodega_antenas_iden[1].usuario.nombres}} {{antena.logs_bodega_antenas_iden[1].usuario.apellido_paterno}}</span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="verGrupoElectrogeno" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Información de grupo electrógeno</h3>

	<table class="table table-condensed">
		<tbody>
			<tr>
				<td class="info">Estado</td>
				<td class="default" colspan="2">
					<span style="text-transform: uppercase;" ng-class="{'label label-success':ctrl.editarGrupoElectrogeno.estado.id == 1, 'label label-warning':ctrl.editarGrupoElectrogeno.estado.id == 2, 'label label-danger':ctrl.editarGrupoElectrogeno.estado.id == 3}">{{ctrl.editarGrupoElectrogeno.estado.estado}}</span>
				</td>
				<td class="info">Proveniente</td>
				<td class="default" colspan="2">{{ctrl.editarGrupoElectrogeno.proveniente_sitio.nombre_completo}}</td>
			</tr>
			<tr class="text-center" style="background-color:#d2f3ce;">
				<td colspan="6">Grupo Electrógeno</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_grupo_electrogeno.marca}}</td>
				<td class="info">Modelo</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_grupo_electrogeno.modelo}}</td>
				<td class="info">Número de serie</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.numero_serie_grupo_electrogeno}}</td>
			</tr>
			<tr class="text-center" style="background-color:#b9d1e5;">
				<td colspan="6">Motor</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_motor_ge.marca}}</td>
				<td class="info">Modelo</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_motor_ge.modelo}}</td>
				<td class="info">Número de serie</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.numero_serie_motor}}</td>
			</tr>
			<tr class="text-center" style="background-color:#d7e7b4;">
				<td colspan="6">Generador</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_generador_ge.marca}}</td>
				<td class="info">Modelo</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_generador_ge.modelo}}</td>
				<td class="info">Número de serie</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.numero_serie_generador}}</td>
			</tr>
			<tr>
				<td class="info">Potencia</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_generador_ge.potencia}} kW</td>
				<td class="info">Corriente</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_generador_ge.corriente}} A</td>
				<td class="info" colspan="2"></td>
			</tr>
			<tr class="text-center" style="background-color:#e7d7b4;">
				<td colspan="6">AVR</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_avr_ge.marca}}</td>
				<td class="info">Modelo</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_avr_ge.modelo}}</td>
				<td class="info">Número de serie</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.numero_serie_avr}}</td>
			</tr>
			<tr class="text-center bg-success">
				<td colspan="6">Controlador GE</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_controlador_ge.marca}}</td>
				<td class="info">Modelo</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_controlador_ge.modelo}}</td>
				<td class="info">Número de serie</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.numero_serie_controlador_ge}}</td>
			</tr>
			<tr class="text-center" style="background-color:#f7f7ca;">
				<td colspan="6">Controlador TTA</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_controlador_tta.marca}}</td>
				<td class="info">Modelo</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.tipo_controlador_tta.modelo}}</td>
				<td class="info">Número de serie</td>
				<td class="default">{{ctrl.editarGrupoElectrogeno.numero_serie_controlador_tta}}</td>
			</tr>
		</tbody>
	</table>
</div>

<div id="verAntenaMicroondas" class="popup">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>Información de antena microondas</h3>

	<table class="table table-condensed">
		<tbody>
			<tr>
				<td class="info">Estado</td>
				<td class="default">
					<span style="text-transform: uppercase;" ng-class="{'label label-success':ctrl.editarAntenaMicroondas.estado.id == 1, 'label label-warning':ctrl.editarAntenaMicroondas.estado.id == 2, 'label label-danger':ctrl.editarAntenaMicroondas.estado.id == 3}">{{ctrl.editarAntenaMicroondas.estado.estado}}</span>
				</td>
			</tr>
			<tr>
				<td class="info">Proveniente</td>
				<td class="default">{{ctrl.editarAntenaMicroondas.proveniente_sitio.nombre_completo}}</td>
			</tr>
			<tr>
				<td class="info">Marca</td>
				<td class="default">{{ctrl.editarAntenaMicroondas.tipo_antena_mw.marca}}</td>
			</tr>
			<tr>
				<td class="info">Modelo</td>
				<td class="default">{{ctrl.editarAntenaMicroondas.tipo_antena_mw.modelo}}</td>
			</tr>
			<tr>
				<td class="info">Número de serie</td>
				<td class="default">{{ctrl.editarAntenaMicroondas.numero_serie}}</td>
			</tr>
		</tbody>
	</table>
</div>