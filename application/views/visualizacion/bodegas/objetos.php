<section class="container-fluid ng-cloak">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="page-header">
				<h2>Búsqueda por Objetos</h2>
			</div>
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url("ver/bodegas"); ?>">Bodegas</a></li>
				<li class="active">Objetos</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="panel panel-default">
				<div class="panel-body">
					<select class="form-control" ng-model="ctrl.busqueda" ng-change="ctrl.listarObjeto(ctrl.busqueda)">
						<option value="">Elegir objeto</option>
						<option value="1">Elementos Microondas</option>
						<option value="2">Grupos Electrógenos</option>
						<option value="3">Antenas GUL</option>
						<option value="4">Antenas iDEN</option>
					</select>
					<selectize ng-if="ctrl.busqueda == 1" config="ctrl.configMicroondas" options="ctrl.elementos_mw" ng-model="ctrl.busquedaElementoMicroondas" ng-change="ctrl.listarElementoMicroondas(ctrl.busquedaElementoMicroondas)"></selectize>
					<br>
					<div class="text-center">
						<i class="fa fa-spinner fa-spin fa-4x" ng-if="ctrl.cargando"></i>
					</div>
					<div ng-switch on="ctrl.busqueda">
						<div ng-if="!ctrl.cargando" ng-switch-when="2">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a class="disabled" data-toggle="tab" href="#grupos_electrogenos">Grupos Electrógenos</a></li>
								<li><a data-toggle="tab" href="#elementos">Elementos</a></li>
							</ul>
							<div class="tab-content">
								<div id="grupos_electrogenos" class="tab-pane fade in active">
									<table class="table table-responsive table-hover">
										<thead>
											<tr>
												<th><i class="fa fa-cogs"></i></th>
												<th>Marca</th>
												<th>Modelo</th>
												<th>Estado</th>
												<!--<th>Proveniente</th>-->
												<th>Almacenado en</th>
												<th>Historial</th>
											</tr>
										</thead>
										<tbody>
											<tr ng-repeat="grupo in ctrl.grupos_electrogenos.grupos">
												<td>
													<div class="dropdown">
														<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdownGruposElectrogenosMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
															Acciones
															<span class="caret"></span>
														</button>
														<ul class="dropdown-menu" aria-labelledby="dropdownGruposElectrogenosMenu">
															<li><a href ng-click="ctrl.verGrupoElectrogeno(grupo)"><i class="fa fa-edit"></i> Más Información</a></li>
														</ul>
													</div>
												</td>
												<td>{{grupo.tipo_grupo_electrogeno.marca}}</td>
												<td>{{grupo.tipo_grupo_electrogeno.modelo}}</td>
												<td>
													<span style="text-transform: uppercase;" ng-class="{'label label-success':grupo.estado.id == 1, 'label label-warning':grupo.estado.id == 2, 'label label-danger':grupo.estado.id == 3}">{{grupo.estado.estado}}</span>
												</td>
												<!--<td>{{grupo.proveniente_sitio.nombre_completo}}</td>-->
												<td>{{grupo.almacen.almacen}}</td>
												<td style="font-size:8pt;list-style:none;">
													<span ng-if="!grupo.logs_bodega_grupos_electrogenos.length || grupo.logs_bodega_grupos_electrogenos[0].evento.tipo != 'INSERT'">Almacenado: {{grupo.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
													<span ng-if="grupo.logs_bodega_grupos_electrogenos[0].evento.tipo == 'INSERT'">Almacenado: {{grupo.logs_bodega_grupos_electrogenos[0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{grupo.logs_bodega_grupos_electrogenos[0].usuario.nombres}} {{grupo.logs_bodega_grupos_electrogenos[0].usuario.apellido_paterno}}</span>
													<br>
													<span ng-if="grupo.logs_bodega_grupos_electrogenos[1].evento.tipo == 'UPDATE'">Actualizado: {{grupo.logs_bodega_grupos_electrogenos[1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{grupo.logs_bodega_grupos_electrogenos[1].usuario.nombres}} {{grupo.logs_bodega_grupos_electrogenos[1].usuario.apellido_paterno}}</span>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div id="elementos" class="tab-pane fade">
									<table class="table table-responsive table-hover">
										<thead>
											<tr>
												<th><i class="fa fa-cogs"></i></th>
												<th>Elemento</th>
												<th>Marca</th>
												<th>Proveniente</th>
												<th>Almacenado en</th>
												<th>Historial</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
						<div ng-if="!ctrl.cargando && ctrl.busquedaElementoMicroondas" ng-switch-when="1">
							<table class="table table-responsive table-hover">
								<thead>
									<tr>
										<th><i class="fa fa-cogs"></i></th>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Estado</th>
										<th>Almacenado en</th>
										<th>Historial</th>
									</tr>
									<tr>
										<th>Filtros</th>
										<th><input class="form-control input-sm" type="text" ng-model="ctrl.filtroMarcaMW"></th>
										<th><input class="form-control input-sm" type="text" ng-model="ctrl.filtroModeloMW"></th>
										<th><input class="form-control input-sm" type="text" ng-model="ctrl.filtroEstadoMW"></th>
										<th><input class="form-control input-sm" type="text" ng-model="ctrl.filtroAlmacenMW"></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="antena in ctrl.elemento_mw | filter:{tipo_elemento_mw:{marca:ctrl.filtroMarcaMW}} | filter:{tipo_elemento_mw:{modelo:ctrl.filtroModeloMW}} | filter:{estado:{estado:ctrl.filtroEstadoMW}} | filter:{almacen:{almacen:ctrl.filtroAlmacenMW}}">
										<td>
											<div class="dropdown">
												<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdownAntenasMicroondasMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
													Acciones
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu" aria-labelledby="dropdownAntenasMicroondasMenu">
													<li><a href ng-click="ctrl.verAntenaMicroondas(antena)"><i class="fa fa-edit"></i> Más información</a></li>
												</ul>
											</div>
										</td>
										<td>{{antena.tipo_elemento_mw.marca}}</td>
										<td>{{antena.tipo_elemento_mw.modelo}}</td>
										<td>
											<span style="text-transform: uppercase;" ng-class="{'label label-success':antena.estado.id == 1, 'label label-warning':antena.estado.id == 2, 'label label-danger':antena.estado.id == 3}">{{antena.estado.estado}}</span>
										</td>
										<td>{{antena.almacen.almacen}}</td>
										<td style="font-size:8pt;">
											<span ng-if="!antena.logs_bodega_antenas_mw.length || antena.logs_bodega_antenas_mw[0].evento.tipo != 'INSERT'">Almacenado: {{antena.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
											<span ng-if="antena.logs_bodega_antenas_mw[0].evento.tipo == 'INSERT'">Almacenado: {{antena.logs_bodega_antenas_mw[0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{antena.logs_bodega_antenas_mw[0].usuario.nombres}} {{antena.logs_bodega_antenas_mw[0].usuario.apellido_paterno}}</span>
											<br>
											<span ng-if="antena.logs_bodega_antenas_mw[1].evento.tipo == 'UPDATE'">Actualizado: {{antena.logs_bodega_antenas_mw[1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{antena.logs_bodega_antenas_mw[1].usuario.nombres}} {{antena.logs_bodega_antenas_mw[1].usuario.apellido_paterno}}</span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div ng-if="!ctrl.cargando" ng-switch-when="3">
							<table class="table table-responsive table-hover">
								<thead>
									<tr>
										<th><i class="fa fa-cogs"></i></th>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Estado</th>
										<!--<th>Proveniente</th>-->
										<th>Almacenado en</th>
										<th>Historial</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="antena in ctrl.antenas_gul">
										<td>
											<div class="dropdown">
												<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdownAntenasGULMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
													Acciones
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu" aria-labelledby="dropdownAntenasGULMenu">
													<li><a href ng-click="ctrl.verAntenaGUL(antena)"><i class="fa fa-edit"></i> Más Información</a></li>
												</ul>
											</div>
										</td>
										<td>{{antena.tipo_antena_gul.marca}}</td>
										<td>{{antena.tipo_antena_gul.modelo}}</td>
										<td>
											<span style="text-transform: uppercase;" ng-class="{'label label-success':antena.estado.id == 1, 'label label-warning':antena.estado.id == 2, 'label label-danger':antena.estado.id == 3}">{{antena.estado.estado}}</span>
										</td>
										<!--<td>{{antena.proveniente_sitio.nombre_completo}}</td>-->
										<td>{{antena.almacen.almacen}}</td>
										<td style="font-size:8pt;white-space: nowrap;">
											<span ng-if="!antena.logs_bodega_antenas_gul.length || antena.logs_bodega_antenas_gul[0].evento.tipo != 'INSERT'">Almacenado: {{antena.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
											<span ng-if="antena.logs_bodega_antenas_gul[0].evento.tipo == 'INSERT'">Almacenado: {{antena.logs_bodega_antenas_gul[0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{antena.logs_bodega_antenas_gul[0].usuario.nombres}} {{antena.logs_bodega_antenas_gul[0].usuario.apellido_paterno}}</span>
											<br>
											<span ng-if="antena.logs_bodega_antenas_gul[1].evento.tipo == 'UPDATE'">Actualizado: {{antena.logs_bodega_antenas_gul[1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{antena.logs_bodega_antenas_gul[1].usuario.nombres}} {{antena.logs_bodega_antenas_gul[1].usuario.apellido_paterno}}</span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div ng-if="!ctrl.cargando" ng-switch-when="4">
							<table class="table table-responsive table-hover">
								<thead>
									<tr>
										<th>Acciones</th>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Estado</th>
										<th>Almacenado en</th>
										<th>Historial</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="antena in ctrl.antenas_iden">
										<td>
											<div class="dropdown">
												<button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdownAntenasIDENMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
													Acciones
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu" aria-labelledby="dropdownAntenasIDENMenu">
													<li><a href ng-click="ctrl.verAntenaIDEN(antena)"><i class="fa fa-edit"></i> Más Información</a></li>
												</ul>
											</div>
										</td>
										<td>{{antena.tipo_antena_iden.marca}}</td>
										<td>{{antena.tipo_antena_iden.modelo}}</td>
										<td>
											<span style="text-transform: uppercase;" ng-class="{'label label-success':antena.estado.id == 1, 'label label-warning':antena.estado.id == 2, 'label label-danger':antena.estado.id == 3}">{{antena.estado.estado}}</span>
										</td>
										<td>{{antena.almacen.almacen}}</td>
										<td style="font-size:8pt;">
											<span ng-if="!antena.logs_bodega_antenas_iden.length || antena.logs_bodega_antenas_iden[0].evento.tipo != 'INSERT'">Almacenado: {{antena.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
											<span ng-if="antena.logs_bodega_antenas_iden[0].evento.tipo == 'INSERT'">Almacenado: {{antena.logs_bodega_antenas_iden[0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{antena.logs_bodega_antenas_iden[0].usuario.nombres}} {{antena.logs_bodega_antenas_iden[0].usuario.apellido_paterno}}</span>
											<br>
											<span ng-if="antena.logs_bodega_antenas_iden[1].evento.tipo == 'UPDATE'">Actualizado: {{antena.logs_bodega_antenas_iden[1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{antena.logs_bodega_antenas_iden[1].usuario.nombres}} {{antena.logs_bodega_antenas_iden[1].usuario.apellido_paterno}}</span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>