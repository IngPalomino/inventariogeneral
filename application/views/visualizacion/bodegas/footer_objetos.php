<script type="text/javascript">
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			ref.auth.evaluador = (ref.session.evaluador == 1)? '' : 'disabled';
			ref.cargando = false;

			ref.elementos_mw = [];
			ref.sitios = [];
			ref.almacenes = [];
			ref.enlaces = [];

			ref.grupos_electrogenos = [];
			ref.antenas_mw = [];
			ref.antenas_gul = [];
			ref.antenas_iden = [];

			ref.editarGrupoElectrogeno = {};
			ref.editarAntenaMicroondas = {};
			ref.editarAntenaGUL = {};
			ref.editarAntenaIDEN = {};

			ref.reactivar_GrupoElectrogeno = {};
			ref.reactivar_AntenaMicroondas = {};
			ref.reactivar_AntenaGUL = {};
			ref.reactivar_AntenaIDEN = {};

			ref.mover_GrupoElectrogeno = {};
			ref.mover_AntenaMicroondas = {};
			ref.mover_AntenaGUL = {};
			ref.mover_AntenaIDEN = {};

			ref.eliminar_GrupoElectrogeno = {};
			ref.eliminar_AntenaMicroondas = {};
			ref.eliminar_AntenaGUL = {};
			ref.eliminar_AntenaIDEN = {};

			ref.tipo_antena_gul = {};
			ref.tipo_antena_mw = {};
			ref.tipo_antena_iden = {};

			ref.config = {
				valueField: 'id',
				labelField: 'nombre_completo',
				searchField: 'nombre_completo',
				delimiter: '|',
				placeholder: 'Buscar sitio',
				maxItems: 1
			};

			ref.configMicroondas = {
				valueField: 'id',
				labelField: 'elemento_mw',
				searchField: 'elemento_mw',
				delimiter: '|',
				placeholder: 'Buscar elemento',
				maxItems: 1
			};

			ref.listarObjeto = function(id)
			{
				switch(Number(id))
				{
					case 1:
						listarElementosMicroondas();
						break;
					case 2:
						listarGruposElectrogenos();
						break;
					case 3:
						listarAntenasGUL();
						break;
					case 4:
						listarAntenasIDEN();
						break;
				}
			}

			ref.listarElementoMicroondas = function(elemento = null){
				if(elemento)
				{
					ref.cargando = true;
					$http({
						url:"<?php echo base_url("bodegas/elementos_mw/listar");?>/"+elemento,
						method:"GET"
					}).success(function(data){
						ref.cargando = false;
						if(data.length)
						{
							ref.elemento_mw = data;
							/*console.log(data);*/
						}
						else
						{
							ref.elemento_mw = [];
						}
					}).error(function(err){
						ref.cargando = false;
					});
				}
			}

			listarElementosMicroondas = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("elementos_mw/listar");?>",
					method:"GET"
				}).success(function(data){
					ref.cargando = false;
					if(data.length)
					{
						ref.elementos_mw = data;
						/*console.log(data);*/
					}
					else
					{
						ref.elementos_mw = [];
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			listarGruposElectrogenos = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("bodegas/grupos_electrogenos/listar");?>",
					method:"GET"
				}).success(function(data){
					ref.cargando = false;
					if(Object.keys(data).length)
					{
						ref.grupos_electrogenos = data;

						ref.grupos_electrogenos.grupos.forEach(function(dato,ind,objeto){
							try{ objeto[ind].created_at = Date.parse(objeto[ind].created_at); } catch(err){ objeto[ind].created_at = null; }

							objeto[ind].logs_bodega_grupos_electrogenos.forEach(function(log,indx,objetox){
								try{ objetox[indx].created_at = Date.parse(objetox[indx].created_at); } catch(err){ objetox[indx].created_at = null; }
							});
						});
						/*console.log(data);*/
					}
					else
					{
						
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			listarAntenasMicroondas = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("bodegas/antenas_mw/listar");?>",
					method:"GET"
				}).success(function(data){
					ref.cargando = false;
					if(data.length)
					{
						ref.antenas_mw = data;

						ref.antenas_mw.forEach(function(dato,ind,objeto){
							try{ objeto[ind].created_at = Date.parse(objeto[ind].created_at); } catch(err){ objeto[ind].created_at = null; }

							objeto[ind].logs_bodega_antenas_mw.forEach(function(log,indx,objetox){
								try{ objetox[indx].created_at = Date.parse(objetox[indx].created_at); } catch(err) { objetox[indx].created_at = null; }
							});
						});
						/*console.log(data);*/
					}
					else
					{
						
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			listarAntenasGUL = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("bodegas/antenas_gul/listar");?>",
					method:"GET"
				}).success(function(data){
					ref.cargando = false;
					if(data.length)
					{
						ref.antenas_gul = data;

						ref.antenas_gul.forEach(function(dato,ind,objeto){
							objeto[ind].logs_bodega_antenas_gul.forEach(function(log,indx,objetox){
								try{ objetox[indx].created_at = Date.parse(objetox[indx].created_at); } catch(err) { objetox[indx] = null; }
							});
						});
						/*console.log(data);*/
					}
					else
					{
						
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			listarAntenasIDEN = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("bodegas/antenas_iden/listar");?>",
					method:"GET"
				}).success(function(data){
					ref.cargando = false;
					if(data.length)
					{
						ref.antenas_iden = data;

						ref.antenas_iden.forEach(function(dato,ind,objeto){
							objeto[ind].logs_bodega_antenas_iden.forEach(function(log,indx,objetox){
								try{ objetox[indx].created_at = Date.parse(objetox[indx].created_at); } catch(err) { objetox[indx] = null; }
							});
						});
						/*console.log(data);*/
					}
					else
					{
						
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			listarSitios = function(){
				$http({
					url:"<?php echo base_url("status_site/listar_sitios_total");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sitios = data;
					}
				}).error(function(err){

				});
			}

			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.almacenes = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarTipoGrupoElectrogeno = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_grupo_electrogeno/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_grupo_electrogeno = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoMotorGe = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_motor_ge/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_motor_ge = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoGeneradorGe = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_generador_ge/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_generador_ge = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoAVRGe = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_avr_ge/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_avr_ge = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoControladorGe = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_controlador_ge/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_controlador_ge = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			listarTipoControladorTTA = function(){
				$http({
					url:"<?php echo base_url("index.php/tipo_controlador_tta/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_controlador_tta = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			listarTipoAntenasGUL = function(){
				$http({
					url:"<?php echo base_url("tipo_antena_gul/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_antena_gul = data;
					}
				}).error(function(err){

				});
			}

			listarTipoAntenaMW = function(){
				$http({
					url:"<?php echo base_url("tipo_antena_mw/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_antena_mw = data;
					}
				}).error(function(err){

				});
			}

			listarTipoAntenaIDEN = function(){
				$http({
					url:"<?php echo base_url("tipo_antena_iden/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.marcas.length)
					{
						ref.tipo_antena_iden = data;
					}
				}).error(function(err){

				});
			}

			ref.editandoGrupoElectrogeno = function(info){
				ref.editarGrupoElectrogeno = info;

				ref.editarGrupoElectrogeno.tipo_generador_ge.potencia = function(){
					for(var i = 0; i < ref.tipo_generador_ge.modelos.length; i++)
					{
						if(ref.tipo_generador_ge.modelos[i]["id"] == ref.editarGrupoElectrogeno.tipo_generador_ge.id)
						{
							return Number(ref.tipo_generador_ge.modelos[i].potencia);
						}
					}
				};

				ref.editarGrupoElectrogeno.tipo_generador_ge.corriente = function(){
					for(var i = 0; i < ref.tipo_generador_ge.modelos.length; i++)
					{
						if(ref.tipo_generador_ge.modelos[i]["id"] == ref.editarGrupoElectrogeno.tipo_generador_ge.id)
						{
							return Number(ref.tipo_generador_ge.modelos[i].corriente);
						}
					}
				};

				$("#editarGrupoElectrogeno").bPopup({
					amsl: 0
				});
			}

			ref.grabarGrupoElectrogeno = function(grupo){
				var data = "",
					campos = Object.keys(grupo),
					contador = 0;
				
				campos.forEach(function(campo,ind,objeto){
					if(campo != "id" && campo != "created_at" && campo != "$$hashKey" && campo != "editar" && campo != "sitio" && campo != "eliminado" && campo != "disabled" && campo != "index" && !campo.includes("logs",0))
					{
						if(grupo[campo] != null)
						{
							if(typeof grupo[campo] != "object")
							{
								if(contador > 0)
								{
									data += "&";
								}
								data += campo+"="+grupo[campo];
							}
							else
							{
								if(grupo[campo].length != undefined)
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+JSON.stringify(grupo[campo]);
								}
								else
								{
									if(contador > 0)
									{
										data += "&";
									}
									data += campo+"="+grupo[campo].id;
								}
							}

							contador++;
						}
					}
				});
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("bodegas/grupos_electrogenos/grupo_electrogeno");?>/"+grupo.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					listarGruposElectrogenos();
					$("#editarGrupoElectrogeno").bPopup().close();
				}).error(function(err){

				});
			}

			ref.cancelarEdicionGrupoElectrogeno = function(){
				ref.editarGrupoElectrogeno = {};
				$("#editarGrupoElectrogeno").bPopup().close();
				listarGruposElectrogenos();
			}

			ref.reactivandoGrupoElectrogeno = function(info){
				ref.reactivar_GrupoElectrogeno = info;
				$("#reactivarGrupoElectrogeno").bPopup({
					amsl: 0
				});
			}

			ref.reactivarGrupoElectrogeno = function(grupo){
				var data = "sitio="+grupo.sitio
						+"&tipo_grupo_electrogeno="+grupo.tipo_grupo_electrogeno.id
						+"&numero_serie_grupo_electrogeno="+grupo.numero_serie_grupo_electrogeno
						+"&tipo_motor_ge="+grupo.tipo_motor_ge.id
						+"&numero_serie_motor="+grupo.numero_serie_motor
						+"&tipo_generador_ge="+grupo.tipo_generador_ge.id
						+"&numero_serie_generador="+grupo.numero_serie_generador
						+"&tipo_avr_ge="+grupo.tipo_avr_ge.id
						+"&numero_serie_avr="+grupo.numero_serie_avr
						+"&tipo_controlador_ge="+grupo.tipo_controlador_ge.id
						+"&numero_serie_controlador_ge="+grupo.numero_serie_controlador_ge
						+"&tipo_controlador_tta="+grupo.tipo_controlador_tta.id
						+"&numero_serie_controlador_tta="+grupo.numero_serie_controlador_tta
						+"&comentario="+grupo.comentario;
				/*console.log(data);*/

				$http({
					url:"<?php echo base_url("bodegas/grupos_electrogenos/grupo_electrogeno");?>/"+grupo.id,
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#reactivarGrupoElectrogeno").bPopup().close();
					ref.grupos_electrogenos = [];
					listarGruposElectrogenos();
					ref.reactivar_GrupoElectrogeno = {};
				}).error(function(err){

				});
			}

			ref.cancelarReactivacionGrupoElectrogeno = function(){
				$("#reactivarGrupoElectrogeno").bPopup().close();
				ref.reactivar_GrupoElectrogeno = {};
			}

			ref.moviendoGrupoElectrogeno = function(info){
				ref.mover_GrupoElectrogeno = info;
				ref.mover_GrupoElectrogeno.almacen = "";
				ref.mover_GrupoElectrogeno.a_sitio = "";
				ref.mover_GrupoElectrogeno.tipo_almacen = 1;

				$("#moverGrupoElectrogeno").bPopup({
					amsl: 0
				});
			}

			ref.moverGrupoElectrogeno = function(grupo){
				var data = "tipo_almacen="+grupo.tipo_almacen
						+"&almacen="+((grupo.tipo_almacen == 1)? grupo.almacen : grupo.a_sitio)
						+"&comentario="+grupo.comentario;
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("bodegas/grupos_electrogenos/grupo_electrogeno/mover");?>/"+grupo.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					ref.mover_GrupoElectrogeno = {};
					listarGruposElectrogenos();
					$("#moverGrupoElectrogeno").bPopup().close();
				}).error(function(err){

				});
			}

			ref.cancelarMovimientoGrupoElectrogeno = function(){
				ref.mover_GrupoElectrogeno = {};
				$("#moverGrupoElectrogeno").bPopup().close();
				listarGruposElectrogenos();
			}

			ref.eliminandoGrupoElectrogeno = function(info){
				ref.eliminar_GrupoElectrogeno = info;
				$("#eliminarGrupoElectrogeno").bPopup();
			}

			ref.eliminarGrupoElectrogeno = function(){
				var data = "comentario="+ref.eliminar_GrupoElectrogeno.comentario;

				$http({
					url:"<?php echo base_url("bodegas/grupos_electrogenos/grupo_electrogeno");?>"+"/"+ref.eliminar_GrupoElectrogeno.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.grupos_electrogenos = [];
						listarGruposElectrogenos();
						ref.eliminar_GrupoElectrogeno = {};
						$('#eliminarGrupoElectrogeno').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEliminacionGrupoElectrogeno = function(){
				$("#eliminarGrupoElectrogeno").bPopup().close();
				ref.eliminar_GrupoElectrogeno = {};
			}

			ref.grupoElectrogenoOpcionSitio = function(){
				ref.mover_GrupoElectrogeno.tipo_almacen = 2;
			}

			ref.grupoElectrogenoOpcionAlmacen = function(){
				ref.mover_GrupoElectrogeno.tipo_almacen = 1;
			}

			ref.editandoAntenaMicroondas = function(info){
				ref.editarAntenaMicroondas = info;
				$("#editarAntenaMicroondas").bPopup({
					amsl: 0
				});
			}

			ref.grabarAntenaMicroondas = function(antena){
				var data = "tipo_antena_mw="+antena.tipo_antena_mw.id
						+"&numero_serie="+antena.numero_serie
						+"&estado="+antena.estado.id
						+"&proveniente_sitio="+antena.proveniente_sitio.id;

				$http({
					url:"<?php echo base_url("bodegas/antenas_mw/antena_mw");?>/"+antena.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					listarAntenasMicroondas();
					$("#editarAntenaMicroondas").bPopup().close();
				}).error(function(err){

				});
			}

			ref.cancelarEdicionAntenaMicroondas = function(){
				$("#editarAntenaMicroondas").bPopup().close();
				ref.editarAntenaMicroondas = {};
				listarAntenasMicroondas();
			}

			ref.reactivandoAntenaMicroondas = function(info){
				ref.reactivar_AntenaMicroondas = info;
				$("#reactivarAntenaMicroondas").bPopup({
					amsl: 0
				});
			}

			ref.reactivarAntenaMicroondas = function(antena){
				var data = "tipo_antena_mw="+antena.tipo_antena_mw.id
						+"&numero_serie="+antena.numero_serie
						+"&altura="+antena.altura
						+"&azimuth="+antena.azimuth
						+"&enlace="+antena.enlace
						+"&idu="+antena.idu
						+"&comentario="+antena.comentario;
				/*console.log(data);*/

				$http({
					url:"<?php echo base_url("bodegas/antenas_mw/antena_mw");?>/"+antena.id,
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#reactivarAntenaMicroondas").bPopup().close();
					ref.antenas_mw = [];
					listarAntenasMicroondas();
					ref.reactivar_AntenaMicroondas = {};
				}).error(function(err){

				});
			}

			ref.cancelarReactivacionAntenaMicroondas = function(){
				$("#reactivarAntenaMicroondas").bPopup().close();
				ref.reactivar_AntenaMicroondas = {};
				listarAntenasMicroondas();
				ref.enlaces = [];
			}

			ref.moviendoAntenaMicroondas = function(info){
				ref.mover_AntenaMicroondas = info;
				ref.mover_AntenaMicroondas.almacen = "";
				ref.mover_AntenaMicroondas.a_sitio = "";
				ref.mover_AntenaMicroondas.tipo_almacen = 1;

				$("#moverAntenaMicroondas").bPopup({
					amsl: 0
				});
			}

			ref.moverAntenaMicroondas = function(antena){
				var data = "tipo_almacen="+antena.tipo_almacen
						+"&almacen="+((antena.tipo_almacen == 1)? antena.almacen : antena.a_sitio)
						+"&comentario="+antena.comentario;
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("bodegas/antenas_mw/antena_mw/mover");?>/"+antena.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					ref.mover_AntenaMicroondas = {};
					listarAntenasMicroondas();
					$("#moverAntenaMicroondas").bPopup().close();
				}).error(function(err){

				});
			}

			ref.cancelarMovimientoAntenaMicroondas = function(){
				ref.mover_AntenaMicroondas = {};
				$("#moverAntenaMicroondas").bPopup().close();
				listarAntenasMicroondas();
			}

			ref.antenaMicroondasOpcionSitio = function(){
				ref.mover_AntenaMicroondas.tipo_almacen = 2;
			}

			ref.antenaMicroondasOpcionAlmacen = function(){
				ref.mover_AntenaMicroondas.tipo_almacen = 1;
			}

			ref.eliminandoAntenaMicroondas = function(info){
				ref.eliminar_AntenaMicroondas = info;
				$("#eliminarAntenaMicroondas").bPopup();
			}

			ref.eliminarAntenaMicroondas = function(){
				var data = "comentario="+ref.eliminar_AntenaMicroondas.comentario;

				$http({
					url:"<?php echo base_url("bodegas/antenas_mw/antena_mw");?>"+"/"+ref.eliminar_AntenaMicroondas.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.antenas_mw = [];
						listarAntenasMicroondas();
						ref.eliminar_AntenaMicroondas = {};
						$('#eliminarAntenaMicroondas').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEliminacionAntenaMicroondas = function(){
				$("#eliminarAntenaMicroondas").bPopup().close();
				ref.eliminar_AntenaMicroondas = {};
			}

			ref.listarEnlaces = function(sitio){
				$http({
					url:"<?php echo base_url("enlaces/listar_sitio");?>"+"/"+sitio,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.enlaces = data;
					}
					else
					{
						ref.enlaces = [];
					}
				}).error(function(err){
					
				});
			}

			ref.filtroAntenaMicroondas = function(a){
				var r = true;
				
				if(a.id != ref.reactivar_AntenaMicroondas.enlace)
				{
					r = false;
				}
				
				return r;
			}

			ref.editandoAntenaGUL = function(info){
				ref.editarAntenaGUL = info;
				$("#editarAntenaGUL").bPopup({
					amsl: 0
				});
			}

			ref.grabarAntenaGUL = function(antena){
				var data = "tipo_antena_gul="+antena.tipo_antena_gul.id
						+"&numero_serie="+antena.numero_serie
						+"&estado="+antena.estado.id
						+"&proveniente_sitio="+antena.proveniente_sitio.id;

				$http({
					url:"<?php echo base_url("bodegas/antenas_gul/antena_gul");?>/"+antena.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					listarAntenasGUL();
					$("#editarAntenaGUL").bPopup().close();
				}).error(function(err){

				});
			}

			ref.cancelarEdicionAntenaGUL = function(){
				ref.editarAntenaGUL = {};
				$("#editarAntenaGUL").bPopup().close();
				listarAntenasGUL();
			}

			ref.reactivandoAntenaGUL = function(info){
				ref.reactivar_AntenaGUL = info;
				$("#reactivarAntenaGUL").bPopup({
					amsl: 0
				});

				ref.reactivar_AntenaGUL.crearExtras = function(){
					this.sector = "";
					this.azimuth = "";
					this.tilt_electrico = "";
					this.tilt_mecanico = "";
				}

				if(ref.reactivar_AntenaGUL.tipo_antena_gul.marca != "TELNET")
				{
					ref.reactivar_AntenaGUL.info_telnet = [];
				}
				else
				{
					ref.reactivar_AntenaGUL.sector = "";
					ref.reactivar_AntenaGUL.azimuth = "";
					ref.reactivar_AntenaGUL.tilt_electrico = "";
					ref.reactivar_AntenaGUL.tilt_mecanico = "";
					ref.reactivar_AntenaGUL.info_telnet = [];

					for(var i = 0; i < 3; i++)
					{
						ref.reactivar_AntenaGUL.info_telnet.push(new ref.reactivar_AntenaGUL.crearExtras());
					}
				}
			}

			ref.reactivarAntenaGUL = function(antena){
				borrarPropiedades(antena,"crearExtras");
				borrarPropiedades(antena.info_telnet,"$$hashKey");

				var data = "tipo_antena_gul="+antena.tipo_antena_gul.id
						+"&numero_serie="+antena.numero_serie
						+"&sitio="+antena.sitio
						+"&altura="+antena.altura
						+"&sector="+antena.sector
						+"&azimuth="+antena.azimuth
						+"&tilt_electrico="+antena.tilt_electrico
						+"&tilt_mecanico="+antena.tilt_mecanico
						+"&info_telnet="+JSON.stringify(antena.info_telnet)
						+"&comentario="+antena.comentario;
				/*console.log(data);*/

				$http({
					url:"<?php echo base_url("bodegas/antenas_gul/antena_gul");?>/"+antena.id,
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					$("#reactivarAntenaGUL").bPopup().close();
					ref.antenas_gul = [];
					listarAntenasGUL();
					ref.reactivar_AntenaGUL = {};
				}).error(function(err){

				});
			}

			ref.cancelarReactivacionAntenaGUL = function(){
				$("#reactivarAntenaGUL").bPopup().close();
				ref.reactivar_AntenaGUL = {};
			}

			ref.moviendoAntenaGUL = function(info){
				ref.mover_AntenaGUL = info;
				ref.mover_AntenaGUL.almacen = "";
				ref.mover_AntenaGUL.a_sitio = "";
				ref.mover_AntenaGUL.tipo_almacen = 1;

				$("#moverAntenaGUL").bPopup({
					amsl: 0
				});
			}

			ref.moverAntenaGUL = function(antena){
				var data = "tipo_almacen="+antena.tipo_almacen
						+"&almacen="+((antena.tipo_almacen == 1)? antena.almacen : antena.a_sitio)
						+"&comentario="+antena.comentario;
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("bodegas/antenas_gul/antena_gul/mover");?>/"+antena.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					ref.mover_AntenaGUL = {};
					listarAntenasGUL();
					$("#moverAntenaGUL").bPopup().close();
				}).error(function(err){

				});
			}

			ref.cancelarMovimientoAntenaGUL = function(){
				ref.mover_AntenaGUL = {};
				$("#moverAntenaGUL").bPopup().close();
				listarAntenasGUL();
			}

			ref.antenaGULOpcionSitio = function(){
				ref.mover_AntenaGUL.tipo_almacen = 2;
			}

			ref.antenaGULOpcionAlmacen = function(){
				ref.mover_AntenaGUL.tipo_almacen = 1;
			}

			ref.eliminandoAntenaGUL = function(info){
				ref.eliminar_AntenaGUL = info;
				$("#eliminarAntenaGUL").bPopup();
			}

			ref.eliminarAntenaGUL = function(){
				var data = "comentario="+ref.eliminar_AntenaGUL.comentario;

				$http({
					url:"<?php echo base_url("bodegas/antenas_gul/antena_gul");?>"+"/"+ref.eliminar_AntenaGUL.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.antenas_gul = [];
						listarAntenasGUL();
						ref.eliminar_AntenaGUL = {};
						$('#eliminarAntenaGUL').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEliminacionAntenaGUL = function(){
				ref.eliminar_AntenaGUL = {};
				$("#eliminarAntenaGUL").bPopup().close();
			}

			ref.editandoAntenaIDEN = function(info){
				ref.editarAntenaIDEN = info;
				$("#editarAntenaIDEN").bPopup({
					amsl: 0
				});
			}

			ref.grabarAntenaIDEN = function(antena){
				var data = "tipo_antena_iden="+antena.tipo_antena_iden.id
						+"&numero_serie="+antena.numero_serie
						+"&estado="+antena.estado.id
						+"&proveniente_sitio="+antena.proveniente_sitio.id;

				$http({
					url:"<?php echo base_url("bodegas/antenas_iden/antena_iden");?>/"+antena.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					listarAntenasIDEN();
					ref.editarAntenaIDEN = {};
					$("#editarAntenaIDEN").bPopup().close();
				}).error(function(err){

				});
			}

			ref.cancelarEdicionAntenaIDEN = function(){
				ref.editarAntenaIDEN = {};
				$("#editarAntenaIDEN").bPopup().close();
				listarAntenasIDEN();
			}

			ref.moviendoAntenaIDEN = function(info){
				ref.mover_AntenaIDEN = info;
				ref.mover_AntenaIDEN.almacen = "";
				ref.mover_AntenaIDEN.a_sitio = "";
				ref.mover_AntenaIDEN.tipo_almacen = 1;

				$("#moverAntenaIDEN").bPopup({
					amsl: 0
				});
			}

			ref.moverAntenaIDEN = function(antena){
				var data = "tipo_almacen="+antena.tipo_almacen
						+"&almacen="+((antena.tipo_almacen == 1)? antena.almacen : antena.a_sitio)
						+"&comentario="+antena.comentario;
				/*console.log(data);*/
				$http({
					url:"<?php echo base_url("bodegas/antenas_iden/antena_iden/mover");?>/"+antena.id,
					method:"PUT",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					ref.mover_AntenaIDEN = {};
					listarAntenasIDEN();
					$("#moverAntenaIDEN").bPopup().close();
				}).error(function(err){

				});
			}

			ref.cancelarMovimientoAntenaIDEN = function(){
				ref.mover_AntenaIDEN = {};
				$("#moverAntenaIDEN").bPopup().close();
				listarAntenasIDEN();
			}

			ref.antenaIDENOpcionSitio = function(){
				ref.mover_AntenaIDEN.tipo_almacen = 2;
			}

			ref.antenaIDENOpcionAlmacen = function(){
				ref.mover_AntenaIDEN.tipo_almacen = 1;
			}

			ref.eliminandoAntenaIDEN = function(info){
				ref.eliminar_AntenaIDEN = info;
				$("#eliminarAntenaIDEN").bPopup();
			}

			ref.eliminarAntenaIDEN = function(){
				var data = "comentario="+ref.eliminar_AntenaIDEN.comentario;

				$http({
					url:"<?php echo base_url("bodegas/antenas_iden/antena_iden");?>"+"/"+ref.eliminar_AntenaIDEN.id,
					method:"DELETE",
					data: data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{

					}
					else
					{
						ref.antenas_iden = [];
						listarAntenasIDEN();
						ref.eliminar_AntenaIDEN = {};
						$('#eliminarAntenaIDEN').bPopup().close();
					}
				}).error(function(err){

				});
			}

			ref.cancelarEliminacionAntenaIDEN = function(){
				ref.eliminar_AntenaIDEN = {};
				$("#eliminarAntenaIDEN").bPopup().close();
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			borrarPropiedades = function(objeto,propiedad){
				var campos = Object.keys(objeto);

				campos.forEach(function(campo,ind,obj){
					if(campo == propiedad)
					{
						delete objeto[propiedad];
					}

					if((typeof objeto[campo] == 'object') && (objeto[campo] != null))
					{
						objeto[campo] = borrarPropiedades(objeto[campo],propiedad);
					}
				});

				return objeto;
			}

			listarSitios();
			listarAlmacenes();
			listarTipoGrupoElectrogeno();
			listarTipoMotorGe();
			listarTipoGeneradorGe();
			listarTipoAVRGe();
			listarTipoControladorGe();
			listarTipoControladorTTA();
			listarTipoAntenasGUL();
			listarTipoAntenaMW();
			listarTipoAntenaIDEN();
		}])
		.directive('bsPopover',function(){
			return function(scope,element,attrs){
				element.find("span[rel=popover]").popover();
			}
		});
</script>
</body>
</html>