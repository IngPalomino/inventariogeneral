<script type="text/javascript">
	angular
		.module('app',['selectize'])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			ref.auth.evaluador = (ref.session.evaluador == 1)? '' : 'disabled';

			ref.cargando = false;
			ref.almacenes = [];

			ref.grupos_electrogenos = [];
			ref.antenas_mw = [];
			ref.antenas_gul = [];
			ref.antenas_iden = [];

			ref.editarGrupoElectrogeno = {};
			ref.editarAntenaMicroondas = {};
			ref.editarAntenaGUL = {};
			ref.editarAntenaIDEN = {};

			ref.objetos = [
				{id: 1, objeto: "Grupos Electrógenos"},
				{id: 2, objeto: "Antenas Microondas"},
				{id: 3, objeto: "Antenas GUL"},
				{id: 4, objeto: "Antenas iDEN"}
			];

			ref.listarObjeto = function(id,almacen)
			{
				switch(Number(id))
				{
					case 1:
						listarGruposElectrogenos(almacen);
						break;
					case 2:
						listarAntenasMicroondas(almacen);
						break;
					case 3:
						listarAntenasGUL(almacen);
						break;
					case 4:
						listarAntenasIDEN(almacen);
						break;
				}
			}

			listarAlmacenes = function(){
				$http({
					url:"<?php echo base_url("almacenes/listar");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.almacenes = data;
					}
					else
					{
						
					}
				}).error(function(err){

				});
			}

			listarGruposElectrogenos = function(almacen){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("bodegas/grupos_electrogenos/listar_almacen");?>/"+almacen,
					method:"GET"
				}).success(function(data){
					if(Object.keys(data).length)
					{
						ref.grupos_electrogenos = data;
						ref.cargando = false;
					}
					else
					{
						ref.grupos_electrogenos = [];
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			listarAntenasMicroondas = function(almacen){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("bodegas/antenas_mw/listar_almacen");?>/"+almacen,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenas_mw = data;
						ref.cargando = false;
					}
					else
					{
						ref.antenas_mw = [];
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			listarAntenasGUL = function(almacen){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("bodegas/antenas_gul/listar_almacen");?>/"+almacen,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenas_gul = data;
						ref.cargando = false;
					}
					else
					{
						ref.antenas_gul = [];
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			listarAntenasIDEN = function(almacen){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("bodegas/antenas_iden/listar_almacen");?>/"+almacen,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenas_iden = data;
						ref.cargando = false;
					}
					else
					{
						ref.antenas_iden = [];
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			ref.verGrupoElectrogeno = function(info){
				ref.editarGrupoElectrogeno = info;
				$("#verGrupoElectrogeno").bPopup({
					amsl: 0
				});
			}

			ref.verAntenaMicroondas = function(info){
				ref.editarAntenaMicroondas = info;
				$("#verAntenaMicroondas").bPopup({
					amsl: 0
				});
			}

			ref.verAntenaGUL = function(info){
				ref.editarAntenaGUL = info;
				$("#verAntenaGUL").bPopup({
					amsl: 0
				});
			}

			ref.verAntenaIDEN = function(info){
				ref.editarAntenaIDEN = info;
				$("#verAntenaIDEN").bPopup({
					amsl: 0
				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			listarAlmacenes();
		}]);
</script>
</body>
</html>