<br>
<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4">
			<div class="form-group">
				<label>Almacenes</label>
				<div class="input-group">
					
					<selectize config="ctrl.configAlmacenes" options="ctrl.almacenes" ng-model="ctrl.busqueda.almacen" ng-change="ctrl.searched = false"></selectize>
					
					<div class="input-group-btn">
						<button class="btn btn-default" ng-click="ctrl.buscarAlmacenes(ctrl.busqueda.almacen)" ng-disabled="!ctrl.busqueda.almacen || ctrl.loading">
							<i class="fa fa-search" ng-show="!ctrl.loading"></i>
							<i class="fa fa-spinner fa-spin" ng-show="ctrl.loading"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4">
			<div class="form-group">
				<label>Sitios</label>
				<div class="input-group">
					<selectize config="ctrl.configSitios" options="ctrl.sitios" ng-model="ctrl.busqueda.sitio" ng-change="ctrl.searched = false"></selectize>
					<div class="input-group-btn">
						<button class="btn btn-default" ng-click="ctrl.buscarSitios(ctrl.busqueda.sitio)" ng-disabled="!ctrl.busqueda.sitio || ctrl.loading">
							<i class="fa fa-search" ng-show="!ctrl.loading"></i>
							<i class="fa fa-spinner fa-spin" ng-show="ctrl.loading"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4">
			<div class="form-group">
				<label>Objetos</label>
				<div class="input-group">
					<selectize config="ctrl.configObjetos" options="ctrl.objetos" ng-model="ctrl.busqueda.objeto" ng-change="ctrl.searched = false"></selectize>
					<div class="input-group-btn">
						<button class="btn btn-default" ng-click="ctrl.buscarObjetos(ctrl.busqueda.objeto)" ng-disabled="!ctrl.busqueda.objeto || ctrl.loading">
							<i class="fa fa-search" ng-show="!ctrl.loading"></i>
							<i class="fa fa-spinner fa-spin" ng-show="ctrl.loading"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="row" ng-if="ctrl.elementos_busqueda.length && !ctrl.loading">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Resultados
				</div>
				<div class="panel-body">
					<div class="panel-group" id="accordion">
						<div ng-if="objeto.cantidad >0"  class="panel panel-default" ng-repeat="objeto in ctrl.elementos_busqueda">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a ng-href="#{{objeto.link}}" data-toggle="collapse" data-parent="#accordion">{{objeto.objeto}} <span class="badge">{{objeto.cantidad}}</span></a>
								</h4>
							</div>
							<div id="{{objeto.link}}" class="panel-collapse collapse">
								<div class="panel-body" style="max-height:270px;overflow:auto;">
									<div ng-switch on="objeto.link">
										<div ng-switch-default>
											<table class="table table-condensed table-condensed table-responsive">
												<thead>
													<tr>
														<th><i class="fa fa-cogs"></i></th>
														<th>Marca</th>
														<th>Modelo</th>
														<th>Estado</th>
														<th>Almacen</th>
														<th>Historial</th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="elemento in objeto.info">
														<td><button class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></button></td>
														<td>{{elemento[objeto.tipo].marca}}</td>
														<td>{{elemento[objeto.tipo].modelo}}</td>
														<td>
															<span style="text-transform:uppercase;" ng-class="{'label label-success':elemento.estado.id == 1, 'label label-warning':elemento.estado.id == 2, 'label label-danger':elemento.estado.id == 3}">
																{{elemento.estado.estado}}
															</span>
														</td>
														<td>{{elemento.almacen.almacen}}</td>
														<td style="font-size:8pt;list-style:none;">
															<span ng-if="!elemento[objeto.logs].length || elemento[objeto.logs][0].evento.tipo != 'INSERT'">Almacenado: {{elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
															<span ng-if="elemento[objeto.logs][0].evento.tipo == 'INSERT'">Almacenado: {{elemento[objeto.logs][0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][0].usuario.nombres}} {{elemento[objeto.logs][0].usuario.apellido_paterno}}</span>
															<br>
															<span ng-if="elemento[objeto.logs][1].evento.tipo == 'UPDATE'">Actualizado: {{elemento[objeto.logs][1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][1].usuario.nombres}} {{elemento[objeto.logs][1].usuario.apellido_paterno}}</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									
										<div ng-switch-when="elementos_mw">
											<table class="table table-condensed table-condensed table-responsive">
												<thead>
													<tr>
														<th><i class="fa fa-cogs"></i></th>
														<th>Elemento</th>
														<th>Marca</th>
														<th>Modelo</th>
														<th>Estado</th>
														<th>Almacen</th>
														<th>Historial</th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="elemento in objeto.info">
														<td><button class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></button></td>
														<td>{{elemento.elemento_mw.elemento_mw}}</td>
														<td>{{elemento[objeto.tipo].marca}}</td>
														<td>{{elemento[objeto.tipo].modelo}}</td>
														<td>
															<span style="text-transform:uppercase;" ng-class="{'label label-success':elemento.estado.id == 1, 'label label-warning':elemento.estado.id == 2, 'label label-danger':elemento.estado.id == 3}">
																{{elemento.estado.estado}}
															</span>
														</td>
														<td>{{elemento.almacen.almacen}}</td>
														<td style="font-size:8pt;list-style:none;">
															<span ng-if="!elemento[objeto.logs].length || elemento[objeto.logs][0].evento.tipo != 'INSERT'">Almacenado: {{elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
															<span ng-if="elemento[objeto.logs][0].evento.tipo == 'INSERT'">Almacenado: {{elemento[objeto.logs][0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][0].usuario.nombres}} {{elemento[objeto.logs][0].usuario.apellido_paterno}}</span>
															<br>
															<span ng-if="elemento[objeto.logs][1].evento.tipo == 'UPDATE'">Actualizado: {{elemento[objeto.logs][1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][1].usuario.nombres}} {{elemento[objeto.logs][1].usuario.apellido_paterno}}</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>

<!-- sistema combustible  -->
										<div ng-switch-when="elementos_sistema_combustible">
											<table class="table table-condensed table-condensed table-responsive">
												<thead>
													<tr>
														<th><i class="fa fa-cogs"></i></th>
														<th>Elemento</th>
														<th>Marca</th>
														<th>Modelo</th>
														<th>Nro. Serie</th>
														<th>Estado</th>
														<th>Almacen</th>
														<th>Historial</th>
													</tr>
												</thead>
												<tbody>


										<tr ng-repeat="elemento in ctrl.datainfo">
													
                                          <td><button class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></button></td>
														<td>{{elemento.elemento_sistema_combustible.elemento_sistema_combustible}}</td>

														<td>{{elemento[objeto.tipo].marca}}</td>
														<td>{{elemento[objeto.tipo].modelo}}</td>
									<td>{{elemento.numero_serie}}</td>
														<td>
															<span style="text-transform:uppercase;" ng-class="{'label label-success':elemento.estado.id == 1, 'label label-warning':elemento.estado.id == 2, 'label label-danger':elemento.estado.id == 3}">
																{{elemento.estado.estado}}
															</span>
														</td>
														<td>{{elemento.almacen.almacen}}</td>
														<td style="font-size:8pt;list-style:none;">
															<span ng-if="!elemento[objeto.logs].length || elemento[objeto.logs][0].evento.tipo != 'INSERT'">Almacenado: {{elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
															<span ng-if="elemento[objeto.logs][0].evento.tipo == 'INSERT'">Almacenado: {{elemento[objeto.logs][0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][0].usuario.nombres}} {{elemento[objeto.logs][0].usuario.apellido_paterno}}</span>
															<br>
															<span ng-if="elemento[objeto.logs][1].evento.tipo == 'UPDATE'">Actualizado: {{elemento[objeto.logs][1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][1].usuario.nombres}} {{elemento[objeto.logs][1].usuario.apellido_paterno}}</span>
														</td>
													</tr>



												</tbody>
											</table>
										</div>
<!-- sistema combustible  -->






										<div ng-switch-when="tanques_combustible">
											<table class="table table-condensed table-condensed table-responsive">
												<thead>
													<tr>
														<th><i class="fa fa-cogs"></i></th>
														<th>Tipo</th>
														<th>Estado</th>
														<th>Almacen</th>
														<th>Historial</th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="elemento in objeto.info">
														<td><button class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></button></td>
														<td>
															<span ng-if="elemento[objeto.tipo] == 1">Tanque Interno</span>
															<span ng-if="elemento[objeto.tipo] == 2">Tanque Externo</span>
														</td>
														<td>
															<span style="text-transform:uppercase;" ng-class="{'label label-success':elemento.estado.id == 1, 'label label-warning':elemento.estado.id == 2, 'label label-danger':elemento.estado.id == 3}">
																{{elemento.estado.estado}}
															</span>
														</td>
														<td>{{elemento.almacen.almacen}}</td>
														<td style="font-size:8pt;list-style:none;">
															<span ng-if="!elemento[objeto.logs].length || elemento[objeto.logs][0].evento.tipo != 'INSERT'">Almacenado: {{elemento.created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por Base de Datos</span>
															<span ng-if="elemento[objeto.logs][0].evento.tipo == 'INSERT'">Almacenado: {{elemento[objeto.logs][0].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][0].usuario.nombres}} {{elemento[objeto.logs][0].usuario.apellido_paterno}}</span>
															<br>
															<span ng-if="elemento[objeto.logs][1].evento.tipo == 'UPDATE'">Actualizado: {{elemento[objeto.logs][1].created_at | date:"dd/MM/yyyy - HH:mm:ss"}} por {{elemento[objeto.logs][1].usuario.nombres}} {{elemento[objeto.logs][1].usuario.apellido_paterno}}</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="alert alert-danger" ng-if="!ctrl.loading && !ctrl.elementos_busqueda.length && ctrl.searched && (ctrl.busqueda.almacen || ctrl.busqueda.sitio || ctrl.busqueda.objeto)">
				<i class="fa fa-exclamation-triangle"></i>
				<span ng-if="ctrl.busqueda.almacen">No hay elementos almacenados en esta bodega.</span>
				<span ng-if="ctrl.busqueda.sitio">No hay elementos almacenados en este sitio.</span>
				<span ng-if="ctrl.busqueda.objeto">No se han almacenado {{ctrl.busqueda.objeto}} en alguna bodega.</span>
			</div>
		</div>
	</div>
</div>