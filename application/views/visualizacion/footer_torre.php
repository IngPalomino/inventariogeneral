<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.info_torre= [];

			ref.colorNivelRiesgo = {};

			colorNivelRiesgo = function(x)
			{
				var color = {
					R: 0,
					G: 0,
					B: 0
				};

				if(x >= 1 && x <= 5)
				{
					color.R = 51*x;
					color.G = 255;
					color.B = 0;
				}
				else if(x >= 6 && x <= 9)
				{
					color.R = 255;
					color.G = 573.75 - 63.75*x;
					color.B = 0;
				}

				return color;
			}

			torre = function(){
				$http({
					url:"<?php echo base_url("index.php/status_site/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						data[0].latitud = Number(data[0].latitud);
						data[0].longitud = Number(data[0].longitud);
						data[0].altura_edificacion = Number(data[0].altura_edificacion);
						data[0].altura_torre = Number(data[0].altura_torre);
						data[0].factor_uso_torre = (data[0].factor_uso_torre? Number(data[0].factor_uso_torre) : null);
						data[0].anio_construccion = Number(data[0].anio_construccion);
						data[0].acceso_libre_24h = Boolean(Number(data[0].acceso_libre_24h));
						data[0].in_building = Boolean(Number(data[0].in_building));
						/*data[0].coubicado = Boolean(Number(data[0].coubicado));*/
						data[0].agregador = Boolean(Number(data[0].agregador));
						data[0].dorsal = Boolean(Number(data[0].dorsal));
						try{ data[0].fecha_estado_on_air = new Date(data[0].fecha_estado_on_air + " 00:00:00"); }catch(err){ data[0].fecha_estado_on_air = null; }

						data[0].contratos.forEach(function(contrato,ind,objeto){
							objeto[ind].area = Number(objeto[ind].area);
							try{ objeto[ind].fecha_inicio = new Date(objeto[ind].fecha_inicio + " 00:00:00"); }catch(err){ objeto[ind].fecha_inicio = null; }
							try{ objeto[ind].fecha_fin = new Date(objeto[ind].fecha_fin + " 00:00:00"); }catch(err){ objeto[ind].fecha_fin = null; }

							contrato.contactos_contrato.forEach(function(contacto,indx,objx){
								objx[indx].telefono = JSON.parse(contacto.telefono);
								objx[indx].correo = JSON.parse(contacto.correo);
							});
						});

						ref.info_torre = data[0];
						/*ref.colorNivelRiesgo = colorNivelRiesgo(ref.info_torre.nivel_riesgo);*/
						
					/*console.log(ref.info_torre);*/
					}
					else
					{
						
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			ref.info = {
				info: 1,
				mostrarInfo: function(vista){
					this.info = vista;
				}
			};

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						ref.reestablecerContrasena = {};
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};

			torre();
		}]);
</script>
</body>
</html>

<!--<p>
	<?php print_r($session);?>
</p>-->

<!--<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.info_torre= [];

			torre = function(){
				$http({
					url:"<?php echo base_url("index.php/status_site/sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.info_torre = data[0];
					}
					else
					{
						
					}
				}).error(function(err){

				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){

				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){

				});
			};

			torre();
		}]);
</script>
</body>
</html>-->