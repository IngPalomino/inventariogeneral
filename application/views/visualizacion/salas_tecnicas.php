<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 textos">			
		<div ng-repeat="sala in ctrl.salas" class="col-md-3 col-sm-6 col-xs-12">
				<a href="<?php echo site_url("salastecnicas/mso/{{sala.id}}");?>" class="btn boton" ng-class="<?php echo $this->auth->logged_in()? "" : "'disabled'" ?>"  role="button">
					<table class="table">
						<tbody>
						<tr><td><span class="fa fa-columns fa-5x"></span></td></tr>
							<tr><td><!-- {{sala.id}}.-  -->Sala Técnica <br> {{sala.nombre}}
                                </td>
                            </tr>
						</tbody>
					</table>
				</a>
			</div>
		</div>
	</div>
	<br/>
</section>