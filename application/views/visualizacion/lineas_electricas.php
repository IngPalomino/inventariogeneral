<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="linea in ctrl.lineas_electricas" ng-if="ctrl.lineas_electricas.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Lineas Eléctricas {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr>
								<td class="info">Fecha de puesta en servicio</td>
								<td class="default">{{linea.fecha_servicio | date:"dd/MM/yyyy"}}</td>
								<td class="info">Fases</td>
								<td class="default">{{linea.sistema_linea_electrica.sistema_linea_electrica}}</td>
							</tr>
							<tr>
								<td class="info">Conexión</td>
								<td class="default">
									<span ng-repeat="conexion in linea.lineas_electricas_conexion_linea_electrica" bs-popover>
										<span ng-if="$index > 0">/</span>
										<span class="fa fa-question-circle" rel="popover" data-container="body" data-toggle="popover" data-placement="top"  data-trigger="hover" data-title="Nivel de Tensión" data-content="{{conexion.conexion_linea_electrica.nivel_tension}} {{conexion.conexion_linea_electrica.extra}}"></span> {{conexion.conexion_linea_electrica.conexion_linea_electrica}}
									</span>
								</td>
								<td class="info">Tipo</td>
								<td class="default">
									<span ng-repeat="conexion in linea.lineas_electricas_conexion_linea_electrica">
										<span ng-if="$index > 0">/</span>
										{{conexion.conexion_linea_electrica.tipo}}
									</span>
								</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Sección de Línea</td>
								<td class="default" colspan="2">{{linea.seccion_linea}}</td>
							</tr>
							<tr>
								<td class="info">Distancia</td>
								<td class="default">{{linea.distancia}} Km</td>
								<td class="info">Postes</td>
								<td class="default">{{linea.postes}}</td>
							</tr>
							<tr ng-repeat-start="propietario in linea.propietarios_linea_electrica" ng-if="(propietario.eliminado == 0)">
								<td class="info text-center" colspan="4"><strong>Propietario {{$index + 1}}</strong></td>
							</tr>
							<tr>
								<td class="info">Propietario del Terreno</td>
								<td class="default">{{propietario.propietario}}</td>
								<td class="info">Contrato de servidumbre</td>
								<td class="default">S/ {{propietario.contrato_servidumbre}}</td>
							</tr>
							<tr ng-repeat-end>
								<td class="info">Teléfono</td>
								<td class="default">{{propietario.telefono}}</td>
								<td class="info">Correo Eléctronico</td>
								<td class="default">{{propietario.correo}}</td>
							</tr>
							
							<tr class="bg-success">
								<td>Marca del Transformador</td>
								<td>{{linea.tipo_transformador.marca}}</td>
								<td>Número de Serie</td>
								<td>{{linea.num_serie_transformador}}</td>
							</tr>
							<tr class="bg-success">
								<td>Potencia</td>
								<td>{{linea.potencia_transformador}} KVA</td>
								<td>Conexión</td>
								<td>{{linea.conexion_transformador}}</td>
							</tr>
							<tr class="bg-warning">
								<td>Marca del Transformix</td>
								<td>{{linea.tipo_transformix.marca}}</td>
								<td>Número de Serie</td>
								<td>{{linea.num_serie_transformix}}</td>
							</tr>
							<tr class="bg-warning">
								<td>Potencia</td>
								<td>{{linea.potencia_transformix}} VA</td>
								<td>Conexión</td>
								<td>{{linea.conexion_transformix}}</td>
							</tr>
						</tbody>
					</table>
				</div>	
			</div>
		</div>
	</div>
</div>
<div class="container ng-cloak" ng-if="!ctrl.lineas_electricas.length">
	<div class="well text-center">
		No hay información
	</div>
</div>