<!--<p>
	<?php print_r($session);?>
</p>-->

<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.antenasMicroondas = [];
			ref.antenasMicroondasPendiente = [];
			ref.antenasGUL = [];
			ref.antenasGULPendiente = [];
			ref.antenasIDEN = [];
			ref.antenas_in_building = [];
			
			ref.info_sitio = {};
			
			listarAntenas = function(){
				
				$http({
					url:"<?php echo base_url("antenas_mw/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenasMicroondas = data;
					}
					else
					{

					}
				}).error(function(err){

				});
				
//----------pendiente microondas
		   $http({
					url:"<?php echo base_url("antenas_mw/listar_sitio_pendiente");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenasMicroondasPendiente = data;
					}
					else
					{

					}
				}).error(function(err){

				});
//--------- pendiente microondas


				$http({
					url:"<?php echo base_url("/antenas_gul/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenasGUL = data;

						ref.antenasGUL.forEach(function(antGUL,indx,objx){
							objx[indx].tipo_antena_gul.extras = JSON.parse(objx[indx].tipo_antena_gul.extras);
						});
					}
					else
					{

					}
				}).error(function(err){

				});

// GUL PENDIENTE
				$http({
					url:"<?php echo base_url("/antenas_gul_pendiente/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenasGULPendiente = data;

						ref.antenasGULPendiente.forEach(function(antGUL,indx,objx){
							objx[indx].tipo_antena_gul.extras = JSON.parse(objx[indx].tipo_antena_gul.extras);
						});
					}
					else
					{

					}
				}).error(function(err){

				});
// GUL PENDIENTE


				$http({
					url:"<?php echo base_url("/antenas_iden/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenasIDEN = data;
					}
					else
					{

					}
				}).error(function(err){

				});
				
				$http({
					url:"<?php echo base_url("/antenas_in_building/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.antenas_in_building = data;
					}
					else
					{

					}
				}).error(function(err){

				});
			}
			
			infoSitio = function(){
				$http({
					url:"<?php echo base_url("/status_site/editar/");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.info_sitio = data[0];
					}
					else
					{

					}
				}).error(function(err){

				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						ref.reestablecerContrasena = {};
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
				ref.reestablecerContrasena = {};
			};
			
			listarAntenas();
			infoSitio();
		}]);
</script>
</body>
</html>