<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Tarjetas
				</div>
				<div class="panel-body" style="max-width:1200px;max-height:300px;overflow:scroll;">
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargandoTarjetas"></span>
					</div>
					<table class="table table-striped table-condensed" style="width:2200px;" ng-if="!ctrl.cargandoTarjetas && ctrl.tarjetas.length">
						<thead>
							<tr>
								<th>Board Type</th>
								<th>NE Type</th>
								<th>Slot ID</th>
								<th>Hardware Version</th>
								<th>NE ID</th>
								<th>Board Bar Code</th>
								<th>Board BOM Item</th>
								<th>Description</th>
								<th>Manufacture Date</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="tarjeta in ctrl.tarjetas">
								<td>{{tarjeta.board_type}}</td>
								<td>{{tarjeta.ne_type}}</td>
								<td>{{tarjeta.slot_id}}</td>
								<td>{{tarjeta.hardware_version}}</td>
								<td>{{tarjeta.idu.ne_id}}</td>
								<td>{{tarjeta.board_bar_code}}</td>
								<td>{{tarjeta.board_bom_item}}</td>
								<td style="white-space:nowrap;">{{tarjeta.description}}</td>
								<td>
									<span ng-if="tarjeta.manufacture_date != '0000-00-00'">{{tarjeta.manufacture_date | date:"dd/MM/yyyy"}}</span>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="col-xs-12 ng-cloak">
						<div class="well text-center" ng-if="!ctrl.cargandoTarjetas && !ctrl.tarjetas.length">No hay información</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					ODUs
				</div>
				<div class="panel-body" style="max-height:300px;overflow-y:scroll;">
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargandoODUs"></span>
					</div>
					<table class="table table-striped table-condensed" ng-if="!ctrl.cargandoODUs && ctrl.odus.length">
						<thead>
							<tr>
								<th>NE Type</th>
								<th>Frequency</th>
								<th>Range of Frequency Point</th>
								<th>Produce Time</th>
								<th>Factory Information</th>
								<th>Software Version</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="odu in ctrl.odus">
								<td>{{odu.ne_type}}</td>
								<td>{{odu.frequency}} GHz</td>
								<td>{{odu.range_frequency}}</td>
								<td>{{odu.produce_time | date:"dd/MM/yyyy"}}</td>
								<td>{{odu.factory_information}}</td>
								<td>{{odu.software_version}}</td>
							</tr>
						</tbody>
					</table>
					<div class="col-xs-12 ng-cloak">
						<div class="well text-center" ng-if="!ctrl.cargandoODUs && !ctrl.odus.length">No hay información</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>