<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=places"></script>
<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.sitios = [];
			ref.info_sitio = [];
			ref.enlaces = [];

			/*  José Rivera solicitó esto. */

			crearMapa = function(){
				var myLatLng = {lat: <?php echo (Float)$infoSitio[0]["latitud"]; ?>, lng: <?php echo (Float)$infoSitio[0]["longitud"]; ?>};

				var map = new google.maps.Map(document.getElementById('map'), {
				    zoom: 15,
				    center: myLatLng,
				    mapTypeId: 'satellite',
				    fullscreenControl: true
				});

				var marker = new google.maps.Marker({
				    position: myLatLng,
				    map: map,
				    title: "<?php echo $infoSitio[0]["nombre_completo"]; ?>"
				});

				var infoWindow = new google.maps.InfoWindow({
					content: document.getElementById('infoSitio')
				});

				infoWindow.open(map, marker);
				map.panBy(0, -100);

				marker.addListener('click', function(){
					infoWindow.open(map, marker);
				});

				ref.listarOtrosSitios(map);

				ref.dibujarEnlaces = function(){
					for(i = 0; i < ref.enlaces.length; i++)
					{
						var sitio = ref.enlaces[i];

						switch(Number(sitio["polaridad"])){
							case 1:
								var polaridad = "V";
								break;
							case 2:
								var polaridad = "H";
								break;
							case 3:
								var polaridad = "v+H";
								break;
						}

						var modulacion_adaptativa = (Number(sitio["modulacion_adaptativa"]) == 0)? "NO" : "SI";
						var capacidad_am = (Number(sitio["capacidad_am"]) == 0)? "N/A" : Number(sitio["capacidad_am"]);

						var myinfowindow = new google.maps.InfoWindow({
							    content: '<div class="panel panel-primary">'
							    		+'<div class="panel-heading text-center">Configuración del Enlace</div>'
							    		+'<div class="panel-body">' 
							    		+'<table class="table table-condensed">'
							    		+'<tr><td class="info">Configuración</td><td>'+sitio["configuracion"]+'</td></tr>'
							    		+'<tr><td class="info">Polaridad</td><td>'+polaridad+'</td></tr>'
							    		+'<tr><td class="info">Ancho de Banda</td><td>'+sitio["ancho_banda"]+' MHz</td></tr>'
							    		+'<tr><td class="info">Modulación</td><td>'+sitio["modulacion"]+'</td></tr>'
							    		+'<tr><td class="info">Capacidad</td><td>'+sitio["capacidad"]+' Mbit/s</td></tr>'
							    		+'<tr><td class="info">Modulación Adaptativa (AM)</td><td>'+modulacion_adaptativa+'</td></tr>'
							    		+'<tr><td class="info">Modulación AM</td><td>'+sitio["modulacion_am"]+'</td></tr>'
							    		+'<tr><td class="info">Capacidad AM</td><td>'+capacidad_am+'</td></tr>'
							    		+'<tr><td class="info">Banda</td><td>'+sitio["banda"]+' GHz</td></tr>'
							    		+'</table></div></div>'
							});

						var flightPlanCoordinates = [
			          		myLatLng,
			          		{lat: Number(sitio["idu_2"]["sitio"]["latitud"]), lng: Number(sitio["idu_2"]["sitio"]["longitud"])}
			        	];

			        	var flightPath = new google.maps.Polyline({
			          		path: flightPlanCoordinates,
			          		geodesic: true,
			          		strokeColor: '#FF0000',
			          		strokeOpacity: 1.0,
			          		strokeWeight: 5,
			          		infowindow: myinfowindow
			        	});

			        	var path = flightPath.getPath();

			        	flightPath.setMap(map);

			        	google.maps.event.addListener(flightPath, "click", function (e) {
						   this.infowindow.setPosition(e.latLng);
						   this.infowindow.setContent(myinfowindow); //you accidently reuse the last txt here
						   this.infowindow.open(map);
						});
					}
				}
			}

			listarInfoSitio = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("objetos/resumen_sitio/$sitio"); ?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.info_sitio = data;
						console.log(data);
						ref.cargando = false;
					}
					else
					{
						ref.info_sitio = [];
						ref.cargando = false;
					}
				}).error(function(err){
					console.log(err);
					ref.cargando = false;
				});
			}

			listarEnlaces = function(){
				$http({
					url:"<?php echo base_url("enlaces/listar_sitio_coor/$sitio"); ?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.enlaces = data;
						console.log(data);
					}
					else
					{
						ref.enlaces = [];
					}
				}).error(function(err){
					console.log(err);
				});
			}

			ref.listarOtrosSitios = function(map)
			{
				$http({
					url:"<?php echo base_url("status_site/listar_otros_sitios/$sitio");?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.sitios = data;

						for (var i = 0; i < ref.sitios.length; i++) {
							var sitio = ref.sitios[i];

							var myinfowindow = new google.maps.InfoWindow({
							    content: '<a href="<?php echo base_url('ver/sitios/gis') ?>/'+sitio["id"]+'">'+sitio["nombre_completo"]+'</a>'
							});
							
							var marker = new google.maps.Marker({
								position: {lat: Number(sitio["latitud"]), lng: Number(sitio["longitud"])},
								map: map,
								title: sitio["nombre_completo"],
								icon: (sitio["estado"] == 2)? "<?php echo base_url("assets/images/maps/yellow-dot.png"); ?>" : "<?php echo base_url("assets/images/maps/purple-dot.png"); ?>",
								infowindow: myinfowindow
							});

							google.maps.event.addListener(marker, 'click', function() {
							        this.infowindow.open(map, this);
							});
						}
					}
					else
					{

					}
				}).error(function(err){
					/*console.log(err);*/
				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						ref.reestablecerContrasena = {};
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			crearMapa();
			listarInfoSitio();
			listarEnlaces();
		}]);
</script>
</body>
</html>