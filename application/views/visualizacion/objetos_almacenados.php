<section class="container-fluid">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="page-header">
				<h2>Bodegas Virtuales</h2>
			</div>
		</div>
		<div class="col-xs-10 col-xs-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">
					<ul class="nav nav-pills nav-justified">
						<li role="presentation" ng-class="{active:ctrl.navPanel.index == 0}">
							<a href ng-click="ctrl.navPanel.cambiarIndex(0)">Objetos</a>
						</li>
						<li role="presentation" ng-class="{active:ctrl.navPanel.index == 1}">
							<a href ng-click="ctrl.navPanel.cambiarIndex(1)">Sitios</a>
						</li>
						<li role="presentation" ng-class="{active:ctrl.navPanel.index == 2}">
							<a href ng-click="ctrl.navPanel.cambiarIndex(2)">Almacenes</a>
						</li>
					</ul>
				</div>
				<div class="panel-body ng-cloak" ng-if="ctrl.navPanel.index == 0">
					<ul class="nav nav-pills nav-justified">
						<li role="presentation" ng-class="{active:ctrl.navObjetos.index == 0}">
							<a href ng-click="ctrl.navObjetos.cambiarIndex(0); ctrl.buscarGruposElectrogenos()">Grupos Electrógenos</a>
						</li>
						<li role="presentation" ng-class="{active:ctrl.navObjetos.index == 1}">
							<a href ng-click="ctrl.navObjetos.cambiarIndex(1); ctrl.buscarObjeto()">Aires Acondicionados</a>
						</li>
					</ul><br>
					<div ng-if="ctrl.navObjetos.index == 0">
						<table class="table table-condensed table-striped" ng-if="!ctrl.cargando">
							<thead>
								<tr>
									<th>Acciones</th>
									<th colspan="2">Grupo Electrógeno</th>
									<th>Proveniente</th>
									<th>Almacenado en</th>
									<th>Historial</th>
								</tr>
							</thead>
							<tbody ng-repeat-start="grupo in ctrl.grupos_electrogenos" ng-if="ctrl.grupos_electrogenos.length">
								<tr>
									<td>
										<button class="btn btn-xs btn-info" ng-click="ctrl.mostrarDetalles(grupo)" ng-show="!grupo.mostrar">Detalles</button>
										<button class="btn btn-xs btn-warning" ng-click="ctrl.ocultarDetalles(grupo)" ng-hide="!grupo.mostrar">Ocultar Detalles</button>
									</td>
									<td colspan="2">Marca: {{grupo.tipo_grupo_electrogeno.marca}} - Modelo: {{grupo.tipo_grupo_electrogeno.modelo}}</td>
									<td>{{grupo.logs_grupos_electrogenos[0].evento.proveniente.nombre_completo}}</td>
									<td>
										<span ng-if="grupo.almacen == null">Sitio: {{grupo.sitio.nombre_completo}}</span>
										<span ng-if="grupo.almacen != null">Almacén: {{grupo.almacen.almacen}}</span>
									</td>
									<td style="font-size:8pt;">Almacenado: {{grupo.logs_grupos_electrogenos[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}}<br>por {{grupo.logs_grupos_electrogenos[0].usuario.nombres}} {{grupo.logs_grupos_electrogenos[0].usuario.apellido_paterno}}</td>
								</tr>
							</tbody>
							<tbody ng-repeat-end ng-hide="!grupo.mostrar" style="margin:100px;">
								<tr class="text-center" style="background-color:#d2f3ce;">
									<td colspan="6">Grupo Electrógeno</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_grupo_electrogeno.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_grupo_electrogeno.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_grupo_electrogeno}}</td>
								</tr>
								<tr class="text-center" style="background-color:#b9d1e5;">
									<td colspan="6">Motor</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_motor_ge.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_motor_ge.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_motor}}</td>
								</tr>
								<tr class="text-center" style="background-color:#d7e7b4;">
									<td colspan="6">Generador</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_generador_ge.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_generador_ge.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_generador}}</td>
								</tr>
								<tr>
									<td class="info">Potencia</td>
									<td class="default">{{grupo.tipo_generador_ge.potencia}} kW</td>
									<td class="info">Corriente</td>
									<td class="default">{{grupo.tipo_generador_ge.corriente}} A</td>
									<td class="info" colspan="2"></td>
								</tr>
								<tr class="text-center" style="background-color:#e7d7b4;">
									<td colspan="6">AVR</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_avr_ge.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_avr_ge.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_avr}}</td>
								</tr>
								<tr class="text-center bg-success">
									<td colspan="6">Controlador GE</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_controlador_ge.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_controlador_ge.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_controlador_ge}}</td>
								</tr>
								<tr class="text-center" style="background-color:#f7f7ca;">
									<td colspan="6">Controlador TTA</td>
								</tr>
								<tr>
									<td class="info">Marca</td>
									<td class="default">{{grupo.tipo_controlador_tta.marca}}</td>
									<td class="info">Modelo</td>
									<td class="default">{{grupo.tipo_controlador_tta.modelo}}</td>
									<td class="info">Número de serie</td>
									<td class="default">{{grupo.numero_serie_controlador_tta}}</td>
								</tr>
							</tbody>
						</table>
						<div class="text-center">
							<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
						</div>
					</div>
				</div>
				
				<div class="panel-body ng-cloak" ng-if="ctrl.navPanel.index == 1">
					<label style="width:100%" ng-if="!ctrl.cargando">
						<selectize config="ctrl.config" options='ctrl.sitios' ng-model="ctrl.busqueda_Grupo.sitio" ng-change="ctrl.listarGrupoSitio(ctrl.busqueda_Grupo.sitio); ctrl.cargando == true;"></selectize>
					</label>
					<table class="table table-condensed table-striped" ng-if="ctrl.grupos_electrogenos_sitio.length && ctrl.busqueda_Grupo.sitio && !ctrl.listando">
						<thead>
							<tr>
								<th>Acciones</th>
								<th>Objeto</th>
								<th colspan="2">Información</th>
								<th>Proveniente</th>
								<th>Historial</th>
							</tr>
						</thead>
						<tbody ng-repeat-start="grupo in ctrl.grupos_electrogenos_sitio" ng-if="ctrl.grupos_electrogenos_sitio.length && (grupo.sitio.id == ctrl.busqueda_Grupo.sitio)">
							<tr>
								<td>
									<button class="btn btn-xs btn-info" ng-click="ctrl.mostrarDetalles(grupo)" ng-show="!grupo.mostrar">Detalles</button>
									<button class="btn btn-xs btn-warning" ng-click="ctrl.ocultarDetalles(grupo)" ng-hide="!grupo.mostrar">Ocultar Detalles</button>
								</td>
								<td>Grupo Electrógeno</td>
								<td colspan="2">Marca: {{grupo.tipo_grupo_electrogeno.marca}} - Modelo: {{grupo.tipo_grupo_electrogeno.modelo}}</td>
								<td>{{grupo.logs_grupos_electrogenos[0].evento.proveniente.nombre_completo}}</td>
								<td style="font-size:8pt;">Almacenado: {{grupo.logs_grupos_electrogenos[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}}<br>por {{grupo.logs_grupos_electrogenos[0].usuario.nombres}} {{grupo.logs_grupos_electrogenos[0].usuario.apellido_paterno}}</td>
							</tr>
						</tbody>
						<tbody ng-repeat-end ng-hide="!grupo.mostrar">
							<tr class="text-center" style="background-color:#d2f3ce;">
								<td colspan="6">Grupo Electrógeno</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_grupo_electrogeno.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_grupo_electrogeno.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_grupo_electrogeno}}</td>
							</tr>
							<tr class="text-center" style="background-color:#b9d1e5;">
								<td colspan="6">Motor</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_motor_ge.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_motor_ge.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_motor}}</td>
							</tr>
							<tr class="text-center" style="background-color:#d7e7b4;">
								<td colspan="6">Generador</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_generador_ge.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_generador_ge.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_generador}}</td>
							</tr>
							<tr>
								<td class="info">Potencia</td>
								<td class="default">{{grupo.tipo_generador_ge.potencia}} kW</td>
								<td class="info">Corriente</td>
								<td class="default">{{grupo.tipo_generador_ge.corriente}} A</td>
								<td class="info" colspan="2"></td>
							</tr>
							<tr class="text-center" style="background-color:#e7d7b4;">
								<td colspan="6">AVR</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_avr_ge.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_avr_ge.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_avr}}</td>
							</tr>
							<tr class="text-center bg-success">
								<td colspan="6">Controlador GE</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_controlador_ge.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_controlador_ge.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_controlador_ge}}</td>
							</tr>
							<tr class="text-center" style="background-color:#f7f7ca;">
								<td colspan="6">Controlador TTA</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_controlador_tta.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_controlador_tta.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_controlador_tta}}</td>
							</tr>
						</tbody>
					</table>
					<div class="alert alert-warning" ng-if="!ctrl.grupos_electrogenos_sitio.length && ctrl.busqueda_Grupo.sitio && !ctrl.listando">
						No hay objetos almacenados en este sitio.
					</div>
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando || ctrl.listando"></span>
					</div>
				</div>
				
				<div class="panel-body ng-cloak" ng-if="ctrl.navPanel.index == 2">
					<select class="form-control" ng-model="ctrl.busqueda_Grupo.almacen" ng-change="ctrl.listarGrupoAlmacen(ctrl.busqueda_Grupo.almacen)" ng-if="!ctrl.cargando">
						<option value="">Elegir almacen</option>
						<option ng-repeat="almacen in ctrl.almacenes" value="{{almacen.id}}">{{almacen.almacen}}</option>
					</select><br>
					<table class="table table-condensed table-striped" ng-if="ctrl.grupos_electrogenos_almacen.length && ctrl.busqueda_Grupo.almacen && !ctrl.listando">
						<thead>
							<tr>
								<th>Acciones</th>
								<th>Objeto</th>
								<th colspan="2">Información</th>
								<th>Proveniente</th>
								<th>Historial</th>
							</tr>
						</thead>
						<tbody ng-repeat-start="grupo in ctrl.grupos_electrogenos_almacen" ng-if="ctrl.grupos_electrogenos_almacen.length && (grupo.almacen.id == ctrl.busqueda_Grupo.almacen)">
							<tr>
								<td>
									<button class="btn btn-xs btn-info" ng-click="ctrl.mostrarDetalles(grupo)" ng-show="!grupo.mostrar">Detalles</button>
									<button class="btn btn-xs btn-warning" ng-click="ctrl.ocultarDetalles(grupo)" ng-hide="!grupo.mostrar">Ocultar Detalles</button>
								</td>
								<td>Grupo Electrógeno</td>
								<td colspan="2">Marca: {{grupo.tipo_grupo_electrogeno.marca}} - Modelo: {{grupo.tipo_grupo_electrogeno.modelo}}</td>
								<td>{{grupo.logs_grupos_electrogenos[0].evento.proveniente.nombre_completo}}</td>
								<td style="font-size:8pt;">Almacenado: {{grupo.logs_grupos_electrogenos[0].created_at | date:"dd/MM/yyyy @ HH:mm:ss"}}<br>por {{grupo.logs_grupos_electrogenos[0].usuario.nombres}} {{grupo.logs_grupos_electrogenos[0].usuario.apellido_paterno}}</td>
							</tr>
						</tbody>
						<tbody ng-repeat-end ng-hide="!grupo.mostrar">
							<tr class="text-center" style="background-color:#d2f3ce;">
								<td colspan="6">Grupo Electrógeno</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_grupo_electrogeno.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_grupo_electrogeno.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_grupo_electrogeno}}</td>
							</tr>
							<tr class="text-center" style="background-color:#b9d1e5;">
								<td colspan="6">Motor</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_motor_ge.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_motor_ge.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_motor}}</td>
							</tr>
							<tr class="text-center" style="background-color:#d7e7b4;">
								<td colspan="6">Generador</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_generador_ge.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_generador_ge.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_generador}}</td>
							</tr>
							<tr>
								<td class="info">Potencia</td>
								<td class="default">{{grupo.tipo_generador_ge.potencia}} kW</td>
								<td class="info">Corriente</td>
								<td class="default">{{grupo.tipo_generador_ge.corriente}} A</td>
								<td class="info" colspan="2"></td>
							</tr>
							<tr class="text-center" style="background-color:#e7d7b4;">
								<td colspan="6">AVR</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_avr_ge.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_avr_ge.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_avr}}</td>
							</tr>
							<tr class="text-center bg-success">
								<td colspan="6">Controlador GE</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_controlador_ge.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_controlador_ge.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_controlador_ge}}</td>
							</tr>
							<tr class="text-center" style="background-color:#f7f7ca;">
								<td colspan="6">Controlador TTA</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_controlador_tta.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_controlador_tta.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_controlador_tta}}</td>
							</tr>
						</tbody>
					</table>
					<div class="alert alert-warning" ng-if="!ctrl.grupos_electrogenos_almacen.length && ctrl.busqueda_Grupo.almacen && !ctrl.listando">
						No hay objetos almacenados en este almacén.
					</div>
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando || ctrl.listando"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>