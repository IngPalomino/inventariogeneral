<script type="text/javascript">
	angular
		.module('app',[])
		.service("serviciosEspeciales", function($http, $q){
			var resultado = $q.defer();

			this.listarServicios = function(id, servicio, slot)
			{
				return $http({
					url:"<?php echo base_url("microondas_tarjetas/listar_servicio");?>/"+id+"/"+servicio+"/"+slot,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						resultado.resolve(data);
						listado = resultado.promise;
						return listado;
					}
					else
					{
						
					}
				}).error(function(err){
					
				});
			};

			this.listarPuertos = function(link_name, port_number)
			{
				return $http({
					url:"<?php echo base_url("microondas_tarjetas/listar_puertos");?>/"+link_name+"/"+port_number,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						resultado.resolve(data);
						listado = resultado.promise;
						return listado;
					}
					else
					{
						
					}
				}).error(function(err){
					
				});
			};
		})
		.controller('controlador', ["$http", "$q", "serviciosEspeciales",function($http, $q, serviciosEspeciales){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.idus = [];
			ref.editarIDU = {};
			
			listarIDUS = function(){
				$http({
					url:"<?php echo base_url("index.php/idus/listar_sitio");?>"+"/"+"<?php echo $sitio;?>",
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.idus = data;

						ref.idus.forEach(function(dato, ind, objeto){

							switch(Number(objeto[ind].tipo_idu.id))
							{
								case 1:
									serviciosEspeciales
										.listarServicios(objeto[ind].id, "IFE", 3)
										.then(
											function(resp){
												objeto[ind].ife_3 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "IFE", 4)
										.then(
											function(resp){
												objeto[ind].ife_4 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ODU", 23)
										.then(
											function(resp){
												objeto[ind].odu_23 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ODU", 24)
										.then(
											function(resp){
												objeto[ind].odu_24 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 17)
										.then(
											function(resp){
												objeto[ind].fe_1 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 18)
										.then(
											function(resp){
												objeto[ind].fe_2 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 19)
										.then(
											function(resp){
												objeto[ind].fe_3 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 20)
										.then(
											function(resp){
												objeto[ind].fe_4 = (resp.data.length? true : false);
										});
									break;

								case 2:
									serviciosEspeciales
										.listarServicios(objeto[ind].id, "AUX", 2)
										.then(
											function(resp){
												objeto[ind].ml_1 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "IFU", 2)
										.then(
											function(resp){
												objeto[ind].ifu_2 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "IFU", 3)
										.then(
											function(resp){
												objeto[ind].ifu_3 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "IFU", 4)
										.then(
											function(resp){
												objeto[ind].ifu_4 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "IFU", 5)
										.then(
											function(resp){
												objeto[ind].ifu_5 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "IFU", 6)
										.then(
											function(resp){
												objeto[ind].ifu_6 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ODU", 23)
										.then(
											function(resp){
												objeto[ind].odu_23 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ODU", 24)
										.then(
											function(resp){
												objeto[ind].odu_24 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ODU", 25)
										.then(
											function(resp){
												objeto[ind].odu_25 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ODU", 26)
										.then(
											function(resp){
												objeto[ind].odu_26 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 1)
										.then(
											function(resp){
												objeto[ind].ge_1 = (resp.data.length? resp.data : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 2)
										.then(
											function(resp){
												objeto[ind].ge_2 = (resp.data.length? resp.data : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 3)
										.then(
											function(resp){
												objeto[ind].fe_1 = (resp.data.length? resp.data : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 4)
										.then(
											function(resp){
												objeto[ind].fe_2 = (resp.data.length? resp.data : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 5)
										.then(
											function(resp){
												objeto[ind].fe_3 = (resp.data.length? resp.data : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 6)
										.then(
											function(resp){
												objeto[ind].fe_4 = (resp.data.length? resp.data : false);
										});
									break;
								case 3:
									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 1)
										.then(
											function(resp){
												objeto[ind].ge_1 = (resp.data.length? resp.data : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 2)
										.then(
											function(resp){
												objeto[ind].ge_2 = (resp.data.length? resp.data : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 3)
										.then(
											function(resp){
												objeto[ind].ge_3 = (resp.data.length? resp.data : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 4)
										.then(
											function(resp){
												objeto[ind].ge_4 = (resp.data.length? resp.data : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 5)
										.then(
											function(resp){
												objeto[ind].ge_5 = (resp.data.length? resp.data : false);
										});

									serviciosEspeciales
										.listarPuertos(objeto[ind].link_name, 6)
										.then(
											function(resp){
												objeto[ind].ge_6 = (resp.data.length? resp.data : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ML1", 1)
										.then(
											function(resp){
												objeto[ind].ml1_1 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ML1", 2)
										.then(
											function(resp){
												objeto[ind].ml1_2 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ML1", 3)
										.then(
											function(resp){
												objeto[ind].ml1_3 = (resp.data.length? true : false);
										});
									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ML1", 4)
										.then(
											function(resp){
												objeto[ind].ml1_4 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ML1", 5)
										.then(
											function(resp){
												objeto[ind].ml1_5 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ML1", 6)
										.then(
											function(resp){
												objeto[ind].ml1_6 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ISV3", 1)
										.then(
											function(resp){
												objeto[ind].isv3_1 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ISV3", 2)
										.then(
											function(resp){
												objeto[ind].isv3_2 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ISV3", 3)
										.then(
											function(resp){
												objeto[ind].isv3_3 = (resp.data.length? true : false);
										});
									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ISV3", 4)
										.then(
											function(resp){
												objeto[ind].isv3_4 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ISV3", 5)
										.then(
											function(resp){
												objeto[ind].isv3_5 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ISV3", 6)
										.then(
											function(resp){
												objeto[ind].isv3_6 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ODU", 21)
										.then(
											function(resp){
												objeto[ind].odu_21 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ODU", 22)
										.then(
											function(resp){
												objeto[ind].odu_22 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ODU", 23)
										.then(
											function(resp){
												objeto[ind].odu_23 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ODU", 24)
										.then(
											function(resp){
												objeto[ind].odu_24 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ODU", 25)
										.then(
											function(resp){
												objeto[ind].odu_25 = (resp.data.length? true : false);
										});

									serviciosEspeciales
										.listarServicios(objeto[ind].id, "ODU", 26)
										.then(
											function(resp){
												objeto[ind].odu_26 = (resp.data.length? true : false);
										});
									break;
							}
						});
					}
					else
					{

					}
					console.log(data);
				}).error(function(err){

				});
			}

			ref.verIDU = function(idu){
				ref.editarIDU = idu;
				$("#verIDU").bPopup({
					amsl: 0
				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						ref.reestablecerContrasena = {};
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			listarIDUS();
		}])
		.directive('bsPopover',function(){
			return function(scope,element,attrs){
				element.find("img[rel=popover], span[rel=popover]").popover();
			}
		});
</script>
</body>
</html>