<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="tanque in ctrl.tanques_combustible" ng-if="ctrl.tanques_combustible.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Tanque de combustible {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr>
								<td class="info">Tipo de tanque</td>
								<td class="default">
									<span ng-show="!tanque.editar" ng-if="tanque.tipo_tanque_combustible == 1">Tanque Interno</span>
									<span ng-show="!tanque.editar" ng-if="tanque.tipo_tanque_combustible == 2">Tanque Externo</span>
									<select ng-hide="!tanque.editar" class="form-control" ng-model="tanque.tipo_tanque_combustible">
										<option value="">Elegir tipo</option>
										<option value="1">Tanque Interno</option>
										<option value="2">Tanque Externo</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="info">Fecha de instalación</td>
								<td class="default">
									<span ng-show="!tanque.editar">{{tanque.fechaToString(tanque.fecha_instalacion)}}</span>
									<input ng-hide="!tanque.editar" class="form-control" type="date" ng-model="tanque.fecha_instalacion"/>
								</td>
							</tr>
							<tr>
								<td class="info">Capacidad (Gl)</td>
								<td class="default">
									<span ng-show="!tanque.editar">{{tanque.capacidad}}</span>
									<input ng-hide="!tanque.editar" class="form-control" type="number" step="any" min="0" ng-model="tanque.capacidad"/>
								</td>
							</tr>
							<tr>
								<td class="info">Consumo (Gl/h)</td>
								<td class="default">
									<span ng-show="!tanque.editar">{{tanque.consumo}}</span>
									<input ng-hide="!tanque.editar" class="form-control" type="number" step="any" min="0" ng-model="tanque.consumo"/>
								</td>
							</tr>
							<tr>
								<td class="info">Autonomía (h)</td>
								<td class="default">
									<span>{{tanque.autonomia() | number:1}}</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container ng-cloak" ng-if="!ctrl.tanques_combustible.length">
	<div class="well text-center">
		No hay información
	</div>
</div>