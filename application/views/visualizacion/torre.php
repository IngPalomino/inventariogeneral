<br>
<div class="container">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<nav class="navbar">
				<div class="container-fluid">
					<div class="navbar-header" style="color:rgb(245,121,23);">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-busqueda">
							<span class="sr-only" style="color:rgb(245,121,23);">Toggle navigation</span>
							<span class="fa fa-bars"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="navbar-busqueda">
						<b><ul class="nav navbar-nav">
							<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(1)" href>Nombre y Ubicación</a></li>
							<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(2)" href>Características físicas</a></li>
							<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(3)" href>Operadores y Proyectos</a></li>
							<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(4)" href>Acceso</a></li>
							<li><a style="color:rgb(245,121,23);text-decoration: none;" ng-click="ctrl.info.mostrarInfo(5)" href>Mantenimiento y Contratos</a></li>
						</ul></b>
					</div>
				</div>
			</nav>
		</div>
		<div class="panel-body ng-cloak" style="height:375px;overflow:scroll">
			<table id="nombre_y_ubicacion" class="table table-condensed" ng-if="ctrl.info.info == 1">
				<tbody>
					<tr>
						<td class="info text-right">Código</td>
						<td class="active">{{ctrl.info_torre.codigo}}</td>
					</tr>
					<tr>
						<td class="info text-right">Nombre</td>
						<td class="active">{{ctrl.info_torre.nombre}}</td>
					</tr>
					<tr>
						<td class="info text-right">Nombre completo</td>
						<td class="active">{{ctrl.info_torre.nombre_completo}}</td>
					</tr>
					<tr>
						<td class="info text-right">Departamento</td>
						<td class="active">{{ctrl.info_torre.zona_peru.departamento}}</td>
					</tr>
					<tr>
						<td class="info text-right">Provincia</td>
						<td class="active">{{ctrl.info_torre.zona_peru.provincia}}</td>
					</tr>
					<tr>
						<td class="info text-right">Distrito</td>
						<td class="active">{{ctrl.info_torre.zona_peru.distrito}}</td>
					</tr>
					<tr>
						<td class="info text-right">Dirección</td>
						<td class="active">{{ctrl.info_torre.direccion}}</td>
					</tr>
					<tr>
						<td class="info text-right">Coordenadas</td>
						<td class="active">Latitud: {{ctrl.info_torre.latitud}}°, Longitud: {{ctrl.info_torre.longitud}}°</td>
					</tr>
				</tbody>
			</table>
			<table id="caracteristicas_fisicas" class="table table-condensed" ng-if="ctrl.info.info == 2">
				<tbody>
					<tr>
						<td colspan="2" class="info text-right">Mimetizado</td>
						<td colspan="2" class="active">{{ctrl.info_torre.camuflaje.camuflaje}}</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Posición de torre</td>
						<td colspan="1" class="active">
							<span ng-if="ctrl.info_torre.greenfield_rooftop == 1">Rooftop</span>
							<span ng-if="ctrl.info_torre.greenfield_rooftop == 0">Greenfield</span>
						</td>
						<td colspan="1" class="info text-right">Altura de edificación</td>
						<td colspan="1" class="active">{{ctrl.info_torre.altura_edificacion}} m</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Tipo de torre</td>
						<td colspan="1" class="active">{{ctrl.info_torre.tipo_torre.tipo_torre}}</td>
						<td colspan="1" class="info text-right">Altura de torre</td>
						<td colspan="1" class="active">{{ctrl.info_torre.altura_torre}} m</td>
					</tr>
					<tr>
						<td colspan="2" class="info text-right">Factor de uso de Torre</td>
						<td colspan="2" class="active">
							<span ng-class="{'label label-success':ctrl.info_torre.factor_uso_torre < 0.8,'label label-warning':ctrl.info_torre.factor_uso_torre >= 0.8 && ctrl.info_torre.factor_uso_torre <= 1,'label label-danger':ctrl.info_torre.factor_uso_torre > 1}">{{ctrl.info_torre.factor_uso_torre}}</span>
							
						</td>
					</tr>
					<tr>
						<td colspan="2" class="info text-right">Ubicación</td>
						<td colspan="2" class="active">{{ctrl.info_torre.ubicacion_equipos.ubicacion_equipos}}</td>
					</tr>
				</tbody>
			</table>
			<table id="operadores_y_proyectos" class="table table-condensed" ng-if="ctrl.info.info == 3">
				<tbody>
					<tr>
						<td colspan="1" class="info text-right">Coubicado en</td>
						<td colspan="1" class="active">{{ctrl.info_torre.coubicador.coubicador}}</td>
						<td colspan="1" class="info text-right">Nombre de sitio por coubicador</td>
						<td colspan="1" class="active">{{ctrl.info_torre.nombre_sitio_coubicador}}</td>
						<td colspan="1" class="info text-right">Código por coubicador</td>
						<td colspan="1" class="active">{{ctrl.info_torre.codigo_torrera}}</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Operador coubicante</td>
						<td colspan="1" class="active">{{ctrl.info_torre.operador_coubicante.coubicante}}</td>
						<td colspan="2" class="info text-right">Nombre de sitio por coubicante</td>
						<td colspan="2" class="active">{{ctrl.info_torre.nombre_sitio_coubicante}}</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Proyecto</td>
						<td colspan="1" class="active">{{ctrl.info_torre.proyecto.proyecto}}</td>
						<td colspan="1" class="info text-right">Año de construcción</td>
						<td colspan="1" class="active">{{ctrl.info_torre.anio_construccion}}</td>
						<td colspan="1" class="info text-right">Contrata constructora</td>
						<td colspan="1" class="active">{{ctrl.info_torre.contrata_constructora.contrata_constructora}}</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Tipo de proyecto</td>
						<td colspan="1" class="active"></td>
						<td colspan="1" class="info text-right">Sub tipo</td>
						<td colspan="1" class="active"></td>
						<td colspan="1" class="info text-right">Proyecto</td>
						<td colspan="1" class="active"></td>
					</tr>
				</tbody>
			</table>
			<table id="acceso" class="table table-condensed" ng-if="ctrl.info.info == 4">
				<tbody>
					<tr>
						<td colspan="1" class="info text-right">Acceso libre 24h</td>
						<td colspan="1" class="active">
							<span class="label label-success" ng-if="ctrl.info_torre.acceso_libre_24h == 1">SÍ</span>
							<span class="label label-danger" ng-if="ctrl.info_torre.acceso_libre_24h == 0">NO</span>
						</td>
						<td colspan="1" class="info text-right">Nivel de riesgo</td>
						<td colspan="1" class="active">
							<span class="label label-warning">{{ctrl.info_torre.nivel_riesgo}}</span>
						</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Consideraciones de acceso</td>
						<td colspan="3" class="active">{{ctrl.info_torre.consideracion_acceso}}</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Estado</td>
						<td colspan="1" class="active">
							<span class="label label-success" ng-if="ctrl.info_torre.estado == 2">ON AIR</span>
							<span class="label label-danger" ng-if="ctrl.info_torre.estado == 1">OFF AIR</span>
						</td>
						<td colspan="1" class="info text-right">Fecha On Air</td>
						<td colspan="1" class="active">{{ctrl.info_torre.fecha_estado_on_air | date:"dd/MM/yyyy"}}</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Consideraciones</td>
						<td colspan="3" class="active">{{ctrl.info_torre.consideraciones}}</td>
					</tr>
				</tbody>
			</table>
			<table id="mantenimiento_y_contratos" class="table table-condensed" ng-if="ctrl.info.info == 5">
				<tbody>
					<tr>
						<td colspan="3" class="info text-right">Prioridad</td>
						<td colspan="3" class="active">
							<span class="label" ng-class="{'label-success':ctrl.info_torre.prioridad == 4,'label-danger':ctrl.info_torre.prioridad == 0, 'label-warning':(ctrl.info_torre.prioridad > 0 && ctrl.info_torre.prioridad < 4)}">P{{ctrl.info_torre.prioridad}}</span>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="info text-right">Características especiales</td>
						<td colspan="4" class="active">
							<span class="label label-default" ng-if="ctrl.info_torre.in_building != '1' && ctrl.info_torre.agregador != '1' && ctrl.info_torre.dorsal != '1'">NINGUNA</span>
							<span class="label label-info" ng-if="ctrl.info_torre.in_building == '1'">In-Building</span>
							<span class="label label-danger" ng-if="ctrl.info_torre.agregador == '1'">Agregador</span>
							<span class="label label-warning" ng-if="ctrl.info_torre.dorsal == '1'">Dorsal</span>
						</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Tipo de solución</td>
						<td colspan="2" class="active">{{ctrl.info_torre.tipo_solucion.tipo_solucion}}</td>
						<td colspan="1" class="info text-right">Proveedor de mantenimiento</td>
						<td colspan="2" class="active">{{ctrl.info_torre.proveedor_mantenimiento.proveedor_mantenimiento}}</td>
					</tr>
					<tr ng-repeat-start="contrato in ctrl.info_torre.contratos" ng-if="contrato.eliminado == 0">
						<td colspan="6" class="success text-center">ID contrato: {{contrato.id_contrato}}</td>
					</tr>
					<tr>
						<td colspan="2" class="info text-right">Fecha inicio</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{contrato.fecha_inicio | date:"dd/MM/yyyy"}}</span>
						</td>
						<td colspan="2" class="info text-right">Fecha fin</td>
						<td colspan="1" class="active">
							<span ng-hide="ctrl.editar">{{contrato.fecha_fin | date:"dd/MM/yyyy"}}</span>
						</td>
					</tr>
					<tr>
						<td colspan="1" class="info text-right">Arrendador</td>
						<td colspan="2" class="active">
							<span ng-hide="ctrl.editar">{{contrato.arrendador}}</span>
						</td>
						<td colspan="1" class="info text-right">Área arrendada</td>
						<td colspan="2" class="active">
							<span ng-hide="ctrl.editar">{{contrato.area}} m&sup2;</span>
						</td>
					</tr>
					<tr ng-repeat-end ng-if="(contrato.eliminado == 0) && (contacto.eliminado == 0)" ng-repeat="contacto in contrato.contactos_contrato">
						<td colspan="1" class="info text-right">
							Contacto
						</td>
						<td colspan="1" class="active">
							{{contacto.contacto}}
						</td>
						<td colspan="1" class="info text-right">Número(s) telefónico(s)</td>
						<td colspan="1" class="active">
							<span class="label label-default" style="margin-right:2.5px;" ng-repeat="telefono in contacto.telefono">{{telefono.telefono}}</span>
						</td>
						<td colspan="1" class="info text-right">Correo(s) electrónico(s)</td>
						<td colspan="1" class="active">
							<span class="label label-default" style="margin-right:2.5px;" ng-repeat="correo in contacto.correo">{{correo.correo}}</span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!--<br>
<div class="container">
	<div class="row">
		<div class="col-sm-6">
			<table class="table table-striped">
				<tbody>
					<tr>
						<td class="success text-center" colspan="2"><strong>Identificación</strong></td>
					</tr>
					<tr>
						<td class="info text-right">Código</td>
						<td class="active">{{ctrl.info_torre.codigo}}</td>
					</tr>
					<tr>
						<td class="info text-right">Nombre</td>
						<td class="active">{{ctrl.info_torre.nombre}}</td>
					</tr>
					<tr>
						<td class="info text-right">Nombre completo</td>
						<td class="active">{{ctrl.info_torre.nombre_completo}}</td>
					</tr>
					<tr>
						<td class="success text-center" colspan="2"><strong>Ubicación</strong></td>
					</tr>
					<tr>
						<td class="info text-right">Departamento</td>
						<td class="active">{{ctrl.info_torre.zona_peru.departamento}}</td>
					</tr>
					<tr>
						<td class="info text-right">Provincia</td>
						<td class="active">{{ctrl.info_torre.zona_peru.provincia}}</td>
					</tr>
					<tr>
						<td class="info text-right">Distrito</td>
						<td class="active">{{ctrl.info_torre.zona_peru.distrito}}</td>
					</tr>
					<tr>
						<td class="info text-right">Coordenadas (Latitud, Longitud)</td>
						<td class="active">{{ctrl.info_torre.latitud}}°, {{ctrl.info_torre.longitud}}°</td>
					</tr>
					<tr>
						<td class="info text-right">Dirección</td>
						<td class="active">{{ctrl.info_torre.direccion}}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-sm-6">
			<table class="table table-striped">
				<tbody>
					<tr>
						<td class="success text-center" colspan="2"><strong>Características del sitio</strong></td>
					</tr>
					<tr>
						<td class="info text-right">Agregador</td>
						<td class="active">
							<span class="badge" ng-show="ctrl.info_torre.agregador == 1">Sí</span>
							<span class="badge" ng-hide="ctrl.info_torre.agregador == 1">No</span>
						</td>
					</tr>
					<tr>
						<td class="info text-right">Dorsal</td>
						<td class="active">
							<span class="badge" ng-show="ctrl.info_torre.dorsal == 1">Sí</span>
							<span class="badge" ng-hide="ctrl.info_torre.dorsal == 1">No</span>
						</td>
					</tr>
					<tr>
						<td class="info text-right">Tipo</td>
						<td class="active">
							<span class="badge" ng-show="ctrl.info_torre.in_building == 1">In_building</span>
							<span class="badge" ng-show="ctrl.info_torre.coubicado == 1">Coubicado</span>
						</td>
					</tr>
					<tr>
						<td class="success text-center" colspan="2"><strong>Torre y edificación</strong></td>
					</tr>
					<tr>
						<td class="info text-right">Año de construcción</td>
						<td class="active">{{ctrl.info_torre.anio_construccion}}</td>
					</tr>
					<tr>
						<td class="info text-right">Tipo de torre y altura</td>
						<td class="active">
							<div class="input-group input-group-sm">
								<div class="input-group-addon">{{ctrl.info_torre.tipo_torre.tipo_torre}}</div>
								<input type="text" class="form-control" value="{{ctrl.info_torre.altura_torre}}" readonly>
								<div class="input-group-addon">m</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="info text-right">Camuflaje</td>
						<td class="active">{{ctrl.info_torre.camuflaje.camuflaje}}</td>
					</tr>
					<tr>
						<td class="info text-right">Edificación</td>
						<td class="active">
							<div class="input-group input-group-sm">
								<div class="input-group-addon" ng-hide="ctrl.info_torre.greenfield_rooftop == 1">Greenfield</div>
								<div class="input-group-addon" ng-show="ctrl.info_torre.greenfield_rooftop == 1">Rooftop</div>
								<input type="text" class="form-control input-sm" value="{{ctrl.info_torre.altura_edificacion}}" readonly>
								<div class="input-group-addon">m</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>-->