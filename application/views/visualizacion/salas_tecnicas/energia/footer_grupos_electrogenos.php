<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
            ref.id_mso = <?php echo json_encode($id_mso) ;?>; 

			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			
			ref.cargando = true;
			
			ref.grupos_electrogenos = [];

			console.log(ref.id_mso);
            //navbar
			buscarSala = function(){            	
            	
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("sala_tecnica/listar_sala")?>"+"/"+ ref.id_mso,
					method:"GET"
				}).success(function(data){
					ref.cargando = false;
					if(data.length)
					{
						ref.salas = data;
						//console.log(ref.salas[0]['nombre']);
					  ref.salaname = ref.salas[0]['nombre'];
					}
					else
					{
						
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});
			}
           busca_items = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("mso_categoria_tipo/get_all")?>"+"/",
					method:"GET"
				}).success(function(data){
					//console.log(data);
					ref.cargando = false;
					if(data.length)
					{
					ref.dataItem = data;					
					
					}
					else
					{
						
					}              

				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});				
                }

             //navbar			
		   listarGruposElectrogenos = function(){
				$http({
					url:"<?php echo base_url("index.php/mso_grupos_electrogenos/listar_sitio");?>"+"/"+ref.id_mso,
					method:"GET"
				}).success(function(data){
					if(data.length)
					{
						ref.grupos_electrogenos = data;
						
						ref.cargando = false;
					}
					else
					{
						ref.cargando = false;
					}
				}).error(function(err){
					ref.cargando = false;
				});
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("index.php/sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						ref.reestablecerContrasena = {};
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
			};
			
			listarGruposElectrogenos();
			buscarSala();						
			busca_items();
		}]);
</script>
</body>
</html>