<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="ups in ctrl.ups" ng-if="ctrl.ups.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					UPS {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr class="text-center" style="background-color:#d2f3ce;">
								<td colspan="6">Ups</td>
							</tr>
							<tr>
								<td class="info ">Ubicación</td>
								<td class="default">{{ups.ubicacion}}</td>
								<td class="info">Marca</td>
								<td class="default">{{ups.tipo_ups.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{ups.tipo.modelo}}</td>
							</tr>							
						
							<tr>
								<td class="info">Número de serie</td>
								<td class="default">{{ups.numero_serie}}</td>
								<td class="info">Capacidad</td>
							<td class="default">{{ups.capacidad}}</td>
								<td class="info">Fases</td>
								<td class="default">{{ups.fases}}</td>
							</tr>							
							
							<tr>
								<td class="info">Voltage Entrada (VAC)</td>
								<td class="default">{{ups.voltaje_entrada}}</td>
								<td class="info">Transformador Asociado </td>
								<td class="default">{{ups.tipo_trasformador}}</td>
								<td class="info">Banco de Bateria Asociado</td>
								<td class="default">{{ups.banco_bateria}}</td>
							</tr>

							
							<tr>
							<td class="info">Voltaje Salida (VAC)</td>
								<td class="default">{{ups.voltaje_salida}} kW</td>
                                <td class="info"></td>
								<td class="default"></td>
                                <td class="info"></td>
								<td class="info"></td>
							</tr>



						</tbody>
					</table>
				</div>
			</div>
		</div>		
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.ups.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
</div>