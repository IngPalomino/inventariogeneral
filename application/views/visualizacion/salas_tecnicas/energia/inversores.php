<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="rectificador in ctrl.rectificadores" ng-if="ctrl.rectificadores.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Planta Rectificadora {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr>
								<td class="info">Marca</td>
								<td colspan="2" class="default">{{rectificador.tipo_controlador_rectif.marca}}</td>
								<td class="info">Controlador</td>
								<td colspan="2" class="default">{{rectificador.tipo_controlador_rectif.modelo}}</td>
							</tr>
							<tr>
								
								<td class="info">Fecha de puesta en Servicio</td>
								<td colspan="2" class="default">{{rectificador.anio_servicio | date:"dd/MM/yyyy"}}</td>
								

                                <td class="info">Part Number</td>
								<td class="default" colspan="2">{{rectificador.numero_serie}}</td>




								
							</tr>
							<tr>
								<td class="info">Energización</td>
								<td class="default" colspan="2">{{rectificador.energizacion_controlador_rectif.energizacion_controlador_rectif}}</td>
                                <td class="info">Capacidad Máxima</td>
								<td class="default" colspan="2">{{rectificador.capacidad_maxima}} kW</td>


								
							</tr>
							<tr>
                               <td class="info"></td>
								<td class="default" colspan="2">
									
								</td>


								<td class="info">Ubicación</td>
								<td class="default" colspan="2">{{rectificador.ubicacion_controlador_rectif.ubicacion_controlador_rectif}}</td>
								
							</tr>
						</tbody>
						<tbody>
							<tr class="success text-center">
								<td colspan="6">Módulos rectificadores</td>
							</tr>
							<tr ng-repeat="modulo in rectificador.modulos_rectif | filter:{eliminado:0}">
								<td class="info">Tipo</td>
								<td class="default">{{modulo.tipo_modulos_rectif.modelo}}</td>
								<td class="info">Capacidad</td>
								<td class="default">{{modulo.tipo_modulos_rectif.capacidad}} kW</td>
								<td class="info">Número de serie</td>
								<td class="default">{{modulo.numero_serie}}</td>
							</tr>
							<tr class="warning">
								<td class="info">Módulos Instalados</td>
								<td class="default" colspan="2">{{rectificador.modulosInstalados()}}</td>
								<td class="info">Capacidad Instalada</td>
								<td class="default" colspan="2">{{rectificador.capacidadInstalada()}} kW</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container ng-cloak" ng-if="!ctrl.rectificadores.length">
	<div class="well text-center">
		No hay información
	</div>
</div>