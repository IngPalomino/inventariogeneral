<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="subestacion in ctrl.subestacion" ng-if="ctrl.subestacion.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Sub Estación {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>				
												
                            <tr>
								<td class="info">Marca del Transformador</td>
								<td class="">{{subestacion.tipo_subestacion.marca}}</td>								
								<td class="info">Modelo</td>
								<td>{{subestacion.tipo_subestacion.modelo}}</td>
							</tr>
                          
                            <tr>
								 <td class="info">Potencia</td>
								 <td>{{subestacion.potencia_transformador}} KVA</td>
						         <td class="info">Número de Serie</td>
						         <td>{{subestacion.numero_serie}}</td>
                            </tr>
                            
						    <tr>
							 	<td class="info">Fases</td>
								<td>{{subestacion.fases}}</td>	
								
								<td class="info">Conexión</td>
								<td>{{subestacion.conexion}}</td>
							</tr>
							<tr >
								<td class="info">Tipo</td>
								<td></td>
								<td class="info">Voltaje de Entrada(KVA)</td>
								<td>{{subestacion.voltaje_entrada}}</td>
							</tr>
							<tr>							
								<td class="info">Voltaje de Salida(KVA)</td>
								<td>{{subestacion.voltaje_salida}}</td>
							</tr>
						</tbody>
					</table>
				</div>	
			</div>
		</div>
	</div>
</div>
<div class="container ng-cloak" ng-if="!ctrl.lineas_electricas.length">
	<div class="well text-center">
		No hay información
	</div>
</div>