<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="grupo in ctrl.grupos_electrogenos" ng-if="ctrl.grupos_electrogenos.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Grupo Electrógeno {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr class="text-center" style="background-color:#d2f3ce;">
								<td colspan="6">Grupo Electrógeno</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_grupo_electrogeno.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_grupo_electrogeno.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_grupo_electrogeno}}</td>
							</tr>
							<tr class="text-center" style="background-color:#b9d1e5;">
								<td colspan="6">Motor</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_motor_ge.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_motor_ge.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_motor}}</td>
							</tr>
							<tr class="text-center" style="background-color:#d7e7b4;">
								<td colspan="6">Generador</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_generador_ge.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_generador_ge.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_generador}}</td>
							</tr>
							<tr>
								<td class="info">Potencia</td>
								<td class="default">{{grupo.tipo_generador_ge.potencia}} kW</td>
								<td class="info">Corriente</td>
								<td class="default">{{grupo.tipo_generador_ge.corriente}} A</td>
								<td class="info" colspan="2"></td>
							</tr>
							<tr class="text-center" style="background-color:#e7d7b4;">
								<td colspan="6">AVR</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_avr_ge.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_avr_ge.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_avr}}</td>
							</tr>
							<tr class="text-center bg-success">
								<td colspan="6">Controlador GE</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_controlador_ge.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_controlador_ge.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_controlador_ge}}</td>
							</tr>
							<tr class="text-center" style="background-color:#f7f7ca;">
								<td colspan="6">Controlador TTA</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{grupo.tipo_controlador_tta.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{grupo.tipo_controlador_tta.modelo}}</td>
								<td class="info">Número de serie</td>
								<td class="default">{{grupo.numero_serie_controlador_tta}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.grupos_electrogenos.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
</div>