<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http","$scope",function($http,$scope){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};
            var id_mso = document.getElementById('id_mso').value;
			ref.error = {};
			ref.success = {};
			ref.advertencias = [];
			ref.energiaItems = [];
			//ref.dataItem = [];
			//ref.gabinetes = [];
			 ref.energiadata = [];                    
             ref.gabinetes = [];
             ref.clima = [];
             ref.otros = [];
						
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			ref.auth.evaluador = (ref.session.evaluador == 1)? '' : 'disabled';

			ref.procesando = false;
			ref.archivoPorAprobar = "";
			ref.archivoAnalizado = false;
  // menu

          
            buscarSala = function(){            	
            	
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("sala_tecnica/listar_sala")?>"+"/"+id_mso,
					method:"GET"
				}).success(function(data){
					ref.cargando = false;
					if(data.length)
					{
						ref.salas = data;
						//console.log(ref.salas[0]['nombre']);
					  ref.salaname = ref.salas[0]['nombre'];
					}
					else
					{
						
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});
			}

			//mso tb categoria	
            //energia 1
            //gabinetes 2
            //clima 3
            //otros 4 

            busca_items = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("mso_categoria_tipo/get_all")?>"+"/",
					method:"GET"
				}).success(function(data){
					//console.log(data);
					ref.cargando = false;
					if(data.length)
					{
					ref.dataItem = data;					
					
					}
					else
					{
						
					}              

				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});				
                }
      // menu

	        ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						ref.reestablecerContrasena = {};
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};
			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
				ref.reestablecerContrasena = {};
			};


             jsonTest = function(){
		        //  $http({
				// 	url:"http://104.236.90.122/api/equipos",
				// 	method:"GET",
				// 	headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				// }).success(function(data){
				// 	if(data.length)
				// 	{
				// 		ref.data = data;
				// 		console("test");
				// 	}
				// 	else
				// 	{

				// 	}
				// }).error(function(err){

				// });
				// $.getJSON('http://md5.jsontest.com/?text=%5Btext', function(data) {
				//    console.log(data);
				// });


				
		   };




            jsonTest();
			buscarSala();		
			busca_items();
			//busca_clima_items(3);
			//busca_otros_items(4);
		}])
		
</script>
</body>
</html>