<br>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Georeferenciación
				</div>
				<div class="panel-body">
					<div id="map" style="width:100%;height:480px;"></div>
				</div>
				<div class="panel-footer">
					<div class="row">
						<div class="col-md-4 text-center">
							<img src="<?php echo base_url("assets/images/maps/red-dot.png"); ?>"> Sitio Actual
						</div>
						<div class="col-md-4 text-center">
							<img src="<?php echo base_url("assets/images/maps/yellow-dot.png"); ?>"> ON Air
						</div>
						<div class="col-md-4 text-center">
							<img src="<?php echo base_url("assets/images/maps/purple-dot.png"); ?>"> OFF Air
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="infoSitio">
	<div class="panel panel-primary">
		<div class="panel-heading text-center">
			<strong><?php echo $infoSitio[0]["nombre_completo"]; ?> (<?php echo "P".$infoSitio[0]["prioridad"]; ?>)<strong>
		</div>
		<div class="panel-body">
			<div class="text-center">
				<i class="fa fa-spinner fa-spin fa-3x" ng-if="ctrl.cargando && !ctrl.info_sitio.length"></i>
			</div>
			<div class="col-xs-6" ng-repeat="info in ctrl.info_sitio" ng-if="!ctrl.cargando && ctrl.info_sitio.length">
				<a href='<?php echo base_url("ver/sitios/{{info.link}}/$sitio"); ?>' target="_blank">{{info.objeto}}: {{info.cantidad}}</a>
			</div>
		</div>
		<div class="panel-footer text-center">
			<button class="btn btn-xs btn-warning" ng-click="ctrl.dibujarEnlaces()">Mostrar Enlaces</button>
		</div>
	</div>
</div>