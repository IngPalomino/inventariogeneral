<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="aire in ctrl.aires_acondicionados" ng-if="ctrl.aires_acondicionados.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Aire Acondicionado {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{aire.tipo_aires_acondicionados.marca}}</td>
								<td class="info">Tipo de Equipo</td>
								<td class="default">{{aire.tipo_aires_acondicionados.tipo_equipo}}</td>
							</tr>
							<tr>
								<td class="info">Capacidad</td>
								<td class="default">{{aire.tipo_aires_acondicionados.capacidad}} Ton</td>
								<td class="info">Fecha de Instalación</td>
								<td class="default">{{aire.fecha_instalacion | date:"dd/MM/yyyy"}}</td>
							</tr>
							<tr ng-if="aire.tipo_aires_acondicionados.tipo_equipo == 'Mochila'">
								<td class="info">Modelo</td>
								<td class="default">{{aire.tipo_aires_acondicionados.modelo}}</td>
								<td class="info">Número de Serie</td>
								<td class="default">{{aire.num_serie_aire_acon}}</td>
							</tr>
							<tr style="background-color:#d7bde2;" class="text-center">
								<td colspan="4">Compresor</td>
							</tr>
							<tr>
								<td class="info">Marca</td>
								<td class="default">{{aire.tipo_compresor.marca}}</td>
								<td class="info">Modelo</td>
								<td class="default">{{aire.tipo_compresor.modelo}}</td>
							</tr>
							<tr>
								<td class="info" colspan="2">Tipo</td>
								<td class="default" colspan="2">{{aire.tipo_compresor.tipo}}</td>
							</tr>
							<tr class="success text-center" ng-if-start="aire.tipo_aires_acondicionados.tipo_equipo != 'Mochila'">
								<td colspan="4">Condensador</td>
							</tr>
							<tr>
								<td class="info">Modelo</td>
								<td class="default">{{aire.tipo_condensador.modelo}}</td>
								<td class="info">Número de Serie</td>
								<td class="default">{{aire.num_serie_condensador}}</td>
							</tr>
							<tr style="background-color:#fae5d3" class="text-center">
								<td colspan="4">Evaporador</td>
							</tr>
							<tr ng-if-end>
								<td class="info">Modelo</td>
								<td class="default">{{aire.tipo_evaporador.modelo}}</td>
								<td class="info">Número de Serie</td>
								<td class="default">{{aire.num_serie_evaporador}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container ng-cloak" ng-if="!ctrl.aires_acondicionados.length">
	<div class="well text-center">
		No hay información
	</div>
</div>