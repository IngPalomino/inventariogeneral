<br>
<div class="container">
	<div class="text-center">
		<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-if="ctrl.antenas.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Antenas
				</div>
				<div class="panel-body" style="max-width:1200px;max-height:300px;overflow:scroll;">
					<table class="table table-striped table-condensed" style="width:3000px;">
						<thead>
							<tr>
								<th>NEFdn</th>
								<th>Antenna Device ID</th>
								<th>Antenna Device Type</th>
								<th>Date of Manufacture</th>
								<th>BOM Code</th>
								<th>Manufacturer Data</th>
								<th>Antenna Model</th>
								<th>Serial Number (Antenna Motors)</th>
								<th>Unit Position</th>
								<th>Vendor Name</th>
								<th>Hardware Version</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="antena in ctrl.antenas">
								<td>{{antena.nefdn}}</td>
								<td>{{antena.antenna_device_id}}</td>
								<td>{{antena.antenna_device_type}}</td>
								<td>
									<span ng-if="antena.date_manufacture_physical">{{antena.date_manufacture_physical | date:"dd/MM/yyyy"}}</span>
									<span ng-if="!antena.date_manufacture_physical">{{antena.date_manufacture_m2000 | date:"dd/MM/yyyy"}}</span>
								</td>
								<td>{{antena.bom_code}}</td>
								<td style="white-space:nowrap;">{{antena.manufacturer_data}}</td>
								<td>{{antena.antenna_model}}</td>
								<td>{{antena.serial_number_motor}}</td>
								<td>{{antena.unit_position}}</td>
								<td>{{antena.vendor_name}}</td>
								<td>{{antena.hardware_version}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	
		<div class="col-xs-12" ng-if="ctrl.tarjetas.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Tarjetas
				</div>
				<div class="panel-body" style="max-width:1200px;max-height:300px;overflow:scroll;">
					<table class="table table-striped table-condensed" style="width:3000px;">
						<thead>
							<tr>
								<th>NEFdn</th>
								<th>Board Name</th>
								<th>Board Type</th>
								<th>Date of Manufacture</th>
								<th>BOM Code</th>
								<th>Manufacturer Data</th>
								<th>Serial Number</th>
								<th>Software Version</th>
								<th>Unit Position</th>
								<th>Vendor Name</th>
								<th>Vendor Unit Family Type</th>
								<th>Hardware Version</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="tarjeta in ctrl.tarjetas">
								<td>{{tarjeta.nefdn}}</td>
								<td>{{tarjeta.board_name}}</td>
								<td>{{tarjeta.board_type}}</td>
								<td>{{tarjeta.date_manufacture | date:"dd/MM/yyyy"}}</td>
								<td>{{tarjeta.bom_code}}</td>
								<td style="white-space:nowrap;">{{tarjeta.manufacturer_data}}</td>
								<td>{{tarjeta.serial_number}}</td>
								<td>{{tarjeta.software_version}}</td>
								<td>{{tarjeta.unit_position}}</td>
								<td>{{tarjeta.vendor_name}}</td>
								<td>{{tarjeta.vendor_unit_family_type}}</td>
								<td>{{tarjeta.hardware_version}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="col-xs-12" ng-if="ctrl.gabinetes.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Gabinetes
				</div>
				<div class="panel-body" style="max-width:1200px;max-height:300px;overflow:scroll;">
					<table class="table table-striped table-condensed" style="width:3000px;">
						<thead>
							<tr>
								<th>NEFdn</th>
								<th>Model (Physical)</th>
								<th>Type (Physical)</th>
								<th>Date of Manufacture</th>
								<th>BOM Code</th>
								<th>Manufacturer Data</th>
								<th>Rack Type (Physical)</th>
								<th>Rack Type (M2000)</th>
								<th>Serial Number</th>
								<th>Vendor Name</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="gabinete in ctrl.gabinetes">
								<td>{{gabinete.nefdn}}</td>
								<td>{{gabinete.model_physical}}</td>
								<td>{{gabinete.type_physical}}</td>
								<td>
									<span ng-if="gabinete.date_manufacture_physical">{{gabinete.date_manufacture_physical | date:"dd/MM/yyyy"}}</span>
									<span ng-if="!gabinete.date_manufacture_physical">{{gabinete.date_manufacture_m2000 | date:"dd/MM/yyyy"}}</span>
								</td>
								<td>{{gabinete.bom_code}}</td>
								<td style="white-space:nowrap;">{{gabinete.manufacturer_data}}</td>
								<td>{{gabinete.rack_type_physical}}</td>
								<td>{{gabinete.rack_type_m2000}}</td>
								<td>
									<span ng-if="gabinete.serial_number_physical">{{gabinete.serial_number_physical}}</span>
									<span ng-if="!gabinete.serial_number_physical">{{gabinete.serial_number_m2000}}</span>
								</td>
								<td>{{gabinete.vendor_name}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>