<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url("ver/sitios");?>" class="btn boton" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-university fa-5x"></span></td></td></tr>
							<tr><td>Resumen por sitio</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-xs-6">
				<a href class="btn boton" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-list fa-5x"></span></td></td></tr>
							<tr><td>Listado por objetos</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url("ver/bodegas");?>" class="btn boton" role="button">
					<table class="table">
						<tbody>
							<tr><td><span class="fa fa-archive fa-5x"></span></td></td></tr>
							<tr><td>Bodegas virtuales</td></tr>
						</tbody>
					</table>
				</a>
			</div>
		</div>
	</div>
</section>