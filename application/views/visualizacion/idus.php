<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-if="ctrl.idus.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					IDUs
				</div>
				<div class="panel-body">
					<table class="table table-striped table-condensed">
						<thead>
							<tr>
								<th>NE ID</th>
								<th>Link Name</th>
								<th>Marca</th>
								<th>Modelo</th>
								<th>Número de serie</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="idu in ctrl.idus">
								<td>{{idu.ne_id}}</td>
								<td>{{idu.link_name}}</td>
								<td>{{idu.tipo_idu.marca}}</td>
								<td bs-popover>
									<button class="btn btn-xs btn-primary" ng-click="ctrl.verIDU(idu)">{{idu.tipo_idu.modelo}}</button>
									<span style="color:#326497;" class="fa fa-info-circle" rel="popover" data-trigger="hover" data-html="true" data-container="body" data-toggle="popover" data-title="Datos generales de IDU" data-content="<div style='max-width:320px;min-width:270px;'><i class='fa fa-asterisk'></i> Consumo de Energía: {{idu.tipo_idu.consumo_energia}}<br><i class='fa fa-asterisk'></i> Capacidad de Conmutación: {{idu.tipo_idu.capacidad_conmutacion}} GHz<br><i class='fa fa-asterisk'></i> Peso: {{idu.tipo_idu.peso}} Kg<br><i class='fa fa-asterisk'></i> Dimensiones: {{idu.tipo_idu.dimensiones}}</div>"></span>
									<!--<button class="btn btn-xs btn-primary" ng-click="ctrl.verIDU(idu)"><i class="fa fa-eye"></i></button>-->
								</td>
								<td>{{idu.numero_serie}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container ng-cloak" ng-if="!ctrl.idus.length">
	<div class="well text-center">
		No hay información
	</div>
</div>

<div id="verIDU" class="popup" style="min-width:650px;">
	<span class="button b-close">
		<span>X</span>
	</span>

	<h3>{{ctrl.editarIDU.tipo_idu.marca}} {{ctrl.editarIDU.tipo_idu.modelo}} {{ctrl.editarIDU.ne_id}}</h3>

	<p ng-if="ctrl.editarIDU.tipo_idu.id == 1" style="position: relative;">
		<img src="<?php echo base_url("assets/images/idus/1/1.png"); ?>" usemap="#map1">
		<a href ng-if="ctrl.editarIDU.ife_3"><img src="<?php echo base_url("assets/images/idus/1/IFE3.png"); ?>" style="position:absolute;right:53.4%;top:65%;"></a>
		<a href ng-if="ctrl.editarIDU.ife_4"><img src="<?php echo base_url("assets/images/idus/1/IFE4.png"); ?>" style="position:absolute;right:11%;top:65%;"></a>
		<a href ng-if="ctrl.editarIDU.odu_23"><img src="<?php echo base_url("assets/images/idus/1/ODU_23.png"); ?>" style="position:absolute;right:54%;top:8%;"></a>
		<a href ng-if="ctrl.editarIDU.odu_24"><img src="<?php echo base_url("assets/images/idus/1/ODU_24.png"); ?>" style="position:absolute;right:12%;top:8%;"></a>

		<a href ng-if="ctrl.editarIDU.fe_1"><img src="<?php echo base_url("assets/images/idus/1/FE1.png"); ?>" style="position:absolute;right:76.2%;top:42.8%;"></a>
		<a href ng-if="ctrl.editarIDU.fe_2"><img src="<?php echo base_url("assets/images/idus/1/FE2.png"); ?>" style="position:absolute;right:56.2%;top:41.9%;"></a>
		<a href ng-if="ctrl.editarIDU.fe_3"><img src="<?php echo base_url("assets/images/idus/1/FE3.png"); ?>" style="position:absolute;right:36.2%;top:42.8%;"></a>
		<a href ng-if="ctrl.editarIDU.fe_4"><img src="<?php echo base_url("assets/images/idus/1/FE4.png"); ?>" style="position:absolute;right:17.2%;top:42.8%;"></a>
	</p>
	<p ng-if="ctrl.editarIDU.tipo_idu.id == 2" style="position: relative;">
		<img src="<?php echo base_url("assets/images/idus/2/2.png"); ?>" usemap="#map2">
		<a href ng-if="ctrl.editarIDU.ml_1"><img src="<?php echo base_url("assets/images/idus/2/ML1.png"); ?>" style="position:absolute;right:2.8%;top:76.2%;"></a>
		<a href ng-if="ctrl.editarIDU.ifu_2"><img src="<?php echo base_url("assets/images/idus/2/IFU2.png"); ?>" style="position:absolute;right:2.8%;top:76.2%;"></a>
		<a href ng-if="ctrl.editarIDU.ifu_3"><img src="<?php echo base_url("assets/images/idus/2/IFU3.png"); ?>" style="position:absolute;right:49.8%;top:60.8%;"></a>
		<a href ng-if="ctrl.editarIDU.ifu_4"><img src="<?php echo base_url("assets/images/idus/2/IFU4.png"); ?>" style="position:absolute;right:2.8%;top:60.8%;"></a>
		<a href ng-if="ctrl.editarIDU.ifu_5"><img src="<?php echo base_url("assets/images/idus/2/IFU5.png"); ?>" style="position:absolute;right:49.8%;top:47.8%;"></a>
		<a href ng-if="ctrl.editarIDU.ifu_6"><img src="<?php echo base_url("assets/images/idus/2/IFU6.png"); ?>" style="position:absolute;right:2.8%;top:47.8%;"></a>
		<a href ng-if="ctrl.editarIDU.odu_23"><img src="<?php echo base_url("assets/images/idus/2/ODU_23.png"); ?>" style="position:absolute;right:53.2%;top:24.5%;"></a>
		<a href ng-if="ctrl.editarIDU.odu_24"><img src="<?php echo base_url("assets/images/idus/2/ODU_24.png"); ?>" style="position:absolute;right:53.5%;top:5.5%;"></a>
		<a href ng-if="ctrl.editarIDU.odu_25"><img src="<?php echo base_url("assets/images/idus/2/ODU_25.png"); ?>" style="position:absolute;right:7.5%;top:24.4%;"></a>
		<a href ng-if="ctrl.editarIDU.odu_26"><img src="<?php echo base_url("assets/images/idus/2/ODU_26.png"); ?>" style="position:absolute;right:7.5%;top:5.5%;"></a>
		<a href ng-if="ctrl.editarIDU.ge_1" bs-popover><img src="<?php echo base_url("assets/images/idus/2/GE1.png"); ?>" style="position:absolute;right:87%;top:76.5%;" rel="popover" data-trigger="hover" data-toggle="popover" data-content="{{ctrl.editarIDU.ge_1[0].port_name}}"></a>
		<a href ng-if="ctrl.editarIDU.ge_2" bs-popover><img src="<?php echo base_url("assets/images/idus/2/GE2.png"); ?>" style="position:absolute;right:77.4%;top:76.7%;" rel="popover" data-trigger="hover" data-toggle="popover" data-content="{{ctrl.editarIDU.ge_2[0].port_name}}"></a>
		<a href ng-if="ctrl.editarIDU.fe_1" bs-popover><img src="<?php echo base_url("assets/images/idus/2/FE1.png"); ?>" style="position:absolute;right:67.8%;top:76.7%;" rel="popover" data-trigger="hover" data-toggle="popover" data-content="{{ctrl.editarIDU.fe_1[0].port_name}}"></a>
		<a href ng-if="ctrl.editarIDU.fe_2" bs-popover><img src="<?php echo base_url("assets/images/idus/2/FE2.png"); ?>" style="position:absolute;right:58.2%;top:76.7%;" rel="popover" data-trigger="hover" data-toggle="popover" data-content="{{ctrl.editarIDU.fe_2[0].port_name}}"></a>
		<a href ng-if="ctrl.editarIDU.fe_3" bs-popover><img src="<?php echo base_url("assets/images/idus/2/FE3.png"); ?>" style="position:absolute;right:48.6%;top:76.7%;" rel="popover" data-trigger="hover" data-toggle="popover" data-content="{{ctrl.editarIDU.fe_3[0].port_name}}"></a>
		<a href ng-if="ctrl.editarIDU.fe_4" bs-popover><img src="<?php echo base_url("assets/images/idus/2/FE4.png"); ?>" style="position:absolute;right:38.8%;top:76.7%;" rel="popover" data-trigger="hover" data-toggle="popover" data-content="{{ctrl.editarIDU.fe_4[0].port_name}}"></a>
	</p>
	<p ng-if="ctrl.editarIDU.tipo_idu.id == 3" style="position: relative;">
		<img src="<?php echo base_url("assets/images/idus/3/3.png"); ?>" usemap="#map3">
		<a href ng-if="ctrl.editarIDU.ge_1" bs-popover><img src="<?php echo base_url("assets/images/idus/3/GE1.png"); ?>" style="position:absolute;right:78.5%;top:46.2%;" rel="popover" data-trigger="hover" data-toggle="popover" data-content="{{ctrl.editarIDU.ge_1[0].port_name}}"></a>
		<a href ng-if="ctrl.editarIDU.ge_2" bs-popover><img src="<?php echo base_url("assets/images/idus/3/GE2.png"); ?>" style="position:absolute;right:65.3%;top:45.8%;" rel="popover" data-trigger="hover" data-toggle="popover" data-content="{{ctrl.editarIDU.ge_2[0].port_name}}"></a>
		<a href ng-if="ctrl.editarIDU.ge_3" bs-popover><img src="<?php echo base_url("assets/images/idus/3/GE3.png"); ?>" style="position:absolute;right:51.6%;top:46.3%;" rel="popover" data-trigger="hover" data-toggle="popover" data-content="{{ctrl.editarIDU.ge_3[0].port_name}}"></a>
		<a href ng-if="ctrl.editarIDU.ge_4" bs-popover><img src="<?php echo base_url("assets/images/idus/3/GE4.png"); ?>" style="position:absolute;right:38.6%;top:46.3%;" rel="popover" data-trigger="hover" data-toggle="popover" data-content="{{ctrl.editarIDU.ge_4[0].port_name}}"></a>
		<a href ng-if="ctrl.editarIDU.ge_5" bs-popover><img src="<?php echo base_url("assets/images/idus/3/GE5.png"); ?>" style="position:absolute;right:25.5%;top:46.1%;" rel="popover" data-trigger="hover" data-toggle="popover" data-content="{{ctrl.editarIDU.ge_5[0].port_name}}"></a>
		<a href ng-if="ctrl.editarIDU.ge_6" bs-popover><img src="<?php echo base_url("assets/images/idus/3/GE6.png"); ?>" style="position:absolute;right:11.8%;top:46.4%;" rel="popover" data-trigger="hover" data-toggle="popover" data-content="{{ctrl.editarIDU.ge_6[0].port_name}}"></a>
		<a href ng-if="ctrl.editarIDU.ml1_1"><img src="<?php echo base_url("assets/images/idus/3/ML1.png"); ?>" style="position:absolute;right:51.8%;top:82.2%;"></a>
		<a href ng-if="ctrl.editarIDU.ml1_2"><img src="<?php echo base_url("assets/images/idus/3/ML1.png"); ?>" style="position:absolute;right:8%;top:82.4%;"></a>
		<a href ng-if="ctrl.editarIDU.ml1_3"><img src="<?php echo base_url("assets/images/idus/3/ML1.png"); ?>" style="position:absolute;right:51.8%;top:72.7%;"></a>
		<a href ng-if="ctrl.editarIDU.ml1_4"><img src="<?php echo base_url("assets/images/idus/3/ML1.png"); ?>" style="position:absolute;right:8%;top:72.7%;"></a>
		<a href ng-if="ctrl.editarIDU.ml1_5"><img src="<?php echo base_url("assets/images/idus/3/ML1.png"); ?>" style="position:absolute;right:51.8%;top:62.2%;"></a>
		<a href ng-if="ctrl.editarIDU.ml1_6"><img src="<?php echo base_url("assets/images/idus/3/ML1.png"); ?>" style="position:absolute;right:8%;top:62.2%;"></a>
		<a href ng-if="ctrl.editarIDU.isv3_1"><img src="<?php echo base_url("assets/images/idus/3/ISV3_1.png"); ?>" style="position:absolute;right:51.8%;top:82.2%;"></a>
		<a href ng-if="ctrl.editarIDU.isv3_2"><img src="<?php echo base_url("assets/images/idus/3/ISV3_2.png"); ?>" style="position:absolute;right:8%;top:82.4%;"></a>
		<a href ng-if="ctrl.editarIDU.isv3_3"><img src="<?php echo base_url("assets/images/idus/3/ISV3_3.png"); ?>" style="position:absolute;right:51.8%;top:72.7%;"></a>
		<a href ng-if="ctrl.editarIDU.isv3_4"><img src="<?php echo base_url("assets/images/idus/3/ISV3_4.png"); ?>" style="position:absolute;right:8%;top:72.1%;"></a>
		<a href ng-if="ctrl.editarIDU.isv3_5"><img src="<?php echo base_url("assets/images/idus/3/ISV3_5.png"); ?>" style="position:absolute;right:52%;top:62%;"></a>
		<a href ng-if="ctrl.editarIDU.isv3_6"><img src="<?php echo base_url("assets/images/idus/3/ISV3_6.png"); ?>" style="position:absolute;right:8%;top:61.8%;"></a>
		<a href ng-if="ctrl.editarIDU.odu_21"><img src="<?php echo base_url("assets/images/idus/3/ODU_21.png"); ?>" style="position:absolute;right:66.5%;top:6.7%;"></a>
		<a href ng-if="ctrl.editarIDU.odu_22"><img src="<?php echo base_url("assets/images/idus/3/ODU_22.png"); ?>" style="position:absolute;right:66.5%;top:24.4%;"></a>
		<a href ng-if="ctrl.editarIDU.odu_23"><img src="<?php echo base_url("assets/images/idus/3/ODU_23.png"); ?>" style="position:absolute;right:39.7%;top:6.7%;"></a>
		<a href ng-if="ctrl.editarIDU.odu_24"><img src="<?php echo base_url("assets/images/idus/3/ODU_24.png"); ?>" style="position:absolute;right:39.9%;top:24.6%;"></a>
		<a href ng-if="ctrl.editarIDU.odu_25"><img src="<?php echo base_url("assets/images/idus/3/ODU_25.png"); ?>" style="position:absolute;right:13.3%;top:7.4%;"></a>
		<a href ng-if="ctrl.editarIDU.odu_26"><img src="<?php echo base_url("assets/images/idus/3/ODU_26.png"); ?>" style="position:absolute;right:13.1%;top:25%;"></a>
	</p>
	<p ng-if="ctrl.editarIDU.tipo_idu.id == 4"><img src="<?php echo base_url("assets/images/idus/4/4.png"); ?>" usemap="#map4"></p>
</div>