<br>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Documentación
				</div>
				<div class="panel-body">
				
					<div class="text-center">
						<span class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></span>
					</div>
					<div id="docs">
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>