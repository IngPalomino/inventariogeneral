<br>
<div class="container">
	<div class="row ng-cloak">
		<div class="well text-center" ng-if="ctrl.info_sitio.coubicador.id != 1">
			Está coubicado en {{ctrl.info_sitio.coubicador.coubicador}}
		</div>
		<div class="col-xs-12" ng-repeat="suministro in ctrl.suministros" ng-if="ctrl.suministros.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Suministro {{$index + 1}}
				</div>
				<div class="panel-body">
					<table class="table table-condensed">
						<tbody>
							<tr>
								<td class="info">Tipo de alimentación</td>
								<td class="default">{{suministro.tipo_alimentacion.tipo_alimentacion}}</td>
								<td class="info">Concesionaria</td>
								<td class="default">{{suministro.concesionaria_suministro.concesionaria_suministro}}</td>
							</tr>
							<tr>
								<td class="info">Conexión</td>
								<td class="default">{{suministro.conexion_suministro.conexion_suministro}}</td>
								<td class="info">Suministro</td>
								<td class="default">{{suministro.numero_suministro}}</td>
							</tr>
							<tr>
								<td class="info">Potencia contratada</td>
								<td class="default">{{suministro.potencia_contratada}} KW</td>
								<td class="info">Opcion tarifaria</td>
								<td class="default">{{suministro.opcion_tarifaria.opcion_tarifaria}}</td>
							</tr>
							<tr>
								<td class="info">Nivel de tensión</td>
								<td class="default">{{suministro.nivel_tension}} KV</td>
								<td class="info">Sistema</td>
								<td class="default">{{suministro.sistema_suministro.sistema_suministro}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container ng-cloak" ng-if="!ctrl.suministros.length">
	<div class="well text-center">
		No hay información
	</div>
</div>