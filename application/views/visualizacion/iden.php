<br>
<div class="container">
	<div class="text-center">
		<i class="fa fa-spinner fa-spin fa-5x" ng-if="ctrl.cargando"></i>
	</div>
	<div class="row ng-cloak">
		<div class="col-xs-12" ng-repeat="iden in ctrl.iden" ng-if="!ctrl.cargando && ctrl.iden.length">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					iDEN
				</div>
				<div class="panel-body">
					<table class="table table-condensed table-responsive" style="table-layout: fixed;">
						<tr>
							<td class="info">Site Name</td>
							<td class="default">{{iden.site_name}}</td>
							<td class="info">Antennas</td>
							<td class="default">{{iden.antennas}}</td>
							<td class="info">BRs</td>
							<td class="default">{{iden.brs}}</td>
						</tr>
						<tr>
							<td class="info">Duplexor</td>
							<td class="default">{{iden.duplexor}}</td>
							<td class="info">QBRs</td>
							<td class="default">{{iden.qbrs}}</td>
							<td class="info">Cabinet MSER</td>
							<td class="default">{{iden.cabinet_mser}}</td>
						</tr>
						<tr>
							<td class="info">Cabinet RF</td>
							<td class="default">{{iden.cabinet_rf}}</td>
							<td class="info">Cabinet Control</td>
							<td class="default">{{iden.cabinet_control}}</td>
							<td class="info">RFN</td>
							<td class="default">{{iden.rfn}}</td>
						</tr>
						<tr>
							<td class="info">EBTS Type</td>
							<td class="default">{{iden.ebts_type}}</td>
							<td class="info">iBSC Name</td>
							<td class="default">{{iden.ibsc_name}}</td>
							<td class="info">ACG ID (EBTS Controller)</td>
							<td class="default">{{iden.acg_id}}</td>
						</tr>
						<tr>
							<td class="info">Site IP Address</td>
							<td class="default">{{iden.site_ip_address}}</td>
							<td class="info">ACG Port</td>
							<td class="default">{{iden.acg_port}}</td>
							<td class="info">OMC Port</td>
							<td class="default">{{iden.omc_port}}</td>
						</tr>
						<tr>
							<td class="default" colspan="2"></td>
							<td class="success">Link Status</td>
							<td class="success">{{iden.link_status}}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="col-xs-12 ng-cloak" ng-if="!ctrl.iden.length && !ctrl.cargando">
			<div class="well text-center">
				No hay información
			</div>
		</div>
	</div>
</div>