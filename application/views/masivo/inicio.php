<div class="container panel-menu">
	<div class="row">
		<div class="col-xs-12">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<a href="<?php echo base_url("ingreso_masivo/antenas_gul"); ?>" class="btn boton" ng-class="<?php echo !isPermitted($session["id_usuario"], 3)? "'disabled'" : ""; ?>">
					<table class="table">
						<tbody>
							<tr><td><i class="fa fa-upload fa-5x"></i></td></tr>
							<tr><td>Antenas GUL</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<a href="<?php echo base_url("ingreso_masivo/suministros"); ?>" class="btn boton" ng-class="<?php echo !isPermitted($session["id_usuario"], 9)? "'disabled'" : ""; ?>">
					<table class="table">
						<tbody>
							<tr><td><i class="fa fa-upload fa-5x"></i></td></tr>
							<tr><td>Suministros</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<a href="<?php echo base_url("ingreso_masivo/idus"); ?>" class="btn boton" ng-class="<?php echo !isPermitted($session["id_usuario"], 4)? "'disabled'" : ""; ?>">
					<table class="table">
						<tbody>
							<tr><td><i class="fa fa-upload fa-5x"></i></td></tr>
							<tr><td>IDUs</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<a href="<?php echo base_url("ingreso_masivo/enlaces"); ?>" class="btn boton" ng-class="<?php echo !isPermitted($session["id_usuario"], 5)? "'disabled'" : ""; ?>">
					<table class="table">
						<tbody>
							<tr><td><i class="fa fa-upload fa-5x"></i></td></tr>
							<tr><td>Enlaces</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<a href="<?php echo base_url("ingreso_masivo/pop"); ?>" class="btn boton" ng-class="<?php echo !isPermitted($session["id_usuario"], 2)? "'disabled'" : ""; ?>">
					<table class="table">
						<tbody>
							<tr><td><i class="fa fa-upload fa-5x"></i></td></tr>
							<tr><td>POP</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<a href="<?php echo base_url("ingreso_masivo/rectificadores"); ?>" class="btn boton" ng-class="<?php echo !isPermitted($session["id_usuario"], 7)? "'disabled'" : ""; ?>">
					<table class="table">
						<tbody>
							<tr><td><i class="fa fa-upload fa-5x"></i></td></tr>
							<tr><td>Rectificadores</td></tr>
						</tbody>
					</table>
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<a href="<?php echo base_url("ingreso_masivo/bancos_baterias"); ?>" class="btn boton" ng-class="<?php echo !isPermitted($session["id_usuario"], 8)? "'disabled'" : ""; ?>">
					<table class="table">
						<tbody>
							<tr><td><i class="fa fa-upload fa-5x"></i></td></tr>
							<tr><td>Bancos de Baterías</td></tr>
						</tbody>
					</table>
				</a>
			</div>
		</div>
	</div>
</div>