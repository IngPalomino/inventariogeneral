<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>POP</h2>
			</div>
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url("ingreso_masivo"); ?>">Ingreso Masivo</a></li>
				<li class="active">POP</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					POP
				</div>
				<div class="panel-body">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="well">
							<label>Plantilla de ingreso:</label>
							<a href="<?php echo base_url("ingreso_masivo/plantillas/pop");?>">plantilla_pop.xlsx</a><br>
							<label>Campos desplegables:</label>
							<a href="<?php echo base_url("ingreso_masivo/plantillas/desplegables_pop");?>">plantilla_campos_desplegables.xlsx</a>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading text-center">
								Mis solicitudes
							</div>
							<div class="panel-body" style="height: 276px;word-wrap: break-word;overflow-y: auto;">
								<table class="table table-condensed table-responsive">
									<thead>
										<tr>
											<th>#</th>
											<th>Fecha</th>
											<th>Estado</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="solicitud in ctrl.mis_solicitudes">
											<td>{{$index + 1}}</td>
											<td>{{solicitud.created_at}}</td>
											<td><span ng-class="{'label label-success': solicitud.estado.id == 4, 'label label-warning': solicitud.estado.id == 1}">{{solicitud.estado.estado}}</span></td>
											<td><input type="checkbox"></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="panel-footer text-center">
								<button class="btn btn-danger">Eliminar selección</button>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="well">
							<div class="form-group">
								<label>Archivo:</label>
								<input class="btn btn-default" type="file" id="archivo" name="archivo" uploader-model="ctrl.archivo">
							</div>
							<div class="form-group">
								<button class="btn btn-info" ng-disabled="!ctrl.archivo.length" ng-click="ctrl.subirArchivo(ctrl.archivo)" ng-hide="ctrl.procesando">Analizar archivo</button>
								<button class="btn btn-success" ng-hide="ctrl.procesando" ng-disabled="!ctrl.archivoAnalizado || !ctrl.archivoPorAprobar" ng-click="ctrl.ingresarTemporal()">Enviar archivo para aprobación</button>
								<span ng-show="ctrl.procesando"><i class="fa fa-cog fa-spin"></i> Analizando archivo...</span>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading text-center">
								Resultados
							</div>
							<div class="panel-body" style="height: 200px;word-wrap: break-word;overflow-y: auto;">
								<div class="alert alert-danger" ng-if="ctrl.error.error">
									<h4>Errores</h4>
									<ul>
										<li ng-repeat="error in ctrl.error.analizar">{{error}}</li>
										<li ng-show="ctrl.error.subir">{{ctrl.error.subir}}</li>
									</ul>
								</div>
								<div class="alert alert-warning" ng-if="ctrl.advertencias.length">
									<h4>Advertencias</h4>
									<ul>
										<li ng-repeat="advertencia in ctrl.advertencias">{{advertencia}}</li>
									</ul>
								</div>
								<div class="alert alert-success" ng-show="!ctrl.error.error && ctrl.success.analizar">{{ctrl.success.analizar}}</div>
							</div>
							<div class="panel-footer text-center">
								<button class="btn btn-warning" ng-click="ctrl.borrarHistorial()" ng-disabled="!ctrl.error.error && !ctrl.advertencias.length && !ctrl.success.analizar">Limpiar historial</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>