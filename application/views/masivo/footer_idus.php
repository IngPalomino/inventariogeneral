<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http","$scope",function($http,$scope){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";
			ref.reestablecerContrasena = {};

			ref.error = {};
			ref.success = {};
			ref.advertencias = [];
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';
			ref.auth.evaluador = (ref.session.evaluador == 1)? '' : 'disabled';

			ref.procesando = false;
			ref.archivoPorAprobar = "";
			ref.archivoAnalizado = false;

			listarSolicitudes = function(){
				ref.cargando = true;
				$http({
					url:"<?php echo base_url("ingreso_masivo/idus/listar_solicitudes");?>/"+ref.session.id_usuario,
					method:"GET"
				}).success(function(data){
					ref.cargando = false;
					if(data.length)
					{
						ref.mis_solicitudes = data;
					}
					else
					{
						
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.cargando = false;
				});
			}

			ref.subirArchivo = function(archivo){
				ref.procesando = true;
				ref.borrarHistorial();

				var fdata = new FormData();
				fdata.append('archivo', archivo[0]);
				fdata.append('tipo', 'idus');

				$.ajax({
					url: "<?php echo base_url("ingreso_masivo/idus/subir"); ?>",
					method: "POST",
					data: fdata,
					contentType: false,
					processData: false,
					dataType: "json",
					beforeSend: function(){},
					success: function(data){/*console.log(data);*/
						$scope.$apply(function(){
							if(!data.error)
							{
								ref.error.error = false;
								ref.archivoPorAprobar = data.resultado;
								var adata = "file="+data.resultado;

								$http({
									url:"<?php echo base_url("ingreso_masivo/idus/analizar");?>",
									method:"POST",
									data:adata,
									headers:{'Content-Type': 'application/x-www-form-urlencoded'}
								}).success(function(data){
									ref.procesando = false;
									if(!data.error)
									{
										ref.error.error = false;
										ref.advertencias = data.advertencias;
										ref.success.analizar = data.mensajes[0];
										ref.archivoAnalizado = true;
									}
									else
									{
										ref.error.error = true;
										ref.advertencias = data.advertencias;
										ref.error.analizar = data.mensajes;
										ref.archivoAnalizado = false;
									}
								}).error(function(err){
									/*console.log(err);*/
									ref.procesando = false;
								});
							}
							else
							{
								ref.procesando = false;
								ref.error.error = true;
								ref.error.subir = data.resultado;
							}	
						});
					}
				});
			}

			ref.ingresarTemporal = function(){
				ref.procesando = true;
				var data = "file="+ref.archivoPorAprobar;

				$http({
					url:"<?php echo base_url("ingreso_masivo/idus/enviar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					ref.procesando = false;
					if(!data.error)
					{
						ref.archivo = [];
						ref.archivoAnalizado = false;
						listarSolicitudes();
					}
					else
					{
						ref.error.error = true;
						ref.advertencias = data.advertencias;
						ref.error.analizar = data.mensajes;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			}

			ref.borrarHistorial = function(){
				ref.error = {};
				ref.success = {};
				ref.advertencias = [];
			}

			ref.iniciar_sesion = function(){
				var data = "usuario="+ref.formLogIn.usuario
							+"&contrasena="+ref.formLogIn.contrasena;

				$http({
					url:"<?php echo base_url("sesion/iniciar");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					if(data.error)
					{
						ref.error.error = true;
						ref.error.logIn = data.error;
					}
					else
					{
						if(data.logged)
						{
							location.reload();
						}
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};

			ref.olvide_contrasena = function(){
				$("#reestablecerContrasena").bPopup({
					amsl: 0,
					onClose: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
					},
					onOpen: function(){
						ref.reestablecerContrasena = {};
						ref.success = {};
						ref.success.forgot = "";
						var alertForgot = document.getElementById("alert-forgot");
						alertForgot.innerHTML = "";
						ref.error = {};
					}
				});
			};

			ref.reestablecer_contrasena = function(){
				ref.procesando = true;
				var alertForgot = document.getElementById("alert-forgot");
				alertForgot.innerHTML = "";
				ref.error = {};

				var data = "correo="+encodeURIComponent(ref.reestablecerContrasena.correo);

				$http({
					url:"<?php echo base_url("recuperar_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.error.error = true;
						ref.error.forgot = resp.error;
						alertForgot.innerHTML = ref.error.forgot;
						ref.procesando = false;
					}
					else
					{
						ref.reestablecerContrasena = {};
						setTimeout(ref.cancelar_contrasena,"3000");
						ref.success.forgot = "Se le enviará un correo con un enlace para reestablecer su contraseña.";
						alertForgot.innerHTML = "";
						ref.error = {};
						ref.procesando = false;
					}
				}).error(function(err){
					/*console.log(err);*/
					ref.procesando = false;
				});
			};

			ref.cancelar_contrasena = function(){
				$("#reestablecerContrasena").bPopup().close();
				ref.reestablecerContrasena = {};
			};

			listarSolicitudes();
		}])
		.directive('uploaderModel', ['$parse', function($parse){
			return {
				restrict: 'A',
				link: function (scope, iElement, iAttrs) {
					iElement.on('change', function(e) {
						scope.$apply(function(){
							$parse(iAttrs.uploaderModel).assign(scope, iElement[0].files);
						});
					});
				}
			};
		}]);
</script>
</body>
</html>