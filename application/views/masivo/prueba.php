<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>Antenas GUL</h2>
			</div>
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url("ingreso_masivo"); ?>">Ingreso Masivo</a></li>
				<li class="active">Antenas GUL</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel with-nav-tabs panel-primary">
				<div class="panel-heading text-center">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#subir" data-toggle="tab"><strong>Subir información nueva</strong></a></li>
						<li><a href="#modificar" data-toggle="tab"><strong>Modificar información existente</strong></a></li>
					</ul>
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div id="subir" class="tab-pane fade in active">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="well">
									<div class="form-group">
										<label>Archivo:</label>
										<input class="btn btn-default" type="file" id="archivo" name="archivo" uploader-model="ctrl.archivo">
									</div>
									<div class="form-group">
										<button class="btn btn-info" ng-disabled="!ctrl.archivo.length" ng-click="ctrl.subirArchivo(ctrl.archivo)" ng-hide="ctrl.procesando">Analizar archivo</button>
										<button class="btn btn-success" ng-hide="ctrl.procesando" ng-disabled="!ctrl.archivoPorAprobar">Enviar archivo para aprobación</button>
										<span ng-show="ctrl.procesando"><i class="fa fa-cog fa-spin"></i> Analizando archivo...</span>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading text-center">
										<label>Resultados</label>
									</div>
									<div class="panel-body" style="height: 200px;word-wrap: break-word;overflow-y: scroll;">
										
									</div>
									<div class="panel-footer text-center">
										<button class="btn btn-warning">Limpiar historial</button>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="well">
									<label>Plantilla de ingreso:</label>
									<a href="<?php echo base_url("ingreso_masivo/plantillas/antenas_gul");?>">plantilla_antenas_gul.xlsx</a>
									<label>Marcas y modelos disponibles:</label>
									<a href="<?php echo base_url("ingreso_masivo/plantillas/tipo_antenas_gul");?>">plantilla_tipo_antenas_gul.xlsx</a>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading text-center">
										<label>Mis solicitudes</label>
									</div>
									<div class="panel-body" style="height: 276px;word-wrap: break-word;overflow-y: scroll;">
										<table class="table table-condensed table-responsive">
											<thead>
												<tr>
													<th>Número</th>
													<th>Fecha</th>
													<th>Estado</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>111</td>
													<td>15/06/2017 - 18:00:00</td>
													<td><span class="label label-success">Finalizado</span></td>
													<td><input type="checkbox"></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="panel-footer text-center">
										<button class="btn btn-danger">Eliminar selección</button>
									</div>
								</div>
							</div>
						</div>
						<div id="modificar" class="tab-pane fade">
							<div class="well">
								<label>En desarrollo...</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>