<div class="container ng-cloak">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>Bancos de Baterías</h2>
			</div>
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url("ingreso_masivo"); ?>">Ingreso Masivo</a></li>
				<li class="active">Bancos de Baterías</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Bancos de Baterías
				</div>
				<div class="panel-body">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="well">
							<div class="text-center">
								<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#instrucciones"><i class="fa fa-exclamation-triangle"></i> INSTRUCCIONES</button><br><br>
							</div>
							<label>Plantilla de ingreso nuevo:</label>
							<a href="<?php echo base_url("ingreso_masivo/plantillas/bancos_baterias");?>">plantilla_bancos_baterias.xlsx</a><br>
							<label>Plantilla de actualización</label>
							<a href="<?php echo base_url("ingreso_masivo/plantillas/bancos_baterias_actualizacion");?>">plantilla_bancos_baterias_actualizacion</a><br>
							<label>Marcas y modelos disponibles:</label>
							<a href="<?php echo base_url("ingreso_masivo/plantillas/tipo_bancos_baterias");?>">plantilla_tipo_bancos_baterias.xlsx</a>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading text-center">
								Mis solicitudes
							</div>
							<div class="panel-body" style="height: 276px;word-wrap: break-word;overflow-y: auto;">
								<table class="table table-condensed table-responsive">
									<thead>
										<tr>
											<th>#</th>
											<th>Fecha</th>
											<th>Estado</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="solicitud in ctrl.mis_solicitudes">
											<td>{{$index + 1}}</td>
											<td>{{solicitud.created_at}}</td>
											<td><span ng-class="{'label label-success': solicitud.estado.id == 4, 'label label-warning': solicitud.estado.id == 1}">{{solicitud.estado.estado}}</span></td>
											<td><input type="checkbox"></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="panel-footer text-center">
								<button class="btn btn-danger">Eliminar selección</button>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="well">
							<div class="form-group">
								<label>Archivo:</label>
								<input class="btn btn-default" type="file" id="archivo" name="archivo" uploader-model="ctrl.archivo">
							</div>
							<div class="form-group">
								<button class="btn btn-info" ng-disabled="!ctrl.archivo.length" ng-click="ctrl.subirArchivo(ctrl.archivo)" ng-hide="ctrl.procesando">Analizar archivo</button>
								<button class="btn btn-success" ng-hide="ctrl.procesando" ng-disabled="!ctrl.archivoAnalizado || !ctrl.archivoPorAprobar" ng-click="ctrl.ingresarTemporal()">Enviar archivo para aprobación</button>
								<span ng-show="ctrl.procesando"><i class="fa fa-cog fa-spin"></i> Analizando archivo...</span>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading text-center">
								Resultados
							</div>
							<div class="panel-body" style="height: 280px;word-wrap: break-word;overflow-y: auto;">
								<div class="alert alert-danger" ng-if="ctrl.error.error">
									<h4>Errores</h4>
									<ul>
										<li ng-repeat="error in ctrl.error.analizar">{{error}}</li>
										<li ng-show="ctrl.error.subir">{{ctrl.error.subir}}</li>
									</ul>
								</div>
								<div class="alert alert-warning" ng-if="ctrl.advertencias.length">
									<h4>Advertencias</h4>
									<ul>
										<li ng-repeat="advertencia in ctrl.advertencias">{{advertencia}}</li>
									</ul>
								</div>
								<div class="alert alert-success" ng-show="!ctrl.error.error && ctrl.success.analizar">{{ctrl.success.analizar}}</div>
							</div>
							<div class="panel-footer text-center">
								<button class="btn btn-warning" ng-click="ctrl.borrarHistorial()" ng-disabled="!ctrl.error.error && !ctrl.advertencias.length && !ctrl.success.analizar">Limpiar historial</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="instrucciones" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-exclamation-triangle"></i> Instrucciones</h4>
			</div>
			<div class="modal-body">
				<p>La <b>Plantilla de ingreso nuevo</b> se utiliza únicamente para ingresar nueva información en la base de datos.</p>
				<p>
					La <b>Plantilla de actualización</b> se utiliza únicamente para actualizar información existente.
					Esta plantilla tiene la columna llamada <b>ID del Banco</b> la cual no se debe borrar para poder actualizar el elemento.
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Entendido</button>
			</div>
		</div>
	</div>
</div>