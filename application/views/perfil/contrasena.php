<section class="container-fluid panel-menu">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Cambio de contraseña
				</div>
				<div class="panel-body">
					<form>
						<table class="table">
							<tbody>
								<tr>
									<td class="info">Contraseña actual</td>
									<td><input type="password" ng-model="ctrl.submit.contrasena_actual" class="form-control" /></td>
								</tr>
								<tr>
									<td class="info">Nueva contraseña</td>
									<td><input type="password" ng-model="ctrl.submit.nueva_contrasena" class="form-control" /></td>
								</tr>
								<tr>
									<td class="info">Confirmar contraseña</td>
									<td><input type="password" ng-model="ctrl.submit.confirmar_contrasena" class="form-control" /></td>
								</tr>
							</tbody>
						</table>
						<button class="btn btn-success" ng-click="ctrl.cambiar_contrasena(ctrl.submit)">Cambiar contraseña</button>
					</form>
				</div>
				<div class="panel-footer">
					<div class="row">
						<div class="col-xs-6">
							<p>La contraseña debe tener los siguientes requisitos:</p>
							<ul>
								<li>Debe tener un mínimo de 8 dígitos.</li>
								<li>Debe contener letras mayúsculas y minúsculas.</li>
								<li>Debe contener símbolos como @,.,_,-,+</li>
								<li>Debe contener números.</li>
							</ul>
						</div>
						<div class="col-xs-6">
							<p class="alert ng-cloak" role="alert" ng-hide="ctrl.cambio_contrasena.error.length < 1" ng-class="ctrl.cambio_contrasena.clase">
								{{ctrl.cambio_contrasena.error}}
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>