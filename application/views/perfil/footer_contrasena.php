<script type="text/javascript">
	angular
		.module('app',[])
		.controller('controlador', ["$http",function($http){
			var ref = this;
			ref.formLogIn = {};
			ref.formLogIn.usuario = "";
			ref.formLogIn.contrasena = "";

			ref.error = {};
			ref.error.error = false;
			ref.error.logIn = "";

			ref.session = <?php echo json_encode($session);?>;
			ref.auth = {};
			ref.auth.au0 = (ref.session.auth == 0)? '' : 'disabled';
			ref.auth.au1 = (ref.session.auth <= 1)? '' : 'disabled';
			ref.auth.au2 = (ref.session.auth <= 2)? '' : 'disabled';
			ref.auth.au3 = (ref.session.auth <= 3)? '' : 'disabled';

			ref.submit = {};
			ref.cambio_contrasena = {
				error: ""
			};

			ref.cambiar_contrasena = function(submit){
				var campos = Object.keys(submit),
					data = "";

				campos.forEach(function(campo,ind,objeto){
					data += campo+"="+submit[campo];

					if(ind <= (objeto.length - 2))
					{
						data += "&";
					}
				});

				ref.cambio_contrasena = {
					error: "",
					clase: "alert-danger"
				};

				$http({
					url:"<?php echo base_url("usuarios/cambio_contrasena");?>",
					method:"POST",
					data:data,
					headers:{'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(resp){
					if(resp.error)
					{
						ref.cambio_contrasena.clase = "alert-danger";
						ref.cambio_contrasena.error = resp.mensaje.replace(/(<\w+>)|(<\/\w+>)/g,"");
					}
					else
					{
						ref.cambio_contrasena.clase = "alert-success";
						ref.cambio_contrasena.error = resp.mensaje.replace(/(<\w+>)|(<\/\w+>)/g,"");
					}
				}).error(function(err){

				});
			}

			ref.cerrar_sesion = function(){
				$http({
					url:"<?php echo base_url("index.php/sesion/cerrar");?>",
					method:"GET"
				}).success(function(data){
					if(!data.logged)
					{
						window.location = "<?php echo base_url();?>";
					}
				}).error(function(err){
					/*console.log(err);*/
				});
			};
		}]);
</script>
</body>
</html>