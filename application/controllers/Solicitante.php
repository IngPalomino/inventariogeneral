<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitante extends CI_Controller
{
	public function __construct()
	{  // private $modulo;
		parent::__construct();
		$this->load->library('auth');		
		$this->load->model('Solicitante_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->load->library('session');
		$this->modulo = 5;
		
	}


	public function listar_mw()
	{
		if ($this->auth->logged_in())
		{
			//if ($this->auth->is_admin())
			//{
            $session = $this->session->userdata();
			$id_usuario = $session["id_usuario"];
			echo json_encode($this->Solicitante_model->get_solicitudes_mw_listar_all_usuario($id_usuario));
			
			//}
			//else
			//{
			 //echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			//}
		}
		else
		{
		  echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
		
	}




	public function listar_gul()
	{
		if ($this->auth->logged_in())
		{
			// if ($this->auth->is_admin())
			// {
            $session = $this->session->userdata();
			$id_usuario = $session["id_usuario"];
		    $listado = 	$this->Solicitante_model->get_solicitudes_usuario($id_usuario);	
            echo json_encode($listado);
            // }
			// else
			// {
            // echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			  // }
		}
		else
		{
		  echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
		
	}



}