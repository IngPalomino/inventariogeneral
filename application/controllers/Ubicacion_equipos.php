<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ubicacion_equipos extends CI_Controller {

	public function listar()
	{
		$this->load->model('Ubicacion_equipos_model');

		$listado = $this->Ubicacion_equipos_model->get_ubicaciones();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Ubicacion_equipos_model');

		$ubicacion = $this->Ubicacion_equipos_model->edit($id);

		echo json_encode($ubicacion);
	}

	public function actualizar($id)
	{
		$this->load->model('Ubicacion_equipos_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['ubicacion_equipos'] = $this->input->input_stream('ubicacion_equipos');

		$this->Ubicacion_equipos_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Ubicacion_equipos_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['ubicacion_equipos'] = $this->input->post('ubicacion_equipos');

		$this->Ubicacion_equipos_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Ubicacion_equipos_model');

		$this->Ubicacion_equipos_model->delete($id);
	}
}