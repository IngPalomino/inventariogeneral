<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitios extends CI_Controller {

	public function listar()
	{
		$this->load->model('Sitios_model');

		$listado = $this->Sitios_model->get_sites();

		echo json_encode($listado);
	}

	public function buscar()
	{
		$this->load->helper('form');
		$this->load->model('Sitios_model');

		$cadenas = $this->input->post('cadenas');
		$cadenas = explode(",", $cadenas);
		$listado = $this->Sitios_model->look_for_sites($cadenas);

		echo json_encode($listado);
	}	

	public function editar($id)
	{
		$this->load->model('Sitios_model');

		$sitio = $this->Sitios_model->edit($id);

		echo json_encode($sitio);
	}

	public function actualizar($id)
	{
		$this->load->model('Sitios_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$config = array(
				array(
						'field' => 'codigo',
						'label' => 'código',
						'rules' => 'required|callback_confirmar_codigo',
						'errors' => array(
								'required' => 'Debe ingresar un %s.'
							)
					),
				array(
						'field' => 'nombre',
						'label' => 'nombre',
						'rules' => 'required|callback_confirmar_nombre',
						'errors' => array(
								'required' => 'Debe ingresar el %s del sitio.'
							)
					),
				array(
						'field' => 'departamento',
						'label' => 'departamento',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir el %s.'
							)
					),
				array(
						'field' => 'provincia',
						'label' => 'provincia',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir la %s.'
							)
					),
				array(
						'field' => 'distrito',
						'label' => 'distrito',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir el %s.'
							)
					)
			);

		$error = array(
			'error' => FALSE);

		$data = $this->input->input_stream();

		$this->form_validation->set_data($data);
		$this->form_validation->set_rules($config);

		if($this->form_validation->run() === FALSE)
		{
			$error['error'] = validation_errors();
			echo json_encode($error);
		}
		else
		{
			$datos_zona['departamento'] = $this->input->input_stream('departamento');
			$datos_zona['provincia'] = $this->input->input_stream('provincia');
			$datos_zona['distrito'] = $this->input->input_stream('distrito');
			$this->load->model('Zona_peru_model');
			$zona = $this->Zona_peru_model->get_zona($datos_zona);

			$info = array();
			$info['codigo'] = $this->input->input_stream('codigo');
			$info['nombre'] = $this->input->input_stream('nombre');
			$info['nombre_completo'] = $info['codigo'] . '_' . $info['nombre'];
			$info['zona_peru'] = $zona['0']['id'];
			$info['direccion'] = $this->input->input_stream('direccion');
			$info['longitud'] = floatval($this->input->input_stream('longitud'));
			$info['latitud'] = floatval($this->input->input_stream('latitud'));
			$info['in_building'] = ($this->input->input_stream('in_building') === "true" ? true : false);
			$info['coubicado'] = ($this->input->input_stream('coubicado') === "true" ? true : false);
			$info['agregador'] = ($this->input->input_stream('agregador') === "true" ? true : false);
			$info['dorsal'] = ($this->input->input_stream('dorsal') === "true" ? true : false);
			$info['tipo_torre'] = $this->input->input_stream('tipo_torre');
			$info['ubicacion_equipos'] = $this->input->input_stream('ubicacion_equipos');
			$info['anio_construccion'] = intval($this->input->input_stream('anio_construccion'));
			$info['greenfield_rooftop'] = ($this->input->input_stream('greenfield_rooftop') === "true" ? true : false);
			$info['altura_edificacion'] = floatval($this->input->input_stream('altura_edificacion'));
			$info['altura_torre'] = floatval($this->input->input_stream('altura_torre'));
			$info['camuflaje'] = $this->input->input_stream('camuflaje');

			/*if(*/$this->Sitios_model->update($id, $info);/*)
			{

			}*/
		}
	}

	public function crear()
	{
		$this->load->model('Sitios_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$config = array(
				array(
						'field' => 'codigo',
						'label' => 'código',
						'rules' => 'required|callback_confirmar_codigo',
						'errors' => array(
								'required' => 'Debe ingresar un %s.'
							)
					),
				array(
						'field' => 'nombre',
						'label' => 'nombre',
						'rules' => 'required|callback_confirmar_nombre',
						'errors' => array(
								'required' => 'Debe ingresar el %s del sitio.'
							)
					),
				array(
						'field' => 'departamento',
						'label' => 'departamento',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir el %s.'
							)
					),
				array(
						'field' => 'provincia',
						'label' => 'provincia',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir la %s.'
							)
					),
				array(
						'field' => 'distrito',
						'label' => 'distrito',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir el %s.'
							)
					)
			);

		$this->form_validation->set_rules($config);

		$error = array(
			'error' => FALSE);

		if($this->form_validation->run() === FALSE)
		{
			$error['error'] = validation_errors();
			echo json_encode($error);
		}
		else
		{
			$datos_zona['departamento'] = $this->input->post('departamento');
			$datos_zona['provincia'] = $this->input->post('provincia');
			$datos_zona['distrito'] = $this->input->post('distrito');
			$this->load->model('Zona_peru_model');
			$zona = $this->Zona_peru_model->get_zona($datos_zona);

			$info = array();
			$info['codigo'] = $this->input->post('codigo');
			$info['nombre'] = $this->input->post('nombre');
			$info['nombre_completo'] = $info['codigo'] . '_' . $info['nombre'];
			$info['zona_peru'] = $zona['0']['id'];
			$info['direccion'] = $this->input->post('direccion');
			$info['longitud'] = floatval($this->input->post('longitud'));
			$info['latitud'] = floatval($this->input->post('latitud'));
			$info['in_building'] = ($this->input->post('in_building') === "true" ? true : false);
			$info['coubicado'] = ($this->input->post('coubicado') === "true" ? true : false);
			$info['agregador'] = ($this->input->post('agregador') === "true" ? true : false);
			$info['dorsal'] = ($this->input->post('dorsal') === "true" ? true : false);
			$info['tipo_torre'] = $this->input->post('tipo_torre');
			$info['ubicacion_equipos'] = $this->input->post('ubicacion_equipos');
			$info['anio_construccion'] = intval($this->input->post('anio_construccion'));
			$info['greenfield_rooftop'] = ($this->input->post('greenfield_rooftop') === "true" ? true : false);
			$info['altura_edificacion'] = floatval($this->input->post('altura_edificacion'));
			$info['altura_torre'] = floatval($this->input->post('altura_torre'));
			$info['camuflaje'] = $this->input->post('camuflaje');

			if($this->Sitios_model->insert($info))
			{
				$this->load->library('session');
				$session = $this->session->userdata();
				$this->load->model('Logs_model');
				$this->Sitios_model->ingreso_de_datos($session['usuario'],$info,"tb_sitios");
			}
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Sitios_model');

		$this->Sitios_model->delete($id);
	}

	/************************************/
	/* FUNCIONES DE VALIDACIÓN DE DATOS */
	/************************************/

	function confirmar_codigo($codigo)
	{
		if(strlen($codigo) === 7)
		{
			if (!(preg_match("/(?>^\d{7}+$)/",$codigo)))
			{
				$this->form_validation->set_message('confirmar_codigo', 'Los 7 dígitos del código deben numéricos.');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		else
		{
			$this->form_validation->set_message('confirmar_codigo', 'El código debe ser de 7 dígitos.');
			return FALSE;
		}
	}

	function confirmar_nombre($nombre)
	{
		if(!(preg_match("/(?>^\S+$)/",$nombre)))
		{
			$this->form_validation->set_message('confirmar_nombre','El nombre no debe contener espacios en blanco.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}