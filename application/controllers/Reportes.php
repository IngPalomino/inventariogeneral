<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {

	private $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Reportes_model');
		$this->load->library('excel');
		$this->data = $this->input->post();

		foreach ($this->data as $key => $value)
		{
			if ( ! isset($value) || empty($value))
			{
				unset($this->data[$key]);
			}
		}
	}

	public function reportes_predeterminados_pop()
	{
		$listado = $this->Reportes_model->get_pop($this->data);

		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Reporte POP/Status Site')
					->setSubject('Reporte')
					->setDescription('Reporte predeterminado desde el Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Status Site');

		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Estado')
					->setCellValue('D1', 'Prioridad')
					->setCellValue('E1', 'Departamento')
					->setCellValue('F1', 'Provincia')
					->setCellValue('G1', 'Distrito')
					->setCellValue('H1', 'Dirección')
					->setCellValue('I1', 'Latitud (°)')
					->setCellValue('J1', 'Longitud (°)')
					->setCellValue('K1', 'Tipo mimetizado')
					->setCellValue('L1', 'Coubicado en')
					->setCellValue('M1', 'Nombre sitio coubicador')
					->setCellValue('N1', 'Operador coubicado')
					->setCellValue('O1', 'Nombre sitio coubicante')
					->setCellValue('P1', 'In-building')
					->setCellValue('Q1', 'Dorsal')
					->setCellValue('R1', 'Agregador')
					->setCellValue('S1', 'Fecha On Air')
					->setCellValue('T1', 'Año de construcción')
					->setCellValue('U1', 'Proyecto')
					->setCellValue('V1', 'Contrata constructora')
					->setCellValue('W1', 'Tipo de site para facturación')
					->setCellValue('X1', 'Proveedor de mantenimiento')
					->setCellValue('Y1', 'Consideraciones de acceso')
					->setCellValue('Z1', 'Acceso libre 24h')
					->setCellValue('AA1', 'Nivel de riesgo')
					->setCellValue('AB1', 'Código Wimax 2.5')
					->setCellValue('AC1', 'Código Wimax 3.5')
					->setCellValue('AD1', 'Código iDEN')
					->setCellValue('AE1', 'Código Agregador')
					->setCellValue('AF1', 'Código Repetidor')
					->setCellValue('AG1', 'Código GUL')
					->setCellValue('AH1', 'Código LTE 700')					
					->setCellValue('AI1', 'Fecha Wimax 2.5')
					->setCellValue('AJ1', 'Fecha Wimax 3.5')
					->setCellValue('AK1', 'Fecha iDEN')
					->setCellValue('AL1', 'Fecha Agregador')
					->setCellValue('AM1', 'Fecha Repetidor')
					->setCellValue('AN1', 'Fecha GUL')
					->setCellValue('AO1', 'Fecha LTE 700')
					->setCellValue('AP1', 'Servicios Wimax 2.5')
					->setCellValue('AQ1', 'Servicios Wimax 3.5')
					->setCellValue('AR1', 'Servicios iDEN')
					->setCellValue('AS1', 'Servicios Agregador')
					->setCellValue('AT1', 'Servicios Repetidor')
					->setCellValue('AU1', 'Servicios GUL')
					->setCellValue('AV1', 'Servicios LTE 700')
					->setCellValue('AW1', 'CRM')
					->setCellValue('AX1', 'Supervisor')
					->setCellValue('AY1', 'Responsable(s)')
					->setCellValue('AZ1', 'Frecuencia de mantenimiento');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$activeSheet->getStyle('A1:AZ1')
					->applyFromArray($styleArray);

		$i = 2;
		foreach ($listado as $fila)
		{
			$tipo_estacion = ($fila['greenfield_rooftop'])? 'Rooftop' : 'Greenfield';
			$altura_edificacion = ($fila['greenfield_rooftop'])? $fila['altura_edificacion'] : 'N/A';
			$mimetizado = ($fila['camuflaje']['id'] == 1)? 'NO' : 'SI';
			$tipo_mimetizado = ($fila['camuflaje']['id'] == 1)? 'N/A' : $fila['camuflaje']['camuflaje'] ;
			$in_building = ($fila['in_building'])? 'SI' : 'NO';
			$coubicado = ($fila['coubicador']['id'] == 1 )? 'NO' : 'SI';
			$dorsal = ($fila['dorsal'])? 'SI' : 'NO';
			$agregador = ($fila['agregador'])? 'SI' : 'NO';
			$estado = ($fila['estado'] == 2)? 'ON Air' : 'OFF Air';
			$acceso_libre_24h = ($fila['acceso_libre_24h'])? 'SI' : 'NO';
            // $codigo_bafi = $fila['acceso_libre_24h'];


			$fechaWimax25 = ( isset($fila['wimax25']['fecha_servicio']) )? date('d/m/Y', strtotime($fila['wimax25']['fecha_servicio'])) : '';
			$fechaWimax35 = ( isset($fila['wimax35']['fecha_servicio']) )? date('d/m/Y', strtotime($fila['wimax35']['fecha_servicio'])) : '';
			$fechaIDEN = ( isset($fila['iDEN']['fecha_servicio']) )? date('d/m/Y', strtotime($fila['iDEN']['fecha_servicio'])) : '';
			$fechaAgregador = ( isset($fila['Agregador']['fecha_servicio']) )? date('d/m/Y', strtotime($fila['Agregador']['fecha_servicio'])) : '';
			$fechaRepetidor = ( isset($fila['Repetidor']['fecha_servicio']) )? date('d/m/Y', strtotime($fila['Repetidor']['fecha_servicio'])) : '';
			$fechaGUL = ( isset($fila['GUL']['fecha_servicio']) )? date('d/m/Y', strtotime($fila['GUL']['fecha_servicio'])) : '';
			$fechaLTE_700 = ( isset($fila['LTE_700']['fecha_servicio']) )? date('d/m/Y', strtotime($fila['LTE_700']['fecha_servicio'])) : '';
			
			$activeSheet->setCellValue('A'.$i, $fila['codigo'])
						->setCellValue('B'.$i, $fila['nombre_completo'])
						->setCellValue('C'.$i, $estado)
						->setCellValue('D'.$i, "P".$fila['prioridad'])
						->setCellValue('E'.$i, $fila['zona_peru']['departamento'])
						->setCellValue('F'.$i, $fila['zona_peru']['provincia'])
						->setCellValue('G'.$i, $fila['zona_peru']['distrito'])
						->setCellValue('H'.$i, $fila['direccion'])
						->setCellValue('I'.$i, $fila['latitud'])
						->setCellValue('J'.$i, $fila['longitud'])
						->setCellValue('K'.$i, $tipo_mimetizado)
						->setCellValue('L'.$i, $fila['coubicador']['coubicador'])
						->setCellValue('M'.$i, $fila['nombre_sitio_coubicador'])
						->setCellValue('N'.$i, $fila['operador_coubicante']['coubicante'])
						->setCellValue('O'.$i, $fila['nombre_sitio_coubicante'])
						->setCellValue('P'.$i, $in_building)
						->setCellValue('Q'.$i, $dorsal)
						->setCellValue('R'.$i, $agregador)
						->setCellValue('S'.$i, date('d/m/Y', strtotime($fila['fecha_estado_on_air'])))
						->setCellValue('T'.$i, $fila['anio_construccion'])
						->setCellValue('U'.$i, $fila['proyecto']['proyecto'])
						->setCellValue('V'.$i, $fila['contrata_constructora']['contrata_constructora'])
						->setCellValue('W'.$i, $fila['tipo_solucion']['tipo_solucion'])
						->setCellValue('X'.$i, $fila['proveedor_mantenimiento']['proveedor_mantenimiento'])
						->setCellValue('Y'.$i, $fila['consideracion_acceso'])
						->setCellValue('Z'.$i, $acceso_libre_24h)
						->setCellValue('AA'.$i, $fila['nivel_riesgo'])
						->setCellValue('AB'.$i, $fila['wimax25']['codigo'])
						->setCellValue('AC'.$i, $fila['wimax35']['codigo'])
						->setCellValue('AD'.$i, $fila['iDEN']['codigo'])
						->setCellValue('AE'.$i, $fila['Agregador']['codigo'])
						->setCellValue('AF'.$i, $fila['Repetidor']['codigo'])
						->setCellValue('AG'.$i, $fila['GUL']['codigo'])
						->setCellValue('AH'.$i, $fila['LTE_700']['codigo'])
						->setCellValue('AI'.$i, $fechaWimax25)
						->setCellValue('AJ'.$i, $fechaWimax35)
						->setCellValue('AK'.$i, $fechaIDEN)
						->setCellValue('AL'.$i, $fechaAgregador)
						->setCellValue('AM'.$i, $fechaRepetidor)
						->setCellValue('AN'.$i, $fechaGUL)
						->setCellValue('AO'.$i, $fechaLTE_700)
						->setCellValue('AP'.$i, $fila['wimax25']['servicios'])
						->setCellValue('AQ'.$i, $fila['wimax35']['servicios'])
						->setCellValue('AR'.$i, $fila['iDEN']['servicios'])
						->setCellValue('AS'.$i, $fila['Agregador']['servicios'])
						->setCellValue('AT'.$i, $fila['Repetidor']['servicios'])
						->setCellValue('AU'.$i, $fila['GUL']['servicios'])
						->setCellValue('AV'.$i, $fila['LTE_700']['servicios'])
						->setCellValue('AW'.$i, $fila['crm']['crm'])
						->setCellValue('AX'.$i, $fila['crm']['supervisor'])
						->setCellValue('AY'.$i, $fila['oym_zonales']['oym_zonales'])
						->setCellValue('AZ'.$i, $fila['frecuencia_mantenimiento']['frecuencia_mantenimiento']);

			$i++;
		}

		$activeSheet->setAutoFilter($activeSheet->calculateWorksheetDimension());
		$autoFilter = $activeSheet->getAutoFilter();
		$columnFilter = $autoFilter->getColumn('C');

		$columnFilter->setFilterType(
			PHPExcel_Worksheet_AutoFilter_Column::AUTOFILTER_FILTERTYPE_FILTER
		);
		$columnFilter->createRule()->setRule(
				PHPExcel_Worksheet_AutoFilter_Column_Rule::AUTOFILTER_COLUMN_RULE_EQUAL,
				'ON Air'
		);

		$activeSheet->getColumnDimension('A')->setAutoSize(true);
		$activeSheet->getColumnDimension('B')->setAutoSize(true);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setAutoSize(true);
		$activeSheet->getColumnDimension('E')->setAutoSize(true);
		$activeSheet->getColumnDimension('F')->setAutoSize(true);
		$activeSheet->getColumnDimension('G')->setAutoSize(true);
		$activeSheet->getColumnDimension('H')->setWidth(25);
		$activeSheet->getColumnDimension('I')->setAutoSize(true);
		$activeSheet->getColumnDimension('J')->setAutoSize(true);
		$activeSheet->getColumnDimension('K')->setAutoSize(true);
		$activeSheet->getColumnDimension('L')->setAutoSize(true);
		$activeSheet->getColumnDimension('M')->setWidth(25);
		$activeSheet->getColumnDimension('N')->setAutoSize(true);
		$activeSheet->getColumnDimension('O')->setAutoSize(true);
		$activeSheet->getColumnDimension('P')->setAutoSize(true);
		$activeSheet->getColumnDimension('Q')->setAutoSize(true);
		$activeSheet->getColumnDimension('R')->setAutoSize(true);
		$activeSheet->getColumnDimension('S')->setAutoSize(true);
		$activeSheet->getColumnDimension('T')->setAutoSize(true);
		$activeSheet->getColumnDimension('U')->setAutoSize(true);
		$activeSheet->getColumnDimension('V')->setAutoSize(true);
		$activeSheet->getColumnDimension('W')->setAutoSize(true);
		$activeSheet->getColumnDimension('X')->setAutoSize(true);
		$activeSheet->getColumnDimension('Y')->setWidth(25);
		$activeSheet->getColumnDimension('Z')->setAutoSize(true);
		$activeSheet->getColumnDimension('AA')->setAutoSize(true);
		$activeSheet->getColumnDimension('AB')->setWidth(25);
		$activeSheet->getColumnDimension('AC')->setAutoSize(true);
		$activeSheet->getColumnDimension('AD')->setAutoSize(true);
		$activeSheet->getColumnDimension('AE')->setAutoSize(true);
		$activeSheet->getColumnDimension('AF')->setAutoSize(true);
		$activeSheet->getColumnDimension('AG')->setAutoSize(true);
		$activeSheet->getColumnDimension('AH')->setAutoSize(true);
		$activeSheet->getColumnDimension('AI')->setAutoSize(true);
		$activeSheet->getColumnDimension('AJ')->setAutoSize(true);
		$activeSheet->getColumnDimension('AK')->setAutoSize(true);
		$activeSheet->getColumnDimension('AL')->setAutoSize(true);
		$activeSheet->getColumnDimension('AM')->setAutoSize(true);
		$activeSheet->getColumnDimension('AN')->setAutoSize(true);
		$activeSheet->getColumnDimension('AO')->setAutoSize(true);
		$activeSheet->getColumnDimension('AP')->setAutoSize(true);
		$activeSheet->getColumnDimension('AQ')->setAutoSize(true);
		$activeSheet->getColumnDimension('AR')->setAutoSize(true);
		$activeSheet->getColumnDimension('AS')->setAutoSize(true);
		$activeSheet->getColumnDimension('AT')->setAutoSize(true);
		$activeSheet->getColumnDimension('AU')->setAutoSize(true);
		$activeSheet->getColumnDimension('AV')->setAutoSize(true);
		$activeSheet->getColumnDimension('AW')->setAutoSize(true);
		$activeSheet->getColumnDimension('AX')->setAutoSize(true);
		$activeSheet->getColumnDimension('AY')->setAutoSize(true);
		$activeSheet->getColumnDimension('AZ')->setWidth(35);

		$this->excel->createSheet();
		
		$csheet = 1;

		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('POP');

		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Estado')
					->setCellValue('D1', 'Departamento')
					->setCellValue('E1', 'Provincia')
					->setCellValue('F1', 'Distrito')
					->setCellValue('G1', 'Dirección')
					->setCellValue('H1', 'Latitud (°)')
					->setCellValue('I1', 'Longitud (°)')
					->setCellValue('J1', 'Tipo de Torre')
					->setCellValue('K1', 'Altura de Torre (m)')
					->setCellValue('L1', 'Factor uso de Torre')
					->setCellValue('M1', 'Tipo de Estación')
					->setCellValue('N1', 'Altura de Edificación (m)')
					->setCellValue('O1', 'Tipo Mimetizado')
					->setCellValue('P1', 'Tipo de Proyecto')
					->setCellValue('Q1', 'Coubicado en')
					->setCellValue('R1', 'Operador Coubicado')
					->setCellValue('S1', 'Dorsal / Agregador')
					->setCellValue('T1', 'iDEN')
					->setCellValue('U1', 'Servicios GUL')
					->setCellValue('V1', 'Repetidores')
					->setCellValue('W1', 'Fecha On Air')
					->setCellValue('X1', 'Año de Construcción');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$activeSheet->getStyle('A1:X1')
					->applyFromArray($styleArray);
		$activeSheet->setAutoFilter('A1:X1');

		$i = 2;
		foreach ($listado as $fila)
		{
			$tipo_estacion = ($fila['greenfield_rooftop']) ? 'Rooftop' : 'Greenfield';
			$altura_edificacion = ($fila['greenfield_rooftop']) ? $fila['altura_edificacion'] : 'N/A';
			$mimetizado = ($fila['camuflaje']['id'] == 1) ? 'NO' : 'SI';
			$tipo_mimetizado = ($fila['camuflaje']['id'] == 1) ? 'N/A' : $fila['camuflaje']['camuflaje'];
			$coubicado = ($fila['coubicador']['id'] == 1 ) ? 'NO' : 'SI';
			$ds_agg = array();
			$fila['dorsal'] ? array_push($ds_agg, 'DS') : '';
			$fila['agregador'] ? array_push($ds_agg, 'AGG') : '';
			$dorsal_agregador = '';

			foreach($ds_agg as $key => $value)
			{
				$dorsal_agregador .= ($key > 0)? ' / ' : '';
				$dorsal_agregador .= $value;
			}

			$srv_gul = array();
			$fila['GUL']['codigo'] ? array_push($srv_gul, 'GUL') : '';
			$fila['in_building'] ? array_push($srv_gul, 'In-Building') : '';
			strpos($fila['nombre_completo'], '_CAP_') ? array_push($srv_gul, 'CAP') : '';
			$fila['LTE_700']['codigo'] ? array_push($srv_gul, 'Banda 700') : '';
			$servicios_gul = '';

			foreach ($srv_gul as $key => $value) 
			{
				$servicios_gul .= ($key > 0)? ' / ' : '';
				$servicios_gul .= $value;
			}

			$rep = array();
			$fila['Repetidor']['codigo']? array_push($rep, 'REP') : '';
			strpos($fila['nombre_completo'], '_MSO') ? array_push($rep, 'MSO') : '';
			($fila['nombre_completo'] == '0130607_LI_Husares_de_Junin' || $fila['nombre_completo'] == '0130903_AQ_El_Palomar') ? array_push($rep, 'RSO') : '';
			$repetidores = '';

			foreach ($rep as $key => $value) 
			{
				$repetidores .= ($key > 0) ? ' / ' : '';
				$repetidores .= $value;
			}

			$iden = $fila['iDEN']['codigo'] ? 'SI' : 'NO';
			$estado = ($fila['estado'] == 2) ? 'ON Air' : 'OFF Air';
			$acceso_libre_24h = ($fila['acceso_libre_24h']) ? 'SI' : 'NO';
			$fecha_estado_on_air = $fila['fecha_estado_on_air'] ? date('d/m/Y', strtotime($fila['fecha_estado_on_air'])) : '';
			
			$activeSheet->setCellValue('A'.$i, $fila['codigo'])
						->setCellValue('B'.$i, $fila['nombre_completo'])
						->setCellValue('C'.$i, $estado)
						->setCellValue('D'.$i, $fila['zona_peru']['departamento'])
						->setCellValue('E'.$i, $fila['zona_peru']['provincia'])
						->setCellValue('F'.$i, $fila['zona_peru']['distrito'])
						->setCellValue('G'.$i, $fila['direccion'])
						->setCellValue('H'.$i, $fila['latitud'])
						->setCellValue('I'.$i, $fila['longitud'])
						->setCellValue('J'.$i, $fila['tipo_torre']['tipo_torre'])
						->setCellValue('K'.$i, $fila['altura_torre'])
						->setCellValue('L'.$i, $fila['factor_uso_torre'])
						->setCellValue('M'.$i, $tipo_estacion)
						->setCellValue('N'.$i, $altura_edificacion)
						->setCellValue('O'.$i, $tipo_mimetizado)
						->setCellValue('P'.$i, $fila['tipo_proyecto']['tipo_proyecto'])
						->setCellValue('Q'.$i, $fila['coubicador']['coubicador'])
						->setCellValue('R'.$i, $fila['operador_coubicante']['coubicante'])
						->setCellValue('S'.$i, $dorsal_agregador)
						->setCellValue('T'.$i, $iden)
						->setCellValue('U'.$i, $servicios_gul)
						->setCellValue('V'.$i, $repetidores)
						->setCellValue('W'.$i, $fecha_estado_on_air)
						->setCellValue('X'.$i, $fila['anio_construccion']);

			$i++;
		}

		$activeSheet->getColumnDimension('A')->setAutoSize(true);
		$activeSheet->getColumnDimension('B')->setAutoSize(true);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setAutoSize(true);
		$activeSheet->getColumnDimension('E')->setAutoSize(true);
		$activeSheet->getColumnDimension('F')->setAutoSize(true);
		$activeSheet->getColumnDimension('G')->setWidth(25);
		$activeSheet->getColumnDimension('H')->setAutoSize(true);
		$activeSheet->getColumnDimension('I')->setAutoSize(true);
		$activeSheet->getColumnDimension('J')->setAutoSize(true);
		$activeSheet->getColumnDimension('K')->setAutoSize(true);
		$activeSheet->getColumnDimension('L')->setAutoSize(true);
		$activeSheet->getColumnDimension('M')->setAutoSize(true);
		$activeSheet->getColumnDimension('N')->setAutoSize(true);
		$activeSheet->getColumnDimension('O')->setAutoSize(true);
		$activeSheet->getColumnDimension('P')->setAutoSize(true);
		$activeSheet->getColumnDimension('Q')->setAutoSize(true);
		$activeSheet->getColumnDimension('R')->setAutoSize(true);
		$activeSheet->getColumnDimension('S')->setAutoSize(true);
		$activeSheet->getColumnDimension('T')->setAutoSize(true);
		$activeSheet->getColumnDimension('U')->setAutoSize(true);
		$activeSheet->getColumnDimension('V')->setAutoSize(true);
		$activeSheet->getColumnDimension('W')->setAutoSize(true);
		$activeSheet->getColumnDimension('X')->setAutoSize(true);
		
		$this->excel->createSheet();
		
		$csheet = 2;

		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Contratos');
		
		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'ID Contrato')
					->setCellValue('D1', 'Departamento')
					->setCellValue('E1', 'Provincia')
					->setCellValue('F1', 'Distrito')
					->setCellValue('G1', 'Dirección')
					->setCellValue('H1', 'Área de piso (m2)')
					->setCellValue('I1', 'Fecha inicio contrato')
					->setCellValue('J1', 'Fecha fin contrato')
					->setCellValue('K1', 'Arrendador')
					->setCellValue('L1', 'Contactos')
					->setCellValue('M1', 'Teléfonos')
					->setCellValue('N1', 'Correos');
		
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$activeSheet->getStyle("A1:N1")
					->applyFromArray($styleArray);
		$activeSheet->setAutoFilter('A1:N1');
										
		$i = 2;
		foreach ($listado as $fila)
		{
			$contacto_nombre = '';
			$telefono = '';
			$correo = '';
			
			foreach($fila['contratos'] as $key => $contrato)
			{
				foreach($contrato['contactos_contrato'] as $key_contacto => $contacto)
				{
					$contacto_nombre .= ($key_contacto > 0) ? ', ' : '';
					$contacto_nombre .= $contacto['contacto'];
					
					$telefonos = json_decode($contacto['telefono'], TRUE);

					$telefono .= ($key_contacto > 0)? ', ' : '';

					foreach ($telefonos as $j => $t)
					{
						$telefono .= $t['telefono'];
						if($j > 0) { $telefono .= ' / '; }
					}

					$correos = json_decode($contacto['correo'], TRUE);

					if (count($correos) > 0)
					{
						foreach ($correos as $j => $c)
						{
							$correo .= ($j > 0 && $c['correo']) ? ' / ' : '';
							$correo .= $c['correo'];
						}
					}
				}
				
				$activeSheet->setCellValue('A'.$i, $fila['codigo'])
							->setCellValue('B'.$i, $fila['nombre_completo'])
							->setCellValue('C'.$i, $contrato['id_contrato'])
							->setCellValue('D'.$i, $fila['zona_peru']['departamento'])
							->setCellValue('E'.$i, $fila['zona_peru']['provincia'])
							->setCellValue('F'.$i, $fila['zona_peru']['distrito'])
							->setCellValue('G'.$i, $fila['direccion'])
							->setCellValue('H'.$i, $contrato['area'])
							->setCellValue('I'.$i, date("d/m/Y", strtotime($contrato['fecha_inicio'])))
							->setCellValue('J'.$i, date("d/m/Y", strtotime($contrato['fecha_fin'])))
							->setCellValue('K'.$i, $contrato['arrendador'])
							->setCellValue('L'.$i, $contacto_nombre)
							->setCellValue('M'.$i, $telefono)
							->setCellValue('N'.$i, $correo);
				
				$i++;
			}
		}
		
		$activeSheet->getColumnDimension('A')->setAutoSize(true);
		$activeSheet->getColumnDimension('B')->setAutoSize(true);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setAutoSize(true);
		$activeSheet->getColumnDimension('E')->setAutoSize(true);
		$activeSheet->getColumnDimension('F')->setAutoSize(true);
		$activeSheet->getColumnDimension('G')->setWidth(25);
		$activeSheet->getColumnDimension('H')->setAutoSize(true);
		$activeSheet->getColumnDimension('I')->setAutoSize(true);
		$activeSheet->getColumnDimension('J')->setAutoSize(true);
		$activeSheet->getColumnDimension('K')->setAutoSize(true);
		$activeSheet->getColumnDimension('L')->setWidth(25);
		$activeSheet->getColumnDimension('M')->setWidth(25);
		$activeSheet->getColumnDimension('N')->setWidth(25);

		$this->excel->setActiveSheetIndex(0);
		
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Status_Site/POP_'.date("d-m-Y-h-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function reportes_predeterminados_antenas_gul()
	{
		$listado = $this->Reportes_model->get_antenas_gul($this->data);

		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Reporte Antenas GUL')
					->setSubject('Reporte')
					->setDescription('Reporte predeterminado desde el Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Antenas GUL');

		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Prioridad')
					->setCellValue('D1', 'Departamento')
					->setCellValue('E1', 'Provincia')
					->setCellValue('F1', 'Distrito')
					->setCellValue('G1', 'Marca')
					->setCellValue('H1', 'Modelo')
					->setCellValue('I1', 'Sector')
					->setCellValue('J1', 'Altura (m)')
					->setCellValue('K1', 'Azimuth (°)')
					->setCellValue('L1', 'Tilt mecánico (°)')
					->setCellValue('M1', 'Tilt eléctrico (°)')
					->setCellValue('N1', 'Número de serie');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$activeSheet->getStyle("A1:N1")
					->applyFromArray($styleArray);

		$i = 2;
		foreach ($listado as $fila)
		{
			$fila['info_telnet'] = json_decode($fila['info_telnet'], TRUE);
			
			$sectores = '';
			$azimuths = '';
			$tilt_mecanicos = '';
			$tilt_electricos = '';
			
			if(count($fila['info_telnet']) > 0)
			{
				foreach($fila['info_telnet'] as $key => $telnet)
				{
					$sectores .= ($key > 0 && $telnet['sector']) ? ', ' : '';
					$sectores .= $telnet["sector"];
					
					$azimuths .= ($key > 0 && $telnet['azimuth']) ? ', ' : '';
					$azimuths .= $telnet["azimuth"];
					
					$tilt_mecanicos .= ($key > 0) ? ', ' : '';
					$tilt_mecanicos .= $telnet['tilt_mecanico'];
					
					$tilt_electricos .= ($key > 0 && $telnet['tilt_electrico']) ? ', ' : '';
					$tilt_electricos .= $telnet['tilt_electrico'];
				}
			}
			
			$sector = ($fila['tipo_antena_gul']['marca'] != 'TELNET') ? $fila['sector'] : $sectores;
			$azimuth = ($fila['tipo_antena_gul']['marca'] != 'TELNET')? $fila['azimuth'] : $azimuths;
			$tilt_mecanico = ($fila['tipo_antena_gul']['marca'] != 'TELNET') ? $fila['tilt_mecanico'] : $tilt_mecanicos;
			$tilt_electrico = ($fila['tipo_antena_gul']['marca'] != 'TELNET') ? $fila['tilt_electrico'] : $tilt_electricos;
			
			$activeSheet->setCellValue('A'.$i, $fila['codigo'])
						->setCellValue('B'.$i, $fila['nombre_completo'])
						->setCellValue('C'.$i, "P".$fila['prioridad'])
						->setCellValue('D'.$i, $fila['departamento'])
						->setCellValue('E'.$i, $fila['provincia'])
						->setCellValue('F'.$i, $fila['distrito'])
						->setCellValue('G'.$i, $fila['tipo_antena_gul']['marca'])
						->setCellValue('H'.$i, $fila['tipo_antena_gul']['modelo'])
						->setCellValue('I'.$i, $sector)
						->setCellValue('J'.$i, $fila['altura'])
						->setCellValue('K'.$i, $azimuth)
						->setCellValue('L'.$i, $tilt_mecanico)
						->setCellValue('M'.$i, $tilt_electrico)
						->setCellValue('N'.$i, $fila['numero_serie']);

			$i++;
		}

		$activeSheet->getColumnDimension('A')->setAutoSize(true);
		$activeSheet->getColumnDimension('B')->setAutoSize(true);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setAutoSize(true);
		$activeSheet->getColumnDimension('E')->setAutoSize(true);
		$activeSheet->getColumnDimension('F')->setAutoSize(true);
		$activeSheet->getColumnDimension('G')->setAutoSize(true);
		$activeSheet->getColumnDimension('H')->setAutoSize(true);
		$activeSheet->getColumnDimension('I')->setAutoSize(true);
		$activeSheet->getColumnDimension('J')->setAutoSize(true);
		$activeSheet->getColumnDimension('K')->setAutoSize(true);
		$activeSheet->getColumnDimension('L')->setAutoSize(true);
		$activeSheet->getColumnDimension('M')->setAutoSize(true);
		$activeSheet->getColumnDimension('N')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Antenas_GUL_'.date('d-m-Y-h-i-s').'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function reportes_predeterminados_antenas_iden()
	{
		$listado = $this->Reportes_model->get_antenas_iden($this->data);

		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Reporte Antenas iDEN')
					->setSubject('Reporte')
					->setDescription('Reporte predeterminado desde el Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Antenas iDEN');

		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Prioridad')
					->setCellValue('D1', 'Departamento')
					->setCellValue('E1', 'Provincia')
					->setCellValue('F1', 'Distrito')
					->setCellValue('G1', 'Marca')
					->setCellValue('H1', 'Modelo')
					->setCellValue('I1', 'Sector')
					->setCellValue('J1', 'Cantidad')
					->setCellValue('K1', 'Azimuth (°)')
					->setCellValue('L1', 'Tilt mecánico (°)')
					->setCellValue('M1', 'Tilt eléctrico (°)')
					->setCellValue('N1', 'Número de serie')
					->setCellValue('O1', 'Altura (m)');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$activeSheet->getStyle('A1:O1')
					->applyFromArray($styleArray);
		$activeSheet->setAutoFilter('A1:O1');

		$i = 2;
		foreach ($listado as $fila)
		{
			$activeSheet->setCellValue('A'.$i, $fila['codigo'])
						->setCellValue('B'.$i, $fila['nombre_completo'])
						->setCellValue('C'.$i, "P".$fila['prioridad'])
						->setCellValue('D'.$i, $fila['departamento'])
						->setCellValue('E'.$i, $fila['provincia'])
						->setCellValue('F'.$i, $fila['distrito'])
						->setCellValue('G'.$i, $fila['tipo_antena_iden']['marca'])
						->setCellValue('H'.$i, $fila['tipo_antena_iden']['modelo'])
						->setCellValue('I'.$i, $fila['sector'])
						->setCellValue('J'.$i, $fila['cantidad'])
						->setCellValue('K'.$i, $fila['azimuth'])
						->setCellValue('L'.$i, $fila['tilt_mecanico'])
						->setCellValue('M'.$i, $fila['tilt_electrico'])
						->setCellValue('N'.$i, $fila['numero_serie'])
						->setCellValue('O'.$i, $fila['altura']);

			$i++;
		}

		$activeSheet->getColumnDimension('A')->setAutoSize(true);
		$activeSheet->getColumnDimension('B')->setAutoSize(true);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setAutoSize(true);
		$activeSheet->getColumnDimension('E')->setAutoSize(true);
		$activeSheet->getColumnDimension('F')->setAutoSize(true);
		$activeSheet->getColumnDimension('G')->setAutoSize(true);
		$activeSheet->getColumnDimension('H')->setAutoSize(true);
		$activeSheet->getColumnDimension('I')->setAutoSize(true);
		$activeSheet->getColumnDimension('J')->setAutoSize(true);
		$activeSheet->getColumnDimension('K')->setAutoSize(true);
		$activeSheet->getColumnDimension('L')->setAutoSize(true);
		$activeSheet->getColumnDimension('M')->setAutoSize(true);
		$activeSheet->getColumnDimension('N')->setAutoSize(true);
		$activeSheet->getColumnDimension('O')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Antenas_iDEN_'.date('d-m-Y-h-i-s').'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_antenas_in_building()
	{
		$listado = $this->Reportes_model->get_antenas_in_building($this->data);
		
		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Reporte Antenas In Building')
					->setSubject('Reporte')
					->setDescription('Reporte predeterminado desde el Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Antenas In Building');

		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Prioridad')
					->setCellValue('D1', 'Departamento')
					->setCellValue('E1', 'Provincia')
					->setCellValue('F1', 'Distrito')
					->setCellValue('G1', 'Marca')
					->setCellValue('H1', 'Modelo')
					->setCellValue('I1', 'Tipo')
					->setCellValue('J1', 'Configuración')
					->setCellValue('K1', 'Banda (GHz)')
					->setCellValue('L1', 'Peso (Kg)')
					->setCellValue('M1', 'Dimensiones')
					->setCellValue('N1', 'Cantidad');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$activeSheet->getStyle('A1:N1')
					->applyFromArray($styleArray);

		$i = 2;
		foreach ($listado as $fila)
		{
			$activeSheet->setCellValue('A'.$i, $fila['codigo'])
						->setCellValue('B'.$i, $fila['nombre_completo'])
						->setCellValue('C'.$i, "P".$fila['prioridad'])
						->setCellValue('D'.$i, $fila['departamento'])
						->setCellValue('E'.$i, $fila['provincia'])
						->setCellValue('F'.$i, $fila['distrito'])
						->setCellValue('G'.$i, $fila['tipo_antena_in_building']['marca'])
						->setCellValue('H'.$i, $fila['tipo_antena_in_building']['modelo'])
						->setCellValue('I'.$i, $fila['tipo_antena_in_building']['tipo'])
						->setCellValue('J'.$i, $fila['tipo_antena_in_building']['configuracion'])
						->setCellValue('K'.$i, $fila['tipo_antena_in_building']['banda'])
						->setCellValue('L'.$i, $fila['tipo_antena_in_building']['peso'])
						->setCellValue('M'.$i, $fila['tipo_antena_in_building']['dimensiones'])
						->setCellValue('N'.$i, $fila['cantidad']);

			$i++;
		}

		$activeSheet->getColumnDimension('A')->setAutoSize(true);
		$activeSheet->getColumnDimension('B')->setAutoSize(true);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setAutoSize(true);
		$activeSheet->getColumnDimension('E')->setAutoSize(true);
		$activeSheet->getColumnDimension('F')->setAutoSize(true);
		$activeSheet->getColumnDimension('G')->setAutoSize(true);
		$activeSheet->getColumnDimension('H')->setAutoSize(true);
		$activeSheet->getColumnDimension('I')->setAutoSize(true);
		$activeSheet->getColumnDimension('J')->setAutoSize(true);
		$activeSheet->getColumnDimension('K')->setAutoSize(true);
		$activeSheet->getColumnDimension('L')->setAutoSize(true);
		$activeSheet->getColumnDimension('M')->setAutoSize(true);
		$activeSheet->getColumnDimension('N')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Antenas_In_Building_'.date('d-m-Y-h-i-s').'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_idus()
	{
		$listado = $this->Reportes_model->get_idus($this->data);
		
		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Reporte Radios IDUs')
					->setSubject('Reporte')
					->setDescription('Reporte predeterminado desde el Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Radios - IDUs');

		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Prioridad')
					->setCellValue('D1', 'Departamento')
					->setCellValue('E1', 'Provincia')
					->setCellValue('F1', 'Distrito')
					->setCellValue('G1', 'NE ID')
					->setCellValue('H1', 'Link Name')
					->setCellValue('I1', 'Marca')
					->setCellValue('J1', 'Modelo')
					->setCellValue('K1', 'Número de serie')
					->setCellValue('L1', 'Consumo de energía')
					->setCellValue('M1', 'Capacidad de Conmutación (GHz)')
					->setCellValue('N1', 'Peso (Kg)')
					->setCellValue('O1', 'Dimensiones');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$activeSheet->getStyle('A1:O1')
					->applyFromArray($styleArray);

		$i = 2;
		foreach ($listado as $fila)
		{
			$activeSheet->setCellValue('A'.$i, $fila['codigo'])
						->setCellValue('B'.$i, $fila['nombre_completo'])
						->setCellValue('C'.$i, "P".$fila['prioridad'])
						->setCellValue('D'.$i, $fila['departamento'])
						->setCellValue('E'.$i, $fila['provincia'])
						->setCellValue('F'.$i, $fila['distrito'])
						->setCellValue('G'.$i, $fila['ne_id'])
						->setCellValue('H'.$i, $fila['link_name'])
						->setCellValue('I'.$i, $fila['tipo_idu']['marca'])
						->setCellValue('J'.$i, $fila['tipo_idu']['modelo'])
						->setCellValue('K'.$i, $fila['numero_serie'])
						->setCellValue('L'.$i, $fila['tipo_idu']['consumo_energia'])
						->setCellValue('M'.$i, $fila['tipo_idu']['capacidad_conmutacion'])
						->setCellValue('N'.$i, $fila['tipo_idu']['peso'])
						->setCellValue('O'.$i, $fila['tipo_idu']['dimensiones']);

			$i++;
		}

		$activeSheet->getColumnDimension('A')->setAutoSize(true);
		$activeSheet->getColumnDimension('B')->setAutoSize(true);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setAutoSize(true);
		$activeSheet->getColumnDimension('E')->setAutoSize(true);
		$activeSheet->getColumnDimension('F')->setAutoSize(true);
		$activeSheet->getColumnDimension('G')->setAutoSize(true);
		$activeSheet->getColumnDimension('H')->setAutoSize(true);
		$activeSheet->getColumnDimension('I')->setAutoSize(true);
		$activeSheet->getColumnDimension('J')->setAutoSize(true);
		$activeSheet->getColumnDimension('K')->setAutoSize(true);
		$activeSheet->getColumnDimension('L')->setAutoSize(true);
		$activeSheet->getColumnDimension('M')->setAutoSize(true);
		$activeSheet->getColumnDimension('N')->setAutoSize(true);
		$activeSheet->getColumnDimension('O')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_IDUs_'.date('d-m-Y-h-i-s').'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_enlaces()
	{
		$listado = $this->Reportes_model->get_enlaces($this->data);
		
		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Reporte Radios IDUs')
					->setSubject('Reporte')
					->setDescription('Reporte predeterminado desde el Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Enlaces');

		$activeSheet->setCellValue('A1', 'Near End')
					->mergeCells('A1:C2')
					->setCellValue('D1', 'Far End')
					->mergeCells('D1:F2')
					->setCellValue('G1', 'Near End')
					->mergeCells('G1:J2')
					->setCellValue('K1', 'Far End')
					->mergeCells('K1:N2')
					->setCellValue('O1', 'Near End')
					->mergeCells('O1:S1')
					->setCellValue('O2', 'Antena(s)')
					->mergeCells('O2:S2')
					->setCellValue('T1', 'Far End')
					->mergeCells('T1:X1')
					->setCellValue('T2', 'Antena(s)')
					->mergeCells('T2:X2')
					->setCellValue('Y1', 'Configuración del Enlace')
					->mergeCells('Y1:AG2')
					->setCellValue('A3', 'Código')
					->setCellValue('B3', 'Nombre')
					->setCellValue('C3', 'IDU ID')
					->setCellValue('D3', 'Código')
					->setCellValue('E3', 'Nombre')
					->setCellValue('F3', 'IDU ID')
					->setCellValue('G3', 'ODU')
					->setCellValue('H3', 'Número de Serie')
					->setCellValue('I3', 'Ftx (MHz)')
					->setCellValue('J3', 'Ptx (dBm)')
					->setCellValue('K3', 'ODU')
					->setCellValue('L3', 'Número de Serie')
					->setCellValue('M3', 'Ftx (MHz)')
					->setCellValue('N3', 'Ptx (dBm)')
					->setCellValue('O3', 'Marca')
					->setCellValue('P3', 'Modelo')
					->setCellValue('Q3', 'Número de Serie')
					->setCellValue('R3', 'Altura (m)')
					->setCellValue('S3', 'Azimuth (°)')
					->setCellValue('T3', 'Marca')
					->setCellValue('U3', 'Modelo')
					->setCellValue('V3', 'Número de Serie')
					->setCellValue('W3', 'Altura (m)')
					->setCellValue('X3', 'Azimuth (°)')
					->setCellValue('Y3', 'Polaridad')
					->setCellValue('Z3', 'Configuración')
					->setCellValue('AA3', 'Ancho de banda/BW (MHz)')
					->setCellValue('AB3', 'Modulación')
					->setCellValue('AC3', 'Capacidad (Mbit/s)')
					->setCellValue('AD3', 'Modulación Adaptativa (AM)')
					->setCellValue('AE3', 'Modulación AM')
					->setCellValue('AF3', 'Capacidad AM (Mbit/s)')
					->setCellValue('AG3', 'Banda (GHz)');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => '000000'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '22b14c')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
						),
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
								'color' => array('rgb' => '000000')
							)
						));

		$activeSheet->getStyle('A1:C2')
					->applyFromArray($styleArray);
		$activeSheet->getStyle('G1:J2')
					->applyFromArray($styleArray);
		$activeSheet->getStyle('O1:S1')
					->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => '000000'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'ff0000')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
						),
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
								'color' => array('rgb' => '000000')
							)
						));

		$activeSheet->getStyle('D1:F2')
					->applyFromArray($styleArray);
		$activeSheet->getStyle('K1:N2')
					->applyFromArray($styleArray);
		$activeSheet->getStyle('T1:X1')
					->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							'wrap' => true
						),
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
								'color' => array('rgb' => '000000')
							)
						));

		$activeSheet->getStyle('A3:AG3')
					->applyFromArray($styleArray);
		$activeSheet->getStyle('Y1:AG2')
					->applyFromArray($styleArray);
		$activeSheet->getStyle('O2:S2')
					->applyFromArray($styleArray);
		$activeSheet->getStyle('T2:X2')
					->applyFromArray($styleArray);

		$i = 4;
		foreach ($listado as $fila)
		{
			if(count($fila['odus_ne']) > 0)
			{
				$tipo_odu_ne = $fila['odus_ne'][0]['tipo_odu']['tipo'];
				$ftx_odu_ne = $fila['odus_ne'][0]['ftx'];
				$ptx_odu_ne = $fila['odus_ne'][0]['ptx'];
				$ns_odu_ne = '';
				
				foreach($fila['odus_ne'] as $key => $odu)
				{
					$ns_odu_ne .= ($key > 0)? ', ' : '';
					$ns_odu_ne .= $odu['numero_serie'];
				}
			}
			else
			{
				$tipo_odu_ne = '';
				$ftx_odu_ne = '';
				$ptx_odu_ne = '';
				$ns_odu_ne = '';
			}
			
			if(count($fila['odus_fe']) > 0)
			{
				$tipo_odu_fe = $fila['odus_fe'][0]['tipo_odu']['tipo'];
				$ftx_odu_fe = $fila['odus_fe'][0]['ftx'];
				$ptx_odu_fe = $fila['odus_fe'][0]['ptx'];
				$ns_odu_fe = '';
				
				foreach($fila['odus_fe'] as $key => $odu)
				{
					$ns_odu_fe .= ($key > 0)? ', ' : '';
					$ns_odu_fe .= $odu['numero_serie'];
				}
			}
			else
			{
				$tipo_odu_fe = '';
				$ftx_odu_fe = '';
				$ptx_odu_fe = '';
				$ns_odu_fe = '';
			}
			
			$marca_antena_ne = '';
			$modelo_antena_ne = '';
			$ns_antena_ne = '';
			$altura_antena_ne = '';
			$azimuth_antena_ne = '';

			if(count($fila['antenas_mw_ne']) > 0)
			{
				foreach($fila['antenas_mw_ne'] as $key => $antena)
				{
					$marca_antena_ne .= ($key > 0)? ', ' : '';
					$marca_antena_ne .= $antena['tipo_antena_mw']['marca'];
					
					$modelo_antena_ne .= ($key > 0)? ', ' : '';
					$modelo_antena_ne .= $antena['tipo_antena_mw']['modelo'];
					
					$ns_antena_ne .= ($key > 0)? ', ' : '';
					$ns_antena_ne .= $antena['numero_serie'];
					
					$altura_antena_ne .= ($key > 0)? ', ' : '';
					$altura_antena_ne .= $antena['altura'];
					
					$azimuth_antena_ne .= ($key > 0)? ', ' : '';
					$azimuth_antena_ne .= $antena['azimuth'];
				}
			}
			
			$marca_antena_fe = '';
			$modelo_antena_fe = '';
			$ns_antena_fe = '';
			$altura_antena_fe = '';
			$azimuth_antena_fe = '';

			if(count($fila['antenas_mw_fe']) > 0)
			{
				foreach($fila['antenas_mw_fe'] as $key => $antena)
				{
					$marca_antena_fe .= ($key > 0)? ', ' : '';
					$marca_antena_fe .= $antena['tipo_antena_mw']['marca'];
					
					$modelo_antena_fe .= ($key > 0)? ', ' : '';
					$modelo_antena_fe .= $antena['tipo_antena_mw']['modelo'];
					
					$ns_antena_fe .= ($key > 0)? ', ' : '';
					$ns_antena_fe .= $antena['numero_serie'];
					
					$altura_antena_fe .= ($key > 0)? ', ' : '';
					$altura_antena_fe .= $antena['altura'];
					
					$azimuth_antena_fe .= ($key > 0)? ', ' : '';
					$azimuth_antena_fe .= $antena['azimuth'];
				}
			}
			
			switch($fila['polaridad'])
			{
				case 1:
					$polaridad = 'V';
					break;
				case 2:
					$polaridad = 'H';
					break;
				case 3:
					$polaridad = 'V+H';
			}
			
			switch($fila['modulacion_adaptativa'])
			{
				case 0:
					$modulacion_adaptativa = 'NO';
					break;
				case 1:
					$modulacion_adaptativa = 'SI';
					break;
			}
			
			switch($fila['capacidad_am'])
			{
				case 0:
					$capacidad_am = 'N/A';
					break;
				default:
					$capacidad_am = $fila['capacidad_am'];
			}
			
			$activeSheet->setCellValue('A'.$i, $fila['idu_1']['sitio']['codigo'])
						->setCellValue('B'.$i, $fila['idu_1']['sitio']['nombre_completo'])
						->setCellValue('C'.$i, $fila['idu_1']['ne_id'])
						->setCellValue('D'.$i, $fila['idu_2']['sitio']['codigo'])
						->setCellValue('E'.$i, $fila['idu_2']['sitio']['nombre_completo'])
						->setCellValue('F'.$i, $fila['idu_2']['ne_id'])
						->setCellValue('G'.$i, $tipo_odu_ne)
						->setCellValue('H'.$i, $ns_odu_ne)
						->setCellValue('I'.$i, $ftx_odu_ne)
						->setCellValue('J'.$i, $ptx_odu_ne)
						->setCellValue('K'.$i, $tipo_odu_fe)
						->setCellValue('L'.$i, $ns_odu_fe)
						->setCellValue('M'.$i, $ftx_odu_fe)
						->setCellValue('N'.$i, $ptx_odu_fe)
						->setCellValue('O'.$i, $marca_antena_ne)
						->setCellValue('P'.$i, $modelo_antena_ne)
						->setCellValue('Q'.$i, $ns_antena_ne)
						->setCellValue('R'.$i, $altura_antena_ne)
						->setCellValue('S'.$i, $azimuth_antena_ne)
						->setCellValue('T'.$i, $marca_antena_fe)
						->setCellValue('U'.$i, $modelo_antena_fe)
						->setCellValue('V'.$i, $ns_antena_fe)
						->setCellValue('W'.$i, $altura_antena_fe)
						->setCellValue('X'.$i, $azimuth_antena_fe)
						->setCellValue('Y'.$i, $polaridad)
						->setCellValue('Z'.$i, $fila['configuracion'])
						->setCellValue('AA'.$i, $fila['ancho_banda'])
						->setCellValue('AB'.$i, $fila['modulacion'])
						->setCellValue('AC'.$i, $fila['capacidad'])
						->setCellValue('AD'.$i, $modulacion_adaptativa)
						->setCellValue('AE'.$i, $fila['modulacion_am'])
						->setCellValue('AF'.$i, $capacidad_am)
						->setCellValue('AG'.$i, $fila['banda']);

			$i++;
		}

		$activeSheet->getColumnDimension('A')->setAutoSize(true);
		$activeSheet->getColumnDimension('B')->setAutoSize(true);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setAutoSize(true);
		$activeSheet->getColumnDimension('E')->setAutoSize(true);
		$activeSheet->getColumnDimension('F')->setAutoSize(true);
		$activeSheet->getColumnDimension('G')->setAutoSize(true);
		$activeSheet->getColumnDimension('H')->setAutoSize(true);
		$activeSheet->getColumnDimension('I')->setAutoSize(true);
		$activeSheet->getColumnDimension('J')->setAutoSize(true);
		$activeSheet->getColumnDimension('K')->setAutoSize(true);
		$activeSheet->getColumnDimension('L')->setAutoSize(true);
		$activeSheet->getColumnDimension('M')->setAutoSize(true);
		$activeSheet->getColumnDimension('N')->setAutoSize(true);
		$activeSheet->getColumnDimension('O')->setAutoSize(true);
		$activeSheet->getColumnDimension('P')->setAutoSize(true);
		$activeSheet->getColumnDimension('Q')->setAutoSize(true);
		$activeSheet->getColumnDimension('R')->setAutoSize(true);
		$activeSheet->getColumnDimension('S')->setAutoSize(true);
		$activeSheet->getColumnDimension('T')->setAutoSize(true);
		$activeSheet->getColumnDimension('U')->setAutoSize(true);
		$activeSheet->getColumnDimension('V')->setAutoSize(true);
		$activeSheet->getColumnDimension('W')->setAutoSize(true);
		$activeSheet->getColumnDimension('X')->setAutoSize(true);
		$activeSheet->getColumnDimension('Y')->setAutoSize(true);
		$activeSheet->getColumnDimension('Z')->setAutoSize(true);
		$activeSheet->getColumnDimension('AA')->setAutoSize(true);
		$activeSheet->getColumnDimension('AB')->setAutoSize(true);
		$activeSheet->getColumnDimension('AC')->setAutoSize(true);
		$activeSheet->getColumnDimension('AD')->setAutoSize(true);
		$activeSheet->getColumnDimension('AE')->setAutoSize(true);
		$activeSheet->getColumnDimension('AF')->setAutoSize(true);
		$activeSheet->getColumnDimension('AG')->setAutoSize(true);
		
		$activeSheet->getSheetView()->setZoomScale(85);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Enlaces_'.date('d-m-Y-h-i-s').'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function reportes_predeterminados_suministros()
	{
		$listado = $this->Reportes_model->get_suministros($this->data);
		
		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Reporte Suministros')
					->setSubject('Reporte')
					->setDescription('Reporte predeterminado desde el Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Suministros');

		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Prioridad')
					->setCellValue('D1', 'Departamento')
					->setCellValue('E1', 'Provincia')
					->setCellValue('F1', 'Distrito')
					->setCellValue('G1', 'Tipo de alimentación')
					->setCellValue('H1', 'Concesionaria')
					->setCellValue('I1', 'Conexión')
					->setCellValue('J1', 'Opción tarifaria')
					->setCellValue('K1', 'Sistema')
					->setCellValue('L1', 'Suministro')
					->setCellValue('M1', 'Potencia contratada (KW)')
					->setCellValue('N1', 'Nivel de tensión (KV)');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$activeSheet->getStyle('A1:N1')
					->applyFromArray($styleArray);

		$i = 2;
		foreach ($listado as $fila)
		{
			$activeSheet->setCellValue('A'.$i, $fila['codigo'])
						->setCellValue('B'.$i, $fila['nombre_completo'])
						->setCellValue('C'.$i, "P".$fila['prioridad'])
						->setCellValue('D'.$i, $fila['departamento'])
						->setCellValue('E'.$i, $fila['provincia'])
						->setCellValue('F'.$i, $fila['distrito'])
						->setCellValue('G'.$i, $fila['tipo_alimentacion']['tipo_alimentacion'])
						->setCellValue('H'.$i, $fila['concesionaria_suministro']['concesionaria_suministro'])
						->setCellValue('I'.$i, $fila['conexion_suministro']['conexion_suministro'])
						->setCellValue('J'.$i, $fila['opcion_tarifaria']['opcion_tarifaria'])
						->setCellValue('K'.$i, $fila['sistema_suministro']['sistema_suministro'])
						->setCellValue('L'.$i, $fila['numero_suministro'])
						->setCellValue('M'.$i, $fila['potencia_contratada'])
						->setCellValue('N'.$i, $fila['nivel_tension']);

			$i++;
		}

		$activeSheet->setAutoFilter($activeSheet->calculateWorksheetDimension());

		$activeSheet->getColumnDimension('A')->setAutoSize(true);
		$activeSheet->getColumnDimension('B')->setAutoSize(true);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setAutoSize(true);
		$activeSheet->getColumnDimension('E')->setAutoSize(true);
		$activeSheet->getColumnDimension('F')->setAutoSize(true);
		$activeSheet->getColumnDimension('G')->setAutoSize(true);
		$activeSheet->getColumnDimension('H')->setAutoSize(true);
		$activeSheet->getColumnDimension('I')->setAutoSize(true);
		$activeSheet->getColumnDimension('J')->setAutoSize(true);
		$activeSheet->getColumnDimension('K')->setAutoSize(true);
		$activeSheet->getColumnDimension('L')->setAutoSize(true);
		$activeSheet->getColumnDimension('M')->setAutoSize(true);
		$activeSheet->getColumnDimension('N')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Suministros_'.date('d-m-Y-h-i-s').'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function reportes_predeterminados_tanques_combustible()
	{
		$listado = $this->Reportes_model->get_tanques_combustible($this->data);
		
		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Reporte Tanques de combustible')
					->setSubject('Reporte')
					->setDescription('Reporte predeterminado desde el Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Tanques de combustible');

		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Prioridad')
					->setCellValue('D1', 'Departamento')
					->setCellValue('E1', 'Provincia')
					->setCellValue('F1', 'Distrito')
					->setCellValue('G1', 'Tipo de tanque')
					->setCellValue('H1', 'Fecha de instalación')
					->setCellValue('I1', 'Capacidad (Gl)')
					->setCellValue('J1', 'Consumo (Gl/h)')
					->setCellValue('K1', 'Autonomía (h)');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$activeSheet->getStyle('A1:K1')
					->applyFromArray($styleArray);

		$i = 2;
		foreach ($listado as $fila)
		{
			$tipo_tanque = ($fila['tipo_tanque_combustible'] == '1') ? 'Tanque Interno' : (($fila['tipo_tanque_combustible'] == '2')? 'Tanque Externo' : '');
			$autonomia = round(((Float)$fila['consumo'] != 0) ? (((FLoat)$fila['capacidad'])*0.9)/((Float)$fila['consumo']) : 0,2);

			$activeSheet->setCellValue('A'.$i, $fila['codigo'])
						->setCellValue('B'.$i, $fila['nombre_completo'])
						->setCellValue('C'.$i, 'P'.$fila['prioridad'])
						->setCellValue('D'.$i, $fila['departamento'])
						->setCellValue('E'.$i, $fila['provincia'])
						->setCellValue('F'.$i, $fila['distrito'])
						->setCellValue('G'.$i, $tipo_tanque)
						->setCellValue('H'.$i, $fila['fecha_instalacion'])
						->setCellValue('I'.$i, $fila['capacidad'])
						->setCellValue('J'.$i, $fila['consumo'])
						->setCellValue('K'.$i, $autonomia);

			$i++;
		}

		$activeSheet->getColumnDimension('A')->setAutoSize(true);
		$activeSheet->getColumnDimension('B')->setAutoSize(true);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setAutoSize(true);
		$activeSheet->getColumnDimension('E')->setAutoSize(true);
		$activeSheet->getColumnDimension('F')->setAutoSize(true);
		$activeSheet->getColumnDimension('G')->setAutoSize(true);
		$activeSheet->getColumnDimension('H')->setAutoSize(true);
		$activeSheet->getColumnDimension('I')->setAutoSize(true);
		$activeSheet->getColumnDimension('J')->setAutoSize(true);
		$activeSheet->getColumnDimension('K')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Tanques_Combustible_'.date('d-m-Y-h-i-s').'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_lineas_electricas()
	{
		$listado = $this->Reportes_model->get_lineas_electricas($this->data);
		
		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Reporte Líneas Eléctricas')
					->setSubject('Reporte')
					->setDescription('Reporte predeterminado desde el Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Líneas Eléctricas');
		
		$activeSheet->setCellValue('O1', 'Transformador')
					->mergeCells('O1:R1')
					->setCellValue('S1', 'Transformix')
					->mergeCells('S1:V1')
					->setCellValue('A2', 'Código')
					->setCellValue('B2', 'Nombre')
					->setCellValue('C2', 'Prioridad')
					->setCellValue('D2', 'Departamento')
					->setCellValue('E2', 'Provincia')
					->setCellValue('F2', 'Distrito')
					->setCellValue('G2', 'Fecha de puesta en Servicio')
					->setCellValue('H2', 'Fases')
					->setCellValue('I2', 'Tipo')
					->setCellValue('J2', 'Sección de Línea')
					->setCellValue('K2', 'Distancia (km)')
					->setCellValue('L2', 'Postes')
					->setCellValue('M2', 'Propietario del Terreno')
					->setCellValue('N2', 'Contrato de servidumbre')
					->setCellValue('O2', 'Marca')
					->setCellValue('P2', 'Número de Serie')
					->setCellValue('Q2', 'Potencia')
					->setCellValue('R2', 'Conexión')
					->setCellValue('S2', 'Marca')
					->setCellValue('T2', 'Número de Serie')
					->setCellValue('U2', 'Potencia')
					->setCellValue('V2', 'Conexión');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$activeSheet->getStyle('A2:V2')
					->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '22B14C')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$activeSheet->getStyle('O1')
					->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'FF8000')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$activeSheet->getStyle('S1')
					->applyFromArray($styleArray);
		
		$i = 3;
		foreach ($listado as $fila)
		{
			$propietarios = '';
			$contratos = '';
			$conexiones = '';
			
			foreach ($fila['propietarios_linea_electrica'] as $key => $propietario)
			{
				$propietarios .= ($key > 0)? ', ' : '';
				$propietarios .= $propietario['propietario'];
				
				$contratos .= ($key > 0)? ', ' : '';
				$contratos .= $propietario['contrato_servidumbre'];
			}
			
			foreach ($fila['lineas_electricas_conexion_linea_electrica'] as $key => $conexion)
			{
				$conexiones .= ($key > 0)? ' / ' : '';
				$conexiones .= $conexion['conexion_linea_electrica']['conexion_linea_electrica'];
			}
			
			if($fila['fecha_servicio'] == '0000-00-00')
			{
				$fecha = '';
			}
			else
			{
				$fecha = date('d/m/Y', strtotime($fila['fecha_servicio']));
			}
			
			$activeSheet->setCellValue('A'.$i, $fila['codigo'])
						->setCellValue('B'.$i, $fila['nombre_completo'])
						->setCellValue('C'.$i, 'P'.$fila['prioridad'])
						->setCellValue('D'.$i, $fila['departamento'])
						->setCellValue('E'.$i, $fila['provincia'])
						->setCellValue('F'.$i, $fila['distrito'])
						->setCellValue('G'.$i, $fecha)
						->setCellValue('H'.$i, $fila['sistema_linea_electrica']['sistema_linea_electrica'])
						->setCellValue('I'.$i, $conexiones)
						->setCellValue('J'.$i, $fila['seccion_linea'])
						->setCellValue('K'.$i, $fila['distancia'])
						->setCellValue('L'.$i, $fila['postes'])
						->setCellValue('M'.$i, $propietarios)
						->setCellValue('N'.$i, $contratos)
						->setCellValue('O'.$i, $fila['tipo_transformador']['marca'])
						->setCellValue('P'.$i, $fila['num_serie_transformador'])
						->setCellValue('Q'.$i, $fila['potencia_transformador'])
						->setCellValue('R'.$i, $fila['conexion_transformador'])
						->setCellValue('S'.$i, $fila['tipo_transformix']['marca'])
						->setCellValue('T'.$i, $fila['num_serie_transformix'])
						->setCellValue('U'.$i, $fila['potencia_transformix'])
						->setCellValue('V'.$i, $fila['conexion_transformix']);

			$i++;
		}

		$activeSheet->getColumnDimension('A')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('B')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('C')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('D')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('E')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('F')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('G')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('H')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('I')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('J')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('K')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('L')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('M')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('N')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('O')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('P')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('Q')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('R')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('S')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('T')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('U')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('V')->setAutoSize(TRUE);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Líneas_Eléctricas_'.date('d-m-Y-H-i-s').'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_rectificadores()
	{
		$listado = $this->Reportes_model->get_rectificadores($this->data);

		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Reporte Rectificadores')
					->setSubject('Reporte')
					->setDescription('Reporte predeterminado desde el Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Controladores');

		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Prioridad')
					->setCellValue('D1', 'Departamento')
					->setCellValue('E1', 'Provincia')
					->setCellValue('F1', 'Distrito')
					->setCellValue('G1', 'Marca')
					->setCellValue('H1', 'Modelo')
					->setCellValue('I1', 'Part Number')
					->setCellValue('J1', 'Módulos Instalados')
					->setCellValue('K1', 'Capacidad Instalada (kW)')
					->setCellValue('L1', 'Capacidad Máxima (kW)')
					->setCellValue('M1', 'Energización')
					->setCellValue('N1', 'Ubicación')
					->setCellValue('O1', 'Fecha de puesta en servicio');
					;

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$activeSheet->getStyle('A1:O1')
					->applyFromArray($styleArray);

		$i = 2;
		foreach ($listado as $fila)
		{
			$mod_instalados = count($fila['modulos_rectif']);
			
			$capacidad = 0;
			foreach ($fila['modulos_rectif'] as $key => $modulo_rectif)
			{
				$capacidad += (Float)$modulo_rectif['tipo_modulos_rectif']['capacidad'];
			}

			$activeSheet->setCellValue('A'.$i, $fila['codigo'])
						->setCellValue('B'.$i, $fila['nombre_completo'])
						->setCellValue('C'.$i, 'P'.$fila['prioridad'])
						->setCellValue('D'.$i, $fila['departamento'])
						->setCellValue('E'.$i, $fila['provincia'])
						->setCellValue('F'.$i, $fila['distrito'])
						->setCellValue('G'.$i, $fila['tipo_controlador_rectif']['marca'])
						->setCellValue('H'.$i, $fila['tipo_controlador_rectif']['modelo'])
						->setCellValue('I'.$i, $fila['numero_serie'])
						->setCellValue('J'.$i, $mod_instalados)
						->setCellValue('K'.$i, $capacidad)
						->setCellValue('L'.$i, $fila['capacidad_maxima'])
						->setCellValue('M'.$i, $fila['energizacion_controlador_rectif']['energizacion_controlador_rectif'])
						->setCellValue('N'.$i, $fila['ubicacion_controlador_rectif']['ubicacion_controlador_rectif'])
						->setCellValue('O'.$i, date('d/m/Y', strtotime($fila['anio_servicio'])));

			$i++;
		}

		$activeSheet->getColumnDimension('A')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('B')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('C')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('D')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('E')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('F')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('G')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('H')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('I')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('J')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('K')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('L')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('M')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('N')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('O')->setAutoSize(TRUE);
		
		$this->excel->createSheet();
		
		$csheet = 1;

		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Módulos');
		
		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Prioridad')
					->setCellValue('D1', 'Departamento')
					->setCellValue('E1', 'Provincia')
					->setCellValue('F1', 'Distrito')
					->setCellValue('G1', 'Marca Controlador')
					->setCellValue('H1', 'Modelo Controlador')
					->setCellValue('I1', 'Energización Controlador')
					->setCellValue('J1', 'Marca')
					->setCellValue('K1', 'Modelo')
					->setCellValue('L1', 'Capacidad (kW)')
					->setCellValue('M1', 'Amperaje (A)')
					->setCellValue('N1', 'Voltaje (V)')
					->setCellValue('O1', 'Número de Serie');
		
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$activeSheet->getStyle('A1:O1')
					->applyFromArray($styleArray);

		$i = 2;
		foreach ($listado as $fila)
		{
			foreach ($fila['modulos_rectif'] as $key => $modulos_rectif)
			{
				$activeSheet->setCellValue('A'.$i, $fila['codigo'])
							->setCellValue('B'.$i, $fila['nombre_completo'])
							->setCellValue('C'.$i, "P".$fila['prioridad'])
							->setCellValue('D'.$i, $fila['departamento'])
							->setCellValue('E'.$i, $fila['provincia'])
							->setCellValue('F'.$i, $fila['distrito'])
							->setCellValue('G'.$i, $modulos_rectif['tipo_modulos_rectif']['marca'])
							->setCellValue('H'.$i, $fila['tipo_controlador_rectif']['modelo'])
							->setCellValue('I'.$i, $fila['energizacion_controlador_rectif']['energizacion_controlador_rectif'])
							->setCellValue('J'.$i, $fila['tipo_controlador_rectif']['marca'])
							->setCellValue('K'.$i, $modulos_rectif['tipo_modulos_rectif']['modelo'])
							->setCellValue('L'.$i, $modulos_rectif['tipo_modulos_rectif']['capacidad'])
							->setCellValue('M'.$i, $modulos_rectif['tipo_modulos_rectif']['amperaje'])
							->setCellValue('N'.$i, $modulos_rectif['tipo_modulos_rectif']['voltaje'])
							->setCellValue('O'.$i, $modulos_rectif['numero_serie']);
				
				$i++;
			}
		}
		
		$activeSheet->getColumnDimension('A')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('B')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('C')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('D')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('E')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('F')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('G')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('H')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('I')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('J')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('K')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('L')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('M')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('N')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('O')->setAutoSize(TRUE);
		
		$this->excel->setActiveSheetIndex(0);
		
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Rectificadores_'.date('d-m-Y-H-i-s').'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_aires_acondicionados()
	{
		$data = $this->input->post();

		foreach ($data as $key => $value)
		{
			if( !(isset($value)) || ($value == "") || ($value == NULL) )
			{
				unset($data[$key]);
			}
		}

		$this->load->model("Reportes_model");

		$listado = $this->Reportes_model->get_aires_acondicionados($data);
		
		$this->load->library("excel");

		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Reporte Aires Acondicionados")
									->setSubject("Reporte")
									->setDescription("Reporte predeterminado desde el Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Aires Acondicionados');
		
		$this->excel->getActiveSheet()->setCellValue('I1', 'Equipos Liebert/Bard/Marvair/Mclean/Pentair/Telco');
		$this->excel->getActiveSheet()->mergeCells("I1:J1");
		
		$this->excel->getActiveSheet()->setCellValue('N1', 'Compresor');
		$this->excel->getActiveSheet()->mergeCells("N1:P1");
		
		$this->excel->getActiveSheet()->setCellValue('Q1', 'Condensador');
		$this->excel->getActiveSheet()->mergeCells("Q1:R1");
		
		$this->excel->getActiveSheet()->setCellValue('S1', 'Evaporador');
		$this->excel->getActiveSheet()->mergeCells("S1:T1");

		$this->excel->getActiveSheet()->setCellValue('A2', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B2', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C2', 'Prioridad');
		$this->excel->getActiveSheet()->setCellValue('D2', 'Departamento');
		$this->excel->getActiveSheet()->setCellValue('E2', 'Provincia');
		$this->excel->getActiveSheet()->setCellValue('F2', 'Distrito');
		$this->excel->getActiveSheet()->setCellValue('G2', 'Refrigerante');
		$this->excel->getActiveSheet()->setCellValue('H2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('I2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('J2', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('K2', 'Tipo de Equipo');
		$this->excel->getActiveSheet()->setCellValue('L2', 'Capacidad (Ton)');
		$this->excel->getActiveSheet()->setCellValue('M2', 'Fecha de Instalación');
		$this->excel->getActiveSheet()->setCellValue('N2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('O2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('P2', 'Tipo');
		$this->excel->getActiveSheet()->setCellValue('Q2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('R2', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('S2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('T2', 'Número de Serie');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A2:T2")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => 'FFFFFF'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'ff0000')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("I1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => 'FFFFFF'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '7030a0')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("N1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => 'FFFFFF'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '00b050')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("Q1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => 'FFFFFF'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'e26b0a')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("S1")
										->applyFromArray($styleArray);
		
		$i = 3;
		foreach ($listado as $fila)
		{
			$fecha_instalacion = (!$fila['fecha_instalacion'])? "Pendiente" : date("d/m/Y", strtotime($fila['fecha_instalacion']));
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['codigo']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['nombre_completo']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, "P".$fila['prioridad']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['departamento']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['provincia']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['distrito']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['tipo_refrigerante']['tipo_refrigerante']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['tipo_aires_acondicionados']['marca']);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['tipo_aires_acondicionados']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $fila['num_serie_aire_acon']);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $fila['tipo_aires_acondicionados']['tipo_equipo']);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $fila['tipo_aires_acondicionados']['capacidad']);
			$this->excel->getActiveSheet()->setCellValue('M'.$i, $fecha_instalacion);
			$this->excel->getActiveSheet()->setCellValue('N'.$i, $fila['tipo_compresor']['marca']);
			$this->excel->getActiveSheet()->setCellValue('O'.$i, $fila['tipo_compresor']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('P'.$i, $fila['tipo_compresor']['tipo']);
			$this->excel->getActiveSheet()->setCellValue('Q'.$i, $fila['tipo_condensador']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('R'.$i, $fila['num_serie_condensador']." ");
			$this->excel->getActiveSheet()->setCellValue('S'.$i, $fila['tipo_evaporador']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('T'.$i, $fila['num_serie_evaporador']);

			$i++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Aires_Acondicionados_'.date("d-m-Y-H-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function reportes_predeterminados_bancos_baterias_test()
	{

    $this->load->model("Reportes_model");
	$listado = $this->Reportes_model->get_bancos_baterias($data);
	//echo json_encode($listado);

	}
	
	public function reportes_predeterminados_bancos_baterias()
	{		
        
        ini_set('max_execution_time', 0); 
        ini_set('memory_limit','2048M');

		$data = $this->input->post();

		foreach ($data as $key => $value)
		{
			if( !(isset($value)) || ($value == "") || ($value == NULL) )
			{
				unset($data[$key]);
			}
		}

		$this->load->model("Reportes_model");

		$listado = $this->Reportes_model->get_bancos_baterias($data);
       //echo json_encode($listado);

		$this->load->library("excel");

		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Reporte Bancos de Baterías")
									->setSubject("Reporte")
									->setDescription("Reporte predeterminado desde el Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Bancos de Baterías');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Prioridad');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Departamento');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Provincia');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Distrito');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Capacidad (Ah)/Banco');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Cantidad de Celdas');
		$this->excel->getActiveSheet()->setCellValue('K1', 'Fecha de Instalación');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Energización del rectificador');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A1:L1")
										->applyFromArray($styleArray);

		$i = 2;
		foreach ($listado as $fila)
		{
			if($fila['fecha_instalacion'] == "0000-00-00")
			{
				$fecha_instalacion = "";
			}
			else
			{
				$fecha_instalacion = date("d/m/Y", strtotime($fila['fecha_instalacion']));
			}
			
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['codigo']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['nombre_completo']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, "P".$fila['prioridad']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['departamento']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['provincia']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['distrito']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['tipo_bancos_baterias']['marca']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['tipo_bancos_baterias']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['tipo_bancos_baterias']['capacidad']);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $fila['tipo_bancos_baterias']['celdas']);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $fecha_instalacion);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $fila['energizacion_controlador_rectif']);

			$i++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		
		$this->excel->createSheet();
		
		$csheet = 1;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Celdas');
		
		$this->excel->getActiveSheet()->setCellValue('A1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Fecha de Instalación');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Energización del Rectificador');
		
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A1:E1")
										->applyFromArray($styleArray);

		$i = 2;
		foreach ($listado as $fila)
		{
			foreach ($fila['celdas_controlador_rectif_bancos_baterias'] as $key => $celda)
			{
				if($celda['fecha_instalacion'] == "0000-00-00")
				{
					$fecha_instalacion = "";
				}
				else
				{
					$fecha_instalacion = date("d/m/Y", strtotime($celda['fecha_instalacion']));
				}

				$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['nombre_completo']);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $fecha_instalacion);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $celda['numero_serie']." ");
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['energizacion_controlador_rectif']);
				
				$i++;
			}
		}
		
		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		
		$this->excel->setActiveSheetIndex(0);
		
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Bancos_Baterias_'.date("d-m-Y-H-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_grupos_electrogenos()
	{
		$data = $this->input->post();

		foreach ($data as $key => $value)
		{
			if( !(isset($value)) || ($value == "") || ($value == NULL) )
			{
				unset($data[$key]);
			}
		}

		$this->load->model("Reportes_model");

		$listado = $this->Reportes_model->get_grupos_electrogenos($data);
		
		$this->load->library("excel");

		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Reporte Grupos Electrógenos")
									->setSubject("Reporte")
									->setDescription("Reporte predeterminado desde el Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Grupos Electrógenos');
		
		$this->excel->getActiveSheet()->setCellValue('G1', 'Grupo Electrógeno');
		$this->excel->getActiveSheet()->mergeCells("G1:I1");
		
		$this->excel->getActiveSheet()->setCellValue('J1', 'Motor');
		$this->excel->getActiveSheet()->mergeCells("J1:L1");
		
		$this->excel->getActiveSheet()->setCellValue('M1', 'Generador');
		$this->excel->getActiveSheet()->mergeCells("M1:Q1");
		
		$this->excel->getActiveSheet()->setCellValue('R1', 'AVR');
		$this->excel->getActiveSheet()->mergeCells("R1:T1");
		
		$this->excel->getActiveSheet()->setCellValue('U1', 'Controlador GE');
		$this->excel->getActiveSheet()->mergeCells("U1:W1");
		
		$this->excel->getActiveSheet()->setCellValue('X1', 'Controlador TTA');
		$this->excel->getActiveSheet()->mergeCells("X1:Z1");

		$this->excel->getActiveSheet()->setCellValue('A2', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B2', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C2', 'Prioridad');
		$this->excel->getActiveSheet()->setCellValue('D2', 'Departamento');
		$this->excel->getActiveSheet()->setCellValue('E2', 'Provincia');
		$this->excel->getActiveSheet()->setCellValue('F2', 'Distrito');
		$this->excel->getActiveSheet()->setCellValue('G2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('H2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('I2', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('J2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('K2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('L2', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('M2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('N2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('O2', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('P2', 'Potencia (kW)');
		$this->excel->getActiveSheet()->setCellValue('Q2', 'Corriente (A)');
		$this->excel->getActiveSheet()->setCellValue('R2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('S2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('T2', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('U2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('V2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('W2', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('X2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('Y2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('Z2', 'Número de Serie');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A2:Z2")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '22b14c')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("G1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '1092c2')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("J1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '93a52e')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("M1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c8770b')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("R1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '00d225')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("U1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'ffff00')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("X1")
										->applyFromArray($styleArray);
		
		$i = 3;
		foreach ($listado as $fila)
		{
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['codigo']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['nombre_completo']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, "P".$fila['prioridad']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['departamento']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['provincia']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['distrito']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['tipo_grupo_electrogeno']['marca']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['tipo_grupo_electrogeno']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['numero_serie_grupo_electrogeno']);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $fila['tipo_motor_ge']['marca']);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $fila['tipo_motor_ge']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $fila['numero_serie_motor']);
			$this->excel->getActiveSheet()->setCellValue('M'.$i, $fila['tipo_generador_ge']['marca']);
			$this->excel->getActiveSheet()->setCellValue('N'.$i, $fila['tipo_generador_ge']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('O'.$i, $fila['numero_serie_generador']);
			$this->excel->getActiveSheet()->setCellValue('P'.$i, $fila['tipo_generador_ge']['potencia']);
			$this->excel->getActiveSheet()->setCellValue('Q'.$i, $fila['tipo_generador_ge']['corriente']);
			$this->excel->getActiveSheet()->setCellValue('R'.$i, $fila['tipo_avr_ge']['marca']);
			$this->excel->getActiveSheet()->setCellValue('S'.$i, $fila['tipo_avr_ge']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('T'.$i, $fila['numero_serie_avr']);
			$this->excel->getActiveSheet()->setCellValue('U'.$i, $fila['tipo_controlador_ge']['marca']);
			$this->excel->getActiveSheet()->setCellValue('V'.$i, $fila['tipo_controlador_ge']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('W'.$i, $fila['numero_serie_controlador_ge']);
			$this->excel->getActiveSheet()->setCellValue('X'.$i, $fila['tipo_controlador_tta']['marca']);
			$this->excel->getActiveSheet()->setCellValue('Y'.$i, $fila['tipo_controlador_tta']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('Z'.$i, $fila['numero_serie_controlador_tta']);

			$i++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Grupos_Electrogenos_'.date("d-m-Y-H-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_e_antenas()
	{
		$data = $this->input->post();

		foreach ($data as $key => $value)
		{
			if( !(isset($value)) || ($value == "") || ($value == NULL) )
			{
				unset($data[$key]);
			}
		}

		$this->load->model("Reportes_model");

		$listado = $this->Reportes_model->get_e_antenas($data);
		
		$this->load->library("excel");

		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Reporte Antenas de Electrónica")
									->setSubject("Reporte")
									->setDescription("Reporte predeterminado desde el Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Antenas (Nodo B)');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Prioridad');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Departamento');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Provincia');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Distrito');
		$this->excel->getActiveSheet()->setCellValue('G1', 'NEFdn');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Antenna Device ID');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Antenna Device Type');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Date of Manufacture');
		$this->excel->getActiveSheet()->setCellValue('K1', 'BOM Code');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Manufacturer Data');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Antenna Model');
		$this->excel->getActiveSheet()->setCellValue('N1', 'Serial Number (Antenna Motors)');
		$this->excel->getActiveSheet()->setCellValue('O1', 'Unit Position');
		$this->excel->getActiveSheet()->setCellValue('P1', 'Vendor Name');
		$this->excel->getActiveSheet()->setCellValue('Q1', 'Hardware Version');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A1:Q1")
										->applyFromArray($styleArray);
										
		$i = 2;
		foreach ($listado as $fila)
		{
			if($fila["date_manufacture_physical"] != "0000-00-00")
			{
				$date_of_manufacture = date("d/m/Y", strtotime($fila["date_manufacture_physical"]));
			}
			else
			{
				$date_of_manufacture = (($fila["date_manufacture_m2000"] != "0000-00-00")? date("d/m/Y", strtotime($fila["date_manufacture_m2000"])) : "");
			}
			
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['codigo']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['nombre_completo']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, "P".$fila['prioridad']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['departamento']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['provincia']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['distrito']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['nefdn']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['antenna_device_id']);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['antenna_device_type']);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $date_of_manufacture);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $fila['bom_code']);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $fila['manufacturer_data']);
			$this->excel->getActiveSheet()->setCellValue('M'.$i, $fila['antenna_model']);
			$this->excel->getActiveSheet()->setCellValue('N'.$i, $fila['serial_number_motor']);
			$this->excel->getActiveSheet()->setCellValue('O'.$i, $fila['unit_position']);
			$this->excel->getActiveSheet()->setCellValue('P'.$i, $fila['vendor_name']);
			$this->excel->getActiveSheet()->setCellValue('Q'.$i, $fila['hardware_version']);

			$i++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Antenas_Nodo_B_'.date("d-m-Y-H-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_tarjetas_nodo_b($part)
	{
		$data = $this->input->post();

		foreach ($data as $key => $value)
		{
			if( !(isset($value)) || ($value == "") || ($value == NULL) )
			{
				unset($data[$key]);
			}
		}

		$this->load->model("Reportes_model");

		$listado = $this->Reportes_model->get_tarjetas_nodo_b($data,$part);
		
		$this->load->library("excel");
		
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Reporte Tarjetas Nodo B")
									->setSubject("Reporte")
									->setDescription("Reporte predeterminado desde el Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Tarjetas Nodo B');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Prioridad');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Departamento');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Provincia');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Distrito');
		$this->excel->getActiveSheet()->setCellValue('G1', 'NEFdn');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Board Name');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Board Type');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Date of Manufacture');
		$this->excel->getActiveSheet()->setCellValue('K1', 'BOM Code');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Manufacturer Data');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Serial Number');
		$this->excel->getActiveSheet()->setCellValue('N1', 'Software Version');
		$this->excel->getActiveSheet()->setCellValue('O1', 'Unit Position');
		$this->excel->getActiveSheet()->setCellValue('P1', 'Vendor Name');
		$this->excel->getActiveSheet()->setCellValue('Q1', 'Vendor Unit Family Type');
		$this->excel->getActiveSheet()->setCellValue('R1', 'Hardware Version');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A1:R1")
										->applyFromArray($styleArray);
										
		$i = 2;
		foreach ($listado as $fila)
		{
			$date_manufacture = !$fila["date_manufacture"]? "" : date("d/m/Y", strtotime($fila["date_manufacture"]));
			
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['codigo']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['nombre_completo']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, "P".$fila['prioridad']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['departamento']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['provincia']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['distrito']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['nefdn']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['board_name']);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['board_type']);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $date_manufacture);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $fila['bom_code']);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $fila['manufacturer_data']);
			$this->excel->getActiveSheet()->setCellValue('M'.$i, $fila['serial_number']);
			$this->excel->getActiveSheet()->setCellValue('N'.$i, $fila['software_version']);
			$this->excel->getActiveSheet()->setCellValue('O'.$i, $fila['unit_position']);
			$this->excel->getActiveSheet()->setCellValue('P'.$i, $fila['vendor_name']);
			$this->excel->getActiveSheet()->setCellValue('Q'.$i, $fila['vendor_unit_family_type']);
			$this->excel->getActiveSheet()->setCellValue('R'.$i, $fila['hardware_version']);

			$i++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		
		$parte = (count($data) > 0)? "" : "Parte_".($part+1)."_";

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Tarjetas_Nodo_B_'.$parte.date("d-m-Y-H-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_e_gabinetes()
	{
		$data = $this->input->post();

		foreach ($data as $key => $value)
		{
			if( !(isset($value)) || ($value == "") || ($value == NULL) )
			{
				unset($data[$key]);
			}
		}

		$this->load->model("Reportes_model");

		$listado = $this->Reportes_model->get_e_gabinetes($data);
		
		$this->load->library("excel");

		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Reporte Gabinetes Nodo B")
									->setSubject("Reporte")
									->setDescription("Reporte predeterminado desde el Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Gabinetes (Nodo B)');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Prioridad');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Departamento');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Provincia');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Distrito');
		$this->excel->getActiveSheet()->setCellValue('G1', 'NEFdn');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Model (Physical)');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Type (Physical)');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Date of Manufacture');
		$this->excel->getActiveSheet()->setCellValue('K1', 'BOM Code');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Manufacturer Data');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Rack Type (Physical)');
		$this->excel->getActiveSheet()->setCellValue('N1', 'Rack Type (M2000)');
		$this->excel->getActiveSheet()->setCellValue('O1', 'Serial Number');
		$this->excel->getActiveSheet()->setCellValue('P1', 'Vendor Name');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A1:P1")
										->applyFromArray($styleArray);
										
		$i = 2;
		foreach ($listado as $fila)
		{
			if($fila["date_manufacture_physical"] != "0000-00-00")
			{
				$date_of_manufacture = date("d/m/Y", strtotime($fila["date_manufacture_physical"]));
			}
			else
			{
				$date_of_manufacture = (($fila["date_manufacture_m2000"] != "0000-00-00")? date("d/m/Y", strtotime($fila["date_manufacture_m2000"])) : "");
			}
			
			if($fila["serial_number_physical"] != "")
			{
				$serial_number = $fila["serial_number_physical"];
			}
			else
			{
				$serial_number = $fila["serial_number_m2000"];
			}
			
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['codigo']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['nombre_completo']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, "P".$fila['prioridad']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['departamento']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['provincia']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['distrito']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['nefdn']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['model_physical']);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['type_physical']);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $date_of_manufacture);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $fila['bom_code']);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $fila['manufacturer_data']);
			$this->excel->getActiveSheet()->setCellValue('M'.$i, $fila['rack_type_physical']);
			$this->excel->getActiveSheet()->setCellValue('N'.$i, $fila['rack_type_m2000']);
			$this->excel->getActiveSheet()->setCellValue('O'.$i, $serial_number);
			$this->excel->getActiveSheet()->setCellValue('P'.$i, $fila['vendor_name']);

			$i++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Gabinetes_Nodo_B_'.date("d-m-Y-H-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_mw_tarjetas($part)
	{
		$data = $this->input->post();

		foreach ($data as $key => $value)
		{
			if( !(isset($value)) || ($value == "") || ($value == NULL) )
			{
				unset($data[$key]);
			}
		}

		$this->load->model("Reportes_model");

		$listado = $this->Reportes_model->get_mw_tarjetas($data,$part);
		
		$this->load->library("excel");
		
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Reporte Tarjetas Microondas")
									->setSubject("Reporte")
									->setDescription("Reporte predeterminado desde el Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Tarjetas Microondas');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Prioridad');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Departamento');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Provincia');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Distrito');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Board Type');
		$this->excel->getActiveSheet()->setCellValue('H1', 'NE Type');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Slot ID');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Hardware Version');
		$this->excel->getActiveSheet()->setCellValue('K1', 'NE ID');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Board Bar Code');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Board BOM Item');
		$this->excel->getActiveSheet()->setCellValue('N1', 'Description');
		$this->excel->getActiveSheet()->setCellValue('O1', 'Manufacture Date');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A1:O1")
										->applyFromArray($styleArray);
										
		$i = 2;
		foreach ($listado as $fila)
		{
			$manufacture_date = !$fila["manufacture_date"]? "" : date("d/m/Y", strtotime($fila["manufacture_date"]));
			
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['codigo']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['nombre_completo']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, "P".$fila['prioridad']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['departamento']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['provincia']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['distrito']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['board_type']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['ne_type']);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['slot_id']);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $fila['hardware_version']);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $fila['idu']['ne_id']);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $fila['board_bar_code']);
			$this->excel->getActiveSheet()->setCellValue('M'.$i, $fila['board_bom_item']);
			$this->excel->getActiveSheet()->setCellValue('N'.$i, $fila['description']);
			$this->excel->getActiveSheet()->setCellValue('O'.$i, $manufacture_date);

			$i++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		
		$parte = (count($data) > 0)? "" : "Parte_".($part+1)."_";

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Tarjetas_Microondas_'.$parte.date("d-m-Y-H-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_mw_odus()
	{
		$data = $this->input->post();

		foreach ($data as $key => $value)
		{
			if( !(isset($value)) || ($value == "") || ($value == NULL) )
			{
				unset($data[$key]);
			}
		}

		$this->load->model("Reportes_model");

		$listado = $this->Reportes_model->get_mw_odus($data);
		
		$this->load->library("excel");
		
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Reporte ODUs Microondas")
									->setSubject("Reporte")
									->setDescription("Reporte predeterminado desde el Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('ODUs Microondas');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Prioridad');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Departamento');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Provincia');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Distrito');
		$this->excel->getActiveSheet()->setCellValue('G1', 'NE Type');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Frequency (GHz)');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Range of Frequency Point (MHz)');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Produce Time');
		$this->excel->getActiveSheet()->setCellValue('K1', 'Factory Information');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Software Version');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A1:L1")
										->applyFromArray($styleArray);
										
		$i = 2;
		foreach ($listado as $fila)
		{
			$produce_time = !$fila["produce_time"]? "" : date("d/m/Y", strtotime($fila["produce_time"]));
			
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['codigo']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['nombre_completo']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, "P".$fila['prioridad']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['departamento']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['provincia']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['distrito']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['ne_type']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['frequency']);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['range_frequency']);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $produce_time);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $fila['factory_information']);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $fila['software_version']);

			$i++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_ODUs_Microondas_'.date("d-m-Y-H-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function reportes_predeterminados_wimax()
	{
		$data = $this->input->post();

		foreach ($data as $key => $value)
		{
			if( !(isset($value)) || ($value == "") || ($value == NULL) )
			{
				unset($data[$key]);
			}
		}

		$this->load->model("Reportes_model");

		$listado = $this->Reportes_model->get_wimax($data);
		
		$this->load->library("excel");
		
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Reporte Wimax")
									->setSubject("Reporte")
									->setDescription("Reporte predeterminado desde el Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Equipments Inventory');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Prioridad');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Departamento');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Provincia');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Distrito');
		$this->excel->getActiveSheet()->setCellValue('G1', 'BTS Model');
		$this->excel->getActiveSheet()->setCellValue('H1', 'WiMax Code');
		$this->excel->getActiveSheet()->setCellValue('I1', 'BTS Name');
		$this->excel->getActiveSheet()->setCellValue('J1', 'BTS Number');
		$this->excel->getActiveSheet()->setCellValue('K1', 'IP Address');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Entity Type');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Entity Count');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A1:M1")
										->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->setAutoFilter('A1:M1');
										
		$i = 2;
		foreach ($listado["wimax"] as $fila)
		{
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['codigo']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['nombre_completo']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, "P".$fila['prioridad']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['departamento']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['provincia']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['distrito']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['bts_model']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['wimax_code']);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['bts_name']);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $fila['bts_number']);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $fila['ip_address']);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $fila['entity_type']);
			$this->excel->getActiveSheet()->setCellValue('M'.$i, $fila['entity_count']);

			$i++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

		$this->excel->createSheet();

		$csheet = 1;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Detailed Equipments Inventory');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C1', 'BTS Name');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Entity Type');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Entity Serial Number');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Entity Hardware Version Number');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Entity Hardware Revision Number');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Entity Operational Software Version Number');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Entity Boot Software Version Number');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							'wrap' => true
						));

		$this->excel->getActiveSheet()->getStyle("A1:I1")
										->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->setAutoFilter('A1:I1');
										
		$i = 2;
		foreach ($listado["wimax_detailed"] as $fila)
		{
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['codigo']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['nombre_completo']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $fila['bts_name']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['entity_type']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['entity_serial_number']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['entity_hardware_version_number']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['entity_hardware_revision_number']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['entity_operational_software_version_number']);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['entity_boot_software_version_number']);

			$i++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);

		$this->excel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Wimax_'.date("d-m-Y-H-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function reportes_predeterminados_iden()
	{
		$data = $this->input->post();

		foreach ($data as $key => $value)
		{
			if( !(isset($value)) || ($value == "") || ($value == NULL) )
			{
				unset($data[$key]);
			}
		}

		$this->load->model("Reportes_model");

		$listado = $this->Reportes_model->get_iden($data);
		
		$this->load->library("excel");
		
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Reporte iDEN")
									->setSubject("Reporte")
									->setDescription("Reporte predeterminado desde el Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('iDEN');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Prioridad');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Departamento');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Provincia');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Distrito');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Site Name');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Antennas');
		$this->excel->getActiveSheet()->setCellValue('I1', 'BRs');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Duplexor');
		$this->excel->getActiveSheet()->setCellValue('K1', 'QBRs');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Cabinet MSER');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Cabinet RF');
		$this->excel->getActiveSheet()->setCellValue('N1', 'Cabinet Control');
		$this->excel->getActiveSheet()->setCellValue('O1', 'RFN');
		$this->excel->getActiveSheet()->setCellValue('P1', 'EBTS Type');
		$this->excel->getActiveSheet()->setCellValue('Q1', 'iBSC Name');
		$this->excel->getActiveSheet()->setCellValue('R1', 'ACG ID (EBTS Controller)');
		$this->excel->getActiveSheet()->setCellValue('S1', 'Site IP Address');
		$this->excel->getActiveSheet()->setCellValue('T1', 'ACG Port');
		$this->excel->getActiveSheet()->setCellValue('U1', 'OMC Port');
		$this->excel->getActiveSheet()->setCellValue('V1', 'Link Status');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A1:V1")
										->applyFromArray($styleArray);
										
		$i = 2;
		foreach ($listado as $fila)
		{
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['codigo']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['nombre_completo']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, "P".$fila['prioridad']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['departamento']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['provincia']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['distrito']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['site_name']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['antennas']);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['brs']);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $fila['duplexor']);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $fila['qbrs']);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $fila['cabinet_mser']);
			$this->excel->getActiveSheet()->setCellValue('M'.$i, $fila['cabinet_rf']);
			$this->excel->getActiveSheet()->setCellValue('N'.$i, $fila['cabinet_control']);
			$this->excel->getActiveSheet()->setCellValue('O'.$i, $fila['rfn']);
			$this->excel->getActiveSheet()->setCellValue('P'.$i, $fila['ebts_type']);
			$this->excel->getActiveSheet()->setCellValue('Q'.$i, $fila['ibsc_name']);
			$this->excel->getActiveSheet()->setCellValue('R'.$i, $fila['acg_id']);
			$this->excel->getActiveSheet()->setCellValue('S'.$i, $fila['site_ip_address']);
			$this->excel->getActiveSheet()->setCellValue('T'.$i, $fila['acg_port']);
			$this->excel->getActiveSheet()->setCellValue('U'.$i, $fila['omc_port']);
			$this->excel->getActiveSheet()->setCellValue('V'.$i, $fila['link_status']);

			$i++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_iDEN_'.date("d-m-Y-H-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
	
	public function reportes_predeterminados_grupos_electrogenos_almacenados()
	{
		$data = $this->input->post();

		foreach ($data as $key => $value)
		{
			if( !(isset($value)) || ($value == "") || ($value == NULL) )
			{
				unset($data[$key]);
			}
		}

		$this->load->model("Reportes_model");

		$listado = $this->Reportes_model->get_grupos_electrogenos_almacenados($data);
		
		/*echo json_encode($listado);*/
		
		$this->load->library("excel");

		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Reporte Grupos Electrógenos Almacenados")
									->setSubject("Reporte")
									->setDescription("Reporte predeterminado desde el Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Grupos Electrógenos Almacenados');
		
		$this->excel->getActiveSheet()->setCellValue('C1', 'Grupo Electrógeno');
		$this->excel->getActiveSheet()->mergeCells("C1:E1");
		
		$this->excel->getActiveSheet()->setCellValue('F1', 'Motor');
		$this->excel->getActiveSheet()->mergeCells("F1:H1");
		
		$this->excel->getActiveSheet()->setCellValue('I1', 'Generador');
		$this->excel->getActiveSheet()->mergeCells("I1:M1");
		
		$this->excel->getActiveSheet()->setCellValue('N1', 'AVR');
		$this->excel->getActiveSheet()->mergeCells("N1:P1");
		
		$this->excel->getActiveSheet()->setCellValue('Q1', 'Controlador GE');
		$this->excel->getActiveSheet()->mergeCells("Q1:S1");
		
		$this->excel->getActiveSheet()->setCellValue('T1', 'Controlador TTA');
		$this->excel->getActiveSheet()->mergeCells("T1:V1");

		$this->excel->getActiveSheet()->setCellValue('A2', 'Proveniente');
		$this->excel->getActiveSheet()->setCellValue('B2', 'Almacenado en');
		$this->excel->getActiveSheet()->setCellValue('C2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('D2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('E2', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('F2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('G2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('H2', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('I2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('J2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('K2', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('L2', 'Potencia (kW)');
		$this->excel->getActiveSheet()->setCellValue('M2', 'Corriente (A)');
		$this->excel->getActiveSheet()->setCellValue('N2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('O2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('P2', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('Q2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('R2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('S2', 'Número de Serie');
		$this->excel->getActiveSheet()->setCellValue('T2', 'Marca');
		$this->excel->getActiveSheet()->setCellValue('U2', 'Modelo');
		$this->excel->getActiveSheet()->setCellValue('V2', 'Número de Serie');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A2:V2")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '22b14c')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("C1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '1092c2')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("F1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '93a52e')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("I1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c8770b')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("N1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '00d225')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("Q1")
										->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'	=> array(
							'bold'	=> true,
							'color'	=> array('rgb' => '000000'),
							'size' => 11,
							'name' => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'ffff00')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		
		$this->excel->getActiveSheet()->getStyle("T1")
										->applyFromArray($styleArray);
		
		$i = 3;
		foreach ($listado as $fila)
		{	
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['proveniente']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['almacen']['almacen']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $fila['tipo_grupo_electrogeno']['marca']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['tipo_grupo_electrogeno']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['numero_serie_grupo_electrogeno']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['tipo_motor_ge']['marca']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['tipo_motor_ge']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['numero_serie_motor']);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['tipo_generador_ge']['marca']);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $fila['tipo_generador_ge']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $fila['numero_serie_generador']);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $fila['tipo_generador_ge']['potencia']);
			$this->excel->getActiveSheet()->setCellValue('M'.$i, $fila['tipo_generador_ge']['corriente']);
			$this->excel->getActiveSheet()->setCellValue('N'.$i, $fila['tipo_avr_ge']['marca']);
			$this->excel->getActiveSheet()->setCellValue('O'.$i, $fila['tipo_avr_ge']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('P'.$i, $fila['numero_serie_avr']);
			$this->excel->getActiveSheet()->setCellValue('Q'.$i, $fila['tipo_controlador_ge']['marca']);
			$this->excel->getActiveSheet()->setCellValue('R'.$i, $fila['tipo_controlador_ge']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('S'.$i, $fila['numero_serie_controlador_ge']);
			$this->excel->getActiveSheet()->setCellValue('T'.$i, $fila['tipo_controlador_tta']['marca']);
			$this->excel->getActiveSheet()->setCellValue('U'.$i, $fila['tipo_controlador_tta']['modelo']);
			$this->excel->getActiveSheet()->setCellValue('V'.$i, $fila['numero_serie_controlador_tta']);

			$i++;
		}

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Grupos_Electrogenos_Almacenados_'.date("d-m-Y-H-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function reportes_personalizados_sitio_individual()
	{
		$data = $this->input->post();

		$this->load->model("Reportes_model");

		$listado = $this->Reportes_model->get_sitio_individual($data["sitio"]);

		/*echo json_encode($data);*/

		$this->load->library("excel");

		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Reporte ".$listado[0]["nombre_completo"])
									->setSubject("Reporte")
									->setDescription("Reporte predeterminado desde el Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle($listado[0]["nombre_completo"]);
		$activeSheet->getDefaultStyle()->getFont()->setSize(10);

		$activeSheet->setCellValue('A1', 'Infraestructura');
		$activeSheet->setCellValue('A3', 'POP');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => '000000'),
							'size'  => 12,
							'name'  => 'Verdana'
						)
					);

		$this->excel->getActiveSheet()->getStyle("A1")->applyFromArray($styleArray);

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => '000000'),
							'size'  => 10,
							'name'  => 'Verdana'
						)
					);

		$this->excel->getActiveSheet()->getStyle("A3")->applyFromArray($styleArray);

		$activeSheet->setCellValue('A4', 'Código')
					->setCellValue('B4', 'Nombre')
					->setCellValue('C4', 'Prioridad')
					->setCellValue('D4', 'Dirección')
					->setCellValue('E4', 'Departamento')
					->setCellValue('F4', 'Provincia')
					->setCellValue('G4', 'Distrito')
					->setCellValue('H4', 'Latitud')
					->setCellValue('I4', 'Longitud')
					->setCellValue('J4', 'Tipo de torre')
					->setCellValue('K4', 'Altura de torre (m)')
					->setCellValue('L4', 'Factor uso de torre (%)')
					->setCellValue('M4', 'Tipo de estación')
					->setCellValue('N4', 'Altura de edificación (m)')
					->setCellValue('O4', 'Mimetizado')
					->setCellValue('P4', 'Tipo mimetizado')
					->setCellValue('Q4', 'Ubicación de equipos')
					->setCellValue('R4', 'In-building')
					->setCellValue('S4', 'Coubicado')
					->setCellValue('T4', 'Coubicado en')
					->setCellValue('U4', 'Nombre sitio coubicador')
					->setCellValue('V4', 'Código torrera')
					->setCellValue('W4', 'Operador coubicado')
					->setCellValue('X4', 'Nombre sitio coubicante')
					->setCellValue('Y4', 'Dorsal')
					->setCellValue('Z4', 'Agregador')
					->setCellValue('AA4', 'Estado')
					->setCellValue('AB4', 'Fecha On Air')
					->setCellValue('AC4', 'Año de construcción')
					->setCellValue('AD4', 'Proyecto')
					->setCellValue('AE4', 'Contrata constructora')
					->setCellValue('AF4', 'Tipo de site para facturación')
					->setCellValue('AG4', 'Proveedor de mantenimiento')
					->setCellValue('AH4', 'Consideraciones de acceso')
					->setCellValue('AI4', 'Acceso libre 24h')
					->setCellValue('AJ4', 'Nivel de riesgo')
					->setCellValue('AK4', 'Consideraciones')
					->setCellValue('AL4', 'Código Wimax 2.5')
					->setCellValue('AM4', 'Código Wimax 3.5')
					->setCellValue('AN4', 'Código iDEN')
					->setCellValue('AO4', 'Código Agregador')
					->setCellValue('AP4', 'Código Repetidor')
					->setCellValue('AQ4', 'Código GUL')
					->setCellValue('AR4', 'Código LTE 700')
					->setCellValue('AS4', 'Fecha Wimax 2.5')
					->setCellValue('AT4', 'Fecha Wimax 3.5')
					->setCellValue('AU4', 'Fecha iDEN')
					->setCellValue('AV4', 'Fecha Agregador')
					->setCellValue('AW4', 'Fecha Repetidor')
					->setCellValue('AX4', 'Fecha GUL')
					->setCellValue('AY4', 'Fecha LTE 700')
					->setCellValue('AZ4', 'Servicios Wimax 2.5')
					->setCellValue('BA4', 'Servicios Wimax 3.5')
					->setCellValue('BB4', 'Servicios iDEN')
					->setCellValue('BC4', 'Servicios Agregador')
					->setCellValue('BD4', 'Servicios Repetidor')
					->setCellValue('BE4', 'Servicios GUL')
					->setCellValue('BF4', 'Servicios LTE 700')
					->setCellValue('BG4', 'CRM')
					->setCellValue('BH4', 'Supervisor')
					->setCellValue('BI4', 'Responsable(s)');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 10,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A4:BI4")
										->applyFromArray($styleArray);

		$i = 5;
		foreach ($listado as $fila)
		{
			$tipo_estacion = ($fila['greenfield_rooftop'])? "Rooftop" : "Greenfield" ;
			$altura_edificacion = ($fila['greenfield_rooftop'])? $fila['altura_edificacion'] : "N/A" ;
			$mimetizado = ($fila['camuflaje']['id'] == 1)? "NO" : "SI" ;
			$tipo_mimetizado = ($fila['camuflaje']['id'] == 1)? "N/A" : $fila['camuflaje']['camuflaje'] ;
			$in_building = ($fila['in_building'])? "SI" : "NO" ;
			$coubicado = ($fila['coubicador']['id'] == 1 )? "NO" : "SI" ;
			$dorsal = ($fila['dorsal'])? "SI" : "NO" ;
			$agregador = ($fila['agregador'])? "SI" : "NO" ;
			$estado = ($fila['estado'] == 2)? "ON Air" : "OFF Air" ;
			$acceso_libre_24h = ($fila['acceso_libre_24h'])? "SI" : "NO" ;

			$fechaWimax25 = ( isset($fila['wimax25']['fecha_servicio']) )? (((strtotime($fila['wimax25']['fecha_servicio']) + 68400) / 86400) + 25568) : "";
			$fechaWimax35 = ( isset($fila['wimax35']['fecha_servicio']) )? (((strtotime($fila['wimax35']['fecha_servicio']) + 68400) / 86400) + 25568) : "";
			$fechaIDEN = ( isset($fila['iDEN']['fecha_servicio']) )? (((strtotime($fila['iDEN']['fecha_servicio']) + 68400) / 86400) + 25568) : "";
			$fechaAgregador = ( isset($fila['Agregador']['fecha_servicio']) )? (((strtotime($fila['Agregador']['fecha_servicio']) + 68400) / 86400) + 25568) : "";
			$fechaRepetidor = ( isset($fila['Repetidor']['fecha_servicio']) )? (((strtotime($fila['Repetidor']['fecha_servicio']) + 68400) / 86400) + 25568) : "";
			$fechaGUL = ( isset($fila['GUL']['fecha_servicio']) )? (((strtotime($fila['GUL']['fecha_servicio']) + 68400) / 86400) + 25568) : "";
			$fechaLTE_700 = ( isset($fila['LTE_700']['fecha_servicio']) )? (((strtotime($fila['LTE_700']['fecha_servicio']) + 68400) / 86400) + 25568) : "";
			
			$activeSheet->setCellValue('A'.$i, $fila['codigo'])
						->setCellValue('B'.$i, $fila['nombre_completo'])
						->setCellValue('C'.$i, "P".$fila['prioridad'])
						->setCellValue('D'.$i, $fila['direccion'])
						->setCellValue('E'.$i, $fila['zona_peru']['departamento'])
						->setCellValue('F'.$i, $fila['zona_peru']['provincia'])
						->setCellValue('G'.$i, $fila['zona_peru']['distrito'])
						->setCellValue('H'.$i, $fila['latitud'])
						->setCellValue('I'.$i, $fila['longitud'])
						->setCellValue('J'.$i, $fila['tipo_torre']['tipo_torre'])
						->setCellValue('K'.$i, $fila['altura_torre'])
						->setCellValue('L'.$i, $fila['factor_uso_torre'])
						->setCellValue('M'.$i, $tipo_estacion)
						->setCellValue('N'.$i, $altura_edificacion)
						->setCellValue('O'.$i, $mimetizado)
						->setCellValue('P'.$i, $tipo_mimetizado)
						->setCellValue('Q'.$i, $fila['ubicacion_equipos']['ubicacion_equipos'])
						->setCellValue('R'.$i, $in_building)
						->setCellValue('S'.$i, $coubicado)
						->setCellValue('T'.$i, $fila['coubicador']['coubicador'])
						->setCellValue('U'.$i, $fila['nombre_sitio_coubicador'])
						->setCellValue('V'.$i, $fila['nombre_sitio_coubicador'])
						->setCellValue('W'.$i, $fila['operador_coubicante']['coubicante'])
						->setCellValue('X'.$i, $fila['nombre_sitio_coubicante'])
						->setCellValue('Y'.$i, $dorsal)
						->setCellValue('Z'.$i, $agregador)
						->setCellValue('AA'.$i, $estado)
						->setCellValue('AB'.$i, date("d/m/Y", strtotime($fila['fecha_estado_on_air'])))
						->setCellValue('AC'.$i, $fila['anio_construccion'])
						->setCellValue('AD'.$i, $fila['proyecto']['proyecto'])
						->setCellValue('AE'.$i, $fila['contrata_constructora']['contrata_constructora'])
						->setCellValue('AF'.$i, $fila['tipo_solucion']['tipo_solucion'])
						->setCellValue('AG'.$i, $fila['proveedor_mantenimiento']['proveedor_mantenimiento'])
						->setCellValue('AH'.$i, $fila['consideracion_acceso'])
						->setCellValue('AI'.$i, $acceso_libre_24h)
						->setCellValue('AJ'.$i, $fila['nivel_riesgo'])
						->setCellValue('AK'.$i, $fila['consideraciones'])
						->setCellValue('AL'.$i, $fila['wimax25']['codigo'])
						->setCellValue('AM'.$i, $fila['wimax35']['codigo'])
						->setCellValue('AN'.$i, $fila['iDEN']['codigo'])
						->setCellValue('AO'.$i, $fila['Agregador']['codigo'])
						->setCellValue('AP'.$i, $fila['Repetidor']['codigo'])
						->setCellValue('AQ'.$i, $fila['GUL']['codigo'])
						->setCellValue('AR'.$i, $fila['LTE_700']['codigo'])
						->setCellValue('AS'.$i, $fechaWimax25)
						->setCellValue('AT'.$i, $fechaWimax35)
						->setCellValue('AU'.$i, $fechaIDEN)
						->setCellValue('AV'.$i, $fechaAgregador)
						->setCellValue('AW'.$i, $fechaRepetidor)
						->setCellValue('AX'.$i, $fechaGUL)
						->setCellValue('AY'.$i, $fechaLTE_700)
						->setCellValue('AZ'.$i, $fila['wimax25']['servicios'])
						->setCellValue('BA'.$i, $fila['wimax35']['servicios'])
						->setCellValue('BB'.$i, $fila['iDEN']['servicios'])
						->setCellValue('BC'.$i, $fila['Agregador']['servicios'])
						->setCellValue('BD'.$i, $fila['Repetidor']['servicios'])
						->setCellValue('BE'.$i, $fila['GUL']['servicios'])
						->setCellValue('BF'.$i, $fila['LTE_700']['servicios'])
						->setCellValue('BG'.$i, $fila["crm"]["crm"])
						->setCellValue('BH'.$i, $fila["crm"]["supervisor"])
						->setCellValue('BI'.$i, $fila["oym_zonales"]["oym_zonales"]);

			$activeSheet->getStyle('AS'.$i)->getNumberFormat()->applyFromArray(
					array(
							'code' => PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY
						)
				);
			$activeSheet->getStyle('AT'.$i)->getNumberFormat()->applyFromArray(
					array(
							'code' => PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY
						)
				);
			$activeSheet->getStyle('AU'.$i)->getNumberFormat()->applyFromArray(
					array(
							'code' => PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY
						)
				);
			$activeSheet->getStyle('AV'.$i)->getNumberFormat()->applyFromArray(
					array(
							'code' => PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY
						)
				);
			$activeSheet->getStyle('AW'.$i)->getNumberFormat()->applyFromArray(
					array(
							'code' => PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY
						)
				);
			$activeSheet->getStyle('AX'.$i)->getNumberFormat()->applyFromArray(
					array(
							'code' => PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY
						)
				);
			$activeSheet->getStyle('AY'.$i)->getNumberFormat()->applyFromArray(
					array(
							'code' => PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY
						)
				);

			$i++;
		}

		$i++;
		$activeSheet->setCellValue('A'.$i, 'Contratos');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => '000000'),
							'size'  => 10,
							'name'  => 'Verdana'
						)
					);

		$this->excel->getActiveSheet()->getStyle("A".$i)->applyFromArray($styleArray);

		$i++;

		$this->excel->getActiveSheet()->setCellValue('A'.$i, 'ID Contrato');
		$this->excel->getActiveSheet()->setCellValue('B'.$i, 'Arrendador');
		$this->excel->getActiveSheet()->setCellValue('C'.$i, 'Fecha inicio contrato');
		$this->excel->getActiveSheet()->setCellValue('D'.$i, 'Fecha fin contrato');
		$this->excel->getActiveSheet()->setCellValue('E'.$i, 'Área piso (m2)');
		$this->excel->getActiveSheet()->setCellValue('F'.$i, 'Contactos');
		$this->excel->getActiveSheet()->setCellValue('G'.$i, 'Teléfonos');
		
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 10,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A".$i.":"."G".$i)
										->applyFromArray($styleArray);
										
		$i++;
		foreach ($listado as $fila)
		{
			$contacto_nombre = "";
			$telefono = "";
			
			foreach($fila['contratos'] as $key => $contrato)
			{
				foreach($contrato['contactos_contrato'] as $key_contacto => $contacto)
				{
					$contacto_nombre .= ($key_contacto > 0)? ", " : "";
					$contacto_nombre .= $contacto['contacto'];
					
					$telefonos = json_decode($contacto["telefono"],true);

					$telefono .= ($key_contacto > 0)? ", " : "";
					foreach ($telefonos as $j => $t)
					{
						$telefono .= $t["telefono"];
						if($j > 0){ $telefono .= " / "; }
					}
				}
				
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $contrato['id_contrato']);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $contrato['arrendador']);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, date("d/m/Y", strtotime($contrato['fecha_inicio'])));
				$this->excel->getActiveSheet()->setCellValue('D'.$i, date("d/m/Y", strtotime($contrato['fecha_fin'])));
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $contrato['area']);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, $contacto_nombre);
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $telefono);
				
				$i++;
			}
		}

		if(isset($data["Antenas_GUL"]))
		{
			$this->load->model('Antenas_gul_model');
			$listado_antenas_gul = $this->Antenas_gul_model->get_antenas_gul_sitio($data["sitio"]);

			$i++;
			$activeSheet->setCellValue('A'.$i, 'Antenas GUL');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
							)
						);

			$this->excel->getActiveSheet()->getStyle("A".$i)->applyFromArray($styleArray);

			$i++;
			$this->excel->getActiveSheet()->setCellValue('A'.$i, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('B'.$i, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('C'.$i, 'Sector');
			$this->excel->getActiveSheet()->setCellValue('D'.$i, 'Altura (m)');
			$this->excel->getActiveSheet()->setCellValue('E'.$i, 'Azimuth (°)');
			$this->excel->getActiveSheet()->setCellValue('F'.$i, 'Tilt mecánico (°)');
			$this->excel->getActiveSheet()->setCellValue('G'.$i, 'Tilt eléctrico (°)');
			$this->excel->getActiveSheet()->setCellValue('H'.$i, 'Número de serie');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'FFFFFF'),
								'size'  => 10,
								'name'  => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '6788D3')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));

			$this->excel->getActiveSheet()->getStyle("A".$i.":"."H".$i)
											->applyFromArray($styleArray);

			$i++;
			foreach($listado_antenas_gul as $fila)
			{
				$fila["info_telnet"] = json_decode($fila["info_telnet"],true);
			
				$sectores = "";
				$azimuths = "";
				$tilt_mecanicos = "";
				$tilt_electricos = "";
				
				if(count($fila["info_telnet"]) > 0)
				{
					foreach($fila["info_telnet"] as $key => $telnet)
					{
						$sectores .= ($key > 0 && $telnet["sector"])? ", " : "" ;
						$sectores .= $telnet["sector"];
						
						$azimuths .= ($key > 0 && $telnet["azimuth"])? ", " : "" ;
						$azimuths .= $telnet["azimuth"];
						
						$tilt_mecanicos .= ($key > 0)? ", " : "" ;
						$tilt_mecanicos .= $telnet["tilt_mecanico"];
						
						$tilt_electricos .= ($key > 0 && $telnet["tilt_electrico"])? ", " : "" ;
						$tilt_electricos .= $telnet["tilt_electrico"];
					}
				}
				
				$sector = ($fila['tipo_antena_gul']['marca'] != "TELNET")? $fila['sector'] : $sectores;
				$azimuth = ($fila['tipo_antena_gul']['marca'] != "TELNET")? $fila['azimuth'] : $azimuths;
				$tilt_mecanico = ($fila['tipo_antena_gul']['marca'] != "TELNET")? $fila['tilt_mecanico'] : $tilt_mecanicos;
				$tilt_electrico = ($fila['tipo_antena_gul']['marca'] != "TELNET")? $fila['tilt_electrico'] : $tilt_electricos;
				
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['tipo_antena_gul']['marca']);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['tipo_antena_gul']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $sector);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['altura']);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $azimuth);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, $tilt_mecanico);
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $tilt_electrico);
				$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['numero_serie']);

				$i++;
			}
		}

		if(isset($data["IDUs"]))
		{
			$this->load->model('Idus_model');
			$listado_idus = $this->Idus_model->get_idus_sitio($data["sitio"]);

			$i++;
			$activeSheet->setCellValue('A'.$i, 'Radios - IDUs');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
							)
						);

			$this->excel->getActiveSheet()->getStyle("A".$i)->applyFromArray($styleArray);

			$i++;
			$this->excel->getActiveSheet()->setCellValue('A'.$i, 'NE ID');
			$this->excel->getActiveSheet()->setCellValue('B'.$i, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('C'.$i, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('D'.$i, 'Número de serie');
			$this->excel->getActiveSheet()->setCellValue('E'.$i, 'Consumo de energía');
			$this->excel->getActiveSheet()->setCellValue('F'.$i, 'Capacidad de Conmutación (GHz)');
			$this->excel->getActiveSheet()->setCellValue('G'.$i, 'Peso (Kg)');
			$this->excel->getActiveSheet()->setCellValue('H'.$i, 'Dimensiones');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'FFFFFF'),
								'size'  => 10,
								'name'  => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '6788D3')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));

			$this->excel->getActiveSheet()->getStyle("A".$i.":"."H".$i)
											->applyFromArray($styleArray);

			$i++;
			foreach ($listado_idus as $fila)
			{
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['ne_id']);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['tipo_idu']['marca']);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $fila['tipo_idu']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['numero_serie']);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['tipo_idu']['consumo_energia']);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['tipo_idu']['capacidad_conmutacion']);
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['tipo_idu']['peso']);
				$this->excel->getActiveSheet()->setCellValue('H'.$i, $fila['tipo_idu']['dimensiones']);

				$i++;
			}
		}

		if(isset($data["Enlaces"]))
		{
			$this->load->model('Enlaces_model');
			$listado_enlaces = $this->Enlaces_model->get_enlaces_sitio($data["sitio"]);

			$i++;
			$activeSheet->setCellValue('A'.$i, 'Enlaces Microondas');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
							)
						);

			$this->excel->getActiveSheet()->getStyle("A".$i)->applyFromArray($styleArray);

			$i++;
			$i2_enlaces = $i+1;
			$i3_enlaces = $i2_enlaces+1;

			$this->excel->getActiveSheet()->setCellValue('A'.$i, 'Near End');
			$this->excel->getActiveSheet()->mergeCells("A".$i.":"."C".$i2_enlaces);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, 'Far End');
			$this->excel->getActiveSheet()->mergeCells("D".$i.":"."F".$i2_enlaces);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, 'Near End');
			$this->excel->getActiveSheet()->mergeCells("G".$i.":"."J".$i2_enlaces);
			$this->excel->getActiveSheet()->setCellValue('K'.$i, 'Far End');
			$this->excel->getActiveSheet()->mergeCells("K".$i.":"."N".$i2_enlaces);
			$this->excel->getActiveSheet()->setCellValue('O'.$i, 'Near End');
			$this->excel->getActiveSheet()->mergeCells("O".$i.":"."S".$i);
			$this->excel->getActiveSheet()->setCellValue('O'.$i2_enlaces, 'Antena(s)');
			$this->excel->getActiveSheet()->mergeCells("O".$i2_enlaces.":"."S".$i2_enlaces);
			$this->excel->getActiveSheet()->setCellValue('T'.$i, 'Far End');
			$this->excel->getActiveSheet()->mergeCells("T".$i.":"."X".$i);
			$this->excel->getActiveSheet()->setCellValue('T'.$i2_enlaces, 'Antena(s)');
			$this->excel->getActiveSheet()->mergeCells("T".$i2_enlaces.":"."X".$i2_enlaces);
			$this->excel->getActiveSheet()->setCellValue('Y'.$i, 'Configuración del Enlace');
			$this->excel->getActiveSheet()->mergeCells("Y".$i.":"."AG".$i2_enlaces);
			
			$this->excel->getActiveSheet()->setCellValue('A'.$i3_enlaces, 'Código');
			$this->excel->getActiveSheet()->setCellValue('B'.$i3_enlaces, 'Nombre');
			$this->excel->getActiveSheet()->setCellValue('C'.$i3_enlaces, 'IDU ID');
			$this->excel->getActiveSheet()->setCellValue('D'.$i3_enlaces, 'Código');
			$this->excel->getActiveSheet()->setCellValue('E'.$i3_enlaces, 'Nombre');
			$this->excel->getActiveSheet()->setCellValue('F'.$i3_enlaces, 'IDU ID');
			$this->excel->getActiveSheet()->setCellValue('G'.$i3_enlaces, 'ODU');
			$this->excel->getActiveSheet()->setCellValue('H'.$i3_enlaces, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('I'.$i3_enlaces, 'Ftx (kHz)');
			$this->excel->getActiveSheet()->setCellValue('J'.$i3_enlaces, 'Ptx (dBm)');
			$this->excel->getActiveSheet()->setCellValue('K'.$i3_enlaces, 'ODU');
			$this->excel->getActiveSheet()->setCellValue('L'.$i3_enlaces, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('M'.$i3_enlaces, 'Ftx (kHz)');
			$this->excel->getActiveSheet()->setCellValue('N'.$i3_enlaces, 'Ptx (dBm)');
			$this->excel->getActiveSheet()->setCellValue('O'.$i3_enlaces, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('P'.$i3_enlaces, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('Q'.$i3_enlaces, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('R'.$i3_enlaces, 'Altura (m)');
			$this->excel->getActiveSheet()->setCellValue('S'.$i3_enlaces, 'Azimuth (°)');
			$this->excel->getActiveSheet()->setCellValue('T'.$i3_enlaces, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('U'.$i3_enlaces, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('V'.$i3_enlaces, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('W'.$i3_enlaces, 'Altura (m)');
			$this->excel->getActiveSheet()->setCellValue('X'.$i3_enlaces, 'Azimuth (°)');
			$this->excel->getActiveSheet()->setCellValue('Y'.$i3_enlaces, 'Polaridad');
			$this->excel->getActiveSheet()->setCellValue('Z'.$i3_enlaces, 'Configuración');
			$this->excel->getActiveSheet()->setCellValue('AA'.$i3_enlaces, 'Ancho de banda/BW (MHz)');
			$this->excel->getActiveSheet()->setCellValue('AB'.$i3_enlaces, 'Modulación');
			$this->excel->getActiveSheet()->setCellValue('AC'.$i3_enlaces, 'Capacidad (Mbit/s)');
			$this->excel->getActiveSheet()->setCellValue('AD'.$i3_enlaces, 'Modulación Adaptativa (AM)');
			$this->excel->getActiveSheet()->setCellValue('AE'.$i3_enlaces, 'Modulación AM');
			$this->excel->getActiveSheet()->setCellValue('AF'.$i3_enlaces, 'Capacidad AM (Mbit/s)');
			$this->excel->getActiveSheet()->setCellValue('AG'.$i3_enlaces, 'Banda (GHz)');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '22b14c')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
								'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							),
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
									'color' => array('rgb' => '000000')
								)
							));

			$this->excel->getActiveSheet()->getStyle("A".$i.":"."C".$i2_enlaces)
											->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle("G".$i.":"."J".$i2_enlaces)
											->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle("O".$i.":"."S".$i)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => 'ff0000')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
								'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							),
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
									'color' => array('rgb' => '000000')
								)
							));

			$this->excel->getActiveSheet()->getStyle("D".$i.":"."F".$i2_enlaces)
											->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle("K".$i.":"."N".$i2_enlaces)
											->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle("T".$i.":"."X".$i)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'FFFFFF'),
								'size'  => 10,
								'name'  => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '6788D3')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
								'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
								'wrap' => true
							),
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
									'color' => array('rgb' => '000000')
								)
							));

			$this->excel->getActiveSheet()->getStyle("A".$i3_enlaces.":"."AG".$i3_enlaces)
											->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle("Y".$i.":"."AG".$i2_enlaces)
											->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle("O".$i2_enlaces.":"."S".$i2_enlaces)
											->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle("T".$i2_enlaces.":"."X".$i2_enlaces)
											->applyFromArray($styleArray);

			$i3_enlaces++;
			foreach ($listado_enlaces as $fila)
			{
				$fila["odus_ne"] = array();
				$fila["odus_fe"] = array();
				$fila["antenas_mw_ne"] = array();
				$fila["antenas_mw_fe"] = array();
				
				foreach($fila["odus_enlaces"] as &$odu)
				{
					if($odu["idu"] == $fila["idu_1"]["id"])
					{
						array_push($fila["odus_ne"],$odu);
					}
					elseif($odu["idu"] == $fila["idu_2"]["id"])
					{
						array_push($fila["odus_fe"],$odu);
					}
				}
				unset($fila["odus_enlaces"]);
				
				foreach($fila["antenas_mw_enlaces"] as &$antena)
				{
					if($antena["idu"] == $fila["idu_1"]["id"])
					{
						array_push($fila["antenas_mw_ne"],$antena);
					}
					elseif($antena["idu"] == $fila["idu_2"]["id"])
					{
						array_push($fila["antenas_mw_fe"],$antena);
					}
				}
				unset($fila["antenas_mw_enlaces"]);

				if(count($fila["odus_ne"]) > 0)
				{
					$tipo_odu_ne = $fila["odus_ne"][0]["tipo_odu"]["tipo"];
					$ftx_odu_ne = $fila["odus_ne"][0]["ftx"];
					$ptx_odu_ne = $fila["odus_ne"][0]["ptx"];
					$ns_odu_ne = "";
					
					foreach($fila["odus_ne"] as $key => $odu)
					{
						$ns_odu_ne .= ($key > 0)? ", " : "" ;
						$ns_odu_ne .= $odu['numero_serie'];
					}
				}
				else
				{
					$tipo_odu_ne = "";
					$ftx_odu_ne = "";
					$ptx_odu_ne = "";
					$ns_odu_ne = "";
				}
				
				if(count($fila["odus_fe"]) > 0)
				{
					$tipo_odu_fe = $fila["odus_fe"][0]["tipo_odu"]["tipo"];
					$ftx_odu_fe = $fila["odus_fe"][0]["ftx"];
					$ptx_odu_fe = $fila["odus_fe"][0]["ptx"];
					$ns_odu_fe = "";
					
					foreach($fila["odus_fe"] as $key => $odu)
					{
						$ns_odu_fe .= ($key > 0)? ", " : "" ;
						$ns_odu_fe .= $odu['numero_serie'];
					}
				}
				else
				{
					$tipo_odu_fe = "";
					$ftx_odu_fe = "";
					$ptx_odu_fe = "";
					$ns_odu_fe = "";
				}
				
				$marca_antena_ne = "";
				$modelo_antena_ne = "";
				$ns_antena_ne = "";
				$altura_antena_ne = "";
				$azimuth_antena_ne = "";
				
				foreach($fila["antenas_mw_ne"] as $key => $antena)
				{
					$marca_antena_ne .= ($key > 0)? ", " : "" ;
					$marca_antena_ne .= $antena["tipo_antena_mw"]["marca"];
					
					$modelo_antena_ne .= ($key > 0)? ", " : "" ;
					$modelo_antena_ne .= $antena["tipo_antena_mw"]["modelo"];
					
					$ns_antena_ne .= ($key > 0)? ", " : "" ;
					$ns_antena_ne .= $antena["numero_serie"];
					
					$altura_antena_ne .= ($key > 0)? ", " : "" ;
					$altura_antena_ne .= $antena["altura"];
					
					$azimuth_antena_ne .= ($key > 0)? ", " : "" ;
					$azimuth_antena_ne .= $antena["azimuth"];
				}
				
				$marca_antena_fe = "";
				$modelo_antena_fe = "";
				$ns_antena_fe = "";
				$altura_antena_fe = "";
				$azimuth_antena_fe = "";
				
				foreach($fila["antenas_mw_fe"] as $key => $antena)
				{
					$marca_antena_fe .= ($key > 0)? ", " : "" ;
					$marca_antena_fe .= $antena["tipo_antena_mw"]["marca"];
					
					$modelo_antena_fe .= ($key > 0)? ", " : "" ;
					$modelo_antena_fe .= $antena["tipo_antena_mw"]["modelo"];
					
					$ns_antena_fe .= ($key > 0)? ", " : "" ;
					$ns_antena_fe .= $antena["numero_serie"];
					
					$altura_antena_fe .= ($key > 0)? ", " : "" ;
					$altura_antena_fe .= $antena["altura"];
					
					$azimuth_antena_fe .= ($key > 0)? ", " : "" ;
					$azimuth_antena_fe .= $antena["azimuth"];
				}
				
				switch($fila["polaridad"])
				{
					case 1:
						$polaridad = "V";
						break;
					case 2:
						$polaridad = "H";
						break;
					case 3:
						$polaridad = "V+H";
						break;
				}
				
				switch($fila["modulacion_adaptativa"])
				{
					case 0:
						$modulacion_adaptativa = "NO";
						break;
					case 1:
						$modulacion_adaptativa = "SI";
						break;
				}
				
				switch($fila["capacidad_am"])
				{
					case 0:
						$capacidad_am = "N/A";
						break;
					default:
						$capacidad_am = $fila["capacidad_am"];
				}
				
				$this->excel->getActiveSheet()->setCellValue('A'.$i3_enlaces, $fila['idu_1']['sitio']['codigo']);
				$this->excel->getActiveSheet()->setCellValue('B'.$i3_enlaces, $fila['idu_1']['sitio']['nombre_completo']);
				$this->excel->getActiveSheet()->setCellValue('C'.$i3_enlaces, $fila['idu_1']['ne_id']);
				$this->excel->getActiveSheet()->setCellValue('D'.$i3_enlaces, $fila['idu_2']['sitio']['codigo']);
				$this->excel->getActiveSheet()->setCellValue('E'.$i3_enlaces, $fila['idu_2']['sitio']['nombre_completo']);
				$this->excel->getActiveSheet()->setCellValue('F'.$i3_enlaces, $fila['idu_2']['ne_id']);
				$this->excel->getActiveSheet()->setCellValue('G'.$i3_enlaces, $tipo_odu_ne);
				$this->excel->getActiveSheet()->setCellValue('H'.$i3_enlaces, $ns_odu_ne);
				$this->excel->getActiveSheet()->setCellValue('I'.$i3_enlaces, $ftx_odu_ne);
				$this->excel->getActiveSheet()->setCellValue('J'.$i3_enlaces, $ptx_odu_ne);
				$this->excel->getActiveSheet()->setCellValue('K'.$i3_enlaces, $tipo_odu_fe);
				$this->excel->getActiveSheet()->setCellValue('L'.$i3_enlaces, $ns_odu_fe);
				$this->excel->getActiveSheet()->setCellValue('M'.$i3_enlaces, $ftx_odu_fe);
				$this->excel->getActiveSheet()->setCellValue('N'.$i3_enlaces, $ptx_odu_fe);
				$this->excel->getActiveSheet()->setCellValue('O'.$i3_enlaces, $marca_antena_ne);
				$this->excel->getActiveSheet()->setCellValue('P'.$i3_enlaces, $modelo_antena_ne);
				$this->excel->getActiveSheet()->setCellValue('Q'.$i3_enlaces, $ns_antena_ne);
				$this->excel->getActiveSheet()->setCellValue('R'.$i3_enlaces, $altura_antena_ne);
				$this->excel->getActiveSheet()->setCellValue('S'.$i3_enlaces, $azimuth_antena_ne);
				$this->excel->getActiveSheet()->setCellValue('T'.$i3_enlaces, $marca_antena_fe);
				$this->excel->getActiveSheet()->setCellValue('U'.$i3_enlaces, $modelo_antena_fe);
				$this->excel->getActiveSheet()->setCellValue('V'.$i3_enlaces, $ns_antena_fe);
				$this->excel->getActiveSheet()->setCellValue('W'.$i3_enlaces, $altura_antena_fe);
				$this->excel->getActiveSheet()->setCellValue('X'.$i3_enlaces, $azimuth_antena_fe);
				$this->excel->getActiveSheet()->setCellValue('Y'.$i3_enlaces, $polaridad);
				$this->excel->getActiveSheet()->setCellValue('Z'.$i3_enlaces, $fila['configuracion']);
				$this->excel->getActiveSheet()->setCellValue('AA'.$i3_enlaces, $fila['ancho_banda']);
				$this->excel->getActiveSheet()->setCellValue('AB'.$i3_enlaces, $fila['modulacion']);
				$this->excel->getActiveSheet()->setCellValue('AC'.$i3_enlaces, $fila['capacidad']);
				$this->excel->getActiveSheet()->setCellValue('AD'.$i3_enlaces, $modulacion_adaptativa);
				$this->excel->getActiveSheet()->setCellValue('AE'.$i3_enlaces, $fila['modulacion_am']);
				$this->excel->getActiveSheet()->setCellValue('AF'.$i3_enlaces, $capacidad_am);
				$this->excel->getActiveSheet()->setCellValue('AG'.$i3_enlaces, $fila['banda']);

				$i3_enlaces++;
			}
		}

		if(isset($data["Grupos_Electrógenos"]) || isset($data["Plantas_Rectificadoras"]) || isset($data["Bancos_de_Baterías"]) || isset($data["Suministros"]) || isset($data["Líneas_Eléctricas"]) || isset($data["Tanques_de_Combustible"]) || isset($data["Aires_Acondicionados"]))
		{
			$i = isset($i3_enlaces)? $i3_enlaces+2 : $i+2;
			$activeSheet->setCellValue('A'.$i, 'Energía');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 12,
								'name'  => 'Verdana'
							)
						);

			$this->excel->getActiveSheet()->getStyle("A".$i)->applyFromArray($styleArray);
		}

		if(isset($data["Grupos_Electrógenos"]))
		{
			$this->load->model('Grupos_electrogenos_model');
			$listado_grupos_electrogenos = $this->Grupos_electrogenos_model->get_grupos_electrogenos_sitio($data["sitio"]);

			$i = $i+2;
			$activeSheet->setCellValue('A'.$i, 'Grupos Electrógenos');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
							)
						);

			$this->excel->getActiveSheet()->getStyle("A".$i)->applyFromArray($styleArray);

			$i++;
			$i2_grupos = $i+1;

			$this->excel->getActiveSheet()->setCellValue('A'.$i, 'Grupo Electrógeno');
			$this->excel->getActiveSheet()->mergeCells("A".$i.":"."C".$i);
			
			$this->excel->getActiveSheet()->setCellValue('D'.$i, 'Motor');
			$this->excel->getActiveSheet()->mergeCells("D".$i.":"."F".$i);
			
			$this->excel->getActiveSheet()->setCellValue('G'.$i, 'Generador');
			$this->excel->getActiveSheet()->mergeCells("G".$i.":"."K".$i);
			
			$this->excel->getActiveSheet()->setCellValue('L'.$i, 'AVR');
			$this->excel->getActiveSheet()->mergeCells("L".$i.":"."N".$i);
			
			$this->excel->getActiveSheet()->setCellValue('O'.$i, 'Controlador GE');
			$this->excel->getActiveSheet()->mergeCells("O".$i.":"."Q".$i);
			
			$this->excel->getActiveSheet()->setCellValue('R'.$i, 'Controlador TTA');
			$this->excel->getActiveSheet()->mergeCells("R".$i.":"."T".$i);

			$this->excel->getActiveSheet()->setCellValue('A'.$i2_grupos, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('B'.$i2_grupos, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('C'.$i2_grupos, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('D'.$i2_grupos, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('E'.$i2_grupos, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('F'.$i2_grupos, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('G'.$i2_grupos, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('H'.$i2_grupos, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('I'.$i2_grupos, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('J'.$i2_grupos, 'Potencia (kW)');
			$this->excel->getActiveSheet()->setCellValue('K'.$i2_grupos, 'Corriente (A)');
			$this->excel->getActiveSheet()->setCellValue('L'.$i2_grupos, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('M'.$i2_grupos, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('N'.$i2_grupos, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('O'.$i2_grupos, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('P'.$i2_grupos, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('Q'.$i2_grupos, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('R'.$i2_grupos, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('S'.$i2_grupos, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('T'.$i2_grupos, 'Número de Serie');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'FFFFFF'),
								'size'  => 10,
								'name'  => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '6788D3')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));

			$this->excel->getActiveSheet()->getStyle("A".$i2_grupos.":"."T".$i2_grupos)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'	=> array(
								'bold'	=> true,
								'color'	=> array('rgb' => '000000'),
								'size' => 10,
								'name' => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '22b14c')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));
			
			$this->excel->getActiveSheet()->getStyle("A".$i)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'	=> array(
								'bold'	=> true,
								'color'	=> array('rgb' => '000000'),
								'size' => 10,
								'name' => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '1092c2')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));
			
			$this->excel->getActiveSheet()->getStyle("D".$i)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'	=> array(
								'bold'	=> true,
								'color'	=> array('rgb' => '000000'),
								'size' => 10,
								'name' => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '93a52e')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));
			
			$this->excel->getActiveSheet()->getStyle("G".$i)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'	=> array(
								'bold'	=> true,
								'color'	=> array('rgb' => '000000'),
								'size' => 10,
								'name' => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => 'c8770b')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));
			
			$this->excel->getActiveSheet()->getStyle("L".$i)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'	=> array(
								'bold'	=> true,
								'color'	=> array('rgb' => '000000'),
								'size' => 10,
								'name' => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '00d225')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));
			
			$this->excel->getActiveSheet()->getStyle("O".$i)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'	=> array(
								'bold'	=> true,
								'color'	=> array('rgb' => '000000'),
								'size' => 10,
								'name' => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => 'ffff00')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));
			
			$this->excel->getActiveSheet()->getStyle("R".$i)
											->applyFromArray($styleArray);

			$i2_grupos++;
			foreach ($listado_grupos_electrogenos as $fila)
			{
				$this->excel->getActiveSheet()->setCellValue('A'.$i2_grupos, $fila['tipo_grupo_electrogeno']['marca']);
				$this->excel->getActiveSheet()->setCellValue('B'.$i2_grupos, $fila['tipo_grupo_electrogeno']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('C'.$i2_grupos, $fila['numero_serie_grupo_electrogeno']);
				$this->excel->getActiveSheet()->setCellValue('D'.$i2_grupos, $fila['tipo_motor_ge']['marca']);
				$this->excel->getActiveSheet()->setCellValue('E'.$i2_grupos, $fila['tipo_motor_ge']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('F'.$i2_grupos, $fila['numero_serie_motor']);
				$this->excel->getActiveSheet()->setCellValue('G'.$i2_grupos, $fila['tipo_generador_ge']['marca']);
				$this->excel->getActiveSheet()->setCellValue('H'.$i2_grupos, $fila['tipo_generador_ge']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('I'.$i2_grupos, $fila['numero_serie_generador']);
				$this->excel->getActiveSheet()->setCellValue('J'.$i2_grupos, $fila['tipo_generador_ge']['potencia']);
				$this->excel->getActiveSheet()->setCellValue('K'.$i2_grupos, $fila['tipo_generador_ge']['corriente']);
				$this->excel->getActiveSheet()->setCellValue('L'.$i2_grupos, $fila['tipo_avr_ge']['marca']);
				$this->excel->getActiveSheet()->setCellValue('M'.$i2_grupos, $fila['tipo_avr_ge']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('N'.$i2_grupos, $fila['numero_serie_avr']);
				$this->excel->getActiveSheet()->setCellValue('O'.$i2_grupos, $fila['tipo_controlador_ge']['marca']);
				$this->excel->getActiveSheet()->setCellValue('P'.$i2_grupos, $fila['tipo_controlador_ge']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('Q'.$i2_grupos, $fila['numero_serie_controlador_ge']);
				$this->excel->getActiveSheet()->setCellValue('R'.$i2_grupos, $fila['tipo_controlador_tta']['marca']);
				$this->excel->getActiveSheet()->setCellValue('S'.$i2_grupos, $fila['tipo_controlador_tta']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('T'.$i2_grupos, $fila['numero_serie_controlador_tta']);

				$i2_grupos++;
			}
		}

		$i = isset($i2_grupos)? $i2_grupos : $i;

		if(isset($data["Plantas_Rectificadoras"]))
		{
			$this->load->model('Rectificadores_model');
			$listado_rectificadores = $this->Rectificadores_model->get_controlador_rectif_sitio($data["sitio"]);

			$i++;
			$activeSheet->setCellValue('A'.$i, 'Plantas Rectificadoras');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
							)
						);

			$this->excel->getActiveSheet()->getStyle("A".$i)->applyFromArray($styleArray);

			$i++;
			$this->excel->getActiveSheet()->setCellValue('A'.$i, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('B'.$i, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('C'.$i, 'Controlador');
			$this->excel->getActiveSheet()->setCellValue('D'.$i, 'Capacidad Instalada (kW)');
			$this->excel->getActiveSheet()->setCellValue('E'.$i, 'Capacidad Máxima (kW)');
			$this->excel->getActiveSheet()->setCellValue('F'.$i, 'Módulos Instalados');
			$this->excel->getActiveSheet()->setCellValue('G'.$i, 'Fecha de puesta en servicio');
			$this->excel->getActiveSheet()->setCellValue('H'.$i, 'Números de serie');
			$this->excel->getActiveSheet()->setCellValue('I'.$i, 'Energización');
			$this->excel->getActiveSheet()->setCellValue('J'.$i, 'Ubicación');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'FFFFFF'),
								'size'  => 10,
								'name'  => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '6788D3')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));

			$this->excel->getActiveSheet()->getStyle("A".$i.":"."J".$i)
											->applyFromArray($styleArray);

			$i++;
			foreach ($listado_rectificadores as $fila)
			{
				$mod_instalados = count($fila['modulos_rectif']);
				
				$numeros_serie = "";
				$capacidad = 0;
				foreach ($fila['modulos_rectif'] as $key => $modulo_rectif)
				{
					$capacidad += (Float)$modulo_rectif['tipo_modulos_rectif']['capacidad'];

					$numeros_serie .= ($key > 0)? ", " : "" ;
					$numeros_serie .= $modulo_rectif['numero_serie'];
				}

				$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['tipo_controlador_rectif']['marca']);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['modulos_rectif'][0]['tipo_modulos_rectif']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $fila['tipo_controlador_rectif']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $capacidad);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['capacidad_maxima']);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, $mod_instalados);
				$this->excel->getActiveSheet()->setCellValue('G'.$i, date("d/m/Y", strtotime($fila['anio_servicio'])));
				$this->excel->getActiveSheet()->setCellValue('H'.$i, $numeros_serie);
				$this->excel->getActiveSheet()->setCellValue('I'.$i, $fila['energizacion_controlador_rectif']['energizacion_controlador_rectif']);
				$this->excel->getActiveSheet()->setCellValue('J'.$i, $fila['ubicacion_controlador_rectif']['ubicacion_controlador_rectif']);

				$i++;
			}
		}

		if(isset($data["Bancos_de_Baterías"]))
		{
			$this->load->model('Bancos_baterias_model');
			$listado_bancos_baterias = $this->Bancos_baterias_model->get_bancos_baterias_sitio_resumen($data["sitio"]);

			$i++;
			$activeSheet->setCellValue('A'.$i, 'Bancos de Baterías');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
							)
						);

			$this->excel->getActiveSheet()->getStyle("A".$i)->applyFromArray($styleArray);

			$i++;
			$this->excel->getActiveSheet()->setCellValue('A'.$i, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('B'.$i, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('C'.$i, 'Capacidad (Ah)');
			$this->excel->getActiveSheet()->setCellValue('D'.$i, 'Cantidad de celdas');
			$this->excel->getActiveSheet()->setCellValue('E'.$i, 'Fecha de instalación');
			$this->excel->getActiveSheet()->setCellValue('F'.$i, 'Números de serie');
			$this->excel->getActiveSheet()->setCellValue('G'.$i, 'Energización del rectificador');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'FFFFFF'),
								'size'  => 10,
								'name'  => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '6788D3')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));

			$this->excel->getActiveSheet()->getStyle("A".$i.":"."G".$i)
											->applyFromArray($styleArray);

			$i++;
			foreach ($listado_bancos_baterias as $fila)
			{
				$numeros_serie = "";
				foreach($fila["celdas_controlador_rectif_bancos_baterias"] as $key => $celda)
				{
					$numeros_serie .= ($key > 0)? ", " : "" ;
					$numeros_serie .= $celda['numero_serie'];
				}

				$fecha_instalacion = !$fila['fecha_instalacion']? "" : date("d/m/Y", strtotime($fila['fecha_instalacion']));
				
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['tipo_bancos_baterias']['marca']);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['tipo_bancos_baterias']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $fila['tipo_bancos_baterias']['capacidad']);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['tipo_bancos_baterias']['celdas']);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $fecha_instalacion);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, $numeros_serie);
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['energizacion_controlador_rectif']);

				$i++;
			}
		}

		if(isset($data["Suministros"]))
		{
			$this->load->model('Suministros_model');
			$listado_suministros = $this->Suministros_model->get_suministros_sitio($data["sitio"]);

			$i++;
			$activeSheet->setCellValue('A'.$i, 'Suministros');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
							)
						);

			$this->excel->getActiveSheet()->getStyle("A".$i)->applyFromArray($styleArray);

			$i++;
			$this->excel->getActiveSheet()->setCellValue('A'.$i, 'Concesionaria');
			$this->excel->getActiveSheet()->setCellValue('B'.$i, 'Conexión');
			$this->excel->getActiveSheet()->setCellValue('C'.$i, 'Opción tarifaria');
			$this->excel->getActiveSheet()->setCellValue('D'.$i, 'Sistema');
			$this->excel->getActiveSheet()->setCellValue('E'.$i, 'Suministro');
			$this->excel->getActiveSheet()->setCellValue('F'.$i, 'Potencia contratada (KW)');
			$this->excel->getActiveSheet()->setCellValue('G'.$i, 'Nivel de tensión (KV)');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'FFFFFF'),
								'size'  => 10,
								'name'  => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '6788D3')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));

			$this->excel->getActiveSheet()->getStyle("A".$i.":"."G".$i)
											->applyFromArray($styleArray);

			$i++;
			foreach ($listado_suministros as $fila)
			{
				$this->excel->getActiveSheet()->setCellValue('A'.$i, $fila['concesionaria_suministro']['concesionaria_suministro']);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['conexion_suministro']['conexion_suministro']);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $fila['opcion_tarifaria']['opcion_tarifaria']);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['sistema_suministro']['sistema_suministro']);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $fila['numero_suministro']);
				$this->excel->getActiveSheet()->setCellValue('F'.$i, $fila['potencia_contratada']);
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $fila['nivel_tension']);

				$i++;
			}
		}

		if(isset($data["Líneas_Eléctricas"]))
		{
			$this->load->model('Lineas_electricas_model');
			$listado_lineas_electricas = $this->Lineas_electricas_model->get_lineas_electricas_sitio($data["sitio"]);

			$i++;
			$activeSheet->setCellValue('A'.$i, 'Líneas Eléctricas');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
							)
						);

			$this->excel->getActiveSheet()->getStyle("A".$i)->applyFromArray($styleArray);

			$i++;
			$i2_lineas = $i+1;
			$this->excel->getActiveSheet()->setCellValue('I'.$i, 'Transformador');
			$this->excel->getActiveSheet()->mergeCells("I".$i.":"."L".$i);
			
			$this->excel->getActiveSheet()->setCellValue('M'.$i, 'Transformix');
			$this->excel->getActiveSheet()->mergeCells("M".$i.":"."P".$i);

			$this->excel->getActiveSheet()->setCellValue('A'.$i2_lineas, 'Fecha de puesta en Servicio');
			$this->excel->getActiveSheet()->setCellValue('B'.$i2_lineas, 'Fases');
			$this->excel->getActiveSheet()->setCellValue('C'.$i2_lineas, 'Tipo');
			$this->excel->getActiveSheet()->setCellValue('D'.$i2_lineas, 'Sección de Línea');
			$this->excel->getActiveSheet()->setCellValue('E'.$i2_lineas, 'Distancia (km)');
			$this->excel->getActiveSheet()->setCellValue('F'.$i2_lineas, 'Postes');
			$this->excel->getActiveSheet()->setCellValue('G'.$i2_lineas, 'Propietario del Terreno');
			$this->excel->getActiveSheet()->setCellValue('H'.$i2_lineas, 'Contrato de servidumbre');
			$this->excel->getActiveSheet()->setCellValue('I'.$i2_lineas, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('J'.$i2_lineas, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('K'.$i2_lineas, 'Potencia');
			$this->excel->getActiveSheet()->setCellValue('L'.$i2_lineas, 'Conexión');
			$this->excel->getActiveSheet()->setCellValue('M'.$i2_lineas, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('N'.$i2_lineas, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('O'.$i2_lineas, 'Potencia');
			$this->excel->getActiveSheet()->setCellValue('P'.$i2_lineas, 'Conexión');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'FFFFFF'),
								'size'  => 10,
								'name'  => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '6788D3')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));

			$this->excel->getActiveSheet()->getStyle("A".$i2_lineas.":"."P".$i2_lineas)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'	=> array(
								'bold'	=> true,
								'color'	=> array('rgb' => '000000'),
								'size' => 10,
								'name' => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '22B14C')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));
			
			$this->excel->getActiveSheet()->getStyle("I".$i)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'	=> array(
								'bold'	=> true,
								'color'	=> array('rgb' => '000000'),
								'size' => 10,
								'name' => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => 'FF8000')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));
			
			$this->excel->getActiveSheet()->getStyle("M".$i)
											->applyFromArray($styleArray);

			$i2_lineas++;
			foreach ($listado_lineas_electricas as $fila)
			{
				$propietarios = "";
				$contratos = "";
				$conexiones = "";
				
				foreach ($fila['propietarios_linea_electrica'] as $key => $propietario)
				{
					$propietarios .= ($key > 0)? ", " : "" ;
					$propietarios .= $propietario['propietario'];
					
					$contratos .= ($key > 0)? ", " : "" ;
					$contratos .= $propietario['contrato_servidumbre'];
				}
				
				foreach ($fila['lineas_electricas_conexion_linea_electrica'] as $key => $conexion)
				{
					$conexiones .= ($key > 0)? " / " : "";
					$conexiones .= $conexion['conexion_linea_electrica']['conexion_linea_electrica'];
				}
				
				if($fila['fecha_servicio'] == "0000-00-00")
				{
					$fecha = "";
				}
				else
				{
					$fecha = date("d/m/Y",strtotime($fila['fecha_servicio']));
				}
				
				$this->excel->getActiveSheet()->setCellValue('A'.$i2_lineas, $fecha);
				$this->excel->getActiveSheet()->setCellValue('B'.$i2_lineas, $fila['sistema_linea_electrica']['sistema_linea_electrica']);
				$this->excel->getActiveSheet()->setCellValue('C'.$i2_lineas, $conexiones);
				$this->excel->getActiveSheet()->setCellValue('D'.$i2_lineas, $fila['seccion_linea']);
				$this->excel->getActiveSheet()->setCellValue('E'.$i2_lineas, $fila['distancia']);
				$this->excel->getActiveSheet()->setCellValue('F'.$i2_lineas, $fila['postes']);
				$this->excel->getActiveSheet()->setCellValue('G'.$i2_lineas, $propietarios);
				$this->excel->getActiveSheet()->setCellValue('H'.$i2_lineas, $contratos);
				$this->excel->getActiveSheet()->setCellValue('I'.$i2_lineas, $fila['tipo_transformador']['marca']);
				$this->excel->getActiveSheet()->setCellValue('J'.$i2_lineas, $fila['num_serie_transformador']);
				$this->excel->getActiveSheet()->setCellValue('K'.$i2_lineas, $fila['potencia_transformador']);
				$this->excel->getActiveSheet()->setCellValue('L'.$i2_lineas, $fila['conexion_transformador']);
				$this->excel->getActiveSheet()->setCellValue('M'.$i2_lineas, $fila['tipo_transformix']['marca']);
				$this->excel->getActiveSheet()->setCellValue('N'.$i2_lineas, $fila['num_serie_transformix']);
				$this->excel->getActiveSheet()->setCellValue('O'.$i2_lineas, $fila['potencia_transformix']);
				$this->excel->getActiveSheet()->setCellValue('P'.$i2_lineas, $fila['conexion_transformix']);

				$i2_lineas++;
			}
		}

		$i = isset($i2_lineas)? $i2_lineas : $i;

		if(isset($data["Tanques_de_Combustible"]))
		{
			$this->load->model('Tanques_combustible_model');
			$listado_tanques_combustible = $this->Tanques_combustible_model->get_tanques_combustible_sitio($data["sitio"]);

			$i++;
			$activeSheet->setCellValue('A'.$i, 'Tanques de Combustible');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
							)
						);

			$this->excel->getActiveSheet()->getStyle("A".$i)->applyFromArray($styleArray);

			$i++;
			$this->excel->getActiveSheet()->setCellValue('A'.$i, 'Tipo de tanque');
			$this->excel->getActiveSheet()->setCellValue('B'.$i, 'Fecha de instalación');
			$this->excel->getActiveSheet()->setCellValue('C'.$i, 'Capacidad (Gl)');
			$this->excel->getActiveSheet()->setCellValue('D'.$i, 'Consumo (Gl/h)');
			$this->excel->getActiveSheet()->setCellValue('E'.$i, 'Autonomía (h)');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'FFFFFF'),
								'size'  => 10,
								'name'  => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '6788D3')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));

			$this->excel->getActiveSheet()->getStyle("A".$i.":"."E".$i)
											->applyFromArray($styleArray);

			$i++;
			foreach ($listado_tanques_combustible as $fila)
			{
				$tipo_tanque = ($fila['tipo_tanque_combustible'] == "1")? "Tanque Interno" : (($fila['tipo_tanque_combustible'] == "2")? "Tanque Externo" : "" ) ;
				$autonomia = round(((Float)$fila['consumo'] != 0)? (((FLoat)$fila['capacidad'])*0.9)/((Float)$fila['consumo']) : 0,2);

				$this->excel->getActiveSheet()->setCellValue('A'.$i, $tipo_tanque);
				$this->excel->getActiveSheet()->setCellValue('B'.$i, $fila['fecha_instalacion']);
				$this->excel->getActiveSheet()->setCellValue('C'.$i, $fila['capacidad']);
				$this->excel->getActiveSheet()->setCellValue('D'.$i, $fila['consumo']);
				$this->excel->getActiveSheet()->setCellValue('E'.$i, $autonomia);

				$i++;
			}
		}

		if(isset($data["Aires_Acondicionados"]))
		{
			$this->load->model('Aires_acondicionados_model');
			$listado_aires_acondicionados = $this->Aires_acondicionados_model->get_aires_acondicionados_sitio($data["sitio"]);

			$i++;
			$activeSheet->setCellValue('A'.$i, 'Aires Acondicionados');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => '000000'),
								'size'  => 10,
								'name'  => 'Verdana'
							)
						);

			$this->excel->getActiveSheet()->getStyle("A".$i)->applyFromArray($styleArray);

			$i++;
			$i2_aires = $i+1;
			$this->excel->getActiveSheet()->setCellValue('B'.$i, 'Equipos Liebert/Bard/Marvair/Mclean/Pentair/Telco');
			$this->excel->getActiveSheet()->mergeCells("B".$i.":"."C".$i);
			
			$this->excel->getActiveSheet()->setCellValue('G'.$i, 'Compresor');
			$this->excel->getActiveSheet()->mergeCells("G".$i.":"."I".$i);
			
			$this->excel->getActiveSheet()->setCellValue('J'.$i, 'Condensador');
			$this->excel->getActiveSheet()->mergeCells("J".$i.":"."K".$i);
			
			$this->excel->getActiveSheet()->setCellValue('L'.$i, 'Evaporador');
			$this->excel->getActiveSheet()->mergeCells("L".$i.":"."M".$i);

			$this->excel->getActiveSheet()->setCellValue('A'.$i2_aires, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('B'.$i2_aires, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('C'.$i2_aires, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('D'.$i2_aires, 'Tipo de Equipo');
			$this->excel->getActiveSheet()->setCellValue('E'.$i2_aires, 'Capacidad (Ton)');
			$this->excel->getActiveSheet()->setCellValue('F'.$i2_aires, 'Fecha de Instalación');
			$this->excel->getActiveSheet()->setCellValue('G'.$i2_aires, 'Marca');
			$this->excel->getActiveSheet()->setCellValue('H'.$i2_aires, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('I'.$i2_aires, 'Tipo');
			$this->excel->getActiveSheet()->setCellValue('J'.$i2_aires, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('K'.$i2_aires, 'Número de Serie');
			$this->excel->getActiveSheet()->setCellValue('L'.$i2_aires, 'Modelo');
			$this->excel->getActiveSheet()->setCellValue('M'.$i2_aires, 'Número de Serie');

			$styleArray = array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'FFFFFF'),
								'size'  => 10,
								'name'  => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '6788D3')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));

			$this->excel->getActiveSheet()->getStyle("A".$i2_aires.":"."M".$i2_aires)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'	=> array(
								'bold'	=> true,
								'color'	=> array('rgb' => 'FFFFFF'),
								'size' => 10,
								'name' => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => 'ff0000')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));
			
			$this->excel->getActiveSheet()->getStyle("B".$i)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'	=> array(
								'bold'	=> true,
								'color'	=> array('rgb' => 'FFFFFF'),
								'size' => 10,
								'name' => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '7030a0')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));
			
			$this->excel->getActiveSheet()->getStyle("G".$i)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'	=> array(
								'bold'	=> true,
								'color'	=> array('rgb' => 'FFFFFF'),
								'size' => 10,
								'name' => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => '00b050')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));
			
			$this->excel->getActiveSheet()->getStyle("J".$i)
											->applyFromArray($styleArray);
											
			$styleArray = array(
							'font'	=> array(
								'bold'	=> true,
								'color'	=> array('rgb' => 'FFFFFF'),
								'size' => 10,
								'name' => 'Verdana'
							),
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => 'e26b0a')
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							));
			
			$this->excel->getActiveSheet()->getStyle("L".$i)
											->applyFromArray($styleArray);

			$i2_aires++;
			foreach ($listado_aires_acondicionados as $fila)
			{
				$fecha_instalacion = (!$fila['fecha_instalacion'])? "Pendiente" : date("d/m/Y", strtotime($fila['fecha_instalacion']));
				
				$this->excel->getActiveSheet()->setCellValue('A'.$i2_aires, $fila['tipo_aires_acondicionados']['marca']);
				$this->excel->getActiveSheet()->setCellValue('B'.$i2_aires, $fila['tipo_aires_acondicionados']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('C'.$i2_aires, $fila['num_serie_aire_acon']);
				$this->excel->getActiveSheet()->setCellValue('D'.$i2_aires, $fila['tipo_aires_acondicionados']['tipo_equipo']);
				$this->excel->getActiveSheet()->setCellValue('E'.$i2_aires, $fila['tipo_aires_acondicionados']['capacidad']);
				$this->excel->getActiveSheet()->setCellValue('F'.$i2_aires, $fecha_instalacion);
				$this->excel->getActiveSheet()->setCellValue('G'.$i2_aires, $fila['tipo_compresor']['marca']);
				$this->excel->getActiveSheet()->setCellValue('H'.$i2_aires, $fila['tipo_compresor']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('I'.$i2_aires, $fila['tipo_compresor']['tipo']);
				$this->excel->getActiveSheet()->setCellValue('J'.$i2_aires, $fila['tipo_condensador']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('K'.$i2_aires, $fila['num_serie_condensador']." ");
				$this->excel->getActiveSheet()->setCellValue('L'.$i2_aires, $fila['tipo_evaporador']['modelo']);
				$this->excel->getActiveSheet()->setCellValue('M'.$i2_aires, $fila['num_serie_evaporador']);

				$i2_aires++;
			}
		}

		$i = isset($i2_aires)? $i2_aires : $i;


		$activeSheet->getColumnDimension('A')->setAutoSize(true);
		$activeSheet->getColumnDimension('B')->setWidth(25);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setWidth(20);
		$activeSheet->getColumnDimension('E')->setAutoSize(true);
		$activeSheet->getColumnDimension('F')->setWidth(20);
		$activeSheet->getColumnDimension('G')->setWidth(20);
		$activeSheet->getColumnDimension('H')->setWidth(20);
		$activeSheet->getColumnDimension('I')->setAutoSize(true);
		$activeSheet->getColumnDimension('J')->setAutoSize(true);
		$activeSheet->getColumnDimension('K')->setAutoSize(true);
		$activeSheet->getColumnDimension('L')->setAutoSize(true);
		$activeSheet->getColumnDimension('M')->setAutoSize(true);
		$activeSheet->getColumnDimension('N')->setAutoSize(true);
		$activeSheet->getColumnDimension('O')->setAutoSize(true);
		$activeSheet->getColumnDimension('P')->setAutoSize(true);
		$activeSheet->getColumnDimension('Q')->setAutoSize(true);
		$activeSheet->getColumnDimension('R')->setAutoSize(true);
		$activeSheet->getColumnDimension('S')->setAutoSize(true);
		$activeSheet->getColumnDimension('T')->setAutoSize(true);
		$activeSheet->getColumnDimension('U')->setWidth(20);
		$activeSheet->getColumnDimension('V')->setWidth(20);
		$activeSheet->getColumnDimension('W')->setWidth(20);
		$activeSheet->getColumnDimension('X')->setWidth(20);
		$activeSheet->getColumnDimension('Y')->setAutoSize(true);
		$activeSheet->getColumnDimension('Z')->setAutoSize(true);
		$activeSheet->getColumnDimension('AA')->setAutoSize(true);
		$activeSheet->getColumnDimension('AB')->setAutoSize(true);
		$activeSheet->getColumnDimension('AC')->setAutoSize(true);
		$activeSheet->getColumnDimension('AD')->setAutoSize(true);
		$activeSheet->getColumnDimension('AE')->setAutoSize(true);
		$activeSheet->getColumnDimension('AF')->setAutoSize(true);
		$activeSheet->getColumnDimension('AG')->setAutoSize(true);
		/*$activeSheet->getColumnDimension('AH')->setAutoSize(true);*/
		$activeSheet->getColumnDimension('AI')->setAutoSize(true);
		$activeSheet->getColumnDimension('AJ')->setAutoSize(true);
		$activeSheet->getColumnDimension('AK')->setAutoSize(true);
		$activeSheet->getColumnDimension('AL')->setAutoSize(true);
		$activeSheet->getColumnDimension('AM')->setAutoSize(true);
		$activeSheet->getColumnDimension('AN')->setAutoSize(true);
		$activeSheet->getColumnDimension('AO')->setAutoSize(true);
		$activeSheet->getColumnDimension('AP')->setAutoSize(true);
		$activeSheet->getColumnDimension('AQ')->setAutoSize(true);
		$activeSheet->getColumnDimension('AR')->setAutoSize(true);
		$activeSheet->getColumnDimension('AS')->setAutoSize(true);
		$activeSheet->getColumnDimension('AT')->setAutoSize(true);
		$activeSheet->getColumnDimension('AU')->setAutoSize(true);
		$activeSheet->getColumnDimension('AV')->setAutoSize(true);
		$activeSheet->getColumnDimension('AW')->setAutoSize(true);
		$activeSheet->getColumnDimension('AX')->setAutoSize(true);
		$activeSheet->getColumnDimension('AY')->setAutoSize(true);
		$activeSheet->getColumnDimension('AZ')->setAutoSize(true);
		$activeSheet->getColumnDimension('BA')->setAutoSize(true);
		$activeSheet->getColumnDimension('BB')->setAutoSize(true);
		$activeSheet->getColumnDimension('BC')->setAutoSize(true);
		$activeSheet->getColumnDimension('BD')->setAutoSize(true);
		$activeSheet->getColumnDimension('BE')->setAutoSize(true);
		$activeSheet->getColumnDimension('BF')->setAutoSize(true);
		$activeSheet->getColumnDimension('BG')->setAutoSize(true);
		$activeSheet->getColumnDimension('BH')->setAutoSize(true);
		$activeSheet->getColumnDimension('BI')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte_Individual_'.$listado[0]["nombre_completo"].'_'.date("d-m-Y-h-i-s").'.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

   public function reportes_predeterminados_bodegas()
	{   
        $this->load->model('Bodega_antenas_gul_model');
		$this->load->model('Bodega_antenas_iden_model');
		$this->load->model('Bodega_bancos_baterias_model');
		$this->load->model('Bodega_elementos_mw_model');
		$this->load->model('Bodega_grupos_electrogenos_model');
		$this->load->model('Bodega_modulos_rectif_model');
		$this->load->model('Bodega_aires_acondicionados_model');
		$this->load->model('Bodega_tanques_combustible_model');
		$this->load->model('Bodega_lineas_electricas_model');
		$this->load->model('Bodega_elementos_sistema_combustible_model');

		$filtros = $this->input->post();
		//var_dump($filtros);
		// $filtros["almacen"]=1;
		$listado = array(); 
        $listado_bancos_baterias = array(); 

		   $listado_antenas_gul = $this->Bodega_antenas_gul_model->get_antenas_gul($filtros);
			$listado_antenas_iden = $this->Bodega_antenas_iden_model->get_antenas_iden($filtros);
		
			$listado_bancos_baterias = $this->Bodega_bancos_baterias_model->get_bancos_baterias($filtros);
			$listado_elementos_mw = $this->Bodega_elementos_mw_model->get_elementos_mw($filtros);
			$listado_grupos_electrogenos = $this->Bodega_grupos_electrogenos_model->get_grupos_electrogenos($filtros);
			$listado_modulos_rectif = $this->Bodega_modulos_rectif_model->get_modulos_rectif($filtros);
			$listado_aires_acondicionados = $this->Bodega_aires_acondicionados_model->get_aires_acondicionados($filtros);
			$listado_tanques_combustible = $this->Bodega_tanques_combustible_model->get_tanques_combustible($filtros);
		   $listado_lineas_electricas = $this->Bodega_lineas_electricas_model->get_lineas_electricas($filtros);

           $listado_elementos_sistema_combustible = $this->Bodega_elementos_sistema_combustible_model->get_elementos_sistema_combustible($filtros);          
            //echo json_encode($listado_bancos_baterias);
            // var_dump($listado_bancos_baterias);
			// demo
			// echo $listado_bancos_baterias[0]["tipo_bancos_baterias"]["marca"];
			// foreach ($listado_bancos_baterias as $fila)
			//  {
			// echo $fila["tipo_bancos_baterias"]["marca"];
			//  }
			// demo
       //banco baterias       
		
		$listado = $listado_bancos_baterias;

		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Reporte Bodegas Virtuales')
					->setSubject('Reporte')
					->setDescription('Reporte predeterminado desde el Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Banco de baterias');
		$activeSheet->setCellValue('A1', ' Marca ')
					->setCellValue('B1', ' Modelo ')
					->setCellValue('C1', ' Número de serie ')
					->setCellValue('D1', ' Estado ')					
					->setCellValue('E1', ' Almacen ');


		//$objPHPExcel->getActiveSheet()->setAutoFilter("A1:C4");			
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		$activeSheet->getStyle("A1:E1")
					->applyFromArray($styleArray);

		$activeSheet->setAutoFilter($activeSheet->calculateWorksheetDimension());

		$i = 2;
		foreach ($listado as $fila)
		{
		$activeSheet->setCellValue('A'.$i, $fila['tipo_bancos_baterias']['marca'])
						->setCellValue('B'.$i, $fila['tipo_bancos_baterias']['modelo'])
						->setCellValue('C'.$i, $fila['bodega_celdas_bancos_baterias'][0]['numero_serie'])
						->setCellValue('D'.$i, $fila['estado']['estado'])
						
						->setCellValue('E'.$i, $fila['almacen']['almacen']);
			$i++;
		}
		$activeSheet->getColumnDimension('A')->setWidth(13);
		$activeSheet->getColumnDimension('B')->setWidth(16);
		$activeSheet->getColumnDimension('C')->setWidth(29);
		$activeSheet->getColumnDimension('D')->setWidth(14);		
		$activeSheet->getColumnDimension('E')->setWidth(18);
		$this->excel->createSheet();
      // Banco baterias 


       //Elemento Microondas
		$listado = $listado_elementos_mw;        
		$csheet = 1;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Elementos Microondas');
		$activeSheet->setCellValue('A1', ' Elemento ')
		        	->setCellValue('B1', ' Marca ')
		        	->setCellValue('C1', ' Número de serie ')
					->setCellValue('D1', ' Modelo ')
					->setCellValue('E1', ' Estado ')
					->setCellValue('F1', ' Almacen ');
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		$activeSheet->getStyle("A1:F1")
					->applyFromArray($styleArray);
		$activeSheet->setAutoFilter($activeSheet->calculateWorksheetDimension());

		$i = 2;
		foreach ($listado as $fila)
		{
		$activeSheet->setCellValue('A'.$i, $fila['elemento_mw']['elemento_mw'])
						->setCellValue('B'.$i, $fila['tipo_elemento_mw']['marca'])
						->setCellValue('C'.$i, $fila['tipo_elemento_mw']['modelo'])
						->setCellValue('D'.$i, $fila['numero_serie'])
						->setCellValue('E'.$i, $fila['estado']['estado'])			
						->setCellValue('F'.$i, $fila['almacen']['almacen']);

			$i++;
		}
		$activeSheet->getColumnDimension('A')->setWidth(19);
		$activeSheet->getColumnDimension('B')->setWidth(14);
		$activeSheet->getColumnDimension('C')->setWidth(29);
		$activeSheet->getColumnDimension('D')->setWidth(29);
		$activeSheet->getColumnDimension('E')->setWidth(14);
		$activeSheet->getColumnDimension('F')->setWidth(18);
        $this->excel->createSheet();
        //Elemento Microondas
        //Grupos Electrógenos

		$listado = $listado_grupos_electrogenos;
		$csheet = 2;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Grupo Electrogenos');
		$activeSheet->setCellValue('A1', ' Marca ')
		        	->setCellValue('B1', ' Modelo ')
		        	->setCellValue('C1', ' Número de serie ')
					->setCellValue('D1', ' Estado ')
					->setCellValue('E1', ' Almacen ');
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		$activeSheet->getStyle("A1:E1")
					->applyFromArray($styleArray);

		$activeSheet->setAutoFilter($activeSheet->calculateWorksheetDimension());

		$i = 2;
		foreach ($listado as $fila)
		{
		$activeSheet->setCellValue('A'.$i, $fila['tipo_grupo_electrogeno']['marca'])
						->setCellValue('B'.$i, $fila['tipo_grupo_electrogeno']['modelo'])
						->setCellValue('C'.$i, $fila['numero_serie_grupo_electrogeno'])
						->setCellValue('D'.$i, $fila['estado']['estado'])
						->setCellValue('E'.$i, $fila['almacen']['almacen']);

			$i++;
		}
		$activeSheet->getColumnDimension('A')->setWidth(14);
		$activeSheet->getColumnDimension('B')->setWidth(14);
		$activeSheet->getColumnDimension('C')->setWidth(29);
		$activeSheet->getColumnDimension('D')->setWidth(14);		
		$activeSheet->getColumnDimension('E')->setWidth(18);
	    $this->excel->createSheet();
        //Grupos Electrógenos

        //Antenas GUL
		$listado = $listado_antenas_gul;
		$csheet = 3;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Antenas Gul');
		$activeSheet->setCellValue('A1', ' Marca ')
		        	->setCellValue('B1', ' Modelo ')
		        	->setCellValue('C1', ' Número de serie ')
					->setCellValue('D1', ' Estado ')
					->setCellValue('E1', ' Proveniente ')
					->setCellValue('F1', ' Almacen ');
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		$activeSheet->getStyle("A1:F1")
					->applyFromArray($styleArray);
		$activeSheet->setAutoFilter($activeSheet->calculateWorksheetDimension());			

		$i = 2;
		foreach ($listado as $fila)
		{
		$activeSheet->setCellValue('A'.$i, $fila['tipo_antena_gul']['marca'])
						->setCellValue('B'.$i, $fila['tipo_antena_gul']['modelo'])
						->setCellValue('C'.$i, $fila['numero_serie'])
						->setCellValue('D'.$i, $fila['estado']['estado'])
						->setCellValue('E'.$i, $fila['proveniente_sitio']['nombre_completo'])
						->setCellValue('F'.$i, $fila['almacen']['almacen']);

			$i++;
		}
		$activeSheet->getColumnDimension('A')->setWidth(14);
		$activeSheet->getColumnDimension('B')->setWidth(14);
		$activeSheet->getColumnDimension('C')->setWidth(29);
		$activeSheet->getColumnDimension('D')->setWidth(14);
		$activeSheet->getColumnDimension('E')->setWidth(30);
		$activeSheet->getColumnDimension('F')->setWidth(18);
		$this->excel->createSheet();
        //Antenas GUl

        //Aire Acondicionados
		$listado = $listado_aires_acondicionados;		
		$csheet = 4;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Aire Acondicionado');
		$activeSheet->setCellValue('A1', ' Marca ')
		        	->setCellValue('B1', ' Modelo ')
		        	->setCellValue('C1', ' Número de serie ')
					->setCellValue('D1', ' Proveniente ')
					->setCellValue('E1', ' Estado ')
					->setCellValue('F1', ' Almacen ');
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		$activeSheet->getStyle("A1:F1")
					->applyFromArray($styleArray);

        $activeSheet->setAutoFilter($activeSheet->calculateWorksheetDimension());
		$i = 2;
		foreach ($listado as $fila)
		{
		$activeSheet->setCellValue('A'.$i, $fila['tipo_aires_acondicionados']['marca'])
						->setCellValue('B'.$i, $fila['tipo_aires_acondicionados']['modelo'])
                       ->setCellValue('C'.$i, $fila['num_serie_aire_acon'])
						
						->setCellValue('D'.$i, $fila['proveniente_sitio']['nombre_completo'])
						->setCellValue('E'.$i, $fila['estado']['estado'])
						->setCellValue('F'.$i, $fila['almacen']['almacen']);

			$i++;
		}
		$activeSheet->getColumnDimension('A')->setWidth(14);
		$activeSheet->getColumnDimension('B')->setWidth(14);
		$activeSheet->getColumnDimension('C')->setWidth(29);
		$activeSheet->getColumnDimension('D')->setWidth(30);
		$activeSheet->getColumnDimension('E')->setWidth(14);
		$activeSheet->getColumnDimension('F')->setWidth(18);
		$this->excel->createSheet();
		//Aire Acondicionado


		// //Elementos sistema combustible 
	    $listado =  json_decode($listado_elementos_sistema_combustible, true);
	    // var_dump($listado);
	    // echo $listado[0]["elemento_sistema_combustible"]["elemento_sistema_combustible"];
	    // echo $listado[0]["tipo_elemento_sistema_combustible"]["marca"];   
	    

		$csheet = 5;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Elementos Sistema Combustible');
		$activeSheet->setCellValue('A1', ' Elemento ')
		        	->setCellValue('B1', ' Marca ')
					->setCellValue('C1', ' Modelo ')
					->setCellValue('D1', ' Número de serie ')
					->setCellValue('E1', ' Estado ')
					->setCellValue('F1', ' Almacen ');
					
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		$activeSheet->getStyle("A1:F1")
					->applyFromArray($styleArray);
		$activeSheet->setAutoFilter($activeSheet->calculateWorksheetDimension());

		$i = 2;
		foreach ($listado as $fila)
		{
		$activeSheet->setCellValue('A'.$i, $fila["elemento_sistema_combustible"]["elemento_sistema_combustible"])
						->setCellValue('B'.$i, $fila["tipo_elemento_sistema_combustible"]["marca"])
						->setCellValue('C'.$i, $fila["tipo_elemento_sistema_combustible"]["modelo"])
						->setCellValue('D'.$i, $fila["numero_serie"])
						->setCellValue('E'.$i, $fila["estado"]["estado"])
						->setCellValue('F'.$i, $fila["almacen"]["almacen"]);

			$i++;
		}
		$activeSheet->getColumnDimension('A')->setWidth(19);
		$activeSheet->getColumnDimension('B')->setWidth(14);
		$activeSheet->getColumnDimension('C')->setWidth(14);
		$activeSheet->getColumnDimension('D')->setWidth(29);
		$activeSheet->getColumnDimension('E')->setWidth(14);
		$activeSheet->getColumnDimension('F')->setWidth(18);
		$this->excel->createSheet();
	   //Elementos sistema combustible
       //Antenas iDEN
	    $listado = $listado_antenas_iden;  
		$csheet = 6;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Antenas iDEN');
		$activeSheet->setCellValue('A1', ' Marca ')
		        	->setCellValue('B1', ' Modelo ')
		        	->setCellValue('C1', ' Número de serie ')
					->setCellValue('D1', ' Proveniente ')
					->setCellValue('E1', ' Estado ')
					->setCellValue('F1', ' Almacen ');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		$activeSheet->getStyle("A1:F1")
					->applyFromArray($styleArray);
		$activeSheet->setAutoFilter($activeSheet->calculateWorksheetDimension());

		$i = 2;
		foreach ($listado as $fila)
		{
		$activeSheet->setCellValue('A'.$i, $fila["tipo_antena_iden"]["marca"])
						->setCellValue('B'.$i, $fila["tipo_antena_iden"]["modelo"])
						->setCellValue('C'.$i, $fila["numero_serie"])
						->setCellValue('D'.$i, $fila["proveniente_sitio"]["nombre_completo"])
						->setCellValue('E'.$i, $fila["estado"])
						->setCellValue('F'.$i, $fila["almacen"]["almacen"]);

			$i++;
		}
		$activeSheet->getColumnDimension('A')->setWidth(14);
		$activeSheet->getColumnDimension('B')->setWidth(20);
		$activeSheet->getColumnDimension('C')->setWidth(29);
		$activeSheet->getColumnDimension('D')->setWidth(30);
		$activeSheet->getColumnDimension('E')->setWidth(14);
		$activeSheet->getColumnDimension('F')->setWidth(18);
		$this->excel->createSheet();
       //Antenas iDEN

       //Elementos Lineas Electricas
        $listado = $listado_lineas_electricas;  
		$csheet = 7;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Elementos Lineas Electricas');
		$activeSheet->setCellValue('A1', ' Elemento ')
		        	->setCellValue('B1', ' Marca ')
		        	->setCellValue('C1', ' Número de serie ')
					->setCellValue('D1', ' Proveniente ')
					->setCellValue('E1', ' Estado ')
					->setCellValue('F1', ' Almacen ');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));
		$activeSheet->getStyle("A1:F1")
					->applyFromArray($styleArray);
		$activeSheet->setAutoFilter($activeSheet->calculateWorksheetDimension());

		$i = 2;
		foreach ($listado as $fila)
		{

		        $elemento =  $fila["elemento"];
				if($elemento == 1){
                   $elementoTexto="Transformador";
				}elseif ($elemento == 2) {
					$elementoTexto="Transformix";
				}


		$activeSheet->setCellValue('A'.$i, $elementoTexto)
						->setCellValue('B'.$i, $fila["tipo_elemento"]["marca"])
						->setCellValue('C'.$i, $fila["numero_serie"])
						->setCellValue('D'.$i, $fila["proveniente_sitio"]["nombre_completo"])
						->setCellValue('E'.$i, $fila["estado"]["estado"])
						->setCellValue('F'.$i, $fila["almacen"]["almacen"]);

			$i++;
		}

		$activeSheet->getColumnDimension('A')->setWidth(19);
		$activeSheet->getColumnDimension('B')->setWidth(14);
		$activeSheet->getColumnDimension('C')->setWidth(29);
		$activeSheet->getColumnDimension('D')->setWidth(30);
		$activeSheet->getColumnDimension('E')->setWidth(14);
		$activeSheet->getColumnDimension('F')->setWidth(18);
		$this->excel->setActiveSheetIndex(0);

       //Elementos Lineas Electricas

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Bodegas_Virtuales_'.date('d-m-Y-h-i-s').'.xlsx"');
		header('Cache-Control: max-age=0');		
		header('Cache-Control: max-age=1');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');		
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
    }
















}