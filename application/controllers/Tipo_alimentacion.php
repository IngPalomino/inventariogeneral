<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_alimentacion extends CI_Controller {

	public function listar()
	{
		$this->load->model('Tipo_alimentacion_model');

		$listado = $this->Tipo_alimentacion_model->get_types();

		echo json_encode($listado);
	}
}
