<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Elementos_sistema_combustible extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Elementos_sistema_combustible_model');
		$this->load->library('auth');
	}

	public function listar()
	{
		echo json_encode($this->Elementos_sistema_combustible_model->get_types());
	}

	public function id_tipo()
	{
		$info = array();
		$info = $this->input->post();
		$tipo = $this->Elementos_sistema_combustible_model->get_type($info);
		echo json_encode($tipo);
	}

	public function editar($id)
	{
		echo json_encode($this->Elementos_sistema_combustible_model->edit($id));
	}

	public function actualizar($id)
	{
		$this->Elementos_sistema_combustible_model->update($id, $this->input->input_stream());
	}

	public function crear()
	{
		$this->Elementos_sistema_combustible_model->insert($this->input->post());
	}

	public function eliminar($id)
	{
		$this->Elementos_sistema_combustible_model->delete($id);
	}

	public function almacenar()
	{
		// $this->load->model('Bodega_elementos_sistema_combustible_model');
		// $this->load->model('Logs_bodega_elementos_sistema_combustible_model');
		// $info = $this->input->input_stream();		
		// $info_sistema_combustible = json_decode($info["sistema_combustible"], TRUE);
		


		// if(count($info_sistema_combustible) > 0)
		// {
		// 	$this->load->model('Sistema_combustible_model');

		// 	foreach($info_antenas_mw as $antena)
		// 	{
		// 		if($this->Antenas_mw_model->update($antena["id"], array('eliminado' => 1)))
		// 		{
		// 			$info_antena_mw["elemento_sistema_combustible"] = 3;
		// 			$info_antena_mw["tipo_elemento_sistema_combustible"] = $antena["tipo_antena_sistema_combustible"]["id"];
		// 			$info_antena_mw["numero_serie"] = $antena["numero_serie"];
		// 			$info_antena_mw["estado"] = $info["estado"];
		// 			$info_antena_mw["proveniente_sitio"] = $info["proveniente_sitio"];
		// 			$info_antena_mw["tipo_almacen"] = $info["tipo_almacen"];
		// 			$info_antena_mw["almacen"] = $info["almacen"];

		// 			$this->Bodega_elementos_sistema_combustible_model->insert($info_antena_mw);

		// 			$evento = array(
		// 				"accion"		=> "Almacenamiento de Sistema de Combustible",
		// 				"tabla"			=> "tb_bodega_elementos_sistema_combustible"
		// 			);

		// 			$this->Logs_bodega_elementos_sistema_combustible_model->ingreso_de_datos($evento);
		// 		}
		// 	}
		// }
	}
}