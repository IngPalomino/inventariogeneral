<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Objetos extends CI_Controller {
	
	public function listar_objetos_sitio($id)
	{
		$this->load->model('Tanques_combustible_model');
		$listado_tanques = $this->Tanques_combustible_model->get_tanques_combustible_sitio($id);
		foreach($listado_tanques as &$objeto)
		{
			$objeto["tipo_objeto"] = "Tanque de combustible";
			$objeto["link"] = "tanques_combustible";
		}
		
		$this->load->model('Suministros_model');
		$listado_suministros = $this->Suministros_model->get_suministros_sitio($id);
		foreach($listado_suministros as &$objeto)
		{
			$objeto["tipo_objeto"] = "Suministro";
			$objeto["link"] = "suministros";
		}
		
		$this->load->model('Lineas_electricas_model');
		$listado_lineas = $this->Lineas_electricas_model->get_lineas_electricas_sitio($id);
		foreach($listado_lineas as &$objeto)
		{
			$objeto["tipo_objeto"] = "Línea Eléctrica";
			$objeto["link"] = "lineas_electricas";
		}
		
		$this->load->model('Rectificadores_model');
		$listado_rectificadores = $this->Rectificadores_model->get_controlador_rectif_sitio($id);
		foreach($listado_rectificadores as &$objeto)
		{
			$objeto["tipo_objeto"] = "Rectificador";
			$objeto["link"] = "rectificadores";
		}
		
		$this->load->model('Bancos_baterias_model');
		$rectificadores = $this->Bancos_baterias_model->get_bancos_baterias_sitio($id);
		$listado_bancos = array();
		foreach($rectificadores as &$rectificador)
		{
			$listado_bancos = $rectificador["bancos"];
		}
		foreach($listado_bancos as &$objeto)
		{
			$objeto["tipo_objeto"] = "Banco de Baterías";
			$objeto["link"] = "bancos_baterias";
		}
		
		
		$this->load->model('Aires_acondicionados_model');
		$listado_aires = $this->Aires_acondicionados_model->get_aires_acondicionados_sitio($id);
		foreach($listado_aires as &$objeto)
		{
			$objeto["tipo_objeto"] = "Aire Acondicionado";
			$objeto["link"] = "aires_acondicionados";
		}
		
		$this->load->model('Grupos_electrogenos_model');
		$listado_grupos = $this->Grupos_electrogenos_model->get_grupos_electrogenos_sitio($id);
		foreach($listado_grupos as &$objeto)
		{
			$objeto["tipo_objeto"] = "Grupo Electrógeno";
			$objeto["link"] = "grupos_electrogenos";
		}
		
		$listado = array_merge($listado_tanques, $listado_suministros, $listado_lineas, $listado_rectificadores, $listado_bancos, $listado_aires, $listado_grupos);
		echo json_encode($listado);
	}
	
	public function resumen_sitio($id)
	{
		$resumen = array();
		
		$this->load->model('Antenas_mw_model');
		$listado_antenas_mw = $this->Antenas_mw_model->get_antenas_microondas_sitio_md($id);

		if(count($listado_antenas_mw) > 0)
		{
			$antenas_mw_array = array(
				"objeto" 	=> "Antenas Microondas",
				"parent"	=> "infraestructura",
				"cantidad" 	=> count($listado_antenas_mw),
				"info"		=> $listado_antenas_mw,
				"link"		=> "infraestructura/antenas"
			);
			array_push($resumen,$antenas_mw_array);
		}
		
		$this->load->model('Antenas_gul_model');
		$listado_antenas_gul = $this->Antenas_gul_model->get_antenas_gul_sitio($id);
		if(count($listado_antenas_gul) > 0)
		{
			$antenas_gul_array = array(
				"objeto" 	=> "Antenas GUL",
				"parent"	=> "infraestructura",
				"cantidad" 	=> count($listado_antenas_gul),
				"info"		=> $listado_antenas_gul,
				"link"		=> "infraestructura/antenas"
			);
			array_push($resumen,$antenas_gul_array);
		}
		
		$this->load->model('Idus_model');
		$listado_idus = $this->Idus_model->get_idus_sitio($id);
		if(count($listado_idus) > 0)
		{
			$idus_array = array(
				"objeto"	=> "IDUs",
				"parent"	=> "infraestructura",
				"cantidad"	=> count($listado_idus),
				"info"		=> $listado_idus,
				"link"		=> "infraestructura/idus"
			);
			array_push($resumen,$idus_array);
		}
		
		$this->load->model('Enlaces_model');
		$listado_enlaces = $this->Enlaces_model->get_enlaces_sitio($id);
		if(count($listado_enlaces) > 0)
		{
			$enlaces_array = array(
				"objeto"	=> "Enlaces",
				"parent"	=> "infraestructura",
				"cantidad"	=> count($listado_enlaces),
				"info"		=> $listado_enlaces,
				"link"		=> "infraestructura/enlaces"
			);
			array_push($resumen,$enlaces_array);
		}
		
		$this->load->model('Grupos_electrogenos_model');
		$listado_grupos_electrogenos = $this->Grupos_electrogenos_model->get_grupos_electrogenos_sitio($id);
		if(count($listado_grupos_electrogenos) > 0)
		{
			$grupos_electrogenos_array = array(
				"objeto"	=> "Grupos Electrógenos",
				"parent"	=> "energia",
				"cantidad"	=> count($listado_grupos_electrogenos),
				"info"		=> $listado_grupos_electrogenos,
				"link"		=> "energia/grupos_electrogenos"
			);
			array_push($resumen,$grupos_electrogenos_array);
		}
		
		$this->load->model('Rectificadores_model');
		$listado_rectificadores = $this->Rectificadores_model->get_controlador_rectif_sitio($id);
		if(count($listado_rectificadores) > 0)
		{
			$rectificadores_array = array(
				"objeto"	=> "Plantas Rectificadoras",
				"parent"	=> "energia",
				"cantidad"	=> count($listado_rectificadores),
				"info"		=> $listado_rectificadores,
				"link"		=> "energia/rectificadores"
			);
			array_push($resumen,$rectificadores_array);
		}
		
		$this->load->model('Bancos_baterias_model');
		$listado_bancos_baterias = $this->Bancos_baterias_model->get_bancos_baterias_sitio_resumen($id);
		if(count($listado_bancos_baterias) > 0)
		{
			$bancos_baterias_array = array(
				"objeto"	=> "Bancos de Baterías",
				"parent"	=> "energia",
				"cantidad"	=> count($listado_bancos_baterias),
				"info"		=> $listado_bancos_baterias,
				"link"		=> "energia/bancos_baterias"
			);
			array_push($resumen,$bancos_baterias_array);
		}
		
		$this->load->model('Suministros_model');
		$listado_suministros = $this->Suministros_model->get_suministros_sitio($id);
		if(count($listado_suministros) > 0)
		{
			$suministros_array = array(
				"objeto"	=> "Suministros",
				"parent"	=> "energia",
				"cantidad"	=> count($listado_suministros),
				"info"		=> $listado_suministros,
				"link"		=> "energia/suministros"
			);
			array_push($resumen,$suministros_array);
		}
		
		$this->load->model('Lineas_electricas_model');
		$listado_lineas_electricas = $this->Lineas_electricas_model->get_lineas_electricas_sitio($id);
		if(count($listado_lineas_electricas) > 0)
		{
			$lineas_electricas_array = array(
				"objeto"	=> "Líneas Eléctricas",
				"parent"	=> "energia",
				"cantidad"	=> count($listado_lineas_electricas),
				"info"		=> $listado_lineas_electricas,
				"link"		=> "energia/lineas_electricas"
			);
			array_push($resumen,$lineas_electricas_array);
		}
		
		$this->load->model('Tanques_combustible_model');
		$listado_tanques_combustible = $this->Tanques_combustible_model->get_tanques_combustible_sitio($id);
		if(count($listado_tanques_combustible) > 0)
		{
			$tanques_combustible_array = array(
				"objeto"	=> "Tanques de Combustible",
				"parent"	=> "energia",
				"cantidad"	=> count($listado_tanques_combustible),
				"info"		=> $listado_tanques_combustible,
				"link"		=> "energia/tanques_combustible"
			);
			array_push($resumen,$tanques_combustible_array);
		}
		
		$this->load->model('Aires_acondicionados_model');
		$listado_aires_acondicionados = $this->Aires_acondicionados_model->get_aires_acondicionados_sitio($id);
		if(count($listado_aires_acondicionados) > 0)
		{
			$aires_acondicionados_array = array(
				"objeto"	=> "Aires Acondicionados",
				"parent"	=> "energia",
				"cantidad"	=> count($listado_aires_acondicionados),
				"info"		=> $listado_aires_acondicionados,
				"link"		=> "energia/aires_acondicionados"
			);
			array_push($resumen,$aires_acondicionados_array);
		}
		
		echo json_encode($resumen);
	}
}