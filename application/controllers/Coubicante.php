<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coubicante extends CI_Controller {

	public function listar()
	{
		$this->load->model('Coubicante_model');

		$listado = $this->Coubicante_model->get_Coubicante();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Coubicante_model');

		$Coubicante = $this->Coubicante_model->edit($id);

		echo json_encode($Coubicante);
	}

	public function actualizar($id)
	{
		$this->load->model('Coubicante_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['coubicante'] = $this->input->input_stream('Coubicante');

		$this->Coubicante_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Coubicante_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['coubicante'] = $this->input->post('Coubicante');

		$this->Coubicante_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Coubicante_model');

		$this->Coubicante_model->delete($id);
	}
}