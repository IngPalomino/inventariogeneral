<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_masivo extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('auth');
		$this->load->model('Solicitud_aprobar_antenas_model');
	}

	public function listar_solicitudes()
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				echo json_encode($this->Solicitud_aprobar_antenas_model->get_solicitudes());
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function listar_elemento($elemento)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				echo json_encode($this->Solicitud_aprobar_antenas_model->get_elemento($elemento));
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function listar_filas_pendientes($elemento)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				echo json_encode($this->Solicitud_aprobar_antenas_model->get_filas_pendientes($elemento));
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function listar_filas_validadas($elemento)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				echo json_encode($this->Solicitud_aprobar_antenas_model->get_filas_validadas($elemento));
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function aprobar_fila_antenas_gul($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$post = $this->input->post();
				$fila["estado"] = 2;
				$this->load->model('Ingreso_masivo_antenas_gul_model');

				if($this->Ingreso_masivo_antenas_gul_model->update($id, $fila))
				{
					$this->load->model('Antenas_gul_model');

					$info["tipo_antena_gul"] = $post["tipo_antena_gul"];
					$info["numero_serie"] = $post["numero_serie"];
					$info["sector"] = $post["sector"];
					$info["altura"] = $post["altura"];
					$info["azimuth"] = $post["azimuth"];
					$info["tilt_electrico"] = $post["tilt_electrico"];
					$info["tilt_mecanico"] = $post["tilt_mecanico"];
					$info["info_telnet"] = $post["info_telnet"];

					$info = $this->clean_empty($info);

					if($post["id_actualizar"] != "null")
					{
						$this->Antenas_gul_model->update($post["id_actualizar"], $info);
					}
					else
					{
						$info["sitio"] = $post["sitio"];
						$this->Antenas_gul_model->insert($info);
					}
				}
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function rechazar_fila_antenas_gul($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$fila["estado"] = 3;
				$this->load->model('Ingreso_masivo_antenas_gul_model');

				$this->Ingreso_masivo_antenas_gul_model->update($id, $fila);
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function aprobar_fila_suministros($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$post = $this->input->post();
				$fila["estado"] = 2;
				$this->load->model('Ingreso_masivo_suministros_model');

				if($this->Ingreso_masivo_suministros_model->update($id, $fila))
				{
					$this->load->model('Suministros_model');

					$info["concesionaria_suministro"] = $post["concesionaria_suministro"];
					$info["numero_suministro"] = $post["numero_suministro"];
					$info["potencia_contratada"] = $post["potencia_contratada"];
					$info["opcion_tarifaria"] = $post["opcion_tarifaria"];
					$info["nivel_tension"] = $post["nivel_tension"];
					$info["sistema_suministro"] = $post["sistema_suministro"];
					$info["conexion_suministro"] = $post["conexion_suministro"];
					$info["tipo_alimentacion"] = $post["tipo_alimentacion"];

					if($post["id_actualizar"] != "null")
					{
						$this->Suministros_model->update($post["id_actualizar"], $info);
					}
					else
					{
						$info["sitio"] = $post["sitio"];
						$this->Suministros_model->insert($info);
					}
				}
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function rechazar_fila_suministros($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$fila["estado"] = 3;
				$this->load->model('Ingreso_masivo_suministros_model');

				$this->Ingreso_masivo_suministros_model->update($id, $fila);
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function aprobar_fila_idus($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$post = $this->input->post();
				$fila["estado"] = 2;
				$this->load->model('Ingreso_masivo_idus_model');

				if($this->Ingreso_masivo_idus_model->update($id, $fila))
				{
					$this->load->model('Idus_model');

					$info["ne_id"] = $post["ne_id"];
					$info["link_name"] = $post["link_name"];
					$info["tipo_idu"] = $post["tipo_idu"];
					$info["numero_serie"] = $post["numero_serie"];
					$info["idu_name"] = $post["idu_name"];

					if($post["id_actualizar"] != "null")
					{
						$this->Idus_model->update($post["id_actualizar"], $info);
					}
					else
					{
						$info["sitio"] = $post["sitio"];
						$this->Idus_model->insert($info);
					}
				}
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function rechazar_fila_idus($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$fila["estado"] = 3;
				$this->load->model('Ingreso_masivo_idus_model');

				$this->Ingreso_masivo_idus_model->update($id, $fila);
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function aprobar_fila_enlaces($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$post = $this->input->post();
				$fila["estado"] = 2;
				$this->load->model('Ingreso_masivo_enlaces_model');

				if($this->Ingreso_masivo_enlaces_model->update($id, $fila))
				{
					$this->load->model('Enlaces_model');

					$info["idu_1"] = $post["idu_1"];
					$info["idu_2"] = $post["idu_2"];
					$info["configuracion"] = $post["configuracion"];
					$info["polaridad"] = $post["polaridad"];
					$info["ancho_banda"] = $post["ancho_banda"];
					$info["modulacion"] = $post["modulacion"];
					$info["capacidad"] = $post["capacidad"];
					$info["modulacion_adaptativa"] = $post["modulacion_adaptativa"];
					$info["modulacion_am"] = $post["modulacion_am"];
					$info["capacidad_am"] = $post["capacidad_am"];
					$info["banda"] = $post["banda"];
					$info_equipos["odus_enlaces"] = json_decode($post["odus_enlaces"], true);
					$info_equipos["antenas_mw_enlaces"] = json_decode($post["antenas_mw_enlaces"], true);

					if($post["id_actualizar"] != "null")
					{
						if($this->Enlaces_model->update($post["id_actualizar"], $info))
						{
							if(count($info_equipos["odus_enlaces"]) > 0)
							{
								$this->load->model('Odus_model');
								$this->Odus_model->delete_where(array("enlace" => $post["id_actualizar"]));

								foreach($info_equipos["odus_enlaces"] as $odu)
								{
									unset($odu["id"]);
									$odu["tipo_odu"] = $odu["tipo_odu"]["id"];
									$odu["enlace"] = $post["id_actualizar"];
									$this->Odus_model->insert($odu);
								}
							}

							if(count($info_equipos["antenas_mw_enlaces"]) > 0)
							{
								$this->load->model('Antenas_mw_model');
								$this->Antenas_mw_model->delete_where(array("enlace" => $post["id_actualizar"]));

								foreach($info_equipos["antenas_mw_enlaces"] as $antena)
								{
									unset($antena["id"]);
									$antena["tipo_antena_mw"] = $antena["tipo_antena_mw"]["id"];
									$antena["enlace"] = $post["id_actualizar"];
									$this->Antenas_mw_model->insert($antena);
								}
							}
						}
					}
					else
					{
						$respuesta = $this->Enlaces_model->insert($info);

						if($respuesta["resp"])
						{
							if(count($info_equipos["odus_enlaces"]) > 0)
							{
								$this->load->model('Odus_model');

								foreach($info_equipos["odus_enlaces"] as $odu)
								{
									unset($odu["id"]);
									$odu["tipo_odu"] = $odu["tipo_odu"]["id"];
									$odu["enlace"] = $respuesta["last_id"];
									$this->Odus_model->insert($odu);
								}
							}

							if(count($info_equipos["antenas_mw_enlaces"]) > 0)
							{
								$this->load->model('Antenas_mw_model');

								foreach($info_equipos["antenas_mw_enlaces"] as $antena)
								{
									unset($antena["id"]);
									$antena["tipo_antena_mw"] = $antena["tipo_antena_mw"]["id"];
									$antena["enlace"] = $respuesta["last_id"];
									$this->Antenas_mw_model->insert($antena);
								}
							}
						}
					}
				}
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function rechazar_fila_enlaces($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$fila["estado"] = 3;
				$this->load->model('Ingreso_masivo_enlaces_model');

				$this->Ingreso_masivo_enlaces_model->update($id, $fila);
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function aprobar_fila_pop($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$post = $this->input->post();
				$fila["estado"] = 2;
				$this->load->model('Ingreso_masivo_pop_model');

				if($this->Ingreso_masivo_pop_model->update($id, $fila))
				{
					$this->load->model('Status_site_model');

					$info["latitud"] = $post["latitud"];
					$info["longitud"] = $post["longitud"];
					$info["tipo_torre"] = $post["tipo_torre"];
					$info["altura_torre"] = $post["altura_torre"];
					$info["factor_uso_torre"] = $post["factor_uso_torre"];
					$info["greenfield_rooftop"] = $post["greenfield_rooftop"];
					$info["altura_edificacion"] = $post["altura_edificacion"];
					$info["camuflaje"] = $post["camuflaje"];
					$info["tipo_proyecto"] = $post["tipo_proyecto"];
					$info["coubicado"] = $post["coubicado"];
					$info["coubicador"] = $post["coubicador"];
					$info["operador_coubicante"] = $post["operador_coubicante"];

					if($post["id_actualizar"] != "null")
					{
						$this->Status_site_model->update($post["id_actualizar"], $info);
					}
					else
					{
						$info["codigo"] = $post["codigo"];
						$info["nombre"] = $post["nombre"];
						$info["nombre_completo"] = $post["nombre_completo"];
						$this->Status_site_model->insert($info);
					}
				}
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function rechazar_fila_pop($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$fila["estado"] = 3;
				$this->load->model('Ingreso_masivo_pop_model');

				$this->Ingreso_masivo_pop_model->update($id, $fila);
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function aprobar_fila_contratos($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$post = $this->input->post();
				$fila["estado"] = 2;
				$this->load->model('Ingreso_masivo_pop_model');

				if ($this->Ingreso_masivo_pop_model->update_contratos($id, $fila))
				{
					$this->load->model('Status_site_model');
					$info_sitio['zona_peru'] = $post['zona_peru'];
					$info_sitio['direccion'] = $post['direccion'];
					$this->Status_site_model->update($post['sitio'], $info_sitio);

					$this->load->model('Contratos_model');

					$info["area"] = $post["area"];
					$info["fecha_inicio"] = $post["fecha_inicio"];
					$info["fecha_fin"] = $post["fecha_fin"];
					$info["arrendador"] = $post["arrendador"];
					$info_contactos["contactos_contrato"] = json_decode($post["contactos_contrato"], TRUE);

					if ($post["id_actualizar"] != "null")
					{
						if ($this->Contratos_model->update($post["id_actualizar"], $info))
						{
							if (count($info_contactos["contactos_contrato"]) > 0)
							{
								$this->load->model('Contactos_contrato_model');
								if ($this->Contactos_contrato_model->delete_where(array("contrato" => $post["id_actualizar"])))
								{
									foreach ($info_contactos["contactos_contrato"] as $contacto)
									{
										unset($contacto["id"]);
										$contacto["contrato"] = $post["id_actualizar"];
										$contacto["telefono"] = json_encode($contacto["telefono"]);
										$contacto["correo"] = json_encode($contacto["correo"]);
										$this->Contactos_contrato_model->insert($contacto);
									}
								}
							}
						}
					}
					else
					{
						$info["sitio"] = $post["sitio"];
						$info["id_contrato"] = $post["id_contrato"];
						$respuesta = $this->Contratos_model->insert($info);

						if($respuesta["resp"])
						{
							if(count($info_contactos["contactos_contrato"]) > 0)
							{
								$this->load->model('Contactos_contrato_model');

								foreach($info_contactos["contactos_contrato"] as $contacto)
								{
									unset($contacto["id"]);
									$contacto["contrato"] = $respuesta["last_id"];
									$contacto["telefono"] = json_encode($contacto["telefono"]);
									$contacto["correo"] = json_encode($contacto["correo"]);
									$this->Contactos_contrato_model->insert($contacto);
								}
							}
						}
					}
				}
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function rechazar_fila_contratos($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$fila["estado"] = 3;
				$this->load->model('Ingreso_masivo_pop_model');

				$this->Ingreso_masivo_pop_model->update_contratos($id, $fila);
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function aprobar_fila_rectificadores($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$post = $this->input->post();
				$fila["estado"] = 2;
				$this->load->model('Ingreso_masivo_rectificadores_model');

				if ($this->Ingreso_masivo_rectificadores_model->update($id, $fila))
				{
					$this->load->model('Rectificadores_model');

					$info["tipo_controlador_rectif"] = $post["tipo_controlador_rectif"];
					$info["anio_servicio"] = $post["anio_servicio"];
					$info["numero_serie"] = $post["numero_serie"];
					$info["capacidad_maxima"] = $post["capacidad_maxima"];
					$info["energizacion_controlador_rectif"] = $post["energizacion_controlador_rectif"];
					$info["ubicacion_controlador_rectif"] = $post["ubicacion_controlador_rectif"];
					$modulos_rectif = json_decode($post["modulos_rectif"], TRUE);

					if ($post["id_actualizar"] != "null")
					{
						if ($this->Rectificadores_model->update($post["id_actualizar"], $info))
						{
							if (count($modulos_rectif) > 0)
							{
								$this->load->model('Modulos_rectif_model');
								if ($this->Modulos_rectif_model->delete_where(array("controlador_rectif" => $post["id_actualizar"])))
								{
									foreach ($modulos_rectif as $modulo)
									{
										$info_modulo["controlador_rectif"] = $post["id_actualizar"];
										$info_modulo["tipo_modulos_rectif"] = $modulo["tipo_modulos_rectif"]["id"];
										$info_modulo["numero_serie"] = $modulo["numero_serie"];
										$this->Modulos_rectif_model->insert($info_modulo);
									}
								}
							}
						}
					}
					else
					{
						$info["sitio"] = $post["sitio"];
						$respuesta = $this->Rectificadores_model->insert($info);

						if ($respuesta["resp"])
						{
							if (count($modulos_rectif) > 0)
							{
								$this->load->model('Modulos_rectif_model');

								foreach ($modulos_rectif as $modulo)
								{
									$info_modulo["controlador_rectif"] = $respuesta["last_id"];
									$info_modulo["tipo_modulos_rectif"] = $modulo["tipo_modulos_rectif"]["id"];
									$info_modulo["numero_serie"] = $modulo["numero_serie"];
									$this->Modulos_rectif_model->insert($info_modulo);
								}
							}
						}
					}
				}
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function rechazar_fila_rectificadores($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$fila["estado"] = 3;
				$this->load->model('Ingreso_masivo_rectificadores_model');

				$this->Ingreso_masivo_rectificadores_model->update($id, $fila);
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function aprobar_fila_bancos_baterias($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$post = $this->input->post();
				$fila["estado"] = 2;
				$this->load->model('Ingreso_masivo_bancos_baterias_model');

				if($this->Ingreso_masivo_bancos_baterias_model->update($id, $fila))
				{
					$this->load->model('Bancos_baterias_model');

					$info["controlador_rectif"] = $post["controlador_rectif"];
					$info["tipo_bancos_baterias"] = $post["tipo_bancos_baterias"];
					$info["fecha_instalacion"] = $post["fecha_instalacion"];
					$info["confiabilidad"] = $post["confiabilidad"];
					$celdas = json_decode($post["celdas_bancos_baterias"], TRUE);

					if($post["id_actualizar"] != "null")
					{
						if ($this->Bancos_baterias_model->update($post["id_actualizar"], $info))
						{
							if (count($celdas) > 0)
							{
								$this->load->model('Celdas_bancos_baterias_model');
								if ($this->Celdas_bancos_baterias_model->delete_where(array("controlador_rectif_bancos_baterias" => $post["id_actualizar"])))
								{
									foreach ($celdas as $celda)
									{
										$info_celda["controlador_rectif_bancos_baterias"] = $post["id_actualizar"];
										$info_celda["fecha_instalacion"] = $post["fecha_instalacion"];
										$info_celda["numero_serie"] = $celda["numero_serie"];
										$this->Celdas_bancos_baterias_model->insert($info_celda);
									}
								}
							}
						}
					}
					else
					{
						$respuesta = $this->Bancos_baterias_model->insert($info);

						if ($respuesta["resp"])
						{
							if (count($celdas) > 0)
							{
								$this->load->model('Celdas_bancos_baterias_model');

								foreach ($celdas as $celda)
								{
									$info_celda["controlador_rectif_bancos_baterias"] = $respuesta["last_id"];
									$info_celda["fecha_instalacion"] = $post["fecha_instalacion"];
									$info_celda["numero_serie"] = $celda["numero_serie"];
									$this->Celdas_bancos_baterias_model->insert($info_celda);
								}
							}
						}
					}
				}
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function rechazar_fila_bancos_baterias($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$fila["estado"] = 3;
				$this->load->model('Ingreso_masivo_bancos_baterias_model');

				$this->Ingreso_masivo_bancos_baterias_model->update($id, $fila);
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function finalizar_solicitud($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				$solicitud["estado"] = 4;
				$solicitud["fecha_fin"] = date("Y-m-d H:i:s");
				$this->Ingreso_masivo_model->update($id, $solicitud);
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	protected function clean_empty($info_array)
	{
		foreach($info_array as $key => $value)
		{
			if($value == "" || $value == NULL || $value == "null")
			{
				array_splice($info_array, $key, 1);
			}
		}

		return $info_array;
	}
}