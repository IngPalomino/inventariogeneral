<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_masivo_idus extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->modulo = 4;
	}

	public function plantilla_idus()
	{
		$this->load->library("excel");
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla IDUs")
									->setSubject("Plantilla")
									->setDescription("Plantilla para ingresar información de IDUs al Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle("IDUs");

		$activeSheet->setCellValue("A1", "Código")
					->setCellValue("B1", "Nombre")
					->setCellValue("C1", "NE ID")
					->setCellValue("D1", "Link Name")
					->setCellValue("E1", "Marca")
					->setCellValue("F1", "Modelo")
					->setCellValue("G1", "Número de serie");

		$styleArray = array(
				"font" => array(
						"bold"	=> true,
						"color"	=> array("rgb" => "FFFFFF"),
						"size"	=> 11,
						"name"	=> "Verdana"
					),
				"fill" => array(
						"type"	=> PHPExcel_Style_Fill::FILL_SOLID,
						"color"	=> array("rgb" => "6788D3")
					),
				"alignment" => array(
						"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle("A1:G1")->applyFromArray($styleArray);

		$activeSheet->getStyle('A:A')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$activeSheet->getColumnDimension("A")->setAutoSize(true);
		$activeSheet->getColumnDimension("B")->setAutoSize(true);
		$activeSheet->getColumnDimension("C")->setAutoSize(true);
		$activeSheet->getColumnDimension("D")->setAutoSize(true);
		$activeSheet->getColumnDimension("E")->setAutoSize(true);
		$activeSheet->getColumnDimension("F")->setAutoSize(true);
		$activeSheet->getColumnDimension("G")->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_IDUs.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function plantilla_tipo_idu()
	{
		$this->load->model('Tipo_idu_model');
		$listado = $this->Tipo_idu_model->get_types();

		$this->load->library("excel");
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla IDUs")
									->setSubject("Plantilla")
									->setDescription("Plantilla para ingresar información de IDUs al Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle("Tipo IDUs");

		$activeSheet->setCellValue("A1", "Marca");
		$activeSheet->setCellValue("B1", "Modelo");
		$activeSheet->setCellValue("C1", "Consumo de Energía");
		$activeSheet->setCellValue("D1", "Capacidad de Conmutación");
		$activeSheet->setCellValue("E1", "Peso");
		$activeSheet->setCellValue("F1", "Dimensiones");

		$styleArray = array(
				"font" => array(
						"bold"	=> true,
						"color"	=> array("rgb" => "FFFFFF"),
						"size"	=> 11,
						"name"	=> "Verdana"
					),
				"fill" => array(
						"type"	=> PHPExcel_Style_Fill::FILL_SOLID,
						"color"	=> array("rgb" => "6788D3")
					),
				"alignment" => array(
						"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle("A1:F1")->applyFromArray($styleArray);

		$i = 2;

		foreach($listado["modelos"] as $fila)
		{
			$activeSheet->setCellValue("A".$i, $fila["marca"])
						->setCellValue("B".$i, $fila["modelo"])
						->setCellValue("C".$i, $fila["consumo_energia"])
						->setCellValue("D".$i, $fila["capacidad_conmutacion"])
						->setCellValue("E".$i, $fila["peso"])
						->setCellValue("F".$i, $fila["dimensiones"]);

			$i++;
		}

		$activeSheet->getColumnDimension("A")->setAutoSize(true);
		$activeSheet->getColumnDimension("B")->setAutoSize(true);
		$activeSheet->getColumnDimension("C")->setAutoSize(true);
		$activeSheet->getColumnDimension("D")->setAutoSize(true);
		$activeSheet->getColumnDimension("E")->setAutoSize(true);
		$activeSheet->getColumnDimension("F")->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Tipo_IDU.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function upload_file()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"])
		{
			$this->load->model('Autorizacion_grupos_model');

			if($this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo))
			{
				$post = $this->input->post();
				$config["upload_path"] = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo";
				$config["allowed_types"] = "xlsx";
				$config["max_size"] = "1024";
				$config["overwrite"] = FALSE;
				$config["file_name"] = "plantilla_".$post["tipo"]."_".date("d-m-Y-H-i-s");

				$this->load->library("upload", $config);

				if($this->upload->do_upload("archivo"))
				{
					echo json_encode(array(
							"error"		=> FALSE,
							"mensaje"	=> "Archivo subido con éxito.",
							"resultado"	=> $this->upload->data("file_name")
						));
				}
				else
				{
					echo json_encode(array(
							"error"		=> TRUE,
							"mensaje"	=> "No se pudo subir el archivo.",
							"resultado"	=> $this->upload->display_errors()
						));
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción.",
						"resultado"	=> "forbidden"
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró",
					"resultado"	=> "logged_off"
				));
		}
	}

	public function check_file()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"])
		{
			$this->load->model('Autorizacion_grupos_model');

			if($this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo))
			{
				$this->load->model('Ingreso_masivo_idus_model');
				$post = $this->input->post();
				$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"];
				/*$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/plantilla_antenas_gul_19-06-2017-16-09-50.xlsx";*/
				
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						"error"			=> FALSE,
						"advertencia"	=> FALSE,
						"mensajes"		=> array(),
						"advertencias"	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				if(in_array("IDUs", $sheetNames))
				{
					try{ $objPHPExcel->setActiveSheetIndexByName("IDUs"); } catch(Exception $e){ continue; }

					$activeSheet = $objPHPExcel->getActiveSheet();
					$cell_collection = $activeSheet->getCellCollection();
					$cabeceras = array("Código", "Nombre", "NE ID", "Link Name", "Marca", "Modelo", "Número de serie");
					$cabeceras_no_validas = array();
					$cabeceras_validas = array();
					$arr_data = array();

					foreach($cell_collection as $cell)
					{
						$column = $activeSheet->getCell($cell)->getColumn();
						$row = $activeSheet->getCell($cell)->getRow();
						$data_value = $activeSheet->getCell($cell)->getValue();

						if($row == 1)
						{
							$header[$row][$column] = $data_value;

							foreach($header as $filaHeader => $infoHeader)
							{
								if(!in_array($infoHeader[$column], $cabeceras))
								{
									array_push($cabeceras_no_validas, $infoHeader[$column]);
								}
								else
								{
									array_push($cabeceras_validas, $infoHeader[$column]);
								}
							}
						}
						else
						{
							if(!isset($data_value) && $column == "A")
							{
								/*break;*/
								$resultados["advertencia"] = TRUE;
								array_push($resultados["advertencias"], "La columna $column en la fila $row no tiene información, se obviará esta fila.");
							}
							elseif(in_array($header[1][$column], $cabeceras_no_validas))
							{
								
							}
							else
							{
								$arr_data[$row][$column] = $data_value;
							}
						}
					}

					foreach($cabeceras as &$value)
					{
						if(!in_array($value, $cabeceras_validas))
						{
							$resultados["error"] = TRUE;
							array_push($resultados["mensajes"], "La columna $value es obligatoria.");
						}
					}

					if(count($cabeceras_no_validas) > 0)
					{
						$cabeceras_no_validas = array_values(array_unique($cabeceras_no_validas));

						foreach($cabeceras_no_validas as $cabecera)
						{
							$resultados["advertencia"] = TRUE;
							array_push($resultados["advertencias"], "La cabecera $cabecera no corresponde a la plantilla original, se obviará esta columna.");
						}
					}

					/*echo json_encode($cabeceras_no_validas);*/
					$filas = count($arr_data);

					if(count($arr_data) > 0)
					{
						foreach($arr_data as $fila => $infoFila)
						{
							$nombreSeparado = explode("_", $infoFila["B"]);
							$codigoNombre = $nombreSeparado[0];

							foreach($infoFila as $columna => $campo)
							{
								if($header[1][$columna] == "Código")
								{
									$codigo = $campo;
									if(strlen($campo) != 7)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código de sitio en la fila $fila no es válido, debe tener 7 dígitos.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código de sitio en la fila $fila debe contener únicamente números.");
									}
									else
									{
										if($codigoNombre != $campo)
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "El Código de sitio en la fila $fila no coincide con el código del Nombre.");
										}
									}
								}
								elseif($header[1][$columna] == "Nombre")
								{
									$nombre = $this->Ingreso_masivo_idus_model->esNombreCodigoValidos($codigo, $campo);
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Nombre en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_idus_model->esNombreCompletoValido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre de sitio en la fila $fila no existe en la base de datos.");
									}
									elseif(!$nombre)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre y Código de sitio en la fila $fila no existe en la base de datos.");
									}
									elseif(!$this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo, $nombre["id"]))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Usted no tiene permiso para ingresar/actualizar información en el sitio $campo de la fila $fila.");
									}
								}
								elseif($header[1][$columna] == "NE ID")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el NE ID en la fila $fila.");
									}
									elseif(!preg_match("/^((\d{1,4})-(\d{1,5}))*$/", $campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El NE ID en la fila $fila debe seguir el patrón '1234-12345'.");
									}
									elseif($nombre !== false)
									{
										if($this->Ingreso_masivo_idus_model->existeIDUEnSitio($nombre["id"], $campo) !== false)
										{
											$resultados["advertencia"] = TRUE;
											array_push($resultados["advertencias"], "El sitio en la fila $fila ya tiene una IDU con el NE ID $campo, se actualizará este elemento.");
										}
										elseif($this->Ingreso_masivo_idus_model->esIDUDuplicada($campo) !== false)
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "El NE ID $campo en la fila $fila ya existe en la base de datos.");
										}
										else
										{
											/*array_push($idus_validas, array($fila => $campo));*/
											$idus_validas[$fila] = $campo;
										}
									}
								}
								elseif($header[1][$columna] == "Link Name")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Link Name en la fila $fila.");
									}
								}
								elseif($header[1][$columna] == "Marca")
								{
									$marca = $campo;
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Marca en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_idus_model->esMarcaValida($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Marca en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Modelo")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Modelo en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_idus_model->esModeloValido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Modelo en la fila $fila no existe en la base de datos.");
									}
									elseif(!$this->Ingreso_masivo_idus_model->esMarcaModeloValidos($marca, $campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El tipo de IDU (Marca y Modelo) en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Número de serie")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Número de serie en la fila $fila.");
									}
								}
							}
						}

						$values = array_count_values($idus_validas);

						foreach($values as $key => $value)
						{
							if($value > 1)
							{
								$resultados["error"] = TRUE;
								array_push($resultados["mensajes"], "El NE ID $key se repite $value veces.");
							}
						}

						/*echo json_encode($arr_data);*/
					}
					else
					{
						$resultados["error"] = TRUE;
						array_push($resultados["mensajes"], "La hoja no contiene información");
					}
				}
				else
				{
					$resultados["error"] = TRUE;
					array_push($resultados["mensajes"], "No se encontró la hoja llamada 'IDUs'");
				}

				if(!$resultados["error"])
				{
					array_push($resultados["mensajes"], "Filas válidas: ".count($arr_data));
				}

				echo json_encode($resultados);

				if($resultados["error"])
				{
					unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción."
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró."
				));
		}
	}

	public function insert_temp()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"])
		{
			$this->load->model('Autorizacion_grupos_model');

			if($this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo))
			{
				$this->load->model('Ingreso_masivo_idus_model');
				$idIngresoMasivo = $this->Ingreso_masivo_idus_model->registrar_ingreso_masivo($session['id_usuario']);
				$post = $this->input->post();
				$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"];
				/*$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/plantilla_antenas_gul_19-06-2017-16-09-50.xlsx";*/
				
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						"error"			=> FALSE,
						"advertencia"	=> FALSE,
						"mensajes"		=> array(),
						"advertencias"	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				$resultado = 0;

				if(in_array("IDUs", $sheetNames))
				{
					try{ $objPHPExcel->setActiveSheetIndexByName("IDUs"); } catch(Exception $e){ continue; }

					$activeSheet = $objPHPExcel->getActiveSheet();
					$cell_collection = $activeSheet->getCellCollection();
					$cabeceras = array("Código", "Nombre", "NE ID", "Link Name", "Marca", "Modelo", "Número de serie");
					$cabeceras_no_validas = array();
					$cabeceras_validas = array();
					$arr_data = array();

					foreach($cell_collection as $cell)
					{
						$column = $activeSheet->getCell($cell)->getColumn();
						$row = $activeSheet->getCell($cell)->getRow();
						$data_value = $activeSheet->getCell($cell)->getValue();

						if($row == 1)
						{
							$header[$row][$column] = $data_value;

							foreach($header as $filaHeader => $infoHeader)
							{
								if(!in_array($infoHeader[$column], $cabeceras))
								{
									array_push($cabeceras_no_validas, $infoHeader[$column]);
								}
								else
								{
									array_push($cabeceras_validas, $infoHeader[$column]);
								}
							}
						}
						else
						{
							if(!isset($data_value) && $column == "A")
							{
								/*break;*/
							}
							elseif(in_array($header[1][$column], $cabeceras_no_validas))
							{
								
							}
							else
							{
								$arr_data[$row][$column] = $data_value;
							}
						}
					}

					/*echo json_encode($cabeceras_no_validas);*/
					$filas = count($arr_data);

					if($idIngresoMasivo !== FALSE)
					{
						foreach($arr_data as $fila => $infoFila)
						{
							$nombreSeparado = explode("_", $infoFila["B"]);
							$codigoNombre = $nombreSeparado[0];

							foreach($infoFila as $columna => $campo)
							{
								$ingresoMasivo["ingreso_masivo"] = $idIngresoMasivo;

								if($header[1][$columna] == "Código")
								{
									$codigo = $campo;
								}
								elseif($header[1][$columna] == "Nombre")
								{
									$nombre = $this->Ingreso_masivo_idus_model->esNombreCodigoValidos($codigo, $campo);
									$ingresoMasivo["sitio"] = $nombre["id"];
								}
								elseif($header[1][$columna] == "NE ID")
								{
									$ingresoMasivo["ne_id"] = $campo;
									$actualizar = $this->Ingreso_masivo_idus_model->existeIDUEnSitio($nombre["id"], $campo);

									$ingresoMasivo["id_actualizar"] = (($actualizar !== FALSE)? $actualizar["id"] : NULL);
								}
								elseif($header[1][$columna] == "Link Name")
								{
									$ingresoMasivo["link_name"] = $campo;
									$ingresoMasivo["idu_name"] = (strpos($campo, " to ")? substr($campo, 0, strpos($campo, " to ")) : $campo);
								}
								elseif($header[1][$columna] == "Marca")
								{
									$marca = $campo;
								}
								elseif($header[1][$columna] == "Modelo")
								{
									$tipo_idu = $this->Ingreso_masivo_idus_model->esMarcaModeloValidos($marca, $campo);
									$ingresoMasivo["tipo_idu"] = $tipo_idu["id"];
								}
								elseif($header[1][$columna] == "Número de serie")
								{
									$ingresoMasivo["numero_serie"] = $campo;
								}
							}

							if($this->Ingreso_masivo_idus_model->ingresar_temporal($ingresoMasivo))
							{
								$resultado++;
							}
						}

						$resultados["mensajes"] = "Se ha enviado $resultado fila(s) para ser aprobada(s).";
						unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
					}
					else
					{
						$resultados["error"] = TRUE;
						array_push($resultados["mensajes"], "La hoja no contiene información");
					}
				}
				else
				{
					$resultados["error"] = TRUE;
					array_push($resultados["mensajes"], "No se encontró la hoja llamada 'IDUs'");
				}

				if(!$resultados["error"])
				{
					array_push($resultados["mensajes"], "Filas válidas: ".count($arr_data));
				}

				echo json_encode($resultados);

				if($resultados["error"])
				{
					unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción."
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró."
				));
		}
	}

	public function listar_solicitudes($usuario)
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"] && ($session["id_usuario"] == $usuario))
		{
			$this->load->model('Ingreso_masivo_idus_model');
			$listado = $this->Ingreso_masivo_idus_model->get_solicitudes($usuario);
			echo json_encode($listado);
		}
		else
		{
			show_404();
		}
	}
}