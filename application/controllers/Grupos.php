<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupos extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Grupos_model");
	}

	public function listar()
	{
		$listado = $this->Grupos_model->select();

		$salida = array(
				"info" => $listado
			);

		echo json_encode($salida);
	}

	public function select($exclude = null)
	{
		$listado = $this->Grupos_model->select();

		$salida = array(
				"info" => $listado
			);

		echo json_encode($salida);
	}

	public function insert()
	{
		
	}

   public function crear(){
		$this->load->library('session');
		$session = $this->session->userdata();
		if($session["logged_in"])
		{
			$this->load->model("Autorizacion_grupos_model");
			$permiso = $this->Autorizacion_grupos_model->comprobar_autorizacion($session["id_usuario"],array(1,2));
			if($permiso)
			{
				$post = array();		
				$post = $this->input->post();
				$this->load->helper('security');
				$xssTest = true;
				foreach ($post as $key => $value)
				{
					if(($key == "nombre") || ($key == "descripcion"))
					{
						$xssTest = $xssTest && $this->security->xss_clean($value);
					}
					elseif($key == "permisos")
					{
						$xssTest = $xssTest && $this->security->xss_clean($value);
						$permisos = json_decode($value);
						unset($post[$key]);
					}
					else
					{
						unset($post[$key]);
					}
				}

				if(!$xssTest)
				{
					return false;
				}

				if( count($post) > 0 )
				{
					$this->load->model('Grupos_model');
					$this->load->model("Grupos_modulos_model");
					$post["grupo"] = strtolower($post["nombre"]);
					$post["grupo"] = str_replace(" ", "_", $post["grupo"]);
					$respuesta = $this->Grupos_model->insert($post);
                    //var_dump($respuesta);
                    //modulos
                    $id = $respuesta["last_id"];                     
                    foreach ($permisos as $permiso)
							{
								$info_permiso["grupos"] = $id;
								$info_permiso["modulo"] = $permiso;
								$this->Grupos_modulos_model->insert($info_permiso);
					    	}

				}
				else
				{
				  if( count($permisos) > 0 )
				{
					$respuesta = true;
				}
        //permisos			
				}				
			}
			else
			{
				$error['error'] = TRUE;
				$error['mensaje'] = "Permisos insuficientes";
				echo json_encode($error);
			}
		}
		else
		{
			$error['error'] = TRUE;
			$error['mensaje'] = "Expiró su sesión";
			echo json_encode($error);
		}

    }




	public function actualizar($id = null)
	{
		if($id === null)
		{
			return false;
		}

		$this->load->library('session');

		$session = $this->session->userdata();
		if($session["logged_in"])
		{
			$this->load->model("Autorizacion_grupos_model");

			$permiso = $this->Autorizacion_grupos_model->comprobar_autorizacion($session["id_usuario"],array(1,2));

			if($permiso)
			{
				$put = $this->input->input_stream();
				$this->load->helper('security');
				$xssTest = true;
				foreach ($put as $key => $value)
				{
					if(($key == "nombre") || ($key == "descripcion"))
					{
						$xssTest = $xssTest && $this->security->xss_clean($value);
					}
					elseif($key == "permisos")
					{
						$xssTest = $xssTest && $this->security->xss_clean($value);
						$permisos = json_decode($value);
						unset($put[$key]);
					}
					else
					{
						unset($put[$key]);
					}
				}

				if(!$xssTest)
				{
					return false;
				}

				if( count($put) > 0 )
				{
					$this->load->model('Grupos_model');
					$put["grupo"] = strtolower($put["nombre"]);
					$put["grupo"] = str_replace(" ", "_", $put["grupo"]);
					$respuesta = $this->Grupos_model->update($id,$put);
				}
				else
				{
					if( count($permisos) > 0 )
					{
						$respuesta = true;
					}
				}

				if($respuesta)
				{
					if( isset($permisos) && (count($permisos) > 0) )
					{
						$this->load->model("Grupos_modulos_model");
						$resp = $this->Grupos_modulos_model->borrar_grupos_modulos($id);

						if($resp)
						{
							foreach ($permisos as $permiso)
							{
								$info_permiso["grupos"] = $id;
								$info_permiso["modulo"] = $permiso;
								$this->Grupos_modulos_model->insert($info_permiso);
							}
						}
					}

					$error['error'] = FALSE;
					$error['mensaje'] = "Se guardaron los cambios.";
					echo json_encode($error);
				}
				else
				{
					$error['error'] = TRUE;
					$error['mensaje'] = "Hubo un problema.";
					echo json_encode($error);
				}
			}
			else
			{
				$error['error'] = TRUE;
				$error['mensaje'] = "Permisos insuficientes";
				echo json_encode($error);
			}
		}
		else
		{
			$error['error'] = TRUE;
			$error['mensaje'] = "Expiró su sesión";
			echo json_encode($error);
		}
	}

	public function delete($id)
	{

		//echo $id;

	 //    $this->load->library('session');
		// $userSession = $this->session->userdata();
		// $salida = array(
		// 		'respuesta' => FALSE);

		// if( $userSession["logged_in"] )
		// {
		// 	$this->load->helper('form');
		// 	$this->load->library('form_validation');

		// 	$config = array(
		// 			array(
		// 				'field' => 'id',
		// 				'label' => 'grupo',
		// 				'rules' => 'required',
		// 				'errors' => array(
		// 						'required' => 'Debe elegir un %s.'
		// 					)
		// 			)
		// 		);

		// 	$this->form_validation->set_data(array("id",$id));
		// 	$this->form_validation->set_rules($config);

		// 	if($this->form_validation->run() === FALSE)
		// 	{
		// 		$salida['error'] = validation_errors();
		// 		echo json_encode($salida);
		// 	}
		// 	else
		// 	{
		// 		$salida["respuesta"] = $this->Grupos_model->delete($id);

		// 		echo json_encode($salida);
		// 	}
		// }
		// else
		// {
		// 	echo json_encode($salida);
		// }
		$this->Grupos_model->delete($id);
	}

	public function edit($id)
	{
		$this->load->library('session');

		$userSession = $this->session->userdata();

		$salida = array(
				'respuesta' => FALSE,
				'info' => array());

		if( $userSession["logged_in"] )
		{
			$respuesta = $this->Grupos_model->edit($id);

			if( count($respuesta) > 0 )
			{
				$salida["respuesta"] = true;
				$salida["info"] = $respuesta[0];
			}

			echo json_encode($salida);
		}
		else
		{
			echo json_encode($salida);
		}
	}
}