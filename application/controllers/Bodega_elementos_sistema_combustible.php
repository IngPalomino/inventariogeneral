<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_elementos_sistema_combustible extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('auth');
		$this->load->model('Bodega_elementos_sistema_combustible_model');
		$this->load->model('Logs_bodega_elementos_sistema_combustible_model', 'logs');
	}

	public function almacenar()
	{
		$post = $this->input->post();

		if ($this->auth->logged_in())
		{
			if ($this->Bodega_elementos_sistema_combustible_model->insert($post))
			{
				$evento['accion'] = 'Almacenamiento de Sistema de Combustible';
				$evento['tabla'] = 'tb_bodega_sistema_combustible';

				$this->logs->ingreso_de_datos($evento);
			}
		}
		else
		{
			echo json_encode(array('error' => TRUE, 'message' => 'Su sesión ha expirado.'));
		}
	}
	
	public function listar($elemento)
	{
		echo json_encode($this->Bodega_elementos_sistema_combustible_model->get_elemento_sistema_combustible($elemento));
	}

	public function actualizar($id)
	{

		$info = array();
		$info = $this->input->input_stream();
		$sistema_combustible = $this->Bodega_elementos_sistema_combustible_model->edit($id);
        //elimino valores 
         unset($info['logs_bodega_elementos_sistema_combustible']);
		 unset($info['$$hashKey']);
		 unset($info['link']);
		 unset($info['comentario']);
        //elimino valores
		if ($this->Bodega_elementos_sistema_combustible_model->update($id, $info))
		{
			$evento = array(
					'accion'		=> 'Actualización de Elemento Sistema combustible',
					'tabla'			=> 'tb_bodega_elementos_sistema_combustible',
					'id'			=> $id,
					'datos viejos'	=> $sistema_combustible,
					'datos nuevos'	=> $this->Bodega_elementos_sistema_combustible_model->edit($id)
				);

			if ($evento['datos viejos'] != $evento['datos nuevos'])
			{
				 
				$this->logs->actualizacion_de_datos($id, $evento);

			

			}
		}
	}

	public function mover($id)
	{
		$info = $this->input->input_stream(array('tipo_almacen', 'almacen'));
		$sistema_combustible = $this->Bodega_elementos_sistema_combustible_model->edit($id);

		$almacen_antiguo = array(
					'tipo_almacen'	=> $sistema_combustible[0]['tipo_almacen'],
					'almacen'		=> $sistema_combustible[0]['almacen']
				);

		$info['almacen_previo'] = json_encode($almacen_antiguo);

		if ($this->Bodega_elementos_sistema_combustible_model->update($id, $info))
		{
			$sistema_combustible_nueva = $this->Bodega_elementos_sistema_combustible_model->edit($id);

			$almacen_nuevo = array(
					'tipo_almacen' 	=> $sistema_combustible_nueva[0]['tipo_almacen'],
					'almacen'		=> $sistema_combustible_nueva[0]['almacen']
				);

			$evento = array(
					'accion'		=> 'Cambio de almacén de Elemento Sistema Combustible',
					'tabla'			=> 'tb_bodega_sistema_combustible',
					'id'			=> $id,
					'almacen_antiguo'	=> $info['almacen_previo'],
					'almacen_nuevo'	=> json_encode($almacen_nuevo)
				);

			if ($evento['almacen_antiguo'] != $evento['almacen_nuevo'])
			{
				$this->logs->actualizacion_de_datos($id, $evento, $this->input->input_stream('comentario'));
			}
		}
	}

	public function eliminar($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->Bodega_elementos_sistema_combustible_model->update($id, array('eliminado' => 1)))
			{
				$evento['accion'] = 'Eliminación de Elemento Sistema Combustible';
				$evento['tabla'] = 'tb_bodega_elementos_sistema_combustible';
				$evento['id'] = $id;

				$this->logs->actualizacion_de_datos($id, $evento, $this->input->input_stream('comentario'));
			}
		}
		else
		{
			echo json_encode(array('error' => TRUE, 'message' => 'Su sesión ha expirado.'));
		}
	}
}