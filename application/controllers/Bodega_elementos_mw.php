<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_elementos_mw extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('auth');
		$this->load->model('Bodega_elementos_mw_model');
		$this->load->model('Logs_bodega_elementos_mw_model', 'logs');
	}

	public function almacenar()
	{
		$post = $this->input->post();

		if ($this->auth->logged_in())
		{
			if ($this->Bodega_elementos_mw_model->insert($post))
			{
				$evento['accion'] = 'Almacenamiento de Aire Acondicionado';
				$evento['tabla'] = 'tb_bodega_aires_acondicionados';

				$this->logs->ingreso_de_datos($evento);
			}
		}
		else
		{
			echo json_encode(array('error' => TRUE, 'message' => 'Su sesión ha expirado.'));
		}
	}
	
	public function listar($elemento)
	{
	echo json_encode($this->Bodega_elementos_mw_model->get_elemento_mw($elemento));
	}

	public function actualizar($id)
	{
		$info = $this->input->input_stream();
		$antena = $this->Bodega_elementos_mw_model->edit($id);

         //elimino valores 
         unset($info['logs_bodega_elementos_mw']);
		 unset($info['$$hashKey']);
		 unset($info['link']);
		 unset($info['comentario']);
        //elimino valores 



		if ($this->Bodega_elementos_mw_model->update($id, $info))
		{
			$evento = array(
					'accion'		=> 'Actualización de Elemento Microondas',
					'tabla'			=> 'tb_bodega_antenas_mw',
					'id'			=> $id,
					'datos viejos'	=> $antena,
					'datos nuevos'	=> $this->Bodega_elementos_mw_model->edit($id)
				);

			if ($evento['datos viejos'] != $evento['datos nuevos'])
			{
				$this->logs->actualizacion_de_datos($id, $evento);
			}
		}
	}

	public function mover($id)
	{
		$info = $this->input->input_stream(array('tipo_almacen', 'almacen'));
		$antena = $this->Bodega_elementos_mw_model->edit($id);

		$almacen_antiguo = array(
					'tipo_almacen'	=> $antena[0]['tipo_almacen'],
					'almacen'		=> $antena[0]['almacen']
				);

		$info['almacen_previo'] = json_encode($almacen_antiguo);

		if ($this->Bodega_elementos_mw_model->update($id, $info))
		{
			$antena_nueva = $this->Bodega_elementos_mw_model->edit($id);

			$almacen_nuevo = array(
					'tipo_almacen' 	=> $antena_nueva[0]['tipo_almacen'],
					'almacen'		=> $antena_nueva[0]['almacen']
				);

			$evento = array(
					'accion'		=> 'Cambio de almacén de Elemento Microondas',
					'tabla'			=> 'tb_bodega_antenas_mw',
					'id'			=> $id,
					'almacen_antiguo'	=> $info['almacen_previo'],
					'almacen_nuevo'	=> json_encode($almacen_nuevo)
				);

			if ($evento['almacen_antiguo'] != $evento['almacen_nuevo'])
			{
				$this->logs->actualizacion_de_datos($id, $evento, $this->input->input_stream('comentario'));
			}
		}
	}

	public function eliminar($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->Bodega_elementos_mw_model->update($id, array('eliminado' => 1)))
			{
				$evento['accion'] = 'Eliminación de Elemento Microondas';
				$evento['tabla'] = 'tb_bodega_elementos_mw';
				$evento['id'] = $id;

				$this->logs->actualizacion_de_datos($id, $evento, $this->input->input_stream('comentario'));
			}
		}
		else
		{
			echo json_encode(array('error' => TRUE, 'message' => 'Su sesión ha expirado.'));
		}
	}
}