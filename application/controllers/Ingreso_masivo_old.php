<?php
class Ingreso_masivo extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	/********************************************************************************************************/
	/*							Plantilla General															*/
	/********************************************************************************************************/

	public function plantilla_status_site()
	{
		$this->load->library("excel");

		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla")
									->setSubject("Plantilla")
									->setDescription("Plantilla básica para actualización del Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Status Site');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Prioridad');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Dirección');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Departamento');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Provincia');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Distrito');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Latitud');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Longitud');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Tipo de torre');
		$this->excel->getActiveSheet()->setCellValue('K1', 'Altura de torre (m)');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Factor Uso de la Torre (%)');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Tipo de estación');
		$this->excel->getActiveSheet()->setCellValue('N1', 'Altura de edificación (m)');
		$this->excel->getActiveSheet()->setCellValue('O1', 'Mimetizado');
		$this->excel->getActiveSheet()->setCellValue('P1', 'Tipo de mimetizado');
		$this->excel->getActiveSheet()->setCellValue('Q1', 'Ubicación de equipos');
		$this->excel->getActiveSheet()->setCellValue('R1', 'In-building');
		$this->excel->getActiveSheet()->setCellValue('S1', 'Coubicado');
		$this->excel->getActiveSheet()->setCellValue('T1', 'Coubicado en');
		$this->excel->getActiveSheet()->setCellValue('U1', 'Nombre sitio coubicador');
		$this->excel->getActiveSheet()->setCellValue('V1', 'Código torrera');
		$this->excel->getActiveSheet()->setCellValue('W1', 'Operador coubicado');
		$this->excel->getActiveSheet()->setCellValue('X1', 'Nombre para operador coubicado');
		$this->excel->getActiveSheet()->setCellValue('Y1', 'Dorsal');
		$this->excel->getActiveSheet()->setCellValue('Z1', 'Agregador');
		$this->excel->getActiveSheet()->setCellValue('AA1', 'Estado On Air');
		$this->excel->getActiveSheet()->setCellValue('AB1', 'Fecha On Air');
		$this->excel->getActiveSheet()->setCellValue('AC1', 'Año de construcción');
		$this->excel->getActiveSheet()->setCellValue('AD1', 'Proyecto');
		$this->excel->getActiveSheet()->setCellValue('AE1', 'Contrata constructora');
		$this->excel->getActiveSheet()->setCellValue('AF1', 'Tipo de site para facturación');
		$this->excel->getActiveSheet()->setCellValue('AG1', 'Proveedor de mantenimiento');
		$this->excel->getActiveSheet()->setCellValue('AH1', 'Consideraciones de acceso');
		$this->excel->getActiveSheet()->setCellValue('AI1', 'Acceso libre 24h');
		$this->excel->getActiveSheet()->setCellValue('AJ1', 'Nivel de riesgo');
		$this->excel->getActiveSheet()->setCellValue('AK1', 'Consideraciones');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A1:AK1")
										->applyFromArray($styleArray);

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true);

		/*	Hoja de Contratos	*/

		$csheet = 1;

		$this->excel->createSheet($csheet);
		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Contratos');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C1', 'ID contrato');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Arrendador');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Área');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Fecha inicio');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Fecha fin');

		$this->excel->getActiveSheet()->getStyle("A1:G1")
										->applyFromArray($styleArray);

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

		/*	Fin hoja de Contratos	*/

		/*	Hoja de Contactos de contratos	*/

		$csheet = 2;

		$this->excel->createSheet($csheet);
		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Contactos');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->setCellValue('C1', 'ID contrato');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Contacto');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Teléfono(s)');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Correo(s)');

		$this->excel->getActiveSheet()->getStyle("A1:F1")
										->applyFromArray($styleArray);

		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

		/*	Fin hoja de Contactos de contratos	*/

		$this->excel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Status_Site.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	/********************************************************************************************************/
	/*							Plantilla Personalizada														*/
	/********************************************************************************************************/

	public function plantilla_status_site_personalizada()
	{
		$data = $this->input->post();

		/*echo json_encode($data);*/

		$LETRAS = array(
				1 => "A",
				2 => "B",
				3 => "C",
				4 => "D",
				5 => "E",
				6 => "F",
				7 => "G",
				8 => "H",
				9 => "I",
				10 => "J",
				11 => "K",
				12 => "L",
				13 => "M",
				14 => "N",
				15 => "O",
				16 => "P",
				17 => "Q",
				18 => "R",
				19 => "S",
				20 => "T",
				21 => "U",
				22 => "V",
				23 => "W",
				24 => "X",
				25 => "Y",
				26 => "Z",
			);

		$this->load->library("excel");

		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla")
									->setSubject("Plantilla")
									->setDescription("Plantilla básica para actualización del Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;

		$this->excel->setActiveSheetIndex($csheet);

		$this->excel->getActiveSheet()->setTitle('Status Site');

		$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');

		$columnaActual = 3;

		if((isset($data["prioridad"])) && (($data["prioridad"] == "true") || ($data["prioridad"] == "on") || ($data["prioridad"] == TRUE)))
		{
			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;

			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];

			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Prioridad');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;
		}

		if((isset($data["ubicacion"])) && (($data["ubicacion"] == "true") || ($data["ubicacion"] == "on") || ($data["ubicacion"] == TRUE)))
		{
			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Dirección');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Departamento');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Provincia');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Distrito');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;
		}

		if((isset($data["coordenadas"])) && (($data["coordenadas"] == "true") || ($data["coordenadas"] == "on") || ($data["coordenadas"] == TRUE)))
		{
			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Latitud');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Longitud');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;
		}

		if((isset($data["contratos_contactos"])) && (($data["contratos_contactos"] == "true") || ($data["contratos_contactos"] == "on") || ($data["contratos_contactos"] == TRUE)))
		{
			$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

			$csheet = 1;

			$this->excel->createSheet($csheet);
			$this->excel->setActiveSheetIndex($csheet);

			$this->excel->getActiveSheet()->setTitle('Contratos');

			$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
			$this->excel->getActiveSheet()->setCellValue('C1', 'ID contrato');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Arrendador');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Área');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Fecha inicio');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Fecha fin');

			$this->excel->getActiveSheet()->getStyle("A1:G1")
											->applyFromArray($styleArray);

			$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

			$csheet = 2;

			$this->excel->createSheet($csheet);
			$this->excel->setActiveSheetIndex($csheet);

			$this->excel->getActiveSheet()->setTitle('Contactos');

			$this->excel->getActiveSheet()->setCellValue('A1', 'Código');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
			$this->excel->getActiveSheet()->setCellValue('C1', 'ID contrato');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Contacto');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Teléfono(s)');
			$this->excel->getActiveSheet()->setCellValue('F1', 'Correo(s)');

			$this->excel->getActiveSheet()->getStyle("A1:F1")
											->applyFromArray($styleArray);

			$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

			$this->excel->setActiveSheetIndex(0);
		}

		if((isset($data["torre_estacion"])) && (($data["torre_estacion"] == "true") || ($data["torre_estacion"] == "on") || ($data["torre_estacion"] == TRUE)))
		{
			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Tipo de torre');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Altura de torre (m)');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Factor Uso de la Torre (%)');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Tipo de estación');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Altura de edificación (m)');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Tipo de mimetizado');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;
		}

		if((isset($data["caracteristicas_especiales"])) && (($data["caracteristicas_especiales"] == "true") || ($data["caracteristicas_especiales"] == "on") || ($data["caracteristicas_especiales"] == TRUE)))
		{
			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'In-building');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Dorsal');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Agregador');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Estado On Air');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Fecha On Air');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;
		}

		if((isset($data["torrera_coubicador"])) && (($data["torrera_coubicador"] == "true") || ($data["torrera_coubicador"] == "on") || ($data["torrera_coubicador"] == TRUE)))
		{
			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Coubicado en');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Nombre sitio coubicador');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Código torrera');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;
		}

		if((isset($data["operador_coubicante"])) && (($data["operador_coubicante"] == "true") || ($data["operador_coubicante"] == "on") || ($data["operador_coubicante"] == TRUE)))
		{
			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Operador coubicado');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Nombre para operador coubicado');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;
		}

		if((isset($data["construccion"])) && (($data["construccion"] == "true") || ($data["construccion"] == "on") || ($data["construccion"] == TRUE)))
		{
			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Año de construcción');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Proyecto');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Contrata constructora');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;
		}

		if((isset($data["mantenimiento_acceso"])) && (($data["mantenimiento_acceso"] == "true") || ($data["mantenimiento_acceso"] == "on") || ($data["mantenimiento_acceso"] == TRUE)))
		{
			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Tipo de site para facturación');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Proveedor de mantenimiento');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Consideraciones de acceso');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Acceso libre 24h');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Nivel de riesgo');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;

			$cociente = floor($columnaActual / 27);
			$resto = $columnaActual % 27;
			$columna = ($cociente == 0)? $LETRAS[$resto] : $LETRAS[$cociente].$LETRAS[$resto+1];
			$this->excel->getActiveSheet()->setCellValue($columna.'1', 'Consideraciones');
			$this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columnaActual++;
		}

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						));

		$this->excel->getActiveSheet()->getStyle("A1:".$columna."1")
										->applyFromArray($styleArray);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Status_Site.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	/********************************************************************************************************/
	/*							Subida de plantilla															*/
	/********************************************************************************************************/

	public function upload_file($tipo)
	{
		$config = array(
				"upload_path"	=>	"./application/uploads/ingreso_masivo",
				"allowed_types"	=>	"xlsx",
				"max_size"		=>	"1024",
				"overwrite"		=>	true,
				"file_name"		=>	"plantilla_".$tipo
			);

		$this->load->library("upload",$config);

		if( $this->upload->do_upload($tipo) )
		{
			echo json_encode(array(
					"mensaje" => "Éxito",
					"resultado" => "Se subió el archivo.",
					"bool" => true
					));
		}
		else
		{
			echo json_encode(array(
					"mensaje" => "Fallido",
					"resultado" => $this->upload->display_errors(),
					"bool" => false
					));
		}
	}

	public function check_file($tipo)
	{
		$this->load->model("Ingreso_masivo_model");

		$file = "./application/uploads/ingreso_masivo/plantilla_".$tipo.".xlsx";

		$this->load->library("excel");
		$objPHPExcel = PHPExcel_IOFactory::load($file);

		$hojas = array(
				"fileTorre" => array(
						"Status Site",
						"Contratos",
						"Contactos"
					)
			);

		$resultados = array(
				"estado" => true,
				"mensajes" => array()
			);

		if( is_array($hojas[$tipo]) )
		{
			$filas = 0;
			foreach ($hojas[$tipo] as $hoja)
			{
				try{
					$objPHPExcel->setActiveSheetIndexByName($hoja);
				}catch(Exception $e){
					continue;
				}

				$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
				$arr_data = array();

				foreach ($cell_collection as $cell)
				{
					$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getCOlumn();
					$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
					$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

					if($row == 1)
					{
						$header[$row][$column] = $data_value;
					}
					else
					{
						if(!(isset($data_value)) && ($column == "A"))
						{
							break;
						}
						else
						{
							$arr_data[$row][$column] = $data_value;
						}
					}
				}

				$filas += count($arr_data);

				foreach ($arr_data as $fila => $infoFila)
				{
					$nombreSeparado = explode("_",$infoFila["B"]);
					$codNombre = $nombreSeparado[0];

					if($hoja == "Status Site")
					{
						$Departamento = "";
						$Provincia = "";
						$Distrito = "";
						$Coubicador = "";
						$Coubicante = "";
						$tipoEstacion = "";

						foreach ($infoFila as $columna => $campo)
						{
							if ($header[1][$columna] == "Código")
							{
								if(!(strlen($campo) == 7))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." el código del sitio no es válido, debería tener 7 dígitos.";
									array_push($resultados["mensajes"],$mensaje.$campo);
								}
								else
								{
									if(!($codNombre == $campo))
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el código no coincide con el código del nombre.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ($header[1][$columna] == "Nombre")
							{
								if( !( (preg_match("/01[3,6]\d{4}_[A-Z]{2}_\w+/",$campo)) || (preg_match("/02\d{5}_[A-Z]{2}_\w+/",$campo)) || (preg_match("/17\d{5}_[A-Z]{2}_\w+/",$campo)) ) )
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." el nombre del sitio no cumple con el patrón '013####_XX_Xyz' ó '016####_XX_Xyz' ó '17#####_XX_Xyz' ó '02#####_XX_Xyz'.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{

								}
							}
							elseif ($header[1][$columna] == "Prioridad")
							{
								if(!(preg_match("/P[0-4]{1}/", $campo) == 1))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." la prioridad no cumple con el patrón 'P[0-4]'.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{

								}
							}
							elseif ($header[1][$columna] == "Dirección")
							{
								if(!(preg_match("/[\w|\s|-|,|.|ñ|Ñ|á-úÁ-Ú]{10,}/", $campo) == 1))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." la dirección sólo acepta mayúsculas, minúsculas, números, (_), (-) y (.), además debe tener como mínimo 10 caracteres'.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{

								}
							}
							elseif ($header[1][$columna] == "Departamento")
							{
								$Departamento = $campo;
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el Departamento.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !($this->Ingreso_masivo_model->confirmar_departamento($campo)) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el Departamento ingresado no existe.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
							}
							elseif ($header[1][$columna] == "Provincia")
							{
								$Provincia = $campo;
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrita la Provincia.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !($this->Ingreso_masivo_model->confirmar_provincia($campo)) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." la Provincia ingresada no existe.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
							}
							elseif ($header[1][$columna] == "Distrito")
							{
								$Distrito = $campo;
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el Distrito.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !($this->Ingreso_masivo_model->confirmar_distrito($campo)) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el Distrito ingresado no existe.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										if( !($this->Ingreso_masivo_model->confirmar_zona_peru($Departamento,$Provincia,$Distrito)) )
										{
											$resultados["estado"] = false;
											$mensaje = "En la fila ".$fila." la zona ingresada (Departamento, Provincia y Distrito) no existe.";
											array_push($resultados["mensajes"],$mensaje);
										}
										else
										{

										}
									}
								}
							}
							elseif ($header[1][$columna] == "Latitud")
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito la Latitud.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( ((Float)$campo >= -90) && ((Float)$campo <= 90) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor de Latitud ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ($header[1][$columna] == "Longitud")
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito la Latitud.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( ((Float)$campo >= -180) && ((Float)$campo <= 180) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor de Longitud ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
							}
							elseif ($header[1][$columna] == "Arrendador")
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito Arrendador.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( preg_match_all("/[A-Za-zÁ-Úá-ú\s]{5,}/", $campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." los nombres de los arrendadores no son válidos o no tienen mínimo 5 caracteres.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
							}
							elseif ($header[1][$columna] == "Teléfono (Fijo/Móvil)")
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el número telefónico.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( preg_match_all("/[0-9-\/]{6,}/", $campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." los números de los arrendadores no son válidos o no tienen mínimo 6 caracteres.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
							}
							elseif ($header[1][$columna] == "Correo")
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el correo.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( preg_match_all("/([\w.]+)@((?:[\w]+\.)+)([A-Za-z]{2,4})/", $campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." los correos de los arrendadores no son válidos.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
							}
							elseif ( ($header[1][$columna] == "Fecha inicio contrato") || ($header[1][$columna] == "Fecha fin contrato") )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no estan escritas las fecha de inicio o fin de contrato.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( preg_match_all("/([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2,4})/", $campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." las fechas de inicio o fin de contrato no son válidos.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
							}
							elseif ( $header[1][$columna] == "Área piso (m2)" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el área contratada.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( ((Float)$campo >= 20) && ((Float)$campo <= 150) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del área contratada ingresada no es válida.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Tipo de torre" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el tipo de torre.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( $this->Ingreso_masivo_model->confirmar_tipo_torre($campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del tipo de torre ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Altura de torre (m)" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito la altura de la torre.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( ((Float)$campo >= 3) && ((Float)$campo <= 150) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor de la altura de la torre ingresada no es válida.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Factor Uso de la Torre (%)" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el factor de uso de la torre.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( ((Float)$campo >= 0) && ((Float)$campo <= 5) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del factor de uso de la torre ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Tipo de estación" )
							{
								$tipoEstacion = strtoupper($campo);
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el tipo de estación.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( $this->Ingreso_masivo_model->confirmar_tipo_estacion($campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del tipo de estación ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Altura de edificación (m)" )
							{
								if( $tipoEstacion == "GREENFIELD" )
								{
									if( (Float)$campo != 0 )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor de la altura de la edificación ingresada no es válida, debería ser 0 metros.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
								else
								{
									if( $tipoEstacion == "ROOFTOP" )
									{
										if( (Float)$campo < 1 )
										{
											$resultados["estado"] = false;
											$mensaje = "En la fila ".$fila." el valor de la altura de la edificación ingresada no es válida, debería ser mayor a 1 metro.";
											array_push($resultados["mensajes"],$mensaje);
										}
										else
										{

										}
									}
									else
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." se debe especificar el tipo de estación.";
										array_push($resultados["mensajes"],$mensaje);
									}
								}
							}
							elseif ( $header[1][$columna] == "Tipo de mimetizado" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el tipo de mimetizado.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( $this->Ingreso_masivo_model->confirmar_tipo_mimetizado($campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del tipo de mimetizado ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "In-building" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito si la estación es In-building o no.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( ($campo == "SI") || ($campo == "NO") ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del campo In-building no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Dorsal" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito si la estación es Dorsal o no.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( ($campo == "SI") || ($campo == "NO") ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del campo Dorsal no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Agregador" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito si la estación es Agregador o no.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( ($campo == "SI") || ($campo == "NO") ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del campo Agregador no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Estado On Air" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito se encuentra ON Air o no.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( ($campo == "SI") || ($campo == "NO") ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del campo Estado On Air no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Fecha On Air" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito la fecha ON Air.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									/*if( !( preg_match("/([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2,4})/", $campo) ) )*/
									$ffechaa = gmdate("Y-m-d",($campo - 25569) * 86400);
									if( !( preg_match("/([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})/", $ffechaa) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del campo Fecha On Air no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Coubicado en" )
							{
								$Coubicador = $campo;
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito si la estación es coubicada o no.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( $this->Ingreso_masivo_model->confirmar_coubicador($campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del campo Coubicado en ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Nombre sitio coubicador" )
							{
								if( $Coubicador != "N/A" )
								{
									if(!($campo != ""))
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." no está escrito el nombre del sitio del coubicador.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										if( !(strlen($campo) >= 3) )
										{
											$resultados["estado"] = false;
											$mensaje = "En la fila ".$fila." el valor del nombre del sitio del coubicador tiene menos de 3 caracteres.";
											array_push($resultados["mensajes"],$mensaje);
										}
										else
										{
											
										}
									}
								}
								else
								{
									if( $campo != "N/A" )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." si la estación es propia el nombre del sitio coubicador debería ser 'N/A'.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
							}
							elseif ( $header[1][$columna] == "Código torrera" )
							{
								if( $Coubicador != "N/A" )
								{
									if(!($campo != ""))
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." no está escrito el código de la torrera.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										if( !(strlen($campo) >= 3) )
										{
											$resultados["estado"] = false;
											$mensaje = "En la fila ".$fila." el código de la torrera tiene menos de 3 caracteres.";
											array_push($resultados["mensajes"],$mensaje);
										}
										else
										{
											
										}
									}
								}
								else
								{
									if( $campo != "N/A" )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." si la estación es propia el código de la torrera debería ser 'N/A'.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
							}
							elseif ( $header[1][$columna] == "Operador coubicado" )
							{
								$Coubicante = $campo;
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito si la estación coubica a un operador o no.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( $this->Ingreso_masivo_model->confirmar_coubicante($campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del campo Operador coubicado ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Nombre para operador coubicado" )
							{
								if( $Coubicante != "N/A" )
								{
									if(!($campo != ""))
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." no está escrito el nombre del sitio para el operador coubicado.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										if( !(strlen($campo) >= 3) )
										{
											$resultados["estado"] = false;
											$mensaje = "En la fila ".$fila." el valor del nombre del sitio para el operador coubicado tiene menos de 3 caracteres.";
											array_push($resultados["mensajes"],$mensaje);
										}
										else
										{
											
										}
									}
								}
								else
								{
									if( $campo != "N/A" )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." si la estación es propia el nombre del sitio para el operador coubicado debería ser 'N/A'.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
							}
							elseif ( $header[1][$columna] == "Año de construcción" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el año de construcción de la estación.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( (intval($campo) >= 1980) && (intval($campo) <= intval(date("Y"))) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del año de construcción ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Proyecto" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el proyecto.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( $this->Ingreso_masivo_model->confirmar_proyecto($campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del campo Proyecto ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Contrata constructora" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito la contrata constructora.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( $this->Ingreso_masivo_model->confirmar_contrata_constructora($campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del campo Proyecto ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Tipo de site para facturación" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el tipo de site para facturación.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( $this->Ingreso_masivo_model->confirmar_tipo_solucion($campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del tipo de site para facturación ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Proveedor de mantenimiento" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el proveedor de mantenimiento.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( $this->Ingreso_masivo_model->confirmar_proveedor_mantenimiento($campo) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el proveedor de mantenimiento ingresado no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Consideraciones de acceso" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no estan escritas las consideraciones de acceso, si noy consideraciones escriba 'NINGUNA'.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( strlen($campo) >= 7 ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el campo consideraciones de acceso debe tener como mínimo 7 caracteres.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Acceso libre 24h" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito si el sitio tiene acceso libre las 24 horas o no.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( ($campo == "SI") || ($campo == "NO") ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el valor del campo Acceso libre 24h no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Nivel de riesgo" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no está escrito el nivel de riesgo de la estación.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( (intval($campo) >= 1) || (intval($campo) <= 9) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el nivel de riesgo no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Consideraciones" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." no estan escritas las consideraciones, si noy consideraciones escriba 'NINGUNA'.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !( strlen($campo) >= 7 ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." el campo consideraciones de acceso debe tener como mínimo 7 caracteres.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
						}
					}
					elseif($hoja == "Contratos")
					{
						foreach ($infoFila as $columna => $campo)
						{
							if ($header[1][$columna] == "Código")
							{
								if(!(strlen($campo) == 7))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contratos' el código del sitio no es válido, debería tener 7 dígitos.";
									array_push($resultados["mensajes"],$mensaje.$campo);
								}
								else
								{
									if(!($codNombre == $campo))
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." de la hoja 'Contratos' el código no coincide con el código del nombre.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
							}
							elseif ($header[1][$columna] == "Nombre")
							{
								if( !( (preg_match("/01[3,6]\d{4}_[A-Z]{2}_\w+/",$campo)) || (preg_match("/02\d{5}_[A-Z]{2}_\w+/",$campo)) || (preg_match("/17\d{5}_[A-Z]{2}_\w+/",$campo)) ) )
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contratos' el nombre del sitio no cumple con el patrón '013####_XX_Xyz' ó '016####_XX_Xyz' ó '17#####_XX_Xyz' ó '02#####_XX_Xyz'.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{

								}
							}
							elseif ($header[1][$columna] == "ID contrato")
							{
								if( !( preg_match("/GL[R,X]-\d{3}\/\d{2}/",$campo) ) )
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contratos' el ID del contrato no cumple con el patrón 'GLR-###/##' ó 'GLX-###/##'.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{

								}
							}
							elseif ($header[1][$columna] == "Arrendador")
							{
								if($campo == "")
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contratos' no está escrito el nombre del arrendador.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !(strlen($campo) >= 3) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." de la hoja 'Contratos' el nombre del arrendador tiene menos de 3 caracteres.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ($header[1][$columna] == "Área")
							{
								if( !((Float)$campo >= 0) )
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contratos' el valor del área no es válida.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{

								}
							}
							elseif ( $header[1][$columna] == "Fecha inicio" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contratos' no está escrito la fecha de inicio del contrato.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									$ffechaa = gmdate("Y-m-d",($campo - 25569) * 86400);
									if( !( preg_match("/([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})/", $ffechaa) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." de la hoja 'Contratos' el valor del campo Fecha inicio del contrato no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ( $header[1][$columna] == "Fecha fin" )
							{
								if(!($campo != ""))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contratos' no está escrito la fecha de fin del contrato.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									$ffechaa = gmdate("Y-m-d",($campo - 25569) * 86400);
									if( !( preg_match("/([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})/", $ffechaa) ) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." de la hoja 'Contratos' el valor del campo Fecha fin del contrato no es válido.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
						}
					}
					elseif ($hoja == "Contactos")
					{
						foreach ($infoFila as $columna => $campo)
						{
							if ($header[1][$columna] == "Código")
							{
								if(!(strlen($campo) == 7))
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contactos' el código del sitio no es válido, debería tener 7 dígitos.";
									array_push($resultados["mensajes"],$mensaje.$campo);
								}
								else
								{
									if(!($codNombre == $campo))
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." de la hoja 'Contactos' el código no coincide con el código del nombre.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{

									}
								}
							}
							elseif ($header[1][$columna] == "Nombre")
							{
								if( !( (preg_match("/01[3,6]\d{4}_[A-Z]{2}_\w+/",$campo)) || (preg_match("/02\d{5}_[A-Z]{2}_\w+/",$campo)) || (preg_match("/17\d{5}_[A-Z]{2}_\w+/",$campo)) ) )
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contactos' el nombre del sitio no cumple con el patrón '013####_XX_Xyz' ó '016####_XX_Xyz' ó '17#####_XX_Xyz' ó '02#####_XX_Xyz'.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{

								}
							}
							elseif ($header[1][$columna] == "ID contrato")
							{
								if( !( preg_match("/GL[R,X]-\d{3}\/\d{2}/",$campo) ) )
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contactos' el ID del contrato no cumple con el patrón 'GLR-###/##' ó 'GLX-###/##'.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{

								}
							}
							elseif ($header[1][$columna] == "Contacto")
							{
								if($campo == "")
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contactos' no está escrito el contacto.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{
									if( !(strlen($campo) >= 3) )
									{
										$resultados["estado"] = false;
										$mensaje = "En la fila ".$fila." de la hoja 'Contactos' el contacto tiene menos de 3 caracteres.";
										array_push($resultados["mensajes"],$mensaje);
									}
									else
									{
										
									}
								}
							}
							elseif ($header[1][$columna] == "Teléfono(s)")
							{
								if( !( preg_match("/[0-9-]{6,}/",$campo) ) )
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contactos' en el campo Teléfono(s) hay datos inválidos.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{

								}
							}
							elseif ($header[1][$columna] == "Correo(s)")
							{
								if( !( preg_match("/([\w.]+)@((?:[\w]+\.)+)([A-Za-z]{2,4})/",$campo) ) )
								{
									$resultados["estado"] = false;
									$mensaje = "En la fila ".$fila." de la hoja 'Contactos' en el campo Correo(s) hay datos inválidos.";
									array_push($resultados["mensajes"],$mensaje);
								}
								else
								{

								}
							}
						}
					}
				}
			}
		}
		else
		{
			$objPHPExcel->setActiveSheetIndexByName($hojas[$tipo]);

			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
		}

		if($resultados["estado"])
		{
			$mensaje = "Filas válidas (".$filas.")";
			array_push($resultados["mensajes"],$mensaje);
		}

		echo json_encode($resultados);

		if(!($resultados["estado"]))
		{
			unlink("./application/uploads/ingreso_masivo/plantilla_".$tipo.".xlsx");
		}
	}

	public function update_file($tipo)
	{
		$this->load->model("Ingreso_masivo_model");

		$file = "./application/uploads/ingreso_masivo/plantilla_".$tipo.".xlsx";

		$this->load->library("excel");
		$objPHPExcel = PHPExcel_IOFactory::load($file);

		$hojas = array(
				"fileTorre" => array(
						"Status Site",
						"Contratos",
						"Contactos"
					)
			);

		$this->load->library('session');
		$session = $this->session->userdata();

		$idIngresoMasivo = $this->Ingreso_masivo_model->registrar_ingreso_masivo($session['id_usuario'],"tb_status_site");

		$resultado = 0;

		if( is_array($hojas[$tipo]) )
		{
			foreach ($hojas[$tipo] as $hoja)
			{
				try{
					$objPHPExcel->setActiveSheetIndexByName($hoja);
				}catch(Exception $e){
					continue;
				}

				$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
				$arr_data = array();

				foreach ($cell_collection as $cell)
				{
					$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getCOlumn();
					$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
					$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

					if($row == 1)
					{
						$header[$row][$column] = $data_value;
					}
					else
					{
						if(!(isset($data_value)) && ($column == "A"))
						{
							break;
						}
						else
						{
							$arr_data[$row][$column] = $data_value;
						}
					}
				}

				if($idIngresoMasivo != FALSE)
				{
					foreach ($arr_data as $fila => $infoFila)
					{
						$nombreSeparado = explode("_",$infoFila["B"]);
						$codNombre = $nombreSeparado[0];

						if($hoja == "Status Site")
						{
							$Departamento = "";
							$Provincia = "";
							$Distrito = "";
							$Coubicador = "";
							$Coubicante = "";
							$tipoEstacion = "";

							$ingreso = array();
							$ingreso['ingreso_masivo'] = $idIngresoMasivo;

							foreach ($infoFila as $columna => $campo)
							{
								$ingresoMasivo = array();
								if ($header[1][$columna] == "Nombre")
								{
									$resp = $this->Ingreso_masivo_model->get_sitio($campo);
									$ingreso["status_site"] = $resp["id"];
								}
								if ($header[1][$columna] == "Prioridad")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["prioridad"] = intval(substr($campo,1));
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Dirección")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["direccion"] = $campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Departamento")
								{
									$Departamento = $campo;
								}
								if ($header[1][$columna] == "Provincia")
								{
									$Provincia = $campo;
								}
								if ($header[1][$columna] == "Distrito")
								{
									$Distrito = $campo;
									$ingresoMasivo = $ingreso;
									$resp = $this->Ingreso_masivo_model->get_zona_peru($Departamento,$Provincia,$Distrito);
									$ingresoMasivo["zona_peru"] = $resp['id'];
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Arrendador")
								{
									preg_match_all("/[A-Za-zÁ-Úá-ú\s]{5,}/", $campo, $coincidencias);
									$arrendadores = $coincidencias[0];
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["arrendador"] = json_encode($arrendadores);
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Teléfono (Fijo/Móvil)")
								{
									preg_match_all("/[0-9-\/]{6,}/", $campo, $coincidencias);
									$telefonos = $coincidencias[0];
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["telefono"] = json_encode($telefonos);
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Correo")
								{
									preg_match_all("/([\w.]+)@((?:[\w]+\.)+)([A-Za-z]{2,4})/", $campo, $coincidencias);
									$correos = $coincidencias[0];
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["correo"] = json_encode($correos);
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Fecha inicio contrato")
								{
									preg_match_all("/([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2,4})/", $campo, $coincidencias);
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["fecha_inicio_contrato"] = $coincidencias[3][0]."-".$coincidencias[2][0]."-".$coincidencias[1][0];
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Fecha fin contrato")
								{
									preg_match_all("/([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2,4})/", $campo, $coincidencias);
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["fecha_fin_contrato"] = $coincidencias[3][0]."-".$coincidencias[2][0]."-".$coincidencias[1][0];
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Área piso (m2)")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["area_piso"] = (Float)$campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Latitud")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["latitud"] = (Float)$campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Longitud")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["longitud"] = (Float)$campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Tipo de torre")
								{
									$resp = $this->Ingreso_masivo_model->get_tipo_torre($campo);
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["tipo_torre"] = $resp["id"];
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Altura de torre (m)")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["altura_torre"] = (Float)$campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Factor Uso de la Torre (%)")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["factor_uso_torre"] = (Float)$campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Tipo de estación")
								{
									$tipoEstacion = strtoupper($campo);
									$ingresoMasivo = $ingreso;
									if(strtolower($campo) == "greenfield")
									{
										$ingresoMasivo["greenfield_rooftop"] = 0;
									}
									else
									{
										$ingresoMasivo["greenfield_rooftop"] = 1;
									}
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Altura de edificación (m)")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["altura_edificacion"] = ($tipoEstacion == "ROOFTOP")? (Float)$campo : 0 ;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Tipo de mimetizado")
								{
									$resp = $this->Ingreso_masivo_model->get_camuflaje($campo);
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["camuflaje"] = $resp["id"];
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Ubicación de equipos")
								{
									$resp = $this->Ingreso_masivo_model->get_ubicacion_equipos($campo);
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["ubicacion_equipos"] = $resp["id"];
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "In-building")
								{
									$ingresoMasivo = $ingreso;
									if(strtoupper($campo) == "NO")
									{
										$ingresoMasivo["in_building"] = 0;
									}
									else
									{
										$ingresoMasivo["in_building"] = 1;
									}

									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Coubicado")
								{
									$ingresoMasivo = $ingreso;
									if(strtoupper($campo) == "NO")
									{
										$ingresoMasivo["coubicado"] = 0;
									}
									else
									{
										$ingresoMasivo["coubicado"] = 1;
									}

									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Coubicado en")
								{
									$resp = $this->Ingreso_masivo_model->get_coubicador($campo);
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["coubicador"] = $resp["id"];
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Nombre sitio coubicador")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["nombre_sitio_coubicador"] = $campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Código torrera")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["codigo_torrera"] = $campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Operador coubicado")
								{
									$resp = $this->Ingreso_masivo_model->get_coubicante($campo);
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["operador_coubicante"] = $resp["id"];
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Nombre para operador coubicado")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["nombre_sitio_coubicante"] = $campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Dorsal")
								{
									$ingresoMasivo = $ingreso;
									if(strtoupper($campo) == "NO")
									{
										$ingresoMasivo["dorsal"] = 0;
									}
									else
									{
										$ingresoMasivo["dorsal"] = 1;
									}

									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Agregador")
								{
									$ingresoMasivo = $ingreso;
									if(strtoupper($campo) == "NO")
									{
										$ingresoMasivo["agregador"] = 0;
									}
									else
									{
										$ingresoMasivo["agregador"] = 1;
									}

									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Estado On Air")
								{
									$ingresoMasivo = $ingreso;
									if(strtoupper($campo) == "NO")
									{
										$ingresoMasivo["estado"] = 1;
									}
									else
									{
										$ingresoMasivo["estado"] = 2;
									}

									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Fecha On Air")
								{
									$ffechaa = gmdate("Y-m-d",($campo - 25569) * 86400);
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["fecha_estado_on_air"] = $ffechaa;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Año de construcción")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["anio_construccion"] = intval($campo);
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Proyecto")
								{
									$resp = $this->Ingreso_masivo_model->get_proyecto($campo);
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["proyecto"] = $resp["id"];
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Contrata constructora")
								{
									$resp = $this->Ingreso_masivo_model->get_contrata_constructora($campo);
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["contrata_constructora"] = $resp["id"];
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Tipo de site para facturación")
								{
									$resp = $this->Ingreso_masivo_model->get_tipo_solucion($campo);
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["tipo_solucion"] = $resp["id"];
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Proveedor de mantenimiento")
								{
									$resp = $this->Ingreso_masivo_model->get_proveedor_mantenimiento($campo);
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["proveedor_mantenimiento"] = $resp["id"];
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Consideraciones de acceso")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["consideracion_acceso"] = $campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Acceso libre 24h")
								{
									$ingresoMasivo = $ingreso;
									if(strtoupper($campo) == "NO")
									{
										$ingresoMasivo["acceso_libre_24h"] = 0;
									}
									else
									{
										$ingresoMasivo["acceso_libre_24h"] = 1;
									}

									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Nivel de riesgo")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["nivel_riesgo"] = intval($campo);
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Consideraciones")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["consideraciones"] = $campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_status_site",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
							}
						}
						elseif ($hoja == "Contratos")
						{
							$ingreso = array();
							$ingreso['ingreso_masivo'] = $idIngresoMasivo;

							$sitio = 0;

							foreach ($infoFila as $columna => $campo)
							{
								$ingresoMasivo = array();
								if ($header[1][$columna] == "Nombre")
								{
									$resp = $this->Ingreso_masivo_model->get_sitio($campo);
									$sitio = $resp["id"];
									$ingreso["sitio"] = $sitio;
								}
								if ($header[1][$columna] == "ID contrato")
								{
									$idContrato = $this->Ingreso_masivo_model->get_id_contrato($sitio,$campo);
									$ingreso["contratos"] = ($idContrato != FALSE)? $idContrato["id"] : 0 ;

									$ingresoMasivo = $ingreso;
									$ingresoMasivo["id_contrato"] = $campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_contratos",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Arrendador")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["arrendador"] = $campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_contratos",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Área")
								{
									$ingresoMasivo = $ingreso;
									$ingresoMasivo["area"] = (Float)$campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_contratos",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Fecha inicio")
								{
									$ingresoMasivo = $ingreso;
									$ffechaa = gmdate("Y-m-d",($campo - 25569) * 86400);
									$ingresoMasivo["fecha_inicio"] = $ffechaa;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_contratos",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Fecha fin")
								{
									$ingresoMasivo = $ingreso;
									$ffechaa = gmdate("Y-m-d",($campo - 25569) * 86400);
									$ingresoMasivo["fecha_fin"] = $ffechaa;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_contratos",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
							}
						}
						elseif ($hoja == "Contactos")
						{
							$ingreso = array();
							$ingreso['ingreso_masivo'] = $idIngresoMasivo;

							$sitio = 0;
							$contrato = 0;

							foreach ($infoFila as $columna => $campo)
							{
								$ingresoMasivo = array();
								if ($header[1][$columna] == "Nombre")
								{
									$resp = $this->Ingreso_masivo_model->get_sitio($campo);
									$sitio = $resp["id"];
									$ingreso["status_site"] = $sitio;
								}
								if ($header[1][$columna] == "ID contrato")
								{
									$idContrato = $this->Ingreso_masivo_model->get_id_contrato($sitio,$campo);
									$ingreso["contratos"] = $idContrato["id"];

									$contrato = $idContrato["id"];
								}
								if ($header[1][$columna] == "Contacto")
								{
									$idContacto = $this->Ingreso_masivo_model->get_id_contacto($contrato,$campo);
									$ingreso["contactos_contrato"] = ($idContacto != FALSE)? $idContacto["id"] : 0 ;

									$ingresoMasivo = $ingreso;
									$ingresoMasivo["contacto"] = $campo;
									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_contactos_contrato",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Teléfono(s)")
								{
									$ingresoMasivo = $ingreso;
									
									preg_match_all("/[0-9-]{6,}/",$campo,$coincidencias);

									$telefono = array();
									foreach ($coincidencias[0] as $key => $elemento)
									{
										array_push($telefono,array("telefono" => $elemento));
									}

									$ingresoMasivo["telefono"] = json_encode($telefono);

									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_contactos_contrato",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
								if ($header[1][$columna] == "Correo(s)")
								{
									$ingresoMasivo = $ingreso;
									
									preg_match_all("/([\w.]+)@((?:[\w]+\.)+)([A-Za-z]{2,4})/",$campo,$coincidencias);

									$correo = array();
									foreach ($coincidencias[0] as $key => $elemento)
									{
										array_push($correo,array("correo" => $elemento));
									}

									$ingresoMasivo["correo"] = json_encode($correo);

									if( $this->Ingreso_masivo_model->ingresar_temporal("tb_temp_contactos_contrato",$ingresoMasivo) )
									{
										$resultado+=1;
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{

		}
		echo $resultado;
	}

	/********************************************************************************************************/
	/*							Funciones del Modelo														*/
	/********************************************************************************************************/

	public function confirmar_sitio($sitio)
	{
		$this->load->model("Ingreso_masivo_model");

		$respuesta = $this->Ingreso_masivo_model->confirmar_sitio( $sitio );

		echo ($respuesta)? "Existe" : "No existe";
	}

	public function listar()
	{
		$this->load->model("Ingreso_masivo_model");

		$listado = $this->Ingreso_masivo_model->listar();

		echo json_encode($listado);
	}

	public function listar_pendientes($elemento)
	{
		$this->load->model("Ingreso_masivo_model");

		$listado = $this->Ingreso_masivo_model->listar_pendientes("tb_".$elemento);

		echo json_encode($listado);
	}

	public function listar_detalles_ingreso_masivo($id,$elemento)
	{
		$this->load->model("Ingreso_masivo_model");

		$listado = $this->Ingreso_masivo_model->listar_detalles_ingreso_masivo($id,$elemento);

		echo json_encode($listado);
	}
}
?>