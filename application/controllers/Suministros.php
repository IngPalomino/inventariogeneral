<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suministros extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 9;
	}
	
	public function listar()
	{
		$this->load->model('Suministros_model');

		$listado = $this->Suministros_model->get_suministros();

		echo json_encode($listado);
	}

	public function listar_sitio($id) {
		$this->load->model('Suministros_model');

		$listado = $this->Suministros_model->get_suministros_sitio($id);

		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('Suministros_model');

		$suministro = $this->Suministros_model->edit($id);

		echo json_encode($suministro);
	}

	public function actualizar($id)
	{
		$this->load->model('Suministros_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$suministro = $this->Suministros_model->edit($id);

		if($this->Suministros_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Suministro",
					"tabla"			=> "tb_suministro",
					"id"			=> $id,
					"datos viejos"	=> $suministro,
					"datos nuevos"	=> $this->Suministros_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_suministros_model');
				$this->Logs_suministros_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$evento["sitio"] = $this->Suministros_model->get_sitio_suministro($id);
				$evento["link"] = "ver/sitios/energia/suministros/".$evento["sitio"]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('Suministros_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		$info["sitio"] = $id;

		$respuesta = $this->Suministros_model->insert($info);

		if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Creación de Suministro",
					"tabla"		=> "tb_suministros"
				);

			$this->load->model('Logs_suministros_model');
			$this->Logs_suministros_model->ingreso_de_datos($session['id_usuario'],$evento);

			$this->load->model('Status_site_model');
			$evento["link"] = "ver/sitios/energia/suministros/".$id;
			$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($id);
			$evento["elemento"] = $this->Suministros_model->edit($respuesta["last_id"]);
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $id);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Suministros_model');

		$info = $this->input->input_stream();

		$info_tanque['eliminado'] = 1;

		if($this->Suministros_model->update($id, $info_tanque))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Suministro",
					"tabla"			=> "tb_suministros",
					"id"			=> $id
				);

			$this->load->model('Logs_suministros_model');
			$this->Logs_suministros_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$evento["sitio"] = $this->Suministros_model->get_sitio_suministro($id);
			$evento["link"] = "ver/sitios/energia/suministros/".$evento["sitio"]["id"];
			$evento["elemento"] = $this->Suministros_model->edit($id);
			$evento["comentario"] = $info["comentario"];
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}
}