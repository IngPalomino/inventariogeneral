<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Odus extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 5;
	}
	
	public function listar()
	{
		$this->load->model('Odus_model');

		$listado = $this->Odus_model->get_odus();

		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Odus_model');

		$listado = $this->Odus_model->get_odus_sitio($id);

		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('Odus_model');

		$odu = $this->Odus_model->edit($id);

		echo json_encode($odu);
	}

	public function actualizar($id)
	{
		$this->load->model('Odus_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$odu = $this->Odus_model->edit($id);

		if($this->Odus_model->update($id, $info))
		{
			$this->Odus_model->update_odus_enlace($id, $info); //Actualiza todas las ODUs relacionadas.
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de ODU",
					"tabla"			=> "tb_odus_enlaces",
					"id"			=> $id,
					"datos viejos"	=> $odu,
					"datos nuevos"	=> $this->Odus_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_odus_model');
				$this->Logs_odus_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$this->load->model('Logs_enlaces_model');
				$this->Logs_enlaces_model->actualizacion_de_datos($session['id_usuario'],$odu[0]["enlace"],$evento);

				$evento["sitio"] = $this->Odus_model->get_enlace_odu($id);
				$evento["link"] = "ver/sitios/infraestructura/enlaces/".$evento["sitio"][0]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"][0]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$evento["sitio"]["nombre_completo"] = $evento["sitio"][0]["nombre_completo"]." <==> ".$evento["sitio"][1]["nombre_completo"];
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function crear_sitio()
	{
		$this->load->model('Odus_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$respuesta = $this->Odus_model->insert($info);

		if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Creación de ODU",
					"tabla"		=> "tb_odus_enlaces"
				);

			$this->load->model('Logs_odus_model');
			$this->Logs_odus_model->ingreso_de_datos($session['id_usuario'],$evento);

			$evento["sitio"] = $this->Odus_model->get_enlace_odu($respuesta["last_id"]);
			$evento["elemento"] = $this->Odus_model->edit($respuesta["last_id"]);
			$evento["link"] = "ver/sitios/infraestructura/enlaces/".$evento["sitio"][0]["id"];
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"][0]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$evento["sitio"]["nombre_completo"] = $evento["sitio"][0]["nombre_completo"]." <==> ".$evento["sitio"][1]["nombre_completo"];
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Odus_model');

		$info = $this->input->input_stream();

		$info_odu['eliminado'] = 1;

		if($this->Odus_model->update($id, $info_odu))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de ODU",
					"tabla"			=> "tb_odus_enlaces",
					"id"			=> $id
				);

			$this->load->model('Logs_aires_acondicionados_model');
			$this->Logs_aires_acondicionados_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$evento["sitio"] = $this->Odus_model->get_enlace_odu($id);
			$evento["elemento"] = $this->Odus_model->edit($id);
			$evento["link"] = "ver/sitios/infraestructura/enlaces/".$evento["sitio"][0]["id"];
			$evento["comentario"] = $info["comentario"];
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"][0]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$evento["sitio"]["nombre_completo"] = $evento["sitio"][0]["nombre_completo"]." <==> ".$evento["sitio"][1]["nombre_completo"];
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}
}