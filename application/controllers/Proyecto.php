<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyecto extends CI_Controller {

	public function listar()
	{
		$this->load->model('Proyecto_model');

		$listado = $this->Proyecto_model->get_Proyecto();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Proyecto_model');

		$Proyecto = $this->Proyecto_model->edit($id);

		echo json_encode($Proyecto);
	}

	public function actualizar($id)
	{
		$this->load->model('Proyecto_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['proyecto'] = $this->input->input_stream('Proyecto');

		$this->Proyecto_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Proyecto_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['proyecto'] = $this->input->post('Proyecto');

		$this->Proyecto_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Proyecto_model');

		$this->Proyecto_model->delete($id);
	}
}