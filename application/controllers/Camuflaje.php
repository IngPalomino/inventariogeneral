<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Camuflaje extends CI_Controller {

	public function listar()
	{
		$this->load->model('Camuflaje_model');

		$listado = $this->Camuflaje_model->get_Camuflaje();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Camuflaje_model');

		$Camuflaje = $this->Camuflaje_model->edit($id);

		echo json_encode($Camuflaje);
	}

	public function actualizar($id)
	{
		$this->load->model('Camuflaje_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['camuflaje'] = $this->input->input_stream('Camuflaje');

		$this->Camuflaje_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Camuflaje_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['camuflaje'] = $this->input->post('Camuflaje');

		$this->Camuflaje_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Camuflaje_model');

		$this->Camuflaje_model->delete($id);
	}
}