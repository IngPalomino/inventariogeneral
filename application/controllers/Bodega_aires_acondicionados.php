<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_aires_acondicionados extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('auth');
		$this->load->model('Bodega_aires_acondicionados_model');
		$this->load->model('Logs_bodega_aires_acondicionados_model', 'logs');
	}

	public function almacenar()
	{
		$post = $this->input->post();

		if($this->auth->logged_in())
		{
			if($this->Bodega_aires_acondicionados_model->insert($post))
			{
				$evento["accion"] = "Almacenamiento de Aire Acondicionado";
				$evento["tabla"] = "tb_bodega_aires_acondicionados";

				$this->logs->ingreso_de_datos($this->session->userdata('id_usuario'), $evento);
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}

	public function mover($id)
	{
		if ($this->auth->logged_in())
		{
			$info = $this->input->input_stream(array('tipo_almacen', 'almacen'));
			$old = $this->Bodega_aires_acondicionados_model->edit($id);
			$info["almacen_previo"] = json_encode(array("tipo_almacen" => $old[0]["tipo_almacen"], "almacen" => $old[0]["almacen"]));

			if ($this->Bodega_aires_acondicionados_model->update($id, $info))
			{
				$new = $this->Bodega_aires_acondicionados_model->edit($id);

				$evento["accion"] = "Cambio de almacén de Aire Acondicionado.";
				$evento["tabla"] = "tb_bodega_aires_acondicionados";
				$evento["id"] = $id;
				$evento["almacen_antiguo"] = $info["almacen_previo"];
				$evento["almacen_nuevo"] = json_encode(array("tipo_almacen" => $new[0]["tipo_almacen"], "almacen" => $new[0]["almacen"]));

				if ($evento["almacen_antiguo"] != $evento["almacen_nuevo"])
				{
					$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento, $this->input->input_stream('comentario'));
				}
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}

	public function actualizar($id)
	{
		$post = $this->input->input_stream(array("tipo_refrigerante", "tipo_aires_acondicionados", "num_serie_aire_acon", "fecha_instalacion", "tipo_compresor", "tipo_condensador", "num_serie_condensador", "tipo_evaporador", "num_serie_evaporador", "estado"));

		if ($this->auth->logged_in())
		{
			$old = $this->Bodega_aires_acondicionados_model->edit($id);

			if ($this->Bodega_aires_acondicionados_model->update($id, $post))
			{
				$evento["accion"] = "Actualización de Aire Acondicionado.";
				$evento["tabla"] = "tb_bodega_aires_acondicionados";
				$evento["id"] = $id;
				$evento["datos_antiguos"] = $old;
				$evento["datos_nuevos"] = $this->Bodega_aires_acondicionados_model->edit($id);

				if ($evento["datos_antiguos"] != $evento["datos_nuevos"])
				{
					$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento);
				}
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}

	public function eliminar($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->Bodega_aires_acondicionados_model->update($id, array("eliminado" => 1)))
			{
				$evento["accion"] = "Eliminación de Aire Acondicionado";
				$evento["tabla"] = "tb_bodega_aires_acondicionados";
				$evento["id"] = $id;

				$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento, $this->input->input_stream('comentario'));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}
}