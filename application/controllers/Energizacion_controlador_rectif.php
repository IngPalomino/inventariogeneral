<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Energizacion_controlador_rectif extends CI_Controller {

	public function listar()
	{
		$this->load->model('Energizacion_controlador_rectif_model');

		$listado = $this->Energizacion_controlador_rectif_model->get_types();

		echo json_encode($listado);
	}

	public function id_tipo()
	{
		$this->load->model('Energizacion_controlador_rectif_model');

		$info = array();
		$info = $this->input->post();

		$tipo = $this->Energizacion_controlador_rectif_model->get_type($info);

		echo json_encode($tipo);
	}

	public function editar($id)
	{
		$this->load->model('Energizacion_controlador_rectif_model');

		$energizacion = $this->Energizacion_controlador_rectif_model->edit($id);

		echo json_encode($energizacion);
	}

	public function actualizar($id)
	{
		$this->load->model('Energizacion_controlador_rectif_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$this->Energizacion_controlador_rectif_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Energizacion_controlador_rectif_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Energizacion_controlador_rectif_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Energizacion_controlador_rectif_model');

		$this->Energizacion_controlador_rectif_model->delete($id);
	}
}