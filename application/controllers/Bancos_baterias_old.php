<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bancos_baterias extends CI_Controller {

	public function listar()
	{
		$this->load->model('Bancos_baterias_model');

		$listado = $this->Bancos_baterias_model->get_bancos_baterias();

		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Bancos_baterias_model');

		$listado = $this->Bancos_baterias_model->get_bancos_baterias_sitio($id);

		echo json_encode($listado);
	}
	
	public function listar_sitio_controlador($id)
	{
		$this->load->model('Bancos_baterias_model');

		$listado = $this->Bancos_baterias_model->get_bancos_baterias_sitio_controlador($id);

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Bancos_baterias_model');

		$bancos_baterias = $this->Bancos_baterias_model->edit($id);

		echo json_encode($bancos_baterias);
	}

	public function actualizar($id)
	{
		$this->load->model('Bancos_baterias_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();
		
		$info["celdas_controlador_rectif_bancos_baterias"] = json_decode($info["celdas_controlador_rectif_bancos_baterias"]);
		
		$info_banco["controlador_rectif"] = $info["controlador_rectif"];
		$info_banco["tipo_bancos_baterias"] = $info["tipo_bancos_baterias"];

		$bancos_baterias = $this->Bancos_baterias_model->edit($id);

		if($this->Bancos_baterias_model->update($id, $info_banco))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Bancos de baterías",
					"tabla"			=> "tb_controlador_rectif_bancos_baterias",
					"id"			=> $id,
					"datos viejos"	=> $bancos_baterias,
					"datos nuevos"	=> $this->Bancos_baterias_model->edit($id)
				);

			$this->load->model('Logs_controlador_rectif_bancos_baterias_model');
			$this->Logs_controlador_rectif_bancos_baterias_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
			
			if(count($info["celdas_controlador_rectif_bancos_baterias"]) > 0)
			{
				$this->load->model('Celdas_bancos_baterias_model');
				foreach(($info["celdas_controlador_rectif_bancos_baterias"]) as $celda)
				{
					if($this->Celdas_bancos_baterias_model->update($celda->id,$celda))
					{
						$evento = array(
							"accion"	=> "Actualización de Celda de Banco de Baterías",
							"tabla"		=> "tb_celdas_controlador_rectif_bancos_baterias",
							"id"		=> $celda->id
						);
						
						$this->load->model('Logs_celdas_controlador_rectif_bancos_baterias');
						$this->Logs_celdas_controlador_rectif_bancos_baterias->actualizacion_de_datos($session['id_usuario'],$celda->id,$evento);
					}
				}
			}
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('Bancos_baterias_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["sitio"] = $id;
		$cantidad = $info["cantidad_bancos"];
		
		$info_banco["sitio"] = $info["sitio"];
		$info_banco["controlador_rectif"] = $info["controlador_rectif"];
		$info_banco["tipo_bancos_baterias"] = $info["tipo_bancos_baterias"];

		for ($i=0; $i < $cantidad; $i++)
		{
			$respuesta = $this->Bancos_baterias_model->insert($info_banco);
			
			if($respuesta["resp"])
			{
				$this->load->library('session');
				$session = $this->session->userdata();

				$evento = array(
						"accion"	=> "Creación de Bancos de Baterías",
						"tabla"		=> "tb_controlador_rectif_bancos_baterias"
					);

				$this->load->model('Logs_controlador_rectif_bancos_baterias_model');
				$this->Logs_controlador_rectif_bancos_baterias_model->ingreso_de_datos($session['id_usuario'],$evento);
				
				$tipo = $this->Bancos_baterias_model->get_tipo_bancos_baterias($info_banco["tipo_bancos_baterias"]);
				
				for($j = 0; $j < $tipo[0]["celdas"]; $j++)
				{
					$info_celda["controlador_rectif_bancos_baterias"] = $respuesta["last_id"];
					$info_celda["tipo_bancos_baterias"] = $info_banco["tipo_bancos_baterias"];
					$info_celda["fecha_instalacion"] = $info["fecha_instalacion"];
					
					$this->load->model('Celdas_bancos_baterias_model');
					
					if($this->Celdas_bancos_baterias_model->insert($info_celda))
					{
						$this->load->library('session');
						$session = $this->session->userdata();
						
						$evento = array(
								"accion"	=> "Creación de Celda de Banco de Baterías",
								"tabla"		=> "tb_celdas_controlador_rectif_bancos_baterias"
							);
							
						$this->load->model('Logs_celdas_controlador_rectif_bancos_baterias_model');
						$this->Logs_celdas_controlador_rectif_bancos_baterias_model->ingreso_de_datos($session['id_usuario'],$evento);
					}
				}
			}
		}
	}

	public function crear_almacen_alquilado($id)
	{
		$this->load->model('Bancos_baterias_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Bancos_baterias_model->insert($info);
	}

	public function crear_almacen_propio($id)
	{
		$this->load->model('Bancos_baterias_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Bancos_baterias_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Bancos_baterias_model');

		$info = $this->input->input_stream();

		$info_banco['eliminado'] = 1;

		if($this->Bancos_baterias_model->update($id, $info_banco))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Banco de Batería",
					"tabla"			=> "tb_controlador_rectif_bancos_baterias",
					"id"			=> $id
				);

			$this->load->model('Logs_controlador_rectif_bancos_baterias_model');
			$this->Logs_controlador_rectif_bancos_baterias_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);
		}
	}
	
	public function eliminar_bancos($id)
	{
		$this->load->model('Bancos_baterias_model');
		
		$info = $this->input->input_stream();

		$info_banco['eliminado'] = 1;
		
		$where = array();
		$where["controlador_rectif"] = $id;
		
		if($this->Bancos_baterias_model->update_where($where, $info_banco))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Bancos de Baterías de un controlador de rectificador",
					"tabla"			=> "tb_controlador_rectif_bancos_baterias",
					"id"			=> $id
				);

			$this->load->model('Logs_controlador_rectif_bancos_baterias_model');
			$this->Logs_controlador_rectif_bancos_baterias_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);
		}
	}
	
	public function actualizar_bancos()
	{
		$this->load->model('Bancos_baterias_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();
		$info = json_decode($info["data"], true);
		
		foreach($info as $banco)
		{
			$bancos_baterias = $this->Bancos_baterias_model->edit($banco["id"]);

			$info_banco["id"] = $banco["id"];
			$info_banco["sitio"] = $banco["sitio"];
			$info_banco["controlador_rectif"] = $banco["controlador_rectif"];
			$info_banco["eliminado"] = $banco["eliminado"];
			
			if($this->Bancos_baterias_model->update($banco["id"], $info_banco))
			{
				$this->load->library('session');
				$session = $this->session->userdata();

				$evento = array(
						"accion"		=> "Actualización de Bancos de baterías",
						"tabla"			=> "tb_controlador_rectif_bancos_baterias",
						"id"			=> $banco["id"],
						"datos viejos"	=> $bancos_baterias,
						"datos nuevos"	=> $this->Bancos_baterias_model->edit($banco["id"])
					);

				$this->load->model('Logs_controlador_rectif_bancos_baterias_model');
				$this->Logs_controlador_rectif_bancos_baterias_model->actualizacion_de_datos($session['id_usuario'],$banco["id"],$evento);
				
				if(count($banco["celdas_controlador_rectif_bancos_baterias"]) > 0)
				{
					$this->load->model('Celdas_bancos_baterias_model');
					
					foreach($banco["celdas_controlador_rectif_bancos_baterias"] as $celda)
					{
						$celdas = $this->Celdas_bancos_baterias_model->edit($celda["id"]);
						
						if($this->Celdas_bancos_baterias_model->update($celda["id"], $celda))
						{
							$this->load->library('session');
							$session = $this->session->userdata();
							
							$evento = array(
									"accion"		=> "Actualización de Celdas de un Banco de Baterías",
									"tabla"			=> "tb_celdas_controlador_rectif_bancos_baterias",
									"id"			=> $celda["id"],
									"datos viejos"	=> $celdas,
									"datos nuevos"	=> $this->Celdas_bancos_baterias_model->edit($celda["id"])
								);
								
							$this->load->model('Logs_celdas_controlador_rectif_bancos_baterias_model');
							$this->Logs_celdas_controlador_rectif_bancos_baterias_model->actualizacion_de_datos($session['id_usuario'],$celda["id"],$evento);
						}
					}
				}
			}
		}
	}
	
	public function get_tipo_bancos_baterias($id)
	{
		$this->load->model('Bancos_baterias_model');
		$tipo = $this->Bancos_baterias_model->get_tipo_bancos_baterias($id);
		echo json_encode($tipo);
	}
}