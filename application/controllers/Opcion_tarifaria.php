<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Opcion_tarifaria extends CI_Controller {

	public function listar()
	{
		$this->load->model('Opcion_tarifaria_model');
		$listado = $this->Opcion_tarifaria_model->get_types();
		echo json_encode($listado);
	}

	public function id_tipo()
	{
		$this->load->model('Opcion_tarifaria_model');
		$info = $this->input->post();
		$tipo = $this->Opcion_tarifaria_model->get_type($info);
		echo json_encode($tipo);
	}

	public function editar($id)
	{
		$this->load->model('Opcion_tarifaria_model');
		$opcion = $this->Opcion_tarifaria_model->edit($id);
		echo json_encode($opcion);
	}

	public function actualizar($id)
	{
		$this->load->model('Opcion_tarifaria_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = $this->input->input_stream();
		$this->Opcion_tarifaria_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Opcion_tarifaria_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = $this->input->post();
		$this->Opcion_tarifaria_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Opcion_tarifaria_model');
		$this->Opcion_tarifaria_model->delete($id);
	}
}