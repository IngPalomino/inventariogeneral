<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_masivo_enlaces extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->modulo = 5;
	}

	public function plantilla_enlaces()
	{
		$this->load->library("excel");
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla Enlaces")
									->setSubject("Plantilla")
									->setDescription("Plantilla para ingresar información de Enlaces al Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle("Enlaces");

		$activeSheet->setCellValue('A1', 'Near End')
					->mergeCells("A1:C2")
					->setCellValue('D1', 'Far End')
					->mergeCells("D1:F2")
					->setCellValue('G1', 'Near End')
					->mergeCells("G1:J2")
					->setCellValue('K1', 'Far End')
					->mergeCells("K1:N2")
					->setCellValue('O1', 'Near End')
					->mergeCells("O1:S1")
					->setCellValue('O2', 'Antena(s)')
					->mergeCells("O2:S2")
					->setCellValue('T1', 'Far End')
					->mergeCells("T1:X1")
					->setCellValue('T2', 'Antena(s)')
					->mergeCells("T2:X2")
					->setCellValue('Y1', 'Configuración del Enlace')
					->mergeCells("Y1:AG2");
		
		$activeSheet->setCellValue('A3', 'Código')
					->setCellValue('B3', 'Nombre')
					->setCellValue('C3', 'IDU ID')
					->setCellValue('D3', 'Código')
					->setCellValue('E3', 'Nombre')
					->setCellValue('F3', 'IDU ID')
					->setCellValue('G3', 'ODU')
					->setCellValue('H3', 'Número de Serie')
					->setCellValue('I3', 'Ftx (MHz)')
					->setCellValue('J3', 'Ptx (dBm)')
					->setCellValue('K3', 'ODU')
					->setCellValue('L3', 'Número de Serie')
					->setCellValue('M3', 'Ftx (MHz)')
					->setCellValue('N3', 'Ptx (dBm)')
					->setCellValue('O3', 'Marca')
					->setCellValue('P3', 'Modelo')
					->setCellValue('Q3', 'Número de Serie')
					->setCellValue('R3', 'Altura (m)')
					->setCellValue('S3', 'Azimuth (°)')
					->setCellValue('T3', 'Marca')
					->setCellValue('U3', 'Modelo')
					->setCellValue('V3', 'Número de Serie')
					->setCellValue('W3', 'Altura (m)')
					->setCellValue('X3', 'Azimuth (°)')
					->setCellValue('Y3', 'Polaridad')
					->setCellValue('Z3', 'Configuración')
					->setCellValue('AA3', 'Ancho de banda/BW (MHz)')
					->setCellValue('AB3', 'Modulación')
					->setCellValue('AC3', 'Capacidad (Mbit/s)')
					->setCellValue('AD3', 'Modulación Adaptativa (AM)')
					->setCellValue('AE3', 'Modulación AM')
					->setCellValue('AF3', 'Capacidad AM (Mbit/s)')
					->setCellValue('AG3', 'Banda (GHz)');

		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => '000000'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '22b14c')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
						),
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
								'color' => array('rgb' => '000000')
							)
						));

		$activeSheet->getStyle("A1:C2")
					->applyFromArray($styleArray);
		$activeSheet->getStyle("G1:J2")
					->applyFromArray($styleArray);
		$activeSheet->getStyle("O1:S1")
					->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => '000000'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'ff0000')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
						),
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
								'color' => array('rgb' => '000000')
							)
						));

		$activeSheet->getStyle("D1:F2")
					->applyFromArray($styleArray);
		$activeSheet->getStyle("K1:N2")
					->applyFromArray($styleArray);
		$activeSheet->getStyle("T1:X1")
					->applyFromArray($styleArray);
										
		$styleArray = array(
						'font'  => array(
							'bold'  => true,
							'color' => array('rgb' => 'FFFFFF'),
							'size'  => 11,
							'name'  => 'Verdana'
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '6788D3')
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							'wrap' => true
						),
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
								'color' => array('rgb' => '000000')
							)
						));

		$activeSheet->getStyle("A3:AG3")
					->applyFromArray($styleArray);
		$activeSheet->getStyle("Y1:AG2")
					->applyFromArray($styleArray);
		$activeSheet->getStyle("O2:S2")
					->applyFromArray($styleArray);
		$activeSheet->getStyle("T2:X2")
					->applyFromArray($styleArray);

		$activeSheet->getStyle('A:A')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		$activeSheet->getStyle('D:D')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$activeSheet->getColumnDimension('A')->setAutoSize(true);
		$activeSheet->getColumnDimension('B')->setAutoSize(true);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setAutoSize(true);
		$activeSheet->getColumnDimension('E')->setAutoSize(true);
		$activeSheet->getColumnDimension('F')->setAutoSize(true);
		$activeSheet->getColumnDimension('G')->setAutoSize(true);
		$activeSheet->getColumnDimension('H')->setAutoSize(true);
		$activeSheet->getColumnDimension('I')->setAutoSize(true);
		$activeSheet->getColumnDimension('J')->setAutoSize(true);
		$activeSheet->getColumnDimension('K')->setAutoSize(true);
		$activeSheet->getColumnDimension('L')->setAutoSize(true);
		$activeSheet->getColumnDimension('M')->setAutoSize(true);
		$activeSheet->getColumnDimension('N')->setAutoSize(true);
		$activeSheet->getColumnDimension('O')->setAutoSize(true);
		$activeSheet->getColumnDimension('P')->setAutoSize(true);
		$activeSheet->getColumnDimension('Q')->setAutoSize(true);
		$activeSheet->getColumnDimension('R')->setAutoSize(true);
		$activeSheet->getColumnDimension('S')->setAutoSize(true);
		$activeSheet->getColumnDimension('T')->setAutoSize(true);
		$activeSheet->getColumnDimension('U')->setAutoSize(true);
		$activeSheet->getColumnDimension('V')->setAutoSize(true);
		$activeSheet->getColumnDimension('W')->setAutoSize(true);
		$activeSheet->getColumnDimension('X')->setAutoSize(true);
		$activeSheet->getColumnDimension('Y')->setAutoSize(true);
		$activeSheet->getColumnDimension('Z')->setAutoSize(true);
		$activeSheet->getColumnDimension('AA')->setAutoSize(true);
		$activeSheet->getColumnDimension('AB')->setAutoSize(true);
		$activeSheet->getColumnDimension('AC')->setAutoSize(true);
		$activeSheet->getColumnDimension('AD')->setAutoSize(true);
		$activeSheet->getColumnDimension('AE')->setAutoSize(true);
		$activeSheet->getColumnDimension('AF')->setAutoSize(true);
		$activeSheet->getColumnDimension('AG')->setAutoSize(true);

		$activeSheet->getSheetView()->setZoomScale(85);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Enlaces.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function plantilla_elementos_enlaces()
	{
		$this->load->model('Tipo_odu_model');
		$this->load->model('Tipo_antena_mw_model');
		$odus = $this->Tipo_odu_model->get_types();
		$antenas = $this->Tipo_antena_mw_model->get_types();

		$this->load->library("excel");
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla Enlaces")
									->setSubject("Plantilla")
									->setDescription("Plantilla para ingresar información de Enlaces al Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle("Elementos de Enlaces");

		$activeSheet->setCellValue("A1", "ODUs")
					->setCellValue("C1", "Antenas")
					->mergeCells("C1:G1")
					->setCellValue("A2", "Tipo")
					->setCellValue("C2", "Marca")
					->setCellValue("D2", "Modelo")
					->setCellValue("E2", "Diámetro")
					->setCellValue("F2", "Banda")
					->setCellValue("G2", "Peso");

		$styleArray = array(
				"font" => array(
						"bold"	=> true,
						"color"	=> array("rgb" => "FFFFFF"),
						"size"	=> 11,
						"name"	=> "Verdana"
					),
				"fill" => array(
						"type"	=> PHPExcel_Style_Fill::FILL_SOLID,
						"color"	=> array("rgb" => "6788D3")
					),
				"alignment" => array(
						"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					),
				'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
							'color' => array('rgb' => '000000')
						)
					)
			);

		$activeSheet->getStyle("A1:A2")->applyFromArray($styleArray);
		$activeSheet->getStyle("C1:G2")->applyFromArray($styleArray);

		$i = 3;

		foreach($odus as $fila)
		{
			$activeSheet->setCellValue("A".$i, $fila["tipo"]);

			$i++;
		}

		$i = 3;

		foreach($antenas["modelos"] as $fila)
		{
			$activeSheet->setCellValue("C".$i, $fila["marca"])
						->setCellValue("D".$i, $fila["modelo"])
						->setCellValue("E".$i, $fila["diametro"])
						->setCellValue("F".$i, $fila["banda"])
						->setCellValue("G".$i, $fila["peso"]);

			$i++;
		}

		$activeSheet->getColumnDimension("A")->setAutoSize(true);
		$activeSheet->getColumnDimension("C")->setAutoSize(true);
		$activeSheet->getColumnDimension("D")->setAutoSize(true);
		$activeSheet->getColumnDimension("E")->setAutoSize(true);
		$activeSheet->getColumnDimension("F")->setAutoSize(true);
		$activeSheet->getColumnDimension("G")->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Elementos_Enlaces.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function upload_file()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"])
		{
			$this->load->model('Autorizacion_grupos_model');

			if($this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo))
			{
				$post = $this->input->post();
				$config["upload_path"] = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo";
				$config["allowed_types"] = "xlsx";
				$config["max_size"] = "1024";
				$config["overwrite"] = FALSE;
				$config["file_name"] = "plantilla_".$post["tipo"]."_".date("d-m-Y-H-i-s");

				$this->load->library("upload", $config);

				if($this->upload->do_upload("archivo"))
				{
					echo json_encode(array(
							"error"		=> FALSE,
							"mensaje"	=> "Archivo subido con éxito.",
							"resultado"	=> $this->upload->data("file_name")
						));
				}
				else
				{
					echo json_encode(array(
							"error"		=> TRUE,
							"mensaje"	=> "No se pudo subir el archivo.",
							"resultado"	=> $this->upload->display_errors()
						));
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción.",
						"resultado"	=> "forbidden"
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró",
					"resultado"	=> "logged_off"
				));
		}
	}

	public function check_file()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"])
		{
			$this->load->model('Autorizacion_grupos_model');

			if($this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo))
			{
				$this->load->model('Ingreso_masivo_enlaces_model');
				$post = $this->input->post();
				$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"];
				/*$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/plantilla_enlaces.xlsx";*/
				
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						"error"			=> FALSE,
						"advertencia"	=> FALSE,
						"mensajes"		=> array(),
						"advertencias"	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				if(in_array("Enlaces", $sheetNames))
				{
					try{ $objPHPExcel->setActiveSheetIndexByName("Enlaces"); } catch(Exception $e){ continue; }

					$activeSheet = $objPHPExcel->getActiveSheet();
					$cell_collection = $activeSheet->getCellCollection();
					$cabeceras = array("Código", "Nombre", "IDU ID", "ODU", "Número de Serie", "Ftx (MHz)", "Ptx (dBm)", "Marca", "Modelo", "Altura (m)", "Azimuth (°)", "Polaridad", "Configuración", "Ancho de banda/BW (MHz)", "Modulación", "Capacidad (Mbit/s)", "Modulación Adaptativa (AM)", "Modulación AM", "Capacidad AM (Mbit/s)", "Banda (GHz)");
					$cabeceras_no_validas = array();
					$cabeceras_validas = array();
					$arr_data = array();

					foreach($cell_collection as $cell)
					{
						$column = $activeSheet->getCell($cell)->getColumn();
						$row = $activeSheet->getCell($cell)->getRow();
						$data_value = $activeSheet->getCell($cell)->getValue();

						if($row == 3)
						{
							$header[$row][$column] = $data_value;

							foreach($header as $filaHeader => $infoHeader)
							{
								if(!in_array($infoHeader[$column], $cabeceras))
								{
									array_push($cabeceras_no_validas, $infoHeader[$column]);
								}
								else
								{
									array_push($cabeceras_validas, $infoHeader[$column]);
								}
							}
						}
						elseif($row > 3)
						{
							if(!isset($data_value) && $column == "A")
							{
								/*break;*/
								$resultados["advertencia"] = TRUE;
								array_push($resultados["advertencias"], "La columna $column en la fila $row no tiene información, se obviará esta fila.");
							}
							elseif(in_array($header[3][$column], $cabeceras_no_validas))
							{
								
							}
							else
							{
								$arr_data[$row][$column] = $data_value;
							}
						}
					}

					foreach($cabeceras as &$value)
					{
						if(!in_array($value, $cabeceras_validas))
						{
							$resultados["error"] = TRUE;
							array_push($resultados["mensajes"], "La columna $value es obligatoria.");
						}
					}

					if(count($cabeceras_no_validas) > 0)
					{
						$cabeceras_no_validas = array_values(array_unique($cabeceras_no_validas));

						foreach($cabeceras_no_validas as $cabecera)
						{
							$resultados["advertencia"] = TRUE;
							array_push($resultados["advertencias"], "La cabecera $cabecera no corresponde a la plantilla original, se obviará esta columna.");
						}
					}

					/*echo json_encode($arr_data);*/
					$filas = count($arr_data);

					if(count($arr_data) > 0)
					{
						foreach($arr_data as $fila => $infoFila)
						{
							$nombreSeparadoNE = explode("_", $infoFila["B"]);
							$codigoNombreNE = $nombreSeparadoNE[0];
							$nombreSeparadoFE = explode("_", $infoFila["E"]);
							$codigoNombreFE = $nombreSeparadoFE[0];

							foreach($infoFila as $columna => $campo)
							{
								if($header[3][$columna] == "Código" && $columna == "A")
								{
									$codigone = $campo;
									if(strlen($campo) != 7)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código NE $campo en la fila $fila no es válido, debe tener 7 dígitos.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código NE $campo en la fila $fila debe contener únicamente números.");
									}
									else
									{
										if($codigoNombreNE != $campo)
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "El Código NE $campo en la fila $fila no coincide con el código del Nombre.");
										}
									}
								}
								elseif($header[3][$columna] == "Nombre" && $columna == "B")
								{
									$nombrene = $this->Ingreso_masivo_enlaces_model->esNombreCodigoValidos($codigone, $campo);
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Nombre NE en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_enlaces_model->esNombreCompletoValido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre NE $campo en la fila $fila no existe en la base de datos.");
									}
									elseif(!$nombrene)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre y Código de sitio NE en la fila $fila no existe en la base de datos.");
									}
									elseif(!$this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo, $nombrene["id"]))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Usted no tiene permiso para ingresar/actualizar información en el sitio NE $campo de la fila $fila.");
									}
								}
								elseif($header[3][$columna] == "IDU ID" && $columna == "C")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el NE ID Near End en la fila $fila.");
									}
									elseif(!preg_match("/^((\d{1,4})-(\d{1,5}))*$/", $campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El IDU ID Near End en la fila $fila debe seguir el patrón '1234-12345'.");
									}
									elseif($nombrene !== false)
									{
										$idu_1 = $this->Ingreso_masivo_enlaces_model->existeIDUEnSitio($nombrene["id"], $campo);
										if(!$idu_1)
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "La IDU $campo no existe en el sitio NE de la fila $fila.");
										}
									}
								}
								elseif($header[3][$columna] == "Código" && $columna == "D")
								{
									$codigofe = $campo;
									if(strlen($campo) != 7)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código FE $campo en la fila $fila no es válido, debe tener 7 dígitos.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código FE $campo en la fila $fila debe contener únicamente números.");
									}
									else
									{
										if($codigoNombreFE != $campo)
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "El Código FE $campo en la fila $fila no coincide con el código del Nombre.");
										}
									}
								}
								elseif($header[3][$columna] == "Nombre" && $columna == "E")
								{
									$nombrefe = $this->Ingreso_masivo_enlaces_model->esNombreCodigoValidos($codigofe, $campo);
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Nombre FE en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_enlaces_model->esNombreCompletoValido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre FE $campo en la fila $fila no existe en la base de datos.");
									}
									elseif(!$nombrefe)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre y Código de sitio FE en la fila $fila no existe en la base de datos.");
									}
									elseif(!$this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo, $nombrefe["id"]))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Usted no tiene permiso para ingresar/actualizar información en el sitio FE $campo de la fila $fila.");
									}
								}
								elseif($header[3][$columna] == "IDU ID" && $columna == "F")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el NE ID Far End en la fila $fila.");
									}
									elseif(!preg_match("/^((\d{1,4})-(\d{1,5}))*$/", $campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El IDU ID Far End en la fila $fila debe seguir el patrón '1234-12345'.");
									}
									elseif($nombrefe !== false)
									{
										$idu_2 = $this->Ingreso_masivo_enlaces_model->existeIDUEnSitio($nombrefe["id"], $campo);
										if(!$idu_2)
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "La IDU $campo no existe en el sitio FE de la fila $fila.");
										}
										elseif($this->Ingreso_masivo_enlaces_model->existeEnlace($idu_1["id"], $idu_2["id"]) !== false)
										{
											$resultados["advertencia"] = TRUE;
											array_push($resultados["advertencias"], "Ya existe un enlace entre las IDUs ".$idu_1["ne_id"]." y ".$idu_2["ne_id"]." en la fila $fila, se actualizará este Enlace.");
										}
									}
								}
								elseif($header[3][$columna] == "ODU" && $columna == "G")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Tipo de ODU NE en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_enlaces_model->esTipoOduValido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Tipo de ODU NE en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[3][$columna] == "Número de Serie" && $columna == "H")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Número de Serie NE en la fila $fila.");
									}
									elseif(preg_match("/[,\/\\\]+/", $campo))
									{
										$numeros = preg_split("/[\s,\/\\\]+/", $campo);
									}
								}
								elseif($header[3][$columna] == "Ftx (MHz)" && $columna == "I")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Ftx en la fila $fila.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Ftx $campo en la fila $fila debe contener únicamente números.");
									}
								}
								elseif($header[3][$columna] == "Ptx (dBm)" && $columna == "J")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Ptx en la fila $fila.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Ptx $campo en la fila $fila debe contener únicamente números.");
									}
								}
								elseif($header[3][$columna] == "ODU" && $columna == "K")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Tipo de ODU FE en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_enlaces_model->esTipoOduValido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Tipo de ODU FE en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[3][$columna] == "Número de Serie" && $columna == "L")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Número de Serie FE en la fila $fila.");
									}
									elseif(preg_match("/[,\/\\\]+/", $campo))
									{
										$numeros = preg_split("/[\s,\/\\\]+/", $campo);
									}
								}
								elseif($header[3][$columna] == "Ftx (MHz)" && $columna == "M")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Frx en la fila $fila.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Frx $campo en la fila $fila debe contener únicamente números.");
									}
								}
								elseif($header[3][$columna] == "Ptx (dBm)" && $columna == "N")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Prx en la fila $fila.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Prx $campo en la fila $fila debe contener únicamente números.");
									}
								}
								elseif($header[3][$columna] == "Marca" && $columna == "O")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la(s) Marca(s) de Antena(s) NE en la fila $fila.");
									}
									else
									{
										$marcas = preg_split("/[\s,]+/", $campo);

										foreach($marcas as $key => $value)
										{
											if(!$this->Ingreso_masivo_enlaces_model->esMarcaAntenaValida($value))
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "La marca ".((count($marcas) > 1)? $key+1 : "")." ".$value." de Antena NE en la fila $fila no existe en la base de datos.");
											}
										}
									}
								}
								elseif($header[3][$columna] == "Modelo" && $columna == "P")
								{
									if(!isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Modelo de Antena(s) NE en la fila $fila.");
									}
									else
									{
										$modelos = preg_split("/[\s,]+/", $campo);

										if(count($marcas) != count($modelos))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "La cantidad de Modelos de Antenas NE no coincide con la cantidad de Marcas de Antenas NE en la fila $fila.");
										}
										else
										{
											foreach($modelos as $key => $value)
											{
												if(!$this->Ingreso_masivo_enlaces_model->esModeloAntenaValido($value))
												{
													$resultados["error"] = TRUE;
													array_push($resultados["mensajes"], "El modelo ".((count($modelos) > 1)? $key+1 : "")." ".$value." de Antena NE en la fila $fila no existe en la base de datos.");
												}
												elseif($this->Ingreso_masivo_enlaces_model->esTipoAntenaValido($marcas[$key], $value) === false)
												{
													$resultados["error"] = TRUE;
													array_push($resultados["mensajes"], "El Tipo de Antena NE (Marca, Modelo) ".((count($modelos) > 1)? $key+1 : "")." en la fila $fila no existe en la base de datos.");
												}
											}
										}
									}
								}
								elseif($header[3][$columna] == "Número de Serie" && $columna == "Q")
								{
									if(!isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Número de Serie de Antena(s) NE en la fila $fila.");
									}
									else
									{
										$numeros = preg_split("/[\s,\/\\\]+/", $campo);

										if(count($modelos) != count($numeros))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "La cantidad de Números de Serie de Antenas NE no coincide con la cantidad de Modelos de Antenas NE en la fila $fila.");
										}
									}
								}
								elseif($header[3][$columna] == "Altura (m)" && $columna == "R")
								{
									if(!isset($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Altura de Antena(s) NE en la fila $fila.");
									}
									else
									{
										$alturas = preg_split("/[\s,\/\\\]+/", $campo);

										if(count($numeros) != count($alturas))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "La cantidad de Alturas de Antenas NE no coincide con la cantidad de Números de Serie de Antenas NE en la fila $fila.");
										}
										else
										{
											foreach($alturas as $key => $value)
											{
												if(!is_numeric($value))
												{
													$resultados["error"] = TRUE;
													array_push($resultados["mensajes"], "La Altura ".((count($alturas) > 1)? $key+1 : "")." de Antena NE en la fila $fila debe contener únicamente números.");
												}
											}
										}
									}
								}
								elseif($header[3][$columna] == "Azimuth (°)" && $columna == "S")
								{
									if(!isset($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Azimuth de Antena(s) NE en la fila $fila.");
									}
									else
									{
										$azimuths = preg_split("/[\s,\/\\\]+/", $campo);

										if(count($alturas) != count($azimuths))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "La cantidad de Azimuth de Antenas NE no coincide con la cantidad de Alturas de Antenas NE en la fila $fila.");
										}
										else
										{
											foreach($azimuths as $key => $value)
											{
												if(!is_numeric($value))
												{
													$resultados["error"] = TRUE;
													array_push($resultados["mensajes"], "El Azimuth ".((count($azimuths) > 1)? $key+1 : "")." de Antena NE en la fila $fila debe contener únicamente números.");
												}
											}
										}
									}
								}
								elseif($header[3][$columna] == "Marca" && $columna == "T")
								{
									if(!isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la(s) Marca(s) de Antena(s) FE en la fila $fila.");
									}
									else
									{
										$marcasfe = preg_split("/[\s,]+/", $campo);

										foreach($marcasfe as $key => $value)
										{
											if(!$this->Ingreso_masivo_enlaces_model->esMarcaAntenaValida($value))
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "La marca ".((count($marcasfe) > 1)? $key+1 : "")." ".$value." de Antena FE en la fila $fila no existe en la base de datos.");
											}
										}
									}
								}
								elseif($header[3][$columna] == "Modelo" && $columna == "U")
								{
									if(!isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Modelo de Antena(s) FE en la fila $fila.");
									}
									else
									{
										$modelosfe = preg_split("/[\s,]+/", $campo);

										if(count($marcasfe) != count($modelosfe))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "La cantidad de Modelos de Antenas FE no coincide con la cantidad de Marcas de Antenas FE en la fila $fila.");
										}
										else
										{
											foreach($modelosfe as $key => $value)
											{
												if(!$this->Ingreso_masivo_enlaces_model->esModeloAntenaValido($value))
												{
													$resultados["error"] = TRUE;
													array_push($resultados["mensajes"], "El modelo ".((count($modelosfe) > 1)? $key+1 : "")." ".$value." de Antena FE en la fila $fila no existe en la base de datos.");
												}
												elseif($this->Ingreso_masivo_enlaces_model->esTipoAntenaValido($marcasfe[$key], $value) === false)
												{
													$resultados["error"] = TRUE;
													array_push($resultados["mensajes"], "El Tipo de Antena FE (Marca, Modelo) ".((count($modelosfe) > 1)? $key+1 : "")." en la fila $fila no existe en la base de datos.");
												}
											}
										}
									}
								}
								elseif($header[3][$columna] == "Número de Serie" && $columna == "V")
								{
									if(!isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Número de Serie de Antena(s) FE en la fila $fila.");
									}
									else
									{
										$numerosfe = preg_split("/[\s,\/\\\]+/", $campo);

										if(count($modelosfe) != count($numerosfe))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "La cantidad de Números de Serie de Antenas FE no coincide con la cantidad de Modelos de Antenas FE en la fila $fila.");
										}
									}
								}
								elseif($header[3][$columna] == "Altura (m)" && $columna == "W")
								{
									if(!isset($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Altura de Antena(s) FE en la fila $fila.");
									}
									else
									{
										$alturasfe = preg_split("/[\s,\/\\\]+/", $campo);

										if(count($numerosfe) != count($alturasfe))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "La cantidad de Alturas de Antenas FE no coincide con la cantidad de Números de Serie de Antenas FE en la fila $fila.");
										}
										else
										{
											foreach($alturasfe as $key => $value)
											{
												if(!is_numeric($value))
												{
													$resultados["error"] = TRUE;
													array_push($resultados["mensajes"], "La Altura ".((count($alturasfe) > 1)? $key+1 : "")." de Antena FE en la fila $fila debe contener únicamente números.");
												}
											}
										}
									}
								}
								elseif($header[3][$columna] == "Azimuth (°)" && $columna == "X")
								{
									if(!isset($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Azimuth de Antena(s) FE en la fila $fila.");
									}
									else
									{
										$azimuthsfe = preg_split("/[\s,\/\\\]+/", $campo);

										if(count($alturasfe) != count($azimuthsfe))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "La cantidad de Azimuth de Antenas FE no coincide con la cantidad de Alturas de Antenas FE en la fila $fila.");
										}
										else
										{
											foreach($azimuthsfe as $key => $value)
											{
												if(!is_numeric($value))
												{
													$resultados["error"] = TRUE;
													array_push($resultados["mensajes"], "El Azimuth ".((count($azimuthsfe) > 1)? $key+1 : "")." de Antena FE en la fila $fila debe contener únicamente números.");
												}
											}
										}
									}
								}
								elseif($header[3][$columna] == "Polaridad" && $columna == "Y")
								{
									if($campo != "V" && $campo != "H" && $campo != "V+H")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Polaridad en la fila $fila debe ser 'V', 'H' o 'V+H'.");
									}
								}
								elseif($header[3][$columna] == "Configuración" && $columna == "Z")
								{
									if(!isset($columna) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Configuración en la fila $fila.");
									}
									elseif(!preg_match("/^((\d){1,2}\+(\d)( (XPIC))?( (SD|HSB|FD))?)$/", $campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Configuración en la fila $fila no es correcta, debe seguir el patrón ejemplo '1+0 XPIC SD' en mayúsculas.");
									}
								}
								elseif($header[3][$columna] == "Ancho de banda/BW (MHz)" && $columna == "AA")
								{
									if(!isset($columna) || !isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Ancho de Banda/BW en la fila $fila.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Ancho de Banda/BW en la fila $fila debe contener únicamente números.");
									}
								}
								elseif($header[3][$columna] == "Modulación" && $columna == "AB")
								{
									if(!isset($columna) || !isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Modulación en la fila $fila.");
									}
									elseif(preg_match("/[ ]/", $campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Modulación en la fila $fila no debe contener espacios.");
									}
									elseif(!preg_match("/(?<=[0-9])(?=[a-z]+)/i", $campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Modulación en la fila $fila no coincide con el formato ejemplo 16QAM.");
									}
									else
									{
										$modulacion = preg_split("/(?<=[0-9])(?=[a-z]+)/i", $campo);

										if(!is_numeric($modulacion[0]))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "La primera parte de la Modulación en la fila $fila debe contener únicamente números.");
										}
										elseif(!preg_match("/(((^QAM(?![ ]))|(^QPSK(?![ ]))))(Light|Strong|(?![\s\S]))/i", $modulacion[1]))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "La segunda parte de la Modulación en la fila $fila debe contener QAM, QAMStrong, QAMLight, QPSK o QPSKStrong.");
										}
									}
								}
								elseif($header[3][$columna] == "Capacidad (Mbit/s)" && $columna == "AC")
								{
									if(!isset($columna) || !isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Capacidad en la fila $fila.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Capacidad en la fila $fila debe contener únicamente números.");
									}
								}
								elseif($header[3][$columna] == "Modulación Adaptativa (AM)" && $columna == "AD")
								{
									if(!isset($columna) || !isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Modulación Adaptativa en la fila $fila (SI o NO).");
									}
									elseif(strtoupper($campo) != "SI" && strtoupper($campo) != "NO")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe especificar la Modulación Adaptativa en la fila $fila (SI o NO).");
									}
									else
									{
										$modulacion_adaptativa = strtoupper($campo);
									}
								}
								elseif($header[3][$columna] == "Modulación AM" && $columna == "AE")
								{
									if(!isset($columna) || !isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Modulación AM en la fila $fila (N/A si no aplica).");
									}
									else
									{
										if(isset($modulacion_adaptativa) && $modulacion_adaptativa == "SI")
										{
											if(preg_match("/[ ]/", $campo))
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "La Modulación AM en la fila $fila no debe contener espacios.");
											}
											elseif(!preg_match("/(?<=[0-9])(?=[a-z]+)/i", $campo))
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "La Modulación AM en la fila $fila no coincide con el formato ejemplo 16QAM.");
											}
											else
											{
												$modulacion_am = preg_split("/(?<=[0-9])(?=[a-z]+)/i", $campo);

												if(!is_numeric($modulacion_am[0]))
												{
													$resultados["error"] = TRUE;
													array_push($resultados["mensajes"], "La primera parte de la Modulación AM en la fila $fila debe contener únicamente números.");
												}
												elseif(!preg_match("/(((^QAM(?![ ]))|(^QPSK(?![ ]))))(Light|Strong|(?![\s\S]))/i", $modulacion_am[1]))
												{
													$resultados["error"] = TRUE;
													array_push($resultados["mensajes"], "La segunda parte de la Modulación AM en la fila $fila debe contener QAM, QAMStrong, QAMLight, QPSK o QPSKStrong.");
												}
											}
										}
										elseif(isset($modulacion_adaptativa) && $modulacion_adaptativa == "NO")
										{
											if($campo != "N/A")
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "La Modulación AM debe ser N/A en la fila $fila ya que la Modulación Adaptativa está establecida como NO.");
											}
										}
									}
								}
								elseif($header[3][$columna] == "Capacidad AM (Mbit/s)" && $columna == "AF")
								{
									if(!isset($columna) || !isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Capacidad AM en la fila $fila (N/A si no aplica).");
									}
									else
									{
										if(isset($modulacion_adaptativa) && $modulacion_adaptativa == "SI")
										{
											if(!is_numeric($campo))
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "La Capacidad AM en la fila $fila debe contener únicamente números.");
											}
										}
										elseif(isset($modulacion_adaptativa) && $modulacion_adaptativa == "NO")
										{
											if($campo != "N/A")
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "La Capacidad AM debe ser N/A en la fila $fila ya que la Modulación Adaptativa está establecida como NO.");
											}
										}
									}
								}
								elseif($header[3][$columna] == "Banda (GHz)" && $columna == "AG")
								{
									if(!isset($columna) || !isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Banda en la fila $fila.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Banda en la fila $fila debe contener únicamente números.");
									}
								}
							}
						}

						/*echo json_encode($arr_data);*/
					}
					else
					{
						$resultados["error"] = TRUE;
						array_push($resultados["mensajes"], "La hoja no contiene información");
					}
				}
				else
				{
					$resultados["error"] = TRUE;
					array_push($resultados["mensajes"], "No se encontró la hoja llamada 'Enlaces'");
				}

				if(!$resultados["error"])
				{
					array_push($resultados["mensajes"], "Filas válidas: ".count($arr_data));
				}

				echo json_encode($resultados);

				if($resultados["error"])
				{
					unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción."
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró."
				));
		}
	}

	public function insert_temp()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"])
		{
			$this->load->model('Autorizacion_grupos_model');

			if($this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo))
			{
				$this->load->model('Ingreso_masivo_enlaces_model');
				$idIngresoMasivo = $this->Ingreso_masivo_enlaces_model->registrar_ingreso_masivo($session['id_usuario']);
				$post = $this->input->post();
				$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"];
				/*$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/plantilla_enlaces.xlsx";*/
				
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						"error"			=> FALSE,
						"advertencia"	=> FALSE,
						"mensajes"		=> array(),
						"advertencias"	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				$resultado = 0;

				if(in_array("Enlaces", $sheetNames))
				{
					try{ $objPHPExcel->setActiveSheetIndexByName("Enlaces"); } catch(Exception $e){ continue; }

					$activeSheet = $objPHPExcel->getActiveSheet();
					$cell_collection = $activeSheet->getCellCollection();
					$cabeceras = array("Código", "Nombre", "IDU ID", "ODU", "Número de Serie", "Ftx (MHz)", "Ptx (dBm)", "Marca", "Modelo", "Altura (m)", "Azimuth (°)", "Polaridad", "Configuración", "Ancho de banda/BW (MHz)", "Modulación", "Capacidad (Mbit/s)", "Modulación Adaptativa (AM)", "Modulación AM", "Capacidad AM (Mbit/s)", "Banda (GHz)");
					$cabeceras_no_validas = array();
					$cabeceras_validas = array();
					$arr_data = array();

					foreach($cell_collection as $cell)
					{
						$column = $activeSheet->getCell($cell)->getColumn();
						$row = $activeSheet->getCell($cell)->getRow();
						$data_value = $activeSheet->getCell($cell)->getValue();

						if($row == 3)
						{
							$header[$row][$column] = $data_value;

							foreach($header as $filaHeader => $infoHeader)
							{
								if(!in_array($infoHeader[$column], $cabeceras))
								{
									array_push($cabeceras_no_validas, $infoHeader[$column]);
								}
								else
								{
									array_push($cabeceras_validas, $infoHeader[$column]);
								}
							}
						}
						elseif($row > 3)
						{
							if(!isset($data_value) && $column == "A")
							{
								/*break;*/
								$resultados["advertencia"] = TRUE;
								array_push($resultados["advertencias"], "La columna $column en la fila $row no tiene información, se obviará esta fila.");
							}
							elseif(in_array($header[3][$column], $cabeceras_no_validas))
							{
								
							}
							else
							{
								$arr_data[$row][$column] = $data_value;
							}
						}
					}

					/*echo json_encode($arr_data);*/
					$filas = count($arr_data);

					if($idIngresoMasivo !== FALSE)
					{
						foreach($arr_data as $fila => $infoFila)
						{
							$nombreSeparadoNE = explode("_", $infoFila["B"]);
							$codigoNombreNE = $nombreSeparadoNE[0];
							$nombreSeparadoFE = explode("_", $infoFila["E"]);
							$codigoNombreFE = $nombreSeparadoFE[0];

							foreach($infoFila as $columna => $campo)
							{
								$ingresoMasivo["ingreso_masivo"] = $idIngresoMasivo;

								if($header[3][$columna] == "Código" && $columna == "A")
								{
									$codigone = $campo;
								}
								elseif($header[3][$columna] == "Nombre" && $columna == "B")
								{
									$nombrene = $this->Ingreso_masivo_enlaces_model->esNombreCodigoValidos($codigone, $campo);
								}
								elseif($header[3][$columna] == "IDU ID" && $columna == "C")
								{
									$idu_1 = $this->Ingreso_masivo_enlaces_model->existeIDUEnSitio($nombrene["id"], $campo);
									$ingresoMasivo["idu_1"] = $idu_1["id"];
								}
								elseif($header[3][$columna] == "Código" && $columna == "D")
								{
									$codigofe = $campo;
								}
								elseif($header[3][$columna] == "Nombre" && $columna == "E")
								{
									$nombrefe = $this->Ingreso_masivo_enlaces_model->esNombreCodigoValidos($codigofe, $campo);
								}
								elseif($header[3][$columna] == "IDU ID" && $columna == "F")
								{
									$idu_2 = $this->Ingreso_masivo_enlaces_model->existeIDUEnSitio($nombrefe["id"], $campo);
									$ingresoMasivo["idu_2"] = $idu_2["id"];

									$actualizar = $this->Ingreso_masivo_enlaces_model->existeEnlace($idu_1["id"], $idu_2["id"]);

									$ingresoMasivo["id_actualizar"] = (($actualizar !== FALSE)? $actualizar["id"] : NULL);
								}
								elseif($header[3][$columna] == "ODU" && $columna == "G")
								{
									$tipo_odu_ne = $this->Ingreso_masivo_enlaces_model->esTipoOduValido($campo);
								}
								elseif($header[3][$columna] == "Número de Serie" && $columna == "H")
								{
									$numeros_odus_ne = preg_split("/[\s,\/\\\]+/", trim($campo));
								}
								elseif($header[3][$columna] == "Ftx (MHz)" && $columna == "I")
								{
									$ftx_ne = $campo;
								}
								elseif($header[3][$columna] == "Ptx (dBm)" && $columna == "J")
								{
									$ptx_ne = $campo;
								}
								elseif($header[3][$columna] == "ODU" && $columna == "K")
								{
									$tipo_odu_fe = $this->Ingreso_masivo_enlaces_model->esTipoOduValido($campo);
								}
								elseif($header[3][$columna] == "Número de Serie" && $columna == "L")
								{
									$numeros_odus_fe = preg_split("/[\s,\/\\\]+/", trim($campo));
								}
								elseif($header[3][$columna] == "Ftx (MHz)" && $columna == "M")
								{
									$ftx_fe = $campo;
								}
								elseif($header[3][$columna] == "Ptx (dBm)" && $columna == "N")
								{
									$ptx_fe = $campo;
								}
								elseif($header[3][$columna] == "Marca" && $columna == "O")
								{
									$marcas_ne = preg_split("/[\s,]+/", $campo);
								}
								elseif($header[3][$columna] == "Modelo" && $columna == "P")
								{
									$modelos_ne = preg_split("/[\s,]+/", $campo);
								}
								elseif($header[3][$columna] == "Número de Serie" && $columna == "Q")
								{
									$numeros_antenas_ne = preg_split("/[\s,\/\\\]+/", $campo);
								}
								elseif($header[3][$columna] == "Altura (m)" && $columna == "R")
								{
									$alturas_ne = preg_split("/[\s,\/\\\]+/", $campo);
								}
								elseif($header[3][$columna] == "Azimuth (°)" && $columna == "S")
								{
									$azimuths_ne = preg_split("/[\s,\/\\\]+/", $campo);
								}
								elseif($header[3][$columna] == "Marca" && $columna == "T")
								{
									$marcas_fe = preg_split("/[\s,]+/", $campo);
								}
								elseif($header[3][$columna] == "Modelo" && $columna == "U")
								{
									$modelos_fe = preg_split("/[\s,]+/", $campo);
								}
								elseif($header[3][$columna] == "Número de Serie" && $columna == "V")
								{
									$numeros_antenas_fe = preg_split("/[\s,\/\\\]+/", $campo);
								}
								elseif($header[3][$columna] == "Altura (m)" && $columna == "W")
								{
									$alturas_fe = preg_split("/[\s,\/\\\]+/", $campo);
								}
								elseif($header[3][$columna] == "Azimuth (°)" && $columna == "X")
								{
									$azimuths_fe = preg_split("/[\s,\/\\\]+/", $campo);
								}
								elseif($header[3][$columna] == "Polaridad" && $columna == "Y")
								{
									if($campo == "V")
									{
										$ingresoMasivo["polaridad"] = 1;
									}
									elseif($campo == "H")
									{
										$ingresoMasivo["polaridad"] = 2;
									}
									elseif($campo == "V+H")
									{
										$ingresoMasivo["polaridad"] = 3;
									}
								}
								elseif($header[3][$columna] == "Configuración" && $columna == "Z")
								{
									$ingresoMasivo["configuracion"] = strtoupper($campo);
								}
								elseif($header[3][$columna] == "Ancho de banda/BW (MHz)" && $columna == "AA")
								{
									$ingresoMasivo["ancho_banda"] = $campo;
								}
								elseif($header[3][$columna] == "Modulación" && $columna == "AB")
								{
									$ingresoMasivo["modulacion"] = $campo;
								}
								elseif($header[3][$columna] == "Capacidad (Mbit/s)" && $columna == "AC")
								{
									$ingresoMasivo["capacidad"] = $campo;
								}
								elseif($header[3][$columna] == "Modulación Adaptativa (AM)" && $columna == "AD")
								{
									$ingresoMasivo["modulacion_adaptativa"] = (($campo == "SI")? 1 : 0);
								}
								elseif($header[3][$columna] == "Modulación AM" && $columna == "AE")
								{
									$ingresoMasivo["modulacion_am"] = $campo;
								}
								elseif($header[3][$columna] == "Capacidad AM (Mbit/s)" && $columna == "AF")
								{
									$ingresoMasivo["capacidad_am"] = (($campo == "N/A")? 0 : $campo);
								}
								elseif($header[3][$columna] == "Banda (GHz)" && $columna == "AG")
								{
									$ingresoMasivo["banda"] = $campo;
								}
							}

							$respuesta = $this->Ingreso_masivo_enlaces_model->ingresar_temporal($ingresoMasivo);
							$odus_ne = (($actualizar !== FALSE)? $this->Ingreso_masivo_enlaces_model->existenODUs($actualizar["id"], $idu_1["id"]) : FALSE);
							$odus_fe = (($actualizar !== FALSE)? $this->Ingreso_masivo_enlaces_model->existenODUs($actualizar["id"], $idu_2["id"]) : FALSE);
							$antenas_ne = (($actualizar !== FALSE)? $this->Ingreso_masivo_enlaces_model->existenAntenas($actualizar["id"], $idu_1["id"]) : FALSE);
							$antenas_fe = (($actualizar !== FALSE)? $this->Ingreso_masivo_enlaces_model->existenAntenas($actualizar["id"], $idu_2["id"]) : FALSE);

							if($respuesta["resp"])
							{
								$odus_enlace = array();
								$antenas_mw_enlace = array();

								foreach($numeros_odus_ne as $key => $numero_serie)
								{
									$odu_ne["tipo_odu"] = $tipo_odu_ne["id"];
									$odu_ne["numero_serie"] = $numero_serie;
									$odu_ne["ftx"] = $ftx_ne;
									$odu_ne["ptx"] = $ptx_ne;
									$odu_ne["enlace"] = $respuesta["last_id"];
									$odu_ne["idu"] = $idu_1["id"];
									/*$odu_ne["id_actualizar"] = ((($odus_ne !== FALSE) && (count($odus_ne) == count($numeros_odus_ne)))? $odus_ne[$key]["id"] : NULL);*/

									array_push($odus_enlace, $odu_ne);
								}

								foreach($numeros_odus_fe as $key => $numero_serie)
								{
									$odu_fe["tipo_odu"] = $tipo_odu_fe["id"];
									$odu_fe["numero_serie"] = $numero_serie;
									$odu_fe["ftx"] = $ftx_fe;
									$odu_fe["ptx"] = $ptx_fe;
									$odu_fe["enlace"] = $respuesta["last_id"];
									$odu_fe["idu"] = $idu_2["id"];
									/*$odu_fe["id_actualizar"] = ((($odus_fe !== FALSE) && (count($odus_fe) == count($numeros_odus_fe)))? $odus_fe[$key]["id"] : NULL);*/

									array_push($odus_enlace, $odu_fe);
								}

								foreach($numeros_antenas_ne as $key => $value)
								{
									$tipo_antena_mw = $this->Ingreso_masivo_enlaces_model->esTipoAntenaValido($marcas_ne[$key], $modelos_ne[$key]);

									$antena_ne["tipo_antena_mw"] = $tipo_antena_mw["id"];
									$antena_ne["numero_serie"] = $value;
									$antena_ne["altura"] = $alturas_ne[$key];
									$antena_ne["azimuth"] = $azimuths_ne[$key];
									$antena_ne["enlace"] = $respuesta["last_id"];
									$antena_ne["idu"] = $idu_1["id"];
									/*$antena_ne["id_actualizar"] = ((($antenas_ne !== FALSE) && (count($antenas_ne) == count($numeros_antenas_ne)))? $antenas_ne[$key]["id"] : NULL);*/

									array_push($antenas_mw_enlace, $antena_ne);
								}

								foreach($numeros_antenas_fe as $key => $value)
								{
									$tipo_antena_mw = $this->Ingreso_masivo_enlaces_model->esTipoAntenaValido($marcas_fe[$key], $modelos_fe[$key]);

									$antena_fe["tipo_antena_mw"] = $tipo_antena_mw["id"];
									$antena_fe["numero_serie"] = $value;
									$antena_fe["altura"] = $alturas_fe[$key];
									$antena_fe["azimuth"] = $azimuths_fe[$key];
									$antena_fe["enlace"] = $respuesta["last_id"];
									$antena_fe["idu"] = $idu_2["id"];
									/*$antena_fe["id_actualizar"] = ((($antenas_fe !== FALSE) && (count($antenas_fe) == count($numeros_antenas_ne)))? $antenas_fe[$key]["id"] : NULL);*/

									array_push($antenas_mw_enlace, $antena_fe);
								}

								if(count($odus_enlace) > 0)
								{
									foreach($odus_enlace as $odu)
									{
										$this->Ingreso_masivo_enlaces_model->ingresar_temporal_odus($odu);
									}
								}

								if(count($antenas_mw_enlace) > 0)
								{
									foreach($antenas_mw_enlace as $antena)
									{
										$this->Ingreso_masivo_enlaces_model->ingresar_temporal_antenas($antena);
									}
								}

								$resultado++;
							}
						}

						/*echo json_encode($arr_data);*/
					}
					else
					{
						$resultados["error"] = TRUE;
						array_push($resultados["mensajes"], "La hoja no contiene información");
					}
				}
				else
				{
					$resultados["error"] = TRUE;
					array_push($resultados["mensajes"], "No se encontró la hoja llamada 'Enlaces'");
				}

				if(!$resultados["error"])
				{
					array_push($resultados["mensajes"], "Filas válidas: ".count($arr_data));
				}

				echo json_encode($resultados);

				if($resultados["error"])
				{
					unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción."
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró."
				));
		}
	}

	public function listar_solicitudes($usuario)
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"] && ($session["id_usuario"] == $usuario))
		{
			$this->load->model('Ingreso_masivo_enlaces_model');
			$listado = $this->Ingreso_masivo_enlaces_model->get_solicitudes($usuario);
			echo json_encode($listado);
		}
		else
		{
			show_404();
		}
	}
}