<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactos_contrato extends CI_Controller {

	public function listar()
	{
		$this->load->model('Contactos_contrato_model');

		$listado = $this->Contactos_contrato_model->get_contactos();

		echo json_encode($listado);
	}

	public function buscar()
	{
		$this->load->helper('form');
		$this->load->model('Contactos_contrato_model');

		$busqueda = $this->input->post('busqueda');
		$listado = $this->Contactos_contrato_model->look_for_contactos($busqueda);

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Contactos_contrato_model');

		$sitio = $this->Contactos_contrato_model->edit($id);

		echo json_encode($sitio);
	}

	public function actualizar($id)
	{
		$this->load->model('Contactos_contrato_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$config = array(
				array(
						'field' => 'contacto',
						'label' => 'Contacto',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe ingresar un %s.'
							)
					),
				array(
						'field' => 'telefono',
						'label' => 'Telefono',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe ingresar un %s.'
							)
					)
			);

		$error = array(
			'error' => FALSE);

		$data = $this->input->input_stream();

		$this->form_validation->set_data($data);
		$this->form_validation->set_rules($config);

		if($this->form_validation->run() === FALSE)
		{
			$error['error'] = validation_errors();
			echo json_encode($error);
		}
		else
		{
			$info = array();
			$input = $this->input->input_stream();
			foreach ($input as $key => $value)
			{
				if($key!='telefono' || $key!='correo')
				{
					$info[$key]=$value;
				}
				else
				{
					$info[$key]=json_encode($value);
				}
			}

			if($this->Contactos_contrato_model->update($id, $info))
			{
				$this->load->library('session');
				$session = $this->session->userdata();

				$evento = array(
						"accion"	=> "Actualización de Contacto",
						"tabla"		=> "tb_contactos_contrato",
						"id"		=> $id
					);

				$this->load->model('Logs_contactos_contrato_model');
				$this->Logs_contactos_contrato_model->actualizacion_de_datos($session['id_usuario'],$evento);
			}
		}
	}

	public function crear()
	{
		$this->load->model('Contactos_contrato_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$config = array(
				array(
						'field' => 'contacto',
						'label' => 'Contacto',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe ingresar un %s.'
							)
					),
				array(
						'field' => 'telefono',
						'label' => 'Telefono',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe ingresar un %s.'
							)
					)
			);

		$this->form_validation->set_rules($config);

		$error = array(
			'error' => FALSE);

		if($this->form_validation->run() === FALSE)
		{
			$error['error'] = validation_errors();
			echo json_encode($error);
		}
		else
		{
			$info = array();
			$input = $this->input->post();
			foreach ($input as $key => $value)
			{
				if($key!='telefono' || $key!='correo')
				{
					$info[$key]=$value;
				}
				else
				{
					$info[$key]=json_encode($value);
				}
			}

			if($this->Contactos_contrato_model->insert($info))
			{
				$this->load->library('session');
				$session = $this->session->userdata();

				$evento = array(
						"accion"	=> "Creación de Contacto",
						"tabla"		=> "tb_contratos"
					);

				$this->load->model('Logs_contactos_contrato_model');
				$this->Logs_contactos_contrato_model->ingreso_de_datos($session['id_usuario'],$evento,"tb_Contratos");
			}
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Contactos_contrato_model');

		$info = $this->input->input_stream();

		$info_contacto['eliminado'] = 1;

		if($this->Contactos_contrato_model->update($id, $info_contacto))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Contacto",
					"tabla"			=> "tb_contactos_contrato",
					"id"			=> $id
				);

			$this->load->model('Logs_contactos_contrato_model');
			$this->Logs_contactos_contrato_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);
		}
	}

	/************************************/
	/* FUNCIONES DE VALIDACIÓN DE DATOS */
	/************************************/

	
}