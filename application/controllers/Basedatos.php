<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Basedatos extends CI_Controller {
	public function listar()
	{
		$this->load->model('Basedatos_model');

		$listado = $this->Basedatos_model->show_tables();

		echo json_encode($listado);
	}

	public function listar_columnas($tabla)
	{
		$this->load->model('Basedatos_model');

		$listado = $this->Basedatos_model->show_columns($tabla);

		echo json_encode($listado);
	}
}