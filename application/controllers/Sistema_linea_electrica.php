<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema_linea_electrica extends CI_Controller {

	public function listar()
	{
		$this->load->model('Sistema_linea_electrica_model');

		$listado = $this->Sistema_linea_electrica_model->get_types();

		echo json_encode($listado);
	}

	public function id_tipo()
	{
		$this->load->model('Sistema_linea_electrica_model');

		$info = array();
		$info = $this->input->post();

		$tipo = $this->Sistema_linea_electrica_model->get_type($info);

		echo json_encode($tipo);
	}

	public function editar($id)
	{
		$this->load->model('Sistema_linea_electrica_model');

		$antena = $this->Sistema_linea_electrica_model->edit($id);

		echo json_encode($antena);
	}

	public function actualizar($id)
	{
		$this->load->model('Sistema_linea_electrica_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$this->Sistema_linea_electrica_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Sistema_linea_electrica_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Sistema_linea_electrica_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Sistema_linea_electrica_model');

		$this->Sistema_linea_electrica_model->delete($id);
	}
}