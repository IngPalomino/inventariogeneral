<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Antenas_gul_pendiente extends CI_Controller {

	private $modulo;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 3;
	}

	public function listar()
	{
		$this->load->model('Antenas_gul_pendiente_model');
		$listado = $this->Antenas_gul_pendiente_model->get_antenas_gul_pendiente();
		echo json_encode($listado);
	}

	public function listar_sitio($id){
	$this->load->model('Antenas_gul_pendiente_model');
	$listado = $this->Antenas_gul_pendiente_model->get_antenas_gul_pendiente_sitio($id);
		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Antenas_gul_pendiente_model');

		$antena_gul = $this->Antenas_gul_pendiente_model->edit($id);

		echo json_encode($antena_gul_pendiente);
	}

	public function actualizar($id)
	{
		$this->load->model('Antenas_gul_pendiente_model');
		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();
		
		$info_antena['numero_serie'] = $info['numero_serie'];
		$info_antena['sector'] = $info['sector'];
		$info_antena['altura'] = $info['altura'];
		$info_antena['azimuth'] = $info['azimuth'];
		$info_antena['tilt_electrico'] = $info['tilt_electrico'];
		$info_antena['tilt_mecanico'] = $info['tilt_mecanico'];
		$info_antena['tipo_antena_gul'] = $info['tipo_antena_gul'];
		$info_antena["info_telnet"] = $info["info_telnet"];

		$antena_gul = $this->Antenas_gul_pendiente_model->edit($id);

		//verificando altura y azimuth
        //extrayendo marca antena    
          $this->load->model('Tipo_antena_gul_model');
          $data= array('id' => $info['tipo_antena_gul']);       
          $datosTipo = $this->Tipo_antena_gul_model->get_type($data);          
          $marcaAntena =  $datosTipo[0]['marca'];
          if($marcaAntena =="TELNET"){
          //si  es TELNET
          $this->load->model('Antenas_gul_model');       
          $datosTorre = $this->Antenas_gul_model->get_antenas_gul_sitio($id);
           //buscamos coincidencias
	      $ctn = 0;	    
	      $azimuthEnviado = $info['azimuth'];
	      $alturaEnviado = $info['altura'];             
	      foreach ($datosTorre as $key => $torrevalue) {

	      if($id != $torrevalue['id']){

                 $altura_llenado = $torrevalue['altura'];
                 $alInicial = $altura_llenado - 0.5;
                 $alFinal = $altura_llenado + 0.5;

		    if( isset($torrevalue['info_telnet'][0]->azimuth) )
		    	{
		    	   //todos los registron que son telnet y llenos infotelnet[];  
                   if($alFinal<$alturaEnviado||$alInicial>$alturaEnviado){
                   	//si cumplem la altura
                   }else{
                   //no cumplem la altura
                   	 $ctn++;
                   }
		        }

              }

	        }

		    if($ctn >0){
		       //si hay espacio reservados similares
		          echo json_encode($ctn);
		    }else{
		       	//si no hay espacios reservados similares inserto, registro log
		          $this->update_gul_antena_pendiente($info_antena,$id);
		        }


          }else{
          //si No es TELNET
          $this->load->model('Antenas_gul_model');       
          $datosTorre = $this->Antenas_gul_model->get_antenas_gul_sitio($id);
           //buscamos coincidencias
	      $ctn = 0;
	      $acu = 0;
	     // $casilleropedido = $info['azimuth'].'-'.$info['altura'];
	      $azimuthEnviado = $info['azimuth'];
	      $alturaEnviado = $info['altura'];

	          foreach ($datosTorre as $key => $torrevalue) {
                 if($id != $torrevalue['id']){
		              $azimuth_llenado = $torrevalue['azimuth'];
		              $azInicial = $azimuth_llenado - 5;
		              $azFinal = $azimuth_llenado + 5;
		              $altura_llenado = $torrevalue['altura'];
	                  $alInicial = $altura_llenado - 0.5;
	                  $alFinal = $altura_llenado + 0.5;
		          	 if(($alInicial>$alturaEnviado) || ($alturaEnviado > $alFinal)){
		          	 	 // altura sin utilizar                     
		      	     }else{
		          	    //altura utilizada
		      	     	$ctn = $this->azimuth_giro($azimuth_reg,$azimuth_env);	             
	                    $acu += $ctn;
		          	 }
	          	}
	          }

		       if($acu >0){
		          	//si hay espacio reservados similares		       	   
		            echo json_encode($acu);

		          }else{
		          	//si no hay espacios reservados similares inserto, registro log
		             $this->update_gul_antena_pendiente($info_antena,$id);
		        }
          }

//verificando altura y azimuth
	}

	public function crear_sitio($id)
	{    
		$this->load->model('Antenas_gul_pendiente_model');		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$session = $this->session->userdata();	
		$info = array();
		$info = $this->input->post();
		$info_antena['tipo_antena_gul'] = $info['tipo_antena_gul'];
		$info_antena['numero_serie'] = $info['numero_serie'];
		$info_antena['sector'] = $info['sector'];
		$info_antena['altura'] = $info['altura'];
		$info_antena['azimuth'] = $info['azimuth'];
		$info_antena['tilt_electrico'] = $info['tilt_electrico'];
		$info_antena['tilt_mecanico'] = $info['tilt_mecanico'];
		$info_antena["info_telnet"] = $info["info_telnet"];
		$info_antena["idusuario"] = $session["id_usuario"];		
		$info_antena['sitio'] = $id;
        //Verificando Espacio en Torre 
        //extrayendo marca antena    
          $this->load->model('Tipo_antena_gul_model');
          $data= array('id' => $info['tipo_antena_gul']);       
          $datosTipo = $this->Tipo_antena_gul_model->get_type($data);          
          $marcaAntena =  $datosTipo[0]['marca'];
                 //antenas 
                  //GUL
		        $this->load->model('Antenas_gul_model');       
		        $datosTorreG = $this->Antenas_gul_model->get_antenas_gul_sitio($id);
                 //GulPendiente
		        $this->load->model('Antenas_gul_pendiente_model');
                $datosTorreGULP = $this->Antenas_gul_pendiente_model->get_antenas_gul_pendiente_sitio($id);
                 //Microondas
                 $idsitio = $id; 
                 $this->load->model('Antenas_mw_model');       
                 $datosTorreM = $this->Antenas_mw_model->get_antenas_microondas_sitio($idsitio);
                 //Microondas Pendiente
                 $datosTorreMP = $listado = $this->Antenas_mw_model->get_antenas_microondas_sitio_pendiente($idsitio);

          if($marcaAntena =="TELNET"){
                  //si  es TELNET     
		          //buscamos coincidencias
			      $ctnMP = $ctnGP = $ctnM = $acu = $ctn = 0;				     
			      $azimuthEnviado = $info['azimuth'];
			      $alturaEnviado = $info['altura'];  
			      $coincidencias = "";
                  $alturareport = "";
                  $ctn = $this->recorre_telnet($datosTorreG,$alturaEnviado);
                  $ctnGP = $this->recorre_telnet($datosTorreGULP,$alturaEnviado);
                  $ctnM = $this->recorre_telnet($datosTorreM,$alturaEnviado);
                  $ctnMP = $this->recorre_telnet($datosTorreMP,$alturaEnviado);
                  $acu = $ctn + $ctnGP + $ctnM + $ctnMP;
					   if($acu >0){
                         //si hay espacio reservados similares                            
					        echo json_encode($acu);
					   }else{
					     //si no hay espacios reservados similares inserto, registro log
					     $this->ingresar_antena_pendiente($info_antena,$id);
					   }
          }else{
          //si no es TELNET 
          //buscamos coincidencias 
	      $acu = $ctn = $ctnGP = $ctnM = $ctnMP = 0;
	      //altura y azimuth enviados por formulario 	     
	      $azimuth_env = $info['azimuth'];
	      $alturaEnviado = $info['altura'];
	      $report = "";	                
	      $ctn = $this->recorre_no_telnet($datosTorreG,$alturaEnviado,$azimuth_env); 
          $ctnGP = $this->recorre_no_telnet($datosTorreGULP,$alturaEnviado,$azimuth_env);
          $ctnM = $this->recorre_no_telnet($datosTorreM,$alturaEnviado,$azimuth_env);
          $ctnMP = $this->recorre_no_telnet($datosTorreMP,$alturaEnviado,$azimuth_env);     
	      $acu = $ctn + $ctnGP + $ctnM + $ctnMP;
            if($acu >0){
		          	//si hay espacio reservados similares		          	
		            echo json_encode($acu);		                      
		         }else{		          	         	
		          	$this->ingresar_antena_pendiente($info_antena,$id);
		         }
          }
          //Verificando Espacio en Torre
}


public function recorre_telnet($datosTorre,$alturaEnviado)
{  
    $ctn = 0;
    foreach ($datosTorre as $key => $torrevalue) {
		$altura_llenado = $torrevalue['altura'];
		$alInicial = $altura_llenado - 0.5;
		$alFinal = $altura_llenado + 0.5;
			//todos los registros que son telnet y llenos infotelnet[];  
			if($alFinal<$alturaEnviado||$alInicial>$alturaEnviado){
				//si cumplem la altura
			}else{
			//no cumplem la altura			                   	
			$ctn++;
			}		        
	}
	return $ctn;
}

public function recorre_no_telnet($datosTorre,$alturaEnviado,$azimuth_env)
{
$acu = $ctn = 0; 
              foreach ($datosTorre as $key => $torrevalue) {            
                //gul registrado pendiente
            	//azimuth registrado         	
	             $azimuth_reg = $torrevalue['azimuth'];
	             $azInicial = $azimuth_reg - 5;
	             $azFinal = $azimuth_reg + 5;
                 //altura registrada 
	             $altura_llenado = $torrevalue['altura'];
                 $alInicial = $altura_llenado - 0.5;
                 $alFinal = $altura_llenado + 0.5;

	          	 if(($alInicial>$alturaEnviado) || ($alturaEnviado > $alFinal)){
	          	 	 // altura sin utilizar                     
	      	     }else{
	          	    //altura utilizada
	      	     	$ctn = $this->azimuth_giro($azimuth_reg,$azimuth_env);	             
                    $acu += $ctn;
	          	 }
	           }
return $acu;
} 

    public function ingresar_antena_pendiente($info_antena,$id)
	{
	    $this->load->library('session');
		$session = $this->session->userdata();	
        $respuesta = $this->Antenas_gul_pendiente_model->insert($info_antena);
						if($respuesta["resp"])
						{							
							$evento = array(
									"accion"	=> "Creación de Antena GUL Pendiente",
									"tabla"		=> "tb_antenas_gul_pendiente"
							);
							$this->load->model('Logs_antenas_gul_pendiente_model');
							$this->Logs_antenas_gul_pendiente_model->ingreso_de_datos($session['id_usuario'],$evento);
							$this->load->model('Status_site_model');
							$evento["link"] = "ver/sitios/infraestructura/antenas/".$id;
							$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($id);
							$evento["elemento"] = $this->Antenas_gul_pendiente_model->edit($respuesta["last_id"]);
							$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $id);
							$cc = $this->custom->clean_cc($session["usuario"], $cc);
							// $this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
						}
	}


 public function update_gul_antena_pendiente($info_antena,$id)
	{

	if($this->Antenas_gul_pendiente_model->update($id, $info_antena))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Antena GUL Pendiente",
					"tabla"			=> "tb_antenas_gul_pendiente",
					"id"			=> $id,
					"datos viejos"	=> $antena_gul_pendiente,
					"datos nuevos"	=> $this->Antenas_gul_pendiente_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_antenas_gul_pendiente_model');
				$this->Logs_antenas_gul_pendiente_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$evento["sitio"] = $this->Antenas_gul_pendiente_model->get_sitio_antena($id);
				$evento["link"] = "ver/sitios/infraestructura/antenas/".$evento["sitio"]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}


	}


   public function azimuth_giro($azimuth_reg,$azimuth_env)	
	{
    //registrado data 		
 	$azimuth_i = $azimuth_reg - 5;
 	$azimuth_f = $azimuth_reg + 5;
     
 	$cont = 0;
 	$mensaje ='';
    $mensajeh ='';
 	if($azimuth_i>0){
			 		// si es positivo
			       if(($azimuth_env<$azimuth_i)||($azimuth_env>$azimuth_f)){
			       	  $mensaje = "Puede instalar";
			       	
			       }else{
			       	$mensaje = "No puede instalar";
			       
			       	  $cont++;

			       }
 	}else{
 	  // si es negativo el azimuth registrado;         
       $azimuth_i = 0;    
	   if(($azimuth_env<$azimuth_i)||($azimuth_env>$azimuth_f)){	   		
		  $mensaje = "Puede Instalar";
	   }else{
		  $mensaje = "No puede instalar";
		  $cont++;
		} 
      
 	}
    return $cont;
 	}

   








	
}