<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rectificadores extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 7;
	}

	public function listar()
	{
		$this->load->model('Rectificadores_model');

		$listado = $this->Rectificadores_model->get_controlador_rectif();

		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		if($this->input->is_ajax_request())
		{
			$this->load->model('Rectificadores_model');

			$listado = $this->Rectificadores_model->get_controlador_rectif_sitio($id);

			echo json_encode($listado);
		}
		else
		{
			echo "Error";
		}
	}
	
	public function listar_sitio_mso($id)
	{
		if($this->input->is_ajax_request())
		{
			$this->load->model('Rectificadores_model');

			$listado = $this->Rectificadores_model->get_controlador_rectif_sitio_mso($id);

			echo json_encode($listado);
		}
		else
		{
			echo "Error";
		}
	}



	public function listar_eliminado_sitio($id)
	{
		$this->load->model('Rectificadores_model');

		$listado = $this->Rectificadores_model->get_controlador_rectif_eliminado_sitio($id);

		echo json_encode($listado);
	}

	public function editar($id)
	{
	$this->load->model('Rectificadores_model');

	$controlador_rectif = $this->Rectificadores_model->edit($id);

		echo json_encode($controlador_rectif);
	}

	public function actualizar($id)
	{
		$this->load->model('Rectificadores_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();
		
		$info["modulos_rectif"] = json_decode($info["modulos_rectif"]);
		
		$info_rectificador["tipo_controlador_rectif"] = $info["tipo_controlador_rectif"];
		$info_rectificador["anio_servicio"] = $info["anio_servicio"];
		$info_rectificador["capacidad_maxima"] = $info["capacidad_maxima"];
		$info_rectificador["energizacion_controlador_rectif"] = $info["energizacion_controlador_rectif"];
		$info_rectificador["ubicacion_controlador_rectif"] = $info["ubicacion_controlador_rectif"];

        $info_rectificador["numero_serie"] = $info["numero_serie"];
     
		$controlador_rectif = $this->Rectificadores_model->edit($id);

		if($this->Rectificadores_model->update($id, $info_rectificador))
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			
			if(count($info["modulos_rectif"]) > 0)
			{
				$this->load->model('Modulos_rectif_model');
				foreach (($info["modulos_rectif"]) as $modulo_rectif)
				{
					$modulo_rectif->tipo_modulos_rectif = $modulo_rectif->tipo_modulos_rectif->id;
					if(isset($modulo_rectif->id))
					{
						if($this->Modulos_rectif_model->update($modulo_rectif->id,$modulo_rectif))
						{
							$evento = array(
								"accion"	=> "Actualización de Modulo Rectificador",
								"tabla"		=> "tb_modulos_rectif",
								"id"		=> $modulo_rectif->id
							);

							$this->load->model('Logs_modulos_rectif_model');
							$this->Logs_modulos_rectif_model->actualizacion_de_datos($session['id_usuario'],$modulo_rectif->id,$evento);
						}
					}
					else
					{
						if($this->Modulos_rectif_model->insert($modulo_rectif))
						{
							$evento = array(
								"accion"	=> "Creación de Modulo Rectificador",
								"tabla"		=> "tb_modulos_rectif"
							);

							$this->load->model('Logs_modulos_rectif_model');
							$this->Logs_modulos_rectif_model->ingreso_de_datos($session['id_usuario'],$evento);
							
						}
					}
				}
			}

			$evento = array(
					"accion"		=> "Actualización de Planta Rectificadora",
					"tabla"			=> "tb_controlador_rectif",
					"id"			=> $id,
					"datos viejos"	=> $controlador_rectif,
					"datos nuevos"	=> $this->Rectificadores_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_controlador_rectif_model');
				$this->Logs_controlador_rectif_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$evento["sitio"] = $this->Rectificadores_model->get_sitio_rectificador($id);
				$evento["link"] = "ver/sitios/energia/rectificadores/".$evento["sitio"]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('Rectificadores_model');
		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["sitio"] = $id;
		$info["modulos_rectif"] = json_decode($info["modulos_rectif"]);
		
		$info_rectificador["sitio"] = $id;
		$info_rectificador["tipo_controlador_rectif"] = $info["tipo_controlador_rectif"];
		$info_rectificador["anio_servicio"] = $info["anio_servicio"];
	   $info_rectificador["capacidad_maxima"] = $info["capacidad_maxima"];
		$info_rectificador["energizacion_controlador_rectif"] = $info["energizacion_controlador_rectif"];
		$info_rectificador["ubicacion_controlador_rectif"] = $info["ubicacion_controlador_rectif"];
		$info_rectificador["numero_serie"] = $info["partnumber"];
		

		if(isset($info["id_sala_tecnica"])){
         $info_rectificador["id_sala_tecnica"] = $info["id_sala_tecnica"];
        }
        


		$respuesta = $this->Rectificadores_model->insert($info_rectificador);

		if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			
			$info_rectificador["id"] = $respuesta["last_id"];

			//$this->Rectificadores_model->insert_ss_tecnologias($info_rectificador);
			
			if(count($info["modulos_rectif"]) > 0)
			{
				$this->load->model('Modulos_rectif_model');
				foreach (($info["modulos_rectif"]) as $modulo_rectif)
				{
					$modulo_rectif->tipo_modulos_rectif = $modulo_rectif->tipo_modulos_rectif->id;
					$modulo_rectif->controlador_rectif = $respuesta["last_id"];
					if($this->Modulos_rectif_model->insert($modulo_rectif))
					{
						$evento = array(
							"accion"	=> "Creación de Modulo Rectificador",
							"tabla"		=> "tb_modulos_rectif"
						);

						$this->load->model('Logs_modulos_rectif_model');
						$this->Logs_modulos_rectif_model->ingreso_de_datos($session['id_usuario'],$evento);
					}
				}
			}

			$evento = array(
					"accion"	=> "Creación de Planta Rectificadora",
					"tabla"		=> "tb_controlador_rectif",
					"id"		=> $respuesta["last_id"]
				);

			$this->load->model('Logs_controlador_rectif_model');
			$this->Logs_controlador_rectif_model->ingreso_de_datos($session['id_usuario'],$evento);

			$this->load->model('Status_site_model');
			$evento["link"] = "ver/sitios/energia/rectificadores/".$id;
			$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($id);
			$evento["elemento"] = $this->Rectificadores_model->edit($respuesta["last_id"]);
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $id);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);

			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function eliminar($id)
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if( isset($session["logged_in"]) && $session["logged_in"] && ($session["auth"] <= 2) )
		{
			if($this->input->is_ajax_request())
			{
				$this->load->model('Rectificadores_model');

				$info = $this->input->input_stream();

				$info_rectif['eliminado'] = 1;

				if($this->Rectificadores_model->update($id, $info_rectif))
				{
					$this->Rectificadores_model->update_ss_tecnologias($id,1);

					$this->load->model('Bancos_baterias_model');
					$where = array();
					$where["controlador_rectif"] = $id;
					$info_banco = array();
					$info_banco["eliminado"] = 1;
					$this->Bancos_baterias_model->update_where($where, $info_banco);

					$evento = array(
							"accion"		=> "Eliminación de Planta Rectificadora",
							"tabla"			=> "tb_controlador_rectif",
							"id"			=> $id
						);

					$this->load->model('Logs_controlador_rectif_model');
					$this->Logs_controlador_rectif_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

					$evento["sitio"] = $this->Rectificadores_model->get_sitio_rectificador($id);
					$evento["link"] = "ver/sitios/energia/rectificadores/".$evento["sitio"]["id"];
					$evento["elemento"] = $this->Rectificadores_model->edit($id);
					$evento["comentario"] = $info["comentario"];
					$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
					$cc = $this->custom->clean_cc($session["usuario"], $cc);
                  //echo json_encode($evento);
                   // var_dump($evento);
					$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
				}
			}
			else
			{
				echo json_encode(array("error" => "error"));
			}
		}
		else
		{
			return false;
		}
	}
	
	public function restaurar($id)
	{
		$this->load->model('Rectificadores_model');

		$info = $this->input->input_stream();

		$info_rectif['eliminado'] = 0;

		if($this->Rectificadores_model->update($id, $info_rectif))
		{
			$this->Rectificadores_model->update_ss_tecnologias($id,0);

			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Restauración de Rectificador eliminado",
					"tabla"			=> "tb_controlador_rectif",
					"id"			=> $id
				);

			$this->load->model('Logs_controlador_rectif_model');
			$this->Logs_controlador_rectif_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
		}
	}
	
	public function eliminar_definitivo($id)
	{
		$this->load->library('session');
		$session = $this->session->userdata();
		
		if( isset($session["logged_in"]) && $session["logged_in"] && ($session["auth"] <= 1) )
		{
			$this->load->model('Rectificadores_model');

			$info = $this->input->input_stream();

			$info['id'] = $id;

			if($this->Rectificadores_model->delete($id))
			{
				

				$evento = array(
						"accion"		=> "Eliminación definitiva de Rectificador",
						"tabla"			=> "tb_controlador_rectif",
						"id"			=> $id
					);

				$this->load->model('Logs_controlador_rectif_model');
				$this->Logs_controlador_rectif_model->eliminacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);
			}
		}
		else
		{
			echo "No tiene permiso para acceder esta página";
			return FALSE;
		}
	}
}