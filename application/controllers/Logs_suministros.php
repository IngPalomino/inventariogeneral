<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs_suministros extends CI_Controller {

	public function listar()
	{
		$this->load->model('Logs_suministros_model');

		$listado = $this->Logs_suministros_model->get_logs();

		echo json_encode($listado);
	}
}