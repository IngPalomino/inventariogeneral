<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status_site extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 2;
	}

	public function listar($limit = null,$offset = null)
	{
		$this->load->model('Status_site_model');

		$listado = $this->Status_site_model->get_sites($limit,$offset);

		echo json_encode($listado);
	}

	public function buscar()
	{
		$this->load->helper('form');
		$this->load->model('Status_site_model');

		$cadenas = $this->input->post('cadenas');
		$cadenas = explode(",", $cadenas);
		$listado = $this->Status_site_model->look_for_sites($cadenas);

		echo json_encode($listado);
	}
	
	public function buscar_por_suministro()
	{
		$this->load->helper('form');
		$this->load->model('Status_site_model');

		$cadenas = $this->input->post('cadenas');
		$cadenas = explode(",", $cadenas);
		$listado = $this->Status_site_model->look_for_sites_suministro($cadenas);

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Status_site_model');

		$sitio = $this->Status_site_model->listar($id);

		echo json_encode($sitio);
	}

	public function actualizar($id)
	{
		$this->load->model('Status_site_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$config = array(
				/*array(
						'field' => 'codigo',
						'label' => 'código',
						'rules' => 'required|callback_confirmar_codigo',
						'errors' => array(
								'required' => 'Debe ingresar un %s.'
							)
					),
				array(
						'field' => 'nombre',
						'label' => 'nombre',
						'rules' => 'required|callback_confirmar_nombre',
						'errors' => array(
								'required' => 'Debe ingresar el %s del sitio.'
							)
					),*/
				array(
						'field' => 'departamento',
						'label' => 'departamento',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir el %s.'
							)
					),
				array(
						'field' => 'provincia',
						'label' => 'provincia',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir la %s.'
							)
					),
				array(
						'field' => 'distrito',
						'label' => 'distrito',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir el %s.'
							)
					)
			);

		$error = array(
			'error' => FALSE);

		$data = $this->input->input_stream();

		$this->form_validation->set_data($data);
		$this->form_validation->set_rules($config);

		if($this->form_validation->run() === FALSE)
		{
			$error['error'] = validation_errors();
			echo json_encode($error);
		}
		else
		{
			$datos_zona['departamento'] = $this->input->input_stream('departamento');
			$datos_zona['provincia'] = $this->input->input_stream('provincia');
			$datos_zona['distrito'] = $this->input->input_stream('distrito');
			$this->load->model('Zona_peru_model');
			$zona = $this->Zona_peru_model->get_zona($datos_zona);

			$info = array();
			$info['zona_peru'] = $zona['0']['id'];
			$contratos = array();
			$input = $this->input->input_stream();
			foreach ($input as $key => $value)
			{
				if ($value=='true' || $value=='false')
				{
					$info[$key]=($value == 'true')? 1 : 0;
				}
				elseif ($key=='longitud' || $key=='latitud' || $key=='altura_edificacion' || $key=='altura_torre')
				{
					$info[$key]=floatval($value);
				}
				elseif ($key=='anio_construccion' || $key=='nivel_riesgo' || $key=='estado')
				{
					$info[$key]=intval($value);
				}
				elseif($key=='contratos')
				{
					$contratos=json_decode($value);
				}
				else
				{
					if($key != 'departamento' && $key != 'provincia' && $key != 'distrito')
					{
						$info[$key]=$value;
					}
				}
			}

			$datos_viejos = $this->Status_site_model->edit($id);

			if($this->Status_site_model->update($id, $info))
			{
				$this->load->library('session');
				$session = $this->session->userdata();

				if(sizeof($contratos) > 0)
				{
					$this->load->model('Contratos_model');
					foreach ($contratos as $contrato)
					{
						if($this->Contratos_model->update($contrato->id,$contrato))
						{
							$evento = array(
								"accion"	=> "Actualización de Contrato",
								"tabla"		=> "tb_contratos",
								"id"		=> $contrato->id
							);

							$this->load->model('Logs_contratos_model');
							$this->Logs_contratos_model->actualizacion_de_datos($session['id_usuario'],$contrato->id,$evento);

							if(sizeof($contrato->contactos_contrato) > 0)
							{
								$this->load->model('Contactos_contrato_model');
								foreach($contrato->contactos_contrato as $contacto)
								{
									$contacto->telefono = json_encode($contacto->telefono);
									$contacto->correo = json_encode($contacto->correo);
									if($this->Contactos_contrato_model->update($contacto->id,$contacto))
									{
										$evento = array(
											"accion"	=> "Actualización de Contacto",
											"tabla"		=> "tb_contactos_contrato",
											"id"		=> $contacto->id
										);

										$this->load->model('Logs_contactos_contrato_model');
										$this->Logs_contactos_contrato_model->actualizacion_de_datos($session['id_usuario'],$contacto->id,$evento);
									}
								}
							}
						}
					}
				}

				$evento = array(
						"accion"		=> "Actualización de POP",
						"tabla"			=> "tb_status_site",
						"id"			=> $id,
						"datos viejos"	=> $datos_viejos,
						"datos nuevos"	=> $this->Status_site_model->edit($id)
					);

				if($evento["datos viejos"] != $evento["datos nuevos"])
				{
					$this->load->model('Logs_status_site_model');
					$this->Logs_status_site_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

					$evento["sitio"] = $evento["datos nuevos"][0];
					$evento["link"] = "ver/sitios/torre/".$id;
					$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
					$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
					$cc = $this->custom->clean_cc($session["usuario"], $cc);
					$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
				}
			}
		}
	}

	public function crear()
	{
		$this->load->model('Status_site_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$config = array(
				array(
						'field' => 'codigo',
						'label' => 'código',
						'rules' => 'required|callback_confirmar_codigo',
						'errors' => array(
								'required' => 'Debe ingresar un %s.'
							)
					),
				array(
						'field' => 'nombre',
						'label' => 'nombre',
						'rules' => 'required|callback_confirmar_nombre',
						'errors' => array(
								'required' => 'Debe ingresar el %s del sitio.'
							)
					),
				array(
						'field' => 'departamento',
						'label' => 'departamento',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir el %s.'
							)
					),
				array(
						'field' => 'provincia',
						'label' => 'provincia',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir la %s.'
							)
					),
				array(
						'field' => 'distrito',
						'label' => 'distrito',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir el %s.'
							)
					)
			);

		$this->form_validation->set_rules($config);

		$error = array(
			'error' => FALSE);

		if($this->form_validation->run() === FALSE)
		{
			$error['error'] = validation_errors();
			echo json_encode($error);
		}
		else
		{
			$datos_zona['departamento'] = $this->input->post('departamento');
			$datos_zona['provincia'] = $this->input->post('provincia');
			$datos_zona['distrito'] = $this->input->post('distrito');
			$this->load->model('Zona_peru_model');
			$zona = $this->Zona_peru_model->get_zona($datos_zona);

			$info = array();
			$info['codigo'] = $this->input->post('codigo');
			$info['nombre'] = $this->input->post('nombre');
			$info['nombre_completo'] = $info['codigo'] . '_' . $info['nombre'];
			$info['zona_peru'] = $zona['0']['id'];
			$info['direccion'] = $this->input->post('direccion');
			$info['longitud'] = floatval($this->input->post('longitud'));
			$info['latitud'] = floatval($this->input->post('latitud'));
			$info['in_building'] = ($this->input->post('in_building') === "true" ? true : false);
			$info['coubicador'] = $this->input->post('coubicador');
			$info['nombre_sitio_coubicador'] = $this->input->post('nombre_sitio_coubicador');
			$info['codigo_torrera'] = $this->input->post('codigo_torrera');
			$info['factor_uso_torre'] = $this->input->post('factor_uso_torre');
			$info['operador_coubicante'] = $this->input->post('operador_coubicante');
			$info['nombre_sitio_coubicante'] = $this->input->post('nombre_sitio_coubicante');
			$info['proyecto'] = $this->input->post('proyecto');
			$info['tipo_solucion'] = $this->input->post('tipo_solucion');
			$info['proveedor_mantenimiento'] = $this->input->post('proveedor_mantenimiento');
			$info['agregador'] = ($this->input->post('agregador') === "true" ? true : false);
			$info['dorsal'] = ($this->input->post('dorsal') === "true" ? true : false);
			$info['tipo_torre'] = $this->input->post('tipo_torre');
			$info['ubicacion_equipos'] = $this->input->post('ubicacion_equipos');
			$info['anio_construccion'] = intval($this->input->post('anio_construccion'));
			$info['greenfield_rooftop'] = ($this->input->post('greenfield_rooftop') === "true" ? true : false);
			$info['altura_edificacion'] = floatval($this->input->post('altura_edificacion'));
			$info['altura_torre'] = floatval($this->input->post('altura_torre'));
			$info['camuflaje'] = $this->input->post('camuflaje');
			$info['acceso_libre_24h'] = ($this->input->post('acceso_libre_24h') === "true" ? true : false);
			$info['consideracion_acceso'] = $this->input->post('consideracion_acceso');
			$info['camuflaje'] = $this->input->post('camuflaje');
			$info['nivel_riesgo'] = intval($this->input->post('nivel_riesgo'));
			$info['estado'] = intval($this->input->post('estado'));
			$info['consideraciones'] = $this->input->post('consideraciones');
			$info['fecha_estado_on_air'] = $this->input->post('fecha_estado_on_air');
			$info['fecha_inicio_contrato'] = $this->input->post('fecha_inicio_contrato');
			$info['fecha_fin_contrato'] = $this->input->post('fecha_fin_contrato');
			$info['area_piso'] = floatval($this->input->post('area_piso'));
			$info['contrata_constructora'] = $this->input->post('contrata_constructora');

			if($this->Status_site_model->insert($info))
			{
				$this->load->library('session');
				$session = $this->session->userdata();

				$evento = array(
						"accion"	=> "Creación de POP",
						"tabla"		=> "tb_status_site"
					);

				$this->load->model('Logs_status_site_model');
				$this->Logs_status_site_model->ingreso_de_datos($session['id_usuario'],$evento);
			}
		}
	}

	public function crear_2()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$config = array(
				array(
						'field' => 'codigo',
						'label' => 'código',
						'rules' => 'required|callback_confirmar_codigo',
						'errors' => array(
								'required' => 'Debe ingresar un %s.'
							)
					),
				array(
						'field' => 'nombre',
						'label' => 'nombre',
						'rules' => 'required|callback_confirmar_nombre',
						'errors' => array(
								'required' => 'Debe ingresar el %s del sitio.'
							)
					),
				array(
						'field' => 'zona_peru',
						'label' => 'zona_peru',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir un departamento, una provina y un distrito.'
							)
					)
				// ,array(
				// 		'field' => 'supervisor',
				// 		'label' => 'supervisor',
				// 		'rules' => 'required',
				// 		'errors' => array(
				// 				'required' => 'Debe elegir un %s.'
				// 			)
				// 	)
			);

		$post = $this->input->post();
		$post = json_decode($post["data"],true);

		$this->form_validation->set_data($post);
		$this->form_validation->set_rules($config);

		$error = array(
			'resp' => false,
			'error' => FALSE);

		if($this->form_validation->run() === FALSE)
		{
			$error['error'] = validation_errors();
			echo json_encode($error);
		}
		else
		{
			//estado
			$info = array();
			$info["codigo"] = $post["codigo"];
			//nuevo agregado estado
			$info["estado"] = $post["estado"];
			$info["nombre"] = $post["nombre"];
			$info["nombre_completo"] = $post["codigo"]."_".$post["nombre"];
			$info['zona_peru'] = $post["zona_peru"];
			$info['direccion'] = $post["direccion"];
			$info['longitud'] = $post["longitud"];
			$info['latitud'] = $post["latitud"];
			$info['altitud'] = $post["altitud"];
			$info['prioridad'] = $post["prioridad"];
			$info['tipo_solucion'] = ( ($post["tipo_solucion"] != "")? $post["tipo_solucion"] : null);
			$info['proveedor_mantenimiento'] = ( ($post["proveedor_mantenimiento"] != "")? $post["proveedor_mantenimiento"] : null);
			$info['tipo_torre'] = ( ($post["tipo_torre"] != "")? $post["tipo_torre"] : null);
			$info['altura_torre'] = $post["altura_torre"];
			$info['greenfield_rooftop'] = (($post["greenfield_rooftop"])? 1 : 0 );
			$info['altura_edificacion'] = $post["altura_edificacion"];
			$info['camuflaje'] = ( ($post["camuflaje"] != "")? $post["camuflaje"] : null);
			$info['contrata_constructora'] = ( ($post["contrata_constructora"] != "")? $post["contrata_constructora"] : null);
			$info['anio_construccion'] = $post["anio_construccion"];
			$info['coubicado'] = (($post["coubicador"] == 1)? 0 : 1);
			$info['coubicador'] = $post["coubicador"];
			$info['nombre_sitio_coubicador'] = $post["nombre_sitio_coubicador"];
			$info['codigo_torrera'] = $post["codigo_torrera"];
			$info['operador_coubicante'] = $post["operador_coubicante"];
			$info['nombre_sitio_coubicante'] = $post["nombre_sitio_coubicante"];
			$info['acceso_libre_24h'] = (($post["acceso_libre_24h"])? 1 : 0 );
			$info['nivel_riesgo'] = $post["nivel_riesgo"];
			$info['consideracion_acceso'] = $post["consideracion_acceso"];
			$info['consideraciones'] = $post["consideraciones"];
			$info['proyecto'] = $post["proyecto"];
			$info['fecha_estado_on_air'] = $post["fecha_estado_on_air"];
			$info['in_building'] = (($post["in_building"])? 1 : 0 );
			$info['agregador'] = (($post["agregador"])? 1 : 0 );
			$info['dorsal'] = (($post["dorsal"])? 1 : 0 );

			$this->load->model('Status_site_model');

			$sitioNuevo = $this->Status_site_model->insert($info);

			if( $sitioNuevo["resp"] )
			{
				$this->load->library('session');
				$session = $this->session->userdata();

				$evento = array(
						"accion"	=> "Creación de POP",
						"tabla"		=> "tb_status_site"
					);

				$this->load->model('Logs_status_site_model');
				$this->Logs_status_site_model->ingreso_de_datos($session['id_usuario'],$evento);
				
				$infoCRM = array();
				$infoCRM["sitio"] = $sitioNuevo["id"];
				$infoCRM["crm"] = $post["crm"]; //( ( ($post["crm"] != null) && ($post["crm"] != "") )? $post["crm"] : null );
				$infoCRM["supervisor"] = $post["supervisor"];

				$this->load->model('Sitio_crm_model');
				$this->Sitio_crm_model->insert($infoCRM);

				foreach ($post["oym_zonal"] as $zonal)
				{
					if( ($zonal["oym_zonal"] != null) && ($zonal["oym_zonal"] != "") )
					{
						$infoZonal = array();
						$infoZonal["sitio"] = $sitioNuevo["id"];
						$infoZonal["oym_zonal"] = $zonal["oym_zonal"];

						$this->load->model('Sitio_oym_zonal_model');
						$this->Sitio_oym_zonal_model->insert($infoZonal);
					}
				}

				foreach ($post["tecnologias"] as $tecno)
				{
					if( $tecno["tecnologia"] != null )
					{
						$infoTecno = array();
						$infoTecno["sitio"] = $sitioNuevo["id"];
						$infoTecno["tecnologia"] = $tecno["tecnologia"];
						$infoTecno["codigo"] = $tecno["codigo"];
						$infoTecno["fecha_servicio"] = $tecno["fecha_servicio"];
						$infoTecno["servicios"] = $tecno["servicios"];

						$this->load->model('Sitio_tecnologia_model');
						$this->Sitio_tecnologia_model->insert($infoTecno);
					}
				}

				$evento["elemento"] = $this->Status_site_model->edit($sitioNuevo["id"]);
				$evento["link"] = "ver/sitios/torre/".$evento["elemento"][0]["id"];
				$evento["sitio"] = $evento["elemento"][0];
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["elemento"][0]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}

			echo json_encode($sitioNuevo);
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Status_site_model');

		$this->Status_site_model->delete($id);
	}

	public function resumen_sitios()
	{
		$this->load->model('Status_site_model');

		$listado = $this->Status_site_model->resumen_sitios();

		echo json_encode($listado);
	}

	public function tecnologias()
	{
		$this->load->model('Status_site_model');

		$listado = $this->Status_site_model->tecnologias();

		echo json_encode($listado);
	}

	public function listar_otros_sitios($id)
	{
		$this->load->model('Status_site_model');
		$listado = $this->Status_site_model->get_sitios_dif($id);
		echo json_encode($listado);
	}

	public function listar_sitios_total()
	{
		$this->load->model('Status_site_model');
		$listado = $this->Status_site_model->get_sitios_total();
		echo json_encode($listado);
	}

	/************************************/
	/* FUNCIONES DE VALIDACIÓN DE DATOS */
	/************************************/

	function confirmar_codigo($codigo)
	{
		if(strlen($codigo) === 7 ||strlen($codigo) === 8)
		{			

			if(strlen($codigo) === 7){
                 if (!(preg_match("/(?>^\d{7}+$)/",$codigo)))
			    {
				$this->form_validation->set_message('confirmar_codigo', 'Los 7 dígitos del código deben ser numéricos.');
				 return FALSE;
			    }
			   else
				{
					return TRUE;
				}

			}elseif (strlen($codigo) === 8) {			
                if (!(preg_match("/(?>^\d{8}+$)/",$codigo)))
			    {
				$this->form_validation->set_message('confirmar_codigo', 'Los 8 dígitos del código deben ser numéricos.');
				 return FALSE;
			    }
			   else
				{
					return TRUE;
				}
			}
		}
		else
		{
			$this->form_validation->set_message('confirmar_codigo', 'El código debe ser de 7 u 8 dígitos.');
			return FALSE;
		}
	}

	function confirmar_nombre($nombre)
	{
		if(!(preg_match("/(?>^\S+$)/",$nombre)))
		{
			$this->form_validation->set_message('confirmar_nombre','El nombre no debe contener espacios en blanco.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

   function get_factor_uso_sitio($idsitio)
   {
   		$this->load->model('Status_site_model');
		$listado = $this->Status_site_model->get_factor_uso_sitio($idsitio);
		echo json_encode($listado);
   }


   function busca_codigo_on_air(){     
	 $this->load->model('Status_site_model');
	 $codigo = $this->input->post('codigo');	 
	 $listado = $this->Status_site_model->get_codigo_on_air($codigo);
	 echo json_encode($listado);
   } 


}