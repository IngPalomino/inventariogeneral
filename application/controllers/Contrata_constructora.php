<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contrata_constructora extends CI_Controller {

	public function listar()
	{
		$this->load->model('Contrata_constructora_model');

		$listado = $this->Contrata_constructora_model->get_Proyecto();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Contrata_constructora_model');

		$Contrata_constructora = $this->Contrata_constructora_model->edit($id);

		echo json_encode($Contrata_constructora);
	}

	public function actualizar($id)
	{
		$this->load->model('Contrata_constructora_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['contrata_constructora'] = $this->input->input_stream('Contrata_constructora');

		$this->Contrata_constructora_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Contrata_constructora_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['contrata_constructora'] = $this->input->post('Contrata_constructora');

		$this->Contrata_constructora_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Contrata_constructora_model');

		$this->Contrata_constructora_model->delete($id);
	}
}