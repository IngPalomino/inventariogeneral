<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_solucion extends CI_Controller {

	public function listar()
	{
		$this->load->model('Tipo_solucion_model');

		$listado = $this->Tipo_solucion_model->get_Proyecto();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Tipo_solucion_model');

		$Tipo_solucion = $this->Tipo_solucion_model->edit($id);

		echo json_encode($Tipo_solucion);
	}

	public function actualizar($id)
	{
		$this->load->model('Tipo_solucion_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['tipo_solucion'] = $this->input->input_stream('Tipo_solucion');

		$this->Tipo_solucion_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Tipo_solucion_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['tipo_solucion'] = $this->input->post('Tipo_solucion');

		$this->Tipo_solucion_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Tipo_solucion_model');

		$this->Tipo_solucion_model->delete($id);
	}
}