<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fases extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('Fases_model');
	}

	public function listar()
	{		
		$listado = $this->Fases_model->get_fases();
		echo json_encode($listado);
	}

	
}