<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_modulos_rectif extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('auth');
		$this->load->model('Bodega_modulos_rectif_model');
		$this->load->model('Logs_bodega_modulos_rectif_model', 'logs');
	}

	public function almacenar()
	{
		$post = $this->input->post(array('tipo_almacen', 'almacen', 'estado', 'tipo_modulos_rectif'));
		$modulos_rectif = json_decode($this->input->post('numeros_serie'), TRUE);

		if ($this->auth->logged_in())
		{
			if (count($modulos_rectif) > 0)
			{
				foreach ($modulos_rectif as $modulo)
				{
					$modulo["tipo_almacen"] = $post["tipo_almacen"];
					$modulo["almacen"] = $post["almacen"];
					$modulo["estado"] = $post["estado"];
					$modulo["tipo_modulos_rectif"] = $post["tipo_modulos_rectif"];
					if ($this->Bodega_modulos_rectif_model->insert($modulo))
					{
						$evento["accion"] = "Almacenamiento de Módulo Rectificador";
						$evento["tabla"] = "tb_bodega_modulos_rectif";
						$this->logs->ingreso_de_datos($this->session->userdata('id_usuario'), $evento);
					}
				}
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}

     public function mover()
	{
		$this->load->model('Bodega_modulos_rectif_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = $this->input->input_stream();
		$antena = $this->Bodega_modulos_rectif_model->edit($id);

		$almacen_antiguo = array(
					"tipo_almacen" 	=> $antena[0]["tipo_almacen"],
					"almacen"		=> $antena[0]["almacen"]
				);

		$info["almacen_previo"] = json_encode($almacen_antiguo);
		$comentario = $info["comentario"];
		unset($info["comentario"]);

		if($this->Bodega_modulos_rectif_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			$modulos_rectif_nueva = $this->Bodega_modulos_rectif_model->edit($id);

			$almacen_nuevo = array(
					"tipo_almacen" 	=> $modulos_rectif[0]["tipo_almacen"],
					"almacen"		=> $modulos_rectif[0]["almacen"]
				);

			$evento = array(
					"accion"		=> "Cambio de almacén de Módulos Rectificadores",
					"tabla"			=> "tb_bodega_modulos_rectif",
					"id"			=> $id,
					"almacen_antiguo"	=> $info["almacen_previo"],
					"almacen_nuevo"	=> json_encode($almacen_nuevo)
				);

			if($evento["almacen_antiguo"] != $evento["almacen_nuevo"])
			{
				$this->load->model('Logs_bodega_modulos_rectif_model');
				$this->Logs_bodega_modulos_rectif_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$comentario);
			}
		}
	}



	public function eliminar($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->Bodega_modulos_rectif_model->update($id, array("eliminado" => 1)))
			{
				$evento["accion"] = "Eliminación de Módulo Rectificador";
				$evento["tabla"] = "tb_bodega_modulos_rectif";
				$evento["id"] = $id;

				$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento, $this->input->input_stream('comentario'));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}

    public function actualizar($id)
	{
		$post = $this->input->input_stream();

		if ($this->auth->logged_in())
		{
			$old = $this->Bodega_modulos_rectif_model->edit($id);
         //elimino valores 
         unset($post['logs_bodega_modulos_rectif']);
		 unset($post['$$hashKey']);
		 unset($post['link']);
		 unset($post['comentario']);
        //elimino valores 

			if ($this->Bodega_modulos_rectif_model->update($id, $post))
			{
				$evento['accion'] = 'Actualización de Módulo Rectificador.';
				$evento['tabla'] = 'tb_bodega_modulos_rectif';
				$evento['id'] = $id;
				$evento['datos_antiguos'] = $old;
				$evento['datos_nuevos'] = $this->Bodega_modulos_rectif_model->edit($id);

				if ($evento['datos_antiguos'] != $evento['datos_nuevos'])
				{
					$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento);
				}
			}
		}
		else
		{
			echo json_encode(array('error' => TRUE, 'message' => 'Su sesión ha expirado.'));
		}
	}





}