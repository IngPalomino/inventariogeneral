<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_masivo_rectificadores extends CI_Controller
{
	protected $modulo;

	public function __construct()
	{
		parent::__construct();
		$this->modulo = 7;
		$this->load->model('Ingreso_masivo_rectificadores_model');
		$this->load->library('auth');
	}
		
	public function plantilla_rectificadores()
	{
		$this->load->library("excel");
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla Rectificadores")
									->setSubject("Plantilla")
									->setDescription("Plantilla para ingresar información de Rectificadores al Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle("Rectificadores");

		$activeSheet->setCellValue("A1", "Código")
					->setCellValue("B1", "Nombre")
					->setCellValue("C1", "Marca")
					->setCellValue("D1", "Controlador")					
					->setCellValue("E1", "Par Number")					
					->setCellValue("F1", "Modelo")
					->setCellValue("G1", "Capacidad Máxima (kW)")
					->setCellValue("H1", "Módulos Instalados")
					->setCellValue("I1", "Fecha de puesta en servicio")
					->setCellValue("J1", "Números de Serie")
					->setCellValue("K1", "Energización")
					->setCellValue("L1", "Ubicación");

		$styleArray = array(
				"font" => array(
						"bold"	=> true,
						"color"	=> array("rgb" => "FFFFFF"),
						"size"	=> 11,
						"name"	=> "Verdana"
					),
				"fill" => array(
						"type"	=> PHPExcel_Style_Fill::FILL_SOLID,
						"color"	=> array("rgb" => "6788D3")
					),
				"alignment" => array(
						"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle("A1:L1")->applyFromArray($styleArray);

		$activeSheet->getStyle('A:A')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$activeSheet->getColumnDimension("A")->setAutoSize(true);
		$activeSheet->getColumnDimension("B")->setAutoSize(true);
		$activeSheet->getColumnDimension("C")->setAutoSize(true);
		$activeSheet->getColumnDimension("D")->setAutoSize(true);
		$activeSheet->getColumnDimension("E")->setAutoSize(true);
		$activeSheet->getColumnDimension("F")->setAutoSize(true);
		$activeSheet->getColumnDimension("G")->setAutoSize(true);
		$activeSheet->getColumnDimension("H")->setAutoSize(true);
		$activeSheet->getColumnDimension("I")->setAutoSize(true);
		$activeSheet->getColumnDimension("J")->setAutoSize(true);
		$activeSheet->getColumnDimension("K")->setAutoSize(true);
		$activeSheet->getColumnDimension("L")->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Rectificadores.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function plantilla_desplegables_rectificadores()
	{
		$this->load->model('Tipo_controlador_rectif_model');
		$this->load->model('Tipo_modulos_rectif_model');
		$this->load->model('Energizacion_controlador_rectif_model');
		$this->load->model('Ubicacion_controlador_rectif_model');
		$tipo_rectificador = $this->Ingreso_masivo_rectificadores_model->get_tipo_rectificador();
		$energizacion = $this->Energizacion_controlador_rectif_model->get_types();
		$ubicacion = $this->Ubicacion_controlador_rectif_model->get_types();

		$this->load->library("excel");
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla Rectificadores")
									->setSubject("Plantilla")
									->setDescription("Plantilla para ingresar información de Rectificadores al Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle("Desplegables");

		$activeSheet->setCellValue("A1", "Marca")
					->setCellValue("B1", "Modelo")
					->setCellValue("C1", "Controlador")
					->setCellValue("E1", "Energización")
					->setCellValue("G1", "Ubicación");

		$styleArray = array(
				"font" => array(
						"bold"	=> true,
						"color"	=> array("rgb" => "FFFFFF"),
						"size"	=> 11,
						"name"	=> "Verdana"
					),
				"fill" => array(
						"type"	=> PHPExcel_Style_Fill::FILL_SOLID,
						"color"	=> array("rgb" => "6788D3")
					),
				"alignment" => array(
						"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle("A1:C1")->applyFromArray($styleArray);
		$activeSheet->getStyle("E1:E1")->applyFromArray($styleArray);
		$activeSheet->getStyle("G1:G1")->applyFromArray($styleArray);

		$i = 2;

		foreach ($tipo_rectificador as $fila)
		{
			$activeSheet->setCellValue("A".$i, $fila["marca"])
						->setCellValue("B".$i, $fila["modelo"])
						->setCellValue("C".$i, $fila["controlador"]);

			$i++;
		}

		$i = 2;

		foreach ($energizacion as $fila)
		{
			$activeSheet->setCellValue("E".$i, $fila["energizacion_controlador_rectif"]);
			$i++;
		}

		$i = 2;

		foreach ($ubicacion as $fila)
		{
			$activeSheet->setCellValue("G".$i, $fila["ubicacion_controlador_rectif"]);
			$i++;
		}

		$activeSheet->getColumnDimension("A")->setAutoSize(true);
		$activeSheet->getColumnDimension("B")->setAutoSize(true);
		$activeSheet->getColumnDimension("C")->setAutoSize(true);
		$activeSheet->getColumnDimension("E")->setAutoSize(true);
		$activeSheet->getColumnDimension("G")->setAutoSize(true);
		
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Desplegables_Rectificadores.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function upload_file()
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->allowed_module($this->modulo))
			{
				$post = $this->input->post();
				 $config["upload_path"] = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo";
				

				$config["allowed_types"] = "xlsx";
				$config["max_size"] = "1024";
				$config["overwrite"] = FALSE;
				$config["file_name"] = "plantilla_".$post["tipo"]."_".date("d-m-Y-H-i-s");

				$this->load->library("upload", $config);

				if ($this->upload->do_upload("archivo"))
				{
					echo json_encode(array(
							"error"		=> FALSE,
							"mensaje"	=> "Archivo subido con éxito.",
							"resultado"	=> $this->upload->data("file_name")
						));
				}
				else
				{
					echo json_encode(array(
							"error"		=> TRUE,
							"mensaje"	=> "No se pudo subir el archivo.",
							"resultado"	=> $this->upload->display_errors()
						));
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción.",
						"resultado"	=> "forbidden"
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró",
					"resultado"	=> "logged_off"
				));
		}
	}

	public function check_file()
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->allowed_module($this->modulo))
			{
				$post = $this->input->post();
				 $file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"];

			
				/*$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/plantilla_antenas_gul_19-06-2017-16-09-50.xlsx";*/
				
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						"error"			=> FALSE,
						"advertencia"	=> FALSE,
						"mensajes"		=> array(),
						"advertencias"	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				if(in_array("Rectificadores", $sheetNames))
				{
					try{ $objPHPExcel->setActiveSheetIndexByName("Rectificadores"); } catch(Exception $e){ continue; }

					$activeSheet = $objPHPExcel->getActiveSheet();
					$cell_collection = $activeSheet->getCellCollection();
					$cabeceras = array("Código", "Nombre", "Marca", "Modelo", "Controlador","Par Number", "Capacidad Máxima (kW)", "Módulos Instalados", "Fecha de puesta en servicio", "Números de Serie", "Energización", "Ubicación");
					$cabeceras_no_validas = array();
					$cabeceras_validas = array();
					$arr_data = array();

					foreach($cell_collection as $cell)
					{
						$column = $activeSheet->getCell($cell)->getColumn();
						$row = $activeSheet->getCell($cell)->getRow();
						$data_value = $activeSheet->getCell($cell)->getValue();

						if($row == 1)
						{
							$header[$row][$column] = $data_value;

							foreach($header as $filaHeader => $infoHeader)
							{
								if(!in_array($infoHeader[$column], $cabeceras))
								{
									array_push($cabeceras_no_validas, $infoHeader[$column]);
								}
								else
								{
									array_push($cabeceras_validas, $infoHeader[$column]);
								}
							}
						}
						else
						{
							if(!isset($data_value) && $column == "A")
							{
								/*break;*/
								$resultados["advertencia"] = TRUE;
								array_push($resultados["advertencias"], "La columna $column en la fila $row no tiene información, se obviará esta fila.");
							}
							elseif(in_array($header[1][$column], $cabeceras_no_validas))
							{
								
							}
							else
							{
								$arr_data[$row][$column] = $data_value;
							}
						}
					}

					foreach($cabeceras as &$value)
					{
						if(!in_array($value, $cabeceras_validas))
						{
							$resultados["error"] = TRUE;
							array_push($resultados["mensajes"], "La columna $value es obligatoria.");
						}
					}

					if(count($cabeceras_no_validas) > 0)
					{
						$cabeceras_no_validas = array_values(array_unique($cabeceras_no_validas));

						foreach($cabeceras_no_validas as $cabecera)
						{
							$resultados["advertencia"] = TRUE;
							array_push($resultados["advertencias"], "La cabecera $cabecera no corresponde a la plantilla original, se obviará esta columna.");
						}
					}

					/*echo json_encode($cabeceras_no_validas);*/
					$filas = count($arr_data);

					if(count($arr_data) > 0)
					{
						foreach($arr_data as $fila => $infoFila)
						{
							$nombreSeparado = explode("_", $infoFila["B"]);
							$codigoNombre = $nombreSeparado[0];

							foreach($infoFila as $columna => $campo)
							{
								if($header[1][$columna] == "Código")
								{
									$codigo = $campo;
									if(strlen($campo) != 7)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código de sitio en la fila $fila no es válido, debe tener 7 dígitos.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código de sitio en la fila $fila debe contener únicamente números.");
									}
									else
									{
										if($codigoNombre != $campo)
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "El Código de sitio en la fila $fila no coincide con el código del Nombre.");
										}
									}
								}
								elseif($header[1][$columna] == "Nombre")
								{
									$nombre = $this->Ingreso_masivo_rectificadores_model->esNombreCodigoValidos($codigo, $campo);
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Nombre en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_rectificadores_model->esNombreCompletoValido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre de sitio en la fila $fila no existe en la base de datos.");
									}
									elseif(!$nombre)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre y Código de sitio en la fila $fila no existe en la base de datos.");
									}
									elseif(!$this->auth->allowed_module($this->modulo, $nombre["id"]))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Usted no tiene permiso para ingresar/actualizar información en el sitio $campo de la fila $fila.");
									}
								}
								elseif($header[1][$columna] == "Marca")
								{
									$marca = $campo;
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Marca en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_rectificadores_model->marca_valida($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Marca en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Controlador")
								{
									$controlador = $this->Ingreso_masivo_rectificadores_model->tipo_controlador_rectif_valido($marca, $campo);
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Controlador en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_rectificadores_model->controlador_valido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Controlador en la fila $fila no existe en la base de datos.");
									}
									elseif ($controlador === FALSE)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Tipo de Controlador (Marca y Controlador) en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Modelo")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Modelo en la fila $fila.");
									}
									elseif( ! $this->Ingreso_masivo_rectificadores_model->modelo_valido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Modelo en la fila $fila no existe en la base de datos.");
									}
									elseif ($this->Ingreso_masivo_rectificadores_model->tipo_modulo_rectif_valido($marca, $campo, $controlador["id"]) === FALSE)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Tipo de Módulo (Marca y Modelo) en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Capacidad Máxima (kW)")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Capacidad Máxima (kW) en la fila $fila.");
									}
									elseif (!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Capacidad Máxima (kW) en la fila $fila debe contener únicamente números.");
									}
								}
								elseif($header[1][$columna] == "Módulos Instalados")
								{
									$modulos_instalados = $campo;
									if(!isset($columna))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el número de Módulos Instalados en la fila $fila.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Módulos Instalados en la fila $fila debe contener únicamente números");
									}
								}
								elseif($header[1][$columna] == "Fecha de puesta en servicio")
								{
									if(!isset($columna) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Fecha de puesta en servicio en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_rectificadores_model->validate_date($campo, "d/m/Y"))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Fecha de puesta en servicio en la fila $fila no es una fecha válida.");
									}
								}
								elseif($header[1][$columna] == "Números de Serie")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar los Números de Serie en la fila $fila.");
									}
									elseif ( ! preg_match("/[,\/\\\]+/", $campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Los Números de Serie en la fila $fila deben estar separados por comas.");
									}
									else
									{
										$numeros_serie = preg_split("/[\s,\/\\\]+/", $campo);

										if (count($numeros_serie) != $modulos_instalados)
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "El número de Módulos Instalados no coincide con la cantidad de Números de Serie ingresados en la fila $fila.");
										}
									}
								}
								elseif($header[1][$columna] == "Energización")
								{
									$energizacion = $this->Ingreso_masivo_rectificadores_model->energizacion_valida($campo);
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Energización en la fila $fila.");
									}
									elseif( ! $energizacion)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Energización en la fila $fila no existe en la base de datos.");
									}
									elseif ($this->Ingreso_masivo_rectificadores_model->existe_rectificador($energizacion["id"], $nombre["id"]) !== FALSE)
									{
										$resultados["advertencia"] = TRUE;
										array_push($resultados["advertencias"], "El sitio en la fila $fila ya tiene una planta rectificadora en ".$energizacion["energizacion_controlador_rectif"].", se actualizará este elemento.");
									}
								}
								elseif ($header[1][$columna] == "Ubicación")
								{
									if ( ! isset($campo) || ! isset($columna))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Ubicación en la fila $fila.");
									}
									elseif ( ! $this->Ingreso_masivo_rectificadores_model->ubicacion_valida($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Ubicación en la fila $fila no existe en la base de datos.");
									}
								}
                                elseif ($header[1][$columna] == "Par Number")
								{
									if ( ! isset($campo) || ! isset($columna))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar Par Number del controlador en la fila $fila.");
									}
									$numero_serie = $campo;
									
								}
							}
						}

						/*echo json_encode($arr_data);*/
					}
					else
					{
						$resultados["error"] = TRUE;
						array_push($resultados["mensajes"], "La hoja no contiene información");
					}
				}
				else
				{
					$resultados["error"] = TRUE;
					array_push($resultados["mensajes"], "No se encontró la hoja llamada 'Rectificadores'");
				}

				if(!$resultados["error"])
				{
					array_push($resultados["mensajes"], "Filas válidas: ".count($arr_data));
				}

				echo json_encode($resultados);

				if($resultados["error"])
				{
					 unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
                   

	


				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción."
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró."
				));
		}
	}

	public function insert_temp()
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->allowed_module($this->modulo))
			{
				$idIngresoMasivo = $this->Ingreso_masivo_rectificadores_model->registrar_ingreso_masivo($this->session->userdata("id_usuario"));
				$post = $this->input->post();
				 $file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"];

				
				
               


				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						"error"			=> FALSE,
						"advertencia"	=> FALSE,
						"mensajes"		=> array(),
						"advertencias"	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				$resultado = 0;

				if(in_array("Rectificadores", $sheetNames))
				{
					try{ $objPHPExcel->setActiveSheetIndexByName("Rectificadores"); } catch(Exception $e){ continue; }

					$activeSheet = $objPHPExcel->getActiveSheet();
					$cell_collection = $activeSheet->getCellCollection();
					$cabeceras = array("Código", "Nombre", "Marca", "Modelo", "Controlador","Par Number", "Capacidad Máxima (kW)", "Módulos Instalados", "Fecha de puesta en servicio", "Números de Serie", "Energización", "Ubicación");
					$cabeceras_no_validas = array();
					$cabeceras_validas = array();
					$arr_data = array();

					foreach($cell_collection as $cell)
					{
						$column = $activeSheet->getCell($cell)->getColumn();
						$row = $activeSheet->getCell($cell)->getRow();
						$data_value = $activeSheet->getCell($cell)->getValue();

						if($row == 1)
						{
							$header[$row][$column] = $data_value;

							foreach($header as $filaHeader => $infoHeader)
							{
								if(!in_array($infoHeader[$column], $cabeceras))
								{
									array_push($cabeceras_no_validas, $infoHeader[$column]);
								}
								else
								{
									array_push($cabeceras_validas, $infoHeader[$column]);
								}
							}
						}
						else
						{
							if(!isset($data_value) && $column == "A")
							{
								/*break;*/
							}
							elseif(in_array($header[1][$column], $cabeceras_no_validas))
							{
								
							}
							else
							{
								$arr_data[$row][$column] = $data_value;
							}
						}
					}

					/*echo json_encode($cabeceras_no_validas);*/
					$filas = count($arr_data);

					if($idIngresoMasivo !== FALSE)
					{
						foreach($arr_data as $fila => $infoFila)
						{
							$nombreSeparado = explode("_", $infoFila["B"]);
							$codigoNombre = $nombreSeparado[0];

							foreach($infoFila as $columna => $campo)
							{
								$ingresoMasivo["ingreso_masivo"] = $idIngresoMasivo;

								if($header[1][$columna] == "Código")
								{
									$codigo = $campo;
								}
								elseif($header[1][$columna] == "Nombre")
								{
									$nombre = $this->Ingreso_masivo_rectificadores_model->esNombreCodigoValidos($codigo, $campo);
									$ingresoMasivo["sitio"] = $nombre["id"];
								}
								elseif($header[1][$columna] == "Marca")
								{
									$marca = $campo;
								}
								elseif($header[1][$columna] == "Controlador")
								{
									$controlador = $this->Ingreso_masivo_rectificadores_model->tipo_controlador_rectif_valido($marca, $campo);
									$ingresoMasivo["tipo_controlador_rectif"] = $controlador["id"];
								}
								elseif($header[1][$columna] == "Par Number")
								{
									$ingresoMasivo["numero_serie"] = $campo;
								}
								elseif($header[1][$columna] == "Modelo")
								{
									$tipo_modulo = $this->Ingreso_masivo_rectificadores_model->tipo_modulo_rectif_valido($marca, $campo, $controlador["id"]);
								}
								elseif($header[1][$columna] == "Capacidad Máxima (kW)")
								{
									$ingresoMasivo["capacidad_maxima"] = $campo;
								}
								elseif($header[1][$columna] == "Módulos Instalados")
								{
									//$modulos_instalados = $campo;
								}
								elseif($header[1][$columna] == "Fecha de puesta en servicio")
								{
									$date = DateTime::createFromFormat("d/m/Y", $campo);
									$ingresoMasivo["anio_servicio"] = $date->format("Y-m-d");
								}
								elseif($header[1][$columna] == "Números de Serie")
								{
									$numeros_serie = preg_split("/[\s,\/\\\]+/", $campo);
								}
								elseif($header[1][$columna] == "Energización")
								{
									$energizacion = $this->Ingreso_masivo_rectificadores_model->energizacion_valida($campo);
									$ingresoMasivo["energizacion_controlador_rectif"] = $energizacion["id"];
									$actualizar = $this->Ingreso_masivo_rectificadores_model->existe_rectificador($energizacion["id"], $nombre["id"]);

									if ($actualizar !== FALSE)
									{
										$ingresoMasivo["id_actualizar"] = $actualizar["id"];
									}
									else
									{
										$ingresoMasivo["id_actualizar"] = NULL;
									}
								}
								elseif ($header[1][$columna] == "Ubicación")
								{
									$ubicacion = $this->Ingreso_masivo_rectificadores_model->ubicacion_valida($campo);
									$ingresoMasivo["ubicacion_controlador_rectif"] = $ubicacion["id"];
								}
							}

							$respuesta = $this->Ingreso_masivo_rectificadores_model->ingresar_temporal($ingresoMasivo);

							if ($respuesta["resp"])
							{
								if (count($numeros_serie) > 0)
								{
									foreach ($numeros_serie as $numero)
									{
										$modulo["controlador_rectif"] = $respuesta["last_id"];
										$modulo["tipo_modulos_rectif"] = $tipo_modulo["id"];
										$modulo["numero_serie"] = $numero;

										$this->Ingreso_masivo_rectificadores_model->ingresar_temporal_modulos($modulo);
									}
								}

								$resultado++;
							}
						}

						$resultados["mensajes"] = "Se ha enviado $resultado fila(s) para ser aprobada(s).";
						
					 unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
   
						/*echo json_encode($arr_data);*/
					}
					else
					{
						$resultados["error"] = TRUE;
						array_push($resultados["mensajes"], "La hoja no contiene información");
					}
				}
				else
				{
					$resultados["error"] = TRUE;
					array_push($resultados["mensajes"], "No se encontró la hoja llamada 'Rectificadores'");
				}

				if(!$resultados["error"])
				{
					array_push($resultados["mensajes"], "Filas válidas: ".count($arr_data));
				}

				echo json_encode($resultados);

				if($resultados["error"])
				{
					 unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);

				

				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción."
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró."
				));
		}
	}
	

	public function listar_solicitudes($usuario)
	{
		if($this->auth->logged_in() && ($this->session->userdata("id_usuario") == $usuario))
		{
			echo json_encode($this->Ingreso_masivo_rectificadores_model->get_solicitudes($usuario));
		}
		else
		{
			show_404();
		}
	}
}