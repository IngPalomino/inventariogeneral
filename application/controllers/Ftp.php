<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ftp extends CI_Controller {
	
	public function listar()
	{
		$dir = $this->input->post("dir");
		$dir = "/INFRA/01.Inventario_General".$dir;
		/*$dir = ( isset($dir) && ($dir != "") && ($dir != null) )? $this->input->post("dir") : "/" ;*/

		$this->load->library('ftp');

		/*$config['hostname'] = '172.21.54.201';
		$config['username'] = 'mamaro';
		$config['password'] = 'YWaPSFHo';
		$config['debug']        = TRUE;*/
		
		$config['hostname'] = '10.30.25.45';
		$config['username'] = 'cistey';
		$config['password'] = 'c1st3y06120201';
		$config['debug']        = TRUE;

		$this->ftp->connect($config);
		
		$listado = $this->ftp_listar($this->ftp,$dir);
		
		foreach($listado as &$value)
		{
			$value = str_replace("/INFRA/01.Inventario_General", "", $value);
		}
		
		$this->ftp->close();
		echo json_encode($listado);
	}
	
	Private static function cmpDirArchTexto($a,$b)
	{
		if( $a["dir"] == $b["dir"] )
		{
			return strcmp($a["text"], $b["text"]);
		}
		else
		{
			return ( $a["dir"] > $b["dir"] )? -1 : 1 ;
		}
	}

	function ftp_listar($conn,$dir)
	{
		if(!isset($dir))
		{
			return false;
		}
		else
		{
			
			$list = $conn->list_files($dir);
			
			$listadoFinal = array();
			
			foreach($list as $dir_file)
			{
				$exts = explode(".",$dir_file);
				$patron = "/^[a-zA-Z0-9]+$/";
				
				$dir_file = array(
							"dir" => ( (count($exts) > 1) ? !preg_match($patron,end($exts) ) : true ),
							"nombre" => $dir_file
					);
					
				$ext2fa = array(
								"xls" => "fa fa-file-excel-o",
								"xlsx" => "fa fa-file-excel-o",
								"pdf" => "fa fa-file-pdf-o",
								"doc" => "fa fa-file-word-o",
								"docx" => "fa fa-file-word-o",
								"jpg" => "fa fa-file-image-o",
								"dwg" => "fa fa-map-o",
								"msg" => "fa fa-envelope-o",
								"zip" => "fa fa-file-zip-o",
								"txt" => "fa fa-file-text-o"
							);
							
				$icon = "";
				$swext = strtolower(end($exts));
				
				$icon = ( array_key_exists($swext,$ext2fa) )? $ext2fa[$swext] : "fa fa-file-o";
				
				if($dir_file["dir"])
				{
					$li_attr = array(
									"class" => "menuDir",
									"direccionftp" => $dir_file["nombre"]
								);
				}
				else
				{
					$li_attr = array(
									"class" => "menuFile",
									"direccionftp" => $dir_file["nombre"]
								);
				}
				$li_attr = json_encode($li_attr);

				$text = explode("/",$dir_file["nombre"]);
				$node = array(
						"text" => end($text),
						"icon" => ( ($dir_file["dir"])? "" : $icon ),
						"state" => array(
								"opened" => false,
								"disabled" => false,
								"selected" => false
							),
						"children" => ( ($dir_file["dir"])? $this->ftp_listar($conn,$dir_file["nombre"]) : array() ),
						"li_attr" => $li_attr,
						"dir" => $dir_file["dir"]
					);
					
				array_push($listadoFinal,$node);
			}
			
			usort($listadoFinal,array("Ftp","cmpDirArchTexto"));
			return $listadoFinal;
		}
	}
	
	public function download()
	{
		$dir = $this->input->post("dir");

		$this->load->library('ftp');
		
		$config['hostname'] = '10.30.25.45';
		$config['username'] = 'cistey';
		$config['password'] = 'c1st3y06120201';
		$config['debug']        = TRUE;

		/*$config['hostname'] = '172.21.54.201';
		$config['username'] = 'mamaro';
		$config['password'] = 'YWaPSFHo';
		$config['debug']        = TRUE;*/

		$this->ftp->connect($config);

		$exploded = explode("/",$dir);
		$file = end($exploded);
		
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$file.'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		$this->ftp->download($dir, 'php://output');
		
		$this->ftp->close();
	}
	
	public function delete()
	{
		$dir = $this->input->post("dir");

		$this->load->library('ftp');

		$config['hostname'] = '10.30.25.45';
		$config['username'] = 'cistey';
		$config['password'] = 'c1st3y06120201';
		$config['debug']        = TRUE;

		/*$config['hostname'] = '172.21.54.201';
		$config['username'] = 'mamaro';
		$config['password'] = 'YWaPSFHo';
		$config['debug']        = TRUE;*/

		$this->ftp->connect($config);

		$this->ftp->delete_file($dir);
		
		$this->ftp->close();
	}

	public function delete_dir()
		{
			$dir = $this->input->post("dir");

			$this->load->library('ftp');

			$config['hostname'] = '10.30.25.45';
			$config['username'] = 'cistey';
			$config['password'] = 'c1st3y06120201';
			$config['debug']        = TRUE;

			$this->ftp->connect($config);
			
			$this->ftp->delete_dir($dir);
			
			$this->ftp->close();
		}




	
    public function create()
	{
		$dir = $this->input->post("dir");

		$this->load->library('ftp');

		$config['hostname'] = '10.30.25.45';
		$config['username'] = 'cistey';
		$config['password'] = 'c1st3y06120201';
		$config['debug']    = TRUE;

		$this->ftp->connect($config);

		$this->ftp->mkdir($dir, 0755);	

		$this->ftp->close();
	}




	public function upload()
	{
		
		ini_set( 'memory_limit', '2048M' );
		ini_set('upload_max_filesize', '100M');  
		ini_set('post_max_size', '100M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$name = $this->input->post("name");
		
		/*$config['upload_path'] = './application/uploads/ftp';*/
		$config['upload_path'] = '/var/www/html/uploads/inventarioGeneral/ftp';
		$config['allowed_types'] = '*';
		// $config['max_size'] = '5120';
		$config['max_size'] = '100120';
		$config['overwrite'] = TRUE;
		$this->load->library('upload', $config);
		
		if($this->upload->do_upload('file'))
		{
			$dir = $this->input->post("dir");
			
			$upload_data = $this->upload->data();
            $fileName = $upload_data['file_name'];
			
			/*$source = './application/uploads/ftp/'.$fileName;*/
			$source = '/var/www/html/uploads/inventarioGeneral/ftp/'.$fileName;
			
			$this->load->library('ftp');
			
			$ftp_config['hostname'] = '10.30.25.45';
			$ftp_config['username'] = 'cistey';
			$ftp_config['password'] = 'c1st3y06120201';
			$ftp_config['debug']        = TRUE;
				
			/*$ftp_config['hostname'] = '172.21.54.201';
			$ftp_config['username'] = 'mamaro';
			$ftp_config['password'] = 'YWaPSFHo';
			$ftp_config['debug']        = TRUE;*/

			$this->ftp->connect($ftp_config);
			
			$destination = $dir.'/'.$fileName;
			
			$this->ftp->upload($source,$destination);
			
			$this->ftp->close();
			
			unlink($source);
		}
	}
	
	public function rename()
	{
		$dir = $this->input->post("dir");
		$name = $this->input->post("value");

		$this->load->library('ftp');

		$config['hostname'] = '10.30.25.45';
		$config['username'] = 'cistey';
		$config['password'] = 'c1st3y06120201';
		$config['debug']        = TRUE;

		/*$config['hostname'] = '172.21.54.201';
		$config['username'] = 'mamaro';
		$config['password'] = 'YWaPSFHo';
		$config['debug']        = TRUE;*/

		$this->ftp->connect($config);

		$exploded = explode("/",$dir,-1);
		$path = implode("/",$exploded);
		
		$explodedFile = explode("/",$dir);
		$file = end($explodedFile);
		
		$explodedExt = explode(".",$file);
		$ext = end($explodedExt);
		
		$this->ftp->rename($dir,'/'.$path.'/'.$name.'.'.$ext);
		
		$this->ftp->close();
	}
	
     public function rename_dir()
	{
		$dir = $this->input->post("dir");
		$name = $this->input->post("value");

		$this->load->library('ftp');
		
		$config['hostname'] = '10.30.25.45';
		$config['username'] = 'cistey';
		$config['password'] = 'c1st3y06120201';
		$config['debug']        = TRUE;

		$this->ftp->connect($config);
		$exploded = explode("/",$dir,-1);
		$path = implode("/",$exploded);		
		$this->ftp->rename($dir,'/'.$path.'/'.$name.'/');		
		$this->ftp->close();
	}












	/*public function listar_ftp()
	{
		$this->load->library('ftp');

		$config['hostname'] = '10.30.25.45';
		$config['username'] = 'anima';
		$config['password'] = '4n1m406120234';
		$config['debug']        = TRUE;
		
		$this->ftp->connect($config);
		
		$listado = $this->ftp->list_files("/INFRA/01.Inventario_General/0130545_LM_Huertos_De_San_Jose/");
		
		foreach($listado as &$value)
		{
			$value = str_replace("/INFRA/01.Inventario_General", "", $value);
		}
		
		print_r($listado);
	}*/
}