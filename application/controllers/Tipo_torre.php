<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_torre extends CI_Controller {

	public function listar()
	{
		$this->load->model('Tipo_torre_model');

		$listado = $this->Tipo_torre_model->get_tipo_torre();

		echo json_encode($listado);
	}


	public function editar($id)
	{
		$this->load->model('Tipo_torre_model');

		$tipo_torre = $this->Tipo_torre_model->edit($id);

		echo json_encode($tipo_torre);
	}

	public function actualizar($id)
	{
		$this->load->model('Tipo_torre_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['Tipo_torre'] = $this->input->input_stream('Tipo_torre');

		$this->Tipo_torre_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Tipo_torre_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['Tipo_torre'] = $this->input->post('Tipo_torre');

		$this->Tipo_torre_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Tipo_torre_model');

		$this->Tipo_torre_model->delete($id);
	}
}