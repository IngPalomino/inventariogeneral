<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antenas_iden extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 3;
	}

	public function listar()
	{
		$this->load->model('Antenas_iden_model');

		$listado = $this->Antenas_iden_model->get_antenas_iden();

		echo json_encode($listado);
	}

	public function listar_sitio($id){
		$this->load->model('Antenas_iden_model');

		$listado = $this->Antenas_iden_model->get_antenas_iden_sitio($id);

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Antenas_iden_model');

		$antena_iden = $this->Antenas_iden_model->edit($id);

		echo json_encode($antena_iden);
	}

	public function actualizar($id)
	{
		$this->load->model('Antenas_iden_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$info_antena = array();
		$info_antena['numero_serie'] = $info['numero_serie'];
		$info_antena['sector'] = $info['sector'];
		$info_antena['altura'] = $info['altura'];
		$info_antena['azimuth'] = $info['azimuth'];
		$info_antena['tilt_electrico'] = $info['tilt_electrico'];
		$info_antena['tilt_mecanico'] = $info['tilt_mecanico'];
		$info_antena['cantidad'] = $info['cantidad'];

		$data_tipo = array();
		$data_tipo['marca'] = $info['marca'];
		$data_tipo['modelo'] = $info['modelo'];

		$this->load->model('Tipo_antena_iden_model');

		$tipo = $this->Tipo_antena_iden_model->get_type($data_tipo);
		$info_antena['tipo_antena_iden'] = $tipo[0]['id'];

		$antena_iden = $this->Antenas_iden_model->edit($id);

		if($this->Antenas_iden_model->update($id, $info_antena))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Antena iDEN",
					"tabla"			=> "tb_antenas_iden",
					"id"			=> $id,
					"datos viejos"	=> $antena_iden,
					"datos nuevos"	=> $this->Antenas_iden_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_antenas_iden_model');
				$this->Logs_antenas_iden_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$evento["sitio"] = $this->Antenas_iden_model->get_sitio_antena($id);
				$evento["link"] = "ver/sitios/infraestructura/antenas/".$evento["sitio"]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('Antenas_iden_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$info_antena = array();
		$info_antena['numero_serie'] = $info['numero_serie'];
		$info_antena['sector'] = $info['sector'];
		$info_antena['altura'] = $info['altura'];
		$info_antena['azimuth'] = $info['azimuth'];
		$info_antena['tilt_electrico'] = $info['tilt_electrico'];
		$info_antena['tilt_mecanico'] = $info['tilt_mecanico'];
		$info_antena['cantidad'] = $info['cantidad'];
		$info_antena['sitio'] = $id;

		$data_tipo = array();
		$data_tipo['marca'] = $info['marca'];
		$data_tipo['modelo'] = $info['modelo'];

		$this->load->model('Tipo_antena_iden_model');

		$tipo = $this->Tipo_antena_iden_model->get_type($data_tipo);
		$info_antena['tipo_antena_iden'] = $tipo[0]['id'];

		$respuesta = $this->Antenas_iden_model->insert($info_antena);

		if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Creación de Antena iDEN",
					"tabla"		=> "tb_antenas_iden"
				);

			$this->load->model('Logs_antenas_iden_model');
			$this->Logs_antenas_iden_model->ingreso_de_datos($session['id_usuario'],$evento);

			$this->load->model('Status_site_model');
			$evento["link"] = "ver/sitios/infraestructura/antenas/".$id;
			$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($id);
			$evento["elemento"] = $this->Antenas_iden_model->edit($respuesta["last_id"]);
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $id);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function almacenar($id)
	{
		$this->load->library('session');
		$this->load->model('Antenas_iden_model');

		$info = $this->input->input_stream();
		$session = $this->session->userdata();

		$info_antena['eliminado'] = 1;

		if(isset($session["logged_in"]) && $session["auth"] <= 2)
		{
			if($this->Antenas_iden_model->update($id, $info_antena))
			{
				$evento = array(
						"accion"		=> "Almacenamiento de Antena iDEN",
						"tabla"			=> "tb_antenas_iden",
						"id"			=> $id
					);

				$this->load->model('Logs_antenas_iden_model');
				$this->Logs_antenas_iden_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

				$this->load->model('Bodega_antenas_iden_model');
				unset($info['comentario']);

				if($this->Bodega_antenas_iden_model->insert($info))
				{
					$evento = array(
						"accion"		=> "Almacenamiento de Antena iDEN",
						"tabla"			=> "tb_bodega_antenas_iden"
					);

					$this->load->model('Logs_bodega_antenas_iden_model');
					$this->Logs_bodega_antenas_iden_model->ingreso_de_datos($session['id_usuario'],$evento);
				}
			}
		}
		else
		{
			show_404();
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Antenas_iden_model');

		$info = $this->input->input_stream();

		$info_antena['eliminado'] = 1;

		if($this->Antenas_iden_model->update($id, $info_antena))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Antena iDEN",
					"tabla"			=> "tb_antenas_iden",
					"id"			=> $id
				);

			$this->load->model('Logs_antenas_iden_model');
			$this->Logs_antenas_iden_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$evento["sitio"] = $this->Antenas_iden_model->get_sitio_antena($id);
			$evento["link"] = "ver/sitios/infraestructura/antenas/".$evento["sitio"]["id"];
			$evento["elemento"] = $this->Antenas_iden_model->edit($id);
			$evento["comentario"] = $info["comentario"];
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}
}