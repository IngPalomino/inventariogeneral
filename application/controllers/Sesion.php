<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sesion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url', 'custom'));
		$this->load->library('auth');
	}

	public function iniciar()
	{
		$post = $this->input->post();

		$this->load->library('session');
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_data($post);

		$config = array(
				array(
						'field' => 'usuario',
						'label' => 'usuario',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe ingresar un %s.'
							)
					),
				array(
						'field' => 'contrasena',
						'label' => 'contraseña',
						'rules' => 'required|callback_confirmar_inicio['.$post["usuario"].']',
						'errors' => array(
								'required' => 'Debe ingresar una %s.'
							)
					)
			);

		$this->form_validation->set_rules($config);
		$this->form_validation->set_error_delimiters("", "");

		$salida = array(
			'error' => FALSE,
			'logged' => FALSE);

		$userSession = array(
			'id_usuario' => 0,
			'usuario' => '',
			'nombres' => '',
			'logged_in' => false);

		if($this->form_validation->run() === FALSE)
		{
			$salida['error'] = validation_errors();
			echo json_encode($salida);
		}
		else
		{
			$usuario = $this->input->post('usuario');

			$this->load->model('Usuarios_model');
			$user = $this->Usuarios_model->select_by_user($usuario);

			$fechaActual = strtotime(date("Y-m-d H:i:s"));
			$user["fecha_inicio"] = strtotime($user["fecha_inicio"]);
			$user["fecha_fin"] = strtotime($user["fecha_fin"]);

			if( $user["fecha_inicio"] > $fechaActual )
			{
				$salida = array(
						'error' => "Su usuario aún no está activo.",
						'logged' => FALSE);

					echo json_encode($salida);
					return false;
			}
			else
			{
				if( ($user["fecha_fin"] != null) && ($user["fecha_fin"] < $fechaActual) )
				{
					$salida = array(
						'error' => "Su usuario ha caducado",
						'logged' => FALSE);

					echo json_encode($salida);
					return false;
				}
			}

			$responsable = $this->Usuarios_model->get_responsable($user['responsable']);

			$userSession['id_usuario'] = $user['id_usuario'];
			$userSession['usuario'] = $user['correo'];
			$userSession['nombres'] = $user['nombres'];
			$userSession['apellidos'] = $user['apellido_paterno'];
			$userSession['evaluador'] = $user['evaluador'];
			$userSession['grupos'] = $this->Usuarios_model->get_grupos_usuario($user['id_usuario']);
			$userSession['responsable'] = ($responsable? $responsable["nombres"]." ".$responsable["apellido_paterno"] : "");
			$userSession['logged_in'] = true;
			$this->session->set_userdata($userSession);

			$this->load->model('Logs_model');
			$this->Logs_model->inicio_sesion($user['id_usuario']);

			$salida['logged'] = TRUE;
			echo json_encode($salida);
		}
	}

	function confirmar_inicio($contrasena,$usuario)
	{
		$this->load->model('Sesion_model');

		$respuesta = $this->Sesion_model->confirmar_inicio($usuario,$contrasena);
		
		if(!$respuesta)
		{
			$this->form_validation->set_message('confirmar_inicio', 'El usuario y/o la contraseña no son válidos');
			return FALSE;
		}
		else
		{
			return true;
		}
	}

	public function cerrar()
	{
		$this->load->model('Logs_model');

		if ($this->auth->logged_in())
		{
			$this->Logs_model->cerrar_sesion($this->session->userdata('id_usuario'));
		}

		$this->session->sess_destroy();

		echo json_encode(array('logged' => FALSE));
	}
}