<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class E_gabinetes extends CI_Controller {
	
	public function listar()
	{
		$this->load->model('E_gabinetes_model');
		$listado = $this->E_gabinetes_model->get_gabinetes();
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('E_gabinetes_model');
		$listado = $this->E_gabinetes_model->get_gabinetes_sitio($id);
		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('E_gabinetes_model');
		$antena = $this->E_gabinetes_model->edit($id);
		echo json_encode($antena);
	}

	public function actualizar()
	{
		$this->load->model('E_gabinetes_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = $this->input->input_stream();
		
		$info["data"] = json_decode($info["data"],true);

		if(count($info["data"]) > 0)
		{
			foreach($info["data"] as $gabinete)
			{
				$info_gabinete["nefdn"] = $gabinete["nefdn"];
				$info_gabinete["model_physical"] = $gabinete["model_physical"];
				$info_gabinete["type_physical"] = $gabinete["type_physical"];
				$info_gabinete["date_manufacture_physical"] = str_replace("/","-",$gabinete["date_manufacture_physical"]);
				$info_gabinete["date_manufacture_physical"] = $info_gabinete["date_manufacture_physical"]? date("Y-m-d", strtotime($info_gabinete["date_manufacture_physical"])) : NULL;
				$info_gabinete["date_manufacture_m2000"] = str_replace("/","-",$gabinete["date_manufacture_m2000"]);
				$info_gabinete["date_manufacture_m2000"] = $info_gabinete["date_manufacture_m2000"]? date("Y-m-d", strtotime($info_gabinete["date_manufacture_m2000"])) : NULL;
				$info_gabinete["bom_code"] = $gabinete["bom_code"];
				$info_gabinete["manufacturer_data"] = $gabinete["manufacturer_data"];
				$info_gabinete["rack_type_physical"] = $gabinete["rack_type_physical"];
				$info_gabinete["rack_type_m2000"] = $gabinete["rack_type_m2000"];
				$info_gabinete["serial_number_physical"] = $gabinete["serial_number_physical"];
				$info_gabinete["serial_number_m2000"] = $gabinete["serial_number_m2000"];
				$info_gabinete["vendor_name"] = $gabinete["vendor_name"];
				$this->E_gabinetes_model->update($gabinete["id"],$info_gabinete);
			}
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('E_gabinetes_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["data"] = json_decode($info["data"],true);

		if(count($info["data"]) > 0)
		{
			foreach($info["data"] as $gabinete)
			{
				$gabinete["sitio"] = $id;
				$gabinete["date_manufacture_physical"] = str_replace("/","-",$gabinete["date_manufacture_physical"]);
				$gabinete["date_manufacture_physical"] = $gabinete["date_manufacture_physical"]? date("Y-m-d", strtotime($gabinete["date_manufacture_physical"])) : NULL;
				$gabinete["date_manufacture_m2000"] = str_replace("/","-",$gabinete["date_manufacture_m2000"]);
				$gabinete["date_manufacture_m2000"] = $gabinete["date_manufacture_m2000"]? date("Y-m-d", strtotime($gabinete["date_manufacture_m2000"])) : NULL;
				$this->E_gabinetes_model->insert($gabinete);
			}
		}
	}

	public function eliminar()
	{
		$this->load->model('E_gabinetes_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = array();
		$info = $this->input->input_stream();
		
		$info["data"] = json_decode($info["data"],true);
		
		foreach($info["data"] as $gabinete)
		{
			$this->E_gabinetes_model->delete($gabinete);
		}
	}
}