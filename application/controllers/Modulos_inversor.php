<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modulos_inversor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('custom'));
		$this->load->model('Modulos_inversor_model');
		$this->load->library('session');
	}

	public function listar()
	{
		$listado = $this->Modulos_inversor_model->get_modulos_inversor();
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$listado = $this->Modulos_inversor_model->get_modulos_inversor_sitio($id);
		echo json_encode($listado);
	}

	public function editar($id)
	{
		$modulos_inversor = $this->Modulos_inversor_model->edit($id);
		echo json_encode($modulos_inversor);
	}

	public function actualizar($id)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$modulos_inversor = $this->Modulos_inversor_model->edit($id);

		if($this->Modulos_inversor_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Modulos Inversores",
					"tabla"			=> "tb_modulos_inversor",
					"id"			=> $id,
					"datos viejos"	=> $modulos_inversor,
					"datos nuevos"	=> $this->Modulos_inversor_model->edit($id)
				);

			$this->load->model('Logs_modulos_inversor_model');
			$this->Logs_modulos_inversor_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
		}
	}

	public function crear_sitio($id)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["sitio"] = $id;

		if($this->Modulos_inversor_model->insert($info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Creación de Modulos Inversores",
					"tabla"		=> "tb_modulos_inversor"
				);

			$this->load->model('Logs_modulos_inversor_model');
			$this->Logs_modulos_inversor_model->ingreso_de_datos($session['id_usuario'],$evento);
		}
	}

	public function eliminar($id)
	{
		$info = $this->input->input_stream();

		$info_modulo['eliminado'] = 1;

		if($this->Modulos_inversor_model->update($id, $info_modulo))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Modulo Inversor",
					"tabla"			=> "tb_modulos_inversor",
					"id"			=> $id
				);

			$this->load->model('Logs_modulos_inversor_model');
			$this->Logs_modulos_inversor_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);
		}
	}

	public function almacenar()
	{
		$info = $this->input->post();
		$info_modulo["eliminado"] = 1;
		$info_modulos = json_decode($info["modulos_inversor"], TRUE);
		$this->load->model('Bodega_modulos_inversor_model');
		$this->load->model('Logs_bodega_modulos_inversor_model');

		foreach($info_modulos as $modulo)
		{
			if($this->Modulos_inversor_model->update($modulo["id"], $info_modulo))
			{
				$bmod["controlador_inversor"] = $modulo["controlador_inversor"];
				$bmod["tipo_modulos_inversor"] = $modulo["tipo_modulos_inversor"]["id"];
				$bmod["numero_serie"] = $modulo["numero_serie"];
				$bmod["estado"] = $info["estado"];
				$bmod["proveniente_sitio"] = $info["proveniente_sitio"];
				$bmod["tipo_almacen"] = $info["tipo_almacen"];
				$bmod["almacen"] = $info["almacen"];
				$this->Bodega_modulos_inversor_model->insert($bmod);

				$evento = array(
					"accion"		=> "Almacenamiento de Módulo Inversor",
					"tabla"			=> "tb_bodega_modulos_inversor"
				);

				$this->Logs_bodega_modulos_inversor_model->ingreso_de_datos($this->session->userdata('id_usuario'), $evento);
			}
		}
	}
}