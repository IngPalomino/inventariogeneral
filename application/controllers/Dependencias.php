<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dependencias extends CI_Controller {

	public function listar()
	{
		$this->load->model('Dependencias_model');

		$listado = $this->Dependencias_model->get_dependencies();

		echo json_encode($listado);
	}

	/*public function editar($id)
	{
		$this->load->model('Dependencias_model');

		$usuario = $this->Dependencias_model->get_user($id);

		echo json_encode($usuario);
	}

	public function actualizar($id)
	{
		$this->load->model('Dependencias_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['nombres'] = $this->input->input_stream('nombres');
		$info['apellidos'] = $this->input->input_stream('apellidos');
		$info['autorizacion'] = $this->input->input_stream('autorizacion');

		$this->Dependencias_model->update($id, $info);
	}*/

	public function crear()
	{
		$this->load->model('Dependencias_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Dependencias_model->insert($info);
	}

	/*public function eliminar($id)
	{
		$this->load->model('Dependencias_model');

		$this->Dependencias_model->delete($id);
	}*/
}