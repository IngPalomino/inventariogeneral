<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bancos_baterias extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 8;
	}
	
	public function listar()
	{
		$this->load->model('Bancos_baterias_model');

		$listado = $this->Bancos_baterias_model->get_bancos_baterias();

		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Bancos_baterias_model');

		$listado = $this->Bancos_baterias_model->get_bancos_baterias_sitio($id);

		echo json_encode($listado);
	}

    public function listar_sitio_mso($id)
	{
	   $this->load->model('Bancos_baterias_model');
       $listado = $this->Bancos_baterias_model->get_bancos_baterias_sitio_mso($id);

       
	   echo json_encode($listado);
	}
	
	/*public function listar_sitio_resumen($id)
	{
		$this->load->model('Bancos_baterias_model');

		$listado = $this->Bancos_baterias_model->get_bancos_baterias_sitio_resumen($id);

		echo json_encode($listado);
	}*/
	
	public function editar($id)
	{
		$this->load->model('Bancos_baterias_model');

		$aire = $this->Bancos_baterias_model->edit($id);

		echo json_encode($aire);
	}

	public function actualizar($id)
	{
		$this->load->model('Bancos_baterias_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();
		
		$info["celdas_controlador_rectif_bancos_baterias"] = json_decode($info["celdas_controlador_rectif_bancos_baterias"],true);
		
		$info_banco["controlador_rectif"] = $info["controlador_rectif"];
		$info_banco["tipo_bancos_baterias"] = $info["tipo_bancos_baterias"];
		$info_banco["fecha_instalacion"] = $info["fecha_instalacion"];
		
		$banco = $this->Bancos_baterias_model->edit($id);

		if($this->Bancos_baterias_model->update($id, $info_banco))
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			
			if(count($info["celdas_controlador_rectif_bancos_baterias"]) > 0)
			{
				$this->load->model('Celdas_bancos_baterias_model');
				foreach($info["celdas_controlador_rectif_bancos_baterias"] as $celda)
				{
					$celdas = $this->Celdas_bancos_baterias_model->edit($celda["id"]);
					
					$celda_info["fecha_instalacion"] = $celda["fecha_instalacion"];
					$celda_info["numero_serie"] = $celda["numero_serie"];
					$celda_info["tipo_bancos_baterias"] = $celda["tipo_bancos_baterias"]["id"];
					
					if($this->Celdas_bancos_baterias_model->update($celda["id"],$celda_info))
					{
						$this->load->library('session');
						$session = $this->session->userdata();
							
						$evento = array(
									"accion"		=> "Actualización de Celdas de un Banco de Baterías",
									"tabla"			=> "tb_celdas_controlador_rectif_bancos_baterias",
									"id"			=> $celda["id"],
									"datos viejos"	=> $celdas,
									"datos nuevos"	=> $this->Celdas_bancos_baterias_model->edit($celda["id"])
								);
						
						if($evento["datos viejos"] != $evento["datos nuevos"])
						{
							$this->load->model('Logs_celdas_controlador_rectif_bancos_baterias_model');
							$this->Logs_celdas_controlador_rectif_bancos_baterias_model->actualizacion_de_datos($session['id_usuario'],$celda["id"],$evento);
						}
					}
				}
			}

			$evento = array(
					"accion"		=> "Actualización de Banco de Baterías",
					"tabla"			=> "tb_controlador_rectif_bancos_baterias",
					"id"			=> $id,
					"datos viejos"	=> $banco,
					"datos nuevos"	=> $this->Bancos_baterias_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_controlador_rectif_bancos_baterias_model');
				$this->Logs_controlador_rectif_bancos_baterias_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$evento["sitio"] = $this->Bancos_baterias_model->get_sitio_banco($id);
				$evento["link"] = "ver/sitios/energia/bancos_baterias/".$evento["sitio"]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function crear_sitio()
	{
		$this->load->model('Bancos_baterias_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		$cantidad = $info["cantidad_bancos"];
		
		$info_banco["controlador_rectif"] = $info["controlador_rectif"];
		$info_banco["tipo_bancos_baterias"] = $info["tipo_bancos_baterias"];
		$info_banco["fecha_instalacion"] = $info["fecha_instalacion"];		

		if(isset($info["id_sala_tecnica"])){
			$info_banco["id_sala_tecnica"] = $info["id_sala_tecnica"];
		}
		
		for($i = 0; $i < $cantidad; $i++)
		{
			$respuesta = $this->Bancos_baterias_model->insert($info_banco);
			if($respuesta["resp"])
			{
				$this->load->library('session');
				$session = $this->session->userdata();
				
				$tipo = $this->Bancos_baterias_model->get_tipo_bancos_baterias($info_banco["tipo_bancos_baterias"]);
				
				for($j = 0; $j < $tipo[0]["celdas"]; $j++)
				{
					$info_celda["controlador_rectif_bancos_baterias"] = $respuesta["last_id"];
					$info_celda["fecha_instalacion"] = $info["fecha_instalacion"];
					$info_celda["numero_serie"] = "Pendiente";
					
					$this->load->model('Celdas_bancos_baterias_model');
					
					if($this->Celdas_bancos_baterias_model->insert($info_celda))
					{
						$this->load->library('session');
						$session = $this->session->userdata();
						
						$evento = array(
								"accion"	=> "Creación de Celda de Banco de Baterías",
								"tabla"		=> "tb_celdas_controlador_rectif_bancos_baterias"
							);
							
						$this->load->model('Logs_celdas_controlador_rectif_bancos_baterias_model');
						$this->Logs_celdas_controlador_rectif_bancos_baterias_model->ingreso_de_datos($session['id_usuario'],$evento);
					}
				}

				$evento = array(
						"accion"	=> "Creación de Banco de Baterías",
						"tabla"		=> "tb_controlador_rectif_bancos_baterias",
						"id"		=> $respuesta["last_id"]
					);

				$this->load->model('Logs_controlador_rectif_bancos_baterias_model');
				$this->Logs_controlador_rectif_bancos_baterias_model->ingreso_de_datos($session['id_usuario'],$evento);

				$evento["sitio"] = $this->Bancos_baterias_model->get_sitio_banco($respuesta["last_id"]);
				$evento["link"] = "ver/sitios/energia/bancos_baterias/".$evento["sitio"]["id"];
				$evento["elemento"] = $this->Bancos_baterias_model->edit($respuesta["last_id"]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				//$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Bancos_baterias_model');

		$info = $this->input->input_stream();

		$info_banco['eliminado'] = 1;

		if($this->Bancos_baterias_model->update($id, $info_banco))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Banco de Baterías",
					"tabla"			=> "tb_controlador_rectif_bancos_baterias",
					"id"			=> $id
				);

			$this->load->model('Logs_controlador_rectif_bancos_baterias_model');
			$this->Logs_controlador_rectif_bancos_baterias_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$evento["sitio"] = $this->Bancos_baterias_model->get_sitio_banco($id);
			$evento["link"] = "ver/sitios/energia/bancos_baterias/".$evento["sitio"]["id"];
			$evento["elemento"] = $this->Bancos_baterias_model->edit($id);
			$evento["comentario"] = $info["comentario"];
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function almacenar($id)
	{
		$this->load->model('Bancos_baterias_model');

		$info = $this->input->input_stream();
		$info_banco["eliminado"] = 1;
		$info_celdas = json_decode($info["celdas_banco_baterias"], true);
		
		if($this->Bancos_baterias_model->update($id, $info_banco))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Almacenamiento de Banco de Baterías",
					"tabla"			=> "tb_controlador_rectif_bancos_baterias",
					"id"			=> $id
				);

			$this->load->model('Logs_controlador_rectif_bancos_baterias_model');
			$this->Logs_controlador_rectif_bancos_baterias_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$this->load->model('Celdas_bancos_baterias_model');
			$this->Celdas_bancos_baterias_model->update_where(array("controlador_rectif_bancos_baterias" => $id), $info_banco);

			$this->load->model('Bodega_bancos_baterias_model');
			
			unset($info['comentario']);
			unset($info['celdas_banco_baterias']);
			

			$respuesta = $this->Bodega_bancos_baterias_model->insert($info);

			if($respuesta["resp"])
			{
				$evento = array(
					"accion"		=> "Almacenamiento de Banco de Baterías",
					"tabla"			=> "tb_bodega_bancos_baterias"
				);

				$this->load->model('Logs_bodega_bancos_baterias_model');
				$this->Logs_bodega_bancos_baterias_model->ingreso_de_datos($session['id_usuario'],$evento);

				if(count($info_celdas) > 0)
				{
					$this->load->model('Bodega_celdas_bancos_baterias_model');

					foreach($info_celdas as $celda)
					{
						$info_celda["banco_baterias"] = $respuesta["last_id"];
						$info_celda["tipo_bancos_baterias"] = $celda["tipo_bancos_baterias"];
						$info_celda["fecha_instalacion"] = $celda["fecha_instalacion"];
						$info_celda["numero_serie"] = $celda["numero_serie"];

						$this->Bodega_celdas_bancos_baterias_model->insert($info_celda);
					}
				}
			}
		}
	}
}