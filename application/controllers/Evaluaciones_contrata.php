<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluaciones_contrata extends CI_Controller {
	public function listar()
	{
		$this->load->model('Evaluaciones_contrata_model');

		$listado = $this->Evaluaciones_contrata_model->listar();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Evaluaciones_contrata_model');

		$listado = $this->Evaluaciones_contrata_model->editar($id);

		echo json_encode($listado);
	}

	public function actualizar()
	{
		
	}

	public function insertar()
	{
		
	}

	public function borrar()
	{
		
	}
}

?>