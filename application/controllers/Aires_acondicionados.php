<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aires_acondicionados extends CI_Controller {

	private $modulo;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Aires_acondicionados_model');
		$this->load->model('Notificaciones_model');
		$this->load->library(array('auth', 'custom'));
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 12;
	}
	
	public function listar()
	{
		echo json_encode($this->Aires_acondicionados_model->get_aires_acondicionados());
	}

	public function listar_sitio($id)
	{
		echo json_encode($this->Aires_acondicionados_model->get_aires_acondicionados_sitio($id));
	}

	public function listar_sitio_mso($id)
	{
		echo json_encode($this->Aires_acondicionados_model->get_aires_acondicionados_sitio_mso($id));
	}


	public function editar($id)
	{
		echo json_encode($this->Aires_acondicionados_model->edit($id));
	}

	public function actualizar($id)
	{
		$info = $this->input->input_stream();

		$aire = $this->Aires_acondicionados_model->edit($id);

		if($this->Aires_acondicionados_model->update($id, $info))
		{
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Aire Acondicionado",
					"tabla"			=> "tb_aires_acondicionados",
					"id"			=> $id,
					"datos viejos"	=> $aire,
					"datos nuevos"	=> $this->Aires_acondicionados_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_aires_acondicionados_model');
				$this->Logs_aires_acondicionados_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$evento["sitio"] = $this->Aires_acondicionados_model->get_sitio_aire($id);
				$evento["link"] = "ver/sitios/energia/aires_acondicionados/".$evento["sitio"]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('Aires_acondicionados_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		$info["sitio"] = $id;
		
        if(isset($info["id_sala_tecnica"])){
         $info["id_sala_tecnica"] = $info["id_sala_tecnica"];
        }	

		$respuesta = $this->Aires_acondicionados_model->insert($info);

		if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Creación de Aire Acondicionado",
					"tabla"		=> "tb_aires_acondicionados"
				);

			$this->load->model('Logs_aires_acondicionados_model');
			$this->Logs_aires_acondicionados_model->ingreso_de_datos($session['id_usuario'],$evento);

			$this->load->model('Status_site_model');
			$evento["link"] = "ver/sitios/energia/aires_acondicionados/".$id;
			$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($id);
			$evento["elemento"] = $this->Aires_acondicionados_model->edit($respuesta["last_id"]);
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $id);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Aires_acondicionados_model');

		$info = $this->input->input_stream();

		$info_aire['eliminado'] = 1;

		if($this->Aires_acondicionados_model->update($id, $info_aire))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Aire Acondicionado",
					"tabla"			=> "tb_aires_acondicionados",
					"id"			=> $id
				);

			$this->load->model('Logs_aires_acondicionados_model');
			$this->Logs_aires_acondicionados_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$evento["sitio"] = $this->Aires_acondicionados_model->get_sitio_aire($id);
			$evento["link"] = "ver/sitios/energia/aires_acondicionados/".$evento["sitio"]["id"];
			$evento["elemento"] = $this->Aires_acondicionados_model->edit($id);
			$evento["comentario"] = $info["comentario"];
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function almacenar($id)
	{
		$this->load->model('Aires_acondicionados_model');

		$info = $this->input->input_stream();
		$info_aire["eliminado"] = 1;
		
		if($this->Aires_acondicionados_model->update($id, $info_aire))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Almacenamiento de Aire Acondicionado",
					"tabla"			=> "tb_aires_acondicionados",
					"id"			=> $id
				);

			$this->load->model('Logs_aires_acondicionados_model');
			$this->Logs_aires_acondicionados_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$this->load->model('Bodega_aires_acondicionados_model');
			unset($info['comentario']);

			if($this->Bodega_aires_acondicionados_model->insert($info))
			{
				$evento = array(
					"accion"		=> "Almacenamiento de Aire Acondicionado",
					"tabla"			=> "tb_bodega_aires_acondicionados"
				);

				$this->load->model('Logs_bodega_aires_acondicionados_model');
				$this->Logs_bodega_aires_acondicionados_model->ingreso_de_datos($session['id_usuario'],$evento);
			}
		}
	}
}