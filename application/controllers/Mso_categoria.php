<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mso_categoria extends CI_Controller
{
	public function __construct()
	{  
		parent::__construct();
		$this->load->library('auth');		
		$this->load->model('Mso_categoria_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->load->library('session');	
		
	}

	public function listar()
	{

		if ($this->auth->logged_in())
		{

        $lista = $this->Mso_categoria_model->get_all();  
		echo json_encode($lista);
		}
		else
		{
		  echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
		
	}


    public function listar_categoria($id)
	{

		if ($this->auth->logged_in())
		{
        $lista = $this->Mso_categoria_model->get_id($id);  
		echo json_encode($lista);
		}
		else
		{
		 echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
		
	}

	 




}