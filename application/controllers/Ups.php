<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ups extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 6;
	}

	public function listar()
	{
		$this->load->model('Ups_model');
		$listado = $this->Ups_model->get_ups();
		echo json_encode($listado);
	}

	public function listar_sitio($id) {
		$this->load->model('Ups_model');
		$listado = $this->Ups_model->get_ups_sitio_mso($id);
		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('Ups_model');
		$ups = $this->Ups_model->edit($id);
		echo json_encode($ups);
	}

	public function actualizar($id)
	{
		$this->load->model('Ups_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = array();
		$info = $this->input->input_stream();
		$ups = $this->Ups_model->edit($id);         
        // echo json_encode($info);
		if($this->Ups_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			$evento = array(
					"accion"		=> "Actualización de Ups",
					"tabla"			=> "tb_ups",
					"id"			=> $id,
					"datos viejos"	=> $ups,
					"datos nuevos"	=> $this->Ups_model->edit($id)
				);
				
			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_ups_model');
				$this->Logs_ups_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
				$evento["sitio"] = $this->Ups_model->get_sitio_ups($id);
				$evento["link"] = "ver/salastecnicas/mso/energia/ups/".$evento["sitio"]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				//$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function crear_sitio($id)
	{

		$this->load->model('Ups_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = array();
		$infoUps = array(); 		
		$info = $this->input->post();
         //var_dump($info);
		$infoUps["sitio"] = $id;
		$infoUps["ubicacion"] = $info["ubicacion"]; 
		$infoUps["tipo_ups"] = $info["tipo_ups"];
		$infoUps["numero_serie"] = $info["numero_serie"];
		$infoUps["capacidad"] = $info["capacidad"]; 
		$infoUps["fases"] = $info["fases"]; 
		$infoUps["tipo_trasformador"] = $info["tipo_trasformador"];
		$infoUps["controlador_rectif_bancos_baterias"] = $info["controlador_rectif_bancos_baterias"];  
		$infoUps["voltaje_entrada"] = $info["voltaje_entrada"];
		$infoUps["voltaje_salida"] = $info["voltaje_salida"];

		if(isset($info["id_sala_tecnica"])){
		     $infoUps["id_sala_tecnica"] = $info["id_sala_tecnica"];
		     $idmso = $info["id_sala_tecnica"];
		}
       //   var_dump($infoUps);
		$respuesta = $this->Ups_model->insert($infoUps);
	   //logs_ups
		if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			$evento = array(
				"accion"	=> "Creación de Ups",
				"id"	=> $id,
				"tabla"		=> "tb_ups"
			);
			$this->load->model('Logs_ups_model');
			$this->Logs_ups_model->ingreso_de_datos($session['id_usuario'],$evento);
			$this->load->model('Status_site_model');
			$evento["link"] = "ver/salastecnicas/mso/energia/ups/".$idmso;
			$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($id);			
			$evento["elemento"] = $this->Ups_model->edit($respuesta["last_id"]);
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $id);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			
			//$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function almacenar($id)
	{
		$this->load->model('Ups_model');
		$info = $this->input->input_stream();
		$info_ups["eliminado"] = 1;		
		if($this->Ups_model->update($id, $info_ups))
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			$evento = array(
					"accion"		=> "Almacenamiento de Ups",
					"tabla"			=> "tb_ups",
					"id"			=> $id
			);
			$this->load->model('Logs_ups_model');
			$this->Logs_ups_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$this->load->model('Bodega_ups_model');
			//unset($info['comentario']);
            //unset($info['tipo']);
			if($this->Bodega_ups_model->insert($info))
			{
				$evento = array(
					"accion"		=> "Almacenamiento de Ups",
					"tabla"			=> "tb_bodega_ups"
				);
				$this->load->model('Logs_bodega_ups_model');
				$this->Logs_bodega_ups_model->ingreso_de_datos($session['id_usuario'],$evento);
			}
		}
	}
	
	public function listar_sitios()
	{
		$this->load->model('Ups_model');

		$listado = $this->Ups_model->get_sitios_ups();

		echo json_encode($listado);
	}
	public function eliminar($id)
	{
		$this->load->model('Ups_model');

		$info = $this->input->input_stream();

		$info_ups['eliminado'] = 1;

		if($this->Ups_model->update($id, $info_ups))
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			$evento = array(
					"accion"		=> "Eliminación de Ups",
					"tabla"			=> "tb_ups",
					"id"			=> $id
			);

			$this->load->model('Logs_ups_model');
			$this->Logs_ups_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$evento["sitio"] = $this->Ups_model->get_sitio_ups($id);
			$evento["link"] = "ver/salastecnicas/mso/energia/ups/".$evento["id_mso"];
			$evento["elemento"] = $this->Ups_model->edit($id);
			$evento["comentario"] = $info["comentario"];
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}
}