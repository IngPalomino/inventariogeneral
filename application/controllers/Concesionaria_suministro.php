<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Concesionaria_suministro extends CI_Controller {

	public function listar()
	{
		$this->load->model('Concesionaria_suministro_model');

		$listado = $this->Concesionaria_suministro_model->get_types();

		echo json_encode($listado);
	}

	public function id_tipo()
	{
		$this->load->model('Concesionaria_suministro_model');

		$info = array();
		$info = $this->input->post();

		$tipo = $this->Concesionaria_suministro_model->get_type($info);

		echo json_encode($tipo);
	}

	public function editar($id)
	{
		$this->load->model('Concesionaria_suministro_model');

		$antena = $this->Concesionaria_suministro_model->edit($id);

		echo json_encode($antena);
	}

	public function actualizar($id)
	{
		$this->load->model('Concesionaria_suministro_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$this->Concesionaria_suministro_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Concesionaria_suministro_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Concesionaria_suministro_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Concesionaria_suministro_model');

		$this->Concesionaria_suministro_model->delete($id);
	}
}