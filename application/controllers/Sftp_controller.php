<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sftp_controller extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('sftp');
	}
	
	public function listar()
	{
		$dir = $this->input->post("dir");
		
		$config['hostname'] = '172.21.54.201';
		$config['username'] = 'mamaro';
		$config['password'] = 'YWaPSFHo';
		$config['debug']        = TRUE;
		
		$this->sftp->connect($config);

		$conn = $this->sftp;

		$listado = $this->sftp_listar($this->sftp,$dir);
		
		$this->sftp->close();
		echo json_encode($listado);
	}

	Private static function cmpDirArchTexto($a,$b)
	{
		if( $a["dir"] == $b["dir"] )
		{
			return strcmp($a["text"], $b["text"]);
		}
		else
		{
			return ( $a["dir"] > $b["dir"] )? -1 : 1 ;
		}
	}
	
	function sftp_listar($conn,$dir)
	{
		if(!isset($dir))
		{
			return false;
		}
		else
		{
			$list = $conn->list_files($dir);

			$listadoFinal = array();
			
			foreach($list as $dir_file)
			{
				$exts = explode(".",$dir_file);

				$dir_file = htmlentities($dir_file,ENT_QUOTES | ENT_SUBSTITUTE,"UTF-8"); // En prueba
				
				$dir_file = array(
							"dir" => ( (count($exts) > 1) ? preg_match("/[a-zA-Z]{5}/",end($exts) ) : true ),
							"nombre" => $dir_file
					);
					
				$ext2fa = array(
								"xls" => "fa fa-file-excel-o",
								"xlsx" => "fa fa-file-excel-o",
								"pdf" => "fa fa-file-pdf-o",
								"doc" => "fa fa-file-word-o",
								"docx" => "fa fa-file-word-o",
								"jpg" => "fa fa-file-image-o",
								"png" => "fa fa-file-image-o",
								"dwg" => "fa fa-map-o",
								"msg" => "fa fa-envelope-o",
								"zip" => "fa fa-file-zip-o",
								"txt" => "fa fa-file-text-o"
							);
							
				$icon = "";
				$swext = strtolower(end($exts));
				
				$icon = ( array_key_exists($swext,$ext2fa) )? $ext2fa[$swext] : "fa fa-file-o";

				if($dir_file["dir"])
				{
					$li_attr = array(
									"class" => "menuDir",
									"direccionftp" => $dir_file["nombre"]
								);
				}
				else
				{
					$li_attr = array(
									"class" => "menuFile",
									"direccionftp" => $dir_file["nombre"]
								);
				}
				$li_attr = json_encode($li_attr);

				$text = explode("/",$dir_file["nombre"]);
				$node = array(
						"text" => end($text), /* html_entity_decode((end($text)),ENT_SUBSTITUTE,"UTF-8") */
						"icon" => ( ($dir_file["dir"])? "" : $icon ),
						"state" => array(
								"opened" => false,
								"disabled" => false,
								"selected" => false
							),
						"children" => ( ($dir_file["dir"])? $this->sftp_listar($conn,$dir_file["nombre"]) : array() ),
						"li_attr" => $li_attr,
						"dir" => $dir_file["dir"]
					);

				array_push($listadoFinal,$node);
			}
			
			usort($listadoFinal,array("Sftp_controller","cmpDirArchTexto"));
			return $listadoFinal;
		}
	}
	
	public function download()
	{
		$dir = $this->input->post("dir");
		
		$config['hostname'] = '172.21.54.201';
		$config['username'] = 'mamaro';
		$config['password'] = 'YWaPSFHo';
		$config['debug']        = TRUE;
		
		$this->sftp->connect($config);
		
		$exploded = explode("/",$dir);
		$file = end($exploded);
		
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$file.'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		$this->sftp->download($dir, 'php://output');
		
		$this->sftp->close();
	}
	
	public function delete()
	{
		$dir = $this->input->post("dir");
		
		$config['hostname'] = '172.21.54.201';
		$config['username'] = 'mamaro';
		$config['password'] = 'YWaPSFHo';
		$config['debug']        = TRUE;

		$this->sftp->connect($config);
		
		$this->sftp->delete_file($dir);
		
		$this->sftp->close();
	}
	
	public function upload()
	{
		$name = $this->input->post("name");
		
		$config['upload_path'] = './application/uploads/ftp';
		$config['allowed_types'] = '*';
		$config['max_size'] = '5120';
		$config['overwrite'] = TRUE;
		$this->load->library('upload', $config);
		
		if($this->upload->do_upload('file'))
		{
			$dir = $this->input->post("dir");
			
			$upload_data = $this->upload->data();
            $fileName = $upload_data['file_name'];
			
			$source = './application/uploads/ftp/'.$fileName;
				
			$sftp_config['hostname'] = '172.21.54.201';
			$sftp_config['username'] = 'mamaro';
			$sftp_config['password'] = 'YWaPSFHo';
			$sftp_config['debug']        = TRUE;

			$this->sftp->connect($sftp_config);
			
			$destination = $dir.'/'.$fileName;
			
			$this->sftp->upload($destination,$source);
			
			$this->sftp->close();
			
			unlink($source);
		}
	}
	
	public function rename()
	{
		$dir = $this->input->post("dir");
		$name = $this->input->post("value");
		
		$config['hostname'] = '172.21.54.201';
		$config['username'] = 'mamaro';
		$config['password'] = 'YWaPSFHo';
		$config['debug']        = TRUE;

		$this->sftp->connect($config);
		
		$exploded = explode("/",$dir,-1);
		$path = implode("/",$exploded);
		
		$explodedFile = explode("/",$dir);
		$file = end($explodedFile);
		
		$explodedExt = explode(".",$file);
		$ext = end($explodedExt);
		
		$this->sftp->rename($dir,$path.'/'.$name.'.'.$ext);
		
		$this->sftp->close();
	}
}