<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enlaces extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
	}
	
	public function listar()
	{
		$this->load->model('Enlaces_model');

		$listado = $this->Enlaces_model->get_enlaces();

		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Enlaces_model');

		$listado = $this->Enlaces_model->get_enlaces_sitio($id);

		usort($listado,array("Enlaces","cmpSitioCodigo"));

		echo json_encode($listado);
	}

	public function listar_sitio_coor($id)
	{
		$this->load->model('Enlaces_model');
		$listado = $this->Enlaces_model->get_enlaces_sitio_coor($id);
		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('Enlaces_model');

		$enlace = $this->Enlaces_model->edit($id);

		echo json_encode($enlace);
	}

	public function actualizar($id)
	{
		$this->load->model('Enlaces_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();
		
		$configuracion = "";
		
		if(!$info["configuracion_part_3"])
		{
			if(!$info["configuracion_part_4"])
			{
				$configuracion = $info["configuracion_part_1"]."+".$info["configuracion_part_2"];
			}
			else
			{
				$configuracion = $info["configuracion_part_1"]."+".$info["configuracion_part_2"]." ".$info["configuracion_part_4"];
			}
		}
		else
		{
			if(!$info["configuracion_part_4"])
			{
				$configuracion = $info["configuracion_part_1"]."+".$info["configuracion_part_2"]." ".$info["configuracion_part_3"];
			}
			else
			{
				$configuracion = $info["configuracion_part_1"]."+".$info["configuracion_part_2"]." ".$info["configuracion_part_3"]." ".$info["configuracion_part_4"];
			}
		}
		
		$info_enlace["configuracion"] = $configuracion;
		$info_enlace["polaridad"] = $info["polaridad"];
		$info_enlace["ancho_banda"] = $info["ancho_banda"];
		$info_enlace["modulacion"] = $info["modulacion"];
		$info_enlace["capacidad"] = $info["capacidad"];
		$info_enlace["modulacion_adaptativa"] = $info["modulacion_adaptativa"];
		$info_enlace["capacidad_am"] = $info["capacidad_am"];
		$info_enlace["banda"] = $info["banda"];
		$info_enlace["modulacion_am"] = $info["modulacion_am"];

		$enlace = $this->Enlaces_model->edit($id);

		if($this->Enlaces_model->update($id, $info_enlace))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Enlace Microondas",
					"tabla"			=> "tb_enlaces",
					"id"			=> $id,
					"datos viejos"	=> $enlace,
					"datos nuevos"	=> $this->Enlaces_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_enlaces_model');
				$this->Logs_enlaces_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$evento["sitio"] = $this->Enlaces_model->get_sitios_enlace($id);
				$evento["link"] = "ver/sitios/infraestructura/enlaces/".$evento["sitio"][0]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos(5, $evento["sitio"][0]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$evento["sitio"]["nombre_completo"] = $evento["sitio"][0]["nombre_completo"]." <==> ".$evento["sitio"][1]["nombre_completo"];
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function crear_sitio()
	{
		$this->load->model('Enlaces_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$configuracion = "";
		$modulacion_am = "";
		$capacidad_am = 0;
		
		if(!$info["configuracion_3"])
		{
			if(!$info["configuracion_4"])
			{
				$configuracion = $info["configuracion_1"]."+".$info["configuracion_2"];
			}
			else
			{
				$configuracion = $info["configuracion_1"]."+".$info["configuracion_2"]." ".$info["configuracion_4"];
			}
		}
		else
		{
			if(!$info["configuracion_4"])
			{
				$configuracion = $info["configuracion_1"]."+".$info["configuracion_2"]." ".$info["configuracion_3"];
			}
			else
			{
				$configuracion = $info["configuracion_1"]."+".$info["configuracion_2"]." ".$info["configuracion_3"]." ".$info["configuracion_4"];
			}
		}	
        

       //modulacion_adaptativa 
        if(isset($info["modulacion_adaptativa"])){
        }else{
           $info["modulacion_adaptativa"] = false;
        }
       //modulacion_adaptativa


		if($info["modulacion_adaptativa"] != 'true')
		{
			$info["modulacion_adaptativa"] = 0;
			$modulacion_am = "N/A";
			$capacidad_am = 0;
		}
		else
		{
			$info["modulacion_adaptativa"] = 1;
			$modulacion_am = $info["modulacion_am_numero"].$info["modulacion_am_letras"];
			$capacidad_am = $info["capacidad_am"];
		}

		$info["odus_enlaces_ne"] = json_decode($info["odus_enlaces_ne"],true);
		$info["odus_enlaces_fe"] = json_decode($info["odus_enlaces_fe"],true);
		$info["antenas_mw_enlaces_ne"] = json_decode($info["antenas_mw_enlaces_ne"],true);
		$info["antenas_mw_enlaces_fe"] = json_decode($info["antenas_mw_enlaces_fe"],true);
		
		$info_enlace["idu_1"] = $info["idu_1"];
		$info_enlace["idu_2"] = $info["idu_2"];
		$info_enlace["configuracion"] = $configuracion;
		$info_enlace["polaridad"] = $info["polaridad"];
		$info_enlace["ancho_banda"] = $info["ancho_banda"];
		$info_enlace["modulacion"] = $info["modulacion_numero"].$info["modulacion_letras"];
		$info_enlace["capacidad"] = $info["capacidad"];
		$info_enlace["modulacion_adaptativa"] = $info["modulacion_adaptativa"];
		$info_enlace["modulacion_am"] = $modulacion_am;
		$info_enlace["capacidad_am"] = $capacidad_am;
		$info_enlace["banda"] = $info["banda"];
		$info_equipos["odus_enlaces"] = array_merge($info["odus_enlaces_ne"],$info["odus_enlaces_fe"]);
        
        $altura_ne = $info["antenas_mw_enlaces_ne"][0]["altura"];
        $idsitio_ne = $info["antenas_mw_enlaces_ne"][0]["idsitio_ne"];
        $azimuth_ne = $info["antenas_mw_enlaces_ne"][0]["azimuth"];
        unset($info["antenas_mw_enlaces_ne"][0]["idsitio_ne"]);

        //antenas_mw_fe
        $altura_fe = $info["antenas_mw_enlaces_fe"][0]["altura"];
        $idsitio_fe = $info["antenas_mw_enlaces_fe"][0]["idsitio_fe"];
        $azimuth_fe = $info["antenas_mw_enlaces_fe"][0]["azimuth"];
        unset($info["antenas_mw_enlaces_fe"][0]["idsitio_fe"]);

        //se elimina idsitio porque no esta en la tabla micro
        //var_dump($info["antenas_mw_enlaces_ne"]);

	    $info_equipos["antenas_mw_enlaces"] = array_merge($info["antenas_mw_enlaces_ne"],$info["antenas_mw_enlaces_fe"]);
 

  //microondas ne  
  $acu = $acu2 = 0;     
  $acu += $this->antenas_registradas($idsitio_ne,$altura_ne,$azimuth_ne);
  $acu2 += $this->antenas_registradas($idsitio_fe,$altura_fe,$azimuth_fe);
  $acu = $acu + $acu2 ;
//microondas ne	

		 if($acu >0){
		  //si hay espacio reservados similares
		    echo json_encode($acu);
		    //echo "modulacion adaptativa :".$info["modulacion_adaptativa"];
		}else{
		//echo json_encode($acu);
		$this->grabar_enlace_antena_microondas($info_enlace,$info_equipos);
		//echo  "modulacion adaptativa :".$info["modulacion_adaptativa"];
		}
    //Verificando Espacio en Torre

	}

public function antenas_registradas($idsitio,$altura,$azimuth){

      //Verificando Espacio en Torre
          // Antenas 
          //Microondas      
          $this->load->model('Antenas_mw_model');       
          $datosTorre = $this->Antenas_mw_model->get_antenas_microondas_sitio_md($idsitio);
          //Microondas Pendiente
          $datosTorreMP = $this->Antenas_mw_model->get_antenas_microondas_sitio_pendiente($idsitio);
          //GUL
		  $this->load->model('Antenas_gul_model');       
		  $datosTorreG = $this->Antenas_gul_model->get_antenas_gul_sitio($idsitio);
          //GulPendiente		
		  $this->load->model('Antenas_gul_pendiente_model');
          $datosTorreGULP = $this->Antenas_gul_pendiente_model->get_antenas_gul_pendiente_sitio($idsitio);

          //buscamos coincidencias               
	      $acuGP = $acuG = $acuMP = $ctnG = $acu = $ctn = 0; 
          //buscamos coincidencias
	      //altura y azimuth enviados por formulario 
	      $alturaEnviado = $altura;	  	     
	      $azimuth_env = $azimuth;
	          $rport = "";
	          foreach ($datosTorre as $key => $torrevalue) {

                if(($torrevalue['estado_aprobacion']=="1")||($torrevalue['estado_aprobacion']=="2")){
                    	//ANTENAS PENDIENTES
                    }else{
		             //azimuth registrado        	
		               $azimuth_reg = $torrevalue['azimuth'];                                 
	                 //altura registrada
		               $altura_llenado = $torrevalue['altura'];
	                   $alInicial = $altura_llenado - 0.5;
	                   $alFinal = $altura_llenado + 0.5;                       
		              if(($alInicial>$alturaEnviado) || ($alturaEnviado > $alFinal)){
				          // altura sin utilizar                 
				      }else{
				       //altura utilizada				       
				       $this->load->library('searchowerspace');
                       $ctn = $this->searchowerspace->azimuth_giro($azimuth_reg,$azimuth_env);
				       $acu += $ctn;
				     }
			       }  

	              }
			$acuMP = $this->recorre_no_telnet($datosTorreMP,$alturaEnviado,$azimuth_env);   
            $acuG = $this->recorre_no_telnet($datosTorreG,$alturaEnviado,$azimuth_env);
			$acuGP = $this->recorre_no_telnet($datosTorreGULP,$alturaEnviado,$azimuth_env);
            $acu = $acu + $acuMP + $acuG + $acuGP;
           
           return $acu;

}




public function recorre_no_telnet($datosTorre,$alturaEnviado,$azimuth_env)
{
$acu = $ctn = 0; 
              foreach ($datosTorre as $key => $torrevalue) {            
                //gul registrado pendiente
            	//azimuth registrado        	
	             $azimuth_reg = $torrevalue['azimuth'];
	             $azInicial = $azimuth_reg - 5;
	             $azFinal = $azimuth_reg + 5;
                 //altura registrada
	             $altura_llenado = $torrevalue['altura'];
                 $alInicial = $altura_llenado - 0.5;
                 $alFinal = $altura_llenado + 0.5;

	          	 if(($alInicial>$alturaEnviado) || ($alturaEnviado > $alFinal)){
	          	 	 // altura sin utilizar                     
	      	     }else{
	          	    //altura utilizada
	      	     	$ctn = $this->azimuth_giro($azimuth_reg,$azimuth_env);	             
                    $acu += $ctn;
	          	 }
	           }
return $acu;
}






	public function listar_sitios($id)
	{
		$this->load->model('Enlaces_model');
		$sitios = $this->Enlaces_model->get_sitios($id);
		echo json_encode($sitios);
	}

	public function eliminar($id)
	{
		$this->load->model('Enlaces_model');

		$info = $this->input->input_stream();

		$info_enlace['eliminado'] = 1;

		if($this->Enlaces_model->update($id, $info_enlace))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Enlace Microondas",
					"tabla"			=> "tb_enlaces",
					"id"			=> $id
				);

			$info_equipos["eliminado"] = 1;
			$this->load->model('Antenas_mw_model');
			$this->load->model('Odus_model');

			$this->Antenas_mw_model->update_where(array("enlace" => $id),$info_equipos);
			$this->Odus_model->update_where(array("enlace" => $id),$info_equipos);

			$this->load->model('Logs_enlaces_model');
			$this->Logs_enlaces_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$evento["sitio"] = $this->Enlaces_model->get_sitios_enlace($id);
			$evento["elemento"] = $this->Enlaces_model->edit($id);
			$evento["link"] = "ver/sitios/infraestructura/enlaces/".$evento["sitio"][0]["id"];
			$evento["comentario"] = $info["comentario"];
			$cc = $this->Notificaciones_model->get_correos_grupos(5, $evento["sitio"][0]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$evento["sitio"]["nombre_completo"] = $evento["sitio"][0]["nombre_completo"]." <==> ".$evento["sitio"][1]["nombre_completo"];
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	Private static function cmpSitioCodigo($a,$b)
	{
		if( $a["idu_2"]["sitio"]["codigo"] == $b["idu_2"]["sitio"]["codigo"] )
		{
			return 0;
		}
		else
		{
			return ( $a["idu_2"]["sitio"]["codigo"] < $b["idu_2"]["sitio"]["codigo"] )? -1 : 1 ;
		}
	}

    public function grabar_enlace_antena_microondas($info_enlace,$info_equipos){
        $this->load->model('Enlaces_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
			
    $respuesta = $this->Enlaces_model->insert($info_enlace);	
		if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Creación de Enlace Microondas",
					"tabla"		=> "tb_enlaces"
				);

			$creacion_enlace = $evento["accion"];
			$this->load->model('Logs_enlaces_model');
			$this->Logs_enlaces_model->ingreso_de_datos($session['id_usuario'],$evento);
			
			if(count($info_equipos["odus_enlaces"]) > 0)
			{
				$this->load->model('Odus_model');
				foreach($info_equipos["odus_enlaces"] as $odu)
				{
					$odu["tipo_odu"] = $odu["tipo_odu"]["id"];
					$odu["enlace"] = $respuesta["last_id"];
					if($this->Odus_model->insert($odu))
					{
						$evento = array(
							"accion"	=> "Creación de ODU",
							"tabla"		=> "tb_odus_enlaces"
						);
						
						$this->load->model('Logs_odus_model');
						$this->Logs_odus_model->ingreso_de_datos($session['id_usuario'],$evento);
					}
				}
			}
		

		if(count($info_equipos["antenas_mw_enlaces"]) > 0)
			{
				$this->load->model('Antenas_mw_model');
				foreach($info_equipos["antenas_mw_enlaces"] as $antena)
				{
					$antena["tipo_antena_mw"] = $antena["tipo_antena_mw"]["id"];
					$antena["enlace"] = $respuesta["last_id"];
					$antena["estado_aprobacion"] = 1;
					$antena["id_usuario"] = $session['id_usuario'];
					//0 antiguos registros microndas
					//1 pendiente microondas
					//2 reservado microondas
					//3 reservado microondas
					if($this->Antenas_mw_model->insert($antena))
					{
						$evento = array(
							"accion"	=> "Creación de Antena Microondas",
							"tabla"		=> "tb_antenas_mw_enlaces"
						);
						
						$this->load->model('Logs_antenas_mw_model');
						$this->Logs_antenas_mw_model->ingreso_de_datos($session['id_usuario'],$evento);
					}
				}
			}
			/* Enviar correo */
			$evento["accion"] = $creacion_enlace;
			$evento["sitio"] = $this->Enlaces_model->get_sitios_enlace($respuesta["last_id"]);
			$evento["elemento"] = $this->Enlaces_model->edit($respuesta["last_id"]);
			$evento["link"] = "ver/sitios/infraestructura/enlaces/".$evento["sitio"][0]["id"];
			$cc = $this->Notificaciones_model->get_correos_grupos(5, $evento["sitio"][0]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$evento["sitio"]["nombre_completo"] = $evento["sitio"][0]["nombre_completo"]." <==> ".$evento["sitio"][1]["nombre_completo"];
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			/* Fin envio de correo */
		}


    }




  public function azimuth_giro($azimuth_reg,$azimuth_env)	
	{
    //registrado data 		
 	$azimuth_i = $azimuth_reg - 5;
 	$azimuth_f = $azimuth_reg + 5;
     
 	$cont = 0;
 	$mensaje ='';
    $mensajeh ='';
 	if($azimuth_i>0){
			 		// si es positivo
			       if(($azimuth_env<$azimuth_i)||($azimuth_env>$azimuth_f)){
			       	  $mensaje = "Puede instalar";
			       	
			       }else{
			       	$mensaje = "No puede instalar";
			       
			       	  $cont++;

			       }
 	}else{
 	  // si es negativo el azimuth registrado;         
       $azimuth_i = 0;    
	   if(($azimuth_env<$azimuth_i)||($azimuth_env>$azimuth_f)){	   		
		  $mensaje = "Puede Instalar";
	   }else{
		  $mensaje = "No puede instalar";
		  $cont++;
		} 
      
 	}
    return $cont;
 	}







}