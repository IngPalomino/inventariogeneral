<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antenas_mw extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 5;
	}

	public function listar()
	{
		$this->load->model('Antenas_mw_model');

		$listado = $this->Antenas_mw_model->get_antenas_microondas();

		echo json_encode($listado);
	}
     
	public function lista_log()
	{
		$this->load->model('Logs_antenas_mw_model');

		$listado = $this->Logs_antenas_mw_model->get_logs();

		echo json_encode($listado);
	}

	public function listar_sitio($id){
		$this->load->model('Antenas_mw_model');
		//$listado = $this->Antenas_mw_model->get_antenas_microondas_sitio($id);
		//md código mejorado
        $listado = $this->Antenas_mw_model->get_antenas_microondas_sitio_md($id);
		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Antenas_mw_model');

		$antena_microondas = $this->Antenas_mw_model->edit($id);

		echo json_encode($antena_microondas);
	}

	public function actualizar($id)
	{
		$this->load->model('Antenas_mw_model');
		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();
        $idsitio = $info["idsitio"];
        unset($info['idsitio']);
        //Verificando Espacio en Torre 
          //Microondas     
          $this->load->model('Antenas_mw_model');       
          $datosTorre = $this->Antenas_mw_model->get_antenas_microondas_sitio($idsitio);
          //Microondas Pendiente
          $datosTorreMP = $listado = $this->Antenas_mw_model->get_antenas_microondas_sitio_pendiente($idsitio);
          //GUL
		  $this->load->model('Antenas_gul_model');       
		  $datosTorreG = $this->Antenas_gul_model->get_antenas_gul_sitio($idsitio);
          //Gul Pendiente
          $this->load->model('Antenas_gul_pendiente_model');
          $datosTorreGULP = $this->Antenas_gul_pendiente_model->get_antenas_gul_pendiente_sitio($idsitio);

          //buscamos coincidencias
          //var_dump($datosTorre);
	     $acu = $acuG = $acuM = $acuMP = $ctnG =  $ctn = 0;
	      //altura y azimuth enviados por formulario 
	      $alturaEnviado = $info['altura'];	  	     
	      $azimuth_env = $info['azimuth'];
	         // $alturas = "";    
	          foreach ($datosTorre as $key => $torrevalue) {  
	            if($id != $torrevalue['id']){

	          if(($torrevalue['estado_aprobacion']=="1")||($torrevalue['estado_aprobacion']=="2")){
                    	//ANTENAS PENDIENTES
                    }else{
                    	 //$t1++;
                    	//aqui solamente las antenas instaladas
		                  //azimuth registrado        	
			              $azimuth_reg = $torrevalue['azimuth'];	             
		                  //altura registrada
			              $altura_llenado = $torrevalue['altura'];
		                  $alInicial = $altura_llenado - 0.5;
		                  $alFinal = $altura_llenado + 0.5;
		                  
			          	  if(($alInicial>$alturaEnviado) || ($alturaEnviado > $alFinal)){
			          	 	 // altura sin utilizar                 
			          	  }else{
			          	     //altura utilizada
		                   $ctn = $this->azimuth_giro($azimuth_reg,$azimuth_env);            
		                   $acu += $ctn;
			          	 }
                    }


	          	}
	           }

         $acuMP = $this->recorre_no_telnet($datosTorreMP,$alturaEnviado,$azimuth_env);
         $acuG = $this->recorre_no_telnet($datosTorreG,$alturaEnviado,$azimuth_env);
		 $acuGP = $this->recorre_no_telnet($datosTorreGULP,$alturaEnviado,$azimuth_env);
         $acu = $acu + $acuMP + $acuG + $acuGP;

		          if($acu >0){
		          	//si hay espacio reservados similares
		            echo json_encode($acu);
		          }else{
		          	//echo json_encode($ctn);
		          	$this->actualizar_antena_microondas($info,$id);
		          } 
      //Verificando Espacio en Torre
	}



	public function crear_enlace()
	{
        $this->load->library('session');
		$session = $this->session->userdata();
		$this->load->model('Antenas_mw_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = array();
		$info = $this->input->post();
		$idsitio = $info["idsitio"];



        unset($info['idsitio']);
        // se elimina idsitio porque no esta en la tabla micro
		//var_dump($info);
        $info["id_usuario"] = $session["id_usuario"];        
        //Verificando Espacio en Torre 
        //Antenas Microondas     
         $this->load->model('Antenas_mw_model');       
         $datosTorre = $this->Antenas_mw_model->get_antenas_microondas_sitio($idsitio);
         //Microondas Pendiente
         $datosTorreMP = $listado = $this->Antenas_mw_model->get_antenas_microondas_sitio_pendiente($idsitio);
        //GUL
		$this->load->model('Antenas_gul_model');       
		$datosTorreG = $this->Antenas_gul_model->get_antenas_gul_sitio($idsitio);
        //GulPendiente
		$this->load->model('Antenas_gul_pendiente_model');
        $datosTorreGULP = $this->Antenas_gul_pendiente_model->get_antenas_gul_pendiente_sitio($idsitio);
          //buscamos coincidencias               
	     $acuG = $acuGP = $acu = $acuMP = $ctnG =  $ctn = 0;
	     //altura y azimuth enviados por formulario 
			 $alturaEnviado = $info['altura'];	  	     
			 $azimuth_env = $info['azimuth'];
	         // $alturas = "";    
	         foreach ($datosTorre as $key => $torrevalue) {
               if(($torrevalue['estado_aprobacion']=="1")||($torrevalue['estado_aprobacion']=="2")){
                  //ANTENAS PENDIENTES
                 }else{                    	
                    	//aqui solamente las antenas instaladas
		                //azimuth registrado        	
			              $azimuth_reg = $torrevalue['azimuth'];	             
		                //altura registrada
			              $altura_llenado = $torrevalue['altura'];
		                  $alInicial = $altura_llenado - 0.5;
		                  $alFinal = $altura_llenado + 0.5;
			          	  if(($alInicial>$alturaEnviado) || ($alturaEnviado > $alFinal)){
			          	  // altura sin utilizar                 
			          	  }else{
			          	     //altura utilizada
		                   $ctn = $this->azimuth_giro($azimuth_reg,$azimuth_env);
		                   $acu += $ctn;
			          	 }
                    }      	
	           }

	              
		$acuMP = $this->recorre_no_telnet($datosTorreMP,$alturaEnviado,$azimuth_env);  
        $acuG = $this->recorre_no_telnet($datosTorreG,$alturaEnviado,$azimuth_env);
		$acuGP = $this->recorre_no_telnet($datosTorreGULP,$alturaEnviado,$azimuth_env);
         $acu = $acu + $acuMP + $acuG + $acuGP;
		       if($acu >0){
		          	//si hay espacio reservados similares
		           echo json_encode($acu);
		       }else{	
		           //echo json_encode($acu);	          	
		          $this->registar_antena_microondas($info);
		       } 
          //Verificando Espacio en Torre


	}
	

public function recorre_no_telnet($datosTorre,$alturaEnviado,$azimuth_env)
{
$acu = $ctn = 0; 
              foreach ($datosTorre as $key => $torrevalue) {            
                //gul registrado pendiente
            	//azimuth registrado        	
	             $azimuth_reg = $torrevalue['azimuth'];
	             $azInicial = $azimuth_reg - 5;
	             $azFinal = $azimuth_reg + 5;
                 //altura registrada
	             $altura_llenado = $torrevalue['altura'];
                 $alInicial = $altura_llenado - 0.5;
                 $alFinal = $altura_llenado + 0.5;

	          	 if(($alInicial>$alturaEnviado) || ($alturaEnviado > $alFinal)){
	          	 	 // altura sin utilizar                     
	      	     }else{
	          	    //altura utilizada
	      	     	$ctn = $this->azimuth_giro($azimuth_reg,$azimuth_env);	             
                    $acu += $ctn;
	          	 }
	           }
return $acu;
} 



	public function eliminar($id)
	{
		$this->load->model('Antenas_mw_model');

		$info = $this->input->input_stream();

		$info_antena['eliminado'] = 1;

		if($this->Antenas_mw_model->update($id, $info_antena))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Antena Microondas",
					"tabla"			=> "tb_antenas_mw_enlaces",
					"id"			=> $id
				);

			$this->load->model('Logs_antenas_mw_model');
			$this->Logs_antenas_mw_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$evento["sitio"] = $this->Antenas_mw_model->get_sitio_antena($id);
			$evento["elemento"] = $this->Antenas_mw_model->edit($id);
			$evento["link"] = "ver/sitios/infraestructura/enlaces/".$evento["sitio"][0]["id"];
			$evento["comentario"] = $info["comentario"];
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"][0]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$evento["sitio"]["nombre_completo"] = $evento["sitio"][0]["nombre_completo"]." <==> ".$evento["sitio"][1]["nombre_completo"];
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function almacenar($id)
	{
		$this->load->library('session');
		$this->load->model('Antenas_mw_model');

		$info = $this->input->input_stream();
		$session = $this->session->userdata();

		$info_antena['eliminado'] = 1;

		if(isset($session["logged_in"]) && $session["auth"] <= 2)
		{
			if($this->Antenas_mw_model->update($id, $info_antena))
			{
				$evento = array(
						"accion"		=> "Almacenamiento de Antena Microondas",
						"tabla"			=> "tb_antenas_mw_enlaces",
						"id"			=> $id
					);

				$this->load->model('Logs_antenas_mw_model');
				$this->Logs_antenas_mw_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

				$this->load->model('Bodega_antenas_mw_model');
				unset($info['comentario']);

				if($this->Bodega_antenas_mw_model->insert($info))
				{
					$evento = array(
						"accion"		=> "Almacenamiento de Antena Microondas",
						"tabla"			=> "tb_bodega_antenas_mw_enlaces"
					);

					$this->load->model('Logs_bodega_antenas_mw_model');
					$this->Logs_bodega_antenas_mw_model->ingreso_de_datos($session['id_usuario'],$evento);
				}
			}
		}
		else
		{
			show_404();
		}
	}

// version 2.3
	public function listar_sitio_pendiente($id){
		$this->load->model('Antenas_mw_model');
		$listado = $this->Antenas_mw_model->get_antenas_microondas_sitio_pendiente($id);
		echo json_encode($listado);
	}

    public function registar_antena_microondas($info){

    	$this->load->library('session');
		$session = $this->session->userdata();
		$this->load->model('Antenas_mw_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
    	$respuesta = $this->Antenas_mw_model->insert($info);
		if($respuesta["resp"])
		{
			$evento = array(
					"accion"	=> "Creación de Antena Microondas en un Enlace",
					"tabla"		=> "tb_antenas_mw_enlaces"
				);

			$this->load->model('Logs_antenas_mw_model');
			$this->Logs_antenas_mw_model->ingreso_de_datos($session['id_usuario'],$evento);

			$evento["sitio"] = $this->Antenas_mw_model->get_sitio_antena($respuesta["last_id"]);
			$evento["elemento"] = $this->Antenas_mw_model->edit($respuesta["last_id"]);
			$evento["link"] = "ver/sitios/infraestructura/enlaces/".$evento["sitio"][0]["id"];
		$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"][0]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$evento["sitio"]["nombre_completo"] = $evento["sitio"][0]["nombre_completo"]." <==> ".$evento["sitio"][1]["nombre_completo"];
			// $this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}	
    }


public function actualizar_antena_microondas($info,$id){
	    $this->load->model('Antenas_mw_model');
		$this->load->helper('form');
		$this->load->library('form_validation');

		$antena = $this->Antenas_mw_model->edit($id);
		if($this->Antenas_mw_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Antena Microondas",
					"tabla"			=> "tb_antenas_mw_enlaces",
					"id"			=> $id,
					"datos viejos"	=> $antena,
					"datos nuevos"	=> $this->Antenas_mw_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_antenas_mw_model');
				$this->Logs_antenas_mw_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$this->load->model('Logs_enlaces_model');
				$this->Logs_enlaces_model->actualizacion_de_datos($session['id_usuario'],$antena[0]["enlace"],$evento);

				$evento["sitio"] = $this->Antenas_mw_model->get_sitio_antena($id);
				$evento["link"] = "ver/sitios/infraestructura/enlaces/".$evento["sitio"][0]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"][0]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$evento["sitio"]["nombre_completo"] = $evento["sitio"][0]["nombre_completo"]." <==> ".$evento["sitio"][1]["nombre_completo"];
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
}

  public function azimuth_giro($azimuth_reg,$azimuth_env)	
	{
    //registrado data 		
 	$azimuth_i = $azimuth_reg - 5;
 	$azimuth_f = $azimuth_reg + 5;
     
 	$cont = 0;
 	$mensaje ='';
    $mensajeh ='';
 	if($azimuth_i>0){
			 		// si es positivo
			       if(($azimuth_env<$azimuth_i)||($azimuth_env>$azimuth_f)){
			       	  $mensaje = "Puede instalar";			       	
			       }else{
			       	$mensaje = "No puede instalar";			       
			       	  $cont++;
			       }
 	}else{
 	  // si es negativo el azimuth registrado;         
       $azimuth_i = 0;    
	   if(($azimuth_env<$azimuth_i)||($azimuth_env>$azimuth_f)){	   		
		  $mensaje = "Puede instalar";
	   }else{
		  $mensaje = "No puede instalar";
		  $cont++;
		}      
 	}
    return $cont;
 	}














      

}