<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inversores extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 7;
	}

	public function listar()
	{
		$this->load->model('Inversores_model');

		$listado = $this->Inversores_model->get_controlador_inversores();

		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		if($this->input->is_ajax_request())
		{
			$this->load->model('Inversores_model');

			$listado = $this->Inversores_model->get_controlador_inversor_sitio($id);

			echo json_encode($listado);
		}
		else
		{
			echo "Error";
		}
	}
	
	public function listar_sitio_mso($id)
	{
		if($this->input->is_ajax_request())
		{
			$this->load->model('Inversores_model');

			$listado = $this->Inversores_model->get_controlador_inversor_sitio_mso($id);

			echo json_encode($listado);
		}
		else
		{
			echo "Error";
		}
	}



	public function listar_eliminado_sitio($id)
	{
		$this->load->model('Inversores_model');

		$listado = $this->Inversores_model->get_controlador_inversor_eliminado_sitio($id);

		echo json_encode($listado);
	}

	public function editar($id)
	{
	$this->load->model('Inversores_model');

	$controlador_inversor = $this->Inversores_model->edit($id);

		echo json_encode($controlador_inversor);
	}

	public function actualizar($id)
	{
		$this->load->model('Inversores_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();
		
		$info["modulos_inversor"] = json_decode($info["modulos_inversor"]);
		
		$info_inversor["tipo_controlador_inversor"] = $info["tipo_controlador_inversor"];
		$info_inversor["anio_servicio"] = $info["anio_servicio"];
		$info_inversor["capacidad_maxima"] = $info["capacidad_maxima"];
		$info_inversor["energizacion_controlador_inversor"] = $info["energizacion_controlador_inversor"];
		$info_inversor["ubicacion_controlador_inversor"] = $info["ubicacion_controlador_inversor"];

        $info_inversor["numero_serie"] = $info["numero_serie"];
     
		$controlador_inversor = $this->Inversores_model->edit($id);

		if($this->Inversores_model->update($id, $info_inversor))
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			
			if(count($info["modulos_inversor"]) > 0)
			{
				$this->load->model('Modulos_inversor_model');
				foreach (($info["modulos_inversor"]) as $modulo_inversor)
				{
					$modulo_inversor->tipo_modulos_inversor = $modulo_inversor->tipo_modulos_inversor->id;
					if(isset($modulo_inversor->id))
					{
						if($this->Modulos_inversor_model->update($modulo_inversor->id,$modulo_inversor))
						{
							$evento = array(
								"accion"	=> "Actualización de Modulo Inversor",
								"tabla"		=> "tb_modulos_inversor",
								"id"		=> $modulo_inversor->id
							);

							$this->load->model('Logs_modulos_inversor_model');
							$this->Logs_modulos_inversor_model->actualizacion_de_datos($session['id_usuario'],$modulo_inversor->id,$evento);
						}
					}
					else
					{
						if($this->Modulos_inversor_model->insert($modulo_inversor))
						{
							$evento = array(
								"accion"	=> "Creación de Modulo Inversor",
								"tabla"		=> "tb_modulos_inversor"
							);

							$this->load->model('Logs_modulos_inversor_model');
							$this->Logs_modulos_inversor_model->ingreso_de_datos($session['id_usuario'],$evento);
							
						}
					}
				}
			}

			$evento = array(
					"accion"		=> "Actualización de Planta Inversor",
					"tabla"			=> "tb_controlador_inversor",
					"id"			=> $id,
					"datos viejos"	=> $controlador_inversor,
					"datos nuevos"	=> $this->Inversores_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_controlador_rectif_model');
				$this->Logs_controlador_inversor_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$evento["sitio"] = $this->Inversores_model->get_sitio_inversor($id);
				$evento["link"] = "ver/salastecnicas/mso/energia/inversores/".$evento["sitio"]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('Inversores_model');
		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["sitio"] = $id;
		$info["modulos_inversor"] = json_decode($info["modulos_inversor"]);
		
		$info_inversor["sitio"] = $id;
		$info_inversor["tipo_controlador_inversor"] = $info["tipo_controlador_inversor"];
		$info_inversor["anio_servicio"] = $info["anio_servicio"];
	   $info_inversor["capacidad_maxima"] = $info["capacidad_maxima"];
		$info_inversor["energizacion_controlador_inversor"] = $info["energizacion_controlador_inversor"];
		$info_inversor["ubicacion_controlador_inversor"] = $info["ubicacion_controlador_inversor"];
		$info_inversor["numero_serie"] = $info["partnumber"];
		

		if(isset($info["id_sala_tecnica"])){
         $info_rectificador["id_sala_tecnica"] = $info["id_sala_tecnica"];
        }
        


		$respuesta = $this->Inversores_model->insert($info_inversor);

		if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			
			$info_inversor["id"] = $respuesta["last_id"];

			//$this->Rectificadores_model->insert_ss_tecnologias($info_rectificador);
			
			if(count($info["modulos_inversor"]) > 0)
			{
				$this->load->model('Modulos_inversor_model');
				foreach (($info["modulos_inversor"]) as $modulo_inversor)
				{
					$modulo_inversor->tipo_modulos_inversor = $modulo_inversor->tipo_modulos_inversor->id;
					$modulo_inversor->controlador_inversor = $respuesta["last_id"];
					if($this->Modulos_inversor_model->insert($modulo_inversor))
					{
						$evento = array(
							"accion"	=> "Creación de Modulo Inversor",
							"tabla"		=> "tb_modulos_inversor"
						);

						$this->load->model('Logs_modulos_inversor_model');
						$this->Logs_modulos_inversor_model->ingreso_de_datos($session['id_usuario'],$evento);
					}
				}
			}

			$evento = array(
					"accion"	=> "Creación de Planta Inversor",
					"tabla"		=> "tb_controlador_inversor",
					"id"		=> $respuesta["last_id"]
				);

			$this->load->model('Logs_controlador_inversor_model');
			$this->Logs_controlador_inversor_model->ingreso_de_datos($session['id_usuario'],$evento);

			$this->load->model('Status_site_model');
			$evento["link"] = "ver/salastecnicas/mso/energia/inversores/".$id;
			$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($id);
			$evento["elemento"] = $this->Inversores_model->edit($respuesta["last_id"]);
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $id);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);

			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function eliminar($id)
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if( isset($session["logged_in"]) && $session["logged_in"] && ($session["auth"] <= 2) )
		{
			if($this->input->is_ajax_request())
			{
				$this->load->model('Inversores_model');

				$info = $this->input->input_stream();

				$info_rectif['eliminado'] = 1;

				if($this->Inversores_model->update($id, $info_rectif))
				{
					$this->Inversores_model->update_ss_tecnologias($id,1);

					$this->load->model('Bancos_baterias_model');
					$where = array();
					$where["controlador_inversor"] = $id;
					$info_banco = array();
					$info_banco["eliminado"] = 1;
					$this->Bancos_baterias_model->update_where($where, $info_banco);

					$evento = array(
							"accion"		=> "Eliminación de Planta Inversores",
						    "tabla"			=> "tb_controlador_inversor",
							"id"			=> $id
						);

					$this->load->model('Logs_controlador_inversor_model');
					$this->Logs_controlador_inversor_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

					$evento["sitio"] = $this->Inversores_model->get_sitio_inversor($id);
					$evento["link"] = "ver/salastecnicas/mso/energia/inversores/".$evento["sitio"]["id"];
					$evento["elemento"] = $this->Inversores_model->edit($id);
					$evento["comentario"] = $info["comentario"];
					$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
					$cc = $this->custom->clean_cc($session["usuario"], $cc);
                  //echo json_encode($evento);
                   // var_dump($evento);
					$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
				}
			}
			else
			{
				echo json_encode(array("error" => "error"));
			}
		}
		else
		{
			return false;
		}
	}
	
	public function restaurar($id)
	{
		$this->load->model('Inversores_model');

		$info = $this->input->input_stream();

		$info_inversor['eliminado'] = 0;

		if($this->Inversores_model->update($id, $info_inversor))
		{
			$this->Inversores_model->update_ss_tecnologias($id,0);

			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Restauración de Inversor eliminado",
					"tabla"			=> "tb_controlador_inversor",
					"id"			=> $id
				);

			$this->load->model('Logs_controlador_inversor_model');
			$this->Logs_controlador_inversor_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
		}
	}
	
	public function eliminar_definitivo($id)
	{
		$this->load->library('session');
		$session = $this->session->userdata();
		
		if( isset($session["logged_in"]) && $session["logged_in"] && ($session["auth"] <= 1) )
		{
			$this->load->model('Inversores_model');

			$info = $this->input->input_stream();

			$info['id'] = $id;

			if($this->Inversores_model->delete($id))
			{
				

				$evento = array(
						"accion"		=> "Eliminación definitiva de Inversor",
						"tabla"			=> "tb_controlador_inversor",
						"id"			=> $id
					);

				$this->load->model('Logs_controlador_inversor_model');
				$this->Logs_controlador_inversor_model->eliminacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);
			}
		}
		else
		{
			echo "No tiene permiso para acceder esta página";
			return FALSE;
		}
	}
}