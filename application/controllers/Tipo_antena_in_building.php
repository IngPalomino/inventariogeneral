<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_antena_in_building extends CI_Controller {

	public function listar()
	{
		$this->load->model('Tipo_antena_in_building_model');

		$listado = $this->Tipo_antena_in_building_model->get_types();

		echo json_encode($listado);
	}

	public function id_tipo()
	{
		$this->load->model('Tipo_antena_in_building_model');

		$info = array();
		$info = $this->input->post();

		$tipo = $this->Tipo_antena_in_building_model->get_type($info);

		echo json_encode($tipo);
	}

	public function editar($id)
	{
		$this->load->model('Tipo_antena_in_building_model');

		$antena = $this->Tipo_antena_in_building_model->edit($id);

		echo json_encode($antena);
	}

	public function actualizar($id)
	{
		$this->load->model('Tipo_antena_in_building_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$this->Tipo_antena_in_building_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Tipo_antena_in_building_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Tipo_antena_in_building_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Tipo_antena_in_building_model');

		$this->Tipo_antena_in_building_model->delete($id);
	}
	
	public function listar_tipo_antena()
	{
		$this->load->model('Tipo_antena_in_building_model');
		$listado = $this->Tipo_antena_in_building_model->get_tipo_antena();
		echo json_encode($listado);
	}
}