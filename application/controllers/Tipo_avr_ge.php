<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_avr_ge extends CI_Controller {

	public function listar()
	{
		$this->load->model('Tipo_avr_ge_model');

		$listado = $this->Tipo_avr_ge_model->get_types();

		echo json_encode($listado);
	}

	public function id_tipo()
	{
		$this->load->model('Tipo_avr_ge_model');

		$info = array();
		$info = $this->input->post();

		$tipo = $this->Tipo_avr_ge_model->get_type($info);

		echo json_encode($tipo);
	}

	public function editar($id)
	{
		$this->load->model('Tipo_avr_ge_model');

		$avr = $this->Tipo_avr_ge_model->edit($id);

		echo json_encode($avr);
	}

	public function actualizar($id)
	{
		$this->load->model('Tipo_avr_ge_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$this->Tipo_avr_ge_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Tipo_avr_ge_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Tipo_avr_ge_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Tipo_avr_ge_model');

		$this->Tipo_avr_ge_model->delete($id);
	}
}