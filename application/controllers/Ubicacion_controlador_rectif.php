<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ubicacion_controlador_rectif extends CI_Controller {

	public function listar()
	{
		$this->load->model('Ubicacion_controlador_rectif_model');

		$listado = $this->Ubicacion_controlador_rectif_model->get_types();

		echo json_encode($listado);
	}

	public function id_tipo()
	{
		$this->load->model('Ubicacion_controlador_rectif_model');

		$info = array();
		$info = $this->input->post();

		$tipo = $this->Ubicacion_controlador_rectif_model->get_type($info);

		echo json_encode($tipo);
	}

	public function editar($id)
	{
		$this->load->model('Ubicacion_controlador_rectif_model');

		$ubicacion = $this->Ubicacion_controlador_rectif_model->edit($id);

		echo json_encode($ubicacion);
	}

	public function actualizar($id)
	{
		$this->load->model('Ubicacion_controlador_rectif_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$this->Ubicacion_controlador_rectif_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Ubicacion_controlador_rectif_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Ubicacion_controlador_rectif_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Ubicacion_controlador_rectif_model');

		$this->Ubicacion_controlador_rectif_model->delete($id);
	}
}