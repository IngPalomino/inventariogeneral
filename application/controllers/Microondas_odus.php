<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Microondas_odus extends CI_Controller {
	
	public function listar()
	{
		$this->load->model('Microondas_odus_model');
		$listado = $this->Microondas_odus_model->get_odus();
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Microondas_odus_model');
		$listado = $this->Microondas_odus_model->get_odus_sitio($id);
		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('Microondas_odus_model');
		$odu = $this->Microondas_odus_model->edit($id);
		echo json_encode($odu);
	}

	public function actualizar($id)
	{
		$this->load->model('Microondas_odus_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$odu = $this->Microondas_odus_model->edit($id);

		if($this->Microondas_odus_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Odu",
					"tabla"			=> "tb_e_tarjetas",
					"id"			=> $id,
					"datos viejos"	=> $odu,
					"datos nuevos"	=> $this->Microondas_odus_model->edit($id)
				);

			$this->load->model('Logs_aires_acondicionados_model');
			$this->Logs_aires_acondicionados_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('Microondas_odus_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["data"] = json_decode($info["data"],true);

		if(count($info["data"]) > 0)
		{
			foreach($info["data"] as $odu)
			{
				$produce_time = $this->validateDate($odu["produce_time"])? $odu["produce_time"] : $odu["produce_time"]."-01";
				$odu["sitio"] = $id;
				$odu["produce_time"] = $produce_time;
				$this->Microondas_odus_model->insert($odu);
			}
		}
	}

	public function eliminar()
	{
		$this->load->model('Microondas_odus_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = array();
		$info = $this->input->input_stream();
		
		$info["data"] = json_decode($info["data"],true);
		
		foreach($info["data"] as $odu)
		{
			$this->Microondas_odus_model->delete($odu);
		}
	}
	
	function validateDate($date)
	{
		$d = DateTime::createFromFormat('Y-m-d', $date);
		return $d && $d->format('Y-m-d') === $date;
	}
}