<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_router extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tipo_router_model');
		$this->load->library('auth');
	}

	public function crear()
	{
		if ( ! $this->auth->is_admin() || ! isset($_POST) || empty($_POST))
		{
			show_404();
			return FALSE;
		}

		$this->Tipo_router_model->insert($this->input->post());
	}

	public function listar()
	{
		echo json_encode($this->Tipo_router_model->get_types());
	}

	public function actualizar($id)
	{
		if ( ! $this->auth->is_admin() || ! isset($_POST) || empty($_POST))
		{
			show_404();
			return FALSE;
		}

		$this->Tipo_router_model->update($id, $this->input->input_stream());
	}

	public function eliminar($id)
	{
		if ( ! $this->auth->is_admin())
		{
			show_404();
			return FALSE;
		}

		$this->Tipo_router_model->delete($id);
	}
}