<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_masivo_bancos_baterias extends CI_Controller {

	protected $modulo;

	public function __construct()
	{
		parent::__construct();
		$this->modulo = 8;
		$this->load->model('Ingreso_masivo_bancos_baterias_model');
		$this->load->library('auth');
	}

	public function plantilla_bancos_baterias()
	{
		$this->load->library('excel');
		$this->excel->getProperties()->setCreator('OyM')
									->setLastModifiedBy('OyM')
									->setTitle('Plantilla Bancos de Baterías')
									->setSubject('Plantilla')
									->setDescription('Plantilla para ingresar información de Bancos de Baterías al Inventario General.')
									->setKeywords('office 2007 openxml php')
									->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Bancos de Baterías');

		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Marca')
					->setCellValue('D1', 'Modelo')
					->setCellValue('E1', 'Fecha de Instalación')
					->setCellValue('F1', 'Números de Serie')
					->setCellValue('G1', 'Energización');

		$styleArray = array(
				'font' => array(
						'bold'	=> TRUE,
						'color'	=> array('rgb' => 'FFFFFF'),
						'size'	=> 11,
						'name'	=> 'Verdana'
					),
				'fill' => array(
						'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
						'color'	=> array('rgb' => '6788D3')
					),
				'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle('A1:G1')->applyFromArray($styleArray);

		$activeSheet->getStyle('A:A')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$activeSheet->getColumnDimension('A')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('B')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('C')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('D')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('E')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('F')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('G')->setAutoSize(TRUE);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Bancos_Baterias_ingreso_nuevo.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function plantilla_bancos_baterias_actualizacion()
	{
		$listado = $this->Ingreso_masivo_bancos_baterias_model->get_bancos();
		$this->load->library("excel");
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla Bancos de Baterías")
									->setSubject("Plantilla")
									->setDescription("Plantilla para ingresar información de Bancos de Baterías al Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle("Bancos de Baterías");

		$activeSheet->setCellValue("A1", "Código")
					->setCellValue("B1", "Nombre")
					->setCellValue("C1", "ID del Banco")
					->setCellValue("D1", "Marca")
					->setCellValue("E1", "Modelo")
					->setCellValue("F1", "Fecha de Instalación")
					->setCellValue("G1", "Números de Serie")
					->setCellValue("H1", "Energización");

		$styleArray = array(
				"font" => array(
						"bold"	=> TRUE,
						"color"	=> array("rgb" => "FFFFFF"),
						"size"	=> 11,
						"name"	=> "Verdana"
					),
				"fill" => array(
						"type"	=> PHPExcel_Style_Fill::FILL_SOLID,
						"color"	=> array("rgb" => "6788D3")
					),
				"alignment" => array(
						"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle("A1:H1")->applyFromArray($styleArray);

		$activeSheet->getStyle('A:A')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$i = 2;

		foreach ($listado as $fila)
		{
			$activeSheet->setCellValue("A".$i, $fila["codigo"])
						->setCellValue("B".$i, $fila["nombre_completo"])
						->setCellValue("C".$i, $fila["id"])
						->setCellValue("D".$i, $fila["marca"])
						->setCellValue("E".$i, $fila["modelo"])
						->setCellValue("H".$i, $fila["energizacion_controlador_rectif"]);

			$i++;
		}

		$activeSheet->getColumnDimension("A")->setAutoSize(TRUE);
		$activeSheet->getColumnDimension("B")->setAutoSize(TRUE);
		$activeSheet->getColumnDimension("C")->setAutoSize(TRUE);
		$activeSheet->getColumnDimension("D")->setAutoSize(TRUE);
		$activeSheet->getColumnDimension("E")->setAutoSize(TRUE);
		$activeSheet->getColumnDimension("F")->setAutoSize(TRUE);
		$activeSheet->getColumnDimension("G")->setAutoSize(TRUE);
		$activeSheet->getColumnDimension("H")->setAutoSize(TRUE);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Actualizacion_Bancos_Baterias.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function plantilla_tipo_bancos_baterias()
	{
		$this->load->model('Tipo_bancos_baterias_model');
		$tipo = $this->Tipo_bancos_baterias_model->get_types();

		$this->load->library("excel");
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla Bancos de Baterías")
									->setSubject("Plantilla")
									->setDescription("Plantilla para ingresar información de Bancos de Baterías al Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle("Tipo Bancos de Baterías");

		$activeSheet->setCellValue("A1", "Marca")
					->setCellValue("B1", "Modelo")
					->setCellValue("C1", "Capacidad (Ah)")
					->setCellValue("D1", "Voltaje")
					->setCellValue("E1", "Cantidad de Celdas");

		$styleArray = array(
				"font" => array(
						"bold"	=> TRUE,
						"color"	=> array("rgb" => "FFFFFF"),
						"size"	=> 11,
						"name"	=> "Verdana"
					),
				"fill" => array(
						"type"	=> PHPExcel_Style_Fill::FILL_SOLID,
						"color"	=> array("rgb" => "6788D3")
					),
				"alignment" => array(
						"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle("A1:E1")->applyFromArray($styleArray);

		$i = 2;

		foreach ($tipo["modelos"] as $fila)
		{
			$activeSheet->setCellValue("A".$i, $fila["marca"])
						->setCellValue("B".$i, $fila["modelo"])
						->setCellValue("C".$i, $fila["capacidad"])
						->setCellValue("D".$i, $fila["voltaje"])
						->setCellValue("E".$i, $fila["celdas"]);

			$i++;
		}

		$activeSheet->getColumnDimension("A")->setAutoSize(TRUE);
		$activeSheet->getColumnDimension("B")->setAutoSize(TRUE);
		$activeSheet->getColumnDimension("C")->setAutoSize(TRUE);
		$activeSheet->getColumnDimension("D")->setAutoSize(TRUE);
		$activeSheet->getColumnDimension("E")->setAutoSize(TRUE);
		
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Tipo_Bancos_Baterias.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function upload_file()
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->allowed_module($this->modulo))
			{
				$post = $this->input->post();
				$config["upload_path"] = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo";
				$config["allowed_types"] = "xlsx";
				$config["max_size"] = "1024";
				$config["overwrite"] = FALSE;
				$config["file_name"] = "plantilla_".$post["tipo"]."_".date("d-m-Y-H-i-s");

				$this->load->library("upload", $config);

				if ($this->upload->do_upload("archivo"))
				{
					echo json_encode(array(
							"error"		=> FALSE,
							"mensaje"	=> "Archivo subido con éxito.",
							"resultado"	=> $this->upload->data("file_name")
						));
				}
				else
				{
					echo json_encode(array(
							"error"		=> TRUE,
							"mensaje"	=> "No se pudo subir el archivo.",
							"resultado"	=> $this->upload->display_errors()
						));
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción.",
						"resultado"	=> "forbidden"
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró",
					"resultado"	=> "logged_off"
				));
		}
	}

	public function check_file()
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->allowed_module($this->modulo))
			{
				$post = $this->input->post();
				$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"];
				
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						"error"			=> FALSE,
						"advertencia"	=> FALSE,
						"mensajes"		=> array(),
						"advertencias"	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				if(in_array("Bancos de Baterías", $sheetNames))
				{
					try{ $objPHPExcel->setActiveSheetIndexByName("Bancos de Baterías"); } catch(Exception $e){ continue; }

					$activeSheet = $objPHPExcel->getActiveSheet();
					$cell_collection = $activeSheet->getCellCollection();
					$cabeceras = array("Código", "Nombre", "Marca", "Modelo", "Fecha de Instalación", "Números de Serie", "Energización");
					$cabeceras_no_validas = array();
					$cabeceras_validas = array();
					$arr_data = array();

					foreach($cell_collection as $cell)
					{
						$column = $activeSheet->getCell($cell)->getColumn();
						$row = $activeSheet->getCell($cell)->getRow();
						$data_value = $activeSheet->getCell($cell)->getValue();

						if($row == 1)
						{
							$header[$row][$column] = $data_value;

							foreach($header as $filaHeader => $infoHeader)
							{
								if ( ! in_array($infoHeader[$column], $cabeceras))
								{
									array_push($cabeceras_no_validas, $infoHeader[$column]);
								}
								else
								{
									array_push($cabeceras_validas, $infoHeader[$column]);
								}
							}
						}
						else
						{
							if ( ! isset($data_value) && $column == "A")
							{
								/*break;*/
								$resultados["advertencia"] = TRUE;
								array_push($resultados["advertencias"], "La columna $column en la fila $row no tiene información, se obviará esta fila.");
							}
							elseif (in_array($header[1][$column], $cabeceras_no_validas))
							{
								if ($header[1][$column] == "ID del Banco")
								{
									$arr_data[$row][$column] = $data_value;
								}
								else
								{

								}
							}
							else
							{
								$arr_data[$row][$column] = $data_value;
							}
						}
					}

					foreach($cabeceras as &$value)
					{
						if(!in_array($value, $cabeceras_validas))
						{
							$resultados["error"] = TRUE;
							array_push($resultados["mensajes"], "La columna $value es obligatoria.");
						}
					}

					if(count($cabeceras_no_validas) > 0)
					{
						$cabeceras_no_validas = array_values(array_unique($cabeceras_no_validas));

						foreach($cabeceras_no_validas as $cabecera)
						{
							if ($cabecera == "ID del Banco")
							{
								$resultados["advertencia"] = TRUE;
								array_push($resultados["advertencias"], "La cabecera $cabecera está presente en la plantilla, se actualizarán todos los elementos según el ID especificado.");
							}
							else
							{
								$resultados["advertencia"] = TRUE;
								array_push($resultados["advertencias"], "La cabecera $cabecera no corresponde a la plantilla original, se obviará esta columna.");
							}
						}
					}

					/*echo json_encode($header);*/

					$filas = count($arr_data);

					if(count($arr_data) > 0)
					{
						foreach($arr_data as $fila => $infoFila)
						{
							$nombreSeparado = explode("_", $infoFila["B"]);
							$codigoNombre = $nombreSeparado[0];

							foreach($infoFila as $columna => $campo)
							{
								if($header[1][$columna] == "Código")
								{
									$codigo = $campo;
									if(strlen($campo) != 7)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código de sitio en la fila $fila no es válido, debe tener 7 dígitos.");
									}
									elseif( ! is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código de sitio en la fila $fila debe contener únicamente números.");
									}
									else
									{
										if($codigoNombre != $campo)
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "El Código de sitio en la fila $fila no coincide con el código del Nombre.");
										}
									}
								}
								elseif($header[1][$columna] == "Nombre")
								{
									$nombre = $this->Ingreso_masivo_bancos_baterias_model->nombre_codigo_validos($codigo, $campo);
									if ( ! isset($campo) || ! isset($columna) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Nombre en la fila $fila.");
									}
									elseif ( ! $this->Ingreso_masivo_bancos_baterias_model->nombre_completo_valido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre de sitio en la fila $fila no existe en la base de datos.");
									}
									elseif ( ! $nombre)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre y Código de sitio en la fila $fila no existe en la base de datos.");
									}
									elseif ( ! $this->auth->allowed_module($this->modulo, $nombre["id"]))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Usted no tiene permiso para ingresar/actualizar información en el sitio $campo de la fila $fila.");
									}
								}
								elseif ($header[1][$columna] == "ID del Banco")
								{
									if ( ! isset($columna) || ! isset($campo) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "No debe borrar el ID del Banco en la fila $fila.");
									}
									elseif ( ! $this->Ingreso_masivo_bancos_baterias_model->existe_banco($campo, $nombre["id"]))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El ID del Banco en la fila $fila no corresponde al sitio especificado.");
									}
								}
								elseif($header[1][$columna] == "Marca")
								{
									$marca = $campo;
									if ( ! isset($campo) || ! isset($columna) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Marca en la fila $fila.");
									}
									elseif ( ! $this->Ingreso_masivo_bancos_baterias_model->marca_valida($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Marca en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Modelo")
								{
									$tipo_banco = $this->Ingreso_masivo_bancos_baterias_model->tipo_banco_baterias_valido($marca, $campo);
									if ( ! isset($campo) || ! isset($columna) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Modelo en la fila $fila.");
									}
									elseif ( ! $this->Ingreso_masivo_bancos_baterias_model->modelo_valido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Controlador en la fila $fila no existe en la base de datos.");
									}
									elseif ($tipo_banco === FALSE)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Tipo de Banco (Marca y Modelo) en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Fecha de Instalación")
								{
									if ( ! isset($campo) || ! isset($columna) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Fecha de Instalación en la fila $fila.");
									}
									elseif ( ! $this->Ingreso_masivo_bancos_baterias_model->validate_date($campo, "d/m/Y"))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Fecha de Instalación en la fila $fila no es una fecha válida.");
									}
								}
								elseif($header[1][$columna] == "Números de Serie")
								{
									if ( ! isset($campo) || ! isset($columna) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar los Números de Serie en la fila $fila.");
									}
									elseif ($tipo_banco !== FALSE)
									{
										if ( ! preg_match("/[,\/\\\]+/", $campo))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "Los Números de Serie en la fila $fila deben estar separados por comas.");
										}
										else
										{
											$numeros_serie = preg_split("/[\s,\/\\\]+/", $campo);

											if (count($numeros_serie) != $tipo_banco["celdas"])
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "La cantidad de Números de Serie en la fila $fila debe ser igual a la cantidad de Celdas (".$tipo_banco["celdas"].") de la Marca ".$tipo_banco["marca"].".");
											}
										}
									}
								}
								elseif($header[1][$columna] == "Energización")
								{
									$energizacion = $this->Ingreso_masivo_bancos_baterias_model->energizacion_valida($campo);
									if ( ! isset($campo) || ! isset($columna) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Energización en la fila $fila.");
									}
									elseif( ! $energizacion)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Energización en la fila $fila no existe en la base de datos.");
									}
									elseif ($this->Ingreso_masivo_bancos_baterias_model->rectificador_valido($nombre["id"], $energizacion["id"]) === FALSE)
									{
										$resultados['error'] = TRUE;
										array_push($resultados['mensajes'], 'El sitio en la fila '.$fila.' no tiene una Planta Rectificadora en '.$campo.'.');
									}
								}
							}
						}
					}
					else
					{
						$resultados["error"] = TRUE;
						array_push($resultados["mensajes"], "La hoja no contiene información");
					}
				}
				else
				{
					$resultados["error"] = TRUE;
					array_push($resultados["mensajes"], "No se encontró la hoja llamada 'Bancos de Baterías'");
				}

				if( ! $resultados["error"])
				{
					array_push($resultados["mensajes"], "Filas válidas: ".count($arr_data));
				}
				else
				{
					unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
				}

				echo json_encode($resultados);
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción."
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró."
				));
		}
	}

	public function insert_temp()
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->allowed_module($this->modulo))
			{
				$idIngresoMasivo = $this->Ingreso_masivo_bancos_baterias_model->registrar_ingreso_masivo($this->session->userdata("id_usuario"));
				$post = $this->input->post();
				$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"];
				
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						"error"			=> FALSE,
						"advertencia"	=> FALSE,
						"mensajes"		=> array(),
						"advertencias"	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				$resultado = 0;

				if(in_array("Bancos de Baterías", $sheetNames))
				{
					try{ $objPHPExcel->setActiveSheetIndexByName("Bancos de Baterías"); } catch(Exception $e){ continue; }

					$activeSheet = $objPHPExcel->getActiveSheet();
					$cell_collection = $activeSheet->getCellCollection();
					$cabeceras = array("Código", "Nombre", "Marca", "Modelo", "Fecha de Instalación", "Números de Serie", "Energización");
					$cabeceras_no_validas = array();
					$cabeceras_validas = array();
					$arr_data = array();

					foreach($cell_collection as $cell)
					{
						$column = $activeSheet->getCell($cell)->getColumn();
						$row = $activeSheet->getCell($cell)->getRow();
						$data_value = $activeSheet->getCell($cell)->getValue();

						if($row == 1)
						{
							$header[$row][$column] = $data_value;

							foreach($header as $filaHeader => $infoHeader)
							{
								if ( ! in_array($infoHeader[$column], $cabeceras))
								{
									array_push($cabeceras_no_validas, $infoHeader[$column]);
								}
								else
								{
									array_push($cabeceras_validas, $infoHeader[$column]);
								}
							}
						}
						else
						{
							if ( ! isset($data_value) && $column == "A")
							{
								/*break;*/
							}
							elseif (in_array($header[1][$column], $cabeceras_no_validas))
							{
								if ($header[1][$column] == "ID del Banco")
								{
									$arr_data[$row][$column] = $data_value;
								}
								else
								{

								}
							}
							else
							{
								$arr_data[$row][$column] = $data_value;
							}
						}
					}

					foreach($cabeceras as &$value)
					{
						if(!in_array($value, $cabeceras_validas))
						{
							$resultados["error"] = TRUE;
							array_push($resultados["mensajes"], "La columna $value es obligatoria.");
						}
					}

					if(count($cabeceras_no_validas) > 0)
					{
						$cabeceras_no_validas = array_values(array_unique($cabeceras_no_validas));

						foreach($cabeceras_no_validas as $cabecera)
						{
							if ($cabecera == "ID del Banco")
							{
								$resultados["advertencia"] = TRUE;
								array_push($resultados["advertencias"], "La cabecera $cabecera está presente en la plantilla, se actualizarán todos los elementos según el ID especificado.");
							}
							else
							{
								$resultados["advertencia"] = TRUE;
								array_push($resultados["advertencias"], "La cabecera $cabecera no corresponde a la plantilla original, se obviará esta columna.");
							}
						}
					}

					/*echo json_encode($header);*/

					$filas = count($arr_data);

					if($idIngresoMasivo !== FALSE)
					{
						foreach($arr_data as $fila => $infoFila)
						{
							$nombreSeparado = explode("_", $infoFila["B"]);
							$codigoNombre = $nombreSeparado[0];

							foreach($infoFila as $columna => $campo)
							{
								$ingresoMasivo["ingreso_masivo"] = $idIngresoMasivo;

								if($header[1][$columna] == "Código")
								{
									$codigo = $campo;
								}
								elseif($header[1][$columna] == "Nombre")
								{
									$nombre = $this->Ingreso_masivo_bancos_baterias_model->nombre_codigo_validos($codigo, $campo);
								}
								elseif ($header[1][$columna] == "ID del Banco")
								{
									$ingresoMasivo["id_actualizar"] = $campo;
								}
								elseif($header[1][$columna] == "Marca")
								{
									$marca = $campo;
								}
								elseif($header[1][$columna] == "Modelo")
								{
									$tipo_banco = $this->Ingreso_masivo_bancos_baterias_model->tipo_banco_baterias_valido($marca, $campo);
									$ingresoMasivo["tipo_bancos_baterias"] = $tipo_banco["id"];
								}
								elseif($header[1][$columna] == "Fecha de Instalación")
								{
									$date = DateTime::createFromFormat("d/m/Y", $campo);
									$ingresoMasivo["fecha_instalacion"] = $date->format("Y-m-d");
								}
								elseif($header[1][$columna] == "Números de Serie")
								{
									$numeros_serie = preg_split("/[\s,\/\\\]+/", $campo);
								}
								elseif($header[1][$columna] == "Energización")
								{
									$energizacion = $this->Ingreso_masivo_bancos_baterias_model->energizacion_valida($campo);
									$controlador_rectif = $this->Ingreso_masivo_bancos_baterias_model->rectificador_valido($nombre["id"], $energizacion["id"]);
									$ingresoMasivo["controlador_rectif"] = $controlador_rectif["id"];
								}
							}

							$respuesta = $this->Ingreso_masivo_bancos_baterias_model->ingresar_temporal($ingresoMasivo);

							if ($respuesta["resp"])
							{
								if (count($numeros_serie) > 0)
								{
									foreach ($numeros_serie as $numero)
									{
										$celda["banco_baterias"] = $respuesta["last_id"];
										$celda["fecha_instalacion"] = $ingresoMasivo["fecha_instalacion"];
										$celda["numero_serie"] = $numero;

										$this->Ingreso_masivo_bancos_baterias_model->ingresar_temporal_celdas($celda);
									}
								}

								$resultado++;
							}
						}

						$resultados["mensajes"] = "Se ha enviado $resultado fila(s) para ser aprobada(s).";
						unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
					}
					else
					{
						$resultados["error"] = TRUE;
						array_push($resultados["mensajes"], "La hoja no contiene información");
					}
				}
				else
				{
					$resultados["error"] = TRUE;
					array_push($resultados["mensajes"], "No se encontró la hoja llamada 'Bancos de Baterías'");
				}

				if( ! $resultados["error"])
				{
					array_push($resultados["mensajes"], "Filas válidas: ".count($arr_data));
				}
				else
				{
					unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
				}

				echo json_encode($resultados);
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción."
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró."
				));
		}
	}

	public function listar_solicitudes($usuario)
	{
		if($this->auth->logged_in() && ($this->session->userdata("id_usuario") == $usuario))
		{
			echo json_encode($this->Ingreso_masivo_bancos_baterias_model->get_solicitudes($usuario));
		}
		else
		{
			show_404();
		}
	}
}