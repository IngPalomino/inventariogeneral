<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Almacenes extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Almacenes_model');
	}

	public function listar()
	{
		echo json_encode($this->Almacenes_model->get_types());
	}
	
	public function listar_grupos_electrogenos()
	{
		echo json_encode($this->Almacenes_model->get_grupos_electrogenos());
	}
	
	public function listar_grupos_electrogenos_sitio($id)
	{
		$this->load->model('Almacenes_model');
		$listado = $this->Almacenes_model->get_grupos_electrogenos_sitio($id);
		echo json_encode($listado);
	}
	
	public function listar_grupos_electrogenos_almacen($id)
	{
		$this->load->model('Almacenes_model');
		$listado = $this->Almacenes_model->get_grupos_electrogenos_almacen($id);
		echo json_encode($listado);
	}
	
	public function listar_sitios()
	{
		$this->load->model('Almacenes_model');
		$listado = $this->Almacenes_model->get_sites();
		echo json_encode($listado);
	}	
	
    public function listar_sitios_mso()
	{
		$this->load->model('Almacenes_model');
		$this->load->model('Mso_salas_tecnicas_model');
		$listado = $this->Almacenes_model->get_sites();
		$listado_mso_site = $this->Mso_salas_tecnicas_model->get_sala_tecnicas_especifica();
		 $listadoT = array_merge($listado,$listado_mso_site);
		echo json_encode($listadoT);
		//echo json_encode($listado);
	}


	public function reactivar($id)
	{
		$this->load->model('Grupos_electrogenos_model');

		$info = $this->input->input_stream();

		$info_grupo['en_funcionamiento'] = 1;
		$info_grupo['almacen'] = 0;
		$info_grupo['sitio'] = $info['sitio'];
		
		if($this->Grupos_electrogenos_model->update($id, $info_grupo))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Grupo Electrógeno puesto en funcionamiento",
					"tabla"			=> "tb_grupos_electrogenos",
					"id"			=> $id,
					"sitio_nuevo"	=> $info_grupo["sitio"]
				);

			$this->load->model('Logs_grupos_electrogenos_model');
			$this->Logs_grupos_electrogenos_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);
		}
	}
	
	public function mover_grupo_electrogeno($id)
	{
		$this->load->model('Grupos_electrogenos_model');

		$info = $this->input->input_stream();

		$info_grupo['almacen'] = $info['almacen'];
		$info_grupo['sitio'] = $info['sitio'];
		
		$this->Grupos_electrogenos_model->update($id, $info_grupo);
	}
	
	public function almacenar_grupo_electrogeno()
	{
		$this->load->model('Grupos_electrogenos_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		$info["en_funcionamiento"] = 0;

		if($this->Grupos_electrogenos_model->insert($info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Almacenamiento de Grupo Electrógeno",
					"tabla"		=> "tb_grupos_electrogenos"
				);

			$this->load->model('Logs_grupos_electrogenos_model');
			$this->Logs_grupos_electrogenos_model->ingreso_de_datos($session['id_usuario'],$evento);
		}
	}
	
	public function actualizar_grupo_electrogeno($id)
	{
		$this->load->model('Grupos_electrogenos_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$this->Grupos_electrogenos_model->update($id, $info);
	}
}