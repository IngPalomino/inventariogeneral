<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluaciones_criterio extends CI_Controller {
	public function listar()
	{
		$this->load->model('Evaluaciones_criterio_model');

		$listado = $this->Evaluaciones_criterio_model->listar();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Evaluaciones_criterio_model');

		$listado = $this->Evaluaciones_criterio_model->editar($id);

		echo json_encode($listado);
	}

	public function actualizar()
	{
		
	}

	public function insertar()
	{

	}

	public function borrar()
	{
		
	}
}

?>