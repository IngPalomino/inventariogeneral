<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluaciones_evaluacion extends CI_Controller {
	public function listar()
	{
		$this->load->model('Evaluaciones_evaluacion_model');

		$listado = $this->Evaluaciones_evaluacion_model->listar();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Evaluaciones_evaluacion_model');

		$listado = $this->Evaluaciones_evaluacion_model->editar($id);

		echo json_encode($listado);
	}

	public function actualizar($id)
	{
		$this->load->model('Evaluaciones_evaluacion_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$this->Evaluaciones_evaluacion_model->actualizar($id, $info);

		/*if($this->Evaluaciones_evaluacion_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Tanque de combustible",
					"tabla"			=> "tb_tanques_combustible",
					"id"			=> $id,
					"datos viejos"	=> $tanque_combustible,
					"datos nuevos"	=> $this->Evaluaciones_evaluacion_model->edit($id)
				);

			$this->load->model('Logs_tanques_combustible_model');
			$this->Logs_tanques_combustible_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
		}*/
	}

	public function insertar()
	{
		$this->load->model('Evaluaciones_evaluacion_model');

		$info = array();
		$info = $this->input->post();
		$info_evaluacion = array();
		$info_evaluacion["nombre"] = $info["nombre"];
		$info_evaluacion["periodo"] = $info["periodo"];
		$info_evaluacion["contrata"] = $info["contrata"];

		$respuesta = $this->Evaluaciones_evaluacion_model->insertar($info_evaluacion);

		$info['contenido'] = json_decode($info['contenido']);

		if($respuesta['resp'])
		{
			$this->load->model('Evaluaciones_criterio_model');

			foreach ($info['contenido'] as $criterio)
			{
				$info_criterio = array();
				$info_criterio['evaluacion'] = $respuesta['last_id'];
				$info_criterio['nombre'] = $criterio->criterio;
				$info_criterio['peso'] = $criterio->peso;

				$respCriterio = $this->Evaluaciones_criterio_model->insertar($info_criterio);

				if($respCriterio['resp'])
				{
					$this->load->model('Evaluaciones_sub_criterio_model');

					foreach ($criterio->preguntas as $pregunta)
					{
						$info_subcriterio = array();
						$info_subcriterio['criterio'] = $respCriterio['last_id'];
						$info_subcriterio['nombre'] = $pregunta->sub_criterio;
						$info_subcriterio['pregunta'] = $pregunta->pregunta;
						$info_subcriterio['nota_informativa'] = $pregunta->nota_informativa;

						$respSubCriterio = $this->Evaluaciones_sub_criterio_model->insertar($info_subcriterio);
					}
				}
			}

			$this->load->model('Evaluaciones_evaluaciones_periodo_model');
			$this->load->model('Evaluaciones_avance_evaluacion_model');

			$id_eva = $respuesta['last_id'];
			$contratas = json_decode($info_evaluacion['contrata']);

			$respPeriodo = $this->Evaluaciones_evaluaciones_periodo_model->nueva_evaluacion($id_eva);

			foreach ($contratas as $key=>$value)
			{
				$this->Evaluaciones_avance_evaluacion_model->nueva_evaluacion($id_eva,$value,$respPeriodo["last_id"]);
			}
		}
	}

	public function eliminar($id)
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if($session['logged_in'])
		{
			$this->load->model('Evaluaciones_evaluacion_model');

			$info = $this->input->input_stream();

			$info_evaluacion['eliminado'] = 1;

			if($this->Evaluaciones_evaluacion_model->actualizar($id, $info_evaluacion))
			{
				$evento = array(
						"accion"		=> "Eliminación de Evaluación",
						"tabla"			=> "tb_evaluacion",
						"id"			=> $id
					);

				$this->load->model('Logs_evaluaciones_evaluacion_model');
				$this->Logs_evaluaciones_evaluacion_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);
			}
		}
	}

	public function listar_activas()
	{
		$this->load->model('Evaluaciones_evaluacion_model');

		$listado = $this->Evaluaciones_evaluacion_model->listar_activas();

		echo json_encode($listado);
	}

	public function listar_todas()
	{
		$this->load->model('Evaluaciones_evaluacion_model');

		$listado = $this->Evaluaciones_evaluacion_model->listar_todas();

		echo json_encode($listado);
	}
}

?>