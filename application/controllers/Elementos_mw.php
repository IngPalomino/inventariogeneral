<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Elementos_mw extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Elementos_mw_model');
		$this->load->library('auth');
	}

	public function listar()
	{
		echo json_encode($this->Elementos_mw_model->get_types());
	}

	public function id_tipo()
	{
		$info = array();
		$info = $this->input->post();
		$tipo = $this->Elementos_mw_model->get_type($info);
		echo json_encode($tipo);
	}

	public function editar($id)
	{
		echo json_encode($this->Elementos_mw_model->edit($id));
	}

	public function actualizar($id)
	{
		$this->Elementos_mw_model->update($id, $this->input->input_stream());
	}

	public function crear()
	{
		$this->Elementos_mw_model->insert($this->input->post());
	}

	public function eliminar($id)
	{
		$this->Elementos_mw_model->delete($id);
	}

	public function almacenar()
	{
		$this->load->model('Bodega_elementos_mw_model');
		$this->load->model('Logs_bodega_elementos_mw_model');
		$info = $this->input->input_stream();
		$info_odus = json_decode($info["odus"], TRUE);
		$info_antenas_mw = json_decode($info["antenas_mw"], TRUE);
		
		if(count($info_odus) > 0)
		{
			$this->load->model('Odus_model');

			foreach($info_odus as $odu)
			{
				if($this->Odus_model->update($odu["id"], array('eliminado' => 1)))
				{
					$info_odu["elemento_mw"] = 8;
					$info_odu["tipo_elemento_mw"] = $odu["tipo_odu"]["id"];
					$info_odu["numero_serie"] = $odu["numero_serie"];
					$info_odu["estado"] = $info["estado"];
					$info_odu["proveniente_sitio"] = $info["proveniente_sitio"];
					$info_odu["tipo_almacen"] = $info["tipo_almacen"];
					$info_odu["almacen"] = $info["almacen"];

					$this->Bodega_elementos_mw_model->insert($info_odu);

					$evento = array(
						"accion"		=> "Almacenamiento de ODU",
						"tabla"			=> "tb_bodega_elementos_mw"
					);

					$this->Logs_bodega_elementos_mw_model->ingreso_de_datos($evento);
				}
			}
		}

		if(count($info_antenas_mw) > 0)
		{
			$this->load->model('Antenas_mw_model');

			foreach($info_antenas_mw as $antena)
			{
				if($this->Antenas_mw_model->update($antena["id"], array('eliminado' => 1)))
				{
					$info_antena_mw["elemento_mw"] = 3;
					$info_antena_mw["tipo_elemento_mw"] = $antena["tipo_antena_mw"]["id"];
					$info_antena_mw["numero_serie"] = $antena["numero_serie"];
					$info_antena_mw["estado"] = $info["estado"];
					$info_antena_mw["proveniente_sitio"] = $info["proveniente_sitio"];
					$info_antena_mw["tipo_almacen"] = $info["tipo_almacen"];
					$info_antena_mw["almacen"] = $info["almacen"];

					$this->Bodega_elementos_mw_model->insert($info_antena_mw);

					$evento = array(
						"accion"		=> "Almacenamiento de Antena Microondas",
						"tabla"			=> "tb_bodega_elementos_mw"
					);

					$this->Logs_bodega_elementos_mw_model->ingreso_de_datos($evento);
				}
			}
		}
	}
}