<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_antenas_mw extends CI_Controller {
	
	public function listar()
	{
		$this->load->model('Bodega_antenas_mw_model');
		$listado = $this->Bodega_antenas_mw_model->get_antenas_mw();
		echo json_encode($listado);
	}

	public function listar_almacen($id)
	{
		$this->load->model('Bodega_antenas_mw_model');
		$listado = $this->Bodega_antenas_mw_model->get_antenas_mw_almacen($id);
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Bodega_antenas_mw_model');
		$listado = $this->Bodega_antenas_mw_model->get_antenas_mw_sitio($id);
		echo json_encode($listado);
	}

	public function actualizar($id)
	{
		$this->load->model('Bodega_antenas_mw_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = $this->input->input_stream();
		$antena = $this->Bodega_antenas_mw_model->edit($id);

		if($this->Bodega_antenas_mw_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Antena Microondas",
					"tabla"			=> "tb_bodega_antenas_mw",
					"id"			=> $id,
					"datos viejos"	=> $antena,
					"datos nuevos"	=> $this->Bodega_antenas_mw_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_bodega_antenas_mw_model');
				$this->Logs_bodega_antenas_mw_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
			}
		}
	}

	public function reactivar($id)
	{
		$this->load->model('Bodega_antenas_mw_model');

		$info = $this->input->input_stream();
		$info_antena["eliminado"] = 1;
		
		if($this->Bodega_antenas_mw_model->update($id, $info_antena))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Reinstalación de Antena Microondas",
					"tabla"			=> "tb_bodega_antenas_mw",
					"id"			=> $id
				);

			$this->load->model('Logs_bodega_antenas_mw_model');
			$this->Logs_bodega_antenas_mw_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$this->load->model('Antenas_mw_model');
			unset($info['comentario']);

			if($this->Antenas_mw_model->insert($info))
			{
				$evento = array(
					"accion"		=> "Reinstalación de Antena Microondas",
					"tabla"			=> "tb_antenas_mw"
				);

				$this->load->model('Logs_antenas_mw_model');
				$this->Logs_antenas_mw_model->ingreso_de_datos($session['id_usuario'],$evento);
			}
		}
	}

	public function mover($id)
	{
		$this->load->model('Bodega_antenas_mw_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = $this->input->input_stream();
		$antena = $this->Bodega_antenas_mw_model->edit($id);

		$almacen_antiguo = array(
					"tipo_almacen" 	=> $antena[0]["tipo_almacen"],
					"almacen"		=> $antena[0]["almacen"]
				);

		$info["almacen_previo"] = json_encode($almacen_antiguo);
		$comentario = $info["comentario"];
		unset($info["comentario"]);

		if($this->Bodega_antenas_mw_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			$antena_nueva = $this->Bodega_antenas_mw_model->edit($id);

			$almacen_nuevo = array(
					"tipo_almacen" 	=> $antena_nueva[0]["tipo_almacen"],
					"almacen"		=> $antena_nueva[0]["almacen"]
				);

			$evento = array(
					"accion"		=> "Cambio de almacén de Antena Microondas",
					"tabla"			=> "tb_bodega_antenas_mw",
					"id"			=> $id,
					"almacen_antiguo"	=> $info["almacen_previo"],
					"almacen_nuevo"	=> json_encode($almacen_nuevo)
				);

			if($evento["almacen_antiguo"] != $evento["almacen_nuevo"])
			{
				$this->load->model('Logs_bodega_antenas_mw_model');
				$this->Logs_bodega_antenas_mw_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$comentario);
			}
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Bodega_antenas_mw_model');

		$info = $this->input->input_stream();

		$info_antena['eliminado'] = 1;

		if($this->Bodega_antenas_mw_model->update($id, $info_antena))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Antena Microondas",
					"tabla"			=> "tb_bodega_antenas_mw",
					"id"			=> $id
				);

			$this->load->model('Logs_bodega_antenas_mw_model');
			$this->Logs_bodega_antenas_mw_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);
		}
	}
}