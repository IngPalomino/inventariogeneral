<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_antena_iden extends CI_Controller {

	public function listar()
	{
		$this->load->model('Tipo_antena_iden_model');

		$listado = $this->Tipo_antena_iden_model->get_types();

		echo json_encode($listado);
	}

	public function id_tipo()
	{
		$this->load->model('Tipo_antena_iden_model');

		$info = array();
		$info = $this->input->post();

		$tipo = $this->Tipo_antena_iden_model->get_type($info);

		echo json_encode($tipo);
	}

	public function marcas()
	{
		$this->load->model('Tipo_antena_iden_model');

		$marcas = $this->Tipo_antena_iden_model->get_marcas();

		echo json_encode($marcas);
	}

	public function modelos($marca)
	{
		$this->load->model('Tipo_antena_iden_model');

		$modelos = $this->Tipo_antena_iden_model->get_modelos(urldecode($marca));

		echo json_encode($modelos);
	}

	/*public function versiones($marca,$modelo)
	{
		$this->load->model('Tipo_antena_microondas_model');

		$versiones = $this->Tipo_antena_microondas_model->get_versiones($marca,$modelo);

		echo json_encode($versiones);
	}*/

	public function editar($id)
	{
		$this->load->model('Tipo_antena_iden_model');

		$antena = $this->Tipo_antena_iden_model->edit($id);

		echo json_encode($antena);
	}

	public function actualizar($id)
	{
		$this->load->model('Tipo_antena_iden_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$this->Tipo_antena_iden_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Tipo_antena_iden_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Tipo_antena_iden_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Tipo_antena_iden_model');

		$this->Tipo_antena_iden_model->delete($id);
	}
}