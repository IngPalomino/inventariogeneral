<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_masivo_suministros extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->modulo = 9;
	}

	public function plantilla_suministros()
	{
		$this->load->library("excel");
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla Suministros")
									->setSubject("Plantilla")
									->setDescription("Plantilla para ingresar información de Suministros al Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle("Suministros");

		$activeSheet->setCellValue("A1", "Código")
					->setCellValue("B1", "Nombre")
					->setCellValue("C1", "Tipo de alimentación")
					->setCellValue("D1", "Concesionaria")
					->setCellValue("E1", "Conexión")
					->setCellValue("F1", "Suministro")
					->setCellValue("G1", "Potencia contratada (KW)")
					->setCellValue("H1", "Opción tarifaria")
					->setCellValue("I1", "Nivel de tensión (KV)")
					->setCellValue("J1", "Sistema");

		$styleArray = array(
				"font" => array(
						"bold"	=> true,
						"color"	=> array("rgb" => "FFFFFF"),
						"size"	=> 11,
						"name"	=> "Verdana"
					),
				"fill" => array(
						"type"	=> PHPExcel_Style_Fill::FILL_SOLID,
						"color"	=> array("rgb" => "6788D3")
					),
				"alignment" => array(
						"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle("A1:J1")->applyFromArray($styleArray);

		$activeSheet->getStyle('A:A')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$activeSheet->getColumnDimension("A")->setAutoSize(true);
		$activeSheet->getColumnDimension("B")->setAutoSize(true);
		$activeSheet->getColumnDimension("C")->setAutoSize(true);
		$activeSheet->getColumnDimension("D")->setAutoSize(true);
		$activeSheet->getColumnDimension("E")->setAutoSize(true);
		$activeSheet->getColumnDimension("F")->setAutoSize(true);
		$activeSheet->getColumnDimension("G")->setAutoSize(true);
		$activeSheet->getColumnDimension("H")->setAutoSize(true);
		$activeSheet->getColumnDimension("I")->setAutoSize(true);
		$activeSheet->getColumnDimension("J")->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Suministros.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function plantilla_desplegables_suministros()
	{
		$this->load->model('Tipo_alimentacion_model');
		$this->load->model('Concesionaria_suministro_model');
		$this->load->model('Conexion_suministro_model');
		$this->load->model('Sistema_suministro_model');
		$this->load->model('Opcion_tarifaria_model');
		$tipos_alimentacion = $this->Tipo_alimentacion_model->get_types();
		$concesionarias = $this->Concesionaria_suministro_model->get_types();
		$conexiones = $this->Conexion_suministro_model->get_types();
		$sistemas = $this->Sistema_suministro_model->get_types();
		$opciones = $this->Opcion_tarifaria_model->get_types();

		$this->load->library("excel");
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla Suministros")
									->setSubject("Plantilla")
									->setDescription("Plantilla para ingresar información de Suministros al Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle("Desplegables");

		$activeSheet->setCellValue("A1", "Tipo de alimentación")
					->setCellValue("C1", "Concesionaria")
					->setCellValue("E1", "Conexión")
					->setCellValue("G1", "Sistema")
					->setCellValue("H1", "Fases")
					->setCellValue("J1", "Opción Tarifaria");

		$styleArray = array(
				"font" => array(
						"bold"	=> true,
						"color"	=> array("rgb" => "FFFFFF"),
						"size"	=> 11,
						"name"	=> "Verdana"
					),
				"fill" => array(
						"type"	=> PHPExcel_Style_Fill::FILL_SOLID,
						"color"	=> array("rgb" => "6788D3")
					),
				"alignment" => array(
						"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle("A1:A1")->applyFromArray($styleArray);
		$activeSheet->getStyle("C1:C1")->applyFromArray($styleArray);
		$activeSheet->getStyle("E1:E1")->applyFromArray($styleArray);
		$activeSheet->getStyle("G1:H1")->applyFromArray($styleArray);
		$activeSheet->getStyle("J1:J1")->applyFromArray($styleArray);

		$i = 2;

		foreach($tipos_alimentacion as $fila)
		{
			$activeSheet->setCellValue("A".$i, $fila["tipo_alimentacion"]);

			$i++;
		}

		$i = 2;

		foreach($concesionarias as $fila)
		{
			$activeSheet->setCellValue("C".$i, $fila["concesionaria_suministro"]);

			$i++;
		}

		$i = 2;

		foreach($conexiones as $fila)
		{
			$activeSheet->setCellValue("E".$i, $fila["conexion_suministro"]);

			$i++;
		}

		$i = 2;

		foreach($sistemas as $fila)
		{
			$activeSheet->setCellValue("G".$i, $fila["sistema_suministro"]);
			$activeSheet->setCellValue("H".$i, $fila["fases"]);

			$i++;
		}

		$i = 2;

		foreach($opciones as $fila)
		{
			$activeSheet->setCellValue("J".$i, $fila["opcion_tarifaria"]);

			$i++;
		}

		$activeSheet->getColumnDimension("A")->setAutoSize(true);
		$activeSheet->getColumnDimension("C")->setAutoSize(true);
		$activeSheet->getColumnDimension("E")->setAutoSize(true);
		$activeSheet->getColumnDimension("G")->setAutoSize(true);
		$activeSheet->getColumnDimension("H")->setAutoSize(true);
		$activeSheet->getColumnDimension("J")->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Desplegables_Suministros.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function upload_file()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"])
		{
			$this->load->model('Autorizacion_grupos_model');

			if($this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo))
			{
				$post = $this->input->post();
				$config["upload_path"] = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo";
				$config["allowed_types"] = "xlsx";
				$config["max_size"] = "1024";
				$config["overwrite"] = FALSE;
				$config["file_name"] = "plantilla_".$post["tipo"]."_".date("d-m-Y-H-i-s");

				$this->load->library("upload", $config);

				if($this->upload->do_upload("archivo"))
				{
					echo json_encode(array(
							"error"		=> FALSE,
							"mensaje"	=> "Archivo subido con éxito.",
							"resultado"	=> $this->upload->data("file_name")
						));
				}
				else
				{
					echo json_encode(array(
							"error"		=> TRUE,
							"mensaje"	=> "No se pudo subir el archivo.",
							"resultado"	=> $this->upload->display_errors()
						));
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción.",
						"resultado"	=> "forbidden"
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró",
					"resultado"	=> "logged_off"
				));
		}
	}

	public function check_file()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"])
		{
			$this->load->model('Autorizacion_grupos_model');

			if($this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo))
			{
				$this->load->model('Ingreso_masivo_suministros_model');
				$post = $this->input->post();
				$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"];
				/*$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/plantilla_antenas_gul_19-06-2017-16-09-50.xlsx";*/
				
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						"error"			=> FALSE,
						"advertencia"	=> FALSE,
						"mensajes"		=> array(),
						"advertencias"	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				if(in_array("Suministros", $sheetNames))
				{
					try{ $objPHPExcel->setActiveSheetIndexByName("Suministros"); } catch(Exception $e){ continue; }

					$activeSheet = $objPHPExcel->getActiveSheet();
					$cell_collection = $activeSheet->getCellCollection();
					$cabeceras = array("Código", "Nombre", "Tipo de alimentación", "Concesionaria", "Conexión", "Suministro", "Potencia contratada (KW)", "Opción tarifaria", "Nivel de tensión (KV)", "Sistema");
					$cabeceras_no_validas = array();
					$cabeceras_validas = array();
					$arr_data = array();

					foreach($cell_collection as $cell)
					{
						$column = $activeSheet->getCell($cell)->getColumn();
						$row = $activeSheet->getCell($cell)->getRow();
						$data_value = $activeSheet->getCell($cell)->getValue();

						if($row == 1)
						{
							$header[$row][$column] = $data_value;

							foreach($header as $filaHeader => $infoHeader)
							{
								if(!in_array($infoHeader[$column], $cabeceras))
								{
									array_push($cabeceras_no_validas, $infoHeader[$column]);
								}
								else
								{
									array_push($cabeceras_validas, $infoHeader[$column]);
								}
							}
						}
						else
						{
							if(!isset($data_value) && $column == "A")
							{
								/*break;*/
								$resultados["advertencia"] = TRUE;
								array_push($resultados["advertencias"], "La columna $column en la fila $row no tiene información, se obviará esta fila.");
							}
							elseif(in_array($header[1][$column], $cabeceras_no_validas))
							{
								
							}
							else
							{
								$arr_data[$row][$column] = $data_value;
							}
						}
					}

					foreach($cabeceras as &$value)
					{
						if(!in_array($value, $cabeceras_validas))
						{
							$resultados["error"] = TRUE;
							array_push($resultados["mensajes"], "La columna $value es obligatoria.");
						}
					}

					if(count($cabeceras_no_validas) > 0)
					{
						$cabeceras_no_validas = array_values(array_unique($cabeceras_no_validas));

						foreach($cabeceras_no_validas as $cabecera)
						{
							$resultados["advertencia"] = TRUE;
							array_push($resultados["advertencias"], "La cabecera $cabecera no corresponde a la plantilla original, se obviará esta columna.");
						}
					}

					/*echo json_encode($cabeceras_no_validas);*/
					$filas = count($arr_data);

					if(count($arr_data) > 0)
					{
						foreach($arr_data as $fila => $infoFila)
						{
							$nombreSeparado = explode("_", $infoFila["B"]);
							$codigoNombre = $nombreSeparado[0];

							foreach($infoFila as $columna => $campo)
							{
								if($header[1][$columna] == "Código")
								{
									$codigo = $campo;
									if(strlen($campo) != 7)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código de sitio en la fila $fila no es válido, debe tener 7 dígitos.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código de sitio en la fila $fila debe contener únicamente números.");
									}
									else
									{
										if($codigoNombre != $campo)
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "El Código de sitio en la fila $fila no coincide con el código del Nombre.");
										}
									}
								}
								elseif($header[1][$columna] == "Nombre")
								{
									$nombre = $this->Ingreso_masivo_suministros_model->esNombreCodigoValidos($codigo, $campo);
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Nombre en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_suministros_model->esNombreCompletoValido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre de sitio en la fila $fila no existe en la base de datos.");
									}
									elseif(!$nombre)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre y Código de sitio en la fila $fila no existe en la base de datos.");
									}
									elseif(!$this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo, $nombre["id"]))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Usted no tiene permiso para ingresar/actualizar información en el sitio $campo de la fila $fila.");
									}
									elseif($this->Ingreso_masivo_suministros_model->existeSuministroEnSitio($nombre["id"]) !== false)
									{
										$resultados["advertencia"] = TRUE;
										array_push($resultados["advertencias"], "El sitio $campo en la fila $fila ya tiene un Suministro, se actualizará este elemento.");
									}
								}
								elseif($header[1][$columna] == "Tipo de alimentación")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Tipo de alimentación en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_suministros_model->esTipoAlimentacionValido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Tipo de alimentación en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Concesionaria")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Concesionaria en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_suministros_model->esConcesionariaValida($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La concesionaria en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Conexión")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Conexión en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_suministros_model->esConexionValida($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Conexión en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Suministro")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el número de Suministro en la fila $fila.");
									}
								}
								elseif($header[1][$columna] == "Potencia contratada (KW)")
								{
									if(!isset($columna))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Potencia contratada en la fila $fila.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Potencia contratada (KW) en la fila $fila debe contener únicamente números");
									}
								}
								elseif($header[1][$columna] == "Opción tarifaria")
								{
									if(!isset($columna) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Opción tarifaria en la fila $fila.");
									}
									elseif($this->Ingreso_masivo_suministros_model->esOpcionTarifariaValida($campo) === false)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Opción tarifaria en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Nivel de tensión (KV)")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Nivel de tensión (KV) en la fila $fila.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nivel de tensión (KV) en la fila $fila debe contener únicamente números");
									}
								}
								elseif($header[1][$columna] == "Sistema")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Sistema en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_suministros_model->esSistemaValido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Sistema en la fila $fila no existe en la base de datos.");
									}
								}
							}
						}

						/*echo json_encode($arr_data);*/
					}
					else
					{
						$resultados["error"] = TRUE;
						array_push($resultados["mensajes"], "La hoja no contiene información");
					}
				}
				else
				{
					$resultados["error"] = TRUE;
					array_push($resultados["mensajes"], "No se encontró la hoja llamada 'Suministros'");
				}

				if(!$resultados["error"])
				{
					array_push($resultados["mensajes"], "Filas válidas: ".count($arr_data));
				}

				echo json_encode($resultados);

				if($resultados["error"])
				{
					unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción."
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró."
				));
		}
	}

	public function insert_temp()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"])
		{
			$this->load->model('Autorizacion_grupos_model');

			if($this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo))
			{
				$this->load->model('Ingreso_masivo_suministros_model');
				$idIngresoMasivo = $this->Ingreso_masivo_suministros_model->registrar_ingreso_masivo($session['id_usuario']);
				$post = $this->input->post();
				$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"];
				/*$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/plantilla_antenas_gul_19-06-2017-16-09-50.xlsx";*/
				
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						"error"			=> FALSE,
						"advertencia"	=> FALSE,
						"mensajes"		=> array(),
						"advertencias"	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				$resultado = 0;

				if(in_array("Suministros", $sheetNames))
				{
					try{ $objPHPExcel->setActiveSheetIndexByName("Suministros"); } catch(Exception $e){ continue; }

					$activeSheet = $objPHPExcel->getActiveSheet();
					$cell_collection = $activeSheet->getCellCollection();
					$cabeceras = array("Código", "Nombre", "Tipo de alimentación", "Concesionaria", "Conexión", "Suministro", "Potencia contratada (KW)", "Opción tarifaria", "Nivel de tensión (KV)", "Sistema");
					$cabeceras_no_validas = array();
					$cabeceras_validas = array();
					$arr_data = array();

					foreach($cell_collection as $cell)
					{
						$column = $activeSheet->getCell($cell)->getColumn();
						$row = $activeSheet->getCell($cell)->getRow();
						$data_value = $activeSheet->getCell($cell)->getValue();

						if($row == 1)
						{
							$header[$row][$column] = $data_value;

							foreach($header as $filaHeader => $infoHeader)
							{
								if(!in_array($infoHeader[$column], $cabeceras))
								{
									array_push($cabeceras_no_validas, $infoHeader[$column]);
								}
								else
								{
									array_push($cabeceras_validas, $infoHeader[$column]);
								}
							}
						}
						else
						{
							if(!isset($data_value) && $column == "A")
							{
								/*break;*/
							}
							elseif(in_array($header[1][$column], $cabeceras_no_validas))
							{
								
							}
							else
							{
								$arr_data[$row][$column] = $data_value;
							}
						}
					}

					/*echo json_encode($cabeceras_no_validas);*/
					$filas = count($arr_data);

					if($idIngresoMasivo !== FALSE)
					{
						foreach($arr_data as $fila => $infoFila)
						{
							$nombreSeparado = explode("_", $infoFila["B"]);
							$codigoNombre = $nombreSeparado[0];

							foreach($infoFila as $columna => $campo)
							{
								$ingresoMasivo["ingreso_masivo"] = $idIngresoMasivo;

								if($header[1][$columna] == "Código")
								{
									$codigo = $campo;
								}
								elseif($header[1][$columna] == "Nombre")
								{
									$nombre = $this->Ingreso_masivo_suministros_model->esNombreCodigoValidos($codigo, $campo);
									$ingresoMasivo["sitio"] = $nombre["id"];

									$actualizar = $this->Ingreso_masivo_suministros_model->existeSuministroEnSitio($nombre["id"]);

									$ingresoMasivo["id_actualizar"] = (($actualizar !== FALSE)? $actualizar["id"] : NULL);
								}
								elseif($header[1][$columna] == "Tipo de alimentación")
								{
									$tipo_alimentacion = $this->Ingreso_masivo_suministros_model->esTipoAlimentacionValido($campo);
									$ingresoMasivo["tipo_alimentacion"] = $tipo_alimentacion["id"];
								}
								elseif($header[1][$columna] == "Concesionaria")
								{
									$concesionaria = $this->Ingreso_masivo_suministros_model->esConcesionariaValida($campo);
									$ingresoMasivo["concesionaria_suministro"] = $concesionaria["id"];
								}
								elseif($header[1][$columna] == "Conexión")
								{
									$conexion = $this->Ingreso_masivo_suministros_model->esConexionValida($campo);
									$ingresoMasivo["conexion_suministro"] = $conexion["id"];
								}
								elseif($header[1][$columna] == "Suministro")
								{
									$ingresoMasivo["numero_suministro"] = $campo;
								}
								elseif($header[1][$columna] == "Potencia contratada (KW)")
								{
									$ingresoMasivo["potencia_contratada"] = $campo;
								}
								elseif($header[1][$columna] == "Opción tarifaria")
								{
									$opcion_tarifaria = $this->Ingreso_masivo_suministros_model->esOpcionTarifariaValida($campo);
									$ingresoMasivo["opcion_tarifaria"] = $opcion_tarifaria["id"];
								}
								elseif($header[1][$columna] == "Nivel de tensión (KV)")
								{
									$ingresoMasivo["nivel_tension"] = $campo;
								}
								elseif($header[1][$columna] == "Sistema")
								{
									$sistema = $this->Ingreso_masivo_suministros_model->esSistemaValido($campo);
									$ingresoMasivo["sistema_suministro"] = $sistema["id"];
								}
							}

							if($this->Ingreso_masivo_suministros_model->ingresar_temporal($ingresoMasivo))
							{
								$resultado++;
							}
						}

						$resultados["mensajes"] = "Se ha enviado $resultado fila(s) para ser aprobada(s).";
						unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
					}
					else
					{
						$resultados["error"] = TRUE;
						array_push($resultados["mensajes"], "La hoja no contiene información");
					}
				}
				else
				{
					$resultados["error"] = TRUE;
					array_push($resultados["mensajes"], "No se encontró la hoja llamada 'Suministros'");
				}

				if(!$resultados["error"])
				{
					array_push($resultados["mensajes"], "Filas válidas: ".count($arr_data));
				}

				echo json_encode($resultados);

				if($resultados["error"])
				{
					unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción."
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró."
				));
		}
	}

	public function listar_solicitudes($usuario)
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"] && ($session["id_usuario"] == $usuario))
		{
			$this->load->model('Ingreso_masivo_suministros_model');
			$listado = $this->Ingreso_masivo_suministros_model->get_solicitudes($usuario);
			echo json_encode($listado);
		}
		else
		{
			show_404();
		}
	}
}