<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mantenimientos_lineas_electricas extends CI_Controller {

	public function listar()
	{
		$this->load->model('Mantenimientos_lineas_electricas_model');

		$listado = $this->Mantenimientos_lineas_electricas_model->get_mantenimientos();

		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Mantenimientos_lineas_electricas_model');

		$listado = $this->Mantenimientos_lineas_electricas_model->get_mantenimientos_sitio($id);

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Mantenimientos_lineas_electricas_model');

		$mantenimiento = $this->Mantenimientos_lineas_electricas_model->edit($id);

		echo json_encode($mantenimiento);
	}

	public function actualizar($id)
	{
		$this->load->model('Mantenimientos_lineas_electricas_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();
		
		$info_mantenimiento["tipo_mantenimiento_linea_electrica"] = $info["tipo_mantenimiento_linea_electrica"];
		$info_mantenimiento["fecha"] = $info["fecha"];
		$info_mantenimiento["horas"] = $info["horas"];
		$info_mantenimiento["observaciones"] = $info["observaciones"];

		$mantenimiento = $this->Mantenimientos_lineas_electricas_model->edit($id);

		if($this->Mantenimientos_lineas_electricas_model->update($id, $info_mantenimiento))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Mantenimiento de Grupo Electrógeno",
					"tabla"			=> "tb_mantenimientos_grupos_electrogenos",
					"id"			=> $id,
					"datos viejos"	=> $mantenimiento,
					"datos nuevos"	=> $this->Mantenimientos_lineas_electricas_model->edit($id)
				);

			$this->load->model('Logs_mantenimientos_lineas_electricas_model');
			$this->Logs_mantenimientos_lineas_electricas_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
		}
	}

	public function registrar_mantenimiento($id)
	{
		$this->load->model('Mantenimientos_lineas_electricas_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["linea_electrica"] = $id;

		if($this->Mantenimientos_lineas_electricas_model->insert($info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Registro de Mantenimiento de Grupo Electrógeno",
					"tabla"		=> "tb_mantenimientos_grupos_electrogenos"
				);

			$this->load->model('Logs_mantenimientos_lineas_electricas_model');
			$this->Logs_mantenimientos_lineas_electricas_model->ingreso_de_datos($session['id_usuario'],$evento);
		}
	}
}