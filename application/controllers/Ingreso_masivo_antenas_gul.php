<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_masivo_antenas_gul extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->modulo = 3;
	}

	public function plantilla_antenas_gul()
	{
		$this->load->library("excel");
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla Antenas GUL")
									->setSubject("Plantilla")
									->setDescription("Plantilla para ingresar nueva información de Antenas GUL al Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle("Antenas GUL");

		$activeSheet->setCellValue("A1", "Código")
					->setCellValue("B1", "Nombre")
					->setCellValue("C1", "Marca")
					->setCellValue("D1", "Modelo")
					->setCellValue("E1", "Sector")
					->setCellValue("F1", "Altura (m)")
					->setCellValue("G1", "Azimuth (°)")
					->setCellValue("H1", "Tilt Mecánico (°)")
					->setCellValue("I1", "Tilt Eléctrico (°)")
					->setCellValue("J1", "Número de Serie");

		$styleArray = array(
				"font" => array(
						"bold"	=> true,
						"color"	=> array("rgb" => "FFFFFF"),
						"size"	=> 11,
						"name"	=> "Verdana"
					),
				"fill" => array(
						"type"	=> PHPExcel_Style_Fill::FILL_SOLID,
						"color"	=> array("rgb" => "6788D3")
					),
				"alignment" => array(
						"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle("A1:J1")->applyFromArray($styleArray);

		$activeSheet->getStyle('A:A')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$activeSheet->getColumnDimension("A")->setAutoSize(true);
		$activeSheet->getColumnDimension("B")->setAutoSize(true);
		$activeSheet->getColumnDimension("C")->setAutoSize(true);
		$activeSheet->getColumnDimension("D")->setAutoSize(true);
		$activeSheet->getColumnDimension("E")->setAutoSize(true);
		$activeSheet->getColumnDimension("F")->setAutoSize(true);
		$activeSheet->getColumnDimension("G")->setAutoSize(true);
		$activeSheet->getColumnDimension("H")->setAutoSize(true);
		$activeSheet->getColumnDimension("I")->setAutoSize(true);
		$activeSheet->getColumnDimension("J")->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Antenas_GUL.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function plantilla_tipo_antenas_gul()
	{
		$this->load->model('Tipo_antena_gul_model');
		$listado = $this->Tipo_antena_gul_model->get_all();
		$this->load->library("excel");
		$this->excel->getProperties()->setCreator("OyM")
									->setLastModifiedBy("OyM")
									->setTitle("Plantilla Tipos Antenas GUL")
									->setSubject("Plantilla")
									->setDescription("Plantilla de marcas y modelos para ingresar nueva información de Antenas GUL al Inventario General.")
									->setKeywords("office 2007 openxml php")
									->setCategory("Test result file");

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle("Datos Generales de Antenas GUL");

		$activeSheet->setCellValue("A1", "Marca")
					->setCellValue("B1", "Modelo")
					->setCellValue("C1", "Banda (GHz)")
					->setCellValue("D1", "Peso (Kg)")
					->setCellValue("E1", "Dimensiones");

		$styleArray = array(
				"font" => array(
						"bold"	=> true,
						"color"	=> array("rgb" => "FFFFFF"),
						"size"	=> 11,
						"name"	=> "Verdana"
					),
				"fill" => array(
						"type"	=> PHPExcel_Style_Fill::FILL_SOLID,
						"color"	=> array("rgb" => "6788D3")
					),
				"alignment" => array(
						"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle("A1:E1")->applyFromArray($styleArray);

		$i = 2;

		foreach($listado as $fila)
		{
			$activeSheet->setCellValue("A".$i, $fila["marca"])
						->setCellValue("B".$i, $fila["modelo"])
						->setCellValue("C".$i, $fila["banda"])
						->setCellValue("D".$i, $fila["peso"])
						->setCellValue("E".$i, $fila["dimensiones"]);

			$i++;
		}

		$activeSheet->setAutoFilter($activeSheet->calculateWorksheetDimension());

		$activeSheet->getColumnDimension("A")->setAutoSize(true);
		$activeSheet->getColumnDimension("B")->setAutoSize(true);
		$activeSheet->getColumnDimension("C")->setAutoSize(true);
		$activeSheet->getColumnDimension("D")->setAutoSize(true);
		$activeSheet->getColumnDimension("E")->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Tipo_Antenas_GUL.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function upload_file()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"])
		{
			$this->load->model('Autorizacion_grupos_model');

			if($this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo))
			{
				$post = $this->input->post();
				$config["upload_path"] = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo";
				$config["allowed_types"] = "xlsx";
				$config["max_size"] = "1024";
				$config["overwrite"] = FALSE;
				$config["file_name"] = "plantilla_".$post["tipo"]."_".date("d-m-Y-H-i-s");

				$this->load->library("upload", $config);

				if($this->upload->do_upload("archivo"))
				{
					echo json_encode(array(
							"error"		=> FALSE,
							"mensaje"	=> "Archivo subido con éxito.",
							"resultado"	=> $this->upload->data("file_name")
						));
				}
				else
				{
					echo json_encode(array(
							"error"		=> TRUE,
							"mensaje"	=> "No se pudo subir el archivo.",
							"resultado"	=> $this->upload->display_errors()
						));
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción.",
						"resultado"	=> "forbidden"
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró",
					"resultado"	=> "logged_off"
				));
		}
	}

	public function check_file()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"])
		{
			$this->load->model('Autorizacion_grupos_model');

			if($this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo))
			{
				$this->load->model('Ingreso_masivo_antenas_gul_model');
				$post = $this->input->post();
				$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"];
				/*$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/plantilla_antenas_gul_19-06-2017-16-09-50.xlsx";*/
				
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						"error"			=> FALSE,
						"advertencia"	=> FALSE,
						"mensajes"		=> array(),
						"advertencias"	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				if(in_array("Antenas GUL", $sheetNames))
				{
					try{ $objPHPExcel->setActiveSheetIndexByName("Antenas GUL"); } catch(Exception $e){ continue; }

					$activeSheet = $objPHPExcel->getActiveSheet();
					$cell_collection = $activeSheet->getCellCollection();
					$cabeceras = array("Código", "Nombre", "Marca", "Modelo", "Sector", "Altura (m)", "Azimuth (°)", "Tilt Mecánico (°)", "Tilt Eléctrico (°)", "Número de Serie");
					$cabeceras_no_validas = array();
					$cabeceras_validas = array();
					$arr_data = array();

					foreach($cell_collection as $cell)
					{
						$column = $activeSheet->getCell($cell)->getColumn();
						$row = $activeSheet->getCell($cell)->getRow();
						$data_value = $activeSheet->getCell($cell)->getValue();

						if($row == 1)
						{
							$header[$row][$column] = $data_value;

							foreach($header as $filaHeader => $infoHeader)
							{
								if(!in_array($infoHeader[$column], $cabeceras))
								{
									array_push($cabeceras_no_validas, $infoHeader[$column]);
								}
								else
								{
									array_push($cabeceras_validas, $infoHeader[$column]);
								}
							}
						}
						else
						{
							if(!isset($data_value) && $column == "A")
							{
								/*break;*/
								$resultados["advertencia"] = TRUE;
								array_push($resultados["advertencias"], "La columna $column en la fila $row no tiene información, se obviará esta fila.");
							}
							elseif(in_array($header[1][$column], $cabeceras_no_validas))
							{
								
							}
							else
							{
								$arr_data[$row][$column] = $data_value;
							}
						}
					}

					foreach($cabeceras as &$value)
					{
						if(!in_array($value, $cabeceras_validas))
						{
							$resultados["error"] = TRUE;
							array_push($resultados["mensajes"], "La columna $value es obligatoria.");
						}
					}

					if(count($cabeceras_no_validas) > 0)
					{
						$cabeceras_no_validas = array_values(array_unique($cabeceras_no_validas));

						foreach($cabeceras_no_validas as $cabecera)
						{
							$resultados["advertencia"] = TRUE;
							array_push($resultados["advertencias"], "La cabecera $cabecera no corresponde a la plantilla original, se obviará esta columna.");
						}
					}

					/*echo json_encode($cabeceras_no_validas);*/
					$filas = count($arr_data);

					if(count($arr_data) > 0)
					{
						foreach($arr_data as $fila => $infoFila)
						{
							$nombreSeparado = explode("_", $infoFila["B"]);
							$codigoNombre = $nombreSeparado[0];

							foreach($infoFila as $columna => $campo)
							{
								if($header[1][$columna] == "Código")
								{
									$codigo = $campo;
									if(strlen($campo) != 7)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código de sitio en la fila $fila no es válido, debe tener 7 dígitos.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Código de sitio en la fila $fila debe contener únicamente números.");
									}
									else
									{
										if($codigoNombre != $campo)
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "El Código de sitio en la fila $fila no coincide con el código del Nombre.");
										}
									}
								}
								elseif($header[1][$columna] == "Nombre")
								{
									$nombre = $this->Ingreso_masivo_antenas_gul_model->esNombreCodigoValidos($codigo, $campo);
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Nombre en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_antenas_gul_model->esNombreCompletoValido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre de sitio en la fila $fila no existe en la base de datos.");
									}
									elseif(!$nombre)
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Nombre y Código de sitio en la fila $fila no existe en la base de datos.");
									}
									elseif(!$this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo, $nombre["id"]))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Usted no tiene permiso para ingresar/actualizar información en el sitio de la fila $fila.");
									}
								}
								elseif($header[1][$columna] == "Marca")
								{
									$marca = strtoupper($campo);
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Marca en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_antenas_gul_model->esMarcaValida($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Marca en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Modelo")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Modelo en la fila $fila.");
									}
									elseif(!$this->Ingreso_masivo_antenas_gul_model->esModeloValido($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El Modelo en la fila $fila no existe en la base de datos.");
									}
									elseif(!$this->Ingreso_masivo_antenas_gul_model->esMarcaModeloValidos($marca, $campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "El tipo de antena ingresado (Marca y Modelo) en la fila $fila no existe en la base de datos.");
									}
								}
								elseif($header[1][$columna] == "Sector")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Sector en la fila $fila.");
									}
									else
									{
										if($marca == "TELNET")
										{
											if($this->Ingreso_masivo_antenas_gul_model->existeAntenaTelnetEnSitio($nombre["id"]) !== false)
											{
												$resultados["advertencia"] = TRUE;
												array_push($resultados["advertencias"], "El sitio en la fila $fila ya tiene una Antena GUL marca TELNET, se actualizará este elemento.");
											}

											if(!preg_match("/[,\/\\\]+/", $campo))
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "Los sectores en la fila $fila deben estar separados por comas o slashes.");
											}
											else
											{
												$sectores = preg_split("/[\s,\/\\\]+/", $campo);
												/*echo json_encode($sectores);*/
												foreach($sectores as $key => $sector)
												{
													if(!is_numeric($sector))
													{
														$resultados["error"] = TRUE;
														array_push($resultados["mensajes"], "El Sector ".($key+1)." en la fila $fila debe contener únicamente números.");
													}
													elseif(($sector > 10) || ($sector < 1))
													{
														$resultados["error"] = TRUE;
														array_push($resultados["mensajes"], "El Sector en la fila $fila no puede ser menor que 1 o mayor que 10.");
													}
												}
											}
										}
										else
										{
											if(!is_numeric($campo))
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "El Sector en la fila $fila debe contener únicamente números.");
											}
											elseif(($campo > 10) || ($campo < 1))
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "El Sector en la fila $fila no puede ser menor que 1 o mayor que 10.");
											}
											elseif($this->Ingreso_masivo_antenas_gul_model->existeAntenaEnSitio($nombre["id"], $campo) !== false)
											{
												$resultados["advertencia"] = TRUE;
												array_push($resultados["advertencias"], "El sitio en la fila $fila ya tiene una Antena GUL en el sector $campo, se actualizará este elemento.");
											}
										}
									}
								}
								elseif($header[1][$columna] == "Altura (m)")
								{
									if($campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar la Altura en la fila $fila.");
									}
									elseif(!is_numeric($campo))
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "La Altura en la fila $fila debe contener únicamente números.");
									}
								}
								elseif($header[1][$columna] == "Azimuth (°)")
								{
									if($marca == "TELNET")
									{
										if(!preg_match("/[,\/\\\]+/", $campo))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "Los Azimuth en la fila $fila deben estar separados por comas o slashes.");
										}
										else
										{
											$azimuths = preg_split("/[\s,\/\\\]+/", $campo);
											
											if(count($azimuths) != count($sectores))
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "La cantidad de Azimuth y Sectores en la fila $fila deben coincidir.");
											}
											else
											{
												foreach($azimuths as $key => $sector)
												{
													if(!is_numeric($sector))
													{
														$resultados["error"] = TRUE;
														array_push($resultados["mensajes"], "El Azimuth ".($key+1)." en la fila $fila debe contener únicamente números.");
													}
												}
											}
										}
									}
									else
									{
										if(!is_numeric($campo))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "El Azimuth en la fila $fila debe contener únicamente números.");
										}
									}
								}
								elseif($header[1][$columna] == "Tilt Mecánico (°)")
								{
									if($marca == "TELNET")
									{
										if(!preg_match("/[,\/\\\]+/", $campo))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "Los Tilt Mecánico en la fila $fila deben estar separados por comas o slashes.");
										}
										else
										{
											$mecanicos = preg_split("/[\s,\/\\\]+/", $campo);
											
											if(count($mecanicos) != count($sectores))
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "La cantidad de Tilt Mecánico y Sectores en la fila $fila deben coincidir.");
											}
											else
											{
												foreach($mecanicos as $key => $sector)
												{
													if(!is_numeric($sector))
													{
														$resultados["error"] = TRUE;
														array_push($resultados["mensajes"], "El Tilt Mecánico ".($key+1)." en la fila $fila debe contener únicamente números.");
													}
												}
											}
										}
									}
									else
									{
										if(!is_numeric($campo))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "El Tilt Mecánico en la fila $fila debe contener únicamente números.");
										}
									}
								}
								elseif($header[1][$columna] == "Tilt Eléctrico (°)")
								{
									if($marca == "TELNET")
									{
										if(!preg_match("/[,\/\\\]+/", $campo))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "Los Tilt Eléctrico en la fila $fila deben estar separados por comas o slashes.");
										}
										else
										{
											$electricos = preg_split("/[\s,\/\\\]+/", $campo);
											
											if(count($electricos) != count($sectores))
											{
												$resultados["error"] = TRUE;
												array_push($resultados["mensajes"], "La cantidad de Tilt Eléctrico y Sectores en la fila $fila deben coincidir.");
											}
											else
											{
												foreach($electricos as $key => $sector)
												{
													if(!is_numeric($sector))
													{
														$resultados["error"] = TRUE;
														array_push($resultados["mensajes"], "El Tilt Eléctrico ".($key+1)." en la fila $fila debe contener únicamente números.");
													}
												}
											}
										}
									}
									else
									{
										if(!is_numeric($campo))
										{
											$resultados["error"] = TRUE;
											array_push($resultados["mensajes"], "El Tilt Eléctrico en la fila $fila debe contener únicamente números.");
										}
									}
								}
								elseif($header[1][$columna] == "Número de Serie")
								{
									if( ! isset($campo) || ! isset($columna) || $campo == "")
									{
										$resultados["error"] = TRUE;
										array_push($resultados["mensajes"], "Debe ingresar el Número de Serie en la fila $fila.");
									}
								}
							}
						}

						/*echo json_encode($arr_data);*/
					}
					else
					{
						$resultados["error"] = TRUE;
						array_push($resultados["mensajes"], "La hoja no contiene información");
					}
				}
				else
				{
					$resultados["error"] = TRUE;
					array_push($resultados["mensajes"], "No se encontró la hoja llamada 'Antenas GUL'");
				}

				if(!$resultados["error"])
				{
					array_push($resultados["mensajes"], "Filas válidas: ".count($arr_data));
				}

				echo json_encode($resultados);

				if($resultados["error"])
				{
					unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción."
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró."
				));
		}
	}

	public function insert_temp()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"])
		{
			$this->load->model('Autorizacion_grupos_model');

			if($this->Autorizacion_grupos_model->comprobar_autorizacion_modulo($session["id_usuario"], $this->modulo))
			{
				$this->load->model('Ingreso_masivo_antenas_gul_model');
				$idIngresoMasivo = $this->Ingreso_masivo_antenas_gul_model->registrar_ingreso_masivo($session['id_usuario']);
				$post = $this->input->post();
				$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"];
				/*$file = "/var/www/html/uploads/inventarioGeneral/ingreso_masivo/plantilla_antenas_gul_19-06-2017-16-09-50.xlsx";*/
				
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						"error"			=> FALSE,
						"advertencia"	=> FALSE,
						"mensajes"		=> array(),
						"advertencias"	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				$resultado = 0;

				if(in_array("Antenas GUL", $sheetNames))
				{
					try{ $objPHPExcel->setActiveSheetIndexByName("Antenas GUL"); } catch(Exception $e){ continue; }

					$activeSheet = $objPHPExcel->getActiveSheet();
					$cell_collection = $activeSheet->getCellCollection();
					$cabeceras = array("Código", "Nombre", "Marca", "Modelo", "Sector", "Altura (m)", "Azimuth (°)", "Tilt Mecánico (°)", "Tilt Eléctrico (°)", "Número de Serie");
					$cabeceras_no_validas = array();
					$cabeceras_validas = array();
					$arr_data = array();

					foreach($cell_collection as $cell)
					{
						$column = $activeSheet->getCell($cell)->getColumn();
						$row = $activeSheet->getCell($cell)->getRow();
						$data_value = $activeSheet->getCell($cell)->getValue();

						if($row == 1)
						{
							$header[$row][$column] = $data_value;

							foreach($header as $filaHeader => $infoHeader)
							{
								if(!in_array($infoHeader[$column], $cabeceras))
								{
									array_push($cabeceras_no_validas, $infoHeader[$column]);
								}
								else
								{
									array_push($cabeceras_validas, $infoHeader[$column]);
								}
							}
						}
						else
						{
							if(!isset($data_value) && $column == "A")
							{
								/*break;*/
							}
							elseif(in_array($header[1][$column], $cabeceras_no_validas))
							{
								
							}
							else
							{
								$arr_data[$row][$column] = $data_value;
							}
						}
					}

					$filas = count($arr_data);

					if($idIngresoMasivo !== FALSE)
					{
						foreach($arr_data as $fila => $infoFila)
						{
							$nombreSeparado = explode("_", $infoFila["B"]);
							$codigoNombre = $nombreSeparado[0];

							foreach($infoFila as $columna => $campo)
							{
								$ingresoMasivo["ingreso_masivo"] = $idIngresoMasivo;

								if($header[1][$columna] == "Código")
								{
									$codigo = $campo;
								}
								elseif($header[1][$columna] == "Nombre")
								{
									$nombre = $this->Ingreso_masivo_antenas_gul_model->esNombreCodigoValidos($codigo, $campo);
									$ingresoMasivo["sitio"] = $nombre["id"];
								}
								elseif($header[1][$columna] == "Marca")
								{
									$marca = $campo;
								}
								elseif($header[1][$columna] == "Modelo")
								{
									$tipo_antena_gul = $this->Ingreso_masivo_antenas_gul_model->esMarcaModeloValidos($marca, $campo);
									$ingresoMasivo["tipo_antena_gul"] = $tipo_antena_gul["id"];
								}
								elseif($header[1][$columna] == "Sector")
								{
									if($marca == "TELNET")
									{	
										$actualizarTelnet = $this->Ingreso_masivo_antenas_gul_model->existeAntenaTelnetEnSitio($nombre["id"]);
										if($actualizarTelnet !== false)
										{
											$ingresoMasivo["id_actualizar"] = $actualizarTelnet["id"];
										}
										else
										{
											$ingresoMasivo["id_actualizar"] = NULL;
										}

										$ingresoMasivo["sector"] = NULL;
										$sectores = preg_split("/[\s,\/\\\]+/", $campo);

										foreach($sectores as $key => $sector)
										{
											$info_telnet[$key]["sector"] = $sector;
										}
									}
									else
									{
										$actualizar = $this->Ingreso_masivo_antenas_gul_model->existeAntenaEnSitio($nombre["id"], $campo);
										if($actualizar !== false)
										{
											$ingresoMasivo["id_actualizar"] = $actualizar["id"];
										}
										else
										{
											$ingresoMasivo["id_actualizar"] = NULL;
										}

										$ingresoMasivo["sector"] = $campo;
									}
								}
								elseif($header[1][$columna] == "Altura (m)")
								{
									$ingresoMasivo["altura"] = $campo;
								}
								elseif($header[1][$columna] == "Azimuth (°)")
								{
									if($marca == "TELNET")
									{
										$ingresoMasivo["azimuth"] = NULL;
										$azimuths = preg_split("/[\s,\/\\\]+/", $campo);

										foreach($azimuths as $key => $sector)
										{
											$info_telnet[$key]["azimuth"] = $sector;
										}
									}
									else
									{
										$ingresoMasivo["azimuth"] = $campo;
									}
								}
								elseif($header[1][$columna] == "Tilt Mecánico (°)")
								{
									if($marca == "TELNET")
									{
										$ingresoMasivo["tilt_mecanico"] = NULL;
										$mecanicos = preg_split("/[\s,\/\\\]+/", $campo);
										
										foreach($mecanicos as $key => $sector)
										{
											$info_telnet[$key]["tilt_mecanico"] = $sector;
										}
									}
									else
									{
										$ingresoMasivo["tilt_mecanico"] = $campo;
									}
								}
								elseif($header[1][$columna] == "Tilt Eléctrico (°)")
								{
									if($marca == "TELNET")
									{
										$ingresoMasivo["tilt_electrico"] = NULL;
										$electricos = preg_split("/[\s,\/\\\]+/", $campo);
										
										foreach($electricos as $key => $sector)
										{
											$info_telnet[$key]["tilt_electrico"] = $sector;
										}
									}
									else
									{
										$ingresoMasivo["tilt_electrico"] = $campo;
									}
								}
								elseif($header[1][$columna] == "Número de Serie")
								{
									$ingresoMasivo["numero_serie"] = $campo;
								}

								($marca != "TELNET")? $info_telnet = array() : "";

								$ingresoMasivo["info_telnet"] = json_encode($info_telnet);
							}

							if($this->Ingreso_masivo_antenas_gul_model->ingresar_temporal($ingresoMasivo))
							{
								$resultado++;
							}
						}

						$resultados["mensajes"] = "Se ha enviado $resultado fila(s) para ser aprobada(s).";
						unlink("/var/www/html/uploads/inventarioGeneral/ingreso_masivo/".$post["file"]);
					}
					else
					{
						$resultados["error"] = TRUE;
						array_push($resultados["mensajes"], "La hoja no contiene información");
					}
				}
				else
				{
					$resultados["error"] = TRUE;
					array_push($resultados["mensajes"], "No se encontró la hoja llamada 'Antenas GUL'");
				}
			}
			else
			{
				echo json_encode(array(
						"error"		=> TRUE,
						"mensaje"	=> "Usted no tiene permiso para realizar esta acción."
					));
			}
		}
		else
		{
			echo json_encode(array(
					"error"		=> TRUE,
					"mensaje"	=> "Su sesión expiró."
				));
		}
	}

	public function listar_solicitudes($usuario)
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if(isset($session["logged_in"]) && $session["logged_in"] && ($session["id_usuario"] == $usuario))
		{
			$this->load->model('Ingreso_masivo_antenas_gul_model');
			$listado = $this->Ingreso_masivo_antenas_gul_model->get_solicitudes($usuario);
			echo json_encode($listado);
		}
		else
		{
			show_404();
		}
	}
}