<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usuarios_model');
		$this->load->library(array('auth', 'app', 'form_validation'));
		$this->form_validation->set_error_delimiters('', '');
	}

	public function crear()
	{
		if ( ! $this->auth->logged_in() || ! $this->auth->is_admin())
		{
			show_404();
		}

		$this->form_validation->set_rules('nombres', 'Nombres', 'required');
		$this->form_validation->set_rules('apellido_paterno', 'Apellido Paterno', 'required');
		$this->form_validation->set_rules('correo', 'Correo Electrónico', 'required|valid_email|is_unique[tb_usuarios.correo]');
		$this->form_validation->set_rules('telefono', 'Teléfono', 'trim');
		$this->form_validation->set_rules('contrasena', 'Contraseña', 'required|min_length[8]|max_length[32]|matches[confirmar_contrasena]');
		$this->form_validation->set_rules('confirmar_contrasena', 'Confirmar Contraseña', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			$data['correo'] = strtolower($this->input->post('correo'));
			$data['contrasena'] = $this->input->post('contrasena');
			$data['nombres'] = $this->input->post('nombres');
			$data['apellido_paterno'] = $this->input->post('apellido_paterno');
			$data['apellido_materno'] = $this->input->post('apellido_materno');
			$data['telefono'] = $this->input->post('telefono');
			$grupos = json_decode($this->input->post('grupos'), TRUE);
		}

		if ($this->form_validation->run() == TRUE && $this->Usuarios_model->insert($data, $grupos))
		{
			$message = $this->load->view('correos/nuevo_usuario', $data, TRUE);
			
			if ($this->app->send_email($data['correo'], 'Bienvenido al Inventario de Redes de Entel Perú', $message))
			{
				echo json_encode(array('error' => FALSE, 'messages' => 'Se ha enviado la información de la cuenta al correo del nuevo usuario.'));
			}
			else
			{
				echo json_encode(array('error' => FALSE, 'messages' => 'Se ha creado el usuario pero hubo un problema con el servidor de correos.'));
			}
		}
		else
		{
			echo json_encode(array('error' => TRUE, 'messages' => (validation_errors() ? validation_errors() : $this->app->errors())));
		}
	}

	public function listar()
	{
		if( ! $this->auth->is_admin())
		{
			show_404();
		}

		echo json_encode($this->Usuarios_model->get_users());
	}

	public function editar($id)
	{
		if ( ! $this->auth->logged_in() || ( ! $this->auth->is_admin() && ! ($this->session->userdata('id_usuario') == $id)))
		{
			show_404();
		}

		echo json_encode($this->Usuarios_model->get_user($id));
	}

	public function actualizar($id)
	{
		if ( ! $this->auth->logged_in() || ( ! $this->auth->is_admin() && ! ($this->session->userdata('id_usuario') == $id)))
		{
			show_404();
		}

		$user = $this->Usuarios_model->get_user($id);

		$this->form_validation->set_data($this->input->input_stream());
		$this->form_validation->set_rules('nombres', 'Nombres', 'required');
		$this->form_validation->set_rules('apellido_paterno', 'Apellido Paterno', 'required');
		$this->form_validation->set_rules('telefono', 'Teléfono', 'required');

		if ( ! empty($this->input->input_stream()))
		{
			if ($id != $this->input->input_stream('id_usuario'))
			{
				show_error('Solicitud inválida');
			}

			if ($this->input->input_stream('contrasena'))
			{
				$this->form_validation->set_rules('contrasena', 'Contraseña', 'required|min_length[8]|max_length[32]|matches[confirmar_contrasena]');
				$this->form_validation->set_rules('confirmar_contrasena', 'Confirmar Contraseña', 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'nombres' => $this->input->input_stream('nombres'),
					'apellido_paterno' => $this->input->input_stream('apellido_paterno'),
					'apellido_materno' => $this->input->input_stream('apellido_materno'),
					'telefono' => $this->input->input_stream('telefono')

				);

				if ($this->input->input_stream('contrasena'))
				{
					$data['contrasena'] = $this->input->input_stream('contrasena');
				}

				if ($this->auth->is_admin())
				{
					if ($this->input->input_stream('activo'))
					{
						$data['activo'] = (int) ($this->input->input_stream('activo') === 'true');
					}

					if ($this->input->input_stream('correo'))
					{
						$data['correo'] = strtolower($this->input->input_stream('correo'));
					}

					$group_data = $this->input->input_stream('grupos');
					
					if (isset($group_data) && ! empty($group_data))
					{
						$group_data = json_decode($group_data, TRUE);
						$this->Usuarios_model->remove_from_group($id);

						foreach ($group_data as $grp)
						{
							$this->Usuarios_model->add_to_group($grp, $id);
						}
					}
				}

				if ($this->Usuarios_model->update($user['id_usuario'], $data))
				{
					echo json_encode(array('error' => FALSE, 'messages' => 'Actualizado con éxito.'));
				}
				else
				{
					echo json_encode(array('error' => TRUE, 'messages' => $this->app->errors()));
				}
			}
			else
			{
				echo json_encode(array('error' => TRUE, 'messages' => validation_errors()));
			}
		}
	}

	public function reestablecerContrasena($id)
	{
		$this->load->model('Usuarios_model');

		$this->Usuarios_model->rebootPassword($id);
	}

	public function cambio_contrasena()
	{
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->library('form_validation');

		$session = $this->session->userdata();

		$confirmar_contrasena = $this->input->post("confirmar_contrasena");

		$config = array(
				array(
						'field' => 'contrasena_actual',
						'label' => 'contraseña actual',
						'rules' => 'required|callback_confirmar_contrasena['.$session['usuario'].']',
						'errors' => array(
								'required' => 'Debe ingresar la %s.'
							)
					),
				array(
						'field' => 'nueva_contrasena',
						'label' => 'nueva contraseña',
						'rules' => 'required|callback_confirmar_nueva_contrasena['.$confirmar_contrasena.']',
						'errors' => array(
								'required' => 'Debe ingresar una %s.'
							)
					)
			);

		$error = array(
				"error"		=> FALSE,
				"mensaje"	=> ""
			);

		$this->form_validation->set_rules($config);

		if($this->form_validation->run() === FALSE)
		{
			$error['error'] = TRUE;
			$error['mensaje'] = validation_errors();
			echo json_encode($error);
		}
		else
		{
			$this->load->model('Usuarios_model');
			$resp = $this->Usuarios_model->newPassword($session["id_usuario"],$confirmar_contrasena);

			if($resp)
			{
				$error['error'] = FALSE;
				$error['mensaje'] = "La contraseña se cambió exitosamente";
			}
			else
			{
				$error['error'] = TRUE;
				$error['mensaje'] = "Hubo un problema al intentar cambiar la contraseña";
			}
			echo json_encode($error);
		}
	}

	public function cambio_contrasena_administrador($usuario)
	{
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->library('form_validation');

		$session = $this->session->userdata();

		if($session["logged_in"])
		{
			$this->load->model("Autorizacion_grupos_model");

			$permiso = $this->Autorizacion_grupos_model->comprobar_autorizacion($session["id_usuario"],array(1,2));

			if($permiso)
			{
				$put = $this->input->input_stream();

				foreach ($put as $key => $value)
				{
					if( ($key == "contrasena") || ($key == "confirmar_contrasena") )
					{
						$put[$key] = rawurldecode($value);
					}
					else
					{
						unset($put[$key]);
					}
				}

				$this->load->helper("security");
				$xssTest = true;
				foreach ($put as $key => $value)
				{
					$xssTest = $xssTest && $this->security->xss_clean($value);
				}

				if(!$xssTest)
				{
					return false;
				}

				$confirmar_contrasena = $put["confirmar_contrasena"];

				$config = array(
						array(
								'field' => 'contrasena',
								'label' => 'contraseña',
								'rules' => 'required|callback_confirmar_nueva_contrasena['.$confirmar_contrasena.']',
								'errors' => array(
										'required' => 'Debe ingresar una %s.'
									)
							)
					);

				$error = array(
						"error"		=> FALSE,
						"mensaje"	=> ""
					);

				$this->form_validation->set_data($put);
				$this->form_validation->set_rules($config);

				if($this->form_validation->run() === FALSE)
				{
					$error['error'] = TRUE;
					$error['mensaje'] = validation_errors();
					echo json_encode($error);
				}
				else
				{
					$this->load->model('Usuarios_model');
					$resp = $this->Usuarios_model->newPassword($usuario,$confirmar_contrasena);

					if($resp)
					{
						$infoUsuario = $this->Usuarios_model->edit($usuario);
						$infoUsuario = $infoUsuario[0];
						$infoUsuario["confirmar_contrasena"] = $confirmar_contrasena;

						$error['error'] = FALSE;
						$error['mensaje'] = "La contraseña se cambió exitosamente.";

						$this->load->library('email');
						$this->email->from('inventario.redes@entel.net.pe', 'Inventario de Redes Entel Perú');
						$this->email->to($infoUsuario["correo"]);
						$this->email->subject('Un administrador cambió su contraseña');
						$this->email->message($this->load->view("correos/nueva_contrasena",$infoUsuario,TRUE));
						$this->email->send();
					}
					else
					{
						$error['error'] = TRUE;
						$error['mensaje'] = "Hubo un problema al intentar cambiar la contraseña.";
					}
					echo json_encode($error);
				}
			}
			else
			{
				$error['error'] = TRUE;
				$error['mensaje'] = "No tiene permisos suficientes.";
				echo json_encode($error);
			}
		}
	}

	public function apagar_usuario($id = null)
	{
		if($id === null)
		{
			return false;
		}

		$this->load->library('session');

		$session = $this->session->userdata();
		if($session["logged_in"])
		{
			$this->load->model("Autorizacion_grupos_model");

			$permiso = $this->Autorizacion_grupos_model->comprobar_autorizacion($session["id_usuario"],array(1,2));

			if($permiso)
			{
				$this->load->model('Usuarios_model');
				$post["fecha_fin"] = date("Y-m-d");
				$respuesta = $this->Usuarios_model->update($id,$post);

				if($respuesta)
				{
					$error['error'] = FALSE;
					$error['mensaje'] = "Se apagó el usuario";
					echo json_encode($error);
				}
				else
				{
					$error['error'] = TRUE;
					$error['mensaje'] = "hubo un problema";
					echo json_encode($error);
				}
			}
			else
			{
				$error['error'] = TRUE;
				$error['mensaje'] = "Permisos insuficientes";
				echo json_encode($error);
			}
		}
		else
		{
			$error['error'] = TRUE;
			$error['mensaje'] = "Expiró su sesión";
			echo json_encode($error);
		}
	}

	public function reanudar_usuario($id = null)
	{
		if($id === null)
		{
			return false;
		}

		$this->load->library('session');

		$session = $this->session->userdata();
		if($session["logged_in"])
		{
			$this->load->model("Autorizacion_grupos_model");

			$permiso = $this->Autorizacion_grupos_model->comprobar_autorizacion($session["id_usuario"],array(1,2));

			if($permiso)
			{
				$this->load->model('Usuarios_model');
				$post["fecha_fin"] = null;
				$respuesta = $this->Usuarios_model->update($id,$post);

				if($respuesta)
				{
					$error['error'] = FALSE;
					$error['mensaje'] = "Se reanudó el usuario";
					echo json_encode($error);
				}
				else
				{
					$error['error'] = TRUE;
					$error['mensaje'] = "hubo un problema";
					echo json_encode($error);
				}
			}
			else
			{
				$error['error'] = TRUE;
				$error['mensaje'] = "Permisos insuficientes";
				echo json_encode($error);
			}
		}
		else
		{
			$error['error'] = TRUE;
			$error['mensaje'] = "Expiró su sesión";
			echo json_encode($error);
		}
	}

	public function forgot_password()
    {
    	$this->load->library('form_validation');
		$info = $this->input->post();
		$info["correo"] = urldecode($info["correo"]);

        $config = array(
			array(
					'field' => 'correo',
					'rules' => 'trim|required|callback_check_valid_email|callback_check_email_exists',
					'errors' => array(
							'required' => 'Ingrese un correo electrónico',
							'check_valid_email' => 'Ingrese un correo electrónico válido',
							'check_email_exists' => 'No podemos encontrar ese correo electrónico.'
						)
					)
				);

        $this->form_validation->set_data($info);
		$this->form_validation->set_rules($config);

		$error = array(
			'error' => FALSE
			);
        
        if($this->form_validation->run() === FALSE)
        {
        	$error['error'] = TRUE;
           	$error['messages'] = validation_errors();
			echo json_encode($error);
        }
        else
        {
        	$this->load->helper('url');
        	$this->load->model('Usuarios_model');
        	$clean = $this->security->xss_clean($info["correo"]);
            $userInfo = $this->Usuarios_model->getUserInfoByEmail($clean);
			
            $token = $this->Usuarios_model->insertToken($userInfo["id_usuario"]);        
            $qstring = $this->base64url_encode($token);
            $data["link"] = base_url("reestablecer_contrasena/$qstring");
            $data["user_info"] = $userInfo;

            $this->load->library('email');
            $this->email->from('inventario.redes@entel.net.pe', 'Inventario de Redes Entel Perú');
			$this->email->to($userInfo["correo"]);
			/*$this->email->cc($userInfo["telefono"]."@mensajes.entel.pe");*/
			$this->email->subject('Reestablecimiento de contraseña');
			$this->email->message($this->load->view("correos/forgot_password",$data,TRUE));
			$this->email->send();

			echo json_encode(array('error' => FALSE, 'messages' => 'Se ha enviado un link con instrucciones a su correo, esta ventana se cerrará en 3 segundos...'));
        }
    }

    public function reset_password()
    {
    	$this->load->library('form_validation');
    	$this->load->model('Usuarios_model');
    	$info = $this->input->post();

    	$config = array(
			array(
					'field' => 'contrasena',
					'rules' => 'trim|required|min_length[5]',
					'errors' => array(
							'required' => 'Ingrese su nueva contraseña',
							'min_length' => 'La contraseña debe tener un mínimo de 5 caracteres.'
						)
					),
			array(
					'field' => 'contrasenaconf',
					'rules' => 'trim|required|matches[contrasena]',
					'errors' => array(
							'required' => 'Ingrese la contraseña nuevamente.',
							'matches' => 'Las contraseñas no coinciden.'
						)
					)
				);

    	$this->form_validation->set_data($info);
		$this->form_validation->set_rules($config);

		$error = array(
			'error' => FALSE
			);
        
        $user_info = $this->Usuarios_model->isTokenValid($info["token"]);
        
        if ($this->form_validation->run() === FALSE)
        {   
        	$error['error'] = validation_errors();
			echo json_encode($error);
        }
        else
        {
        	$this->load->model('Tokens_model');
        	$this->Tokens_model->delete_where(array("usuario" => $user_info["id_usuario"]));
            $resp = $this->Usuarios_model->newPassword($user_info["id_usuario"],$info["contrasenaconf"]);
            return $resp;      
        }
    }

    function check_email_exists($correo)
    {
    	$this->load->model('Usuarios_model');
    	$existe = $this->Usuarios_model->getUserInfoByEmail($correo);
    	return ( ($existe != false)? true : false );
    }

    function check_duplicate_email($correo)
    {
    	$this->load->model('Usuarios_model');
    	$existe = $this->Usuarios_model->getUserInfoByEmail($correo);
    	return ( ($existe != false)? false : true );
    }

    function base64url_encode($data) 
    { 
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
	}

	function base64url_decode($data) 
	{ 
	    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}

	function check_valid_email($correo){
		if(!filter_var($correo,FILTER_VALIDATE_EMAIL))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	function confirmar_contrasena($contrasena,$usuario){
		$this->load->model('Usuarios_model');

		$user = $this->Usuarios_model->confirm_user($usuario);
		
		if(sizeof($user))
		{
			if(password_verify($contrasena,$user['0']['contrasena']))
			{
				return TRUE;
			}
			else
			{
				$this->form_validation->set_message('confirmar_contrasena', 'La contraseña no es válida');
				return FALSE;
			}
		}
		else
		{
			$this->form_validation->set_message('confirmar_contrasena', 'Hubo un problema');
			return FALSE;
		}
	}

	function confirmar_nueva_contrasena($nueva_contrasena,$confirmar_contrasena){
		if($nueva_contrasena == $confirmar_contrasena)
		{
			$mayuscula	= preg_match("@[A-Z]@",$nueva_contrasena);
			$minuscula	= preg_match("@[a-z]@",$nueva_contrasena);
			$numero		= preg_match("@[0-9]@",$nueva_contrasena);
			$simbolos	= preg_match("@[\W_]@",$nueva_contrasena);
			$largo		= (strlen($nueva_contrasena) >= 8);

			if(!$mayuscula)
			{
				$this->form_validation->set_message('confirmar_nueva_contrasena', 'Considerar al menos una letra mayúscula.');
				return FALSE;
			}
			else
			{
				if(!$minuscula)
				{
					$this->form_validation->set_message('confirmar_nueva_contrasena', 'Considerar al menos una letra minúscula.');
					return FALSE;
				}
				else
				{
					if(!$numero)
					{
						$this->form_validation->set_message('confirmar_nueva_contrasena', 'Considerar al menos un número.');
						return FALSE;
					}
					else
					{
						if(!$simbolos)
						{
							$this->form_validation->set_message('confirmar_nueva_contrasena', 'Considerar al menos un símbolo.');
							return FALSE;
						}
						else
						{
							if(!$largo)
							{
								$this->form_validation->set_message('confirmar_nueva_contrasena', 'La contraseña debe tener al menos 8 caracteres.');
								return FALSE;
							}
							else
							{
								return TRUE;
							}
						}
					}
				}
			}
		}
		else
		{
			$this->form_validation->set_message('confirmar_nueva_contrasena', 'La contraseña de confirmación es diferente a la nueva contraseña.');
			return FALSE;
		}
	}
}