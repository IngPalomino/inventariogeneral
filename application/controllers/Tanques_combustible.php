<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tanques_combustible extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 11;
	}

	public function listar()
	{
		$this->load->model('Tanques_combustible_model');

		$listado = $this->Tanques_combustible_model->get_tanques_combustible();

		echo json_encode($listado);
	}

	public function listar_sitio($id){
		$this->load->model('Tanques_combustible_model');

		$listado = $this->Tanques_combustible_model->get_tanques_combustible_sitio($id);

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Tanques_combustible_model');

		$tanque_combustible = $this->Tanques_combustible_model->edit($id);

		echo json_encode($tanque_combustible);
	}

	public function actualizar($id)
	{
		$this->load->model('Tanques_combustible_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$tanque_combustible = $this->Tanques_combustible_model->edit($id);

		if($this->Tanques_combustible_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Tanque de combustible",
					"tabla"			=> "tb_tanques_combustible",
					"id"			=> $id,
					"datos viejos"	=> $tanque_combustible,
					"datos nuevos"	=> $this->Tanques_combustible_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_tanques_combustible_model');
				$this->Logs_tanques_combustible_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$evento["sitio"] = $this->Tanques_combustible_model->get_sitio_tanque($id);
				$evento["link"] = "ver/sitios/energia/tanques_combustible/".$evento["sitio"]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('Tanques_combustible_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		$info["sitio"] = $id;

		$respuesta = $this->Tanques_combustible_model->insert($info);

		if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Creación de Tanque de combustible",
					"tabla"		=> "tb_tanques_combustible"
				);

			$this->load->model('Logs_tanques_combustible_model');
			$this->Logs_tanques_combustible_model->ingreso_de_datos($session['id_usuario'],$evento);

			$this->load->model('Status_site_model');
			$evento["link"] = "ver/sitios/energia/tanques_combustible/".$id;
			$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($id);
			$evento["elemento"] = $this->Tanques_combustible_model->edit($respuesta["last_id"]);
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $id);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Tanques_combustible_model');

		$info = $this->input->input_stream();

		$info_tanque['eliminado'] = 1;

		if($this->Tanques_combustible_model->update($id, $info_tanque))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Tanque de combustible",
					"tabla"			=> "tb_tanques_combustible",
					"id"			=> $id
				);

			$this->load->model('Logs_tanques_combustible_model');
			$this->Logs_tanques_combustible_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$evento["sitio"] = $this->Tanques_combustible_model->get_sitio_tanque($id);
			$evento["link"] = "ver/sitios/energia/tanques_combustible/".$evento["sitio"]["id"];
			$evento["elemento"] = $this->Tanques_combustible_model->edit($id);
			$evento["comentario"] = $info["comentario"];
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function almacenar($id)
	{
		$this->load->model('Tanques_combustible_model');

		$info = $this->input->input_stream();
		$info_tanque["eliminado"] = 1;
		
		if($this->Tanques_combustible_model->update($id, $info_tanque))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Almacenamiento de Tanque de Combustible",
					"tabla"			=> "tb_tanques_combustible",
					"id"			=> $id
				);

			$this->load->model('Logs_tanques_combustible_model');
			$this->Logs_tanques_combustible_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$this->load->model('Bodega_tanques_combustible_model');
			unset($info['comentario']);

			if($this->Bodega_tanques_combustible_model->insert($info))
			{
				$evento = array(
					"accion"		=> "Almacenamiento de Tanque de Combustible",
					"tabla"			=> "tb_bodega_tanques_combustible"
				);

				$this->load->model('Logs_bodega_tanques_combustible_model');
				$this->Logs_bodega_tanques_combustible_model->ingreso_de_datos($session['id_usuario'],$evento);
			}
		}
	}
}