<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Idus extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
	}
	
	public function listar()
	{
		$this->load->model('Idus_model');

		$listado = $this->Idus_model->get_idus();

		echo json_encode($listado);
	}

	public function listar_sitio($id) 
	{
		$this->load->model('Idus_model');

		$listado = $this->Idus_model->get_idus_sitio($id);

		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('Idus_model');

		$idu = $this->Idus_model->edit($id);

		echo json_encode($idu);
	}

	public function actualizar($id)
	{
		$this->load->model('Idus_model');
		$idu = $this->Idus_model->edit($id);

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();
		
		$config = array(
			array(
					'field' => 'ne_id',
					'rules' => 'trim|callback_check_duplicate_neid',
					'errors' => array(
							'check_duplicate_neid' => 'El NE ID ya existe.'
						)
					)
				);				
		$this->form_validation->set_data($info);
		$this->form_validation->set_rules($config);
		
		$error = array(
			'error' => FALSE
			);
			
		if($this->form_validation->run() === FALSE)
		{
			$error['error'] = validation_errors();
			echo json_encode($error);
		}
		else
		{
			/*$idu = $this->Idus_model->edit($id);*/

			if($this->Idus_model->update($id, $info))
			{
				$this->load->library('session');
				$session = $this->session->userdata();

				$evento = array(
						"accion"		=> "Actualización de IDU",
						"tabla"			=> "tb_idus",
						"id"			=> $id,
						"datos viejos"	=> $idu,
						"datos nuevos"	=> $this->Idus_model->edit($id)
					);

				if($evento["datos viejos"] != $evento["datos nuevos"])
				{
					$this->load->model('Logs_idus_model');
					$this->Logs_idus_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

					$evento["sitio"] = $this->Idus_model->get_sitio_idu($id);
					$evento["link"] = "ver/sitios/infraestructura/idus/".$evento["sitio"]["id"];
					$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
					$cc = $this->Notificaciones_model->get_correos_grupos(4, $evento["sitio"]["id"]);
					$cc = $this->custom->clean_cc($session["usuario"], $cc);
					$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
				}
			}
		}
	}

	public function crear_sitio($id)
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if( isset($session["logged_in"]) && $session["logged_in"] && ($session["auth"] <= 2))
		{
			$this->load->model('Idus_model');

			$this->load->helper('form');
			$this->load->library('form_validation');
			
			$info = array();
			$info = $this->input->post();
			$info["sitio"] = $id;
			
			$config = array(
				array(
						'field' => 'ne_id',
						'rules' => 'trim|required|callback_check_duplicate_neid',
						'errors' => array(
								'check_duplicate_neid' => 'El NE ID ya existe.'
							)
						)
					);
					
			$this->form_validation->set_rules($config);
			
			$error = array(
				'error' => FALSE
				);
				
			if($this->form_validation->run() === FALSE)
			{
				$error['error'] = validation_errors();
				echo json_encode($error);
			}
			else
			{
				$respuesta = $this->Idus_model->insert($info);
				if($respuesta["resp"])
				{
					$this->load->library('session');
					$session = $this->session->userdata();

					$evento = array(
							"accion"	=> "Creación de IDU",
							"tabla"		=> "tb_idus"
						);

					$this->load->model('Logs_idus_model');
					$this->Logs_idus_model->ingreso_de_datos($session['id_usuario'],$evento);

					$this->load->model('Status_site_model');
					$evento["link"] = "ver/sitios/infraestructura/idus/".$id;
					$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($id);
					$evento["elemento"] = $this->Idus_model->edit($respuesta["last_id"]);
					$cc = $this->Notificaciones_model->get_correos_grupos(4, $id);
					$cc = $this->custom->clean_cc($session["usuario"], $cc);
					$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
				}
			}
		}
		else
		{
			echo "No tiene permiso para acceder esta página.";
			return FALSE;
		}
	}

	public function eliminar($id)
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if( isset($session["logged_in"]) && $session["logged_in"] && ($session["auth"] <= 2))
		{
			$this->load->model('Idus_model');

			$info = $this->input->input_stream();

			$info_idu['eliminado'] = 1;

			if($this->Idus_model->update($id, $info_idu))
			{
				$this->load->library('session');
				$session = $this->session->userdata();

				$evento = array(
						"accion"		=> "Eliminación de IDU",
						"tabla"			=> "tb_idus",
						"id"			=> $id
					);

				$this->load->model('Logs_idus_model');
				$this->Logs_idus_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

				/* Enviar correo */
				$evento["sitio"] = $this->Idus_model->get_sitio_idu($id);
				$evento["link"] = "ver/sitios/infraestructura/idus/".$evento["sitio"]["id"];
				$evento["elemento"] = $this->Idus_model->edit($id);
				$evento["comentario"] = $info["comentario"];
				$cc = $this->Notificaciones_model->get_correos_grupos(4, $evento["sitio"]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
				/* Fin de envio de correo */

				$enlaces_idu = $this->Idus_model->get_enlaces_idu($id);

				$info_equipos["eliminado"] = 1;
				$this->load->model('Antenas_mw_model');
				$this->load->model('Odus_model');

				foreach ($enlaces_idu as $enlace)
				{
					$this->Antenas_mw_model->update_where(array("enlace" => $enlace["id"]),$info_equipos);
					$this->Odus_model->update_where(array("enlace" => $enlace["id"]),$info_equipos);
				}

				$this->load->model('Enlaces_model');
				$info_enlace["eliminado"] = 1;
				if($this->Enlaces_model->update_or_where(array("idu_1" => $id, "idu_2" => $id),$info_enlace))
				{
					$evento = array(
							"accion"		=> "Eliminación de Enlace por eliminación de IDU",
							"tabla"			=> "tb_enlaces",
							"idu"			=> $id
						);

					$this->load->model('Logs_enlaces_model');
					$this->Logs_enlaces_model->actualizacion_de_datos($session['id_usuario'],"",$evento,$info['comentario']);
				}
			}
		}
		else
		{
			echo "No tiene permiso para acceder esta página.";
			return FALSE;
		}
	}
	
	function check_duplicate_neid($neid)
	{
		$this->load->model('Idus_model');
		$count = $this->Idus_model->check_duplicate_neid($neid);

		if($count > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	function obtener_sitio_con_idu($idu){
		$this->load->model('Idus_model');
		$listado = $this->Idus_model->get_sitio_idu($idu);
		echo json_encode($listado);

	}

    

    function get_sitio_con_neid(){
        	
  		$post = $this->input->post();
  		$ne_id = $post["ne_id"];
		$this->load->model('Idus_model');
		$listado = $this->Idus_model->get_sitio_con_neid($ne_id);
		echo json_encode($listado);
	}  


}