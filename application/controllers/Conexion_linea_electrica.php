<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conexion_linea_electrica extends CI_Controller {

	public function listar()
	{
		$this->load->model('Conexion_linea_electrica_model');

		$listado = $this->Conexion_linea_electrica_model->get_types();

		echo json_encode($listado);
	}

	public function id_tipo()
	{
		$this->load->model('Conexion_linea_electrica_model');

		$info = array();
		$info = $this->input->post();

		$tipo = $this->Conexion_linea_electrica_model->get_type($info);

		echo json_encode($tipo);
	}

	public function editar($id)
	{
		$this->load->model('Conexion_linea_electrica_model');

		$antena = $this->Conexion_linea_electrica_model->edit($id);

		echo json_encode($antena);
	}

	public function actualizar($id)
	{
		$this->load->model('Conexion_linea_electrica_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$this->Conexion_linea_electrica_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Conexion_linea_electrica_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Conexion_linea_electrica_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Conexion_linea_electrica_model');

		$this->Conexion_linea_electrica_model->delete($id);
	}
}