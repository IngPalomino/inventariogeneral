<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_proyecto extends CI_Controller {

	public function listar()
	{
		$this->load->model('Tipo_proyecto_model');

		$listado = $this->Tipo_proyecto_model->get_proyecto();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Tipo_proyecto_model');

		$tipo_torre = $this->Tipo_proyecto_model->edit($id);

		echo json_encode($tipo_torre);
	}

	public function actualizar($id)
	{
		$this->load->model('Tipo_proyecto_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['Tipo_torre'] = $this->input->input_stream('Tipo_torre');

		$this->Tipo_proyecto_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Tipo_proyecto_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['Tipo_torre'] = $this->input->post('Tipo_torre');

		$this->Tipo_proyecto_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Tipo_proyecto_model');

		$this->Tipo_proyecto_model->delete($id);
	}
}