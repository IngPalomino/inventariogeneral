<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarjetas extends CI_Controller {
	
	public function listar()
	{
		$this->load->model('Tarjetas_model');
		$listado = $this->Tarjetas_model->get_tarjetas();
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Tarjetas_model');
		$listado = $this->Tarjetas_model->get_tarjetas_sitio($id);
		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('Tarjetas_model');
		$tarjeta = $this->Tarjetas_model->edit($id);
		echo json_encode($tarjeta);
	}

	public function actualizar()
	{
		$this->load->model('Tarjetas_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = $this->input->input_stream();
		
		$info["data"] = json_decode($info["data"],true);

		if(count($info["data"]) > 0)
		{
			foreach($info["data"] as $tarjeta)
			{
				$info_tarjeta["nefdn"] = $tarjeta["nefdn"];
				$info_tarjeta["board_name"] = $tarjeta["board_name"];
				$info_tarjeta["board_type"] = $tarjeta["board_type"];
				$info_tarjeta["date_manufacture"] = str_replace("/","-",$tarjeta["date_manufacture"]);
				$info_tarjeta["date_manufacture"] = $info_tarjeta["date_manufacture"]? date("Y-m-d", strtotime($info_tarjeta["date_manufacture"])) : NULL;
				$info_tarjeta["bom_code"] = $tarjeta["bom_code"];
				$info_tarjeta["manufacturer_data"] = $tarjeta["manufacturer_data"];
				$info_tarjeta["serial_number"] = $tarjeta["serial_number"];
				$info_tarjeta["software_version"] = $tarjeta["software_version"];
				$info_tarjeta["unit_position"] = $tarjeta["unit_position"];
				$info_tarjeta["vendor_name"] = $tarjeta["vendor_name"];
				$info_tarjeta["vendor_unit_family_type"] = $tarjeta["vendor_unit_family_type"];
				$info_tarjeta["hardware_version"] = $tarjeta["hardware_version"];
				$this->Tarjetas_model->update($tarjeta["id"],$info_tarjeta);
			}
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('Tarjetas_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["data"] = json_decode($info["data"],true);

		if(count($info["data"]) > 0)
		{
			foreach($info["data"] as $tarjeta)
			{
				$tarjeta["sitio"] = $id;
				$tarjeta["date_manufacture"] = str_replace("/","-",$tarjeta["date_manufacture"]);
				$tarjeta["date_manufacture"] = $tarjeta["date_manufacture"]? date("Y-m-d", strtotime($tarjeta["date_manufacture"])) : NULL;
				$this->Tarjetas_model->insert($tarjeta);
			}
		}
	}

	public function eliminar()
	{
		$this->load->model('Tarjetas_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = array();
		$info = $this->input->input_stream();
		
		$info["data"] = json_decode($info["data"],true);
		
		foreach($info["data"] as $tarjeta)
		{
			$this->Tarjetas_model->delete($tarjeta);
		}
	}
}