<?php
class Validaciones extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		/*$this->load->model('users_model');*/
	}

	public function listar()
	{

	}

	public function validar()
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if($session['logged_in'] && ($session['auth'] <= 2))
		{
			$datos = $this->input->post();
			$datos["usuario"] = $session["id_usuario"];

			$this->load->model("Ingreso_masivo_model");

			$respuesta = $this->Ingreso_masivo_model->validar($datos);

			$modelAfectada = ucfirst($datos["elemento"])."_model";
			$this->load->model($modelAfectada,"modelAfectada");

			$data = array(
				$datos["campo_ingreso_masivo"] => $respuesta[$datos["campo_ingreso_masivo"]]
				);

			if($this->modelAfectada->update($datos["id_elemento"],$data))
			{
				$this->load->library('session');
				$session = $this->session->userdata();

				$evento = array(
						"accion"	=> "Actualización de POP",
						"tabla"		=> "tb_status_site",
						"id"		=> $datos["id_elemento"]
					);

				$this->load->model('Logs_model');
				$this->Logs_model->actualizacion_de_datos($session['id_usuario'],$evento);

				$this->Ingreso_masivo_model->validar_estado($datos);

				echo TRUE;
			}
		}
		else
		{
			redirect(base_url());
		}
	}

	public function cancelar($id)
	{
		$this->load->library('session');
		$session = $this->session->userdata();

		if($session['logged_in'] && ($session['auth'] <= 2))
		{
			$datos = $this->input->input_stream();
			$datos["usuario"] = $session["id_usuario"];
			$datos["id_ingreso_masivo"] = $id;

			$this->load->model("Ingreso_masivo_model");

			$respuesta = $this->Ingreso_masivo_model->cancelar($datos);

			echo $respuesta;
		}
		else
		{
			redirect(base_url());
		}
	}
}