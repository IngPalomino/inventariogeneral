<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluaciones_avance_evaluacion extends CI_Controller {
	public function listar()
	{
		$this->load->model('Evaluaciones_avance_evaluacion_model');

		$listado = $this->Evaluaciones_avance_evaluacion_model->listar();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Evaluaciones_avance_evaluacion_model');

		$listado = $this->Evaluaciones_avance_evaluacion_model->editar($id);

		echo json_encode($listado);
	}

	public function actualizar($id)
	{

	}

	public function insertar()
	{
		/*$this->load->model('Evaluaciones_avance_evaluacion_model');

		$info = array();
		$info = $this->input->post();
		$info_evaluacion = array();
		$info_evaluacion["nombre"] = $info["nombre"];
		$info_evaluacion["periodo"] = $info["periodo"];
		$info_evaluacion["contrata"] = $info["contrata"];

		$respuesta = $this->Evaluaciones_avance_evaluacion_model->insertar($info_evaluacion);

		$info['contenido'] = json_decode($info['contenido']);

		if($respuesta['resp'])
		{
			$this->load->model('Evaluaciones_criterio_model');

			foreach ($info['contenido'] as $criterio)
			{
				$info_criterio = array();
				$info_criterio['evaluacion'] = $respuesta['last_id'];
				$info_criterio['nombre'] = $criterio->criterio;
				$info_criterio['peso'] = $criterio->peso;

				$respCriterio = $this->Evaluaciones_criterio_model->insertar($info_criterio);

				if($respCriterio['resp'])
				{
					$this->load->model('Evaluaciones_sub_criterio_model');

					foreach ($criterio->preguntas as $pregunta)
					{
						$info_subcriterio = array();
						$info_subcriterio['criterio'] = $respCriterio['last_id'];
						$info_subcriterio['nombre'] = $pregunta->sub_criterio;
						$info_subcriterio['pregunta'] = $pregunta->pregunta;
						$info_subcriterio['nota_informativa'] = $pregunta->nota_informativa;

						$respSubCriterio = $this->Evaluaciones_sub_criterio_model->insertar($info_subcriterio);
					}
				}
			}

			$this->load->model('Evaluaciones_avance_evaluacion');

			$this->Evaluaciones_avance_evaluacion->nueva_evaluacion($respuesta['last_id'],$info_evaluacion['contrata']);

			
		}*/
	}

	public function borrar()
	{
		
	}

	public function nueva_evaluacion($id_eva,$contratas)
	{
		if(isset($id_eva) && isset($contratas))
		{
			$this->load->model('Evaluaciones_avance_evaluacion_model');

			foreach ($contratas as $key => $value)
			{
				$this->Evaluaciones_avance_evaluacion_model->nueva_evaluacion($id_eva,$value);
			}
		}
	}

	public function listar_pendientes($id)
	{
		$this->load->model('Evaluaciones_avance_evaluacion_model');

		$listado = $this->Evaluaciones_avance_evaluacion_model->listar_pendientes($id);

		echo json_encode($listado);
	}

	public function listar_evaluados($id)
	{
		$this->load->model('Evaluaciones_avance_evaluacion_model');

		$listado = $this->Evaluaciones_avance_evaluacion_model->listar_evaluados($id);

		echo json_encode($listado);
	}

	public function info_evaluado($evaPeriodo,$tecnico)
	{
		$this->load->model('Evaluaciones_avance_evaluacion_model');

		$listado = $this->Evaluaciones_avance_evaluacion_model->info_evaluado($evaPeriodo,$tecnico);

		echo json_encode($listado);
	}

	public function registrar_evaluacion($id)
	{
		$this->load->library('session');
		$data['session'] = $this->session->userdata();

		if($data['session']['logged_in'] && ($data['session']['evaluador'] == 1) )
		{
			$this->load->model('Evaluaciones_avance_evaluacion_model');

			$data = $this->input->input_stream();

			$respuesta = $this->Evaluaciones_avance_evaluacion_model->registrar_evaluacion($id,$data);

			if($respuesta)
			{
				$pendientes = $this->Evaluaciones_avance_evaluacion_model->contar_evaluaciones_pendientes($data["evaluacion"],$data["evaluacion_periodo"]);

				if(sizeof($pendientes) == 0)
				{
					$this->Evaluaciones_avance_evaluacion_model->finalizar_evaluacion($data["evaluacion_periodo"]);
				}
			}

			echo json_encode($respuesta);
		}
	}
}

?>