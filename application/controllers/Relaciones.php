<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relaciones extends CI_Controller {
	public function listar()
	{
		$this->load->model('Relaciones_model');

		$listado = $this->Relaciones_model->get_joins();

		echo json_encode($listado);
	}
	
	public function listar_id($id)
	{
		$this->load->model('Relaciones_model');

		$listado = $this->Relaciones_model->get_joins_id($id);

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Relaciones_model');

		$relacion = $this->Relaciones_model->edit($id);

		echo json_encode($relacion);
	}

	public function actualizar($id)
	{
		$this->load->model('Relaciones_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$this->Relaciones_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Relaciones_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = $this->input->post();

		$this->Relaciones_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Relaciones_model');

		$this->Relaciones_model->delete($id);
	}
}