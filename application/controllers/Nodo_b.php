<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nodo_b extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	
	/*public function listar_tarjetas_sitio()
	{
		$data = array();
		$file = "./application/uploads/electronica/electronica_boards_nodes_m2000.xlsx";
		$this->load->library("excel");
		$objPHPExcel = PHPExcel_IOFactory::load($file);
		$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
		
		$fila = array();
		$hoja = array();

		$row1 = 2; $row2 = 2;
		$ultimo = count($cell_collection);
		$i = 0;
		
		foreach($cell_collection as $cell)
		{
			$i++;
			$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
			$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
			$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
			
			if($row > 1)
			{
				$row2 = $row;
				if($row1 != $row2)
				{
					$row1 = $row2;
					array_push($hoja,$fila);
				}
				
				$fila[$column] = $data_value;
				
				if($i == $ultimo)
				{
					array_push($hoja,$fila);
				}
			}
		}
		
		echo json_encode($hoja);
	}*/
	
	public function listar_tarjetas_sitio($sitio)
	{
		$data = array();
		$file = "./application/uploads/electronica/electronica_boards_nodes_m2000.xlsx";
		$this->load->library("excel");
		$objPHPExcel = PHPExcel_IOFactory::load($file);
		$hoja = $objPHPExcel->getActiveSheet();
		$highestRow = $hoja->getHighestRow();
		$highestColumn = $hoja->getHighestColumn();
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		
		for($row = 2; $row <= $highestRow; $row++)
		{
			$fila = new stdClass();
			$nombre = 1;
			
			for($col = 0; $col <= $highestColumnIndex; $col++)
			{
				$fila->$col = $hoja->getCellByColumnAndRow($col, $row)->getValue();
			}
			
			if($fila->$nombre == $sitio)
			{
				array_push($data,$fila);
			}
		}
		
		echo json_encode($data);
	}
}