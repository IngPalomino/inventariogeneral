<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitud_instalacion_antenas extends CI_Controller
{
	public function __construct()
	{  // private $modulo;
		parent::__construct();
		$this->load->library('auth');
		$this->load->model('Solicitud_aprobar_antenas_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));


		$this->load->library('session');		
		$this->load->model('Notificaciones_model');
		$this->load->model('Antenas_mw_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->modulo = 5;
		
	}

	public function listar_solicitudes()
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
		   echo json_encode($this->Solicitud_aprobar_antenas_model->get_solicitudes());
			}
			else
			{
			 echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
		
	}

	public function listar_elemento($elemento)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				echo json_encode($this->Solicitud_aprobar_antenas_model->get_elemento($elemento));
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

   public function listar_elemento_mw($elemento)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				echo json_encode($this->Solicitud_aprobar_antenas_model->get_elemento_mw($elemento));
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}



	public function listar_filas_pendientes($elemento)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
			echo json_encode($this->Solicitud_aprobar_antenas_model->get_filas_pendientes($elemento));
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}

	public function listar_filas_pendientes_mw($elemento)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				echo json_encode($this->Solicitud_aprobar_antenas_model->get_filas_pendientes_mw($elemento));
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}


	public function listar_filas_reservado($elemento)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				echo json_encode($this->Solicitud_aprobar_antenas_model->get_filas_reservado($elemento));
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}


	public function listar_filas_reservado_mw($elemento)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				echo json_encode($this->Solicitud_aprobar_antenas_model->get_filas_reservado_mw($elemento));
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}


	public function listar_filas_instalado($elemento)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				echo json_encode($this->Solicitud_aprobar_antenas_model->get_filas_instalado($elemento));
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}



	public function listar_filas_instalado_mw($elemento)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
				echo json_encode($this->Solicitud_aprobar_antenas_model->get_filas_instalado_mw($elemento));
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
		echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}





	// public function reservar_espacio_antenas_gul($id)
	// {
	// 	if ($this->auth->logged_in())
	// 	{
	// 		if ($this->auth->is_admin())
	// 		{
	// 			$post = $this->input->post();




	// 			$fila["estado"] = 2;
	// 			$this->load->model('Ingreso_masivo_antenas_gul_model');

	// 			if($this->Ingreso_masivo_antenas_gul_model->update($id, $fila))
	// 			{
	// 				$this->load->model('Antenas_gul_model');

	// 				$info["tipo_antena_gul"] = $post["tipo_antena_gul"];
	// 				$info["numero_serie"] = $post["numero_serie"];
	// 				$info["sector"] = $post["sector"];
	// 				$info["altura"] = $post["altura"];
	// 				$info["azimuth"] = $post["azimuth"];
	// 				$info["tilt_electrico"] = $post["tilt_electrico"];
	// 				$info["tilt_mecanico"] = $post["tilt_mecanico"];
	// 				$info["info_telnet"] = $post["info_telnet"];

	// 				$info = $this->clean_empty($info);

	// 				if($post["id_actualizar"] != "null")
	// 				{
	// 					$this->Antenas_gul_model->update($post["id_actualizar"], $info);
	// 				}
	// 				else
	// 				{
	// 					$info["sitio"] = $post["sitio"];
	// 					$this->Antenas_gul_model->insert($info);
	// 				}
	// 			}
	// 		}
	// 		else
	// 		{
	// 			echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
	// 		}
	// 	}
	// 	else
	// 	{
	// 		echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
	// 	}
	// }



public function  update_filas_pendiente_reservado(){
       $post = $this->input->post(); 
       $ids = explode(",", $post["ids"]);      
	   $datos = array("estado"=>1);
       $this->Solicitud_aprobar_antenas_model->update_filas_pendiente_reservado($ids,$datos);
}

public function  update_filas_pendiente_reservado_rechazo(){
       $post = $this->input->post(); 
       $ids = explode(",", $post["ids"]);      
	   $datos = array("estado"=>1,"rechazado"=>1);
       $this->Solicitud_aprobar_antenas_model->update_filas_pendiente_reservado_rechazo($ids,$datos);
}





public function  update_filas_reservado_instalado(){
       $post = $this->input->post(); 
       $ids = explode(",", $post["ids"]);      
	   $datos = array("estado"=>2);
       $this->Solicitud_aprobar_antenas_model->update_filas_reservado_instalado($ids,$datos);
      
       //extraendo-data 
        $info = array();
        foreach ($ids as $key => $id) {

		$info = $this->Solicitud_aprobar_antenas_model->get_filas_instalado_aprobado($id);
        $this->load->model('Antenas_gul_model');         
        //echo json_encode($info);      
        $info_antena['tipo_antena_gul'] = $info[0]['tipo_antena_gul']['id'];		
		$info_antena['numero_serie'] = $info[0]['numero_serie'];
		$info_antena['sector'] = $info[0]['sector'];
		$info_antena['altura'] = $info[0]['altura'];
		$info_antena['azimuth'] = $info[0]['azimuth'];
		$info_antena['tilt_electrico'] = $info[0]['tilt_electrico'];
		$info_antena['tilt_mecanico'] = $info[0]['tilt_mecanico'];
		$info_antena["info_telnet"] = $info[0]["info_telnet"];
		$info_antena['sitio'] = $info[0]["sitio"];
		$idsitio = $info[0]["sitio"];
		//var_dump($info_antena);
        //exit;	
		$respuesta = $this->Antenas_gul_model->insert($info_antena);        

        if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			$evento = array(
					"accion"	=> "Creación de Antena GUL",
					"tabla"		=> "tb_antenas_gul"
				);
			$this->load->model('Logs_antenas_gul_model');
			$this->Logs_antenas_gul_model->ingreso_de_datos($session['id_usuario'],$evento);
            //correo
			$this->load->model('Status_site_model');
			$this->load->model('Notificaciones_model');			
			$evento["link"] = "ver/sitios/infraestructura/antenas/".$idsitio;
			$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($idsitio);
			$evento["elemento"] = $this->Antenas_gul_model->edit($respuesta["last_id"]);
			$this->modulo = 3;
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $idsitio);
			
            //Usuario Solicitante

			$data_antena_log = array();
			$data_antena_log = $this->Logs_antenas_gul_model->get_logs_antena($id);
			$id_usuario_solicitante = $data_antena_log[0]["usuario"];
			$this->load->model('Usuarios_model');
            $data_solicitante = $this->Usuarios_model->get_user($id_usuario_solicitante);        
		    $evento["sitio"]["nombre_solicitante"] = $data_solicitante["nombres"];
            $evento["sitio"]["apellido_solicitante"] = $data_solicitante["apellido_paterno"];

            //Usuario Solicitante	

			$cc = $this->custom->clean_cc($session["usuario"], $cc);		   
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);			
		}
	    }
}

public function  update_filas_reservado_instalado_rechazo(){
       $post = $this->input->post(); 
       $ids = explode(",", $post["ids"]);      
	   $datos = array("estado"=>2,"rechazado"=>1);
       $this->Solicitud_aprobar_antenas_model->update_filas_reservado_instalado_rechazo($ids,$datos);
}



public function crear_sitio($id){
		$this->load->model('Ingreso_masivo_antenas_gul_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info_antena['tipo_antena_gul'] = $info['tipo_antena_gul'];
		$info_antena['numero_serie'] = $info['numero_serie'];
		$info_antena['sector'] = $info['sector'];
		$info_antena['altura'] = $info['altura'];
		$info_antena['azimuth'] = $info['azimuth'];
		$info_antena['tilt_electrico'] = $info['tilt_electrico'];
		$info_antena['tilt_mecanico'] = $info['tilt_mecanico'];
		$info_antena["info_telnet"] = $info["info_telnet"];
		$info_antena['sitio'] = $id;
		
		$respuesta = $this->Ingreso_masivo_antenas_gul_model->insert($info_antena);

	}


	protected function clean_empty($info_array)
	{
		foreach($info_array as $key => $value)
		{
			if($value == "" || $value == NULL || $value == "null")
			{
				array_splice($info_array, $key, 1);
			}
		}

		return $info_array;
	}



    public function solicitudes_listar_filas_mw($elemento)
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->is_admin())
			{
		echo json_encode($this->Solicitud_aprobar_antenas_model->get_solicitudes_listar_filas_mw($elemento));
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
	}


     // microondas
	    public function solicitudes_mw_listar_all()
		{
			if ($this->auth->logged_in())
			{
				if ($this->auth->is_admin())
				{
		    echo json_encode($this->Solicitud_aprobar_antenas_model->get_solicitudes_mw_listar_all());
				}
				else
				{
					echo json_encode(array("error" => TRUE, "mensaje" => "Usted no tiene permiso para realizar esta acción."));
				}
			}
			else
			{
				echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
			}

			//echo "test";
		}

//------Steps microondas 
public function  update_filas_pendiente_reservado_mw(){
       $post = $this->input->post(); 
       $ids = explode(",", $post["ids"]);
       $datos = array("estado_aprobacion"=>2);
       $this->Solicitud_aprobar_antenas_model->update_filas_pendiente_reservado_mw($ids,$datos);
}

public function  update_filas_pendiente_reservado_rechazo_mw(){
       $post = $this->input->post(); 
       $ids = explode(",", $post["ids"]);      
	   $datos = array("estado_aprobacion"=>2,"rechazado"=>1);
       $this->Solicitud_aprobar_antenas_model->update_filas_pendiente_reservado_rechazo_mw($ids,$datos);
}

public function  update_filas_reservado_instalado_mw(){
       $post = $this->input->post(); 
       $ids = explode(",", $post["ids"]);      
	   $datos = array("estado_aprobacion"=>3);
       $respuesta = $this->Solicitud_aprobar_antenas_model->update_filas_reservado_instalado_mw($ids,$datos);


 foreach ($ids as $key => $id) {
               $id = str_replace("   ng-change", "", $id);
  		       $evento = array(
					"accion"	=> "Creación de Antena Microondas en un Enlace",
					"tabla"		=> "tb_antenas_mw_enlaces"
				);
			$session = $this->session->userdata();

			$this->load->model('Logs_antenas_mw_model');
			$this->Logs_antenas_mw_model->ingreso_de_datos($session['id_usuario'],$evento);

			$evento["sitio"] = $this->Antenas_mw_model->get_sitio_antena($id);
			$evento["elemento"] = $this->Antenas_mw_model->edit($id);
	        $evento["link"] = "ver/sitios/infraestructura/enlaces/".$evento["sitio"][0]["id"];
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"][0]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
		    $evento["sitio"]["nombre_completo"] = $evento["sitio"][0]["nombre_completo"]." <==> ".$evento["sitio"][1]["nombre_completo"];

		    //Usuario Solicitante
			$data_antena_log = array();
			$data_antena_log = $this->Logs_antenas_mw_model->get_logs_antena_mw($id);
			$id_usuario_solicitante = $data_antena_log[0]["usuario"];
			$this->load->model('Usuarios_model');
            $data_solicitante = $this->Usuarios_model->get_user($id_usuario_solicitante);
          
		    $evento["sitio"]["nombre_solicitante"] = $data_solicitante["nombres"];
            $evento["sitio"]["apellido_solicitante"] = $data_solicitante["apellido_paterno"];
            //Usuario Solicitante			

			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
}
		


}

public function  update_filas_reservado_instalado_rechazo_mw(){
       $post = $this->input->post(); 
       $ids = explode(",", $post["ids"]);      
	   $datos = array("estado_aprobacion"=>3,"rechazado"=>1);
       $this->Solicitud_aprobar_antenas_model->update_filas_reservado_instalado_rechazo_mw($ids,$datos);
}
// Steps microondas 





}