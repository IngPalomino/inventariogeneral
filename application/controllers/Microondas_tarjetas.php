<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Microondas_tarjetas extends CI_Controller {
	
	public function listar()
	{
		$this->load->model('Microondas_tarjetas_model');
		$listado = $this->Microondas_tarjetas_model->get_tarjetas();
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Microondas_tarjetas_model');
		$listado = $this->Microondas_tarjetas_model->get_tarjetas_sitio($id);
		echo json_encode($listado);
	}

	public function listar_servicio($idu, $like, $slot)
	{
		$this->load->model('Microondas_tarjetas_model');
		$listado = $this->Microondas_tarjetas_model->get_tarjetas_idu($idu, $like, $slot);
		echo json_encode($listado);
	}

	public function listar_puertos($link_name, $port_number)
	{
		$this->load->model('Microondas_tarjetas_model');
		$listado = $this->Microondas_tarjetas_model->get_puertos_idu($link_name, $port_number);
		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('Microondas_tarjetas_model');
		$tarjeta = $this->Microondas_tarjetas_model->edit($id);
		echo json_encode($tarjeta);
	}

	public function actualizar($id)
	{
		$this->load->model('Microondas_tarjetas_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$tarjeta = $this->Microondas_tarjetas_model->edit($id);

		if($this->Microondas_tarjetas_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Tarjeta",
					"tabla"			=> "tb_e_tarjetas",
					"id"			=> $id,
					"datos viejos"	=> $tarjeta,
					"datos nuevos"	=> $this->Microondas_tarjetas_model->edit($id)
				);

			$this->load->model('Logs_aires_acondicionados_model');
			$this->Logs_aires_acondicionados_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('Microondas_tarjetas_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["data"] = json_decode($info["data"],true);

		if(count($info["data"]) > 0)
		{
			foreach($info["data"] as $tarjeta)
			{
				/*$this->form_validation->set_data($tarjeta);
				
				$config = array(
					array(
							'field' => 'idu',
							'rules' => 'trim|required|callback_check_neid['.$id.']',
							'errors' => array(
									'check_neid' => 'El NE ID no existe en este sitio.'
								)
							)
						);
						
				$this->form_validation->set_rules($config);
				
				$error = array(
					'error' => FALSE
					);
					
				if($this->form_validation->run() === FALSE)
				{
					$error['error'] = validation_errors();
					echo json_encode($error);
				}
				else
				{*/
					$idu = $this->Microondas_tarjetas_model->get_idu_id($tarjeta["idu"]);
					$tarjeta["idu"] = $idu["id"];
					$this->Microondas_tarjetas_model->insert($tarjeta);
				/*}*/
			}
		}
	}

	public function eliminar()
	{
		$this->load->model('Microondas_tarjetas_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = array();
		$info = $this->input->input_stream();
		
		$info["data"] = json_decode($info["data"],true);
		
		foreach($info["data"] as $tarjeta)
		{
			$this->Microondas_tarjetas_model->delete($tarjeta);
		}
	}
	
	public function partes_reporte()
	{
		$this->load->model('Microondas_tarjetas_model');
		$partsNumber = $this->Microondas_tarjetas_model->get_parts();
		echo $partsNumber;
	}
	
	function check_neid($idu,$id)
	{
		$info = $this->input->post();
		$info["data"] = json_decode($info["data"],true);
		
		$this->load->model('Idus_model');
		$idus = $this->Idus_model->get_idus_sitio($id);
		return in_array(array("ne_id" => $idu),$idus);
	}
}