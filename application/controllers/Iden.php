<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Iden extends CI_Controller {
	
	public function listar()
	{
		$this->load->model('Iden_model');
		$listado = $this->Iden_model->get_iden();
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Iden_model');
		$listado = $this->Iden_model->get_iden_sitio($id);
		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('Iden_model');
		$iden = $this->Iden_model->edit($id);
		echo json_encode($iden);
	}

	public function actualizar($id)
	{
		$this->load->model('Iden_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = $this->input->input_stream();
		$this->Iden_model->update($id, $info);
	}

	public function crear_sitio($id)
	{
		$this->load->model('Iden_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["data"] = json_decode($info["data"],true);

		if(count($info["data"]) > 0)
		{
			foreach($info["data"] as $iden)
			{
				$iden["sitio"] = $id;
				$this->Iden_model->insert($iden);
			}
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Iden_model');
		$info = $this->input->input_stream();
		$this->Iden_model->delete($id, $info);
	}
}