<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mantenimientos_grupos_electrogenos extends CI_Controller {

	public function listar()
	{
		$this->load->model('Mantenimientos_grupos_electrogenos_model');

		$listado = $this->Mantenimientos_grupos_electrogenos_model->get_mantenimientos();

		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Mantenimientos_grupos_electrogenos_model');

		$listado = $this->Mantenimientos_grupos_electrogenos_model->get_mantenimientos_sitio($id);

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Mantenimientos_grupos_electrogenos_model');

		$mantenimiento = $this->Mantenimientos_grupos_electrogenos_model->edit($id);

		echo json_encode($mantenimiento);
	}

	public function actualizar($id)
	{
		$this->load->model('Mantenimientos_grupos_electrogenos_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();
		
		$info_mantenimiento["tipo_mantenimiento_grupo_electrogeno"] = $info["tipo_mantenimiento_grupo_electrogeno"];
		$info_mantenimiento["fecha"] = $info["fecha"];
		$info_mantenimiento["horas"] = $info["horas"];
		$info_mantenimiento["observaciones"] = $info["observaciones"];

		$mantenimiento = $this->Mantenimientos_grupos_electrogenos_model->edit($id);

		if($this->Mantenimientos_grupos_electrogenos_model->update($id, $info_mantenimiento))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Mantenimiento de Grupo Electrógeno",
					"tabla"			=> "tb_mantenimientos_grupos_electrogenos",
					"id"			=> $id,
					"datos viejos"	=> $mantenimiento,
					"datos nuevos"	=> $this->Mantenimientos_grupos_electrogenos_model->edit($id)
				);

			$this->load->model('Logs_mantenimientos_grupos_electrogenos_model');
			$this->Logs_mantenimientos_grupos_electrogenos_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
		}
	}

	public function registrar_mantenimiento($id)
	{
		$this->load->model('Mantenimientos_grupos_electrogenos_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["grupo_electrogeno"] = $id;

		if($this->Mantenimientos_grupos_electrogenos_model->insert($info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Registro de Mantenimiento de Grupo Electrógeno",
					"tabla"		=> "tb_mantenimientos_grupos_electrogenos"
				);

			$this->load->model('Logs_mantenimientos_grupos_electrogenos_model');
			$this->Logs_mantenimientos_grupos_electrogenos_model->ingreso_de_datos($session['id_usuario'],$evento);
		}
	}
}