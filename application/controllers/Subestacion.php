<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subestacion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 6;
	}
	
	public function listar()
	{
		$this->load->model('Subestacion_model');

		$listado = $this->Subestacion_model->get_subestacion();

		echo json_encode($listado);
	}

	public function listar_sitio($id) {
		$this->load->model('Subestacion_model');

		$listado = $this->Subestacion_model->get_subestacion_sitio($id);

		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('Subestacion_model');

		$grupo = $this->Subestacion_model->edit($id);

		echo json_encode($grupo);
	}

	public function actualizar($id)
	{
		$this->load->model('Subestacion_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$subestacion = $this->Subestacion_model->edit($id);

		if($this->Subestacion_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Subestacion",
					"tabla"			=> "tb_subestacion",
					"id"			=> $id,
					"datos viejos"	=> $subestacion,
					"datos nuevos"	=> $this->Subestacion_model->edit($id)
				);
				
			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_subestacion_model');
				$this->Logs_subestacion_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$evento["sitio"] = $this->Subestacion_model->get_sitio_subestacion($id);
				$evento["link"] = "ver/salastecnicas/mso/energia/subestacion/".$evento["sitio"]["id"];				

			$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
			$cc = $this->Notificaciones_model->get_correos_subestacion($this->modulo, $evento["sitio"]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('Subestacion_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		$info["sitio"] = $id;

		$respuesta = $this->Subestacion_model->insert($info);

		if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Creación de Subestacion",
					"tabla"		=> "tb_subestacion"
				);

		$this->load->model('Logs_subestacion_model');
		$this->Logs_subestacion_model->ingreso_de_datos($session['id_usuario'],$evento);
		$this->load->model('Status_site_model');
		$evento["link"] = "ver/salastecnicas/mso/energia/subestacion/".$id;


			$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($id);
			$evento["elemento"] = $this->Subestacion_model->edit($respuesta["last_id"]);
			$cc = $this->Notificaciones_model->get_correos_subestacion($this->modulo, $id);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function almacenar($id)
	{
		$this->load->model('Subestacion_model');

		$info = $this->input->input_stream();
		$info_grupo["eliminado"] = 1;
		
		if($this->Subestacion_model->update($id, $info_grupo))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Almacenamiento de Subestacion",
					"tabla"			=> "tb_subestacion",
					"id"			=> $id
				);

			$this->load->model('Logs_subestacion_model');
			$this->Logs_subestacion_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$this->load->model('Bodega_subestacion_model');
			unset($info['comentario']);

			if($this->Bodega_subestacion_model->insert($info))
			{
				$evento = array(
					"accion"		=> "Almacenamiento de Subestación",
					"tabla"			=> "tb_bodega_subestacion"
				);

				$this->load->model('Logs_bodega_subestacion_model');
				$this->Logs_bodega_subestacion_model->ingreso_de_datos($session['id_usuario'],$evento);
			}
		}
	}
	
	public function listar_sitios()
	{
		$this->load->model('Subestacion_model');

		$listado = $this->Subestacion_model->get_sitios_subestacion();

		echo json_encode($listado);
	}

	public function eliminar($id)
	{
		$this->load->model('Subestacion_model');

		$info = $this->input->input_stream();

		$info_subestacion['eliminado'] = 1;

		if($this->Subestacion_model->update($id, $subestacion))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Subestacion",
					"tabla"			=> "tb_subestacion",
					"id"			=> $id
				);

			$this->load->model('Logs_subestacion_model');
			$this->Logs_subestacion_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$evento["sitio"] = $this->Subestacion_model->get_sitio_subestacion($id);
			$evento["link"] = "ver/salastecnicas/mso/energia/subestacion/".$evento["sitio"]["id"];

			$evento["elemento"] = $this->Subestacion_model->edit($id);
			$evento["comentario"] = $info["comentario"];
			$cc = $this->Notificaciones_model->get_correos_subestacion($this->modulo, $evento["sitio"]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}
}