<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance extends CI_Controller {

	public function mantenimiento_tokens()
	{
		if(is_cli())
		{
			$this->load->model('Database_maintenance_model');
			$tokens = $this->Database_maintenance_model->get_tokens();

			if(count($tokens) > 0)
			{
				echo "Si existen tokens.";
			}
			else
			{
				echo "No existen tokens.";
			}
		}
		else
		{
			show_404();
		}
	}

	public function clean_plantillas()
	{
		if(is_cli())
		{
			$file = "/var/www/html/oym/inventarioGeneral/uploads/ingreso_masivo/";
		}
		else
		{
			show_404();
		}
	}
}