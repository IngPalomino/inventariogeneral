<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_sistema_combustible extends CI_Controller {
	
	public function listar()
	{
		$this->load->model('Bodega_sistema_combustible_model');
		$listado = $this->Bodega_sistema_combustible_model->get_sistema_combustible();
		echo json_encode($listado);
	}

	public function listar_almacen($id)
	{
		$this->load->model('Bodega_sistema_combustible_model');
		$listado = $this->Bodega_sistema_combustible_model->get_sistema_combustible_almacen($id);
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Bodega_sistema_combustible_model');
		$listado = $this->Bodega_sistema_combustible_model->get_sistema_combustible_sitio($id);
		echo json_encode($listado);
	}

	public function actualizar($id)
	{
		$this->load->model('Bodega_sistema_combustible_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = $this->input->input_stream();
		$antena = $this->Bodega_sistema_combustible_model->edit($id);

		if($this->Bodega_sistema_combustible_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Sistema Combustible",
					"tabla"			=> "tb_bodega_sistema_combustible",
					"id"			=> $id,
					"datos viejos"	=> $antena,
					"datos nuevos"	=> $this->Bodega_sistema_combustible_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_bodega_sistema_combustible_model');
				$this->Logs_bodega_sistema_combustible_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
			}
		}
	}

	public function reactivar($id)
	{
		$this->load->model('Bodega_sistema_combustible_model');

		$info = $this->input->input_stream();
		$info_sistema_combustible["eliminado"] = 1;
		
		if($this->Bodega_sistema_combustible_model->update($id, $info_sistema_combustible))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Reinstalación de Sistema Combustible",
					"tabla"			=> "tb_bodega_sistema_combustible",
					"id"			=> $id
				);

			$this->load->model('Logs_bodega_sistema_combustible_model');
			$this->Logs_bodega_sistema_combustible_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$this->load->model('Sistema_combustible_model');
			unset($info['comentario']);

			if($this->Sistema_combustible_model_model->insert($info))
			{
				$evento = array(
					"accion"		=> "Reinstalación de Sistema Combustible",
					"tabla"			=> "tb_sistema_combustible"
				);

				$this->load->model('Logs_sistema_combustible_model');
				$this->Logs_sistema_combustible_model->ingreso_de_datos($session['id_usuario'],$evento);
			}
		}
	}

	public function mover($id)
	{
		$this->load->model('Bodega_sistema_combustible_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = $this->input->input_stream();
		$antena = $this->Bodega_sistema_combustible_model->edit($id);

		$almacen_antiguo = array(
					"tipo_almacen" 	=> $antena[0]["tipo_almacen"],
					"almacen"		=> $antena[0]["almacen"]
				);

		$info["almacen_previo"] = json_encode($almacen_antiguo);
		$comentario = $info["comentario"];
		unset($info["comentario"]);

		if($this->Bodega_sistema_combustible_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			$sistema_combustible_nueva = $this->Bodega_sistema_combustible_model->edit($id);

			$almacen_nuevo = array(
					"tipo_almacen" 	=> $sistema_combustible[0]["tipo_almacen"],
					"almacen"		=> $sistema_combustible[0]["almacen"]
				);

			$evento = array(
					"accion"		=> "Cambio de almacén de Sistema de Combustible",
					"tabla"			=> "tb_bodega_sistema_combustible",
					"id"			=> $id,
					"almacen_antiguo"	=> $info["almacen_previo"],
					"almacen_nuevo"	=> json_encode($almacen_nuevo)
				);

			if($evento["almacen_antiguo"] != $evento["almacen_nuevo"])
			{
				$this->load->model('Logs_bodega_sistema_combustible_model');
				$this->Logs_bodega_sistema_combustible_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$comentario);
			}
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Bodega_sistema_combustible_model');

		$info = $this->input->input_stream();

		$info_sistema_combustible['eliminado'] = 1;

		if($this->Bodega_sistema_combustible_model->update($id, $info_sistema_combustible))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Sistema Combustible",
					"tabla"			=> "tb_bodega_sistema_combustible",
					"id"			=> $id
				);

			$this->load->model('Logs_bodega_sistema_combustible_model');
			$this->Logs_bodega_sistema_combustible_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);
		}
	}
}