<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_antenas_gul extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('auth');
		$this->load->model('Bodega_antenas_gul_model');
		$this->load->model('Logs_bodega_antenas_gul_model', 'logs');
	}
	
	public function listar()
	{
		$this->load->model('Bodega_antenas_gul_model');
		$listado = $this->Bodega_antenas_gul_model->get_antenas_gul();
		echo json_encode($listado);
	}

	public function listar_almacen($id)
	{
		$this->load->model('Bodega_antenas_gul_model');
		$listado = $this->Bodega_antenas_gul_model->get_antenas_gul_almacen($id);
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Bodega_antenas_gul_model');
		$listado = $this->Bodega_antenas_gul_model->get_antenas_gul_sitio($id);
		echo json_encode($listado);
	}

	public function almacenar()
	{
		$post = $this->input->post();

		if($this->auth->logged_in())
		{
			if($this->Bodega_antenas_gul_model->insert($post))
			{
				$evento["accion"] = "Almacenamiento de Antena GUL";
				$evento["tabla"] = "tb_bodega_antenas_gul";

				$this->logs->ingreso_de_datos($this->session->userdata('id_usuario'), $evento);
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}

	public function actualizar($id)
	{
		$post = $this->input->input_stream(array("tipo_antena_gul", "numero_serie", "estado"));

		if ($this->auth->logged_in())
		{
			$old = $this->Bodega_antenas_gul_model->edit($id);

			if ($this->Bodega_antenas_gul_model->update($id, $post))
			{
				$evento["accion"] = "Actualización de Antena GUL.";
				$evento["tabla"] = "tb_bodega_antenas_gul";
				$evento["id"] = $id;
				$evento["datos_antiguos"] = $old;
				$evento["datos_nuevos"] = $this->Bodega_antenas_gul_model->edit($id);

				if ($evento["datos_antiguos"] != $evento["datos_nuevos"])
				{
					$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento);
				}
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}

	/*public function actualizar($id)
	{
		$this->load->model('Bodega_antenas_gul_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = $this->input->input_stream();
		$antena = $this->Bodega_antenas_gul_model->edit($id);

		if($this->Bodega_antenas_gul_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Antena GUL",
					"tabla"			=> "tb_bodega_antenas_gul",
					"id"			=> $id,
					"datos viejos"	=> $antena,
					"datos nuevos"	=> $this->Bodega_antenas_gul_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_bodega_antenas_gul_model');
				$this->Logs_bodega_antenas_gul_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
			}
		}
	}*/

	public function reactivar($id)
	{
		$this->load->model('Bodega_antenas_gul_model');

		$info = $this->input->input_stream();
		$info_antena["eliminado"] = 1;
		
		if($this->Bodega_antenas_gul_model->update($id, $info_antena))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Reinstalación de Antena GUL",
					"tabla"			=> "tb_bodega_antenas_gul",
					"id"			=> $id
				);

			$this->load->model('Logs_bodega_antenas_gul_model');
			$this->Logs_bodega_antenas_gul_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$this->load->model('Antenas_gul_model');
			unset($info['comentario']);

			if($this->Antenas_gul_model->insert($info))
			{
				$evento = array(
					"accion"		=> "Reinstalación de Antena GUL",
					"tabla"			=> "tb_antenas_gul"
				);

				$this->load->model('Logs_antenas_gul_model');
				$this->Logs_antenas_gul_model->ingreso_de_datos($session['id_usuario'],$evento);
			}
		}
	}

	public function mover($id)
	{
		if ($this->auth->logged_in())
		{
			$info = $this->input->input_stream(array('tipo_almacen', 'almacen'));
			$old = $this->Bodega_antenas_gul_model->edit($id);
			$info["almacen_previo"] = json_encode(array("tipo_almacen" => $old[0]["tipo_almacen"], "almacen" => $old[0]["almacen"]));

			if ($this->Bodega_antenas_gul_model->update($id, $info))
			{
				$new = $this->Bodega_antenas_gul_model->edit($id);

				$evento["accion"] = "Cambio de almacén de Antena GUL.";
				$evento["tabla"] = "tb_bodega_antenas_gul";
				$evento["id"] = $id;
				$evento["almacen_antiguo"] = $info["almacen_previo"];
				$evento["almacen_nuevo"] = json_encode(array("tipo_almacen" => $new[0]["tipo_almacen"], "almacen" => $new[0]["almacen"]));

				if ($evento["almacen_antiguo"] != $evento["almacen_nuevo"])
				{
					$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento, $this->input->input_stream('comentario'));
				}
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}

	public function eliminar($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->Bodega_antenas_gul_model->update($id, array("eliminado" => 1)))
			{
				$evento["accion"] = "Eliminación de Antena GUL";
				$evento["tabla"] = "tb_bodega_antenas_gul";
				$evento["id"] = $id;

				$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento, $this->input->input_stream('comentario'));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}
}