<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Zona_peru extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function listar()
	{
		$this->load->model('Zona_peru_model');

		$listado = $this->Zona_peru_model->get_zonas();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Zona_peru_model');

		$zona = $this->Zona_peru_model->edit($id);

		echo json_encode($zona);
	}

	public function actualizar($id)
	{
		$this->load->model('Zona_peru_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['departamento'] = $this->input->input_stream('departamento');
		$info['provincia'] = $this->input->input_stream('provincia');
		$info['distrito'] = $this->input->input_stream('distrito');

		$this->Zona_peru_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Zona_peru_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['departamento'] = $this->input->post('departamento');
		$info['provincia'] = $this->input->post('provincia');
		$info['distrito'] = $this->input->post('distrito');

		$this->Zona_peru_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Zona_peru_model');

		$this->Zona_peru_model->delete($id);
	}

	public function listarDepartamentos()
	{
		$this->load->model('Zona_peru_model');

		$listado = $this->Zona_peru_model->get_departamentos();

		echo json_encode($listado);
	}

	public function listarProvincias($departamento)
	{
		$this->load->model('Zona_peru_model');

		$departamento = urldecode($departamento);
		$listado = $this->Zona_peru_model->get_provincias($departamento);

		echo json_encode($listado);
	}

	public function listarDistritos($departamento,$provincia)
	{
		$this->load->model('Zona_peru_model');

		$departamento = urldecode($departamento);
		$provincia = urldecode($provincia);
		$listado = $this->Zona_peru_model->get_distritos($departamento,$provincia);

		echo json_encode($listado);
	}
}