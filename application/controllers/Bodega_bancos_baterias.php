<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_bancos_baterias extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('auth');
		$this->load->model('Bodega_bancos_baterias_model');
		$this->load->model('Logs_bodega_bancos_baterias_model', 'logs');
	}

	public function listar()
	{
		$this->load->model('Bodega_bancos_baterias_model');
		$listado = $this->Bodega_bancos_baterias_model->get_bancos_baterias();
		echo json_encode($listado);
	}
	
	public function listar_almacen($id)
	{
		$this->load->model('Bodega_bancos_baterias_model');
		$listado = $this->Bodega_bancos_baterias_model->get_bancos_baterias_almacen($id);
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Bodega_bancos_baterias_model');
		$listado = $this->Bodega_bancos_baterias_model->get_bancos_baterias_sitio($id);
		echo json_encode($listado);
	}

	public function almacenar()
	{
		$post = $this->input->post();
		$celdas = json_decode($post["celdas"], TRUE);
		unset($post["celdas"]);

		if($this->auth->logged_in())
		{
			$respuesta = $this->Bodega_bancos_baterias_model->insert($post);

			if($respuesta["resp"])
			{
				$evento["accion"] = "Almacenamiento de Banco de Baterías";
				$evento["tabla"] = "tb_bodega_bancos_baterias";

				$this->logs->ingreso_de_datos($evento);

				if (count($celdas) > 0)
				{
					$this->load->model('Bodega_celdas_bancos_baterias_model', 'celdas');

					foreach ($celdas as $celda)
					{
						$celda["banco_baterias"] = $respuesta["last_id"];
						$this->celdas->insert($celda);
					}
				}
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}

	public function actualizar($id)
	{
		$info = $this->input->input_stream(array('tipo_bancos_baterias', 'confiabilidad', 'estado'));
		$info_celdas = json_decode($this->input->input_stream('celdas_banco_baterias'), TRUE);

		$grupo = $this->Bodega_bancos_baterias_model->edit($id);

		if($this->Bodega_bancos_baterias_model->update($id, $info))
		{
			if(count($info_celdas) > 0)
			{
				$this->load->model('Bodega_celdas_bancos_baterias_model');

				foreach($info_celdas as $celda)
				{
					$info_celda['numero_serie'] = $celda['numero_serie'];
					$this->Bodega_celdas_bancos_baterias_model->update($celda['id'], $info_celda);
				}
			}

			$evento = array(
					'accion'		=> 'Actualización de Banco de Baterías',
					'tabla'			=> 'tb_bodega_bancos_baterias',
					'id'			=> $id,
					'datos viejos'	=> $grupo,
					'datos nuevos'	=> $this->Bodega_bancos_baterias_model->edit($id)
				);

			if($evento['datos viejos'] != $evento['datos nuevos'])
			{
				$this->logs->actualizacion_de_datos($id, $evento);
			}
		}
	}
	
	public function reactivar($id)
	{
		$this->load->model('Bodega_bancos_baterias_model');

		$info = $this->input->input_stream();
		$info_grupo["eliminado"] = 1;
		
		if($this->Bodega_bancos_baterias_model->update($id, $info_grupo))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Reinstalación de Banco de Baterías",
					"tabla"			=> "tb_bodega_bancos_baterias",
					"id"			=> $id
				);

			$this->load->model('Logs_bodega_bancos_baterias_model');
			$this->Logs_bodega_bancos_baterias_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$this->load->model('Grupos_electrogenos_model');
			unset($info['comentario']);

			if($this->Grupos_electrogenos_model->insert($info))
			{
				$evento = array(
					"accion"		=> "Reinstalación de Grupo Electrógeno",
					"tabla"			=> "tb_grupos_electrogenos"
				);

				$this->load->model('Logs_grupos_electrogenos_model');
				$this->Logs_grupos_electrogenos_model->ingreso_de_datos($evento);
			}
		}
	}

	public function mover($id)
	{
		$info = $this->input->input_stream();
		$grupo = $this->Bodega_bancos_baterias_model->edit($id);

		$almacen_antiguo = array(
					"tipo_almacen" 	=> $grupo[0]["tipo_almacen"],
					"almacen"		=> $grupo[0]["almacen"]
				);

		$info["almacen_previo"] = json_encode($almacen_antiguo);
		$comentario = $info["comentario"];
		unset($info["comentario"]);

		if($this->Bodega_bancos_baterias_model->update($id, $info))
		{
			$grupo_nuevo = $this->Bodega_bancos_baterias_model->edit($id);

			$almacen_nuevo = array(
					"tipo_almacen" 	=> $grupo_nuevo[0]["tipo_almacen"],
					"almacen"		=> $grupo_nuevo[0]["almacen"]
				);

			$evento = array(
					"accion"		=> "Cambio de almacén de Grupo Electrógeno",
					"tabla"			=> "tb_bodega_bancos_baterias",
					"id"			=> $id,
					"almacen_antiguo"	=> $info["almacen_previo"],
					"almacen_nuevo"	=> json_encode($almacen_nuevo)
				);

			if($evento["almacen_antiguo"] != $evento["almacen_nuevo"])
			{
				$this->load->model('Logs_bodega_bancos_baterias_model');
				$this->Logs_bodega_bancos_baterias_model->actualizacion_de_datos($id, $evento, $comentario);
			}
		}
	}
	
	public function eliminar($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->Bodega_bancos_baterias_model->update($id, array("eliminado" => 1)))
			{
				$evento["accion"] = "Eliminación de Banco de Baterías";
				$evento["tabla"] = "tb_bodega_bancos_baterias";
				$evento["id"] = $id;

				$this->logs->actualizacion_de_datos($id, $evento, $this->input->input_stream('comentario'));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}
}