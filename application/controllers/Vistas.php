<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vistas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('custom', 'url', 'app', 'auth'));
		$this->load->library(array('auth', 'app'));
	}

	public function index()
	{
		$this->app->load_main('', 'menu/inicio');
	}

	public function admin_inicio()
	{
		if ( ! $this->auth->is_admin())
		{
			redirect(base_url(), 'refresh');
		}

		$this->app->load_main('Administrar', 'administrador/inicio');
	}

	public function admin_usuarios()
	{
		if ( ! $this->auth->is_admin())
		{
		  redirect(base_url(), 'refresh');
		}
		$this->app->add_data('css', 'selectize')
					->add_data('js', array('selectize', 'angular-selectize'))
					->load_main('Administrar usuarios', 'administrador/usuarios', 'administrador/usuarios');
	}

	public function admin_grupos2()
	{
		if ( ! $this->auth->is_admin())
		{
			redirect(base_url(), 'refresh');
		}

		$this->app->add_data('css', 'selectize')
					->add_data('js', array('selectize', 'angular-selectize'))
					->load_main('Administrar grupos', 'administrador/grupos', 'administrador/grupos');
	}
	//Reestructuración de vistas pendiente desde aquí hacia abajo.

	public function admin_grupos()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Administrar grupos";

		if ($this->auth->is_admin())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('administrador/grupos');
			$this->load->view('administrador/footer_grupos');
		}
		else
		{
			show_404();
		}
	}
	
	public function admin_relaciones_cols()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'app'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Administrar relaciones";

		if ($this->auth->is_super_admin())
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('administrador/header_opciones_relaciones');
			$this->load->view('administrador/relaciones_cols');
			$this->load->view('administrador/footer_relaciones_cols');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function admin_dependencias()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Administrar dependencias";

		if ($this->auth->is_super_admin())
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('administrador/dependencias');
			
			$this->load->view('administrador/footer_dependencias');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function admin_ingreso_masivo()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Administrar ingreso masivo";

		if ($this->auth->is_admin())
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('administrador/ingreso_masivo');
			$this->load->view('administrador/footer_ingreso_masivo');
		}
		else
		{
			redirect(base_url());
		}
	}



	public function aprobar_instalacion_antena()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Aprobar instalación antena";

		if ($this->auth->is_admin())
		{
			$this->load->view('layouts/header2',$data);
            $this->load->view('administrador/aprobar_antena');
			$this->load->view('administrador/footer_aprobar_antena');
		}
		else
		{
			redirect(base_url());
		}
	}



	public function admin_ingreso_masivo_elemento($elemento)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['elemento'] = $elemento;
		$data['title'] = "Administrar validaciones de ingreso masivo";

		$this->load->model('Ingreso_masivo_model');
		$element = $this->Ingreso_masivo_model->get_elemento($elemento);

		if ($this->auth->is_admin())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('administrador/ingreso_masivo/'.$element["tabla"]["plantilla"]);
			$this->load->view('administrador/ingreso_masivo/footer_'.$element["tabla"]["plantilla"]);
		}
		else
		{
			redirect(base_url());
		}
	}


	public function admin_aprobar_antena_elemento($elemento)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['elemento'] = $elemento;
		$data['title'] = "Administrar Solicitud de instalación de antenas";

		$this->load->model('Solicitud_aprobar_antenas_model');
		$element = $this->Solicitud_aprobar_antenas_model->get_elemento($elemento);

		if ($this->auth->is_admin())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('administrador/aprobar_antena/pendientes_antenas');
			$this->load->view('administrador/aprobar_antena/footer_pendientes_antenas');
		}
		else
		{
			redirect(base_url());
		}
	}


	public function admin_aprobar_antena_elemento_mw($elemento)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['elemento'] = $elemento;
		$data['title'] = "Administrar Solicitud de instalación de antenas";

		$this->load->model('Solicitud_aprobar_antenas_model');
		$element = $this->Solicitud_aprobar_antenas_model->get_elemento_mw($elemento);

		if ($this->auth->is_admin())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('administrador/aprobar_antena/pendientes_antenas_mw');
			$this->load->view('administrador/aprobar_antena/footer_pendientes_antenas_mw');
		}
		else
		{
			redirect(base_url());
		}
	}




	public function ingreso_masivo_inicio()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Ingreso masivo";

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('masivo/inicio');
			$this->load->view('layouts/footer2');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_masivo_antenas_gul()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Ingreso masivo de Antenas GUL";

		if ($this->auth->allowed_module(3))
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('masivo/antenas_gul');
			$this->load->view('masivo/footer_antenas_gul');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_masivo_suministros()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Ingreso masivo de Suministros";

		if ($this->auth->allowed_module(9))
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('masivo/suministros');
			$this->load->view('masivo/footer_suministros');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_masivo_idus()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Ingreso masivo de IDUs";

		if ($this->auth->allowed_module(4))
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('masivo/idus');
			$this->load->view('masivo/footer_idus');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_masivo_enlaces()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app',
								'loading-bar.min'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'loading-bar.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Ingreso masivo de Enlaces";

		if ($this->auth->allowed_module(5))
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('masivo/enlaces');
			$this->load->view('masivo/footer_enlaces');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_masivo_pop()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app',
								'loading-bar.min'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'loading-bar.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Ingreso masivo de POP";

		if ($this->auth->allowed_module(2))
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('masivo/pop');
			$this->load->view('masivo/footer_pop');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_masivo_contratos()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app',
								'loading-bar.min'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'loading-bar.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Ingreso masivo de Contratos";

		if ($this->auth->allowed_module(2))
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('masivo/contratos');
			$this->load->view('masivo/footer_contratos');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_masivo_rectificadores()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app',
								'loading-bar.min'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'loading-bar.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Ingreso masivo de Rectificadores";

		if ($this->auth->allowed_module(7))
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('masivo/rectificadores');
			$this->load->view('masivo/footer_rectificadores');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_masivo_bancos_baterias()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'app',
								'loading-bar.min'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'loading-bar.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Ingreso masivo de Bancos de Baterías";

		if ($this->auth->allowed_module(8))
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('masivo/bancos_baterias');
			$this->load->view('masivo/footer_bancos_baterias');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function visualizacion_inicio()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();

		$this->load->view('layouts/header2', $data);
		$this->load->view('visualizacion/inicio');
		$this->load->view('layouts/footer2');
	}

	public function visualizacion_busqueda()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app',
								'jquery-ui.min'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jquery-ui.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Búsqueda";

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_busqueda');
		$this->load->view('visualizacion/resumen_listado');
		$this->load->view('visualizacion/footer_resumen_listado');
	}

	public function visualizacion_bodegas()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		
		$this->load->view('layouts/header2', $data);
		$this->load->view('visualizacion/bodegas/inicio');
		$this->load->view('visualizacion/bodegas/footer_inicio');
	}

	public function visualizacion_gis_sitio($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/gis');
		$this->load->view('visualizacion/footer_gis');
	}

	public function visualizacion_resumen_sitio($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/resumen');
		$this->load->view('visualizacion/footer_resumen');
	}

	public function visualizacion_torre_sitio($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/torre');
		$this->load->view('visualizacion/footer_torre');
	}

	public function visualizacion_antenas_sitio($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/antenas');
		$this->load->view('visualizacion/footer_antenas');
	}

	public function visualizacion_idus_sitio($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/idus');
		$this->load->view('visualizacion/footer_idus');
	}


    public function solicitante()
    {
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		


        if ($this->auth->logged_in())		
		{
		
		$data['session'] = $this->session->userdata();
		$this->load->view('layouts/header2', $data);		
		$this->load->view('visualizacion/solicitante');
		$this->load->view('visualizacion/footer_solicitante');
		}
		else
		{
			redirect(base_url());
		}




    }


    public function solicitante_gul()
    {
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);



   

        if ($this->auth->logged_in())		
		{
		$data['session'] = $this->session->userdata();
		$this->load->view('layouts/header2', $data);		
		$this->load->view('visualizacion/solicitante/solicitante_gul');
		$this->load->view('visualizacion/solicitante/footer_solicitante_gul');
		}
		else
		{
			redirect(base_url());
		}





    }



    public function solicitante_mw()
    {
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);




        if ($this->auth->logged_in())		
		{
		$data['session'] = $this->session->userdata();
		$this->load->view('layouts/header2', $data);		
		$this->load->view('visualizacion/solicitante/solicitante_mw');
		$this->load->view('visualizacion/solicitante/footer_solicitante_mw');
		}
		else
		{
			redirect(base_url());
		}

		

    }


    public function salastecnicas()
    {
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);	

		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);
        if ($this->auth->logged_in())		
		{		
		$data['session'] = $this->session->userdata();
		$this->load->view('layouts/header2', $data);		
		$this->load->view('visualizacion/salas_tecnicas');
		$this->load->view('visualizacion/footer_salas_tecnicas');
		}
		else
		{
			redirect(base_url());
		}
    }

    public function salaMso($id)
    {
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'jstree_style.min',
								'estilo_nav',
								'bpopup',
								'app'
							);		
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'jstree.min',
								'bootstrap.min',
								'bpopup.min'
							);
        if ($this->auth->logged_in())		
		{
		$data['session'] = $this->session->userdata();
		 //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);        
        $data['id_sitio']  = $info_sala_tecnica[0]['sitio'];
        //id_sitio
		$data['id_mso'] = $id;


		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/navbar_mso_visualizacion');			
		$this->load->view('visualizacion/salas_tecnicas/mso_sala');
		$this->load->view('visualizacion/salas_tecnicas/footer_mso_sala');		
		}
		else
		{
			redirect(base_url());
		}
    }

    public function visualizacion_salatecnica_mso_grupo_electrogenos($id){
		$data['css'] = array(
							 	'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'app'
							);	
								
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);
        if ($this->auth->logged_in())		
		{
		
		$data['session'] = $this->session->userdata();		
		$data['id_mso'] = $id;
        //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);        
        $data['id_sitio']  = $info_sala_tecnica[0]['sitio'];
        //id_sitio
		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/navbar_mso_visualizacion');
		$this->load->view('visualizacion/salas_tecnicas/energia/grupos_electrogenos');
		$this->load->view('visualizacion/salas_tecnicas/energia/footer_grupos_electrogenos');
		}
		else
		{
		  redirect(base_url());
		}
    }






	public function visualizacion_enlaces_sitio($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;	

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/enlaces');
		$this->load->view('visualizacion/footer_enlaces');

	}

	public function visualizacion_tanques_combustible($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);


		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/tanques_combustible');
		$this->load->view('visualizacion/footer_tanques_combustible');
	}

	public function visualizacion_suministros($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/suministros');
		$this->load->view('visualizacion/footer_suministros');
	}

	public function visualizacion_lineas_electricas($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/lineas_electricas');
		$this->load->view('visualizacion/footer_lineas_electricas');
	}
	
	public function visualizacion_salatecnica_mso_subestacion($id)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
        $data['id_mso'] = $id;
        //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);
        $sitio = $info_sala_tecnica[0]['sitio'];        
        $data['id_sitio']  = $sitio;
        //id_sitio
		$data['sitio'] = $sitio;
		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/navbar_mso_visualizacion');		
		$this->load->view('visualizacion/salas_tecnicas/energia/subestacion');
		$this->load->view('visualizacion/salas_tecnicas/energia/footer_subestacion');
	}

   public function  visualizacion_salatecnica_mso_ups($id){


	$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
        $data['id_mso'] = $id;
        //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);
        $sitio = $info_sala_tecnica[0]['sitio'];        
        $data['id_sitio']  = $sitio;
        //id_sitio
		$data['sitio'] = $sitio;
		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);


		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/navbar_mso_visualizacion');		
		$this->load->view('visualizacion/salas_tecnicas/energia/ups');
		$this->load->view('visualizacion/salas_tecnicas/energia/footer_ups');

   }
	public function visualizacion_rectificadores($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
	
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/rectificadores');
		$this->load->view('visualizacion/footer_rectificadores');
	}
	
	public function visualizacion_aires_acondicionados($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/aires_acondicionados');
		$this->load->view('visualizacion/footer_aires_acondicionados');
	}


    public function visualizacion_salatecnica_mso_aires_acondicionados($id)
	{

		
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['id_mso'] = $id;
		//id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);
        $sitio = $info_sala_tecnica[0]['sitio'];        
        $data['id_sitio'] = $sitio;
        $data['sitio']  = $sitio;
        //id_sitio	
		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/navbar_mso_visualizacion');
		$this->load->view('visualizacion/salas_tecnicas/clima/aires_acondicionados');
		$this->load->view('visualizacion/salas_tecnicas/clima/footer_aires_acondicionados');
	}

    public function visualizacion_salatecnica_mso_rectificadores($id)
    {
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);

		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();

        $data['id_mso'] = $id;
        //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);
        $sitio = $info_sala_tecnica[0]['sitio'];        
        $data['id_sitio'] = $sitio;
        $data['sitio']  = $sitio;
        //id_sitio
		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/navbar_mso_visualizacion');
		$this->load->view('visualizacion/salas_tecnicas/energia/rectificadores');
		$this->load->view('visualizacion/salas_tecnicas/energia/footer_rectificadores');
    }

    public function visualizacion_salatecnica_mso_inversores($id)
    {
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);

		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
        $data['id_mso'] = $id;
        //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);
        $sitio = $info_sala_tecnica[0]['sitio'];        
        $data['id_sitio'] = $sitio;
        $data['sitio']  = $sitio;
        //id_sitio
		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/navbar_mso_visualizacion');
		$this->load->view('visualizacion/salas_tecnicas/energia/inversores');
		$this->load->view('visualizacion/salas_tecnicas/energia/footer_inversores');
    }



	public function visualizacion_bancos_baterias($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/bancos_baterias');
		$this->load->view('visualizacion/footer_bancos_baterias');
	}
	

    public function visualizacion_salatecnica_mso_bancos_baterias($id)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['id_mso'] = $id;
        //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);        
        $data['id_sitio']  = $info_sala_tecnica[0]['sitio'];
        $data['sitio']  = $info_sala_tecnica[0]['sitio'];
        //id_sitio
		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/navbar_mso_visualizacion');

		$this->load->view('visualizacion/salas_tecnicas/energia/bancos_baterias');
		$this->load->view('visualizacion/salas_tecnicas/energia/footer_bancos_baterias');
	}
	

	public function visualizacion_grupos_electrogenos($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/grupos_electrogenos');
		$this->load->view('visualizacion/footer_grupos_electrogenos');
	}
	
	public function visualizacion_nodo_b($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/nodo_b');
		$this->load->view('visualizacion/footer_nodo_b');
	}
	
	public function visualizacion_microondas($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/microondas');
		$this->load->view('visualizacion/footer_microondas');
	}

	public function visualizacion_wimax($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/wimax');
		$this->load->view('visualizacion/footer_wimax');
	}

	public function visualizacion_iden($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/iden');
		$this->load->view('visualizacion/footer_iden');
	}
	
	public function visualizacion_documentacion($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'jstree_style.min',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'jstree.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/documentacion_ftp');
		$this->load->view('visualizacion/footer_documentacion_ftp');
	}

	public function visualizacion_capacity($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'app'
			);

		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min'
			);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model('Status_site_model');
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		$this->load->view('layouts/header2', $data);
		$this->load->view('layouts/header_opciones');
		$this->load->view('visualizacion/capacity');
		$this->load->view('visualizacion/footer_capacity');
	}
	
	public function ingreso_inicio()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Ingreso y modificación";

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('ingreso/inicio');
			$this->load->view('layouts/footer2');
		}
		else
		{
			redirect(base_url());
		}
	}


	public function ingreso_sitios()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jquery-ui.min',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',								
								'jquery-ui.min'

							);

		$data['session'] = $this->session->userdata();
		$data['modulo'] = 2;
		$data['title'] = "Ingreso de sitios";

		if ($this->auth->logged_in())
		{
			$this->load->model('Status_site_model');
			$data["resumen"] = $this->Status_site_model->resumen_sitios();

			$this->load->view('layouts/header2', $data);
			$this->load->view('ingreso/listar_sitios_2');
			$this->load->view('ingreso/footer_listar_sitios_2');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_sitios_prueba()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jquery-ui.min',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jquery-ui.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('ingreso/listar_sitios_prueba');
			$this->load->view('ingreso/footer_listar_sitios_prueba');
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function ingreso_busqueda()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jquery-ui.min',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jquery-ui.min'
							);

		$data['session'] = $this->session->userdata();

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_busqueda');
			$this->load->view('ingreso/resumen_listado');
			$this->load->view('ingreso/footer_resumen_listado');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_bodegas()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jquery-ui.min',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jquery-ui.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['title'] = "Bodegas Virtuales";

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('ingreso/bodegas/inicio');
			$this->load->view('ingreso/bodegas/footer_inicio');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function bodegas_elemento($elemento)
	{
		$this->load->view('ingreso/bodegas/'.$elemento);
	}

	public function ingreso_gis_sitio($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/gis');
			$this->load->view('ingreso/footer_gis');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_resumen_sitio($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/resumen');
			$this->load->view('ingreso/footer_resumen');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_torre_sitio($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'angular-messages.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 2;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/torre');
			$this->load->view('ingreso/footer_torre');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_antenas_sitio($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 3;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/antenas');
			$this->load->view('ingreso/footer_antenas');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_antenas_sitio_prueba($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
		);
		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/antenas_prueba');
			$this->load->view('ingreso/footer_antenas_prueba');
		}
		else
		{
			redirect(base_url());
		}
	}

	
	public function ingreso_idus_sitio($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 4;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/idus');
			$this->load->view('ingreso/footer_idus');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_enlaces_sitio($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jquery-ui.min',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jquery-ui.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 5;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$data['title'] = "Enlaces Microondas | ".$data['infoSitio'][0]['nombre_completo'];

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/enlaces');
			$this->load->view('ingreso/footer_enlaces');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_tanques_combustible($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 11;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$data['title'] = "Tanques de Combustible | ".$data['infoSitio'][0]['nombre_completo'];

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/tanques_combustible');
			$this->load->view('ingreso/footer_tanques_combustible');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_suministros($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 9;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/suministros');
			$this->load->view('ingreso/footer_suministros');
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function ingreso_lineas_electricas($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 10;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/lineas_electricas');
			$this->load->view('ingreso/footer_lineas_electricas');
		}
		else
		{
			redirect(base_url());
		}
	}
	
    public function ingreso_salastecnicas_mso_energia_subestacion($id){
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['id_mso'] = $id;
        //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);
        $sitio  = $info_sala_tecnica[0]['sitio'];         
        $data['id_sitio']  = $sitio;
        //id_sitio
		$data['sitio'] = $sitio;
		$data['modulo'] = 10;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
		$this->load->view('layouts/header2', $data);
	    $this->load->view('layouts/navbar_mso_ingreso');
        $this->load->view('ingreso/salas_tecnicas/energia/subestacion');
		$this->load->view('ingreso/salas_tecnicas/energia/footer_subestacion');
		}
		else
		{
			redirect(base_url());
		}

    }

    public  function ingreso_salastecnicas_mso_energia_ups($id){
	    
	    $data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'selectize',
								'app'
							);
		
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'selectize',
								'angular-selectize'
							);
		
		$data['session'] = $this->session->userdata();
		$data['id_mso'] = $id;       
        //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);
        $sitio  = $info_sala_tecnica[0]['sitio'];         
        $data['id_sitio']  = $sitio;
        //id_sitio
		$data['sitio'] = $sitio;
		$data['modulo'] = 10;
		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		if ($this->auth->logged_in())
		{
		$this->load->view('layouts/header2', $data);
	    $this->load->view('layouts/navbar_mso_ingreso');
        $this->load->view('ingreso/salas_tecnicas/energia/ups');
		$this->load->view('ingreso/salas_tecnicas/energia/footer_ups');
		}
		else
		{
			redirect(base_url());
		}



    } 




	public function ingreso_rectificadores($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jquery-ui.min',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jquery-ui.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 7;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$data['title'] = "Rectificadores | ".$data['infoSitio'][0]['nombre_completo'];

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/rectificadores');
			$this->load->view('ingreso/footer_rectificadores');
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function ingreso_salastecnicas_mso_energia_rectificadores($id){
    		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jquery-ui.min',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jquery-ui.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();

	   $data['id_mso'] = $id;
        //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);   
        $sitio = $info_sala_tecnica[0]['sitio'];
        $data['id_sitio']  = $sitio;
        //id_sitio



		$data['sitio'] = $sitio;
		$data['modulo'] = 7;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$data['title'] = "Rectificadores | ".$data['infoSitio'][0]['nombre_completo'];

		if ($this->auth->logged_in())
		{

		$this->load->view('layouts/header2', $data);
	    $this->load->view('layouts/navbar_mso_ingreso');
        $this->load->view('ingreso/salas_tecnicas/energia/rectificadores');
		$this->load->view('ingreso/salas_tecnicas/energia/footer_rectificadores');
		
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function ingreso_aires_acondicionados($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jquery-ui.min',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jquery-ui.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 12;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$data['title'] = "Aires Acondicionados | ".$data['infoSitio'][0]['nombre_completo'];

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/aires_acondicionados');
			$this->load->view('ingreso/footer_aires_acondicionados');
		}
		else
		{
			redirect(base_url());
		}
	}
	

   public function ingreso_salastecnicas_mso_clima_aires_acondicionados($id)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jquery-ui.min',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jquery-ui.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();        
       
        $data['id_mso'] = $id;
        //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);   
        $sitio = $info_sala_tecnica[0]['sitio'];
        $data['id_sitio']  = $sitio;
        //id_sitio

		$data['sitio'] = $sitio;
		$data['modulo'] = 12;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$data['title'] = "Aires Acondicionados | ".$data['infoSitio'][0]['nombre_completo'];

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2',$data);			
			$this->load->view('layouts/navbar_mso_ingreso');
			$this->load->view('ingreso/salas_tecnicas/clima/aires_acondicionados');
			$this->load->view('ingreso/salas_tecnicas/clima/footer_aires_acondicionados');
		}
		else
		{
			redirect(base_url());
		}


	}



	public function ingreso_bancos_baterias($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jquery-ui.min',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jquery-ui.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 8;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$data['title'] = "Bancos de Baterías | ".$data['infoSitio'][0]['nombre_completo'];

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/bancos_baterias');
			$this->load->view('ingreso/footer_bancos_baterias');
		}
		else
		{
			redirect(base_url());
		}
	}
	

	public function ingreso_grupos_electrogenos($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 6;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/grupos_electrogenos');
			$this->load->view('ingreso/footer_grupos_electrogenos');
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function ingreso_mso_grupos_electrogenos($mso)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['mso'] = $mso;
		$data['modulo'] = 6;

		 //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($mso);        
        $data['id_sitio']  = $info_sala_tecnica[0]['sitio'];
        //id_sitio

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/salas_tecnicas/energia/grupos_electrogenos');
		$this->load->view('ingreso/salas_tecnicas/energia/footer_grupos_electrogenos');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_salastecnicas_mso_energia_grupos_electrogenos($id){

		$data['css'] = array(
							 	'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'selectize',
								'app'
							);	
								
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'selectize',
								'angular-selectize'
							);
        if ($this->auth->logged_in())		
		{
		$data['session'] = $this->session->userdata();
		$data['id_mso'] = $id;
		$data['modulo'] = 6;
		
        //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);        
        $data['id_sitio']  = $info_sala_tecnica[0]['sitio'];
        //id_sitio

	  $this->load->view('layouts/header2', $data);
	  $this->load->view('layouts/navbar_mso_ingreso');
      $this->load->view('ingreso/salas_tecnicas/energia/grupos_electrogenos');
	  $this->load->view('ingreso/salas_tecnicas/energia/footer_grupos_electrogenos');
		}
		else
		{
		  redirect(base_url());
		}
    }


    public function ingreso_salastecnicas_mso_energia_bancos_baterias($id)
    {
    		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jquery-ui.min',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jquery-ui.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();
		$data['id_mso'] = $id;
        //id_sitio
        $this->load->model("Mso_salas_tecnicas_model");
        $info_sala_tecnica = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id); 
        $sitio = $info_sala_tecnica[0]['sitio'];       
        $data['id_sitio']  =  $sitio; 
        //id_sitio
        $data['sitio']  =  $sitio;
		$data['modulo'] = 8;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$data['title'] = "Bancos de Baterías | ".$data['infoSitio'][0]['nombre_completo'];
		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/navbar_mso_ingreso');
			$this->load->view('ingreso/salas_tecnicas/energia/bancos_baterias');
			$this->load->view('ingreso/salas_tecnicas/energia/footer_bancos_baterias');
		}
		else
		{
			redirect(base_url());
		}
    }

	public function ingreso_nodo_b($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'handsontable.full',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'handsontable.full'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 13;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/nodo_b');
			$this->load->view('ingreso/footer_nodo_b');
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function ingreso_microondas($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'handsontable.full',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'handsontable.full'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 14;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/microondas');
			$this->load->view('ingreso/footer_microondas');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_wimax($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'handsontable.full',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'handsontable.full'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 15;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/wimax');
			$this->load->view('ingreso/footer_wimax');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_iden($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'handsontable.full',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'handsontable.full'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;
		$data['modulo'] = 16;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/iden');
			$this->load->view('ingreso/footer_iden');
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function ingreso_documentacion($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jstree_style.min',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jstree.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/documentacion_ftp');
			$this->load->view('ingreso/footer_documentacion_ftp');
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function ingreso_documentacion_sftp($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'jstree_style.min',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'jstree.min'
							);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model("Status_site_model");
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/documentacion_sftp');
			$this->load->view('ingreso/footer_documentacion_sftp');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function ingreso_capacity($sitio)
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'app',
								'nouislider'
			);

		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'rulers',
								'wNumb',
								'nouislider'
			);

		$data['session'] = $this->session->userdata();
		$data['sitio'] = $sitio;

		$this->load->model('Status_site_model');
		$data['infoSitio'] = $this->Status_site_model->edit($sitio);
		$data['title'] = 'Tipo de Torre | '.$data['infoSitio'][0]['nombre_completo'];
       
        $altura_desde_edificio = $data['infoSitio'][0]['altura_torre']+$data['infoSitio'][0]['altura_edificacion'];

        $data['altura_desde_edificio'] = $altura_desde_edificio;
        $data['altura_edificacionx'] = $data['infoSitio'][0]['altura_edificacion'];
        
        $data['partes'] = ($altura_desde_edificio/10);
       

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('layouts/header_opciones_ingreso');
			$this->load->view('ingreso/capacity');
			$this->load->view('ingreso/footer_capacity');
		}
		else
		{
			redirect(base_url(), 'refresh');
		}
	}

	public function perfil_inicio()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2', $data);
			$this->load->view('perfil/inicio');
			$this->load->view('layouts/footer2');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function perfil_contrasena()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();

		if ($this->auth->logged_in())
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('perfil/contrasena');
			$this->load->view('perfil/footer_contrasena');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function reportes_inicio()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();

		$this->load->view('layouts/header2', $data);
		$this->load->view('reportes/inicio');
		$this->load->view('layouts/footer2');
	}

	public function reportes_predeterminados()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();

		$this->load->view('layouts/header2', $data);
		$this->load->view('reportes/predeterminados');
		$this->load->view('reportes/footer_predeterminados');
	}

	public function reportes_personalizados()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup',
								'selectize',
								'app'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min',
								'selectize',
								'angular-selectize'
							);

		$data['session'] = $this->session->userdata();

		$this->load->view('layouts/header2', $data);
		$this->load->view('reportes/personalizados');
		$this->load->view('reportes/footer_personalizados');
	}

	/*public function evaluaciones()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$data['session'] = $this->session->userdata();

		$this->load->view('layouts/header2', $data);
		$this->load->view('evaluaciones/inicio');
		$this->load->view('evaluaciones/footer_inicio');
	}

	public function evaluaciones_evaluar_desempeno()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$session = $this->session->userdata();
		$this->session->set_userdata($session);

		$data['session'] = $this->session->userdata();
		if($session['logged_in'] && ($session['evaluador'] == 1))
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('evaluaciones/evaluar_desempeno');
			$this->load->view('evaluaciones/footer_evaluar_desempeno');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function evaluacion_evaluar_desempeno($evaluacionPeriodo)
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');

		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$session = $this->session->userdata();
		$this->session->set_userdata($session);

		$data['session'] = $this->session->userdata();

		$data['evaluacionPeriodo'] = $evaluacionPeriodo;
		if($session['logged_in'] && ($session['evaluador'] == 1))
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('evaluaciones/evaluacion_evaluar_desempeno');
			$this->load->view('evaluaciones/footer_evaluacion_evaluar_desempeno');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function evaluacion_evaluar_desempeno_prueba($evaluacionPeriodo)
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');

		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$session = $this->session->userdata();
		$this->session->set_userdata($session);

		$data['session'] = $this->session->userdata();

		$data['evaluacionPeriodo'] = $evaluacionPeriodo;
		if($session['logged_in'] && ($session['evaluador'] == 1))
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('evaluaciones/evaluacion_evaluar_desempeno_prueba');
			$this->load->view('evaluaciones/footer_evaluacion_evaluar_desempeno_prueba');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function evaluado_evaluar_desempeno($evaluacionPeriodo,$evaluado)
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');

		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$session = $this->session->userdata();
		$this->session->set_userdata($session);

		$data['session'] = $this->session->userdata();

		$data['evaluacionPeriodo'] = $evaluacionPeriodo;
		$data['evaluado'] = $evaluado;
		if($session['logged_in'] && ($session['evaluador'] == 1))
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('evaluaciones/evaluado_evaluar_desempeno');
			$this->load->view('evaluaciones/footer_evaluado_evaluar_desempeno');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function evaluaciones_visualizar_desempeno()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');

		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$session = $this->session->userdata();
		$this->session->set_userdata($session);

		$data['session'] = $this->session->userdata();

		$this->load->view('layouts/header2',$data);
		$this->load->view('evaluaciones/visualizar_desempeno');
		$this->load->view('evaluaciones/footer_visualizar_desempeno');
	}

	public function evaluaciones_administar()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');

		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$session = $this->session->userdata();
		$this->session->set_userdata($session);

		$data['session'] = $this->session->userdata();

		if($session['logged_in'] && isAdmin($session["id_usuario"]))
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('evaluaciones/administrar_inicio');
			$this->load->view('layouts/footer2');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function evaluaciones_administar_nueva_evaluacion()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');

		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'jquery-ui.min',
								'selectize',
								'bpopup'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'jquery-ui.min',
								'selectize',
								'angular-selectize',
								'bpopup.min'
							);

		$session = $this->session->userdata();
		$this->session->set_userdata($session);

		$data['session'] = $this->session->userdata();
		if($session['logged_in'] && isAdmin($session["id_usuario"]))
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('evaluaciones/nueva_evaluacion');
			$this->load->view('evaluaciones/footer_nueva_evaluacion');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function evaluaciones_administar_evaluaciones()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');

		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'bpopup'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min',
								'bpopup.min'
							);

		$session = $this->session->userdata();
		$this->session->set_userdata($session);

		$data['session'] = $this->session->userdata();
		if($session['logged_in'] && isAdmin($session["id_usuario"]))
		{
			$this->load->view('layouts/header2',$data);
			$this->load->view('evaluaciones/administrar_evaluaciones');
			$this->load->view('evaluaciones/footer_administrar_evaluaciones');
		}
		else
		{
			redirect(base_url());
		}
	}*/

	public function reestablecer_contrasena()
	{
		$encoded_token = $this->uri->segment(2);
		if (strlen($encoded_token) >= 42)
		{
			$this->load->model('Usuarios_model');
			$token = $this->base64url_decode($encoded_token);
	        $cleanToken = $this->security->xss_clean($token);

			if ( ! $this->Usuarios_model->isTokenValid($cleanToken))
			{
				show_404();
			}
			else
			{
				$data['css'] = array(
										'bootstrap.min',
										'font-awesome.min',
										'estilo_nav',
										'bpopup',
										'app'
									);
				$data['js'] = array(
										'jquery-1.12.2.min',
										'angular.min',
										'bootstrap.min',
										'bpopup.min'
									);

				$data['session'] = $this->session->userdata();
				$data['token'] = $cleanToken;
				$data['usuario'] = $this->Usuarios_model->isTokenValid($cleanToken);

				if ( ! $this->auth->logged_in())
				{
					$this->load->view('layouts/header2', $data);
					$this->load->view('menu/reestablecer_contrasena');
					$this->load->view('menu/footer_reestablecer_contrasena');
				}
				else
				{
					redirect(base_url());
				}
			}
		}
		else
		{
			show_404();
		}
	}

	function base64url_decode($data) 
	{ 
	    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}
}