<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contratos extends CI_Controller {

	public function listar()
	{
		$this->load->model('Contratos_model');

		$listado = $this->Contratos_model->get_contratos();

		echo json_encode($listado);
	}

	public function buscar()
	{
		$this->load->helper('form');
		$this->load->model('Contratos_model');

		$busqueda = $this->input->post('busqueda');
		$listado = $this->Contratos_model->look_for_contratos($busqueda);

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Contratos_model');

		$sitio = $this->Contratos_model->edit($id);

		echo json_encode($sitio);
	}

	public function actualizar($id)
	{
		$this->load->model('Contratos_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$config = array(
				array(
						'field' => 'id_contrato',
						'label' => 'ID contrato',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe ingresar un %s.'
							)
					)
			);

		$error = array(
			'error' => FALSE);

		$data = $this->input->input_stream();

		$this->form_validation->set_data($data);
		$this->form_validation->set_rules($config);

		if($this->form_validation->run() === FALSE)
		{
			$error['error'] = validation_errors();
			echo json_encode($error);
		}
		else
		{
			$info = array();
			$input = $this->input->input_stream();
			foreach ($input as $key => $value)
			{
				if($key!='created_at')
				{
					if ($key=='fecha_inicio' || $key=='fecha_fin')
					{
						$info[$key]=$value;
					}
					elseif ($key=='area')
					{
						$info[$key]=floatval($value);
					}
					elseif ($key=='sitio')
					{
						$info[$key]=intval($value);
					}
					else
					{
						$info[$key]=$value;
					}
				}
			}

			if($this->Contratos_model->update($id, $info))
			{
				$this->load->library('session');
				$session = $this->session->userdata();

				$evento = array(
						"accion"	=> "Actualización de Contrato",
						"tabla"		=> "tb_contratos",
						"id"		=> $id
					);

				$this->load->model('Logs_contratos_model');
				$this->Logs_contratos_model->actualizacion_de_datos($session['id_usuario'],$evento);
			}
		}
	}

	public function crear()
	{
		$this->load->model('Contratos_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$config = array(
				array(
						'field' => 'id_contrato',
						'label' => 'ID contrato',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe ingresar un %s.'
							)
					)
			);

		$this->form_validation->set_rules($config);

		$error = array(
			'error' => FALSE);

		if($this->form_validation->run() === FALSE)
		{
			$error['error'] = validation_errors();
			echo json_encode($error);
		}
		else
		{
			$info = $this->input->post();
			$info_contrato["sitio"] = $info["sitio"];
			$info_contrato["id_contrato"] = $info["id_contrato"];
			$info_contrato["fecha_inicio"] = $info["fecha_inicio"];
			$info_contrato["arrendador"] = $info["arrendador"];
			$info_contrato["area"] = $info["area"];

			if($info["tiene_fecha_fin"] != 'true')
			{
				$info_contrato["fecha_fin"] = NULL;
			}
			else
			{
				$info_contrato["fecha_fin"] = $info["fecha_fin"];
			}

			$respuesta = $this->Contratos_model->insert($info_contrato);
			if($respuesta["resp"])
			{
				$this->load->library('session');
				$session = $this->session->userdata();

				$evento = array(
						"accion"	=> "Creación de Contrato",
						"tabla"		=> "tb_contratos"
					);

				$this->load->model('Logs_contratos_model');
				$this->Logs_contratos_model->ingreso_de_datos($session['id_usuario'],$evento,"tb_Contratos");
			}
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Contratos_model');

		$info = $this->input->input_stream();

		$info_contrato['eliminado'] = 1;

		if($this->Contratos_model->update($id, $info_contrato))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Contrato",
					"tabla"			=> "tb_contratos",
					"id"			=> $id
				);

			$this->load->model('Logs_contratos_model');
			$this->Logs_contratos_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);
		}
	}

	/************************************/
	/* FUNCIONES DE VALIDACIÓN DE DATOS */
	/************************************/

	
}