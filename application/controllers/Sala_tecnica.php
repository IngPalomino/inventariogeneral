<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sala_tecnica extends CI_Controller
{
	


	public function __construct()
	{  
		parent::__construct();
		$this->load->library('auth');		
		$this->load->model('Mso_salas_tecnicas_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->load->library('session');
	}

	public function listar()
	{

		if ($this->auth->logged_in())
		{

        $lista = $this->Mso_salas_tecnicas_model->get_sala_tecnicas();  
		echo json_encode($lista);

		}
		else
		{
		  echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
		
	}

    public function listar_sala($id)
	{

		if ($this->auth->logged_in())
		{
        $lista = $this->Mso_salas_tecnicas_model->get_sala_tecnica_id($id);  
		echo json_encode($lista);
		}
		else
		{
		 echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
		
	}

	 




}