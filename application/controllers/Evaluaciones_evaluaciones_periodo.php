<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluaciones_evaluaciones_periodo extends CI_Controller {
	public function listar()
	{
		$this->load->model('Evaluaciones_evaluaciones_periodo_model');

		$listado = $this->Evaluaciones_evaluaciones_periodo_model->listar();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Evaluaciones_evaluaciones_periodo_model');

		$listado = $this->Evaluaciones_evaluaciones_periodo_model->editar($id);

		echo json_encode($listado);
	}

	public function actualizar($id)
	{

	}

	public function insertar()
	{
		
	}

	public function borrar()
	{
		
	}

	public function listar_pendientes()
	{
		$this->load->model('Evaluaciones_evaluaciones_periodo_model');

		$listado = $this->Evaluaciones_evaluaciones_periodo_model->listar_pendientes();

		echo json_encode($listado);
	}

	public function listar_anios($evaluacion)
	{
		$this->load->model('Evaluaciones_evaluaciones_periodo_model');

		$listado = $this->Evaluaciones_evaluaciones_periodo_model->listar_anios($evaluacion);

		echo json_encode($listado);
	}

	public function listar_periodos($evaluacion,$anio)
	{
		$this->load->model('Evaluaciones_evaluaciones_periodo_model');

		$listado = $this->Evaluaciones_evaluaciones_periodo_model->listar_periodos($evaluacion,$anio);

		echo json_encode($listado);
	}

	public function info_avance_desempeno($id)
	{
		$this->load->model('Evaluaciones_evaluaciones_periodo_model');

		$listado = $this->Evaluaciones_evaluaciones_periodo_model->info_avance_desempeno($id);

		echo json_encode($listado);
	}
}

?>