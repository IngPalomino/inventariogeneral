<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class E_antenas extends CI_Controller {
	
	public function listar()
	{
		$this->load->model('E_antenas_model');
		$listado = $this->E_antenas_model->get_antenas();
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('E_antenas_model');
		$listado = $this->E_antenas_model->get_antenas_sitio($id);
		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('E_antenas_model');
		$antena = $this->E_antenas_model->edit($id);
		echo json_encode($antena);
	}

	public function actualizar()
	{
		$this->load->model('E_antenas_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = $this->input->input_stream();
		
		$info["data"] = json_decode($info["data"],true);

		if(count($info["data"]) > 0)
		{
			foreach($info["data"] as $antena)
			{
				$info_antena["nefdn"] = $antena["nefdn"];
				$info_antena["antenna_device_id"] = $antena["antenna_device_id"];
				$info_antena["antenna_device_type"] = $antena["antenna_device_type"];
				$info_antena["date_manufacture_physical"] = str_replace("/","-",$antena["date_manufacture_physical"]);
				$info_antena["date_manufacture_physical"] = $info_antena["date_manufacture_physical"]? date("Y-m-d", strtotime($info_antena["date_manufacture_physical"])) : NULL;
				$info_antena["date_manufacture_m2000"] = str_replace("/","-",$antena["date_manufacture_m2000"]);
				$info_antena["date_manufacture_m2000"] = $info_antena["date_manufacture_m2000"]? date("Y-m-d", strtotime($info_antena["date_manufacture_m2000"])) : NULL;
				$info_antena["bom_code"] = $antena["bom_code"];
				$info_antena["manufacturer_data"] = $antena["manufacturer_data"];
				$info_antena["antenna_model"] = $antena["antenna_model"];
				$info_antena["serial_number_motor"] = $antena["serial_number_motor"];
				$info_antena["unit_position"] = $antena["unit_position"];
				$info_antena["vendor_name"] = $antena["vendor_name"];
				$info_antena["hardware_version"] = $antena["hardware_version"];
				$this->E_antenas_model->update($antena["id"],$info_antena);
			}
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('E_antenas_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["data"] = json_decode($info["data"],true);

		if(count($info["data"]) > 0)
		{
			foreach($info["data"] as $antena)
			{
				$antena["sitio"] = $id;
				$antena["date_manufacture_physical"] = str_replace("/","-",$antena["date_manufacture_physical"]);
				$antena["date_manufacture_physical"] = $antena["date_manufacture_physical"]? date("Y-m-d", strtotime($antena["date_manufacture_physical"])) : NULL;
				$antena["date_manufacture_m2000"] = str_replace("/","-",$antena["date_manufacture_m2000"]);
				$antena["date_manufacture_m2000"] = $antena["date_manufacture_m2000"]? date("Y-m-d", strtotime($antena["date_manufacture_m2000"])) : NULL;
				$this->E_antenas_model->insert($antena);
			}
		}
	}

	public function eliminar()
	{
		$this->load->model('E_antenas_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = array();
		$info = $this->input->input_stream();
		
		$info["data"] = json_decode($info["data"],true);
		
		foreach($info["data"] as $antena)
		{
			$this->E_antenas_model->delete($antena);
		}
	}
}