<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prueba extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Prueba_model');
	}
	
	public function listar()
	{
		echo json_encode($this->Prueba_model->test());
	}

	public function listar2()
	{
		echo json_encode($this->Prueba_model->test2());
	}
}