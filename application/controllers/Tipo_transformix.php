<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_transformix extends CI_Controller {

	public function listar()
	{
		$this->load->model('Tipo_transformix_model');

		$listado = $this->Tipo_transformix_model->get_types();

		echo json_encode($listado);
	}

	public function id_tipo()
	{
		$this->load->model('Tipo_transformix_model');

		$info = array();
		$info = $this->input->post();

		$tipo = $this->Tipo_transformix_model->get_type($info);

		echo json_encode($tipo);
	}

	public function editar($id)
	{
		$this->load->model('Tipo_transformix_model');

		$antena = $this->Tipo_transformix_model->edit($id);

		echo json_encode($antena);
	}

	public function actualizar($id)
	{
		$this->load->model('Tipo_transformix_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$this->Tipo_transformix_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Tipo_transformix_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Tipo_transformix_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Tipo_transformix_model');

		$this->Tipo_transformix_model->delete($id);
	}
}