<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Objetos_bodegas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Bodega_antenas_gul_model');
		$this->load->model('Bodega_antenas_iden_model');
		$this->load->model('Bodega_bancos_baterias_model');
		$this->load->model('Bodega_elementos_mw_model');
		$this->load->model('Bodega_grupos_electrogenos_model');
		$this->load->model('Bodega_modulos_rectif_model');
		$this->load->model('Bodega_aires_acondicionados_model');
		$this->load->model('Bodega_tanques_combustible_model');
		$this->load->model('Bodega_lineas_electricas_model');
		$this->load->model('Bodega_elementos_sistema_combustible_model');
	}

	public function listar()
	{
		$this->load->model('Objetos_bodegas_model');
		echo json_encode($this->Objetos_bodegas_model->get_types($this->input->post()));
	}


	// public function listar_sistema_combustible_test()
	// {

 //    $listado_elementos_sistema_combustible = $this->Bodega_elementos_sistema_combustible_model->get_elementos_sistema_combustible();
   
 //     $lista =  json_decode($listado_elementos_sistema_combustible, true);
 //    //var_dump($lista);
 //     echo count($lista);
 //    }  


	public function busqueda()
	{
		$filtros = $this->input->post();

		$listado = array();

		if(isset($filtros['objeto']))
		{
			switch ($filtros['objeto'])
			{
				case 'antenas_gul':
					$listado_antenas_gul = $this->Bodega_antenas_gul_model->get_antenas_gul();
					break;
				case 'antenas_iden':
					$listado_antenas_iden = $this->Bodega_antenas_iden_model->get_antenas_iden();
					break;
				case 'bancos_baterias':
					$listado_bancos_baterias = $this->Bodega_bancos_baterias_model->get_bancos_baterias();
					break;
				case 'elementos_mw':
					$listado_elementos_mw = $this->Bodega_elementos_mw_model->get_elementos_mw();
					break;
				case 'grupos_electrogenos':
					$listado_grupos_electrogenos = $this->Bodega_grupos_electrogenos_model->get_grupos_electrogenos();
					break;
				case 'modulos_rectif':
					$listado_modulos_rectif = $this->Bodega_modulos_rectif_model->get_modulos_rectif();
					break;
				case 'aires_acondicionados':
					$listado_aires_acondicionados = $this->Bodega_aires_acondicionados_model->get_aires_acondicionados();
					break;
				case 'tanques_combustible':
					$listado_tanques_combustible = $this->Bodega_tanques_combustible_model->get_tanques_combustible();
					break;

				case 'elementos_sistema_combustible':
					$listado_elementos_sistema_combustible = $this->Bodega_elementos_sistema_combustible_model->get_elementos_sistema_combustible();
					break;
					
				case 'lineas_electricas':
					$listado_lineas_electricas = $this->Bodega_lineas_electricas_model->get_lineas_electricas();
			}
		}
		else
		{
			$listado_antenas_gul = $this->Bodega_antenas_gul_model->get_antenas_gul($filtros);
			$listado_antenas_iden = $this->Bodega_antenas_iden_model->get_antenas_iden($filtros);
			$listado_bancos_baterias = $this->Bodega_bancos_baterias_model->get_bancos_baterias($filtros);
			$listado_elementos_mw = $this->Bodega_elementos_mw_model->get_elementos_mw($filtros);
			$listado_grupos_electrogenos = $this->Bodega_grupos_electrogenos_model->get_grupos_electrogenos($filtros);
			$listado_modulos_rectif = $this->Bodega_modulos_rectif_model->get_modulos_rectif($filtros);
			$listado_aires_acondicionados = $this->Bodega_aires_acondicionados_model->get_aires_acondicionados($filtros);
			$listado_tanques_combustible = $this->Bodega_tanques_combustible_model->get_tanques_combustible($filtros);
			$listado_lineas_electricas = $this->Bodega_lineas_electricas_model->get_lineas_electricas($filtros);

            $listado_elementos_sistema_combustible = $this->Bodega_elementos_sistema_combustible_model->get_elementos_sistema_combustible($filtros);


		}

		if(isset($listado_antenas_gul) && count($listado_antenas_gul) > 0)
		{
			$antenas_gul_array['objeto'] = 'Antenas GUL';
			$antenas_gul_array['cantidad'] = count($listado_antenas_gul);
			$antenas_gul_array['info'] = $listado_antenas_gul;
			$antenas_gul_array['link'] = 'antenas_gul';
			$antenas_gul_array['tipo'] = 'tipo_antena_gul';
			$antenas_gul_array['logs'] = 'logs_bodega_antenas_gul';

			array_push($listado, $antenas_gul_array);
		}
		
		if(isset($listado_antenas_iden) && count($listado_antenas_iden) > 0)
		{
			$antenas_iden_array['objeto'] = 'Antenas iDEN';
			$antenas_iden_array['cantidad'] = count($listado_antenas_iden);
			$antenas_iden_array['info'] = $listado_antenas_iden;
			$antenas_iden_array['link'] = 'antenas_iden';
			$antenas_iden_array['tipo'] = 'tipo_antena_iden';
			$antenas_iden_array['logs'] = 'logs_bodega_antenas_iden';

			array_push($listado, $antenas_iden_array);
		}
		
		if(isset($listado_bancos_baterias) && count($listado_bancos_baterias) > 0)
		{
			$bancos_baterias_array['objeto'] = 'Bancos de Baterías';
			$bancos_baterias_array['cantidad'] = count($listado_bancos_baterias);
			$bancos_baterias_array['info'] = $listado_bancos_baterias;
			$bancos_baterias_array['link'] = 'bancos_baterias';
			$bancos_baterias_array['tipo'] = 'tipo_bancos_baterias';
			$bancos_baterias_array['logs'] = 'logs_bodega_bancos_baterias';

			array_push($listado, $bancos_baterias_array);
		}
		
		if(isset($listado_elementos_mw) && count($listado_elementos_mw) > 0)
		{
			$elementos_mw_array['objeto'] = 'Elementos Microondas';
			$elementos_mw_array['cantidad'] = count($listado_elementos_mw);
			$elementos_mw_array['info'] = $listado_elementos_mw;
			$elementos_mw_array['link'] = 'elementos_mw';
			$elementos_mw_array['tipo'] = 'tipo_elemento_mw';
			$elementos_mw_array['logs'] = 'logs_bodega_elementos_mw';

			array_push($listado, $elementos_mw_array);
		}
		
		if(isset($listado_grupos_electrogenos) && count($listado_grupos_electrogenos) > 0)
		{
			$grupos_electrogenos_array['objeto'] = 'Grupos Electrógenos';
			$grupos_electrogenos_array['cantidad'] = count($listado_grupos_electrogenos);
			$grupos_electrogenos_array['info'] = $listado_grupos_electrogenos;
			$grupos_electrogenos_array['link'] = 'grupos_electrogenos';
			$grupos_electrogenos_array['tipo'] = 'tipo_grupo_electrogeno';
			$grupos_electrogenos_array['logs'] = 'logs_bodega_grupos_electrogenos';

			array_push($listado, $grupos_electrogenos_array);
		}

		if(isset($listado_modulos_rectif) && count($listado_modulos_rectif) > 0)
		{
			$modulos_rectif_array['objeto'] = 'Módulos Rectificadores';
			$modulos_rectif_array['cantidad'] = count($listado_modulos_rectif);
			$modulos_rectif_array['info'] = $listado_modulos_rectif;
			$modulos_rectif_array['link'] = 'modulos_rectif';
			$modulos_rectif_array['tipo'] = 'tipo_modulos_rectif';
			$modulos_rectif_array['logs'] = 'logs_bodega_modulos_rectif';

			array_push($listado, $modulos_rectif_array);
		}

		if(isset($listado_aires_acondicionados) && count($listado_aires_acondicionados) > 0)
		{
			$aires_acondicionados_array['objeto'] = 'Aires Acondicionados';
			$aires_acondicionados_array['cantidad'] = count($listado_aires_acondicionados);
			$aires_acondicionados_array['info'] = $listado_aires_acondicionados;
			$aires_acondicionados_array['link'] = 'aires_acondicionados';
			$aires_acondicionados_array['tipo'] = 'tipo_aires_acondicionados';
			$aires_acondicionados_array['logs'] = 'logs_bodega_aires_acondicionados';

			array_push($listado, $aires_acondicionados_array);
		}

		if(isset($listado_tanques_combustible) && count($listado_tanques_combustible) > 0)
		{
			$tanques_combustible_array['objeto'] = 'Tanques de Combustible';
			$tanques_combustible_array['cantidad'] = count($listado_tanques_combustible);
			$tanques_combustible_array['info'] = $listado_tanques_combustible;
			$tanques_combustible_array['link'] = 'tanques_combustible';
			$tanques_combustible_array['tipo'] = 'tipo_tanque_combustible';
			$tanques_combustible_array['logs'] = 'logs_bodega_tanques_combustible';

			array_push($listado, $tanques_combustible_array);
		}

		if (isset($listado_lineas_electricas) && count($listado_lineas_electricas) > 0)
		{
			$lineas_electricas_array['objeto'] = 'Elementos de Líneas Eléctricas';
			$lineas_electricas_array['cantidad'] = count($listado_lineas_electricas);
			$lineas_electricas_array['info'] = $listado_lineas_electricas;
			$lineas_electricas_array['link'] = 'lineas_electricas';
			$lineas_electricas_array['tipo'] = 'tipo_elemento';
			$lineas_electricas_array['logs'] = 'logs_bodega_lineas_electricas';

			array_push($listado, $lineas_electricas_array);
		}
		

       
       if(isset($listado_elementos_sistema_combustible) && count($listado_elementos_sistema_combustible) > 0)
		{
			$elementos_sistema_combustible_array['objeto'] = 'Elementos Sistema Combustible';
			//depende del caso de como traigas la data
			$elementos_sistema_combustible_array['cantidad'] = count(json_decode($listado_elementos_sistema_combustible, true));

			$elementos_sistema_combustible_array['info'] = $listado_elementos_sistema_combustible;
			$elementos_sistema_combustible_array['link'] = 'elementos_sistema_combustible';
			$elementos_sistema_combustible_array['tipo'] = 'tipo_elemento_sistema_combustible';
			$elementos_sistema_combustible_array['logs'] = 'logs_bodega_elementos_sistema_combustible';

			array_push($listado, $elementos_sistema_combustible_array);
		}


		echo json_encode($listado);
	}
}