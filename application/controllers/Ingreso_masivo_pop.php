<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_masivo_pop extends CI_Controller {

	private $modulo;

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('auth', 'excel'));
		$this->modulo = 2;
	}

	public function plantilla_pop()
	{
		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Plantilla POP')
					->setSubject('Plantilla')
					->setDescription('Plantilla para ingresar información de POP al Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('POP');

		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Latitud (°)')
					->setCellValue('D1', 'Longitud (°)')
					->setCellValue('E1', 'Tipo de Torre')
					->setCellValue('F1', 'Altura de Torre (m)')
					->setCellValue('G1', 'Factor Uso de Torre (%)')
					->setCellValue('H1', 'Tipo de Estación')
					->setCellValue('I1', 'Altura de Edificación (m)')
					->setCellValue('J1', 'Tipo Mimetizado')
					->setCellValue('K1', 'Tipo de Proyecto')
					->setCellValue('L1', 'Coubicado en')
					->setCellValue('M1', 'Operador Coubicado');

		$styleArray = array(
				'font' => array(
						'bold'	=> TRUE,
						'color'	=> array('rgb' => 'FFFFFF'),
						'size'	=> 11,
						'name'	=> 'Verdana'
					),
				'fill' => array(
						'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
						'color'	=> array('rgb' => '6788D3')
					),
				'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle('A1:M1')->applyFromArray($styleArray);

		$activeSheet->getStyle('A:A')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$activeSheet->getColumnDimension('A')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('B')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('C')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('D')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('E')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('F')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('G')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('H')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('I')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('J')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('K')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('L')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('M')->setAutoSize(TRUE);

		$this->excel->createSheet();

		$csheet = 1;
		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Contratos');

		$activeSheet->setCellValue('A1', 'Código')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'ID Contrato')
					->setCellValue('D1', 'Departamento')
					->setCellValue('E1', 'Provincia')
					->setCellValue('F1', 'Distrito')
					->setCellValue('G1', 'Dirección')
					->setCellValue('H1', 'Área de piso (m2)')
					->setCellValue('I1', 'Fecha inicio contrato')
					->setCellValue('J1', 'Fecha fin contrato')
					->setCellValue('K1', 'Arrendador')
					->setCellValue('L1', 'Contactos')
					->setCellValue('M1', 'Teléfonos')
					->setCellValue('N1', 'Correos')
					->setCellValue('A2', '013XXXXX')
					->setCellValue('B2', '013XXXXX_Nombre_Sitio')
					->setCellValue('L2', 'Contacto1 / Contacto2 / Contacto3')
					->setCellValue('M2', 'Teléfono1 / Teléfono2 / Teléfono3')
					->setCellValue('N2', 'Correo1 / Correo2 / Correo3');

		$styleArray = array(
				'font' => array(
						'bold'	=> TRUE,
						'color'	=> array('rgb' => 'FFFFFF'),
						'size'	=> 11,
						'name'	=> 'Verdana'
					),
				'fill' => array(
						'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
						'color'	=> array('rgb' => '6788D3')
					),
				'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle('A1:N1')->applyFromArray($styleArray);

		$activeSheet->getStyle('A:A')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$activeSheet->getColumnDimension('A')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('B')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('C')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('D')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('E')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('F')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('G')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('H')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('I')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('J')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('K')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('L')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('M')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('N')->setAutoSize(TRUE);

		$this->excel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_POP.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function plantilla_desplegables_pop()
	{
		$this->load->model('Tipo_torre_model');
		$this->load->model('Camuflaje_model');
		$this->load->model('Tipo_proyecto_model');
		$this->load->model('Coubicador_model');
		$this->load->model('Coubicante_model');
		$tipo_torre = $this->Tipo_torre_model->get_tipo_torre();
		$tipo_mimetizado = $this->Camuflaje_model->get_camuflaje();
		$tipo_proyecto = $this->Tipo_proyecto_model->get_proyecto();
		$coubicador = $this->Coubicador_model->get_Coubicador();
		$coubicante = $this->Coubicante_model->get_Coubicante();

		$this->excel->getProperties()
					->setCreator('OyM')
					->setLastModifiedBy('OyM')
					->setTitle('Plantilla POP')
					->setSubject('Plantilla')
					->setDescription('Plantilla para ingresar información de POP al Inventario General.')
					->setKeywords('office 2007 openxml php')
					->setCategory('Test result file');

		$csheet = 0;
		$this->excel->setActiveSheetIndex($csheet);

		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Desplegables');

		$activeSheet->setCellValue('A1', 'Tipo de Torre')
					->setCellValue('C1', 'Tipo Mimetizado')
					->setCellValue('E1', 'Tipo de Proyecto')
					->setCellValue('G1', 'Coubicado en')
					->setCellValue('I1', 'Operador Coubicado');

		$styleArray = array(
				'font' => array(
						'bold'	=> TRUE,
						'color'	=> array('rgb' => 'FFFFFF'),
						'size'	=> 11,
						'name'	=> 'Verdana'
					),
				'fill' => array(
						'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
						'color'	=> array('rgb' => '6788D3')
					),
				'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					)
			);

		$activeSheet->getStyle('A1:A1')->applyFromArray($styleArray);
		$activeSheet->getStyle('C1:C1')->applyFromArray($styleArray);
		$activeSheet->getStyle('E1:E1')->applyFromArray($styleArray);
		$activeSheet->getStyle('G1:G1')->applyFromArray($styleArray);
		$activeSheet->getStyle('I1:I1')->applyFromArray($styleArray);

		$i = 2;

		foreach($tipo_torre as $fila)
		{
			$activeSheet->setCellValue('A'.$i, $fila['tipo_torre']);

			$i++;
		}

		$i = 2;

		foreach($tipo_mimetizado as $fila)
		{
			$activeSheet->setCellValue('C'.$i, $fila['camuflaje']);

			$i++;
		}

		$i = 2;

		foreach($tipo_proyecto as $fila)
		{
			$activeSheet->setCellValue('E'.$i, $fila['tipo_proyecto']);

			$i++;
		}

		$i = 2;

		foreach($coubicador as $fila)
		{
			$activeSheet->setCellValue('G'.$i, $fila['coubicador']);

			$i++;
		}

		$i = 2;

		foreach($coubicante as $fila)
		{
			$activeSheet->setCellValue('I'.$i, $fila['coubicante']);

			$i++;
		}

		$activeSheet->getColumnDimension('A')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('C')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('E')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('G')->setAutoSize(TRUE);
		$activeSheet->getColumnDimension('I')->setAutoSize(TRUE);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Plantilla_Desplegables_POP.xlsx"');
		header('Cache-Control: max-age=0');
		
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function upload_file()
	{
		if($this->auth->logged_in())
		{
			if($this->auth->allowed_module($this->modulo))
			{
				$post = $this->input->post();
				$config['upload_path'] = '/var/www/html/uploads/inventarioGeneral/ingreso_masivo';
				$config['allowed_types'] = 'xlsx';
				$config['max_size'] = '1024';
				$config['overwrite'] = FALSE;
				$config['file_name'] = 'plantilla_'.$post['tipo'].'_'.date('d-m-Y-H-i-s');

				$this->load->library('upload', $config);

				if($this->upload->do_upload('archivo'))
				{
					echo json_encode(array(
							'error'		=> FALSE,
							'mensaje'	=> 'Archivo subido con éxito.',
							'resultado'	=> $this->upload->data('file_name')
						));
				}
				else
				{
					echo json_encode(array(
							'error'		=> TRUE,
							'mensaje'	=> 'No se pudo subir el archivo.',
							'resultado'	=> $this->upload->display_errors()
						));
				}
			}
			else
			{
				echo json_encode(array(
						'error'		=> TRUE,
						'mensaje'	=> 'Usted no tiene permiso para realizar esta acción.',
						'resultado'	=> 'forbidden'
					));
			}
		}
		else
		{
			echo json_encode(array(
					'error'		=> TRUE,
					'mensaje'	=> 'Su sesión expiró',
					'resultado'	=> 'logged_off'
				));
		}
	}

	public function check_file()
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->allowed_module($this->modulo))
			{
				$this->load->model('Ingreso_masivo_pop_model');
				$post = $this->input->post();
				$file = '/var/www/html/uploads/inventarioGeneral/ingreso_masivo/'.$post['file'];
				
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						'error'			=> FALSE,
						'advertencia'	=> FALSE,
						'mensajes'		=> array(),
						'advertencias'	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				foreach ($sheetNames as $hoja)
				{
					if ($hoja == 'POP')
					{
						try{ $objPHPExcel->setActiveSheetIndexByName($hoja); } catch(Exception $e){ continue; }

						$activeSheet = $objPHPExcel->getActiveSheet();
						$cell_collection = $activeSheet->getCellCollection();
						$cabeceras = array('Código', 'Nombre', 'Latitud (°)', 'Longitud (°)', 'Tipo de Torre', 'Altura de Torre (m)', 'Factor Uso de Torre (%)', 'Tipo de Estación', 'Altura de Edificación (m)', 'Tipo Mimetizado', 'Tipo de Proyecto', 'Coubicado en', 'Operador Coubicado');
						$cabeceras_no_validas = array();
						$cabeceras_validas = array();
						$arr_data = array();

						foreach ($cell_collection as $cell)
						{
							$column = $activeSheet->getCell($cell)->getColumn();
							$row = $activeSheet->getCell($cell)->getRow();
							$data_value = $activeSheet->getCell($cell)->getValue();

							if ($row == 1)
							{
								$header[$row][$column] = $data_value;

								foreach ($header as $filaHeader => $infoHeader)
								{
									if ( ! in_array($infoHeader[$column], $cabeceras))
									{
										array_push($cabeceras_no_validas, $infoHeader[$column]);
									}
									else
									{
										array_push($cabeceras_validas, $infoHeader[$column]);
									}
								}
							}
							else
							{
								if ( ! isset($data_value) && $column == 'A')
								{
									/*break;*/
									$resultados['advertencia'] = TRUE;
									array_push($resultados['advertencias'], "La columna {$column} en la fila {$row} no tiene información, se obviará esta fila.");
								}
								elseif (in_array($header[1][$column], $cabeceras_no_validas))
								{
									
								}
								else
								{
									$arr_data[$row][$column] = $data_value;
								}
							}
						}

						foreach ($cabeceras as &$value)
						{
							if ( ! in_array($value, $cabeceras_validas))
							{
								$resultados['error'] = TRUE;
								array_push($resultados['mensajes'], "La columna {$value} es obligatoria.");
							}
						}

						if (count($cabeceras_no_validas) > 0)
						{
							$cabeceras_no_validas = array_values(array_unique($cabeceras_no_validas));

							foreach ($cabeceras_no_validas as $cabecera)
							{
								$resultados['advertencia'] = TRUE;
								array_push($resultados['advertencias'], "La cabecera {$cabecera} no corresponde a la plantilla original de la hoja {$hoja}, se obviará esta columna.");
							}
						}

						/*echo json_encode($cabeceras_no_validas);*/
						$filas = count($arr_data);

						if (count($arr_data) > 0)
						{
							foreach ($arr_data as $fila => $infoFila)
							{
								$nombreSeparado = explode('_', $infoFila['B']);
								$codigoNombre = $nombreSeparado[0];

								foreach ($infoFila as $columna => $campo)
								{
									if ($header[1][$columna] == 'Código')
									{
										$codigo = $campo;
										if (strlen($campo) != 7)
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Código de sitio en la fila {$fila} no es válido, debe tener 7 dígitos ({$hoja}).");
										}
										elseif ( ! is_numeric($campo))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Código de sitio en la fila {$fila} debe contener únicamente números ({$hoja}).");
										}
										else
										{
											if ($codigoNombre != $campo)
											{
												$resultados['error'] = TRUE;
												array_push($resultados['mensajes'], "El Código de sitio en la fila {$fila} no coincide con el código del Nombre ({$hoja}).");
											}
										}
									}
									elseif ($header[1][$columna] == 'Nombre')
									{
										$nombre = $this->Ingreso_masivo_pop_model->esNombreCodigoValidos($codigo, $campo);
										if ($campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], 'Debe ingresar el Nombre en la fila {$fila} ({$hoja}).');
										}
										elseif ( ! $this->Ingreso_masivo_pop_model->esNombreCompletoValido($campo))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Nombre de sitio en la fila {$fila} no existe en la base de datos ({$hoja}).");
										}
										elseif ( ! $nombre)
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Nombre y Código de sitio en la fila {$fila} no existe en la base de datos ({$hoja}).");
										}
										elseif ( ! $this->auth->allowed_module($this->modulo, $nombre['id']))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Usted no tiene permiso para ingresar/actualizar información en el sitio {$campo} de la fila {$fila} ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Latitud (°)')
									{
										if ($campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar la Latitud (°) en la fila {$fila} ({$hoja}).");
										}
										elseif ( !( ((Float)$campo >= -90) && ((Float)$campo <= 90) ) )
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "La Latitud en la fila {$fila} no es válida ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Longitud (°)')
									{
										if ( ! isset($columna) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar la Longitud (°) en la fila {$fila} ({$hoja}).");
										}
										elseif ( ! ( ((Float)$campo >= -180) && ((Float)$campo <= 180) ) )
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "La Longitud en la fila {$fila} no es válida ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Tipo de Torre')
									{
										if ( ! isset($columna) || ! isset($campo) )
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar el Tipo de Torre en la fila {$fila} ({$hoja}).");
										}
										elseif ($this->Ingreso_masivo_pop_model->esTipoTorreValido($campo) === FALSE)
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Tipo de Torre en la fila {$fila} no existe en la base de datos ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Altura de Torre (m)')
									{
										if ( ! isset($campo))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar la Altura de Torre (m) en la fila {$fila} ({$hoja}).");
										}
										elseif ( ! is_numeric($campo))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "La Altura de Torre (m) en la fila {$fila} debe contener únicamente números ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Factor Uso de Torre (%)')
									{
										if ( ! isset($campo))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar el Factor Uso de la Torre (%) en la fila {$fila} ({$hoja}).");
										}
										elseif ( ! is_numeric($campo))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Factor Uso de la Torre en la fila {$fila} debe contener únicamente números ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Tipo de Estación')
									{
										if ( ! isset($columna) || !isset($campo) )
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar el Tipo de Estación en la fila {$fila} ({$hoja}).");
										}
										elseif ($campo != 'Greenfield' && $campo != 'Rooftop')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Tipo de Estación en la fila {$fila} debe ser 'Greenfield' o 'Rooftop' ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Altura de Edificación (m)')
									{
										if ( ! isset($columna))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar la Altura de Edificación (m) en la fila {$fila} ({$hoja}).");
										}
										elseif ( ! is_numeric($campo) && $campo != 'N/A')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "La Altura de Edificación en la fila {$fila} debe contener únicamente números ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Tipo Mimetizado')
									{
										if ( ! isset($columna) || ! isset($campo) )
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar el Tipo de Mimetizado en la fila {$fila} ({$hoja}).");
										}
										elseif ($this->Ingreso_masivo_pop_model->esTipoMimetizadoValido((($campo == 'N/A') ? 'NO' : $campo)) === FALSE)
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Tipo de Mimetizado en la fila {$fila} no existe en la base de datos ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Tipo de Proyecto')
									{
										if ( ! isset($columna) || ! isset($campo) )
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar el Tipo de Proyecto en la fila {$fila} ({$hoja}).");
										}
										elseif ($this->Ingreso_masivo_pop_model->esTipoProyectoValido($campo) === FALSE)
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Tipo de Proyecto en la fila {$fila} no existe en la base de datos ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Coubicado en')
									{
										if ( ! isset($columna) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar 'Coubicado en' en la fila {$fila} ({$hoja}).");
										}
										elseif ($this->Ingreso_masivo_pop_model->esCoubicadorValido($campo) === FALSE)
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El 'Coubicado en' en la fila {$fila} no existe en la base de datos ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Operador Coubicado')
									{
										if ( ! isset($columna) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar 'Operador Coubicado' en la fila {$fila} ({$hoja}).");
										}
										elseif ($this->Ingreso_masivo_pop_model->esCoubicanteValido($campo) === FALSE)
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El 'Operador Coubicado' en la fila {$fila} no existe en la base de datos ({$hoja}).");
										}
									}
								}
							}
						}
						else
						{
							$resultados['error'] = TRUE;
							array_push($resultados['mensajes'], "La hoja {$hoja} no contiene información, elimine esta hoja si no la utilizará.");
						}
					}
					elseif ($hoja == 'Contratos')
					{
						try{ $objPHPExcel->setActiveSheetIndexByName($hoja); } catch(Exception $e){ continue; }

						$activeSheet = $objPHPExcel->getActiveSheet();
						$cell_collection = $activeSheet->getCellCollection();
						$cabeceras = array('Código', 'Nombre', 'ID Contrato', 'Departamento', 'Provincia', 'Distrito', 'Dirección', 'Área de piso (m2)', 'Fecha inicio contrato', 'Fecha fin contrato', 'Arrendador', 'Contactos', 'Teléfonos', 'Correos');
						$cabeceras_no_validas = array();
						$cabeceras_validas = array();
						$arr_data = array();

						foreach ($cell_collection as $cell)
						{
							$column = $activeSheet->getCell($cell)->getColumn();
							$row = $activeSheet->getCell($cell)->getRow();
							$data_value = $activeSheet->getCell($cell)->getValue();

							if ($row == 1)
							{
								$header[$row][$column] = $data_value;

								foreach ($header as $filaHeader => $infoHeader)
								{
									if ( ! in_array($infoHeader[$column], $cabeceras))
									{
										array_push($cabeceras_no_validas, $infoHeader[$column]);
									}
									else
									{
										array_push($cabeceras_validas, $infoHeader[$column]);
									}
								}
							}
							else
							{
								if ( ! isset($data_value) && $column == 'A')
								{
									/*break;*/
									$resultados['advertencia'] = TRUE;
									array_push($resultados['advertencias'], "La columna $column en la fila {$row} de {$hoja} no tiene información, se obviará esta fila.");
								}
								elseif (in_array($header[1][$column], $cabeceras_no_validas))
								{
									
								}
								else
								{
									$arr_data[$row][$column] = $data_value;
								}
							}
						}

						foreach ($cabeceras as &$value)
						{
							if ( ! in_array($value, $cabeceras_validas))
							{
								$resultados['error'] = TRUE;
								array_push($resultados['mensajes'], "La columna {$value} en la hoja {$hoja} es obligatoria.");
							}
						}

						if (count($cabeceras_no_validas) > 0)
						{
							$cabeceras_no_validas = array_values(array_unique($cabeceras_no_validas));

							foreach ($cabeceras_no_validas as $cabecera)
							{
								$resultados['advertencia'] = TRUE;
								array_push($resultados['advertencias'], "La cabecera {$cabecera} no corresponde a la plantilla original de la hoja {$hoja}, se obviará esta columna.");
							}
						}

						$filas_contratos = count($arr_data);

						if ($filas_contratos > 0)
						{
							foreach ($arr_data as $fila => $infoFila)
							{
								$nombreSeparado = explode('_', $infoFila['B']);
								$codigoNombre = $nombreSeparado[0];

								foreach ($infoFila as $columna => $campo)
								{
									if ($header[1][$columna] == 'Código')
									{
										$codigo = $campo;
										if (strlen($campo) != 7)
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Código de sitio en la fila {$fila} no es válido, debe tener 7 dígitos ({$hoja}).");
										}
										elseif ( ! is_numeric($campo))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Código de sitio en la fila {$fila} debe contener únicamente números ({$hoja}).");
										}
										else
										{
											if ($codigoNombre != $campo)
											{
												$resultados['error'] = TRUE;
												array_push($resultados['mensajes'], "El Código de sitio en la fila {$fila} no coincide con el código del Nombre ({$hoja}).");
											}
										}
									}
									elseif ($header[1][$columna] == 'Nombre')
									{
										$nombre = $this->Ingreso_masivo_pop_model->esNombreCodigoValidos($codigo, $campo);

										if ($campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar el Nombre en la fila {$fila} ({$hoja}).");
										}
										elseif ( ! $this->Ingreso_masivo_pop_model->esNombreCompletoValido($campo))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Nombre de sitio en la fila {$fila} no existe en la base de datos ({$hoja}).");
										}
										elseif ( ! $nombre)
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Nombre y Código de sitio en la fila {$fila} no existe en la base de datos ({$hoja}).");
										}
										elseif ( ! $this->auth->allowed_module($this->modulo, $nombre['id']))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Usted no tiene permiso para ingresar/actualizar información en el sitio {$campo} de la fila {$fila} ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'ID Contrato')
									{
										if ( ! isset($columna) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar el ID de contrato en la fila {$fila} ({$hoja}).");
										}
										elseif ($nombre !== FALSE)
										{
											if ($this->Ingreso_masivo_pop_model->existeContratoEnSitio($campo, $nombre['id']) !== FALSE)
											{
												$resultados['advertencia'] = TRUE;
												array_push($resultados['advertencias'], "El sitio en la fila {$fila} ya tiene un contrato con ID {$campo}, se actualizará este elemento ({$hoja}).");
											}
										}
									}
									elseif ($header[1][$columna] == 'Departamento')
									{
										$departamento = strtoupper($campo);

										if ( ! isset($columna) || ! isset($campo) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar el Departamento en la fila {$fila} ({$hoja}).");
										}
										elseif ( ! $this->Ingreso_masivo_pop_model->departamento_valido($departamento))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Departamento en la fila {$fila} no es válido ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Provincia')
									{
										$provincia = strtoupper($campo);

										if ( ! isset($columna) || ! isset($campo) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar la Provincia en la fila {$fila} ({$hoja}).");
										}
										elseif ( ! $this->Ingreso_masivo_pop_model->provincia_valida($provincia))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "La Provincia en la fila {$fila} no es válida ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Distrito')
									{
										if ( ! isset($columna) || ! isset($campo) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar el Distrito en la fila {$fila} ({$hoja}).");
										}
										elseif ( ! $this->Ingreso_masivo_pop_model->distrito_valido(strtoupper($campo)))
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "El Distrito en la fila {$fila} no es válida ({$hoja}).");
										}
										elseif ($this->Ingreso_masivo_pop_model->zona_peru_valida($departamento, $provincia, strtoupper($campo)) === FALSE)
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "La combinación de Departamento, Provincia y Distrito en la fila {$fila} no es válida ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Dirección')
									{
										if ( ! isset($columna) || ! isset($campo) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar la Dirección en la fila {$fila} ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Área de piso (m2)')
									{
										if ( ! isset($columna) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar el Área de piso (m2) en la fila {$fila} ({$hoja}).");
										}
										elseif ( ! is_numeric($campo))
										{
											$resultados['advertencia'] = TRUE;
											array_push($resultados['advertencias'], "El Área de piso en la fila {$fila} no es númerica, se considerá 'Sin Data' ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Fecha inicio contrato')
									{
										if ( ! isset($columna) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar la Fecha de inicio de contrato la fila {$fila} ({$hoja}).");
										}
										elseif (is_numeric($campo))
										{
											$campo = gmdate('Y-m-d', (($campo - 25569) * 86400));

											if ( ! $this->validateDate($campo))
											{
												$resultados['error'] = TRUE;
												array_push($resultados['mensajes'], "La Fecha de inicio de contrato en la fila {$fila} no es una fecha válida ({$hoja}).");
											}
										}
										elseif (preg_match("/[\/\\\]/", $campo))
										{
											$campo = date('Y-m-d', strtotime(str_replace('/', '-', $campo)));

											if ( ! $this->validateDate($campo))
											{
												$resultados['error'] = TRUE;
												array_push($resultados['mensajes'], "La Fecha de inicio de contrato en la fila {$fila} no es una fecha válida ({$hoja}).");
											}
										}
										else
										{
											$resultados['advertencia'] = TRUE;
											array_push($resultados['advertencias'], "La Fecha de inicio de contrato en la fila {$fila} se considerará 'Sin Data' ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Fecha fin contrato')
									{
										if ( ! isset($columna) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar la Fecha de fin de contrato la fila {$fila} ({$hoja}).");
										}
										elseif (is_numeric($campo))
										{
											$campo = gmdate('Y-m-d', (($campo - 25569) * 86400));

											if ( ! $this->validateDate($campo))
											{
												$resultados['error'] = TRUE;
												array_push($resultados['mensajes'], "La Fecha de fin de contrato en la fila {$fila} no es una fecha válida ({$hoja}).");
											}
										}
										elseif (preg_match("/[\/\\\]/", $campo))
										{
											$campo = date('Y-m-d', strtotime(str_replace('/', '-', $campo)));

											if ( ! $this->validateDate($campo))
											{
												$resultados['error'] = TRUE;
												array_push($resultados['mensajes'], "La Fecha de fin de contrato en la fila {$fila} no es una fecha válida ({$hoja}).");
											}
										}
										else
										{
											$resultados['advertencia'] = TRUE;
											array_push($resultados['advertencias'], "La Fecha de fin de contrato en la fila {$fila} se considerará 'Sin Data' ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Arrendador')
									{
										if ( ! isset($columna) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar el Arrendador en la fila {$fila} ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Contactos')
									{
										if ( ! isset($columna) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar Contactos en la fila {$fila} ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Teléfonos')
									{
										if ( ! isset($columna) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar Teléfonos en la fila {$fila} ({$hoja}).");
										}
									}
									elseif ($header[1][$columna] == 'Correos')
									{
										if ( ! isset($columna) || $campo == '')
										{
											$resultados['error'] = TRUE;
											array_push($resultados['mensajes'], "Debe ingresar Correos en la fila {$fila} ({$hoja}).");
										}
									}
								}
							}
						}
						else
						{
							$resultados['error'] = TRUE;
							array_push($resultados['mensajes'], "La hoja {$hoja} no contiene información, elimine esta hoja si no la utilizará.");
						}
					}
					else
					{
						$resultados['advertencia'] = TRUE;
						array_push($resultados['advertencias'], "La hoja {$hoja} no corresponde a la plantilla original de POP, se obviará esta hoja.");
					}
				}

				if( ! $resultados['error'])
				{
					isset($filas) ? array_push($resultados['mensajes'], 'Filas válidas POP: '.$filas) : '';
					isset($filas_contratos) ? array_push($resultados['mensajes'], 'Filas válidas Contratos: '.$filas_contratos) : '';
				}

				echo json_encode($resultados);

				if($resultados['error'])
				{
					unlink('/var/www/html/uploads/inventarioGeneral/ingreso_masivo/'.$post['file']);
				}
			}
			else
			{
				echo json_encode(array(
						'error'		=> TRUE,
						'mensaje'	=> 'Usted no tiene permiso para realizar esta acción.'
					));
			}
		}
		else
		{
			echo json_encode(array(
					'error'		=> TRUE,
					'mensaje'	=> 'Su sesión expiró.'
				));
		}
	}

	public function insert_temp()
	{
		if ($this->auth->logged_in())
		{
			if ($this->auth->allowed_module($this->modulo))
			{
				$this->load->model('Ingreso_masivo_pop_model');
				$post = $this->input->post();
				$file = '/var/www/html/uploads/inventarioGeneral/ingreso_masivo/'.$post['file'];
				
				$objPHPExcel = PHPExcel_IOFactory::load($file);

				$resultados = array(
						'error'			=> FALSE,
						'advertencia'	=> FALSE,
						'mensajes'		=> array(),
						'advertencias'	=> array()
					);

				$sheetNames = $objPHPExcel->getSheetNames();

				$resultado = 0;

				foreach ($sheetNames as $hoja)
				{
					if ($hoja == 'POP')
					{
						$idIngresoMasivoPOP = $this->Ingreso_masivo_pop_model->registrar_ingreso_masivo(5);
						try{ $objPHPExcel->setActiveSheetIndexByName($hoja); } catch(Exception $e){ continue; }

						$activeSheet = $objPHPExcel->getActiveSheet();
						$cell_collection = $activeSheet->getCellCollection();
						$cabeceras = array('Código', 'Nombre', 'Latitud (°)', 'Longitud (°)', 'Tipo de Torre', 'Altura de Torre (m)', 'Factor Uso de Torre (%)', 'Tipo de Estación', 'Altura de Edificación (m)', 'Tipo Mimetizado', 'Tipo de Proyecto', 'Coubicado en', 'Operador Coubicado');
						$cabeceras_no_validas = array();
						$cabeceras_validas = array();
						$arr_data = array();

						foreach ($cell_collection as $cell)
						{
							$column = $activeSheet->getCell($cell)->getColumn();
							$row = $activeSheet->getCell($cell)->getRow();
							$data_value = $activeSheet->getCell($cell)->getValue();

							if ($row == 1)
							{
								$header[$row][$column] = $data_value;

								foreach ($header as $filaHeader => $infoHeader)
								{
									if ( ! in_array($infoHeader[$column], $cabeceras))
									{
										array_push($cabeceras_no_validas, $infoHeader[$column]);
									}
									else
									{
										array_push($cabeceras_validas, $infoHeader[$column]);
									}
								}
							}
							else
							{
								if ( ! isset($data_value) && $column == 'A')
								{
									/*break;*/
								}
								elseif (in_array($header[1][$column], $cabeceras_no_validas))
								{
									
								}
								else
								{
									$arr_data[$row][$column] = $data_value;
								}
							}
						}

						/*echo json_encode($cabeceras_no_validas);*/
						$filas = count($arr_data);

						if ($idIngresoMasivoPOP !== FALSE)
						{
							foreach ($arr_data as $fila => $infoFila)
							{
								$nombreSeparado = explode('_', $infoFila['B']);
								$codigoNombre = $nombreSeparado[0];
								$nombreSolo = substr($infoFila['B'], strpos($infoFila['B'], '_') + 1);

								foreach ($infoFila as $columna => $campo)
								{
									$ingresoMasivo['ingreso_masivo'] = $idIngresoMasivoPOP;

									if ($header[1][$columna] == 'Código')
									{
										$codigo = $campo;
										$ingresoMasivo['codigo'] = $campo;
									}
									elseif ($header[1][$columna] == 'Nombre')
									{
										$nombre = $this->Ingreso_masivo_pop_model->esNombreCodigoValidos($codigo, $campo);
										$ingresoMasivo['nombre'] = $nombreSolo;
										$ingresoMasivo['nombre_completo'] = $campo;
										$ingresoMasivo['id_actualizar'] = (($nombre !== FALSE)? $nombre['id'] : NULL);
									}
									elseif ($header[1][$columna] == 'Latitud (°)')
									{
										$ingresoMasivo['latitud'] = $campo;
									}
									elseif ($header[1][$columna] == 'Longitud (°)')
									{
										$ingresoMasivo['longitud'] = $campo;
									}
									elseif ($header[1][$columna] == 'Tipo de Torre')
									{
										$tipo_torre = $this->Ingreso_masivo_pop_model->esTipoTorreValido($campo);
										$ingresoMasivo['tipo_torre'] = $tipo_torre['id'];
									}
									elseif ($header[1][$columna] == 'Altura de Torre (m)')
									{
										$ingresoMasivo['altura_torre'] = $campo;
									}
									elseif ($header[1][$columna] == 'Factor Uso de Torre (%)')
									{
										$ingresoMasivo['factor_uso_torre'] = $campo;
									}
									elseif ($header[1][$columna] == 'Tipo de Estación')
									{
										if ($campo == 'Greenfield')
										{
											$ingresoMasivo['greenfield_rooftop'] = 0;
										}
										else
										{
											$ingresoMasivo['greenfield_rooftop'] = 1;
										}
									}
									elseif ($header[1][$columna] == 'Altura de Edificación (m)')
									{
										$ingresoMasivo['altura_edificacion'] = $campo;
									}
									elseif ($header[1][$columna] == 'Tipo Mimetizado')
									{
										$camuflaje = $this->Ingreso_masivo_pop_model->esTipoMimetizadoValido((($campo == 'N/A')? 'NO' : $campo));
										$ingresoMasivo['camuflaje'] = $camuflaje['id'];
									}
									elseif ($header[1][$columna] == 'Tipo de Proyecto')
									{
										$tipo_proyecto = $this->Ingreso_masivo_pop_model->esTipoProyectoValido($campo);
										$ingresoMasivo['tipo_proyecto'] = $tipo_proyecto['id'];
									}
									elseif ($header[1][$columna] == 'Coubicado en')
									{
										$coubicado = $this->Ingreso_masivo_pop_model->esCoubicadorValido($campo);
										$ingresoMasivo['coubicado'] = (($coubicado !== FALSE)? 1 : 0);
										$ingresoMasivo['coubicador'] = $coubicado['id'];
									}
									elseif ($header[1][$columna] == 'Operador Coubicado')
									{
										$operador_coubicado = $this->Ingreso_masivo_pop_model->esCoubicanteValido($campo);
										$ingresoMasivo['operador_coubicante'] = $operador_coubicado['id'];
									}
								}

								if ($this->Ingreso_masivo_pop_model->ingresar_temporal($ingresoMasivo))
								{
									$resultado++;
								}
							}

							/*echo json_encode($arr_data);*/
						}
						else
						{
							$resultados['error'] = TRUE;
							array_push($resultados['mensajes'], 'La hoja no contiene información');
						}
					}
					elseif ($hoja == 'Contratos')
					{
						$idIngresoMasivoContratos = $this->Ingreso_masivo_pop_model->registrar_ingreso_masivo(6);
						try{ $objPHPExcel->setActiveSheetIndexByName($hoja); } catch(Exception $e){ continue; }

						$activeSheet = $objPHPExcel->getActiveSheet();
						$cell_collection = $activeSheet->getCellCollection();
						$cabeceras = array('Código', 'Nombre', 'ID Contrato', 'Departamento', 'Provincia', 'Distrito', 'Dirección', 'Área de piso (m2)', 'Fecha inicio contrato', 'Fecha fin contrato', 'Arrendador', 'Contactos', 'Teléfonos', 'Correos');
						$cabeceras_no_validas = array();
						$cabeceras_validas = array();
						$arr_data = array();

						foreach ($cell_collection as $cell)
						{
							$column = $activeSheet->getCell($cell)->getColumn();
							$row = $activeSheet->getCell($cell)->getRow();
							$data_value = $activeSheet->getCell($cell)->getValue();

							if ($row == 1)
							{
								$header[$row][$column] = $data_value;

								foreach ($header as $filaHeader => $infoHeader)
								{
									if ( ! in_array($infoHeader[$column], $cabeceras))
									{
										array_push($cabeceras_no_validas, $infoHeader[$column]);
									}
									else
									{
										array_push($cabeceras_validas, $infoHeader[$column]);
									}
								}
							}
							else
							{
								if ( ! isset($data_value) && $column == "A")
								{
									/*break;*/
								}
								elseif (in_array($header[1][$column], $cabeceras_no_validas))
								{
									
								}
								else
								{
									$arr_data[$row][$column] = $data_value;
								}
							}
						}

						/*echo json_encode($cabeceras_no_validas);*/
						$filas_contratos = count($arr_data);

						if ($idIngresoMasivoContratos !== FALSE)
						{
							foreach ($arr_data as $fila => $infoFila)
							{
								$nombreSeparado = explode('_', $infoFila['B']);
								$codigoNombre = $nombreSeparado[0];

								foreach ($infoFila as $columna => $campo)
								{
									$ingresoMasivo['ingreso_masivo'] = $idIngresoMasivoContratos;

									if ($header[1][$columna] == 'Código')
									{
										$codigo = $campo;
									}
									elseif ($header[1][$columna] == 'Nombre')
									{
										$nombre = $this->Ingreso_masivo_pop_model->esNombreCodigoValidos($codigo, $campo);
										$ingresoMasivo['sitio'] = $nombre['id'];
									}
									elseif ($header[1][$columna] == 'ID Contrato')
									{
										$actualizar = $this->Ingreso_masivo_pop_model->existeContratoEnSitio($campo, $nombre['id']);
										$ingresoMasivo['id_contrato'] = $campo;
										$ingresoMasivo['id_actualizar'] = (($actualizar !== FALSE)? $actualizar['id'] : NULL);
									}
									elseif ($header[1][$columna] == 'Departamento')
									{
										$departamento = $campo;
									}
									elseif ($header[1][$columna] == 'Provincia')
									{
										$provincia = $campo;
									}
									elseif ($header[1][$columna] == 'Distrito')
									{
										$ingresoMasivo['zona_peru'] = $this->Ingreso_masivo_pop_model->zona_peru_valida($departamento, $provincia, $campo)->id;
									}
									elseif ($header[1][$columna] == 'Dirección')
									{
										$ingresoMasivo['direccion'] = $campo;
									}
									elseif ($header[1][$columna] == 'Área de piso (m2)')
									{
										$ingresoMasivo['area'] = (is_numeric($campo)? $campo : 0);
									}
									elseif ($header[1][$columna] == 'Fecha inicio contrato')
									{
										if (is_numeric($campo))
										{
											$ingresoMasivo['fecha_inicio'] = gmdate('Y-m-d', (($campo - 25569) * 86400));
										}
										elseif (preg_match("/[\/\\\]/", $campo))
										{
											$ingresoMasivo['fecha_inicio'] = date('Y-m-d', strtotime(str_replace('/', '-', $campo)));
										}
										else
										{
											$ingresoMasivo['fecha_inicio'] = NULL;
										}
									}
									elseif ($header[1][$columna] == 'Fecha fin contrato')
									{
										if (is_numeric($campo))
										{
											$ingresoMasivo['fecha_fin'] = gmdate('Y-m-d', (($campo - 25569) * 86400));
										}
										elseif (preg_match("/[\/\\\]/", $campo))
										{
											$ingresoMasivo['fecha_fin'] = date('Y-m-d', strtotime(str_replace('/', '-', $campo)));
										}
										else
										{
											$ingresoMasivo['fecha_fin'] = NULL;
										}
									}
									elseif($header[1][$columna] == 'Arrendador')
									{
										$ingresoMasivo['arrendador'] = $campo;
									}
									elseif($header[1][$columna] == 'Contactos')
									{
										$contactos = preg_split("/[\/\\\]+/", $campo);
									}
									elseif($header[1][$columna] == 'Teléfonos')
									{
										$telefonos = preg_split("/[\/\\\]+/", $campo);
									}
									elseif($header[1][$columna] == 'Correos')
									{
										$correos = preg_split("/[\/\\\]+/", $campo);
									}
								}

								$respuesta = $this->Ingreso_masivo_pop_model->ingresar_temporal_contratos($ingresoMasivo);

								$extra_telef = array_diff_key($telefonos, $contactos);
								$extra_correos = array_diff_key($correos, $contactos);
								$last_contacto = end($contactos);

								if($respuesta['resp'])
								{
									if(count($contactos) > 0)
									{
										foreach($contactos as $key => $contacto)
										{
											$contact['contrato'] = $respuesta['last_id'];
											$contact['contacto'] = trim($contacto);
											$telephone = array();
											$email = array();

											if($key >= count($telefonos))
											{
												array_push($telephone, array('telefono' => ''));
											}
											elseif($key <= count($telefonos))
											{
												array_push($telephone, array('telefono' => trim($telefonos[$key])));

												if(count($extra_telef) > 0 && $contacto == $last_contacto)
												{
													foreach($extra_telef as $telef)
													{
														array_push($telephone, array('telefono' => trim($telef)));
													}
												}
											}

											if($key >= count($correos))
											{
												array_push($email, array('correo' => ''));
											}
											elseif($key <= count($correos))
											{
												array_push($email, array('correo' => trim($correos[$key])));

												if(count($extra_correos) > 0 && $contacto == $last_contacto)
												{
													foreach($extra_correos as $corr)
													{
														array_push($email, array('correo' => trim($corr)));
													}
												}
											}

											$contact['telefono'] = json_encode($telephone);
											$contact['correo'] = json_encode($email);

											$this->Ingreso_masivo_pop_model->ingresar_temporal_contactos($contact);
										}
									}
								}
							}
						}
						else
						{
							$resultados['error'] = TRUE;
							array_push($resultados['mensajes'], "La hoja {$hoja} no contiene información, elimine esta hoja si no la utilizará.");
						}
					}
				}

				if( ! $resultados['error'])
				{
					isset($filas) ? array_push($resultados['mensajes'], 'Se enviaron '.$filas.' filas de POP para aprobar.') : '';
					isset($filas_contratos) ? array_push($resultados['mensajes'], 'Se enviaron '.$filas_contratos.' filas de Contratos para aprobar.') : '';
					unlink('/var/www/html/uploads/inventarioGeneral/ingreso_masivo/'.$post['file']);
				}

				echo json_encode($resultados);
			}
			else
			{
				echo json_encode(array(
						'error'		=> TRUE,
						'mensaje'	=> 'Usted no tiene permiso para realizar esta acción.'
					));
			}
		}
		else
		{
			echo json_encode(array(
					'error'		=> TRUE,
					'mensaje'	=> 'Su sesión expiró.'
				));
		}
	}

	public function listar_solicitudes($usuario)
	{
		if($this->auth->logged_in() && $usuario == $this->session->userdata('id_usuario'))
		{
			$this->load->model('Ingreso_masivo_pop_model');
			echo json_encode($this->Ingreso_masivo_pop_model->get_solicitudes($usuario));
		}
		else
		{
			show_404();
		}
	}

	private function validateDate($date)
	{
		$d = DateTime::createFromFormat('Y-m-d', $date);
		return $d && $d->format('Y-m-d') === $date;
	}
}