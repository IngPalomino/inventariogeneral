<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_antenas_iden extends CI_Controller {
	
	public function listar()
	{
		$this->load->model('Bodega_antenas_iden_model');
		$listado = $this->Bodega_antenas_iden_model->get_antenas_iden();
		echo json_encode($listado);
	}

	public function listar_almacen($id)
	{
		$this->load->model('Bodega_antenas_iden_model');
		$listado = $this->Bodega_antenas_iden_model->get_antenas_iden_almacen($id);
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Bodega_antenas_iden_model');
		$listado = $this->Bodega_antenas_iden_model->get_antenas_iden_sitio($id);
		echo json_encode($listado);
	}

	public function actualizar($id)
	{
		$this->load->model('Bodega_antenas_iden_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = $this->input->input_stream();
		$antena = $this->Bodega_antenas_iden_model->edit($id);

		if($this->Bodega_antenas_iden_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Antena iDEN",
					"tabla"			=> "tb_bodega_antenas_iden",
					"id"			=> $id,
					"datos viejos"	=> $antena,
					"datos nuevos"	=> $this->Bodega_antenas_iden_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_bodega_antenas_iden_model');
				$this->Logs_bodega_antenas_iden_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
			}
		}
	}

	public function reactivar($id)
	{
		$this->load->model('Bodega_antenas_iden_model');

		$info = $this->input->input_stream();
		$info_antena["eliminado"] = 1;
		
		if($this->Bodega_antenas_iden_model->update($id, $info_antena))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Reinstalación de Antena iDEN",
					"tabla"			=> "tb_bodega_antenas_iden",
					"id"			=> $id
				);

			$this->load->model('Logs_bodega_antenas_iden_model');
			$this->Logs_bodega_antenas_iden_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$this->load->model('Antenas_iden_model');
			unset($info['comentario']);

			if($this->Antenas_gul_model->insert($info))
			{
				$evento = array(
					"accion"		=> "Reinstalación de Antena iDEN",
					"tabla"			=> "tb_antenas_iden"
				);

				$this->load->model('Logs_antenas_iden_model');
				$this->Logs_antenas_iden_model->ingreso_de_datos($session['id_usuario'],$evento);
			}
		}
	}

	public function mover($id)
	{
		if ($this->auth->logged_in())
		{
			$info = $this->input->input_stream(array('tipo_almacen', 'almacen'));
			$old = $this->Bodega_antenas_iden_model->edit($id);
			$info["almacen_previo"] = json_encode(array("tipo_almacen" => $old[0]["tipo_almacen"], "almacen" => $old[0]["almacen"]));

			if ($this->Bodega_antenas_iden_model->update($id, $info))
			{
				$new = $this->Bodega_antenas_iden_model->edit($id);

				$evento["accion"] = "Cambio de almacén de Antena iDEN.";
				$evento["tabla"] = "tb_bodega_antenas_iden";
				$evento["id"] = $id;
				$evento["almacen_antiguo"] = $info["almacen_previo"];
				$evento["almacen_nuevo"] = json_encode(array("tipo_almacen" => $new[0]["tipo_almacen"], "almacen" => $new[0]["almacen"]));

				if ($evento["almacen_antiguo"] != $evento["almacen_nuevo"])
				{
					$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento, $this->input->input_stream('comentario'));
				}
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}

	public function eliminar($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->Bodega_antenas_iden_model->update($id, array("eliminado" => 1)))
			{
				$evento["accion"] = "Eliminación de Antena iDEN";
				$evento["tabla"] = "tb_bodega_antenas_iden";
				$evento["id"] = $id;

				$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento, $this->input->input_stream('comentario'));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}
}