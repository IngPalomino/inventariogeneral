<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coubicador extends CI_Controller {

	public function listar()
	{
		$this->load->model('Coubicador_model');

		$listado = $this->Coubicador_model->get_Coubicador();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Coubicador_model');

		$Coubicador = $this->Coubicador_model->edit($id);

		echo json_encode($Coubicador);
	}

	public function actualizar($id)
	{
		$this->load->model('Coubicador_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['coubicador'] = $this->input->input_stream('Coubicador');

		$this->Coubicador_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Coubicador_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['coubicador'] = $this->input->post('Coubicador');

		$this->Coubicador_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Coubicador_model');

		$this->Coubicador_model->delete($id);
	}
}