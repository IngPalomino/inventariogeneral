<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedor_mantenimiento extends CI_Controller {

	public function listar()
	{
		$this->load->model('Proveedor_mantenimiento_model');

		$listado = $this->Proveedor_mantenimiento_model->get_Proyecto();

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Proveedor_mantenimiento_model');

		$Proveedor_mantenimiento = $this->Proveedor_mantenimiento_model->edit($id);

		echo json_encode($Proveedor_mantenimiento);
	}

	public function actualizar($id)
	{
		$this->load->model('Proveedor_mantenimiento_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['proveedor_mantenimiento'] = $this->input->input_stream('Proveedor_mantenimiento');

		$this->Proveedor_mantenimiento_model->update($id, $info);
	}

	public function crear()
	{
		$this->load->model('Proveedor_mantenimiento_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info['proveedor_mantenimiento'] = $this->input->post('Proveedor_mantenimiento');

		$this->Proveedor_mantenimiento_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Proveedor_mantenimiento_model');

		$this->Proveedor_mantenimiento_model->delete($id);
	}
}