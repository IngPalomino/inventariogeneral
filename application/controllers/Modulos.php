<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modulos extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Modulos_model");
	}

	public function select($exclude = false)
	{
		$listado = $this->Modulos_model->select();

		$salida = array(
				"info" => $listado
			);

		echo json_encode($salida);
	}

	public function insert()
	{
		
	}

	public function update($id)
	{
		$this->load->library('session');

		$userSession = $this->session->userdata();

		$salida = array(
			'error' => FALSE,
			'respuesta' => FALSE);

		if( $userSession["logged_in"] )
		{
			$this->load->helper('form');
			$this->load->library('form_validation');

			$config = array(
					array(
						'field' => 'id',
						'label' => 'grupo',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir un %s.'
							)
					)
				);

			$this->form_validation->set_data(array("id",$id));
			$this->form_validation->set_rules($config);

			if($this->form_validation->run() === FALSE)
			{
				$salida['error'] = validation_errors();
				echo json_encode($salida);
			}
			else
			{
				$put = $this->input->input_stream();
				$salida["respuesta"] = $this->Modulos_model->update($id,$put);

				echo json_encode($salida);
			}
		}
		else
		{
			echo json_encode($salida);
		}
	}

	public function delete($id)
	{
		$this->load->library('session');

		$userSession = $this->session->userdata();

		$salida = array(
				'respuesta' => FALSE);

		if( $userSession["logged_in"] )
		{
			$this->load->helper('form');
			$this->load->library('form_validation');

			$config = array(
					array(
						'field' => 'id',
						'label' => 'grupo',
						'rules' => 'required',
						'errors' => array(
								'required' => 'Debe elegir un %s.'
							)
					)
				);

			$this->form_validation->set_data(array("id",$id));
			$this->form_validation->set_rules($config);

			if($this->form_validation->run() === FALSE)
			{
				$salida['error'] = validation_errors();
				echo json_encode($salida);
			}
			else
			{
				$salida["respuesta"] = $this->Modulos_model->delete($id);

				echo json_encode($salida);
			}
		}
		else
		{
			echo json_encode($salida);
		}
	}

	public function edit($id)
	{
		$this->load->library('session');

		$userSession = $this->session->userdata();

		$salida = array(
				'respuesta' => FALSE,
				'info' => array());

		if( $userSession["logged_in"] )
		{
			$respuesta = $this->Modulos_model->edit($id);

			if( count($respuesta) > 0 )
			{
				$salida["respuesta"] = true;
				$salida["info"] = $respuesta[0];
			}

			echo json_encode($salida);
		}
		else
		{
			echo json_encode($salida);
		}
	}
}