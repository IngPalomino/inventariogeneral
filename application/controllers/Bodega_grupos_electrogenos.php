<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_grupos_electrogenos extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('auth');
		$this->load->model('Bodega_grupos_electrogenos_model');
		$this->load->model('Logs_bodega_grupos_electrogenos_model', 'logs');
	}
	
	public function listar()
	{
		$this->load->model('Bodega_grupos_electrogenos_model');
		$listado = $this->Bodega_grupos_electrogenos_model->get_grupos_electrogenos();
		echo json_encode($listado);
	}
	
	public function listar_almacen($id)
	{
		$this->load->model('Bodega_grupos_electrogenos_model');
		$listado = $this->Bodega_grupos_electrogenos_model->get_grupos_electrogenos_almacen($id);
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Bodega_grupos_electrogenos_model');
		$listado = $this->Bodega_grupos_electrogenos_model->get_grupos_electrogenos_sitio($id);
		echo json_encode($listado);
	}

	public function almacenar()
	{
		$post = $this->input->post();

		if($this->auth->logged_in())
		{
			if($this->Bodega_grupos_electrogenos_model->insert($post))
			{
				$evento["accion"] = "Almacenamiento de Grupo Electrógeno";
				$evento["tabla"] = "tb_bodega_grupos_electrogenos";

				$this->logs->ingreso_de_datos($this->session->userdata('id_usuario'), $evento);
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}

	public function actualizar($id)
	{
		$post = $this->input->input_stream(array("tipo_grupo_electrogeno", "numero_serie_grupo_electrogeno", "tipo_motor_ge", "numero_serie_motor", "tipo_generador_ge", "numero_serie_generador", "tipo_avr_ge", "numero_serie_avr", "tipo_controlador_ge", "numero_serie_controlador_ge", "tipo_controlador_tta", "numero_serie_controlador_tta", "estado"));

		if ($this->auth->logged_in())
		{
			$old = $this->Bodega_grupos_electrogenos_model->edit($id);

			if ($this->Bodega_grupos_electrogenos_model->update($id, $post))
			{
				$evento["accion"] = "Actualización de Grupo Electrógeno.";
				$evento["tabla"] = "tb_bodega_grupos_electrogenos";
				$evento["id"] = $id;
				$evento["datos_antiguos"] = $old;
				$evento["datos_nuevos"] = $this->Bodega_grupos_electrogenos_model->edit($id);

				if ($evento["datos_antiguos"] != $evento["datos_nuevos"])
				{
					$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento);
				}
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}
	
	public function reactivar($id)
	{
		$this->load->model('Bodega_grupos_electrogenos_model');

		$info = $this->input->input_stream();
		$info_grupo["eliminado"] = 1;
		
		if($this->Bodega_grupos_electrogenos_model->update($id, $info_grupo))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Reinstalación de Grupo Electrógeno",
					"tabla"			=> "tb_bodega_grupos_electrogenos",
					"id"			=> $id
				);

			$this->load->model('Logs_bodega_grupos_electrogenos_model');
			$this->Logs_bodega_grupos_electrogenos_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$this->load->model('Grupos_electrogenos_model');
			unset($info['comentario']);

			if($this->Grupos_electrogenos_model->insert($info))
			{
				$evento = array(
					"accion"		=> "Reinstalación de Grupo Electrógeno",
					"tabla"			=> "tb_grupos_electrogenos"
				);

				$this->load->model('Logs_grupos_electrogenos_model');
				$this->Logs_grupos_electrogenos_model->ingreso_de_datos($session['id_usuario'],$evento);
			}
		}
	}

	public function mover($id)
	{
		$this->load->model('Bodega_grupos_electrogenos_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = $this->input->input_stream();
		$grupo = $this->Bodega_grupos_electrogenos_model->edit($id);

		$almacen_antiguo = array(
					"tipo_almacen" 	=> $grupo[0]["tipo_almacen"],
					"almacen"		=> $grupo[0]["almacen"]
				);

		$info["almacen_previo"] = json_encode($almacen_antiguo);
		$comentario = $info["comentario"];
		unset($info["comentario"]);

		if($this->Bodega_grupos_electrogenos_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			$grupo_nuevo = $this->Bodega_grupos_electrogenos_model->edit($id);

			$almacen_nuevo = array(
					"tipo_almacen" 	=> $grupo_nuevo[0]["tipo_almacen"],
					"almacen"		=> $grupo_nuevo[0]["almacen"]
				);

			$evento = array(
					"accion"		=> "Cambio de almacén de Grupo Electrógeno",
					"tabla"			=> "tb_bodega_grupos_electrogenos",
					"id"			=> $id,
					"almacen_antiguo"	=> $info["almacen_previo"],
					"almacen_nuevo"	=> json_encode($almacen_nuevo)
				);

			if($evento["almacen_antiguo"] != $evento["almacen_nuevo"])
			{
				$this->load->model('Logs_bodega_grupos_electrogenos_model');
				$this->Logs_bodega_grupos_electrogenos_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$comentario);
			}
		}
	}
	
	public function eliminar($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->Bodega_grupos_electrogenos_model->update($id, array("eliminado" => 1)))
			{
				$evento["accion"] = "Eliminación de Grupo Electrógeno";
				$evento["tabla"] = "tb_bodega_grupos_electrogenos";
				$evento["id"] = $id;

				$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento, $this->input->input_stream('comentario'));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}
}