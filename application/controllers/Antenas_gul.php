<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antenas_gul extends CI_Controller {

	private $modulo;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 3;
	}


	public function listar()
	{
		$this->load->model('Antenas_gul_model');

		$listado = $this->Antenas_gul_model->get_antenas_gul();

		echo json_encode($listado);
	}

	public function listar_sitio($id){
		$this->load->model('Antenas_gul_model');

		$listado = $this->Antenas_gul_model->get_antenas_gul_sitio($id);

		echo json_encode($listado);
	}

	public function editar($id)
	{
		$this->load->model('Antenas_gul_model');

		$antena_gul = $this->Antenas_gul_model->edit($id);

		echo json_encode($antena_gul);
	}

	public function actualizar($id)
	{

		$this->load->model('Antenas_gul_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = array();
		$info = $this->input->input_stream();		
		$info_antena['numero_serie'] = $info['numero_serie'];
       
        //extrayendo marca antena    
		$this->load->model('Tipo_antena_gul_model');
		$data= array('id' => $info['tipo_antena_gul']);       
		$datosTipo = $this->Tipo_antena_gul_model->get_type($data);          
		$marcaAntena =  $datosTipo[0]['marca'];
        
        if($marcaAntena =="TELNET"){
        $info_antena['sector'] = 0;
        }else{
          $info_antena['sector'] = $info['sector'];
        }		


		$info_antena['altura'] = floatval($info['altura']);
		//$info_antena['altura'] = $info['altura'];
		$info_antena['azimuth'] = $info['azimuth'];
		$info_antena['tilt_electrico'] = $info['tilt_electrico'];
		$info_antena['tilt_mecanico'] = $info['tilt_mecanico'];
		$info_antena['tipo_antena_gul'] = $info['tipo_antena_gul'];
		$info_antena["info_telnet"] = $info["info_telnet"];        
       
	    //sitio
		$datosSitio = $this->Antenas_gul_model->get_sitio_antena($id);          
		$idsitio = $datosSitio['id'];
		       $acuM = $ctnM = $acu = $ctn = 0;
               //antenas gul
		       $this->load->model('Antenas_gul_model');       
		       $datosTorre = $this->Antenas_gul_model->get_antenas_gul_sitio($idsitio);
		      //GulPendiente
		       $this->load->model('Antenas_gul_pendiente_model');
		       $datosTorreGULP = $this->Antenas_gul_pendiente_model->get_antenas_gul_pendiente_sitio($idsitio);
               //Microondas         
		       $this->load->model('Antenas_mw_model');       
		     $datosTorreM = $this->Antenas_mw_model->get_antenas_microondas_sitio($idsitio);
               //Microondas Pendiente
              $datosTorreMP = $listado = $this->Antenas_mw_model->get_antenas_microondas_sitio_pendiente($idsitio);
		if($marcaAntena =="TELNET"){
			//$info_antena['sector'] = 0;
            //si  es TELNET		    
			$ctnMP = $ctnGP = $ctnM = $ctn = $acu = $acuM  = 0;	
			$alturaEnviado = $info['altura'];  
			$coincidencias = " ";
			   foreach ($datosTorre as $key => $torrevalue) {
				if($id != $torrevalue['id']){			      	
					$altura_llenado = $torrevalue['altura'];
					$alInicial = $altura_llenado - 0.5;
					$alFinal = $altura_llenado + 0.5;				    
						if($alFinal<$alturaEnviado||$alInicial>$alturaEnviado){
				                   	//si cumplem la altura
						}else{
				                   //no cumplem la altura
							$ctn++;
						}				       
				    }//id diferente			        
				}		  

                $ctnGP = $this->recorre_telnet($datosTorreGULP,$alturaEnviado);
                $ctnM = $this->recorre_telnet($datosTorreM,$alturaEnviado);
                $ctnMP = $this->recorre_telnet($datosTorreMP,$alturaEnviado);
                   $acu = $ctn + $ctnGP + $ctnM + $ctnMP;
				   if($acu >0){
				        //si hay espacio reservados similares
				   	echo json_encode($acu);
				   }else{
				        //si no hay espacios reservados similares inserto
				   	//var_dump($info_antena);
				   	$this->update_gul_antena($id,$info_antena);
				   }
		}else{
          //si NO es TELNET 
			$acu = $ctn =  $ctnGP = $ctnM  =  $ctnMP = 0;
	      //altura y azimuth enviados por formulario 	     
					$azimuth_env = $info['azimuth'];
					$alturaEnviado = $info['altura'];	

					foreach ($datosTorre as $key => $torrevalue) { 
						if($id != $torrevalue['id']){
			             //azimuth registrado        	
							$azimuth_reg = $torrevalue['azimuth'];
							$azInicial = $azimuth_reg - 5;
							$azFinal = $azimuth_reg + 5;
		                 //altura registrada
							$altura_llenado = $torrevalue['altura'];
							$alInicial = $altura_llenado - 0.5;
							$alFinal = $altura_llenado + 0.5;

							if(($alInicial>$alturaEnviado) || ($alturaEnviado > $alFinal)){
			          	 // altura sin utilizar
							}else{
			          	     //altura utilizada
								$ctn = $this->azimuth_giro($azimuth_reg,$azimuth_env);
								$acu += $ctn;
							}
		          	     }//sino es mismo id	          	
		             }
          $ctnGP = $this->recorre_no_telnet($datosTorreGULP,$alturaEnviado,$azimuth_env);
          $ctnM = $this->recorre_no_telnet($datosTorreM,$alturaEnviado,$azimuth_env);
          $ctnMP = $this->recorre_no_telnet($datosTorreMP,$alturaEnviado,$azimuth_env);

		          $acu = $acu + $ctnGP + $ctnM + $ctnMP;
		          if($acu >0){
		          	//si hay espacio reservados similares
		          	echo json_encode($acu);
		          }else{
		          	//echo json_encode($acu);
		          	$this->update_gul_antena($id,$info_antena);
		          }
		      }
         
	}



	public function recorre_telnet($datosTorre,$alturaEnviado)
	{  
	    $ctn = 0;
	    foreach ($datosTorre as $key => $torrevalue) {
			$altura_llenado = $torrevalue['altura'];
			$alInicial = $altura_llenado - 0.5;
			$alFinal = $altura_llenado + 0.5;
				//todos los registros que son telnet y llenos infotelnet[];  
				if($alFinal<$alturaEnviado||$alInicial>$alturaEnviado){
					//si cumplem la altura
				}else{
				//no cumplem la altura			                   	
				$ctn++;
				}		        
		}
		return $ctn;
	}

	public function recorre_no_telnet($datosTorre,$alturaEnviado,$azimuth_env)
	{
	$acu = $ctn = 0; 
	              foreach ($datosTorre as $key => $torrevalue) {            
	                //gul registrado pendiente
	            	//azimuth registrado        	
		             $azimuth_reg = $torrevalue['azimuth'];
		             $azInicial = $azimuth_reg - 5;
		             $azFinal = $azimuth_reg + 5;
	                 //altura registrada
		             $altura_llenado = $torrevalue['altura'];
	                 $alInicial = $altura_llenado - 0.5;
	                 $alFinal = $altura_llenado + 0.5;

		          	 if(($alInicial>$alturaEnviado) || ($alturaEnviado > $alFinal)){
		          	 	 // altura sin utilizar                     
		      	     }else{
		          	    //altura utilizada
		      	     	$ctn = $this->azimuth_giro($azimuth_reg,$azimuth_env);	             
	                    $acu += $ctn;
		          	 }
		           }
	return $acu;
	} 


	public function crear_sitio($id)
	{
		$this->load->model('Antenas_gul_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info_antena['tipo_antena_gul'] = $info['tipo_antena_gul'];
		$info_antena['numero_serie'] = $info['numero_serie'];
		$info_antena['sector'] = $info['sector'];
		$info_antena['altura'] = $info['altura'];
		$info_antena['azimuth'] = $info['azimuth'];
		$info_antena['tilt_electrico'] = $info['tilt_electrico'];
		$info_antena['tilt_mecanico'] = $info['tilt_mecanico'];
		$info_antena["info_telnet"] = $info["info_telnet"];
		$info_antena['sitio'] = $id;
		
		$respuesta = $this->Antenas_gul_model->insert($info_antena);

		if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Creación de Antena GUL",
					"tabla"		=> "tb_antenas_gul"
				);



			$this->load->model('Logs_antenas_gul_model');
			$this->Logs_antenas_gul_model->ingreso_de_datos($session['id_usuario'],$evento);

			$this->load->model('Status_site_model');
			$evento["link"] = "ver/sitios/infraestructura/antenas/".$id;
			$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($id);
			$evento["elemento"] = $this->Antenas_gul_model->edit($respuesta["last_id"]);
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $id);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			// $this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function almacenar($id)
	{
		$this->load->library('session');
		$this->load->model('Antenas_gul_model');

		$info = $this->input->input_stream();
		$session = $this->session->userdata();

		$info_antena['eliminado'] = 1;

		if(isset($session["logged_in"]) && $session["auth"] <= 2)
		{
			if($this->Antenas_gul_model->update($id, $info_antena))
			{
				$evento = array(
						"accion"		=> "Almacenamiento de Antena GUL",
						"tabla"			=> "tb_antenas_gul",
						"id"			=> $id
					);

				$this->load->model('Logs_antenas_gul_model');
				$this->Logs_antenas_gul_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

				$this->load->model('Bodega_antenas_gul_model');
				unset($info['comentario']);

				if($this->Bodega_antenas_gul_model->insert($info))
				{
					$evento = array(
						"accion"		=> "Almacenamiento de Antena GUL",
						"tabla"			=> "tb_bodega_antenas_gul"
					);

					$this->load->model('Logs_bodega_antenas_gul_model');
					$this->Logs_bodega_antenas_gul_model->ingreso_de_datos($session['id_usuario'],$evento);
				}
			}
		}
		else
		{
			show_404();
		}
	}

	public function eliminar($id)
	{
		$this->load->model('Antenas_gul_model');

		$info = $this->input->input_stream();

		$info_antena['eliminado'] = 1;

		if($this->Antenas_gul_model->update($id, $info_antena))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Antena GUL",
					"tabla"			=> "tb_antenas_gul",
					"id"			=> $id
				);

			$this->load->model('Logs_antenas_gul_model');
			$this->Logs_antenas_gul_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$evento["sitio"] = $this->Antenas_gul_model->get_sitio_antena($id);
			$evento["link"] = "ver/sitios/infraestructura/antenas/".$evento["sitio"]["id"];
			$evento["elemento"] = $this->Antenas_gul_model->edit($id);
			$evento["comentario"] = $info["comentario"];
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

   public function update_gul_antena($id,$info_antena)
	{
            //var_dump($info_antena);
            //echo "testting";
    		            	$antena_gul = $this->Antenas_gul_model->edit($id);
							if($this->Antenas_gul_model->update($id, $info_antena))
							{
								$this->load->library('session');
								$session = $this->session->userdata();

								$evento = array(
										"accion"		=> "Actualización de Antena GUL",
										"tabla"			=> "tb_antenas_gul",
										"id"			=> $id,
										"datos viejos"	=> $antena_gul,
										"datos nuevos"	=> $this->Antenas_gul_model->edit($id)
									);

								if($evento["datos viejos"] != $evento["datos nuevos"])
								{
									$this->load->model('Logs_antenas_gul_model');
									$this->Logs_antenas_gul_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

									$evento["sitio"] = $this->Antenas_gul_model->get_sitio_antena($id);
									$evento["link"] = "ver/sitios/infraestructura/antenas/".$evento["sitio"]["id"];
									$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
									$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
									$cc = $this->custom->clean_cc($session["usuario"], $cc);
									$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
								}
							}
	}


  public function azimuth_giro($azimuth_reg,$azimuth_env)	
	{
    //registrado data 		
 	$azimuth_i = $azimuth_reg - 5;
 	$azimuth_f = $azimuth_reg + 5;
     
 	$cont = 0;
 	$mensaje ='';
    $mensajeh ='';
 	if($azimuth_i>0){
			 		// si es positivo
			       if(($azimuth_env<$azimuth_i)||($azimuth_env>$azimuth_f)){
			       	  $mensaje = "Puede instalar";
			       	
			       }else{
			       	$mensaje = "No puede instalar";
			       
			       	  $cont++;

			       }
 	}else{
 	  // si es negativo el azimuth registrado;         
       $azimuth_i = 0;    
	   if(($azimuth_env<$azimuth_i)||($azimuth_env>$azimuth_f)){	   		
		  $mensaje = "Puede Instalar";
	   }else{
		  $mensaje = "No puede instalar";
		  $cont++;
		} 
      
 	}
    return $cont;
 	}






}