<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_tanques_combustible extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('auth');
		$this->load->model('Bodega_tanques_combustible_model');
		$this->load->model('Logs_bodega_tanques_combustible_model', 'logs');
	}

	public function almacenar()
	{
		$post = $this->input->post();

		if($this->auth->logged_in())
		{
			if($this->Bodega_tanques_combustible_model->insert($post))
			{
				$evento["accion"] = "Almacenamiento de Tanque de Combustible";
				$evento["tabla"] = "tb_bodega_tanques_combustible";

				$this->logs->ingreso_de_datos($this->session->userdata('id_usuario'), $evento);
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}

	public function eliminar($id)
	{
		if ($this->auth->logged_in())
		{
			if ($this->Bodega_tanques_combustible_model->update($id, array("eliminado" => 1)))
			{
				$evento["accion"] = "Eliminación de Tanque de Combustible";
				$evento["tabla"] = "tb_bodega_tanques_combustible";
				$evento["id"] = $id;

				$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento, $this->input->input_stream('comentario'));
			}
		}
		else
		{
			echo json_encode(array("error" => TRUE, "message" => "Su sesión ha expirado."));
		}
	}
}