<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lineas_electricas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Notificaciones_model');
		$this->load->model('Lineas_electricas_model');
		$this->load->model('Logs_lineas_electricas_model', 'logs');
		$this->load->library(array('auth', 'custom'));
		$this->load->helper(array('url', 'custom'));
		$this->modulo = 10;
	}

	public function listar()
	{
		echo json_encode($this->Lineas_electricas_model->get_lineas_electricas());
	}

	public function listar_sitio($id)
	{
		echo json_encode($this->Lineas_electricas_model->get_lineas_electricas_sitio($id));
	}

	public function editar($id)
	{
		echo json_encode($this->Lineas_electricas_model->edit($id));
	}

	public function actualizar($id)
	{
		$info = $this->input->input_stream();
		
		$info["propietarios_linea_electrica"] = json_decode($info["propietarios_linea_electrica"]);
		$info["conexiones_linea_electrica"] = json_decode($info["conexiones_linea_electrica"]);
		
		$info_linea["fecha_servicio"] = $info["fecha_servicio"];
		$info_linea["sistema_linea_electrica"] = $info["sistema_linea_electrica"];
		$info_linea["seccion_linea"] = $info["seccion_linea"];
		$info_linea["distancia"] = $info["distancia"];
		$info_linea["postes"] = $info["postes"];
		$info_linea["tipo_transformador"] = $info["tipo_transformador"];
		$info_linea["num_serie_transformador"] = $info["num_serie_transformador"];
		$info_linea["potencia_transformador"] = $info["potencia_transformador"];
		$info_linea["conexion_transformador"] = $info["conexion_transformador"];
		$info_linea["tipo_transformix"] = $info["tipo_transformix"];
		$info_linea["num_serie_transformix"] = $info["num_serie_transformix"];
		$info_linea["potencia_transformix"] = $info["potencia_transformix"];
		$info_linea["conexion_transformix"] = $info["conexion_transformix"];

		$lineas_electricas = $this->Lineas_electricas_model->edit($id);

		if($this->Lineas_electricas_model->update($id, $info_linea))
		{
			$session = $this->session->userdata();
			
			if(count($info["propietarios_linea_electrica"]) > 0)
			{
				$this->load->model('Propietarios_linea_electrica_model');
				foreach (($info["propietarios_linea_electrica"]) as $propietario)
				{
					if($this->Propietarios_linea_electrica_model->update($propietario->id,$propietario))
					{
						$evento = array(
							"accion"	=> "Actualización de Propietario",
							"tabla"		=> "tb_propietarios_linea_electrica",
							"id"		=> $propietario->id
						);

						$this->load->model('Logs_propietarios_linea_electrica_model');
						$this->Logs_propietarios_linea_electrica_model->actualizacion_de_datos($session['id_usuario'],$propietario->id,$evento);
					}
				}
			}
			
			if(count($info["conexiones_linea_electrica"]) > 0)
			{
				$this->load->model('Lineas_electricas_conexion_linea_electrica_model');
				$this->Lineas_electricas_conexion_linea_electrica_model->cleanUp($id);
				foreach (($info["conexiones_linea_electrica"]) as $conexion)
				{
					$datos_conexion = array(
										"lineas_electricas" => $id,
										"conexion_linea_electrica" => $conexion
									);
					if($this->Lineas_electricas_conexion_linea_electrica_model->insert($datos_conexion))
					{
						$evento = array(
							"accion"	=> "Actualización de Conexión de Línea Eléctrica",
							"tabla"		=> "tb_lineas_electricas_conexion_linea_electrica",
							"id"		=> $id
						);
						
						
					}
				}
			}

			$evento = array(
					"accion"		=> "Actualización de Linea Eléctrica",
					"tabla"			=> "tb_lineas_electricas",
					"id"			=> $id,
					"datos viejos"	=> $lineas_electricas,
					"datos nuevos"	=> $this->Lineas_electricas_model->edit($id)
				);

			if($evento["datos viejos"] != $evento["datos nuevos"])
			{
				$this->load->model('Logs_lineas_electricas_model');
				$this->Logs_lineas_electricas_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);

				$evento["sitio"] = $this->Lineas_electricas_model->get_sitio_linea($id);
				$evento["link"] = "ver/sitios/energia/lineas_electricas/".$evento["sitio"]["id"];
				$evento["cambios"] = $this->custom->array_changes($evento["datos viejos"][0], $evento["datos nuevos"][0]);
				$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
				$cc = $this->custom->clean_cc($session["usuario"], $cc);
				$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/modificacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
			}
		}
	}

	public function crear_sitio($id)
	{
		$info = $this->input->post();
		
		$info["sitio"] = $id;
		
		$info["conexiones_linea_electrica"] = json_decode($info["conexiones_linea_electrica"]);
		
		$info_linea["sitio"] = $id;
		$info_linea["fecha_servicio"] = $info["fecha_servicio"];
		$info_linea["sistema_linea_electrica"] = $info["sistema_linea_electrica"];
		$info_linea["seccion_linea"] = $info["seccion_linea"];
		$info_linea["distancia"] = $info["distancia"];
		$info_linea["postes"] = $info["postes"];
		$info_linea["tipo_transformador"] = $info["tipo_transformador"];
		$info_linea["num_serie_transformador"] = $info["num_serie_transformador"];
		$info_linea["potencia_transformador"] = $info["potencia_transformador"];
		$info_linea["conexion_transformador"] = $info["conexion_transformador"];
		$info_linea["tipo_transformix"] = $info["tipo_transformix"];
		$info_linea["num_serie_transformix"] = $info["num_serie_transformix"];
		$info_linea["potencia_transformix"] = $info["potencia_transformix"];
		$info_linea["conexion_transformix"] = $info["conexion_transformix"];
		
		$respuesta = $this->Lineas_electricas_model->insert($info_linea);

		if($respuesta["resp"])
		{
			$this->load->library('session');
			$session = $this->session->userdata();
			
			if(count($info["conexiones_linea_electrica"]) > 0)
			{
				$this->load->model('Lineas_electricas_conexion_linea_electrica_model');
				foreach (($info["conexiones_linea_electrica"]) as $conexion)
				{
					$datos_conexion = array(
										"lineas_electricas" => $respuesta["last_id"],
										"conexion_linea_electrica" => $conexion
									);
					if($this->Lineas_electricas_conexion_linea_electrica_model->insert($datos_conexion))
					{
						$evento = array(
							"accion"	=> "Actualización de Conexión de Línea Eléctrica",
							"tabla"		=> "tb_lineas_electricas_conexion_linea_electrica",
							"id"		=> $id
						);
						
						
					}
				}
			}

			$evento = array(
					"accion"	=> "Creación de Linea Eléctrica",
					"tabla"		=> "tb_lineas_electricas"
				);

			$this->load->model('Logs_lineas_electricas_model');
			$this->Logs_lineas_electricas_model->ingreso_de_datos($session['id_usuario'],$evento);

			$this->load->model('Status_site_model');
			$evento["link"] = "ver/sitios/energia/lineas_electricas/".$id;
			$evento["sitio"] = $this->Status_site_model->get_sitio_nombre($id);
			$evento["elemento"] = $this->Lineas_electricas_model->edit($respuesta["last_id"]);
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $id);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/creacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function eliminar($id)
	{
		$info = $this->input->input_stream();

		$info_linea['eliminado'] = 1;

		if($this->Lineas_electricas_model->update($id, $info_linea))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Linea Eléctrica",
					"tabla"			=> "tb_lineas_electricas",
					"id"			=> $id
				);

			$this->load->model('Logs_lineas_electricas_model');
			$this->Logs_lineas_electricas_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);

			$evento["sitio"] = $this->Lineas_electricas_model->get_sitio_linea($id);
			$evento["link"] = "ver/sitios/energia/lineas_electricas/".$evento["sitio"]["id"];
			$evento["elemento"] = $this->Lineas_electricas_model->edit($id);
			$evento["comentario"] = $info["comentario"];
			$cc = $this->Notificaciones_model->get_correos_grupos($this->modulo, $evento["sitio"]["id"]);
			$cc = $this->custom->clean_cc($session["usuario"], $cc);
			$this->custom->send_email($session["usuario"], $evento["accion"], $this->load->view("correos/eliminacion_equipo", array("evento" => $evento, "session" => $session), true), $cc);
		}
	}

	public function almacenar($id)
	{
		if ($this->auth->allowed_module($this->modulo))
		{
			$info = $this->input->input_stream(array('elemento', 'tipo_elemento', 'numero_serie', 'potencia', 'conexion', 'tipo_almacen', 'almacen', 'estado', 'proveniente_sitio'));

			switch ((int) $info['elemento'])
			{
				case 1:
					$info_ts['tipo_transformador'] = NULL;
					$info_ts['num_serie_transformador'] = NULL;
					$info_ts['potencia_transformador'] = NULL;
					$info_ts['conexion_transformador'] = NULL;
					break;
				case 2:
					$info_ts['tipo_transformix'] = NULL;
					$info_ts['num_serie_transformix'] = NULL;
					$info_ts['potencia_transformix'] = NULL;
					$info_ts['conexion_transformix'] = NULL;
					break;
			}

			if ($this->Lineas_electricas_model->update($id, $info_ts))
			{
				$evento = array(
						'accion'	=> 'Almacenamiento de Elemento Línea Eléctrica.',
						'tabla'		=> 'tb_lineas_electricas',
						'id'		=> $id
					);

				$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento, $this->input->input_stream('comentario'));

				$this->load->model('Bodega_lineas_electricas_model');

				if ($this->Bodega_lineas_electricas_model->insert($info))
				{
					$evento = array(
							'accion'	=> 'Almacenamiento de Elemento Línea Eléctrica.',
							'tabla'		=> 'tb_bodega_lineas_electricas'
						);

					$this->load->model('Logs_bodega_lineas_electricas_model');
					$this->Logs_bodega_lineas_electricas_model->ingreso_de_datos($evento);
				}
			}
		}
		else
		{
			echo json_encode(array('error' => TRUE, 'message' => 'Usted no tiene permiso para realizar esta acción.'));
		}
	}
}