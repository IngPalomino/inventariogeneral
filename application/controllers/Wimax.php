<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wimax extends CI_Controller {
	
	public function listar()
	{
		$this->load->model('Wimax_model');
		$listado = $this->Wimax_model->get_wimax();
		echo json_encode($listado);
	}

	public function listar_sitio($id)
	{
		$this->load->model('Wimax_model');
		$listado = $this->Wimax_model->get_wimax_sitio($id);
		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('Wimax_model');
		$wimax = $this->Wimax_model->edit($id);
		echo json_encode($wimax);
	}

	public function crear_sitio($id)
	{
		$this->load->model('Wimax_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		
		$info["data"] = json_decode($info["data"],true);

		if(count($info["data"]) > 0)
		{
			foreach($info["data"] as $wimax)
			{
				$wimax["sitio"] = $id;
				$this->Wimax_model->insert($wimax);
			}
		}
	}

	public function eliminar()
	{
		$this->load->model('Wimax_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$info = array();
		$info = $this->input->input_stream();
		
		$info["data"] = json_decode($info["data"],true);
		
		foreach($info["data"] as $wimax)
		{
			$this->Wimax_model->delete($wimax);
		}
	}
}