<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mso_categoria_tipo extends CI_Controller
{
	public function __construct()
	{  
		parent::__construct();
		$this->load->library('auth');		
		$this->load->model('Mso_categoria_tipo_model');
		$this->load->library('custom');
		$this->load->helper(array('url', 'custom'));
		$this->load->library('session');	
		
	}

	public function get_all()
	{

		if ($this->auth->logged_in())
		{

        $lista = $this->Mso_categoria_tipo_model->get_all();  
		echo json_encode($lista);
		}
		else
		{
		  echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
		
	}


    public function get_id($id)
	{

		if ($this->auth->logged_in())
		{
        $lista = $this->Mso_categoria_tipo_model->get_id($id);  
		echo json_encode($lista);
		}
		else
		{
		 echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
		
	}

    public function get_id_categoria($id)
	{

		if ($this->auth->logged_in())
		{
        $lista = $this->Mso_categoria_tipo_model->get_id_categoria($id);  
		echo json_encode($lista);
		}
		else
		{
		 echo json_encode(array("error" => TRUE, "mensaje" => "Su sesión expiró."));
		}
		
	}
	 




}