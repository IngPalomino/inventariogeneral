<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antenas_in_building extends CI_Controller {
	
	public function listar()
	{
		$this->load->model('Antenas_in_building_model');

		$listado = $this->Antenas_in_building_model->get_antenas_in_building();

		echo json_encode($listado);
	}

	public function listar_sitio($id) {
		$this->load->model('Antenas_in_building_model');

		$listado = $this->Antenas_in_building_model->get_antenas_in_building_sitio($id);

		echo json_encode($listado);
	}
	
	public function editar($id)
	{
		$this->load->model('Antenas_in_building_model');

		$antena = $this->Antenas_in_building_model->edit($id);

		echo json_encode($antena);
	}

	public function actualizar($id)
	{
		$this->load->model('Antenas_in_building_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->input_stream();

		$antena = $this->Antenas_in_building_model->edit($id);

		if($this->Antenas_in_building_model->update($id, $info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Actualización de Antena In Building",
					"tabla"			=> "tb_antenas_in_building",
					"id"			=> $id,
					"datos viejos"	=> $antena,
					"datos nuevos"	=> $this->Antenas_in_building_model->edit($id)
				);

			$this->load->model('Logs_antenas_in_building_model');
			$this->Logs_antenas_in_building_model->actualizacion_de_datos($session['id_usuario'],$id,$evento);
		}
	}

	public function crear_sitio($id)
	{
		$this->load->model('Antenas_in_building_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();
		$info["sitio"] = $id;

		if($this->Antenas_in_building_model->insert($info))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"	=> "Creación de Antena In Building",
					"tabla"		=> "tb_antenas_in_building"
				);

			$this->load->model('Logs_antenas_in_building_model');
			$this->Logs_antenas_in_building_model->ingreso_de_datos($session['id_usuario'],$evento);
		}
	}

	public function crear_almacen_alquilado($id)
	{
		$this->load->model('Antenas_in_building_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Antenas_in_building_model->insert($info);
	}

	public function crear_almacen_propio($id)
	{
		$this->load->model('Antenas_in_building_model');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$info = array();
		$info = $this->input->post();

		$this->Antenas_in_building_model->insert($info);
	}

	public function eliminar($id)
	{
		$this->load->model('Antenas_in_building_model');

		$info = $this->input->input_stream();

		$info_antena['eliminado'] = 1;

		if($this->Antenas_in_building_model->update($id, $info_antena))
		{
			$this->load->library('session');
			$session = $this->session->userdata();

			$evento = array(
					"accion"		=> "Eliminación de Antena In Building",
					"tabla"			=> "tb_antenas_in_building",
					"id"			=> $id
				);

			$this->load->model('Logs_antenas_in_building_model');
			$this->Logs_antenas_in_building_model->actualizacion_de_datos($session['id_usuario'],$id,$evento,$info['comentario']);
		}
	}
}