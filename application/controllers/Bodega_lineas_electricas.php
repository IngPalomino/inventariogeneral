<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega_lineas_electricas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('auth');
		$this->load->model('Bodega_lineas_electricas_model');
		$this->load->model('Logs_bodega_lineas_electricas_model', 'logs');
	}

	public function almacenar()
	{
		if ($this->auth->logged_in())
		{
			if($this->Bodega_aires_acondicionados_model->insert($this->input->post()))
			{
				$evento['accion'] = 'Almacenamiento de Aire Acondicionado';
				$evento['tabla'] = 'tb_bodega_lineas_electricas';

				$this->logs->ingreso_de_datos($this->session->userdata('id_usuario'), $evento);
			}
		}
		else
		{
			echo json_encode(array('error' => TRUE, 'message' => 'Su sesión ha expirado.'));
		}
	}

	public function actualizar($id)
	{
		$post = $this->input->input_stream();

		if ($this->auth->logged_in())
		{
			$old = $this->Bodega_lineas_electricas_model->edit($id);

			if ($this->Bodega_lineas_electricas_model->update($id, $post))
			{
				$evento['accion'] = 'Actualización de Línea Eléctrica.';
				$evento['tabla'] = 'tb_bodega_lineas_electricas';
				$evento['id'] = $id;
				$evento['datos_antiguos'] = $old;
				$evento['datos_nuevos'] = $this->Bodega_lineas_electricas_model->edit($id);

				if ($evento['datos_antiguos'] != $evento['datos_nuevos'])
				{
					$this->logs->actualizacion_de_datos($this->session->userdata('id_usuario'), $id, $evento);
				}
			}
		}
		else
		{
			echo json_encode(array('error' => TRUE, 'message' => 'Su sesión ha expirado.'));
		}
	}
}