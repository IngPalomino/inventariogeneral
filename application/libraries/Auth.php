<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth {

	public function __construct()
	{
		$this->load->library(array('session'));
		$this->load->model('Auth_model');
	}

	public function __get($var)
	{
		return get_instance()->$var;
	}

	public function logged_in()
	{
		return (bool) $this->session->userdata('usuario');
	}

	public function is_admin($id = FALSE)
	{
		$admin_groups = array(1, 2);
		return $this->in_group($admin_groups, $id);
	}

	public function is_super_admin($id = FALSE)
	{
		return $this->in_group(array(1), $id);
	}

	public function in_group($check_group, $id = FALSE, $check_all = FALSE)
	{
		$id || $id = $this->session->userdata('id_usuario');

		if ( ! is_array($check_group))
		{
			$check_group = array($check_group);
		}

		$user_groups = $this->Auth_model->get_user_groups($id);
		$groups_array = array();

		foreach ($user_groups as $group)
		{
			$groups_array[$group['id']] = $group['nombre'];
		}

		foreach ($check_group as $key => $value)
		{
			$groups = is_string($value) ? $groups_array : array_keys($groups_array);

			if (in_array($value, $groups) xor $check_all)
			{
				return !$check_all;
			}
		}

		return $check_all;
	}

	public function allowed_module($modulo, $sitio = NULL, $id = FALSE)
	{
		$module_groups = $this->Auth_model->get_module_groups($modulo);
		$modules_array = array();

		foreach ($module_groups as $group)
		{
			$modules_array[$group['id']] = $group['nombre'];
		}

		if ($sitio)
		{
			$crm = $this->Auth_model->get_crm_group($sitio);
			$delete = (($crm['grupo'] == 8) ? 11 : 8);

			if (($key = array_search($delete, array_keys($modules_array))) !== FALSE)
			{
				array_splice($modules_array, $key, 1);
			}
		}

		return $this->in_group($modules_array, $id);
	}
}