<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom
{
	public function send_email($recipient, $subject, $message, $cc = null)
	{
		$CI =& get_instance();
		$CI->load->library('email');
		$CI->email->from('inventario.redes@entel.net.pe', 'Inventario de Redes Entel Perú');
		$CI->email->to($recipient);
		$cc? $CI->email->cc($cc) : "";
		$CI->email->subject($subject);
		$CI->email->message($message);
		return $CI->email->send();
	}

	public function array_changes($old, $new)
	{
		$CI =& get_instance();
		$CI->load->helper('custom');
		$old_array = array();
		$new_array = array();

		foreach ($new as $key => $value)
		{
			if(($key != "id") && ($key != "sitio") && ($key != "eliminado") && ($key != "created_at"))
			{
				if($new[$key] != $old[$key])
				{
					array_push($old_array, array('<li style="color:red;">'.$key => (is_array($old[$key])? array_to_ul($old[$key]) : $old[$key]).'</li>'));
					array_push($new_array, array('<li style="color:red;">'.$key => (is_array($new[$key])? array_to_ul($new[$key]) : $new[$key]).'</li>'));
				}
				else
				/*if($new[$key] != $old[$key])*/
				{
					array_push($old_array, array('<li>'.$key => (is_array($old[$key])? array_to_ul($old[$key]) : $old[$key]).'</li>'));
					/*array_push($new_array, array('<li>'.$key => (is_array($new[$key])? json_encode($new[$key]) : $new[$key]).'</li>'));*/
				}
			}
		}

		return array("datos_viejos" => $old_array, "datos_nuevos" => $new_array);
	}


    public function clean_cc($to, $cc)
    {

        if(($key = array_search($to, $cc)) !== false)

            {
                array_splice($cc, $key, 1);
            }

            foreach ($cc as $key => $value)
                {

                if($value === null){ unset($cc[$key]); }
                }

            return $cc;
    }







	public function default_view_data()
	{
		$data['css'] = array(
								'bootstrap.min',
								'font-awesome.min',
								'estilo_nav',
								'custom'
							);
		$data['js'] = array(
								'jquery-1.12.2.min',
								'angular.min',
								'bootstrap.min'
							);

		$data['session'] = $this->session->userdata();

		return $data;
	}

	public function validate_date($date, $format = "Y-m-d H:i:s")
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
}