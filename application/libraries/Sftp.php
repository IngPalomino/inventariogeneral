<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'third_party/phpseclib/Net/SFTP.php');
set_include_path(APPPATH.'third_party/phpseclib');

class Sftp {
	var $hostname	 = '';
	var $username	 = '';
	var $password	 = '';
	var $debug 		 = FALSE;
	var $sftp        = FALSE;

	public function get_error()
	{
		$error = isset($this->error)? $this->error : "";
		return $error;
	}
	
	public function Sftp($config = array())
	{
		if (count($config) > 0)
		{
			$this->initialize($config);
		}
		
		log_message('debug', "SFTP Class Initialized");
	}
	
	public function initialize($config = array())
	{
		foreach ($config as $key => $val)
		{
			if (isset($this->$key))
			{
				$this->$key = $val;
			}
		}

		$this->hostname = preg_replace('|.+?://|', '', $this->hostname);
	}
	
	public function connect($config = array())
	{
		if (count($config) > 0)
		{
			$this->initialize($config);
		}

		$this->sftp = new Net_SFTP($this->hostname);
		if (!$this->sftp->login($this->username, $this->password))
		{
			$this->error = "Login Failed";
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

    public function list_files($path = '.')
	{
		$ret = array();
		$files =  $this->sftp->nlist($path);
		if( count($files) > 0)
		{
			foreach ($files as $obj)
			{
				if($obj != "." && $obj != "..")
				{
					array_push($ret, $path."/".$obj);
				}
			}
		}
		return $ret;
	}
	
	public function download($rempath, $locpath)
	{
		return  $this->sftp->get($rempath, $locpath);
	}
	
	public function delete_file($filepath)
	{
		return  $this->sftp->delete($filepath);
	}
	
	public function upload($rempath, $locpath)
	{
		return $this->sftp->put($rempath, $locpath, NET_SFTP_LOCAL_FILE);
	}
	
	public function rename($old_file, $new_file)
	{
		return $this->sftp->rename($old_file, $new_file);
	}
	
	public function close($reason = 'End')
	{
		return $this->sftp->_disconnect($reason);
	}
}