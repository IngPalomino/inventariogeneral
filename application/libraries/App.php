<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App {

	private $data = array();
	protected $errors;

	public function __construct()
	{
		$this->data['css'] = array(
				'bootstrap.min',
				'font-awesome.min',
				'loading-bar.min',
				'app'
			);

		$this->data['js'] = array(
				'jquery-1.12.4.min',
				'angular.min',
				'bootstrap.min',
				'loading-bar.min'
			);

		$this->data['modules'] = array(
				'common/app',
				'common/service',
				'common/directive'
			);

		$this->data['session'] = $this->session->userdata();

		$this->errors = array();
	}

	public function __get($var)
	{
		return get_instance()->{$var};
	}

	public function set($name, $value)
	{
		$this->data[$name] = $value;
		return $this;
	}

	public function add_data($key, $view_data, $position = FALSE)
	{
		$view_data = ! is_array($view_data) ? (array) $view_data : $view_data;

		if ( ! isset($this->data[$key]))
		{
			$this->set($key, array());
		}

		foreach ($view_data as $vdata)
		{
			if ($position === FALSE)
			{
				array_push($this->data[$key], $vdata);
			}
			else
			{
				array_splice($this->data[$key], $position, 0, $vdata);
			}
		}

		return $this;
	}

	public function load($template = 'default', $view = '', $return = FALSE)
	{
		$this->set('content', $view);
		$view_html = $this->load->view('templates/'.$template, $this->data, $return);
		$this->data = array();

		if ($return) return $view_html;
	}

	public function load_pop($title, $view, $ng_controller = 'common/controller')
	{
		$site_view = explode('/', $view)[0];

		return $this->set('title', $title)
					->set('site_view', $site_view)
					->add_data('modules', $ng_controller, 0)
					->load('default', $view);
	}

	public function load_main($title, $view, $ng_controller = 'common/controller')
	{
		return $this->set('title', $title)
					->add_data('modules', $ng_controller, 0)
					->load('default', $view);
	}

	public function send_email($recipient, $subject, $message, $cc = NULL)
	{
		$this->load->library('email');

		if (isset($cc) && ! empty($cc))
		{
			$this->email->cc($cc);
		}

		return $this->email->from('inventario.redes@entel.pe', 'Inventario de Redes Entel Perú')
							->to($recipient)
							->subject($subject)
							->message($message)
							->send();
	}

	public function _filter_data($table, $data)
	{
		$filtered_data = array();
		$columns = $this->db->list_fields($table);

		if (is_array($data))
		{
			foreach ($columns as $column)
			{
				if (array_key_exists($column, $data))
				{
					$filtered_data[$column] = $data[$column];
				}
			}
		}

		return $filtered_data;
	}

	public function set_error($error)
	{
		$this->errors[] = $error;

		return $error; 
	}

	public function errors()
	{
		$return = '';

		foreach ($this->errors as $error)
		{
			$return .= $error.' ';
		}

		$this->errors = array();

		return $return;
	}
}