<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

/********************************************/
/* Vistas                                   */
/********************************************/

$route['default_controller'] = 'vistas';
$route['administrar'] = 'vistas/admin_inicio';
$route['administrar/usuarios'] = 'vistas/admin_usuarios';
$route['administrar/usuarios2'] = 'vistas/admin_usuarios2';
$route['administrar/grupos'] = 'vistas/admin_grupos';
$route['administrar/objetos'] = 'vistas/admin_objetos';
$route['administrar/relaciones/cols'] = 'vistas/admin_relaciones_cols';
$route['administrar/dependencias'] = 'vistas/admin_dependencias';
$route['administrar/ingreso_masivo'] = 'vistas/admin_ingreso_masivo';

$route['administrar/aprobar_antena'] = 'vistas/aprobar_instalacion_antena';

$route['administrar/ingreso_masivo/elemento/(:any)'] = 'vistas/admin_ingreso_masivo_elemento/$1';

$route['administrar/aprobar_antena/elemento/(:any)'] = 'vistas/admin_aprobar_antena_elemento/$1';

$route['administrar/aprobar_antena/mw/elemento/(:any)'] = 'vistas/admin_aprobar_antena_elemento_mw/$1';

$route['validaciones'] = 'vistas/validaciones_inicio';
$route['validaciones/ingreso_masivo/(:any)/(:any)'] = 'vistas/validaciones_ingreso_masivo/$1/$2';
$route['ingreso_masivo'] = 'vistas/ingreso_masivo_inicio';
$route['ingreso_masivo/antenas_gul'] = 'vistas/ingreso_masivo_antenas_gul';
$route['ingreso_masivo/suministros'] = 'vistas/ingreso_masivo_suministros';
$route['ingreso_masivo/idus'] = 'vistas/ingreso_masivo_idus';
$route['ingreso_masivo/enlaces'] = 'vistas/ingreso_masivo_enlaces';
$route['ingreso_masivo/pop'] = 'vistas/ingreso_masivo_pop';
$route['ingreso_masivo/contratos'] = 'vistas/ingreso_masivo_contratos';
$route['ingreso_masivo/rectificadores'] = 'vistas/ingreso_masivo_rectificadores';
$route['ingreso_masivo/bancos_baterias'] = 'vistas/ingreso_masivo_bancos_baterias';
$route['ver'] = 'vistas/visualizacion_inicio';
$route['ver/bodegas'] = 'vistas/visualizacion_bodegas';
$route['ver/bodegas/objetos'] = 'vistas/visualizacion_bodegas_objetos';
$route['ver/bodegas/sitios'] = 'vistas/visualizacion_bodegas_sitios';
$route['ver/bodegas/almacenes'] = 'vistas/visualizacion_bodegas_almacenes';
$route['ver/sitios'] = 'vistas/visualizacion_busqueda';
$route['ver/sitios/gis/(:num)'] = 'vistas/visualizacion_gis_sitio/$1';
$route['ver/sitios/resumen/(:any)'] = 'vistas/visualizacion_resumen_sitio/$1';
$route['ver/sitios/torre/(:any)'] = 'vistas/visualizacion_torre_sitio/$1';
$route['ver/sitios/infraestructura/antenas/(:any)'] = 'vistas/visualizacion_antenas_sitio/$1';
$route['ver/sitios/infraestructura/idus/(:any)'] = 'vistas/visualizacion_idus_sitio/$1';
$route['ver/sitios/infraestructura/enlaces/(:any)'] = 'vistas/visualizacion_enlaces_sitio/$1';
$route['ver/sitios/energia/tanques_combustible/(:any)'] = 'vistas/visualizacion_tanques_combustible/$1';
$route['ver/sitios/energia/suministros/(:any)'] = 'vistas/visualizacion_suministros/$1';
$route['ver/sitios/energia/lineas_electricas/(:any)'] = 'vistas/visualizacion_lineas_electricas/$1';
$route['ver/sitios/energia/rectificadores/(:any)'] = 'vistas/visualizacion_rectificadores/$1';
$route['ver/sitios/energia/aires_acondicionados/(:any)'] = 'vistas/visualizacion_aires_acondicionados/$1';
$route['ver/sitios/energia/bancos_baterias/(:any)'] = 'vistas/visualizacion_bancos_baterias/$1';
$route['ver/sitios/energia/grupos_electrogenos/(:any)'] = 'vistas/visualizacion_grupos_electrogenos/$1';

// Mso(salas tecnicas)
// ver
$route['sala_tecnica/listar']['get'] = 'sala_tecnica/listar';
$route['salastecnicas'] = 'vistas/salastecnicas/';
$route['salastecnicas/mso/(:any)'] = 'vistas/salaMso/$1';
$route['resumen/salastecnicas/mso/(:any)'] = 'vistas/resumen_salaMso/$1';

$route['ver/salastecnicas/mso/energia/grupos_electrogenos/(:any)'] = 'vistas/visualizacion_salatecnica_mso_grupo_electrogenos/$1';
$route['ver/salastecnicas/mso/energia/bancos_baterias/(:any)'] = 'vistas/visualizacion_salatecnica_mso_bancos_baterias/$1';

$route['ver/salastecnicas/mso/energia/rectificadores/(:any)'] = 'vistas/visualizacion_salatecnica_mso_rectificadores/$1';

$route['ver/salastecnicas/mso/energia/inversores/(:any)'] = 'vistas/visualizacion_salatecnica_mso_inversores/$1';




$route['ver/salastecnicas/mso/energia/subestacion/(:any)'] = 'vistas/visualizacion_salatecnica_mso_subestacion/$1';

$route['ver/salastecnicas/mso/energia/ups/(:any)'] = 'vistas/visualizacion_salatecnica_mso_ups/$1';

$route['ver/salastecnicas/mso/clima/aire_acondicionado/(:any)'] = 'vistas/visualizacion_salatecnica_mso_aires_acondicionados/$1';
// get listar
$route['fases/listar']['get'] = 'fases/listar';

// ingreso
// grupo_electrogeno 
$route['ingreso/mso/energia/grupos_electrogenos/(:any)'] = 'vistas/ingreso_mso_grupos_electrogenos/$1';
$route['ingreso/salastecnicas/mso/energia/grupos_electrogenos/(:any)'] =
'vistas/ingreso_salastecnicas_mso_energia_grupos_electrogenos/$1';
//banco_bateria
$route['ingreso/salastecnicas/mso/energia/bancos_baterias/(:any)'] = 'vistas/ingreso_salastecnicas_mso_energia_bancos_baterias/$1';

$route['ingreso/salastecnicas/mso/energia/bancos_baterias_new/(:any)'] = 'vistas/ingreso_bancos_baterias_new/$1';

$route['ingreso/salastecnicas/mso/energia/rectificadores/(:any)'] = 'vistas/ingreso_salastecnicas_mso_energia_rectificadores/$1';

$route['ingreso/salastecnicas/mso/energia/inversores/(:any)'] = 'vistas/ingreso_salastecnicas_mso_energia_inversores/$1';


$route['ingreso/salastecnicas/mso/energia/subestacion/(:any)'] = 'vistas/ingreso_salastecnicas_mso_energia_subestacion/$1';

$route['ingreso/salastecnicas/mso/energia/ups/(:any)'] = 'vistas/ingreso_salastecnicas_mso_energia_ups/$1';

$route['ingreso/salastecnicas/mso/clima/aire_acondicionado/(:any)'] = 'vistas/ingreso_salastecnicas_mso_clima_aires_acondicionados/$1';

// Nuevo grupo electrogeno mso
$route['mso_grupos_electrogenos/listar']['get'] = 'mso_grupos_electrogenos/listar';
$route['mso_grupos_electrogenos/grupo_electrogeno/(:any)']['get'] = 'mso_grupos_electrogenos/editar/$1';

$route['mso_grupos_electrogenos/grupo_electrogeno/(:any)']['put'] = 'mso_grupos_electrogenos/actualizar/$1';

$route['mso_grupos_electrogenos/grupo_electrogeno/sitio/(:any)']['post'] = 'mso_grupos_electrogenos/crear_sitio/$1';
$route['mso_grupos_electrogenos/grupo_electrogeno/(:any)']['delete'] = 'mso_grupos_electrogenos/eliminar/$1';
$route['mso_grupos_electrogenos/listar_sitio/(:any)']['get'] = 'mso_grupos_electrogenos/listar_sitio/$1';
$route['mso_grupos_electrogenos/grupo_electrogeno/almacenar/(:any)']['post'] = 'mso_grupos_electrogenos/almacenar/$1';
$route['mso_grupos_electrogenos/grupo_electrogeno/reactivar/(:any)']['put'] = 'mso_almacenes/reactivar/$1';

//aire acondicionado mso


//aire acondicionado mso



// ingreso 

// Mso(salas tecnicas )
$route['ver/sitios/documentacion/(:any)'] = 'vistas/visualizacion_documentacion/$1';
$route['ver/sitios/electronica/nodo_b/(:any)'] = 'vistas/visualizacion_nodo_b/$1';
$route['ver/sitios/electronica/microondas/(:any)'] = 'vistas/visualizacion_microondas/$1';
$route['ver/sitios/electronica/wimax/(:any)'] = 'vistas/visualizacion_wimax/$1';
$route['ver/sitios/electronica/iden/(:any)'] = 'vistas/visualizacion_iden/$1';
$route['ver/sitios/capacity/(:num)'] = 'vistas/visualizacion_capacity/$1';
$route['ingreso'] = 'vistas/ingreso_inicio';

$route['ingreso/sitios'] = 'vistas/ingreso_sitios';

$route['ingreso/busqueda'] = 'vistas/ingreso_busqueda';
$route['ingreso/objetos_almacenados'] = 'vistas/ingreso_almacenados';
$route['ingreso/bodegas'] = 'vistas/ingreso_bodegas';
$route['ingreso/bodegas/busqueda'] = 'vistas/ingreso_busqueda_bodegas';
$route['ingreso/bodegas/(:any)/(:num)'] = 'vistas/ingreso_bodegas_elemento/$1/$2';
$route['ingreso/bodegas/objetos'] = 'vistas/ingreso_bodegas_objetos';
$route['ingreso/bodegas/sitios'] = 'vistas/ingreso_bodegas_sitios';
$route['ingreso/bodegas/almacenes'] = 'vistas/ingreso_bodegas_almacenes';
$route['ingreso/sitios/gis/(:any)'] = 'vistas/ingreso_gis_sitio/$1';
$route['ingreso/sitios/resumen/(:any)'] = 'vistas/ingreso_resumen_sitio/$1';
$route['ingreso/sitios/torre/(:any)'] = 'vistas/ingreso_torre_sitio/$1';
$route['ingreso/sitios/infraestructura/antenas/(:any)'] = 'vistas/ingreso_antenas_sitio/$1';
$route['ingreso/sitios/infraestructura/idus/(:any)'] = 'vistas/ingreso_idus_sitio/$1';
$route['ingreso/sitios/infraestructura/enlaces/(:any)'] = 'vistas/ingreso_enlaces_sitio/$1';
$route['ingreso/sitios/energia/tanques_combustible/(:any)'] = 'vistas/ingreso_tanques_combustible/$1';
$route['ingreso/sitios/energia/suministros/(:any)'] = 'vistas/ingreso_suministros/$1';
$route['ingreso/sitios/energia/lineas_electricas/(:any)'] = 'vistas/ingreso_lineas_electricas/$1';
$route['ingreso/sitios/energia/rectificadores/(:any)'] = 'vistas/ingreso_rectificadores/$1';
$route['ingreso/sitios/energia/aires_acondicionados/(:any)'] = 'vistas/ingreso_aires_acondicionados/$1';
$route['ingreso/sitios/energia/bancos_baterias/(:any)'] = 'vistas/ingreso_bancos_baterias/$1';
$route['ingreso/sitios/energia/bancos_baterias_new/(:any)'] = 'vistas/ingreso_bancos_baterias_new/$1';
$route['ingreso/sitios/energia/grupos_electrogenos/(:any)'] = 'vistas/ingreso_grupos_electrogenos/$1';
$route['ingreso/sitios/documentacion/(:any)'] = 'vistas/ingreso_documentacion/$1';
$route['ingreso/sitios/documentacion_sftp/(:any)'] = 'vistas/ingreso_documentacion_sftp/$1';/*
SFTP*/
$route['ingreso/sitios/electronica/nodo_b/(:any)'] = 'vistas/ingreso_nodo_b/$1';
$route['ingreso/sitios/electronica/microondas/(:any)'] = 'vistas/ingreso_microondas/$1';
$route['ingreso/sitios/electronica/wimax/(:any)'] = 'vistas/ingreso_wimax/$1';
$route['ingreso/sitios/electronica/iden/(:num)'] = 'vistas/ingreso_iden/$1';
$route['ingreso/sitios/capacity/(:num)'] = 'vistas/ingreso_capacity/$1';
$route['perfil'] = 'vistas/perfil_inicio';
$route['perfil/contrasena'] = 'vistas/perfil_contrasena';
$route['reportes'] = 'vistas/reportes_inicio';
$route['reportes/predeterminados'] = 'vistas/reportes_predeterminados';
$route['reportes/personalizados'] = 'vistas/reportes_personalizados';
$route['evaluaciones'] = 'vistas/evaluaciones';
$route['evaluaciones/evaluar_desempeno'] = 'vistas/evaluaciones_evaluar_desempeno';
$route['evaluaciones/evaluar_desempeno/(:any)'] = 'vistas/evaluacion_evaluar_desempeno/$1';
$route['evaluaciones/evaluar_desempeno/(:any)/(:any)'] = 'vistas/evaluado_evaluar_desempeno/$1/$2';
$route['evaluaciones/visualizar_desempeno'] = 'vistas/evaluaciones_visualizar_desempeno';
$route['evaluaciones/administrar'] = 'vistas/evaluaciones_administar';
$route['evaluaciones/administrar/nueva_evaluacion'] = 'vistas/evaluaciones_administar_nueva_evaluacion';
$route['evaluaciones/administrar/evaluaciones'] = 'vistas/evaluaciones_administar_evaluaciones';
$route['georeferenciacion'] = 'vistas/georeferenciacion';

$route['ingreso/sitios_prueba'] = 'vistas/ingreso_sitios_prueba';
$route['validaciones/ingreso_masivo_prueba/(:any)/(:any)'] = 'vistas/validaciones_ingreso_masivo_prueba/$1/$2';
$route['ingreso/sitios/infraestructura/antenas_prueba/(:any)'] = 'vistas/ingreso_antenas_sitio_prueba/$1';
$route['evaluaciones/evaluar_desempeno_prueba/(:any)'] = 'vistas/evaluacion_evaluar_desempeno_prueba/$1';
$route['reestablecer_contrasena/(:any)'] = 'vistas/reestablecer_contrasena/$1';

$route['solicitante'] = 'vistas/solicitante/';
$route['solicitante/gul'] = 'vistas/solicitante_gul';
$route['solicitante/mw'] = 'vistas/solicitante_mw';





/********************************************/
/* Rutas a Modelos                          */
/********************************************/

$route['reset_contrasena']['post'] = 'usuarios/reset_password';
$route['recuperar_contrasena']['post'] = 'usuarios/forgot_password';

$route['sesion/iniciar']['post'] = 'sesion/iniciar';
$route['sesion/cerrar'] = 'sesion/cerrar';

$route['basedatos/listar']['get'] = 'basedatos/listar';
$route['basedatos/listar/columnas/(:any)']['get'] = 'basedatos/listar_columnas/$1';

$route['relaciones/listar']['get'] = 'relaciones/listar';
$route['relaciones/relacion']['post'] = 'relaciones/crear';
$route['relaciones/relacion/(:any)']['get'] = 'relaciones/listar_id/$1';
$route['relaciones/relacion/(:any)']['delete'] = 'relaciones/eliminar/$1';
$route['relaciones/relacion/(:any)']['put'] = 'relaciones/actualizar/$1';

$route['dependencias/listar']['get'] = 'dependencias/listar';
$route['dependencias/dependencia']['post'] = 'dependencias/crear';

$route['usuarios/usuario/(:any)']['get'] = 'usuarios/editar/$1';
$route['usuarios/usuario/(:any)']['put'] = 'usuarios/actualizar/$1';
$route['usuarios/usuario']['post'] = 'usuarios/crear';
$route['usuarios/usuario/(:any)']['delete'] = 'usuarios/eliminar/$1';
$route['usuarios/reestablecer/(:any)']['put'] = 'usuarios/reestablecerContrasena/$1';
$route['usuarios/listar']['get'] = 'usuarios/listar';
$route['usuarios/cambio_contrasena']['post'] = 'usuarios/cambio_contrasena';
$route['administrador/usuario/contrasena/(:num)']['put'] = 'usuarios/cambio_contrasena_administrador/$1';
$route['administrador/usuario/reactivar/(:num)']['put'] = 'usuarios/reanudar_usuario/$1';
$route['administrador/usuario/desactivar/(:num)']['put'] = 'usuarios/apagar_usuario/$1';

//grupo 
$route['administrador/grupos/grupo']['post'] = 'grupos/crear';
$route['administrador/grupos/grupo/(:any)']['delete'] = 'grupos/delete/$1';
$route['administrador/grupos/grupo/(:num)']['put'] = 'grupos/actualizar/$1';

$route['sitios/sitio/(:any)']['get'] = 'sitios/editar/$1';
$route['sitios/sitio/(:any)']['put'] = 'sitios/actualizar/$1';
$route['sitios/sitio']['post'] = 'sitios/crear';
$route['sitios/sitio/(:any)']['delete'] = 'sitios/eliminar/$1';
$route['sitios/listar']['get'] = 'sitios/listar';
$route['sitios/buscar']['post'] = 'sitios/buscar';

$route['status_site/sitio/(:any)']['get'] = 'status_site/editar/$1';
$route['status_site/sitio/(:any)']['put'] = 'status_site/actualizar/$1';
$route['status_site/sitio']['post'] = 'status_site/crear_2';
$route['status_site/sitio/(:any)']['delete'] = 'status_site/eliminar/$1';
$route['status_site/listar/(:num)/(:num)']['get'] = 'status_site/listar/$1/$2';
$route['status_site/buscar']['post'] = 'status_site/buscar';

$route['contratos/contrato/(:any)']['get'] = 'contratos/editar/$1';
$route['contratos/contrato/(:any)']['put'] = 'contratos/actualizar/$1';
$route['contratos/contrato']['post'] = 'contratos/crear';
$route['contratos/contrato/(:any)']['delete'] = 'contratos/eliminar/$1';
$route['contratos/listar']['get'] = 'contratos/listar';
$route['contratos/buscar']['post'] = 'contratos/buscar';

$route['contactos_contrato/contacto/(:any)']['get'] = 'contactos_contrato/editar/$1';
$route['contactos_contrato/contacto/(:any)']['put'] = 'contactos_contrato/actualizar/$1';
$route['contactos_contrato/contacto']['post'] = 'contactos_contrato/crear';
$route['contactos_contrato/contacto/(:any)']['delete'] = 'contactos_contrato/eliminar/$1';
$route['contactos_contrato/listar']['get'] = 'contactos_contrato/listar';
$route['contactos_contrato/buscar']['post'] = 'contactos_contrato/buscar';

$route['zonas/zona/(:any)']['get'] = 'zona_peru/editar/$1';
$route['zonas/zona/(:any)']['put'] = 'zona_peru/actualizar/$1';
$route['zonas/zona']['post'] = 'zona_peru/crear';
$route['zonas/zona/(:any)']['delete'] = 'zona_peru/eliminar/$1';
$route['zonas/listar']['get'] = 'zona_peru/listar';
$route['zonas/listar/departamentos']['get'] = 'zona_peru/listarDepartamentos';
$route['zonas/listar/provincias/(:any)']['get'] = 'zona_peru/listarProvincias/$1';
$route['zonas/listar/distritos/(:any)/(:any)']['get'] = 'zona_peru/listarDistritos/$1/$2';

$route['ubicacion_equipos/listar']['get'] = 'ubicacion_equipos/listar';
$route['ubicacion_equipos/ubicacion/(:any)']['get'] = 'ubicacion_equipos/editar/$1';
$route['ubicacion_equipos/ubicacion/(:any)']['put'] = 'ubicacion_equipos/actualizar/$1';
$route['ubicacion_equipos/ubicacion']['post'] = 'ubicacion_equipos/crear';
$route['ubicacion_equipos/ubicacion/(:any)']['delete'] = 'ubicacion_equipos/eliminar/$1';

$route['tipo_torre/listar']['get'] = 'tipo_torre/listar';
$route['tipo_torre/tipo/(:any)']['get'] = 'tipo_torre/editar/$1';
$route['tipo_torre/tipo/(:any)']['put'] = 'tipo_torre/actualizar/$1';
$route['tipo_torre/tipo']['post'] = 'tipo_torre/crear';
$route['tipo_torre/tipo/(:any)']['delete'] = 'tipo_torre/eliminar/$1';

$route['camuflaje/listar']['get'] = 'camuflaje/listar';
$route['camuflaje/tipo/(:any)']['get'] = 'camuflaje/editar/$1';
$route['camuflaje/tipo/(:any)']['put'] = 'camuflaje/actualizar/$1';
$route['camuflaje/tipo']['post'] = 'camuflaje/crear';
$route['camuflaje/tipo/(:any)']['delete'] = 'camuflaje/eliminar/$1';

$route['idu/listar']['get'] = 'idu/listar';
$route['idu/idu/(:any)']['get'] = 'idu/editar/$1';
$route['idu/idu/(:any)']['put'] = 'idu/actualizar/$1';
$route['idu/idu/sitio/(:any)']['post'] = 'idu/crear_sitio/$1';
$route['idu/idu/almacen_alquilado/(:any)']['post'] = 'idu/crear_almacen_alquilado/$1';
$route['idu/idu/almacen_propio/(:any)']['post'] = 'idu/crear_almacen_propio/$1';
$route['idu/idu/(:any)']['delete'] = 'idu/eliminar/$1';
$route['idu/listar_sitio/(:any)']['get'] = 'idu/listar_sitio/$1';

$route['ubicacion_idu/listar']['get'] = 'ubicacion_idu/listar';
$route['ubicacion_idu/idu/(:any)']['get'] = 'ubicacion_idu/editar/$1';
$route['ubicacion_idu/idu/(:any)']['put'] = 'ubicacion_idu/actualizar/$1';
$route['ubicacion_idu/idu']['post'] = 'ubicacion_idu/crear';
$route['ubicacion_idu/idu/(:any)']['delete'] = 'ubicacion_idu/eliminar/$1';
$route['ubicacion_idu/sitio/(:any)']['get'] = 'ubicacion_idu/listar_sitio/$1';

$route['tipo_idu/listar']['get'] = 'tipo_idu/listar';
$route['tipo_idu/tipo/(:any)']['get'] = 'tipo_idu/editar/$1';
$route['tipo_idu/tipo/(:any)']['put'] = 'tipo_idu/actualizar/$1';
$route['tipo_idu/tipo']['post'] = 'tipo_idu/crear';
$route['tipo_idu/tipo/(:any)']['delete'] = 'tipo_idu/eliminar/$1';
$route['tipo_idu/tipo/id']['post'] = 'tipo_idu/id_tipo';
$route['tipo_idu/marcas']['get'] = 'tipo_idu/marcas';
$route['tipo_idu/modelos/(:any)']['get'] = 'tipo_idu/modelos/$1';
$route['tipo_idu/versiones/(:any)/(:any)']['get'] = 'tipo_idu/versiones/$1/$2';

$route['tipo_antena_microondas/listar']['get'] = 'tipo_antena_microondas/listar';
$route['tipo_antena_microondas/tipo/(:any)']['get'] = 'tipo_antena_microondas/editar/$1';
$route['tipo_antena_microondas/tipo/(:any)']['put'] = 'tipo_antena_microondas/actualizar/$1';
$route['tipo_antena_microondas/tipo']['post'] = 'tipo_antena_microondas/crear';
$route['tipo_antena_microondas/tipo/(:any)']['delete'] = 'tipo_antena_microondas/eliminar/$1';
$route['tipo_antena_microondas/tipo/id']['post'] = 'tipo_antena_microondas/id_tipo';
$route['tipo_antena_microondas/marcas']['get'] = 'tipo_antena_microondas/marcas';
$route['tipo_antena_microondas/modelos/(:any)']['get'] = 'tipo_antena_microondas/modelos/$1';
$route['tipo_antena_microondas/versiones/(:any)/(:any)']['get'] = 'tipo_antena_microondas/versiones/$1/$2';

$route['antenas_gul/listar']['get'] = 'antenas_gul/listar';
$route['antenas_gul/antena/(:any)']['get'] = 'antenas_gul/editar/$1';
$route['antenas_gul/antena/(:any)']['put'] = 'antenas_gul/actualizar/$1';
$route['antenas_gul/antena/sitio/(:any)']['post'] = 'antenas_gul/crear_sitio/$1';
$route['antenas_gul/antena_gul/almacenar/(:num)']['post'] = 'antenas_gul/almacenar/$1';
$route['antenas_gul/antena/(:any)']['delete'] = 'antenas_gul/eliminar/$1';
$route['antenas_gul/listar_sitio/(:any)']['get'] = 'antenas_gul/listar_sitio/$1';

//solicitud Reserva  de espacio
$route['antenas_gul_pendiente/listar']['get'] = 'antenas_gul_pendiente/listar';
$route['antenas_gul_pendiente/antena/(:any)']['get'] = 'antenas_gul_pendiente/editar/$1';
$route['antenas_gul_pendiente/antena/(:any)']['put'] = 'antenas_gul_pendiente/actualizar/$1';
$route['antenas_gul_pendiente/antena/sitio/(:any)']['post'] = 'antenas_gul_pendiente/crear_sitio/$1';
$route['antenas_gul_pendiente/antena_gul/almacenar/(:num)']['post'] = 'antenas_gul_pendiente/almacenar/$1';
$route['antenas_gul_pendiente/antena/(:any)']['delete'] = 'antenas_gul_pendiente/eliminar/$1';
$route['antenas_gul_pendiente/listar_sitio/(:any)']['get'] = 'antenas_gul_pendiente/listar_sitio/$1';
//solicitud Reserva  de espacio

$route['tipo_antena_gul/listar']['get'] = 'tipo_antena_gul/listar';
$route['tipo_antena_gul/tipo/(:any)']['get'] = 'tipo_antena_gul/editar/$1';
$route['tipo_antena_gul/tipo/(:any)']['put'] = 'tipo_antena_gul/actualizar/$1';
$route['tipo_antena_gul/tipo']['post'] = 'tipo_antena_gul/crear';
$route['tipo_antena_gul/tipo/(:any)']['delete'] = 'tipo_antena_gul/eliminar/$1';
$route['tipo_antena_gul/tipo/id']['post'] = 'tipo_antena_gul/id_tipo';
$route['tipo_antena_gul/marcas']['get'] = 'tipo_antena_gul/marcas';
$route['tipo_antena_gul/modelos/(:any)']['get'] = 'tipo_antena_gul/modelos/$1';

$route['antenas_iden/listar']['get'] = 'antenas_iden/listar';
$route['antenas_iden/antena/(:any)']['get'] = 'antenas_iden/editar/$1';
$route['antenas_iden/antena/(:any)']['put'] = 'antenas_iden/actualizar/$1';
$route['antenas_iden/antena/sitio/(:any)']['post'] = 'antenas_iden/crear_sitio/$1';
$route['antenas_iden/antena_iden/almacenar/(:num)']['post'] = 'antenas_iden/almacenar/$1';
$route['antenas_iden/antena/(:any)']['delete'] = 'antenas_iden/eliminar/$1';
$route['antenas_iden/listar_sitio/(:any)']['get'] = 'antenas_iden/listar_sitio/$1';

$route['tipo_antena_iden/listar']['get'] = 'tipo_antena_iden/listar';
$route['tipo_antena_iden/tipo/(:any)']['get'] = 'tipo_antena_iden/editar/$1';
$route['tipo_antena_iden/tipo/(:any)']['put'] = 'tipo_antena_iden/actualizar/$1';
$route['tipo_antena_iden/tipo']['post'] = 'tipo_antena_iden/crear';
$route['tipo_antena_iden/tipo/(:any)']['delete'] = 'tipo_antena_iden/eliminar/$1';
$route['tipo_antena_iden/tipo/id']['post'] = 'tipo_antena_iden/id_tipo';
$route['tipo_antena_iden/marcas']['get'] = 'tipo_antena_iden/marcas';
$route['tipo_antena_iden/modelos/(:any)']['get'] = 'tipo_antena_iden/modelos/$1';

$route['antenas_in_building/listar']['get'] = 'antenas_in_building/listar';
$route['antenas_in_building/antena/(:any)']['get'] = 'antenas_in_building/editar/$1';
$route['antenas_in_building/antena/(:any)']['put'] = 'antenas_in_building/actualizar/$1';
$route['antenas_in_building/antena/sitio/(:any)']['post'] = 'antenas_in_building/crear_sitio/$1';
$route['antenas_in_building/antena/almacen_alquilado/(:any)']['post'] = 'antenas_in_building/crear_almacen_alquilado/$1';
$route['antenas_in_building/antena/almacen_propio/(:any)']['post'] = 'antenas_in_building/crear_almacen_propio/$1';

$route['antenas_in_building/antena/(:any)']['delete'] = 'antenas_in_building/eliminar/$1';
$route['antenas_in_building/listar_sitio/(:any)']['get'] = 'antenas_in_building/listar_sitio/$1';

$route['tanques_combustible/listar']['get'] = 'tanques_combustible/listar';
$route['tanques_combustible/tanque/(:any)']['get'] = 'tanques_combustible/editar/$1';
$route['tanques_combustible/tanque/(:any)']['put'] = 'tanques_combustible/actualizar/$1';
$route['tanques_combustible/tanque/sitio/(:any)']['post'] = 'tanques_combustible/crear_sitio/$1';
$route['tanques_combustible/tanque_combustible/almacenar/(:any)']['post'] = 'tanques_combustible/almacenar/$1';
$route['tanques_combustible/tanque/(:any)']['delete'] = 'tanques_combustible/eliminar/$1';
$route['tanques_combustible/listar_sitio/(:any)']['get'] = 'tanques_combustible/listar_sitio/$1';

$route['suministros/listar']['get'] = 'suministros/listar';
$route['suministros/suministro/(:any)']['get'] = 'suministros/editar/$1';
$route['suministros/suministro/(:any)']['put'] = 'suministros/actualizar/$1';
$route['suministros/suministro/sitio/(:any)']['post'] = 'suministros/crear_sitio/$1';
$route['suministros/suministro/almacen_alquilado/(:any)']['post'] = 'suministros/crear_almacen_alquilado/$1';
$route['suministros/suministro/almacen_propio/(:any)']['post'] = 'suministros/crear_almacen_propio/$1';
$route['suministros/suministro/(:any)']['delete'] = 'suministros/eliminar/$1';
$route['suministros/listar_sitio/(:any)']['get'] = 'suministros/listar_sitio/$1';

$route['lineas_electricas/listar']['get'] = 'lineas_electricas/listar';
$route['lineas_electricas/linea/(:any)']['get'] = 'lineas_electricas/editar/$1';
$route['lineas_electricas/linea_electrica/(:any)']['put'] = 'lineas_electricas/actualizar/$1';
$route['lineas_electricas/linea_electrica/sitio/(:any)']['post'] = 'lineas_electricas/crear_sitio/$1';
$route['lineas_electricas/linea_electrica/almacen_alquilado/(:any)']['post'] = 'lineas_electricas/crear_almacen_alquilado/$1';
$route['lineas_electricas/linea_electrica/almacen_propio/(:any)']['post'] = 'lineas_electricas/crear_almacen_propio/$1';
$route['lineas_electricas/linea_electrica/(:any)']['delete'] = 'lineas_electricas/eliminar/$1';
$route['lineas_electricas/listar_sitio/(:any)']['get'] = 'lineas_electricas/listar_sitio/$1';

$route['propietarios_linea_electrica/propietario/(:any)']['post'] = 'propietarios_linea_electrica/crear/$1';
$route['propietarios_linea_electrica/propietario/(:any)']['delete'] = 'propietarios_linea_electrica/eliminar/$1';

$route['rectificadores/listar']['get'] = 'rectificadores/listar';
$route['rectificadores/rectificador/(:any)']['get'] = 'rectificadores/editar/$1';
$route['rectificadores/rectificador/(:any)']['put'] = 'rectificadores/actualizar/$1';
$route['rectificadores/rectificador/sitio/(:any)']['post'] = 'rectificadores/crear_sitio/$1';
$route['rectificadores/rectificador/almacen_alquilado/(:any)']['post'] = 'rectificadores/crear_almacen_alquilado/$1';
$route['rectificadores/rectificador/almacen_propio/(:any)']['post'] = 'rectificadores/crear_almacen_propio/$1';
$route['rectificadores/rectificador/(:any)']['delete'] = 'rectificadores/eliminar/$1';
$route['rectificadores/listar_sitio/(:any)']['get'] = 'rectificadores/listar_sitio/$1';
$route['rectificadores/eliminar_rectificador/(:any)']['delete'] = 'rectificadores/eliminar_definitivo/$1';

$route['modulos_rectif/rectificador/(:any)']['delete'] = 'modulos_rectif/eliminar/$1';

$route['aires_acondicionados/listar']['get'] = 'aires_acondicionados/listar';
$route['aires_acondicionados/aire_acondicionado/(:any)']['get'] = 'aires_acondicionados/editar/$1';
$route['aires_acondicionados/aire_acondicionado/(:any)']['put'] = 'aires_acondicionados/actualizar/$1';
$route['aires_acondicionados/aire_acondicionado/sitio/(:any)']['post'] = 'aires_acondicionados/crear_sitio/$1';
$route['aires_acondicionados/aire_acondicionado/almacen_alquilado/(:any)']['post'] = 'aires_acondicionados/crear_almacen_alquilado/$1';
$route['aires_acondicionados/aire_acondicionado/almacen_propio/(:any)']['post'] = 'aires_acondicionados/crear_almacen_propio/$1';
$route['aires_acondicionados/aire_acondicionado/almacenar/(:any)']['post'] = 'aires_acondicionados/almacenar/$1';
$route['aires_acondicionados/aire_acondicionado/(:any)']['delete'] = 'aires_acondicionados/eliminar/$1';
$route['aires_acondicionados/listar_sitio/(:any)']['get'] = 'aires_acondicionados/listar_sitio/$1';


$route['bancos_baterias/listar']['get'] = 'bancos_baterias/listar';
$route['bancos_baterias/banco_bateria/(:any)']['get'] = 'bancos_baterias/editar/$1';
$route['bancos_baterias/banco_bateria/(:any)']['put'] = 'bancos_baterias/actualizar/$1';
$route['bancos_baterias/banco_bateria/sitio']['post'] = 'bancos_baterias/crear_sitio';
$route['bancos_baterias/banco_bateria/(:any)']['delete'] = 'bancos_baterias/eliminar/$1';
$route['bancos_baterias/listar_sitio/(:any)']['get'] = 'bancos_baterias/listar_sitio/$1';
$route['bancos_baterias/bancos_baterias']['put'] = 'bancos_baterias/actualizar_bancos';
$route['bancos_baterias/bancos_baterias/(:any)']['delete'] = 'bancos_baterias/eliminar_bancos/$1';
$route['bancos_baterias/banco_baterias/almacenar/(:num)']['post'] = 'bancos_baterias/almacenar/$1';

// ups
$route['ups/ups/sitio/(:any)']['post'] = 'ups/crear_sitio/$1';
$route['ups/listar']['get'] = 'ups/listar';
$route['ups/ups/(:any)']['get'] = 'ups/editar/$1';
$route['ups/ups/(:any)']['put'] = 'ups/actualizar/$1';
$route['ups/ups/(:any)']['delete'] = 'ups/eliminar/$1';
$route['ups/listar_sitio/(:any)']['get'] = 'ups/listar_sitio/$1';
$route['ups/ups']['put'] = 'ups/actualizar_bancos';
$route['ups/ups/(:any)']['delete'] = 'ups/eliminar/$1';
$route['ups/ups/almacenar/(:num)']['post'] = 'ups/almacenar/$1';
//ups

$route['odus/listar']['get'] = 'odus/listar';
$route['odus/odu/(:any)']['get'] = 'odus/editar/$1';
$route['odus/odu/(:any)']['put'] = 'odus/actualizar/$1';
$route['odus/odu/sitio']['post'] = 'odus/crear_sitio';
$route['odus/odu/(:any)']['delete'] = 'odus/eliminar/$1';
$route['odus/listar_sitio/(:any)']['get'] = 'odus/listar_sitio/$1';

$route['antenas_mw/listar']['get'] = 'antenas_mw/listar';
$route['antenas_mw/antena_mw/(:any)']['get'] = 'antenas_mw/editar/$1';
$route['antenas_mw/antena_mw/(:any)']['put'] = 'antenas_mw/actualizar/$1';
$route['antenas_mw/antena_mw/enlace']['post'] = 'antenas_mw/crear_enlace';
$route['antenas_mw/antena_mw/(:any)']['delete'] = 'antenas_mw/eliminar/$1';
$route['antenas_mw/listar_sitio/(:any)']['get'] = 'antenas_mw/listar_sitio/$1';
$route['antenas_mw/antena_mw/almacenar/(:num)']['post'] = 'antenas_mw/almacenar/$1';

$route['grupos_electrogenos/listar']['get'] = 'grupos_electrogenos/listar';
$route['grupos_electrogenos/grupo_electrogeno/(:any)']['get'] = 'grupos_electrogenos/editar/$1';
$route['grupos_electrogenos/grupo_electrogeno/(:any)']['put'] = 'grupos_electrogenos/actualizar/$1';
$route['grupos_electrogenos/grupo_electrogeno/sitio/(:any)']['post'] = 'grupos_electrogenos/crear_sitio/$1';
$route['grupos_electrogenos/grupo_electrogeno/(:any)']['delete'] = 'grupos_electrogenos/eliminar/$1';
$route['grupos_electrogenos/listar_sitio/(:any)']['get'] = 'grupos_electrogenos/listar_sitio/$1';
$route['grupos_electrogenos/grupo_electrogeno/almacenar/(:any)']['post'] = 'grupos_electrogenos/almacenar/$1';
$route['grupos_electrogenos/grupo_electrogeno/reactivar/(:any)']['put'] = 'almacenes/reactivar/$1';

$route['tarjetas/listar_sitio/(:any)']['get'] = 'tarjetas/listar_sitio/$1';
$route['tarjetas/tarjetas']['put'] = 'tarjetas/actualizar';
$route['tarjetas/tarjetas/sitio/(:any)']['post'] = 'tarjetas/crear_sitio/$1';
$route['tarjetas/tarjetas']['delete'] = 'tarjetas/eliminar';

$route['e_antenas/listar_sitio/(:any)']['get'] = 'e_antenas/listar_sitio/$1';
$route['e_antenas/e_antenas']['put'] = 'e_antenas/actualizar';
$route['e_antenas/e_antenas/sitio/(:any)']['post'] = 'e_antenas/crear_sitio/$1';
$route['e_antenas/e_antenas']['delete'] = 'e_antenas/eliminar';

$route['e_gabinetes/listar_sitio/(:any)']['get'] = 'e_gabinetes/listar_sitio/$1';
$route['e_gabinetes/e_gabinetes']['put'] = 'e_gabinetes/actualizar';
$route['e_gabinetes/e_gabinetes/sitio/(:any)']['post'] = 'e_gabinetes/crear_sitio/$1';
$route['e_gabinetes/e_gabinetes']['delete'] = 'e_gabinetes/eliminar';

$route['microondas_tarjetas/listar_sitio/(:any)']['get'] = 'microondas_tarjetas/listar_sitio/$1';
$route['microondas_tarjetas/tarjetas/sitio/(:any)']['post'] = 'microondas_tarjetas/crear_sitio/$1';
$route['microondas_tarjetas/tarjetas']['delete'] = 'microondas_tarjetas/eliminar';

$route['microondas_odus/listar_sitio/(:any)']['get'] = 'microondas_odus/listar_sitio/$1';
$route['microondas_odus/odus/sitio/(:any)']['post'] = 'microondas_odus/crear_sitio/$1';
$route['microondas_odus/odus']['delete'] = 'microondas_odus/eliminar';

$route['wimax_detailed/listar_sitio/(:any)']['get'] = 'wimax_detailed/listar_sitio/$1';
$route['wimax_detailed/wimax/sitio/(:any)']['post'] = 'wimax_detailed/crear_sitio/$1';
$route['wimax_detailed/wimax']['delete'] = 'wimax_detailed/eliminar';

$route['wimax/listar_sitio/(:any)']['get'] = 'wimax/listar_sitio/$1';
$route['wimax/wimax/sitio/(:any)']['post'] = 'wimax/crear_sitio/$1';
$route['wimax/wimax']['delete'] = 'wimax/eliminar';

$route['iden/listar_sitio/(:num)']['get'] = 'iden/listar_sitio/$1';
$route['iden/iden/(:num)']['put'] = 'iden/actualizar/$1';
$route['iden/iden/sitio/(:num)']['post'] = 'iden/crear_sitio/$1';
$route['iden/iden/(:num)']['delete'] = 'iden/eliminar/$1';

$route['almacenes/grupo_electrogeno']['post'] = 'almacenes/almacenar_grupo_electrogeno';
$route['almacenes/grupo_electrogeno/(:any)']['put'] = 'almacenes/actualizar_grupo_electrogeno/$1';
$route['almacenes/grupo_electrogeno/mover/(:any)']['put'] = 'almacenes/mover_grupo_electrogeno/$1';

$route['mantenimientos_grupos_electrogenos/listar']['get'] = 'mantenimientos_grupos_electrogenos/listar';
$route['mantenimientos_grupos_electrogenos/grupo_electrogeno/(:any)']['get'] = 'mantenimientos_grupos_electrogenos/editar/$1';
$route['mantenimientos_grupos_electrogenos/grupo_electrogeno/(:any)']['put'] = 'mantenimientos_grupos_electrogenos/actualizar/$1';
$route['mantenimientos_grupos_electrogenos/grupo_electrogeno/sitio/(:any)']['post'] = 'mantenimientos_grupos_electrogenos/registrar_mantenimiento/$1';
$route['mantenimientos_grupos_electrogenos/grupo_electrogeno/almacen_alquilado/(:any)']['post'] = 'mantenimientos_grupos_electrogenos/crear_almacen_alquilado/$1';
$route['mantenimientos_grupos_electrogenos/grupo_electrogeno/almacen_propio/(:any)']['post'] = 'mantenimientos_grupos_electrogenos/crear_almacen_propio/$1';
/*$route['mantenimientos_grupos_electrogenos/grupo_electrogeno/(:any)']['delete'] = 'mantenimientos_grupos_electrogenos/eliminar/$1';*/
$route['mantenimientos_grupos_electrogenos/listar_sitio/(:any)']['get'] = 'mantenimientos_grupos_electrogenos/listar_sitio/$1';

$route['mantenimientos_lineas_electricas/listar']['get'] = 'mantenimientos_lineas_electricas/listar';
$route['mantenimientos_lineas_electricas/linea_electrica/(:any)']['get'] = 'mantenimientos_lineas_electricas/editar/$1';
$route['mantenimientos_lineas_electricas/linea_electrica/(:any)']['put'] = 'mantenimientos_lineas_electricas/actualizar/$1';
$route['mantenimientos_lineas_electricas/linea_electrica/sitio/(:any)']['post'] = 'mantenimientos_lineas_electricas/registrar_mantenimiento/$1';
$route['mantenimientos_lineas_electricas/linea_electrica/almacen_alquilado/(:any)']['post'] = 'mantenimientos_lineas_electricas/crear_almacen_alquilado/$1';
$route['mantenimientos_lineas_electricas/linea_electrica/almacen_propio/(:any)']['post'] = 'mantenimientos_lineas_electricas/crear_almacen_propio/$1';
/*$route['mantenimientos_lineas_electricas/linea_electrica/(:any)']['delete'] = 'mantenimientos_lineas_electricas/eliminar/$1';*/
$route['mantenimientos_lineas_electricas/listar_sitio/(:any)']['get'] = 'mantenimientos_lineas_electricas/listar_sitio/$1';

$route['idus/listar']['get'] = 'idus/listar';
$route['idus/idu/(:any)']['get'] = 'idus/editar/$1';
$route['idus/idu/(:any)']['put'] = 'idus/actualizar/$1';
$route['idus/idu/sitio/(:any)']['post'] = 'idus/crear_sitio/$1';
$route['idus/idu/almacen_alquilado/(:any)']['post'] = 'idus/crear_almacen_alquilado/$1';
$route['idus/idu/almacen_propio/(:any)']['post'] = 'idus/crear_almacen_propio/$1';
$route['idus/idu/(:any)']['delete'] = 'idus/eliminar/$1';
$route['idus/listar_sitio/(:any)']['get'] = 'idus/listar_sitio/$1';





$route['enlaces/listar']['get'] = 'enlaces/listar';
$route['enlaces/enlace/(:any)']['get'] = 'enlaces/editar/$1';
$route['enlaces/enlace/(:any)']['put'] = 'enlaces/actualizar/$1';
$route['enlaces/enlace/sitio']['post'] = 'enlaces/crear_sitio';
$route['enlaces/enlace/(:any)']['delete'] = 'enlaces/eliminar/$1';
$route['enlaces/listar_sitio/(:any)']['get'] = 'enlaces/listar_sitio/$1';

$route['concesionaria_suministro/listar']['get'] = 'concesionaria_suministro/listar';
$route['concesionaria_suministro/tipo/(:any)']['get'] = 'concesionaria_suministro/editar/$1';
$route['concesionaria_suministro/tipo/(:any)']['put'] = 'concesionaria_suministro/actualizar/$1';
$route['concesionaria_suministro/tipo']['post'] = 'concesionaria_suministro/crear';
$route['concesionaria_suministro/tipo/(:any)']['delete'] = 'concesionaria_suministro/eliminar/$1';
$route['concesionaria_suministro/tipo/id']['post'] = 'concesionaria_suministro/id_tipo';

$route['conexion_suministro/listar']['get'] = 'conexion_suministro/listar';
$route['conexion_suministro/tipo/(:any)']['get'] = 'conexion_suministro/editar/$1';
$route['conexion_suministro/tipo/(:any)']['put'] = 'conexion_suministro/actualizar/$1';
$route['conexion_suministro/tipo']['post'] = 'conexion_suministro/crear';
$route['conexion_suministro/tipo/(:any)']['delete'] = 'conexion_suministro/eliminar/$1';
$route['conexion_suministro/tipo/id']['post'] = 'conexion_suministro/id_tipo';

$route['conexion_linea_electrica/listar']['get'] = 'conexion_linea_electrica/listar';
$route['conexion_linea_electrica/tipo/(:any)']['get'] = 'conexion_linea_electrica/editar/$1';
$route['conexion_linea_electrica/tipo/(:any)']['put'] = 'conexion_linea_electrica/actualizar/$1';
$route['conexion_linea_electrica/tipo']['post'] = 'conexion_linea_electrica/crear';
$route['conexion_linea_electrica/tipo/(:any)']['delete'] = 'conexion_linea_electrica/eliminar/$1';
$route['conexion_linea_electrica/tipo/id']['post'] = 'conexion_linea_electrica/id_tipo';

$route['sistema_suministro/listar']['get'] = 'sistema_suministro/listar';
$route['sistema_suministro/tipo/(:any)']['get'] = 'sistema_suministro/editar/$1';
$route['sistema_suministro/tipo/(:any)']['put'] = 'sistema_suministro/actualizar/$1';
$route['sistema_suministro/tipo']['post'] = 'sistema_suministro/crear';
$route['sistema_suministro/tipo/(:any)']['delete'] = 'sistema_suministro/eliminar/$1';
$route['sistema_suministro/tipo/id']['post'] = 'sistema_suministro/id_tipo';

$route['ingreso_masivo/plantilla_status_site']['get'] = 'ingreso_masivo/plantilla_status_site';
$route['ingreso_masivo/plantilla_status_site']['post'] = 'ingreso_masivo/plantilla_status_site_personalizada';
$route['ingreso_masivo/upload_file/(:any)']['post'] = 'ingreso_masivo/upload_file/$1';
$route['ingreso_masivo/check_file/(:any)']['post'] = 'ingreso_masivo/check_file/$1';
$route['ingreso_masivo/update_file/(:any)']['get'] = 'ingreso_masivo/update_file/$1';
$route['ingreso_masivo/confirmar_sitio/(:any)'] = 'ingreso_masivo/confirmar_sitio/$1';
$route['ingreso_masivo/listar'] = 'ingreso_masivo/listar';
$route['ingreso_masivo/listar_pendientes/(:any)'] = 'ingreso_masivo/listar_pendientes/$1';
$route['ingreso_masivo/listar_detalles/(:any)/(:any)'] = 'ingreso_masivo/listar_detalles_ingreso_masivo/$1/$2';

$route['coubicador/listar']['get'] = 'coubicador/listar';
$route['coubicador/tipo/(:any)']['get'] = 'coubicador/editar/$1';
$route['coubicador/tipo/(:any)']['put'] = 'coubicador/actualizar/$1';
$route['coubicador/tipo']['post'] = 'coubicador/crear';
$route['coubicador/tipo/(:any)']['delete'] = 'coubicador/eliminar/$1';

$route['coubicante/listar']['get'] = 'coubicante/listar';
$route['coubicante/tipo/(:any)']['get'] = 'coubicante/editar/$1';
$route['coubicante/tipo/(:any)']['put'] = 'coubicante/actualizar/$1';
$route['coubicante/tipo']['post'] = 'coubicante/crear';
$route['coubicante/tipo/(:any)']['delete'] = 'coubicante/eliminar/$1';

$route['proyecto/listar']['get'] = 'proyecto/listar';
$route['proyecto/tipo/(:any)']['get'] = 'proyecto/editar/$1';
$route['proyecto/tipo/(:any)']['put'] = 'proyecto/actualizar/$1';
$route['proyecto/tipo']['post'] = 'proyecto/crear';
$route['proyecto/tipo/(:any)']['delete'] = 'proyecto/eliminar/$1';

$route['contrata_constructora/listar']['get'] = 'contrata_constructora/listar';
$route['contrata_constructora/tipo/(:any)']['get'] = 'contrata_constructora/editar/$1';
$route['contrata_constructora/tipo/(:any)']['put'] = 'contrata_constructora/actualizar/$1';
$route['contrata_constructora/tipo']['post'] = 'contrata_constructora/crear';
$route['contrata_constructora/tipo/(:any)']['delete'] = 'contrata_constructora/eliminar/$1';

$route['tipo_solucion/listar']['get'] = 'tipo_solucion/listar';
$route['tipo_solucion/tipo/(:any)']['get'] = 'tipo_solucion/editar/$1';
$route['tipo_solucion/tipo/(:any)']['put'] = 'tipo_solucion/actualizar/$1';
$route['tipo_solucion/tipo']['post'] = 'tipo_solucion/crear';
$route['tipo_solucion/tipo/(:any)']['delete'] = 'tipo_solucion/eliminar/$1';

$route['proveedor_mantenimiento/listar']['get'] = 'proveedor_mantenimiento/listar';
$route['proveedor_mantenimiento/tipo/(:any)']['get'] = 'proveedor_mantenimiento/editar/$1';
$route['proveedor_mantenimiento/tipo/(:any)']['put'] = 'proveedor_mantenimiento/actualizar/$1';
$route['proveedor_mantenimiento/tipo']['post'] = 'proveedor_mantenimiento/crear';
$route['proveedor_mantenimiento/tipo/(:any)']['delete'] = 'proveedor_mantenimiento/eliminar/$1';

$route['bodegas/grupos_electrogenos/listar']['get'] = 'bodega_grupos_electrogenos/listar';
$route['bodegas/grupos_electrogenos/listar_almacen/(:num)']['get'] = 'bodega_grupos_electrogenos/listar_almacen/$1';
$route['bodegas/grupos_electrogenos/listar_sitio/(:num)']['get'] = 'bodega_grupos_electrogenos/listar_sitio/$1';
$route['bodegas/grupos_electrogenos/grupo_electrogeno/(:num)']['put'] = 'bodega_grupos_electrogenos/actualizar/$1';
$route['bodegas/grupos_electrogenos/grupo_electrogeno/(:num)']['post'] = 'bodega_grupos_electrogenos/reactivar/$1';
$route['bodegas/grupos_electrogenos/grupo_electrogeno/mover/(:num)']['put'] = 'bodega_grupos_electrogenos/mover/$1';
$route['bodegas/grupos_electrogenos/grupo_electrogeno/(:num)']['delete'] = 'bodega_grupos_electrogenos/eliminar/$1';

$route['bodegas/elementos_mw/listar/(:num)']['get'] = 'bodega_elementos_mw/listar/$1';
$route['bodegas/elementos_mw/elemento_mw/(:num)']['delete'] = 'bodega_elementos_mw/eliminar/$1';

// sistema combustible
$route['bodegas/elementos_sistema_combustible/listar/(:num)']['get'] = 'bodega_elementos_sistema_combustible/listar/$1';

$route['bodegas/elementos_sistema_combustible/elemento_sistema_combustible/(:num)']['delete'] = 'bodega_elementos_sistema_combustible/eliminar/$1';
// sistema combustible


$route['bodegas/antenas_mw/listar']['get'] = 'bodega_elementos_mw/listar_antenas';
$route['bodegas/antenas_mw/listar_almacen/(:num)']['get'] = 'bodega_antenas_mw/listar_almacen/$1';
$route['bodegas/antenas_mw/listar_sitio/(:num)']['get'] = 'bodega_antenas_mw/listar_sitio/$1';
$route['bodegas/antenas_mw/antena_mw/(:num)']['put'] = 'bodega_antenas_mw/actualizar/$1';
$route['bodegas/antenas_mw/antena_mw/(:num)']['post'] = 'bodega_antenas_mw/reactivar/$1';
$route['bodegas/antenas_mw/antena_mw/mover/(:num)']['put'] = 'bodega_antenas_mw/mover/$1';
$route['bodegas/antenas_mw/antena_mw/(:num)']['delete'] = 'bodega_antenas_mw/eliminar/$1';


// Sistema combustible
$route['bodegas/sistema_combustible/listar']['get'] = 'bodega_elementos_sistema_combustible/listar_antenas';
$route['bodegas/sistema_combustible/listar_almacen/(:num)']['get'] = 'bodega_sistema_combustible/listar_almacen/$1';
$route['bodegas/sistema_combustible/listar_sitio/(:num)']['get'] = 'bodega_sistema_combustible/listar_sitio/$1';
$route['bodegas/sistema_combustible/sistema_combustible/(:num)']['put'] = 'bodega_sistema_combustible/actualizar/$1';
$route['bodegas/sistema_combustible/sistema_combustible/(:num)']['post'] = 'bodega_sistema_combustible/reactivar/$1';
$route['bodegas/sistema_combustible/sistema_combustible/mover/(:num)']['put'] = 'bodega_sistema_combustible/mover/$1';
$route['bodegas/sistema_combustible/sistema_combustible/(:num)']['delete'] = 'bodega_sistema_combustible/eliminar/$1';
//Sistema combustible

//Módulos Rectificadores 
// $route['bodega_modulos_rectif/mover/(:num)']['put'] = 'bodega_modulos_rectif/almacenar/$1';
// $route['bodega_modulos_rectif/eliminar/(:num)']['delete'] = 'bodega_modulos_rectif/eliminar/$1';

//Módulos Rectificadores 




$route['bodegas/antenas_gul/listar']['get'] = 'bodega_antenas_gul/listar';
$route['bodegas/antenas_gul/listar_almacen/(:num)']['get'] = 'bodega_antenas_gul/listar_almacen/$1';
$route['bodegas/antenas_gul/listar_sitio/(:num)']['get'] = 'bodega_antenas_gul/listar_sitio/$1';
$route['bodegas/antenas_gul/antena_gul/(:num)']['put'] = 'bodega_antenas_gul/actualizar/$1';
$route['bodegas/antenas_gul/antena_gul/(:num)']['post'] = 'bodega_antenas_gul/reactivar/$1';
$route['bodegas/antenas_gul/antena_gul/mover/(:num)']['put'] = 'bodega_antenas_gul/mover/$1';
$route['bodegas/antenas_gul/antena_gul/(:num)']['delete'] = 'bodega_antenas_gul/eliminar/$1';

$route['bodegas/antenas_iden/listar']['get'] = 'bodega_antenas_iden/listar';
$route['bodegas/antenas_iden/listar_almacen/(:num)']['get'] = 'bodega_antenas_iden/listar_almacen/$1';
$route['bodegas/antenas_iden/listar_sitio/(:num)']['get'] = 'bodega_antenas_iden/listar_sitio/$1';
$route['bodegas/antenas_iden/antena_iden/(:num)']['put'] = 'bodega_antenas_iden/actualizar/$1';
$route['bodegas/antenas_iden/antena_iden/(:num)']['post'] = 'bodega_antenas_iden/reactivar/$1';
$route['bodegas/antenas_iden/antena_iden/mover/(:num)']['put'] = 'bodega_antenas_iden/mover/$1';
$route['bodegas/antenas_iden/antena_iden/(:num)']['delete'] = 'bodega_antenas_iden/eliminar/$1';

$route['bodegas/bancos_baterias/listar']['get'] = 'bodega_bancos_baterias/listar';
$route['bodegas/bancos_baterias/listar_almacen/(:num)']['get'] = 'bodega_bancos_baterias/listar_almacen/$1';
$route['bodegas/bancos_baterias/listar_sitio/(:num)']['get'] = 'bodega_bancos_baterias/listar_sitio/$1';
$route['bodegas/bancos_baterias/banco_baterias/(:num)']['put'] = 'bodega_bancos_baterias/actualizar/$1';
$route['bodegas/bancos_baterias/banco_baterias/(:num)']['post'] = 'bodega_bancos_baterias/reactivar/$1';
$route['bodegas/bancos_baterias/banco_baterias/mover/(:num)']['put'] = 'bodega_bancos_baterias/mover/$1';
$route['bodegas/bancos_baterias/banco_baterias/(:num)']['delete'] = 'bodega_bancos_baterias/eliminar/$1';

$route['bodegas/aires_acondicionados/aire_acondicionado/(:num)']['delete'] = 'bodega_aires_acondicionados/eliminar/$1';

$route['bodegas/modulos_rectif/modulo_rectif/(:num)']['delete'] = 'bodega_modulos_recif/eliminar/$1';

$route['bodegas/tanques_combustible/tanque_combustible/(:num)']['delete'] = 'bodega_tanques_combustible/eliminar/$1';

$route['ingreso/(:any)']['get'] = 'vistas/bodegas_elemento/$1';

$route['ingreso_masivo/plantillas/antenas_gul']['get'] = 'ingreso_masivo_antenas_gul/plantilla_antenas_gul';
$route['ingreso_masivo/plantillas/tipo_antenas_gul']['get'] = 'ingreso_masivo_antenas_gul/plantilla_tipo_antenas_gul';
$route['ingreso_masivo/antenas_gul/subir']['post'] = 'ingreso_masivo_antenas_gul/upload_file';
$route['ingreso_masivo/antenas_gul/analizar']['post'] = 'ingreso_masivo_antenas_gul/check_file';
$route['ingreso_masivo/antenas_gul/enviar']['post'] = 'ingreso_masivo_antenas_gul/insert_temp';
$route['ingreso_masivo/antenas_gul/listar_solicitudes/(:num)']['get'] = 'ingreso_masivo_antenas_gul/listar_solicitudes/$1';

$route['ingreso_masivo/plantillas/suministros']['get'] = 'ingreso_masivo_suministros/plantilla_suministros';
$route['ingreso_masivo/plantillas/desplegables_suministros']['get'] = 'ingreso_masivo_suministros/plantilla_desplegables_suministros';
$route['ingreso_masivo/suministros/subir']['post'] = 'ingreso_masivo_suministros/upload_file';
$route['ingreso_masivo/suministros/analizar']['post'] = 'ingreso_masivo_suministros/check_file';
$route['ingreso_masivo/suministros/enviar']['post'] = 'ingreso_masivo_suministros/insert_temp';
$route['ingreso_masivo/suministros/listar_solicitudes/(:num)']['get'] = 'ingreso_masivo_suministros/listar_solicitudes/$1';

$route['ingreso_masivo/plantillas/idus']['get'] = 'ingreso_masivo_idus/plantilla_idus';
$route['ingreso_masivo/plantillas/tipo_idu']['get'] = 'ingreso_masivo_idus/plantilla_tipo_idu';
$route['ingreso_masivo/idus/subir']['post'] = 'ingreso_masivo_idus/upload_file';
$route['ingreso_masivo/idus/analizar']['post'] = 'ingreso_masivo_idus/check_file';
$route['ingreso_masivo/idus/enviar']['post'] = 'ingreso_masivo_idus/insert_temp';
$route['ingreso_masivo/idus/listar_solicitudes/(:num)']['get'] = 'ingreso_masivo_idus/listar_solicitudes/$1';

$route['ingreso_masivo/plantillas/enlaces']['get'] = 'ingreso_masivo_enlaces/plantilla_enlaces';
$route['ingreso_masivo/plantillas/elementos_enlaces']['get'] = 'ingreso_masivo_enlaces/plantilla_elementos_enlaces';
$route['ingreso_masivo/enlaces/subir']['post'] = 'ingreso_masivo_enlaces/upload_file';
$route['ingreso_masivo/enlaces/analizar']['post'] = 'ingreso_masivo_enlaces/check_file';
$route['ingreso_masivo/enlaces/enviar']['post'] = 'ingreso_masivo_enlaces/insert_temp';
$route['ingreso_masivo/enlaces/listar_solicitudes/(:num)']['get'] = 'ingreso_masivo_enlaces/listar_solicitudes/$1';

$route['ingreso_masivo/plantillas/pop']['get'] = 'ingreso_masivo_pop/plantilla_pop';
$route['ingreso_masivo/plantillas/desplegables_pop']['get'] = 'ingreso_masivo_pop/plantilla_desplegables_pop';
$route['ingreso_masivo/pop/subir']['post'] = 'ingreso_masivo_pop/upload_file';
$route['ingreso_masivo/pop/analizar']['post'] = 'ingreso_masivo_pop/check_file';
$route['ingreso_masivo/pop/enviar']['post'] = 'ingreso_masivo_pop/insert_temp';
$route['ingreso_masivo/pop/listar_solicitudes/(:num)']['get'] = 'ingreso_masivo_pop/listar_solicitudes/$1';

$route['ingreso_masivo/plantillas/rectificadores']['get'] = 'ingreso_masivo_rectificadores/plantilla_rectificadores';
$route['ingreso_masivo/plantillas/desplegables_rectificadores']['get'] = 'ingreso_masivo_rectificadores/plantilla_desplegables_rectificadores';
$route['ingreso_masivo/rectificadores/subir']['post'] = 'ingreso_masivo_rectificadores/upload_file';
$route['ingreso_masivo/rectificadores/analizar']['post'] = 'ingreso_masivo_rectificadores/check_file';
$route['ingreso_masivo/rectificadores/enviar']['post'] = 'ingreso_masivo_rectificadores/insert_temp';
$route['ingreso_masivo/rectificadores/listar_solicitudes/(:num)']['get'] = 'ingreso_masivo_rectificadores/listar_solicitudes/$1';

$route['ingreso_masivo/plantillas/bancos_baterias']['get'] = 'ingreso_masivo_bancos_baterias/plantilla_bancos_baterias';
$route['ingreso_masivo/plantillas/bancos_baterias_actualizacion']['get'] = 'ingreso_masivo_bancos_baterias/plantilla_bancos_baterias_actualizacion';
$route['ingreso_masivo/plantillas/tipo_bancos_baterias']['get'] = 'ingreso_masivo_bancos_baterias/plantilla_tipo_bancos_baterias';
$route['ingreso_masivo/bancos_baterias/subir']['post'] = 'ingreso_masivo_bancos_baterias/upload_file';
$route['ingreso_masivo/bancos_baterias/analizar']['post'] = 'ingreso_masivo_bancos_baterias/check_file';
$route['ingreso_masivo/bancos_baterias/enviar']['post'] = 'ingreso_masivo_bancos_baterias/insert_temp';
$route['ingreso_masivo/bancos_baterias/listar_solicitudes/(:num)']['get'] = 'ingreso_masivo_bancos_baterias/listar_solicitudes/$1';

$route['ingreso_masivo/solicitudes/solicitud/(:num)']['put'] = 'ingreso_masivo/finalizar_solicitud/$1';
$route['ingreso_masivo/filas_pendientes/listar/(:num)']['get'] = 'ingreso_masivo/listar_filas_pendientes/$1';

// Solicitud gul
$route['solicitud_instalacion_antenas/filas_pendientes/listar/(:num)']['get'] = 'solicitud_instalacion_antenas/listar_filas_pendientes/$1';

$route['solicitud_instalacion_antenas/filas_reservado/listar/(:num)']['get'] = 'solicitud_instalacion_antenas/listar_filas_reservado/$1';

$route['solicitud_instalacion_antenas/filas_instalado/listar/(:num)']['get'] = 'solicitud_instalacion_antenas/listar_filas_instalado/$1';


// Solicitud Microondas
$route['solicitud_instalacion_antenas/solicitudes_mw/listar/(:num)']['get'] = 'solicitud_instalacion_antenas/solicitudes_listar_filas_mw/$1';

$route['solicitud_instalacion_antenas/filas_pendientes_mw/listar/(:num)']['get'] = 'solicitud_instalacion_antenas/listar_filas_pendientes_mw/$1';

$route['solicitud_instalacion_antenas/filas_reservado_mw/listar/(:num)']['get'] = 'solicitud_instalacion_antenas/listar_filas_reservado_mw/$1';

$route['solicitud_instalacion_antenas/filas_instalado_mw/listar/(:num)']['get'] = 'solicitud_instalacion_antenas/listar_filas_instalado_mw/$1';


// $route['solicitud_instalacion_antenas/solicitudes_mw/listarAll/(:num)']['get'] = 'solicitud_instalacion_antenas/solicitudes_mw_listar_all/$1';


$route['ingreso_masivo/filas_validadas/listar/(:num)']['get'] = 'ingreso_masivo/listar_filas_validadas/$1';

$route['ingreso_masivo/antenas_gul/aprobar/(:num)']['post'] = 'ingreso_masivo/aprobar_fila_antenas_gul/$1';
$route['ingreso_masivo/antenas_gul/rechazar/(:num)']['put'] = 'ingreso_masivo/rechazar_fila_antenas_gul/$1';

$route['ingreso_masivo/suministros/aprobar/(:num)']['post'] = 'ingreso_masivo/aprobar_fila_suministros/$1';
$route['ingreso_masivo/suministros/rechazar/(:num)']['put'] = 'ingreso_masivo/rechazar_fila_suministros/$1';

$route['ingreso_masivo/idus/aprobar/(:num)']['post'] = 'ingreso_masivo/aprobar_fila_idus/$1';
$route['ingreso_masivo/idus/rechazar/(:num)']['put'] = 'ingreso_masivo/rechazar_fila_idus/$1';

$route['ingreso_masivo/enlaces/aprobar/(:num)']['post'] = 'ingreso_masivo/aprobar_fila_enlaces/$1';
$route['ingreso_masivo/enlaces/rechazar/(:num)']['put'] = 'ingreso_masivo/rechazar_fila_enlaces/$1';

$route['ingreso_masivo/pop/aprobar/(:num)']['post'] = 'ingreso_masivo/aprobar_fila_pop/$1';
$route['ingreso_masivo/pop/rechazar/(:num)']['put'] = 'ingreso_masivo/rechazar_fila_pop/$1';

$route['ingreso_masivo/contratos/aprobar/(:num)']['post'] = 'ingreso_masivo/aprobar_fila_contratos/$1';
$route['ingreso_masivo/contratos/rechazar/(:num)']['put'] = 'ingreso_masivo/rechazar_fila_contratos/$1';

$route['ingreso_masivo/rectificadores/aprobar/(:num)']['post'] = 'ingreso_masivo/aprobar_fila_rectificadores/$1';
$route['ingreso_masivo/rectificadores/rechazar/(:num)']['put'] = 'ingreso_masivo/rechazar_fila_rectificadores/$1';

$route['ingreso_masivo/bancos_baterias/aprobar/(:num)']['post'] = 'ingreso_masivo/aprobar_fila_bancos_baterias/$1';
$route['ingreso_masivo/bancos_baterias/rechazar/(:num)']['put'] = 'ingreso_masivo/rechazar_fila_bancos_baterias/$1';

$route['reportes/predeterminados/pop']['get'] = 'reportes/reportes_predeterminados_pop';
$route['reportes/predeterminados/pop']['post'] = 'reportes/reportes_predeterminados_pop';
$route['reportes/predeterminados/antenas_gul']['get'] = 'reportes/reportes_predeterminados_antenas_gul';
$route['reportes/predeterminados/antenas_gul']['post'] = 'reportes/reportes_predeterminados_antenas_gul';
$route['reportes/predeterminados/antenas_iden']['get'] = 'reportes/reportes_predeterminados_antenas_iden';
$route['reportes/predeterminados/antenas_iden']['post'] = 'reportes/reportes_predeterminados_antenas_iden';
$route['reportes/predeterminados/antenas_in_building']['get'] = 'reportes/reportes_predeterminados_antenas_in_building';
$route['reportes/predeterminados/antenas_in_building']['post'] = 'reportes/reportes_predeterminados_antenas_in_building';
$route['reportes/predeterminados/idus']['get'] = 'reportes/reportes_predeterminados_idus';
$route['reportes/predeterminados/idus']['post'] = 'reportes/reportes_predeterminados_idus';
$route['reportes/predeterminados/enlaces']['get'] = 'reportes/reportes_predeterminados_enlaces';
$route['reportes/predeterminados/enlaces']['post'] = 'reportes/reportes_predeterminados_enlaces';
$route['reportes/predeterminados/suministros']['get'] = 'reportes/reportes_predeterminados_suministros';
$route['reportes/predeterminados/suministros']['post'] = 'reportes/reportes_predeterminados_suministros';
$route['reportes/predeterminados/tanques_combustible']['get'] = 'reportes/reportes_predeterminados_tanques_combustible';
$route['reportes/predeterminados/tanques_combustible']['post'] = 'reportes/reportes_predeterminados_tanques_combustible';
$route['reportes/predeterminados/lineas_electricas']['get'] = 'reportes/reportes_predeterminados_lineas_electricas';
$route['reportes/predeterminados/lineas_electricas']['post'] = 'reportes/reportes_predeterminados_lineas_electricas';
$route['reportes/predeterminados/rectificadores']['get'] = 'reportes/reportes_predeterminados_rectificadores';
$route['reportes/predeterminados/rectificadores']['post'] = 'reportes/reportes_predeterminados_rectificadores';
$route['reportes/predeterminados/aires_acondicionados']['get'] = 'reportes/reportes_predeterminados_aires_acondicionados';
$route['reportes/predeterminados/aires_acondicionados']['post'] = 'reportes/reportes_predeterminados_aires_acondicionados';
$route['reportes/predeterminados/bancos_baterias']['get'] = 'reportes/reportes_predeterminados_bancos_baterias';
$route['reportes/predeterminados/bancos_baterias']['post'] = 'reportes/reportes_predeterminados_bancos_baterias';
$route['reportes/predeterminados/grupos_electrogenos']['get'] = 'reportes/reportes_predeterminados_grupos_electrogenos';
$route['reportes/predeterminados/grupos_electrogenos']['post'] = 'reportes/reportes_predeterminados_grupos_electrogenos';
$route['reportes/predeterminados/antenas_nodo_b']['get'] = 'reportes/reportes_predeterminados_e_antenas';
$route['reportes/predeterminados/antenas_nodo_b']['post'] = 'reportes/reportes_predeterminados_e_antenas';
$route['reportes/predeterminados/tarjetas_nodo_b/(:any)']['get'] = 'reportes/reportes_predeterminados_tarjetas_nodo_b/$1';
$route['reportes/predeterminados/tarjetas_nodo_b']['post'] = 'reportes/reportes_predeterminados_tarjetas_nodo_b';
$route['reportes/predeterminados/gabinetes_nodo_b']['get'] = 'reportes/reportes_predeterminados_e_gabinetes';
$route['reportes/predeterminados/gabinetes_nodo_b']['post'] = 'reportes/reportes_predeterminados_e_gabinetes';
$route['reportes/predeterminados/tarjetas_microondas/(:any)']['get'] = 'reportes/reportes_predeterminados_mw_tarjetas/$1';
$route['reportes/predeterminados/tarjetas_microondas']['post'] = 'reportes/reportes_predeterminados_mw_tarjetas';
$route['reportes/predeterminados/odus_microondas']['get'] = 'reportes/reportes_predeterminados_mw_odus';
$route['reportes/predeterminados/odus_microondas']['post'] = 'reportes/reportes_predeterminados_mw_odus';
$route['reportes/predeterminados/wimax']['get'] = 'reportes/reportes_predeterminados_wimax';
$route['reportes/predeterminados/wimax']['post'] = 'reportes/reportes_predeterminados_wimax';
$route['reportes/predeterminados/iden']['get'] = 'reportes/reportes_predeterminados_iden';
$route['reportes/predeterminados/iden']['post'] = 'reportes/reportes_predeterminados_iden';

$route['reportes/predeterminados/bodegas']['get'] = 'reportes/reportes_predeterminados_bodegas';
$route['reportes/predeterminados/bodegas']['post'] = 'reportes/reportes_predeterminados_bodegas';


$route['reportes/predeterminados/grupos_electrogenos_almacenados']['get'] = 'reportes/reportes_predeterminados_grupos_electrogenos_almacenados';
$route['reportes/predeterminados/grupos_electrogenos_almacenados']['post'] = 'reportes/reportes_predeterminados_grupos_electrogenos_almacenados';

$route['reportes/personalizados/sitio_individual']['get'] = 'reportes/reportes_personalizados_sitio_individual';
$route['reportes/personalizados/sitio_individual']['post'] = 'reportes/reportes_personalizados_sitio_individual';

$route['validaciones/validar']['get'] = 'validaciones/listar';
$route['validaciones/validar']['post'] = 'validaciones/validar';
$route['validaciones/validar/(:any)']['put'] = 'validaciones/cancelar/$1';

$route['evaluaciones/contrata/listar']['get'] = 'evaluaciones_contrata/listar';

$route['evaluaciones/evaluaciones_periodo/listar_pendientes']['get'] = 'evaluaciones_evaluaciones_periodo/listar_pendientes';

$route['evaluaciones/avance_evaluacion/listar_pendientes/(:any)']['get'] = 'evaluaciones_avance_evaluacion/listar_pendientes/$1';
$route['evaluaciones/avance_evaluacion/listar_evaluados/(:any)']['get'] = 'evaluaciones_avance_evaluacion/listar_evaluados/$1';
$route['evaluaciones/avance_evaluacion/(:any)/(:any)'] = 'evaluaciones_avance_evaluacion/info_evaluado/$1/$2';
$route['evaluaciones/avance_evaluacion/(:any)']['put'] = 'evaluaciones_avance_evaluacion/registrar_evaluacion/$1';

$route['evaluaciones/evaluacion/listar_activas']['get'] = 'evaluaciones_evaluacion/listar_activas';
$route['evaluaciones/evaluacion/(:any)']['put'] = 'evaluaciones_evaluacion/actualizar/$1';
$route['evaluaciones/evaluacion/(:any)']['delete'] = 'evaluaciones_evaluacion/eliminar/$1';

$route['ftp/listar']['post'] = 'ftp/listar';
$route['ftp/download']['post'] = 'ftp/download';
$route['ftp/delete']['post'] = 'ftp/delete';
$route['ftp/upload']['post'] = 'ftp/upload';
$route['ftp/rename']['post'] = 'ftp/rename';

$route['sftp_controller/listar']['post'] = 'sftp_controller/listar';
$route['sftp_controller/download']['post'] = 'sftp_controller/download';
$route['sftp_controller/delete']['post'] = 'sftp_controller/delete';
$route['sftp_controller/upload']['post'] = 'sftp_controller/upload';
$route['sftp_controller/rename']['post'] = 'sftp_controller/rename';

/********************************************/
/* Rutas a Logs          	                */
/********************************************/

$route['logs_suministros/listar']['get'] = 'logs_suministros/listar';

/*$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;*/
