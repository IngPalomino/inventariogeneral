(function() {
	'use strict';

	angular
		.module('app')
		.directive('ngLoading', ngLoading);

	ngLoading.$inject = ['$compile', '$http'];

	function ngLoading($compile, $http) {
		var loadingSpinner = '<div class="text-center"><i class="fa fa-spinner fa-spin fa-4x"></i></div>';
		var notFound = '<div class="well text-center">No hay información.</div>';

		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var originalContent = element.html();
				element.html(loadingSpinner);

				scope.$watch(attrs.ngLoading, function(val) {
					if (val.length) {
						element.html(originalContent);
						$compile(element.contents())(scope);
					} else if ( ! val.length && $http.pendingRequests.length === 0) {
						element.html(notFound);
					} else {
						element.html(loadingSpinner);
					}
				});
			}
		}
	}
})();