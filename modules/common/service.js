(function() {
	'use strict';

	angular
		.module('app')
		.factory('common', common);

	common.$inject = ['phpService'];

	function common(phpService) {
		var service = {
			serializeObj: serializeObj,
			base_url: phpService.base_url,
			objEditar: objEditar,
			arrayDifferences: arrayDifferences
		};

		return service;

		function serializeObj(obj) {
			var result = [];

			for (var property in obj) {
				if (property != 'id' && property != 'created_at' && typeof obj[property] != 'function') {
					if (Array.isArray(obj[property])) {
						result.push(encodeURIComponent(property) + '=' + angular.toJson(obj[property]));
					} else if (typeof obj[property] == 'object') {
						try { result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]['id'])); }
						catch(err) { result.push(encodeURIComponent(property) + '=' + null); }
					} else if (property == 'tipo_almacen') {
						if (obj[property] == 1) {
							result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
							result.push('almacen=' + encodeURIComponent(obj['almacen']));
						} else if (obj[property] == 2) {
							result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
							result.push('almacen=' + encodeURIComponent(obj['a_sitio']));
							delete obj['a_sitio'];
						}
					} else {
						result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
					}
				}
			}

			return result.join('&');
		}

		function objEditar(obj) {
			var tipo = typeof obj,
				copia,
				propNames;

			if (tipo != 'object') {
				copia = JSON.parse(JSON.stringify(obj));
			} else {
				if (obj != null && obj != undefined) {
					if (Array.isArray(obj)) {
						copia = [];
						obj.forEach(function(r, i, o) {
							var reg = objEditar(r);
							copia.push(reg);
						});
					} else {
						try {
							copia = new Date(obj.getTime());
						} catch(err) {
							copia = Object.create(Object.getPrototypeOf(obj));
							propNames = Object.getOwnPropertyNames(obj);

							propNames.forEach(function(name) {
								var tipo2 = typeof obj[name];

								if (tipo2 != 'object') {
									var desc = Object.getOwnPropertyDescriptor(obj, name);
									Object.defineProperty(copia, name, desc);
								} else {
									copia[name] = objEditar(obj[name]);
								}
							});
						}
					}
				} else {
					copia = obj;
				}
			}

			return copia;
		}

		function arrayDifferences(oldArray, newArray) {
			var returnArray;

			if (Array.isArray(newArray)) {
				returnArray = [];

				newArray.forEach(function(arr, ind, obj) {
					if (oldArray[ind] != newArray[ind]) {
						returnArray.push(arr);
					}
				});
			} else {
				returnArray = {};
				var keys = Object.keys(oldArray);

				keys.forEach(function(key) {
					if (Array.isArray(oldArray[key]) || Array.isArray(newArray[key])) {
						if (angular.toJson(oldArray[key]) != angular.toJson(newArray[key])) {
							returnArray[key] = newArray[key];
						}
					} else {
						if (oldArray[key] != newArray[key]) {
							returnArray[key] = newArray[key];
						}
					}
				});
			}

			return returnArray;
		}
	}
})();