(function() {
	'use strict';

	angular
		.module('app')
		.controller('sharedController', sharedController);

	sharedController.$inject = ['$http', 'common'];

	function sharedController($http, common) {
		var ref = this;

		ref.iniciarSesion = iniciarSesion;
		ref.cerrarSesion = cerrarSesion;
		ref.enviarForgotPassword = enviarForgotPassword;
		ref.loginForm = {};
		ref.loginForm.usuario = '';
		ref.loginForm.contrasena = '';
		ref.loginMessages = {};
		ref.loginMessages.error = '';
		ref.loginMessages.success = '';
		ref.forgotPasswordForm = {};
		ref.forgotPasswordMessages = {};
		ref.forgotPasswordMessages.error = '';
		ref.forgotPasswordMessages.success = '';
		ref.restablecerContrasena = restablecerContrasena;

		function iniciarSesion() {
			ref.loginMessages = {};

			$http({
				url: common.base_url('sesion/iniciar'),
				method: 'POST',
				data: common.serializeObj(ref.loginForm),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function successCallback(resp) {
				if (resp.data.error) {
					ref.loginMessages.error = resp.data.error;
				} else if (resp.data.logged) {
					location.reload();
				}
			});
		}

		function cerrarSesion() {
			$http({
				url: common.base_url('sesion/cerrar'),
				method: 'GET'
			}).then(function successCallback(resp) {
				if ( ! resp.data.logged) {
					window.location = common.base_url();
				}
			});
		}

		$('#forgotPassword').on('hidden.bs.modal', function() {
			ref.forgotPasswordForm = {};
			ref.forgotPasswordMessages = {};
		});

		function restablecerContrasena() {
			$('#forgotPassword').modal('show');;
		}

		function enviarForgotPassword() {
			ref.procesando = true;
			ref.forgotPasswordMessages = {};

			$http({
				url: common.base_url('usuarios/forgot_password'),
				method: 'POST',
				data: common.serializeObj(ref.forgotPasswordForm),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function successCallback(resp) {
				ref.procesando = false;

				if (resp.data.error) {
					ref.forgotPasswordMessages.error = resp.data.messages;
				} else {
					ref.forgotPasswordForm = {};
					ref.forgotPasswordMessages.success = resp.data.messages;
					setTimeout(function cerrarPopup() { $('#forgotPassword').modal('hide'); }, 3000);
				}
			});
		}
	}
})();