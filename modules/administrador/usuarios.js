(function() {
	'use strict';

	angular
		.module('app', ['angular-loading-bar', 'selectize'])
		.controller('controlador', controlador);

	controlador.$inject = ['$http', 'common'];

	function controlador($http, common) {
		var ref = this;
		var unchangedForm = 'No se han hecho cambios en el formulario.';

		ref.config = {
			valueField: 'id',
			labelField: 'nombre',
			searchField: 'nombre',
			delimiter: '|',
			placeholder: 'Seleccionar grupo(s)'
		};
		ref.crearUsuario = crearUsuario;
		ref.crearUsuarioMessages = {};
		ref.disabledCrear = disabledCrear;
		ref.editandoUsuario = editandoUsuario;
		ref.editarUsuarioForm = {};
		ref.editarUsuarioMessages = {};
		ref.filtroUsuarios = filtroUsuarios;
		ref.grabarUsuario = grabarUsuario;
		ref.grupos = [];
		ref.nuevoUsuario = nuevoUsuario;
		ref.nuevoUsuarioForm = {grupos: []};
		ref.search = {
			usuario: '',
			nombres: '',
			apellido: '',
			grupo: ''
		};
		ref.usarRandom = usarRandom;
		ref.usuarios = [];

		String.prototype.pick = pick;
		String.prototype.shuffle = shuffle;

		listar();
		listarGrupos();

		function listar() {
			$http({
				url: common.base_url('usuarios/listar'),
				method: 'GET'
			}).then(function successCallback(resp) {
				if (resp.data.length) {
					ref.usuarios = resp.data;

					ref.usuarios.forEach(function(usuario) {
						usuario.activo = Boolean(Number(usuario.activo));

						usuario.grupos = [];

						usuario.usuarios_grupos.forEach(function(grupo) {
							usuario.grupos.push(grupo.id);
						});
					});
				}
			});
		}

		function listarGrupos() {
			$http({
				url: common.base_url('grupos/select'),
				method: 'GET'
			}).then(function successCallback(resp) {
				if (resp.data.info.length) {
					ref.grupos = resp.data.info;
				} else {
					ref.grupos = [];
				}
			});
		}

		$('#nuevoUsuario').on('hidden.bs.modal', function() {
			ref.nuevoUsuarioForm = {grupos: []};
			ref.crearUsuarioMessages = {};
		});

		function nuevoUsuario() {
			$('#nuevoUsuario').modal('show');
		}

		function crearUsuario() {
			ref.crearUsuarioMessages = {};

			$http({
				url: common.base_url('usuarios/usuario'),
				method: 'POST',
				data: common.serializeObj(ref.nuevoUsuarioForm),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function successCallback(resp) {
				if ( ! resp.data.error) {
					ref.crearUsuarioMessages.success = resp.data.messages;
					listar();
					setTimeout(function() { $('#nuevoUsuario').modal('hide'); }, 3000);
				} else {
					ref.crearUsuarioMessages.error = resp.data.messages;
				}
			});
		}

		$('#editarUsuario').on('hidden.bs.modal', function() {
			ref.editarUsuarioForm = {};
			ref.editarUsuarioForm_e = {};
			ref.editarUsuarioMessages = {};
		});

		function editandoUsuario(usuario) {
			ref.editarUsuarioForm = usuario;
			ref.editarUsuarioForm_e = common.objEditar(ref.editarUsuarioForm);
			$('#editarUsuario').modal('show');
		}

		function filtroUsuarios(a) {
			var resultado = true;

			if ( ! a.correo.includes(ref.search.usuario.toLowerCase()))
			{
				resultado = false;
			}

			if ( ! a.nombres.toLowerCase().includes(ref.search.nombres.toLowerCase()))
			{
				resultado = false;
			}

			if ( ! a.apellido_paterno.toLowerCase().includes(ref.search.apellido.toLowerCase()))
			{
				resultado = false;
			}

			a.usuarios_grupos.forEach(function(grupo) {
				if ( ! grupo.nombre.toLowerCase().includes(ref.search.grupo.toLowerCase()))
				{
					resultado = false;
				}
			});

			return resultado;
		}

		function grabarUsuario() {
			ref.editarUsuarioMessages = {};
			var data = common.serializeObj(ref.editarUsuarioForm_e);

			$http({
				url: common.base_url('usuarios/usuario/' + ref.editarUsuarioForm.id_usuario),
				method: 'PUT',
				data: data,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function successCallback(resp) {
				if ( ! resp.data.error) {
					listar();
					$('#editarUsuario').modal('hide');
				} else {
					ref.editarUsuarioMessages.error = resp.data.messages;
				}
			});
		}

		function pick(min, max) {
			var n, chars = '';

			if (typeof max === 'undefined') {
				n = min;
			} else {
				n = min + Math.floor(Math.random() * (max - min + 1));
			}

			for (var i = 0; i < n; i++) {
				chars += this.charAt(Math.floor(Math.random() * this.length));
			}

			return chars;
		}

		function shuffle() {
			var array = this.split('');
			var tmp, current, top = array.length;

			if (top) while (--top) {
				current = Math.floor(Math.random() * (top + 1));
				tmp = array[current];
				array[current] = array[top];
				array[top] = tmp;
			}

			return array.join('');
		}

		function usarRandom(usuario) {
			var specials = '@._-';
			var lowercase = 'abcdefghijklmnopqrstuvwxyz';
			var uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
			var numbers = '0123456789';
			var all = specials + lowercase + uppercase + numbers;
			var rnd = '';

			rnd += specials.pick(1);
			rnd += lowercase.pick(1);
			rnd += uppercase.pick(1);
			rnd += all.pick(9);
			rnd = rnd.shuffle();

			usuario.contrasena = rnd;
			usuario.confirmar_contrasena = rnd;
		}

		function disabledCrear() {
			if ( ! ref.nuevoUsuarioForm.correo || ! ref.nuevoUsuarioForm.nombres || ! ref.nuevoUsuarioForm.apellido_paterno || ! ref.nuevoUsuarioForm.contrasena || ! ref.nuevoUsuarioForm.confirmar_contrasena || ! ref.nuevoUsuarioForm.grupos.length) {
				return true;
			}
		}
	}
})();