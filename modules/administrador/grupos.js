(function() {
	'use strict';

	angular
		.module('app', ['selectize'])
		.controller('controlador', controlador);

	controlador.$inject = ['$http', 'common'];

	function controlador($http, common) {
		var ref = this;

		ref.config = {
			valueField: 'id',
			labelField: 'modulo',
			searchField: 'modulo',
			delimiter: '|',
			placeholder: 'Seleccionar módulo(s)'
		};
		ref.crearGrupo = crearGrupo;
		ref.crearGrupoMessages = {};
		ref.editandoGrupo = editandoGrupo;
		ref.editarGrupo = {};
		ref.editarGrupo_e = {};
		ref.editarGrupoMessages = {};
		ref.grabarGrupo = grabarGrupo;
		ref.grupos = [];
		ref.modulos = [];
		ref.nuevoGrupo = nuevoGrupo;
		ref.nuevoGrupoForm = {};

		listarGrupos();
		listarModulos();
		listarUsuarios();

		function listarGrupos() {
			$http({
				url: common.base_url('grupos/listar'),
				method: 'GET'
			}).then(function successCallback(resp) {
				if (resp.data.length) {
					ref.grupos = resp.data;

					ref.grupos.forEach(function(grupo) {
						grupo.permisos = [];

						grupo.grupos_modulos.forEach(function(modulo) {
							grupo.permisos.push(modulo.modulo);
						});
					});
				} else {
					ref.grupos = [];
				}
			});
		}

		function listarModulos() {
			$http({
				url: common.base_url('modulos/select'),
				method: 'GET'
			}).then(function successCallback(resp) {
				if (resp.data.length) {
					ref.modulos = resp.data;
				} else {
					ref.modulos = [];
				}
			});
		}

		function listarUsuarios() {
			$http({
				url: common.base_url('usuarios/listar'),
				method: 'GET'
			}).then(function successCallback(resp) {
				if (resp.data.length) {
					ref.usuarios = resp.data;

					ref.usuarios.forEach(function(usuario) {
						usuario.activo = Boolean(Number(usuario.activo));

						usuario.grupos = [];

						usuario.usuarios_grupos.forEach(function(grupo) {
							usuario.grupos.push(grupo.id);
						});
					});
				}
			});
		}

		$('#editarGrupo').on('hidden.bs.modal', function() {
			ref.editarGrupo = {};
			ref.editarGrupo_e = {};
			ref.editarGrupoMessages = {};
		});

		function editandoGrupo(info) {
			ref.editarGrupo = info;
			ref.editarGrupo_e = common.objEditar(ref.editarGrupo);
			$('#editarGrupo').modal('show');
		}

		function grabarGrupo() {
			ref.editarGrupoMessages = {};
			var data = common.serializeObj(ref.editarGrupo_e);

			$http({
				url: common.base_url('grupos/grupo/' + ref.editarGrupo_e.id),
				method: 'PUT',
				data: data,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function successCallback(resp) {
				if ( ! resp.data.error) {
					listarGrupos();
					$('#editarGrupo').modal('hide');
				} else {
					ref.editarGrupoMessages.error = resp.data.messages;
				}
			});
		}

		$('#nuevoGrupo').on('hidden.bs.modal', function() {
			ref.nuevoGrupoForm = {};
			ref.crearGrupoMessages = {};
		});

		function nuevoGrupo() {
			$('#nuevoGrupo').modal('show');
		}

		function crearGrupo() {
			ref.crearGrupoMessages = {};
			var data = common.serializeObj(ref.nuevoGrupoForm);

			$http({
				url: common.base_url('grupos/grupo'),
				method: 'POST',
				data: data,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function successCallback(resp) {
				if ( ! resp.data.error) {
					listarGrupos();
					$('#nuevoGrupo').modal('hide');
				} else {
					ref.crearGrupoMessages.error = resp.data.messages;
				}
			});
		}
	}
})();